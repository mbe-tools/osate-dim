/**
 */
package fr.mem4csd.osatedim.deltatrace.aaxl2aaxl;

import org.eclipse.emf.ecore.EFactory;

/**
 * <!-- begin-user-doc -->
 * The <b>Factory</b> for the model.
 * It provides a create method for each non-abstract class of the model.
 * <!-- end-user-doc -->
 * @see fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlPackage
 * @generated
 */
public interface Aaxl2AaxlFactory extends EFactory {
	/**
	 * The singleton instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Aaxl2AaxlFactory eINSTANCE = fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.impl.Aaxl2AaxlFactoryImpl.init();
	/**
	 * Returns a new object of class '<em>Trace Spec</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Trace Spec</em>'.
	 * @generated
	 */
	Aaxl2AaxlTraceSpec createAaxl2AaxlTraceSpec();

	/**
	 * Returns a new object of class '<em>Trace</em>'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return a new object of class '<em>Trace</em>'.
	 * @generated
	 */
	Aaxl2AaxlTrace createAaxl2AaxlTrace();

	/**
	 * Returns the package supported by this factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the package supported by this factory.
	 * @generated
	 */
	Aaxl2AaxlPackage getAaxl2AaxlPackage();

} //Aaxl2AaxlFactory
