/**
 */
package fr.mem4csd.osatedim.deltatrace.aaxl2aaxl;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EObject;

import org.eclipse.emf.ecore.change.FeatureChange;

import org.osate.aadl2.Element;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Trace</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace#getRightInstance <em>Right Instance</em>}</li>
 *   <li>{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace#getLeftInstance <em>Left Instance</em>}</li>
 *   <li>{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace#getObjectsToAttach <em>Objects To Attach</em>}</li>
 *   <li>{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace#getObjectChanges <em>Object Changes</em>}</li>
 *   <li>{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace#getObjectsToDetach <em>Objects To Detach</em>}</li>
 * </ul>
 *
 * @see fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlPackage#getAaxl2AaxlTrace()
 * @model
 * @generated
 */
public interface Aaxl2AaxlTrace extends EObject {
	/**
	 * Returns the value of the '<em><b>Right Instance</b></em>' reference list.
	 * The list contents are of type {@link org.osate.aadl2.Element}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right Instance</em>' reference list.
	 * @see fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlPackage#getAaxl2AaxlTrace_RightInstance()
	 * @model
	 * @generated
	 */
	EList<Element> getRightInstance();

	/**
	 * Returns the value of the '<em><b>Left Instance</b></em>' reference list.
	 * The list contents are of type {@link org.osate.aadl2.Element}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left Instance</em>' reference list.
	 * @see fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlPackage#getAaxl2AaxlTrace_LeftInstance()
	 * @model
	 * @generated
	 */
	EList<Element> getLeftInstance();

	/**
	 * Returns the value of the '<em><b>Objects To Attach</b></em>' reference list.
	 * The list contents are of type {@link org.osate.aadl2.Element}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Objects To Attach</em>' reference list.
	 * @see fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlPackage#getAaxl2AaxlTrace_ObjectsToAttach()
	 * @model
	 * @generated
	 */
	EList<Element> getObjectsToAttach();

	/**
	 * Returns the value of the '<em><b>Object Changes</b></em>' map.
	 * The key is of type {@link org.eclipse.emf.ecore.EObject},
	 * and the value is of type list of {@link org.eclipse.emf.ecore.change.FeatureChange},
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Object Changes</em>' map.
	 * @see fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlPackage#getAaxl2AaxlTrace_ObjectChanges()
	 * @model mapType="org.eclipse.emf.ecore.change.EObjectToChangesMapEntry&lt;org.eclipse.emf.ecore.EObject, org.eclipse.emf.ecore.change.FeatureChange&gt;"
	 * @generated
	 */
	EMap<EObject, EList<FeatureChange>> getObjectChanges();

	/**
	 * Returns the value of the '<em><b>Objects To Detach</b></em>' reference list.
	 * The list contents are of type {@link org.osate.aadl2.Element}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Objects To Detach</em>' reference list.
	 * @see fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlPackage#getAaxl2AaxlTrace_ObjectsToDetach()
	 * @model
	 * @generated
	 */
	EList<Element> getObjectsToDetach();

} // Aaxl2AaxlTrace
