/**
 */
package fr.mem4csd.osatedim.deltatrace.aaxl2aaxl;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EObject;

import org.osate.aadl2.instance.SystemInstance;

/**
 * <!-- begin-user-doc -->
 * A representation of the model object '<em><b>Trace Spec</b></em>'.
 * <!-- end-user-doc -->
 *
 * <p>
 * The following features are supported:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTraceSpec#getRightSystem <em>Right System</em>}</li>
 *   <li>{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTraceSpec#getLeftSystem <em>Left System</em>}</li>
 *   <li>{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTraceSpec#getTraces <em>Traces</em>}</li>
 * </ul>
 *
 * @see fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlPackage#getAaxl2AaxlTraceSpec()
 * @model
 * @generated
 */
public interface Aaxl2AaxlTraceSpec extends EObject {
	/**
	 * Returns the value of the '<em><b>Right System</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Right System</em>' reference.
	 * @see #setRightSystem(SystemInstance)
	 * @see fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlPackage#getAaxl2AaxlTraceSpec_RightSystem()
	 * @model
	 * @generated
	 */
	SystemInstance getRightSystem();

	/**
	 * Sets the value of the '{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTraceSpec#getRightSystem <em>Right System</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Right System</em>' reference.
	 * @see #getRightSystem()
	 * @generated
	 */
	void setRightSystem(SystemInstance value);

	/**
	 * Returns the value of the '<em><b>Left System</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Left System</em>' reference.
	 * @see #setLeftSystem(SystemInstance)
	 * @see fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlPackage#getAaxl2AaxlTraceSpec_LeftSystem()
	 * @model
	 * @generated
	 */
	SystemInstance getLeftSystem();

	/**
	 * Sets the value of the '{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTraceSpec#getLeftSystem <em>Left System</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @param value the new value of the '<em>Left System</em>' reference.
	 * @see #getLeftSystem()
	 * @generated
	 */
	void setLeftSystem(SystemInstance value);

	/**
	 * Returns the value of the '<em><b>Traces</b></em>' reference list.
	 * The list contents are of type {@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace}.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the value of the '<em>Traces</em>' reference list.
	 * @see fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlPackage#getAaxl2AaxlTraceSpec_Traces()
	 * @model
	 * @generated
	 */
	EList<Aaxl2AaxlTrace> getTraces();

} // Aaxl2AaxlTraceSpec
