/**
 */
package fr.mem4csd.osatedim.deltatrace.aaxl2aaxl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;

/**
 * <!-- begin-user-doc -->
 * The <b>Package</b> for the model.
 * It contains accessors for the meta objects to represent
 * <ul>
 *   <li>each class,</li>
 *   <li>each feature of each class,</li>
 *   <li>each operation of each class,</li>
 *   <li>each enum,</li>
 *   <li>and each data type</li>
 * </ul>
 * <!-- end-user-doc -->
 * @see fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlFactory
 * @model kind="package"
 * @generated
 */
public interface Aaxl2AaxlPackage extends EPackage {
	/**
	 * The package name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNAME = "aaxl2aaxl";

	/**
	 * The package namespace URI.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_URI = "http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl";

	/**
	 * The package namespace name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	String eNS_PREFIX = "aaxl2aaxl";

	/**
	 * The singleton instance of the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	Aaxl2AaxlPackage eINSTANCE = fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.impl.Aaxl2AaxlPackageImpl.init();

	/**
	 * The meta object id for the '{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.impl.Aaxl2AaxlTraceSpecImpl <em>Trace Spec</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.impl.Aaxl2AaxlTraceSpecImpl
	 * @see fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.impl.Aaxl2AaxlPackageImpl#getAaxl2AaxlTraceSpec()
	 * @generated
	 */
	int AAXL2_AAXL_TRACE_SPEC = 0;

	/**
	 * The feature id for the '<em><b>Right System</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AAXL2_AAXL_TRACE_SPEC__RIGHT_SYSTEM = 0;

	/**
	 * The feature id for the '<em><b>Left System</b></em>' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AAXL2_AAXL_TRACE_SPEC__LEFT_SYSTEM = 1;

	/**
	 * The feature id for the '<em><b>Traces</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AAXL2_AAXL_TRACE_SPEC__TRACES = 2;

	/**
	 * The number of structural features of the '<em>Trace Spec</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AAXL2_AAXL_TRACE_SPEC_FEATURE_COUNT = 3;

	/**
	 * The number of operations of the '<em>Trace Spec</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AAXL2_AAXL_TRACE_SPEC_OPERATION_COUNT = 0;

	/**
	 * The meta object id for the '{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.impl.Aaxl2AaxlTraceImpl <em>Trace</em>}' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.impl.Aaxl2AaxlTraceImpl
	 * @see fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.impl.Aaxl2AaxlPackageImpl#getAaxl2AaxlTrace()
	 * @generated
	 */
	int AAXL2_AAXL_TRACE = 1;

	/**
	 * The feature id for the '<em><b>Right Instance</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AAXL2_AAXL_TRACE__RIGHT_INSTANCE = 0;

	/**
	 * The feature id for the '<em><b>Left Instance</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AAXL2_AAXL_TRACE__LEFT_INSTANCE = 1;

	/**
	 * The feature id for the '<em><b>Objects To Attach</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AAXL2_AAXL_TRACE__OBJECTS_TO_ATTACH = 2;

	/**
	 * The feature id for the '<em><b>Object Changes</b></em>' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AAXL2_AAXL_TRACE__OBJECT_CHANGES = 3;

	/**
	 * The feature id for the '<em><b>Objects To Detach</b></em>' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AAXL2_AAXL_TRACE__OBJECTS_TO_DETACH = 4;

	/**
	 * The number of structural features of the '<em>Trace</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AAXL2_AAXL_TRACE_FEATURE_COUNT = 5;

	/**
	 * The number of operations of the '<em>Trace</em>' class.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 * @ordered
	 */
	int AAXL2_AAXL_TRACE_OPERATION_COUNT = 0;


	/**
	 * Returns the meta object for class '{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTraceSpec <em>Trace Spec</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Trace Spec</em>'.
	 * @see fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTraceSpec
	 * @generated
	 */
	EClass getAaxl2AaxlTraceSpec();

	/**
	 * Returns the meta object for the reference '{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTraceSpec#getRightSystem <em>Right System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Right System</em>'.
	 * @see fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTraceSpec#getRightSystem()
	 * @see #getAaxl2AaxlTraceSpec()
	 * @generated
	 */
	EReference getAaxl2AaxlTraceSpec_RightSystem();

	/**
	 * Returns the meta object for the reference '{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTraceSpec#getLeftSystem <em>Left System</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference '<em>Left System</em>'.
	 * @see fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTraceSpec#getLeftSystem()
	 * @see #getAaxl2AaxlTraceSpec()
	 * @generated
	 */
	EReference getAaxl2AaxlTraceSpec_LeftSystem();

	/**
	 * Returns the meta object for the reference list '{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTraceSpec#getTraces <em>Traces</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Traces</em>'.
	 * @see fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTraceSpec#getTraces()
	 * @see #getAaxl2AaxlTraceSpec()
	 * @generated
	 */
	EReference getAaxl2AaxlTraceSpec_Traces();

	/**
	 * Returns the meta object for class '{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace <em>Trace</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for class '<em>Trace</em>'.
	 * @see fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace
	 * @generated
	 */
	EClass getAaxl2AaxlTrace();

	/**
	 * Returns the meta object for the reference list '{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace#getRightInstance <em>Right Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Right Instance</em>'.
	 * @see fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace#getRightInstance()
	 * @see #getAaxl2AaxlTrace()
	 * @generated
	 */
	EReference getAaxl2AaxlTrace_RightInstance();

	/**
	 * Returns the meta object for the reference list '{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace#getLeftInstance <em>Left Instance</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Left Instance</em>'.
	 * @see fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace#getLeftInstance()
	 * @see #getAaxl2AaxlTrace()
	 * @generated
	 */
	EReference getAaxl2AaxlTrace_LeftInstance();

	/**
	 * Returns the meta object for the reference list '{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace#getObjectsToAttach <em>Objects To Attach</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Objects To Attach</em>'.
	 * @see fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace#getObjectsToAttach()
	 * @see #getAaxl2AaxlTrace()
	 * @generated
	 */
	EReference getAaxl2AaxlTrace_ObjectsToAttach();

	/**
	 * Returns the meta object for the map '{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace#getObjectChanges <em>Object Changes</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the map '<em>Object Changes</em>'.
	 * @see fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace#getObjectChanges()
	 * @see #getAaxl2AaxlTrace()
	 * @generated
	 */
	EReference getAaxl2AaxlTrace_ObjectChanges();

	/**
	 * Returns the meta object for the reference list '{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace#getObjectsToDetach <em>Objects To Detach</em>}'.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the meta object for the reference list '<em>Objects To Detach</em>'.
	 * @see fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace#getObjectsToDetach()
	 * @see #getAaxl2AaxlTrace()
	 * @generated
	 */
	EReference getAaxl2AaxlTrace_ObjectsToDetach();

	/**
	 * Returns the factory that creates the instances of the model.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @return the factory that creates the instances of the model.
	 * @generated
	 */
	Aaxl2AaxlFactory getAaxl2AaxlFactory();

	/**
	 * <!-- begin-user-doc -->
	 * Defines literals for the meta objects that represent
	 * <ul>
	 *   <li>each class,</li>
	 *   <li>each feature of each class,</li>
	 *   <li>each operation of each class,</li>
	 *   <li>each enum,</li>
	 *   <li>and each data type</li>
	 * </ul>
	 * <!-- end-user-doc -->
	 * @generated
	 */
	interface Literals {
		/**
		 * The meta object literal for the '{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.impl.Aaxl2AaxlTraceSpecImpl <em>Trace Spec</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.impl.Aaxl2AaxlTraceSpecImpl
		 * @see fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.impl.Aaxl2AaxlPackageImpl#getAaxl2AaxlTraceSpec()
		 * @generated
		 */
		EClass AAXL2_AAXL_TRACE_SPEC = eINSTANCE.getAaxl2AaxlTraceSpec();

		/**
		 * The meta object literal for the '<em><b>Right System</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AAXL2_AAXL_TRACE_SPEC__RIGHT_SYSTEM = eINSTANCE.getAaxl2AaxlTraceSpec_RightSystem();

		/**
		 * The meta object literal for the '<em><b>Left System</b></em>' reference feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AAXL2_AAXL_TRACE_SPEC__LEFT_SYSTEM = eINSTANCE.getAaxl2AaxlTraceSpec_LeftSystem();

		/**
		 * The meta object literal for the '<em><b>Traces</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AAXL2_AAXL_TRACE_SPEC__TRACES = eINSTANCE.getAaxl2AaxlTraceSpec_Traces();

		/**
		 * The meta object literal for the '{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.impl.Aaxl2AaxlTraceImpl <em>Trace</em>}' class.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @see fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.impl.Aaxl2AaxlTraceImpl
		 * @see fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.impl.Aaxl2AaxlPackageImpl#getAaxl2AaxlTrace()
		 * @generated
		 */
		EClass AAXL2_AAXL_TRACE = eINSTANCE.getAaxl2AaxlTrace();

		/**
		 * The meta object literal for the '<em><b>Right Instance</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AAXL2_AAXL_TRACE__RIGHT_INSTANCE = eINSTANCE.getAaxl2AaxlTrace_RightInstance();

		/**
		 * The meta object literal for the '<em><b>Left Instance</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AAXL2_AAXL_TRACE__LEFT_INSTANCE = eINSTANCE.getAaxl2AaxlTrace_LeftInstance();

		/**
		 * The meta object literal for the '<em><b>Objects To Attach</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AAXL2_AAXL_TRACE__OBJECTS_TO_ATTACH = eINSTANCE.getAaxl2AaxlTrace_ObjectsToAttach();

		/**
		 * The meta object literal for the '<em><b>Object Changes</b></em>' map feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AAXL2_AAXL_TRACE__OBJECT_CHANGES = eINSTANCE.getAaxl2AaxlTrace_ObjectChanges();

		/**
		 * The meta object literal for the '<em><b>Objects To Detach</b></em>' reference list feature.
		 * <!-- begin-user-doc -->
		 * <!-- end-user-doc -->
		 * @generated
		 */
		EReference AAXL2_AAXL_TRACE__OBJECTS_TO_DETACH = eINSTANCE.getAaxl2AaxlTrace_ObjectsToDetach();

	}

} //Aaxl2AaxlPackage
