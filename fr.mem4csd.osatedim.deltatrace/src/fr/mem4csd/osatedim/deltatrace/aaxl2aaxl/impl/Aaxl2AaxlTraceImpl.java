/**
 */
package fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.NotificationChain;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.common.util.EMap;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EStructuralFeature;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.change.ChangePackage;
import org.eclipse.emf.ecore.change.FeatureChange;

import org.eclipse.emf.ecore.change.impl.EObjectToChangesMapEntryImpl;

import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;
import org.eclipse.emf.ecore.util.EcoreEMap;
import org.eclipse.emf.ecore.util.InternalEList;

import org.osate.aadl2.Element;
import fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlPackage;
import fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Trace</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.impl.Aaxl2AaxlTraceImpl#getRightInstance <em>Right Instance</em>}</li>
 *   <li>{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.impl.Aaxl2AaxlTraceImpl#getLeftInstance <em>Left Instance</em>}</li>
 *   <li>{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.impl.Aaxl2AaxlTraceImpl#getObjectsToAttach <em>Objects To Attach</em>}</li>
 *   <li>{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.impl.Aaxl2AaxlTraceImpl#getObjectChanges <em>Object Changes</em>}</li>
 *   <li>{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.impl.Aaxl2AaxlTraceImpl#getObjectsToDetach <em>Objects To Detach</em>}</li>
 * </ul>
 *
 * @generated
 */
public class Aaxl2AaxlTraceImpl extends MinimalEObjectImpl.Container implements Aaxl2AaxlTrace {
	/**
	 * The cached value of the '{@link #getRightInstance() <em>Right Instance</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRightInstance()
	 * @generated
	 * @ordered
	 */
	protected EList<Element> rightInstance;

	/**
	 * The cached value of the '{@link #getLeftInstance() <em>Left Instance</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLeftInstance()
	 * @generated
	 * @ordered
	 */
	protected EList<Element> leftInstance;

	/**
	 * The cached value of the '{@link #getObjectsToAttach() <em>Objects To Attach</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectsToAttach()
	 * @generated
	 * @ordered
	 */
	protected EList<Element> objectsToAttach;

	/**
	 * The cached value of the '{@link #getObjectChanges() <em>Object Changes</em>}' map.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectChanges()
	 * @generated
	 * @ordered
	 */
	protected EMap<EObject, EList<FeatureChange>> objectChanges;

	/**
	 * The cached value of the '{@link #getObjectsToDetach() <em>Objects To Detach</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getObjectsToDetach()
	 * @generated
	 * @ordered
	 */
	protected EList<Element> objectsToDetach;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Aaxl2AaxlTraceImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Aaxl2AaxlPackage.Literals.AAXL2_AAXL_TRACE;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Element> getRightInstance() {
		if (rightInstance == null) {
			rightInstance = new EObjectResolvingEList<Element>(Element.class, this, Aaxl2AaxlPackage.AAXL2_AAXL_TRACE__RIGHT_INSTANCE);
		}
		return rightInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Element> getLeftInstance() {
		if (leftInstance == null) {
			leftInstance = new EObjectResolvingEList<Element>(Element.class, this, Aaxl2AaxlPackage.AAXL2_AAXL_TRACE__LEFT_INSTANCE);
		}
		return leftInstance;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Element> getObjectsToAttach() {
		if (objectsToAttach == null) {
			objectsToAttach = new EObjectResolvingEList<Element>(Element.class, this, Aaxl2AaxlPackage.AAXL2_AAXL_TRACE__OBJECTS_TO_ATTACH);
		}
		return objectsToAttach;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EMap<EObject, EList<FeatureChange>> getObjectChanges() {
		if (objectChanges == null) {
			objectChanges = new EcoreEMap<EObject,EList<FeatureChange>>(ChangePackage.Literals.EOBJECT_TO_CHANGES_MAP_ENTRY, EObjectToChangesMapEntryImpl.class, this, Aaxl2AaxlPackage.AAXL2_AAXL_TRACE__OBJECT_CHANGES);
		}
		return objectChanges;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Element> getObjectsToDetach() {
		if (objectsToDetach == null) {
			objectsToDetach = new EObjectResolvingEList<Element>(Element.class, this, Aaxl2AaxlPackage.AAXL2_AAXL_TRACE__OBJECTS_TO_DETACH);
		}
		return objectsToDetach;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public NotificationChain eInverseRemove(InternalEObject otherEnd, int featureID, NotificationChain msgs) {
		switch (featureID) {
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE__OBJECT_CHANGES:
				return ((InternalEList<?>)getObjectChanges()).basicRemove(otherEnd, msgs);
		}
		return super.eInverseRemove(otherEnd, featureID, msgs);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE__RIGHT_INSTANCE:
				return getRightInstance();
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE__LEFT_INSTANCE:
				return getLeftInstance();
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE__OBJECTS_TO_ATTACH:
				return getObjectsToAttach();
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE__OBJECT_CHANGES:
				if (coreType) return getObjectChanges();
				else return getObjectChanges().map();
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE__OBJECTS_TO_DETACH:
				return getObjectsToDetach();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE__RIGHT_INSTANCE:
				getRightInstance().clear();
				getRightInstance().addAll((Collection<? extends Element>)newValue);
				return;
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE__LEFT_INSTANCE:
				getLeftInstance().clear();
				getLeftInstance().addAll((Collection<? extends Element>)newValue);
				return;
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE__OBJECTS_TO_ATTACH:
				getObjectsToAttach().clear();
				getObjectsToAttach().addAll((Collection<? extends Element>)newValue);
				return;
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE__OBJECT_CHANGES:
				((EStructuralFeature.Setting)getObjectChanges()).set(newValue);
				return;
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE__OBJECTS_TO_DETACH:
				getObjectsToDetach().clear();
				getObjectsToDetach().addAll((Collection<? extends Element>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE__RIGHT_INSTANCE:
				getRightInstance().clear();
				return;
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE__LEFT_INSTANCE:
				getLeftInstance().clear();
				return;
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE__OBJECTS_TO_ATTACH:
				getObjectsToAttach().clear();
				return;
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE__OBJECT_CHANGES:
				getObjectChanges().clear();
				return;
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE__OBJECTS_TO_DETACH:
				getObjectsToDetach().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE__RIGHT_INSTANCE:
				return rightInstance != null && !rightInstance.isEmpty();
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE__LEFT_INSTANCE:
				return leftInstance != null && !leftInstance.isEmpty();
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE__OBJECTS_TO_ATTACH:
				return objectsToAttach != null && !objectsToAttach.isEmpty();
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE__OBJECT_CHANGES:
				return objectChanges != null && !objectChanges.isEmpty();
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE__OBJECTS_TO_DETACH:
				return objectsToDetach != null && !objectsToDetach.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //Aaxl2AaxlTraceImpl
