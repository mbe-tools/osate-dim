/**
 */
package fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EPackage;

import org.eclipse.emf.ecore.impl.EFactoryImpl;

import org.eclipse.emf.ecore.plugin.EcorePlugin;

import fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.*;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Factory</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Aaxl2AaxlFactoryImpl extends EFactoryImpl implements Aaxl2AaxlFactory {
	/**
	 * Creates the default factory implementation.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Aaxl2AaxlFactory init() {
		try {
			Aaxl2AaxlFactory theAaxl2AaxlFactory = (Aaxl2AaxlFactory)EPackage.Registry.INSTANCE.getEFactory(Aaxl2AaxlPackage.eNS_URI);
			if (theAaxl2AaxlFactory != null) {
				return theAaxl2AaxlFactory;
			}
		}
		catch (Exception exception) {
			EcorePlugin.INSTANCE.log(exception);
		}
		return new Aaxl2AaxlFactoryImpl();
	}

	/**
	 * Creates an instance of the factory.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Aaxl2AaxlFactoryImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public EObject create(EClass eClass) {
		switch (eClass.getClassifierID()) {
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE_SPEC: return createAaxl2AaxlTraceSpec();
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE: return createAaxl2AaxlTrace();
			default:
				throw new IllegalArgumentException("The class '" + eClass.getName() + "' is not a valid classifier");
		}
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Aaxl2AaxlTraceSpec createAaxl2AaxlTraceSpec() {
		Aaxl2AaxlTraceSpecImpl aaxl2AaxlTraceSpec = new Aaxl2AaxlTraceSpecImpl();
		return aaxl2AaxlTraceSpec;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Aaxl2AaxlTrace createAaxl2AaxlTrace() {
		Aaxl2AaxlTraceImpl aaxl2AaxlTrace = new Aaxl2AaxlTraceImpl();
		return aaxl2AaxlTrace;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Aaxl2AaxlPackage getAaxl2AaxlPackage() {
		return (Aaxl2AaxlPackage)getEPackage();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @deprecated
	 * @generated
	 */
	@Deprecated
	public static Aaxl2AaxlPackage getPackage() {
		return Aaxl2AaxlPackage.eINSTANCE;
	}

} //Aaxl2AaxlFactoryImpl
