/**
 */
package fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.impl;

import java.util.Collection;

import org.eclipse.emf.common.notify.Notification;

import org.eclipse.emf.common.util.EList;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.InternalEObject;

import org.eclipse.emf.ecore.impl.ENotificationImpl;
import org.eclipse.emf.ecore.impl.MinimalEObjectImpl;

import org.eclipse.emf.ecore.util.EObjectResolvingEList;

import org.osate.aadl2.instance.SystemInstance;

import fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlPackage;
import fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace;
import fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTraceSpec;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model object '<em><b>Trace Spec</b></em>'.
 * <!-- end-user-doc -->
 * <p>
 * The following features are implemented:
 * </p>
 * <ul>
 *   <li>{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.impl.Aaxl2AaxlTraceSpecImpl#getRightSystem <em>Right System</em>}</li>
 *   <li>{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.impl.Aaxl2AaxlTraceSpecImpl#getLeftSystem <em>Left System</em>}</li>
 *   <li>{@link fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.impl.Aaxl2AaxlTraceSpecImpl#getTraces <em>Traces</em>}</li>
 * </ul>
 *
 * @generated
 */
public class Aaxl2AaxlTraceSpecImpl extends MinimalEObjectImpl.Container implements Aaxl2AaxlTraceSpec {
	/**
	 * The cached value of the '{@link #getRightSystem() <em>Right System</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getRightSystem()
	 * @generated
	 * @ordered
	 */
	protected SystemInstance rightSystem;

	/**
	 * The cached value of the '{@link #getLeftSystem() <em>Left System</em>}' reference.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getLeftSystem()
	 * @generated
	 * @ordered
	 */
	protected SystemInstance leftSystem;

	/**
	 * The cached value of the '{@link #getTraces() <em>Traces</em>}' reference list.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #getTraces()
	 * @generated
	 * @ordered
	 */
	protected EList<Aaxl2AaxlTrace> traces;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Aaxl2AaxlTraceSpecImpl() {
		super();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	protected EClass eStaticClass() {
		return Aaxl2AaxlPackage.Literals.AAXL2_AAXL_TRACE_SPEC;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemInstance getRightSystem() {
		if (rightSystem != null && rightSystem.eIsProxy()) {
			InternalEObject oldRightSystem = (InternalEObject)rightSystem;
			rightSystem = (SystemInstance)eResolveProxy(oldRightSystem);
			if (rightSystem != oldRightSystem) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Aaxl2AaxlPackage.AAXL2_AAXL_TRACE_SPEC__RIGHT_SYSTEM, oldRightSystem, rightSystem));
			}
		}
		return rightSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemInstance basicGetRightSystem() {
		return rightSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setRightSystem(SystemInstance newRightSystem) {
		SystemInstance oldRightSystem = rightSystem;
		rightSystem = newRightSystem;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Aaxl2AaxlPackage.AAXL2_AAXL_TRACE_SPEC__RIGHT_SYSTEM, oldRightSystem, rightSystem));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemInstance getLeftSystem() {
		if (leftSystem != null && leftSystem.eIsProxy()) {
			InternalEObject oldLeftSystem = (InternalEObject)leftSystem;
			leftSystem = (SystemInstance)eResolveProxy(oldLeftSystem);
			if (leftSystem != oldLeftSystem) {
				if (eNotificationRequired())
					eNotify(new ENotificationImpl(this, Notification.RESOLVE, Aaxl2AaxlPackage.AAXL2_AAXL_TRACE_SPEC__LEFT_SYSTEM, oldLeftSystem, leftSystem));
			}
		}
		return leftSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public SystemInstance basicGetLeftSystem() {
		return leftSystem;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void setLeftSystem(SystemInstance newLeftSystem) {
		SystemInstance oldLeftSystem = leftSystem;
		leftSystem = newLeftSystem;
		if (eNotificationRequired())
			eNotify(new ENotificationImpl(this, Notification.SET, Aaxl2AaxlPackage.AAXL2_AAXL_TRACE_SPEC__LEFT_SYSTEM, oldLeftSystem, leftSystem));
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EList<Aaxl2AaxlTrace> getTraces() {
		if (traces == null) {
			traces = new EObjectResolvingEList<Aaxl2AaxlTrace>(Aaxl2AaxlTrace.class, this, Aaxl2AaxlPackage.AAXL2_AAXL_TRACE_SPEC__TRACES);
		}
		return traces;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public Object eGet(int featureID, boolean resolve, boolean coreType) {
		switch (featureID) {
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE_SPEC__RIGHT_SYSTEM:
				if (resolve) return getRightSystem();
				return basicGetRightSystem();
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE_SPEC__LEFT_SYSTEM:
				if (resolve) return getLeftSystem();
				return basicGetLeftSystem();
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE_SPEC__TRACES:
				return getTraces();
		}
		return super.eGet(featureID, resolve, coreType);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@SuppressWarnings("unchecked")
	@Override
	public void eSet(int featureID, Object newValue) {
		switch (featureID) {
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE_SPEC__RIGHT_SYSTEM:
				setRightSystem((SystemInstance)newValue);
				return;
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE_SPEC__LEFT_SYSTEM:
				setLeftSystem((SystemInstance)newValue);
				return;
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE_SPEC__TRACES:
				getTraces().clear();
				getTraces().addAll((Collection<? extends Aaxl2AaxlTrace>)newValue);
				return;
		}
		super.eSet(featureID, newValue);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public void eUnset(int featureID) {
		switch (featureID) {
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE_SPEC__RIGHT_SYSTEM:
				setRightSystem((SystemInstance)null);
				return;
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE_SPEC__LEFT_SYSTEM:
				setLeftSystem((SystemInstance)null);
				return;
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE_SPEC__TRACES:
				getTraces().clear();
				return;
		}
		super.eUnset(featureID);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	@Override
	public boolean eIsSet(int featureID) {
		switch (featureID) {
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE_SPEC__RIGHT_SYSTEM:
				return rightSystem != null;
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE_SPEC__LEFT_SYSTEM:
				return leftSystem != null;
			case Aaxl2AaxlPackage.AAXL2_AAXL_TRACE_SPEC__TRACES:
				return traces != null && !traces.isEmpty();
		}
		return super.eIsSet(featureID);
	}

} //Aaxl2AaxlTraceSpecImpl
