/**
 */
package fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.impl;

import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EPackage;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.EcorePackage;

import org.eclipse.emf.ecore.change.ChangePackage;

import org.eclipse.emf.ecore.impl.EPackageImpl;

import org.osate.aadl2.Aadl2Package;

import org.osate.aadl2.instance.InstancePackage;

import fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlFactory;
import fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlPackage;
import fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace;
import fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTraceSpec;

/**
 * <!-- begin-user-doc -->
 * An implementation of the model <b>Package</b>.
 * <!-- end-user-doc -->
 * @generated
 */
public class Aaxl2AaxlPackageImpl extends EPackageImpl implements Aaxl2AaxlPackage {
	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aaxl2AaxlTraceSpecEClass = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private EClass aaxl2AaxlTraceEClass = null;

	/**
	 * Creates an instance of the model <b>Package</b>, registered with
	 * {@link org.eclipse.emf.ecore.EPackage.Registry EPackage.Registry} by the package
	 * package URI value.
	 * <p>Note: the correct way to create the package is via the static
	 * factory method {@link #init init()}, which also performs
	 * initialization of the package, or returns the registered package,
	 * if one already exists.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see org.eclipse.emf.ecore.EPackage.Registry
	 * @see fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlPackage#eNS_URI
	 * @see #init()
	 * @generated
	 */
	private Aaxl2AaxlPackageImpl() {
		super(eNS_URI, Aaxl2AaxlFactory.eINSTANCE);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private static boolean isInited = false;

	/**
	 * Creates, registers, and initializes the <b>Package</b> for this model, and for any others upon which it depends.
	 *
	 * <p>This method is used to initialize {@link Aaxl2AaxlPackage#eINSTANCE} when that field is accessed.
	 * Clients should not invoke it directly. Instead, they should simply access that field to obtain the package.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see #eNS_URI
	 * @see #createPackageContents()
	 * @see #initializePackageContents()
	 * @generated
	 */
	public static Aaxl2AaxlPackage init() {
		if (isInited) return (Aaxl2AaxlPackage)EPackage.Registry.INSTANCE.getEPackage(Aaxl2AaxlPackage.eNS_URI);

		// Obtain or create and register package
		Object registeredAaxl2AaxlPackage = EPackage.Registry.INSTANCE.get(eNS_URI);
		Aaxl2AaxlPackageImpl theAaxl2AaxlPackage = registeredAaxl2AaxlPackage instanceof Aaxl2AaxlPackageImpl ? (Aaxl2AaxlPackageImpl)registeredAaxl2AaxlPackage : new Aaxl2AaxlPackageImpl();

		isInited = true;

		// Initialize simple dependencies
		Aadl2Package.eINSTANCE.eClass();
		ChangePackage.eINSTANCE.eClass();
		EcorePackage.eINSTANCE.eClass();
		InstancePackage.eINSTANCE.eClass();

		// Create package meta-data objects
		theAaxl2AaxlPackage.createPackageContents();

		// Initialize created meta-data
		theAaxl2AaxlPackage.initializePackageContents();

		// Mark meta-data to indicate it can't be changed
		theAaxl2AaxlPackage.freeze();

		// Update the registry and return the package
		EPackage.Registry.INSTANCE.put(Aaxl2AaxlPackage.eNS_URI, theAaxl2AaxlPackage);
		return theAaxl2AaxlPackage;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAaxl2AaxlTraceSpec() {
		return aaxl2AaxlTraceSpecEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAaxl2AaxlTraceSpec_RightSystem() {
		return (EReference)aaxl2AaxlTraceSpecEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAaxl2AaxlTraceSpec_LeftSystem() {
		return (EReference)aaxl2AaxlTraceSpecEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAaxl2AaxlTraceSpec_Traces() {
		return (EReference)aaxl2AaxlTraceSpecEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EClass getAaxl2AaxlTrace() {
		return aaxl2AaxlTraceEClass;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAaxl2AaxlTrace_RightInstance() {
		return (EReference)aaxl2AaxlTraceEClass.getEStructuralFeatures().get(0);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAaxl2AaxlTrace_LeftInstance() {
		return (EReference)aaxl2AaxlTraceEClass.getEStructuralFeatures().get(1);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAaxl2AaxlTrace_ObjectsToAttach() {
		return (EReference)aaxl2AaxlTraceEClass.getEStructuralFeatures().get(2);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAaxl2AaxlTrace_ObjectChanges() {
		return (EReference)aaxl2AaxlTraceEClass.getEStructuralFeatures().get(3);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public EReference getAaxl2AaxlTrace_ObjectsToDetach() {
		return (EReference)aaxl2AaxlTraceEClass.getEStructuralFeatures().get(4);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Aaxl2AaxlFactory getAaxl2AaxlFactory() {
		return (Aaxl2AaxlFactory)getEFactoryInstance();
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isCreated = false;

	/**
	 * Creates the meta-model objects for the package.  This method is
	 * guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void createPackageContents() {
		if (isCreated) return;
		isCreated = true;

		// Create classes and their features
		aaxl2AaxlTraceSpecEClass = createEClass(AAXL2_AAXL_TRACE_SPEC);
		createEReference(aaxl2AaxlTraceSpecEClass, AAXL2_AAXL_TRACE_SPEC__RIGHT_SYSTEM);
		createEReference(aaxl2AaxlTraceSpecEClass, AAXL2_AAXL_TRACE_SPEC__LEFT_SYSTEM);
		createEReference(aaxl2AaxlTraceSpecEClass, AAXL2_AAXL_TRACE_SPEC__TRACES);

		aaxl2AaxlTraceEClass = createEClass(AAXL2_AAXL_TRACE);
		createEReference(aaxl2AaxlTraceEClass, AAXL2_AAXL_TRACE__RIGHT_INSTANCE);
		createEReference(aaxl2AaxlTraceEClass, AAXL2_AAXL_TRACE__LEFT_INSTANCE);
		createEReference(aaxl2AaxlTraceEClass, AAXL2_AAXL_TRACE__OBJECTS_TO_ATTACH);
		createEReference(aaxl2AaxlTraceEClass, AAXL2_AAXL_TRACE__OBJECT_CHANGES);
		createEReference(aaxl2AaxlTraceEClass, AAXL2_AAXL_TRACE__OBJECTS_TO_DETACH);
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	private boolean isInitialized = false;

	/**
	 * Complete the initialization of the package and its meta-model.  This
	 * method is guarded to have no affect on any invocation but its first.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public void initializePackageContents() {
		if (isInitialized) return;
		isInitialized = true;

		// Initialize package
		setName(eNAME);
		setNsPrefix(eNS_PREFIX);
		setNsURI(eNS_URI);

		// Obtain other dependent packages
		InstancePackage theInstancePackage = (InstancePackage)EPackage.Registry.INSTANCE.getEPackage(InstancePackage.eNS_URI);
		Aadl2Package theAadl2Package = (Aadl2Package)EPackage.Registry.INSTANCE.getEPackage(Aadl2Package.eNS_URI);
		ChangePackage theChangePackage = (ChangePackage)EPackage.Registry.INSTANCE.getEPackage(ChangePackage.eNS_URI);

		// Create type parameters

		// Set bounds for type parameters

		// Add supertypes to classes

		// Initialize classes, features, and operations; add parameters
		initEClass(aaxl2AaxlTraceSpecEClass, Aaxl2AaxlTraceSpec.class, "Aaxl2AaxlTraceSpec", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAaxl2AaxlTraceSpec_RightSystem(), theInstancePackage.getSystemInstance(), null, "rightSystem", null, 0, 1, Aaxl2AaxlTraceSpec.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAaxl2AaxlTraceSpec_LeftSystem(), theInstancePackage.getSystemInstance(), null, "leftSystem", null, 0, 1, Aaxl2AaxlTraceSpec.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAaxl2AaxlTraceSpec_Traces(), this.getAaxl2AaxlTrace(), null, "traces", null, 0, -1, Aaxl2AaxlTraceSpec.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		initEClass(aaxl2AaxlTraceEClass, Aaxl2AaxlTrace.class, "Aaxl2AaxlTrace", !IS_ABSTRACT, !IS_INTERFACE, IS_GENERATED_INSTANCE_CLASS);
		initEReference(getAaxl2AaxlTrace_RightInstance(), theAadl2Package.getElement(), null, "rightInstance", null, 0, -1, Aaxl2AaxlTrace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAaxl2AaxlTrace_LeftInstance(), theAadl2Package.getElement(), null, "leftInstance", null, 0, -1, Aaxl2AaxlTrace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAaxl2AaxlTrace_ObjectsToAttach(), theAadl2Package.getElement(), null, "objectsToAttach", null, 0, -1, Aaxl2AaxlTrace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAaxl2AaxlTrace_ObjectChanges(), theChangePackage.getEObjectToChangesMapEntry(), null, "objectChanges", null, 0, -1, Aaxl2AaxlTrace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, IS_COMPOSITE, !IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);
		initEReference(getAaxl2AaxlTrace_ObjectsToDetach(), theAadl2Package.getElement(), null, "objectsToDetach", null, 0, -1, Aaxl2AaxlTrace.class, !IS_TRANSIENT, !IS_VOLATILE, IS_CHANGEABLE, !IS_COMPOSITE, IS_RESOLVE_PROXIES, !IS_UNSETTABLE, IS_UNIQUE, !IS_DERIVED, IS_ORDERED);

		// Create resource
		createResource(eNS_URI);
	}

} //Aaxl2AaxlPackageImpl
