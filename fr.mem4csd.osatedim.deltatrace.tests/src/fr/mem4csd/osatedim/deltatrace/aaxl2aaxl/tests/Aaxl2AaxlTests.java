/**
 */
package fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>aaxl2aaxl</b></em>' package.
 * <!-- end-user-doc -->
 * @generated
 */
public class Aaxl2AaxlTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new Aaxl2AaxlTests("aaxl2aaxl Tests");
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Aaxl2AaxlTests(String name) {
		super(name);
	}

} //Aaxl2AaxlTests
