/**
 */
package fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.tests;

import junit.framework.Test;
import junit.framework.TestSuite;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test suite for the '<em><b>Aaxl2Aaxl</b></em>' model.
 * <!-- end-user-doc -->
 * @generated
 */
public class Aaxl2AaxlAllTests extends TestSuite {

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(suite());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static Test suite() {
		TestSuite suite = new Aaxl2AaxlAllTests("Aaxl2Aaxl Tests");
		return suite;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Aaxl2AaxlAllTests(String name) {
		super(name);
	}

} //Aaxl2AaxlAllTests
