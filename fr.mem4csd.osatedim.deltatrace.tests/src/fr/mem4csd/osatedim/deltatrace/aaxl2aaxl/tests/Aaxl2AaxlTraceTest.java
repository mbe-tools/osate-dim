/**
 */
package fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.tests;

import fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlFactory;
import fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace;

import junit.framework.TestCase;

import junit.textui.TestRunner;

/**
 * <!-- begin-user-doc -->
 * A test case for the model object '<em><b>Trace</b></em>'.
 * <!-- end-user-doc -->
 * @generated
 */
public class Aaxl2AaxlTraceTest extends TestCase {

	/**
	 * The fixture for this Trace test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Aaxl2AaxlTrace fixture = null;

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public static void main(String[] args) {
		TestRunner.run(Aaxl2AaxlTraceTest.class);
	}

	/**
	 * Constructs a new Trace test case with the given name.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	public Aaxl2AaxlTraceTest(String name) {
		super(name);
	}

	/**
	 * Sets the fixture for this Trace test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected void setFixture(Aaxl2AaxlTrace fixture) {
		this.fixture = fixture;
	}

	/**
	 * Returns the fixture for this Trace test case.
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @generated
	 */
	protected Aaxl2AaxlTrace getFixture() {
		return fixture;
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#setUp()
	 * @generated
	 */
	@Override
	protected void setUp() throws Exception {
		setFixture(Aaxl2AaxlFactory.eINSTANCE.createAaxl2AaxlTrace());
	}

	/**
	 * <!-- begin-user-doc -->
	 * <!-- end-user-doc -->
	 * @see junit.framework.TestCase#tearDown()
	 * @generated
	 */
	@Override
	protected void tearDown() throws Exception {
		setFixture(null);
	}

} //Aaxl2AaxlTraceTest
