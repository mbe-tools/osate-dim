package fr.mem4csd.osatedim.tests;

import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.osate.aadl2.Aadl2Package;
import org.osate.aadl2.AadlPackage;
import org.osate.aadl2.BasicPropertyAssociation;
import org.osate.aadl2.BooleanLiteral;
import org.osate.aadl2.ClassifierValue;
import org.osate.aadl2.ComponentCategory;
import org.osate.aadl2.DirectionType;
import org.osate.aadl2.EnumerationType;
import org.osate.aadl2.IntegerLiteral;
import org.osate.aadl2.ListValue;
import org.osate.aadl2.ModalPropertyValue;
import org.osate.aadl2.NamedElement;
import org.osate.aadl2.NamedValue;
import org.osate.aadl2.Property;
import org.osate.aadl2.PropertySet;
import org.osate.aadl2.RecordType;
import org.osate.aadl2.RecordValue;
import org.osate.aadl2.StringLiteral;
import org.osate.aadl2.UnitLiteral;
import org.osate.aadl2.UnitsType;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.ConnectionInstance;
import org.osate.aadl2.instance.ConnectionKind;
import org.osate.aadl2.instance.FeatureCategory;
import org.osate.aadl2.instance.FeatureInstance;
import org.osate.aadl2.instance.InstancePackage;
import org.osate.aadl2.instance.InstanceReferenceValue;
import org.osate.aadl2.instance.PropertyAssociationInstance;
import org.osate.aadl2.instance.SystemInstance;
import org.osate.aadl2.instance.SystemOperationMode;
import org.osate.aadl2.modelsupport.FileNameConstants;
import fr.mem4csd.osatedim.utils.PropertySetUtils;
import fr.tpt.mem4csd.utils.osate.standalone.OsateStandaloneSetup;

public class InplaceModifications{
	
	public InplaceModifications(SystemInstance topSystemInst) {
		resSet = topSystemInst.eResource().getResourceSet();
		aadlProjectResource = (PropertySet) resSet.getResource(OsateStandaloneSetup.PREDECLARED_POPERTY_SETS_URI.appendSegment( "AADL_Project" ).appendFileExtension( FileNameConstants.SOURCE_FILE_EXT ), true ).getContents().get(0);
		deploymentResource = (PropertySet) resSet.getResource(OsateStandaloneSetup.PREDECLARED_POPERTY_SETS_URI.appendSegment( "Deployment_Properties" ).appendFileExtension( FileNameConstants.SOURCE_FILE_EXT ), true ).getContents().get(0);
		dataModelResource = (PropertySet) resSet.getResource(OsateStandaloneSetup.ANNEX_CONTRIBUTION_PROPERTY_SETS_URI.appendSegment( "Data_Model" ).appendFileExtension( FileNameConstants.SOURCE_FILE_EXT ), true ).getContents().get(0);
		memoryResource = (PropertySet) resSet.getResource(OsateStandaloneSetup.PREDECLARED_POPERTY_SETS_URI.appendSegment( "Memory_Properties" ).appendFileExtension( FileNameConstants.SOURCE_FILE_EXT ), true ).getContents().get(0);
		ramsesResource = (PropertySet) resSet.getResource(topSystemInst.eResource().getURI().trimSegments(4).appendSegment( "RAMSES" ).appendFileExtension( FileNameConstants.SOURCE_FILE_EXT ), true ).getContents().get(0);
		aadlRuntimeResource = (AadlPackage) resSet.getResource(topSystemInst.eResource().getURI().trimSegments(4).appendSegment( "AADL_Runtime" ).appendFileExtension( FileNameConstants.SOURCE_FILE_EXT ), true ).getContents().get(0);
		programmingResource = (PropertySet) resSet.getResource(OsateStandaloneSetup.PREDECLARED_POPERTY_SETS_URI.appendSegment( "Programming_Properties" ).appendFileExtension( FileNameConstants.SOURCE_FILE_EXT ), true ).getContents().get(0);
		baseTypesResource = (AadlPackage) (AadlPackage) resSet.getResource(OsateStandaloneSetup.ANNEX_CONTRIBUTION_PACKAGES_URI.appendSegment( "Base_Types" ).appendFileExtension( FileNameConstants.SOURCE_FILE_EXT ), true ).getContents().get(0);
		threadResource = (PropertySet) resSet.getResource(OsateStandaloneSetup.PREDECLARED_POPERTY_SETS_URI.appendSegment( "Thread_Properties" ).appendFileExtension( FileNameConstants.SOURCE_FILE_EXT ), true ).getContents().get(0);
		actualMemoryBindingProperty = PropertySetUtils.getPropertyWithName(deploymentResource, "Actual_Memory_Binding");
		initialValueProperty = PropertySetUtils.getPropertyWithName(dataModelResource, "Initial_Value");
		dataRepresentationProperty = PropertySetUtils.getPropertyWithName(dataModelResource, "Data_Representation");
		elementNamesProperty = PropertySetUtils.getPropertyWithName(dataModelResource, "Element_Names");
		baseTypeProperty = PropertySetUtils.getPropertyWithName(dataModelResource, "Base_Type");
		sourceNameProperty = PropertySetUtils.getPropertyWithName(programmingResource, "Source_Name");
		dimensionProperty = PropertySetUtils.getPropertyWithName(dataModelResource, "Dimension");
		numberRepresentationProperty = PropertySetUtils.getPropertyWithName(dataModelResource, "Number_Representation");
		dataSizeProperty = PropertySetUtils.getPropertyWithName(memoryResource, "Data_Size");
		priorityProperty = PropertySetUtils.getPropertyWithName(threadResource, "Priority");
		executionSlotsProperty = PropertySetUtils.getPropertyWithName(ramsesResource, "Execution_Slots");
		isProcessorProperty = PropertySetUtils.getPropertyWithName(ramsesResource, "Is_Processor");
		
	}
	
	private final ResourceSet resSet;
	private final PropertySet aadlProjectResource;
	private final PropertySet deploymentResource;
	private final PropertySet dataModelResource;
	private final PropertySet memoryResource;
	private final AadlPackage aadlRuntimeResource;
	private final PropertySet ramsesResource;
	private final PropertySet programmingResource;
	private final AadlPackage baseTypesResource;
	private final PropertySet threadResource;
	private final Property actualMemoryBindingProperty;
	private final Property initialValueProperty;
	private final Property dataRepresentationProperty;
	private final Property elementNamesProperty;
	private final Property baseTypeProperty;
	private final Property sourceNameProperty;
	private final Property dimensionProperty;
	private final Property numberRepresentationProperty;
	private final Property dataSizeProperty;
	private final Property priorityProperty;
	private final Property executionSlotsProperty;
	private final Property isProcessorProperty;
	
	protected ComponentInstance createDataSubcomponent(ComponentInstance proc, String name, ComponentInstance mem) {
		ComponentInstance data = proc.createComponentInstance();
		data.setName(name);
		data.setCategory(ComponentCategory.DATA);
		addActualMemoryBindingProperty(data, mem);
		return data;
	}
	
	protected void addActualMemoryBindingProperty(ComponentInstance data, ComponentInstance mem) {
		PropertyAssociationInstance prop1 = (PropertyAssociationInstance) data.createOwnedPropertyAssociation();
		prop1.setProperty(actualMemoryBindingProperty);
		((InstanceReferenceValue) ((ListValue) prop1.createOwnedValue().createOwnedValue(Aadl2Package.eINSTANCE.getListValue())).createOwnedListElement(InstancePackage.eINSTANCE.getInstanceReferenceValue())).setReferencedInstanceObject(mem);
	}
	
	private void addInitialValueProperty(ComponentInstance data, String value) {
		PropertyAssociationInstance prop = (PropertyAssociationInstance) data.createOwnedPropertyAssociation();
		prop.setProperty(initialValueProperty);
		((StringLiteral) ((ListValue) prop.createOwnedValue().createOwnedValue(Aadl2Package.eINSTANCE.getListValue())).createOwnedListElement(Aadl2Package.eINSTANCE.getStringLiteral())).setValue(value);
	}
	
	private void addDataRepresentationProperty(ComponentInstance data, String value) {
		PropertyAssociationInstance prop = (PropertyAssociationInstance) data.createOwnedPropertyAssociation();
		prop.setProperty(dataRepresentationProperty);
		((NamedValue) prop.createOwnedValue().createOwnedValue(Aadl2Package.eINSTANCE.getNamedValue())).setNamedValue(((EnumerationType) dataRepresentationProperty.getPropertyType()).findLiteral(value));
	}
	
	private void addElementNamesProperty(NamedElement element) {
		PropertyAssociationInstance prop = (PropertyAssociationInstance) element.createOwnedPropertyAssociation();
		prop.setProperty(elementNamesProperty);
		ListValue propval = (ListValue) prop.createOwnedValue().createOwnedValue(Aadl2Package.eINSTANCE.getListValue()); 
		((StringLiteral) propval.createOwnedListElement(Aadl2Package.eINSTANCE.getStringLiteral())).setValue("p");
		((StringLiteral) propval.createOwnedListElement(Aadl2Package.eINSTANCE.getStringLiteral())).setValue("config");
	}
	
	private void addBaseTypeProperty(ComponentInstance data, String baseValue) {
		PropertyAssociationInstance prop = (PropertyAssociationInstance) data.createOwnedPropertyAssociation();
		prop.setProperty(baseTypeProperty);
		if (baseValue == "InputProcessorPortAddr") {
			((ClassifierValue)((ListValue) prop.createOwnedValue().createOwnedValue(Aadl2Package.eINSTANCE.getListValue())).createOwnedListElement(Aadl2Package.eINSTANCE.getClassifierValue())).setClassifier(aadlRuntimeResource.getOwnedPublicSection().getOwnedClassifiers().get(19));
		} else if (baseValue == "ThreadConfigAddr") {
			((ClassifierValue)((ListValue) prop.createOwnedValue().createOwnedValue(Aadl2Package.eINSTANCE.getListValue())).createOwnedListElement(Aadl2Package.eINSTANCE.getClassifierValue())).setClassifier(aadlRuntimeResource.getOwnedPublicSection().getOwnedClassifiers().get(41));
		} else if (baseValue == "Unsigned_16") {
			((ClassifierValue)((ListValue) prop.createOwnedValue().createOwnedValue(Aadl2Package.eINSTANCE.getListValue())).createOwnedListElement(Aadl2Package.eINSTANCE.getClassifierValue())).setClassifier(baseTypesResource.getOwnedPublicSection().getOwnedClassifiers().get(7));
		} else if (baseValue == "Integer_16") {
			((ClassifierValue)((ListValue) prop.createOwnedValue().createOwnedValue(Aadl2Package.eINSTANCE.getListValue())).createOwnedListElement(Aadl2Package.eINSTANCE.getClassifierValue())).setClassifier(baseTypesResource.getOwnedPublicSection().getOwnedClassifiers().get(3));
		} else if (baseValue == "PortConnectionAddr") {
			((ClassifierValue)((ListValue) prop.createOwnedValue().createOwnedValue(Aadl2Package.eINSTANCE.getListValue())).createOwnedListElement(Aadl2Package.eINSTANCE.getClassifierValue())).setClassifier(aadlRuntimeResource.getOwnedPublicSection().getOwnedClassifiers().get(7));
		} else if (baseValue == "DoubleBufferType") {
			((ClassifierValue)((ListValue) prop.createOwnedValue().createOwnedValue(Aadl2Package.eINSTANCE.getListValue())).createOwnedListElement(Aadl2Package.eINSTANCE.getClassifierValue())).setClassifier(aadlRuntimeResource.getOwnedPublicSection().getOwnedClassifiers().get(34));
		} else if (baseValue == "PortReferenceAddr") {
			((ClassifierValue)((ListValue) prop.createOwnedValue().createOwnedValue(Aadl2Package.eINSTANCE.getListValue())).createOwnedListElement(Aadl2Package.eINSTANCE.getClassifierValue())).setClassifier(aadlRuntimeResource.getOwnedPublicSection().getOwnedClassifiers().get(5));
		} else if (baseValue == "ThreadConfigAddrArray") {
			((ClassifierValue)((ListValue) prop.createOwnedValue().createOwnedValue(Aadl2Package.eINSTANCE.getListValue())).createOwnedListElement(Aadl2Package.eINSTANCE.getClassifierValue())).setClassifier(aadlRuntimeResource.getOwnedPublicSection().getOwnedClassifiers().get(42));
		}	
	}
	
	private void addSourceNameProperty(ComponentInstance data, String value) {
		PropertyAssociationInstance prop = (PropertyAssociationInstance) data.createOwnedPropertyAssociation();
		prop.setProperty(sourceNameProperty);
		((StringLiteral) prop.createOwnedValue().createOwnedValue(Aadl2Package.eINSTANCE.getStringLiteral())).setValue(value);
	}
	
	private void addDimensionProperty(ComponentInstance data, Integer value) {
		PropertyAssociationInstance prop = (PropertyAssociationInstance) data.createOwnedPropertyAssociation();
		prop.setProperty(dimensionProperty);
		IntegerLiteral propval = (IntegerLiteral)((ListValue) prop.createOwnedValue().createOwnedValue(Aadl2Package.eINSTANCE.getListValue())).createOwnedListElement(Aadl2Package.eINSTANCE.getIntegerLiteral());
		propval.setBase(0);
		propval.setValue(value);
	}
	
	private void addNumberRepresentationProperty(ComponentInstance data) {
		PropertyAssociationInstance prop = (PropertyAssociationInstance) data.createOwnedPropertyAssociation();
		prop.setProperty(numberRepresentationProperty);
		((NamedValue) prop.createOwnedValue().createOwnedValue(Aadl2Package.eINSTANCE.getNamedValue())).setNamedValue(((EnumerationType) numberRepresentationProperty.getPropertyType()).findLiteral("Signed"));
	}
	
	private void addDataSizeProperty(ComponentInstance data) {
		PropertyAssociationInstance prop = (PropertyAssociationInstance) data.createOwnedPropertyAssociation();
		prop.setProperty(dataSizeProperty);
		IntegerLiteral propval = ((IntegerLiteral) prop.createOwnedValue().createOwnedValue(Aadl2Package.eINSTANCE.getIntegerLiteral()));
		propval.setValue(2);
		propval.setUnit((UnitLiteral)((UnitsType) aadlProjectResource.getOwnedPropertyTypes().get(15)).findLiteral("Bytes"));
	}
	
	private void addPriorityProperty(FeatureInstance feat) {
		PropertyAssociationInstance prop = (PropertyAssociationInstance) feat.createOwnedPropertyAssociation();
		prop.setProperty(priorityProperty);
		IntegerLiteral propval = (IntegerLiteral) prop.createOwnedValue().createOwnedValue(Aadl2Package.eINSTANCE.getIntegerLiteral());
		propval.setBase(0);
		propval.setValue(5);
	}
	
	private void addDoubleBaseTypeProperty(ComponentInstance data) {
		PropertyAssociationInstance prop = (PropertyAssociationInstance) data.createOwnedPropertyAssociation();
		prop.setProperty(baseTypeProperty);
		ListValue propval = (ListValue) prop.createOwnedValue().createOwnedValue(Aadl2Package.eINSTANCE.getListValue());
		((ClassifierValue) propval.createOwnedListElement(Aadl2Package.eINSTANCE.getClassifierValue())).setClassifier(aadlRuntimeResource.getOwnedPublicSection().getOwnedClassifiers().get(5));
		((ClassifierValue) propval.createOwnedListElement(Aadl2Package.eINSTANCE.getClassifierValue())).setClassifier(aadlRuntimeResource.getOwnedPublicSection().getOwnedClassifiers().get(41));
	}
	
	private ComponentInstance addSixPropertyDataSubcomponent(ComponentInstance proc, String name, ComponentInstance mem, String initValue, String sourceName) {
		ComponentInstance data = createDataSubcomponent(proc, name, mem);
		addInitialValueProperty(data, initValue);
		addDataRepresentationProperty(data, "struct");
		addElementNamesProperty(data);
		addDoubleBaseTypeProperty(data);
		addSourceNameProperty(data, sourceName);
		return data;
	}
	
	private void addFivePropertyDataSubcomponentA(ComponentInstance proc, String name, ComponentInstance mem, String initValue) {
		ComponentInstance data = createDataSubcomponent(proc, name, mem);
		addInitialValueProperty(data, initValue);
		addDataSizeProperty(data);
		addDataRepresentationProperty(data, "Integer");
		addNumberRepresentationProperty(data);
	}
	
	private void addFivePropertyDataSubcomponentB(ComponentInstance proc, String name, ComponentInstance mem, String initValue, String baseValue, Integer dimension) {
		ComponentInstance data = createDataSubcomponent(proc, name, mem);
		addInitialValueProperty(data, initValue);
		addDataRepresentationProperty(data, "Array");
		addBaseTypeProperty(data, baseValue);
		addDimensionProperty(data, dimension);		
	}

	private void addFourPropertyDataSubcomponentA(ComponentInstance proc, String name, ComponentInstance mem) {
		ComponentInstance data = createDataSubcomponent(proc, name, mem);
		addDataSizeProperty(data);
		addDataRepresentationProperty(data, "Integer");
		addNumberRepresentationProperty(data);
	}
	
	private void addFourPropertyDataSubcomponentB(ComponentInstance proc, String name, ComponentInstance mem, String baseValue) {
		ComponentInstance data = createDataSubcomponent(proc, name, mem);
		addDataRepresentationProperty(data, "Array");
		addBaseTypeProperty(data, baseValue);
		addDimensionProperty(data, 1);			
	}
	
	private void addTwoPropertyDataSubcomponent(ComponentInstance proc, String name, ComponentInstance mem, String initValue) {
		ComponentInstance data = createDataSubcomponent(proc, name, mem);
		addInitialValueProperty(data, initValue);
	}
	
	private void modifyRAMSESFeature(FeatureInstance feat, ComponentInstance data) {
		feat.setCategory(FeatureCategory.DATA_ACCESS);
		feat.setDirection(DirectionType.IN);
		addPriorityProperty(feat);
		addElementNamesProperty(feat);
		((NamedValue) feat.getOwnedPropertyAssociations().get(3).getOwnedValues().get(0).getOwnedValue()).setNamedValue(((EnumerationType) dataRepresentationProperty.getPropertyType()).findLiteral("Struct"));
		feat.setType(data);
	}

	protected void modifyRAMSES(SystemInstance topSystemInst) {
		topSystemInst.setName("refined_model_impl_Instance");
		ComponentInstance proc = topSystemInst.getAllComponentInstances(ComponentCategory.PROCESS).get(0);
		ComponentInstance mem = topSystemInst.getAllComponentInstances(ComponentCategory.MEMORY).get(0);
		ComponentInstance data1 = addSixPropertyDataSubcomponent(proc, "the_sender_context", mem, "{&CUTsender_p_port_ref, &the_sender_config}", "__sampled_communications_linux__sender_context");
		ComponentInstance data2 = addSixPropertyDataSubcomponent(proc, "the_receiver_context", mem, "{&CUTreceiver_p_port_ref, &the_receiver_config}", "__sampled_communications_linux__receiver_context");
		addTwoPropertyDataSubcomponent(proc, "CUTsender_p_port_ref", mem, "{DATA, OUT, THREAD, &the_sender_config,&CUTsender_p_output_port, sizeof(Base_Types__Integer_16)}");
		addTwoPropertyDataSubcomponent(proc, "CUTsender_p_outport_port", mem, "{&CUTsender_p_port_variable, 0, 1, 1,CUTsender_p_dest_index_by_mode,CUTsender_p_cnxs_reference}");
		addFivePropertyDataSubcomponentB(proc, "CUTsender_p_cnxs_reference", mem, "{&intraprocess_cnx_CUTsender_p_TOCUTreceiver_p_IN_ALL}", "PortConnectionAddr", 1);
		addFourPropertyDataSubcomponentA(proc, "CUTsender_p_port_variable", mem);
		addFivePropertyDataSubcomponentB(proc, "CUTsender_p_dest_index_by_mode", mem, "{1,1}", "Integer_16", 2);
		addTwoPropertyDataSubcomponent(proc, "intraprocess_cnx_CUTsender_p_TOCUTreceiver_p_IN_ALL", mem, "{&CUTsender_p_port_ref,&CUTreceiver_p_port_ref,&intraprocess_cnx_protocol_CUTsender_p_TO_CUTreceiver_p_IN_ALL}");
		addTwoPropertyDataSubcomponent(proc, "intraprocess_cnx_protocol_CUTsender_p_TO_CUTreceiver_p_IN_ALL", mem, "{SAMPLED,NULL}");
		addTwoPropertyDataSubcomponent(proc, "CUTreceiver_p_port_ref", mem, "{DATA, IN, THREAD,&the_receiver_config,&CUTreceiver_p_in_port, sizeof(Base_Types__Integer_16)}");
		addFivePropertyDataSubcomponentB(proc, "CUTreceiver_p_src_index_by_mode", mem, "{0}", "Integer_16", 1);
		addFivePropertyDataSubcomponentB(proc, "CUTreceiver_p_data_rw_per_mode", mem, "{{&CUTsender_p_CUTreceiver_p_value_read, &CUTsender_p_CUTreceiver_p_value_write}}", "DoubleBufferType", 1);
		addTwoPropertyDataSubcomponent(proc, "CUTreceiver_p_in_port", mem, "{&the_receiver_globalQueue,&CUTreceiver_p_port_ref,&CUTreceiver_p_data_port,1,CUTreceiver_p_cnxs_reference,DEFAULT,0,DISPATCH_TIME}");
		addFivePropertyDataSubcomponentB(proc, "CUTreceiver_p_cnxs_reference", mem, "{&intraprocess_cnx_CUTsender_p_TOCUTreceiver_p_IN_ALL}", "PortConnectionAddr", 1);
		addTwoPropertyDataSubcomponent(proc, "CUTreceiver_p_data_port", mem, "{1, CUTreceiver_p_src_index_by_mode, CUTreceiver_p_data_rw_per_mode}");
		addFourPropertyDataSubcomponentA(proc, "CUTsender_p_CUTreceiver_p_value_read", mem);
		addFourPropertyDataSubcomponentA(proc, "CUTsender_p_CUTreceiver_p_value_write", mem);
		addTwoPropertyDataSubcomponent(proc, "the_sender_config", mem, "{PERIODIC, NULL, &the_sender_config_periodic, 2000000000, 0,5, 2000000000, 1,{0},&the_sender_tid, &the_sender_mode_event, 1, the_sender_output_ports_reference,0, NULL,NULL,RUNTIME_OK,0xFF,STOPPED_THREAD_STATE,0, NULL,NULL,NULL,NULL,NULL,NULL,NULL,&the_sender_context,0,0,&the_sender_watchdog}");
		createDataSubcomponent(proc, "the_sender_tid", mem);
		createDataSubcomponent(proc, "the_sender_mode_event", mem);
		createDataSubcomponent(proc, "the_sender_config_periodic", mem);
		createDataSubcomponent(proc, "the_sender_watchdog", mem);
		addFivePropertyDataSubcomponentB(proc, "the_sender_output_ports_reference", mem, "{&CUTsender_p_port_ref}", "PortReferenceAddr", 1);
		addTwoPropertyDataSubcomponent(proc, "the_receiver_config", mem, "{PERIODIC, &the_receiver_globalQueue, &the_receiver_config_periodic, 1000000000, 0,10, 1000000000, 1,{0},&the_receiver_tid, &the_receiver_mode_event, 0, NULL,1, the_receiver_input_ports_reference,NULL,RUNTIME_OK,0xFF,STOPPED_THREAD_STATE,0, NULL,NULL,NULL,NULL,NULL,NULL,NULL,&the_receiver_context,0,0,&the_receiver_watchdog}");
		createDataSubcomponent(proc, "the_receiver_tid", mem);
		createDataSubcomponent(proc, "the_receiver_mode_event", mem);
		createDataSubcomponent(proc, "the_receiver_config_periodic", mem);
		createDataSubcomponent(proc, "the_receiver_globalQueue_evt", mem);
		createDataSubcomponent(proc, "the_receiver_watchdog", mem);
		addFivePropertyDataSubcomponentB(proc, "the_receiver_input_ports_reference", mem, "{&CUTreceiver_p_port_ref}", "PortReferenceAddr", 1);
		addTwoPropertyDataSubcomponent(proc, "the_receiver_globalQueue", mem, "{&the_receiver_config,0, 0, 0, 1, the_receiver_input_port_reference_array,&the_receiver_globalQueue_evt}");
		addFivePropertyDataSubcomponentB(proc, "the_receiver_input_port_reference_array", mem, "{&CUTreceiver_p_port_ref}", "PortReferenceAddr", 1);
		addTwoPropertyDataSubcomponent(proc, "this_process", mem, "{{&the_sender_config,&the_receiver_config},1,{0},2000000000}");
		addFivePropertyDataSubcomponentA(proc, "thread_number_in_all_modes", mem, "2");
		addFivePropertyDataSubcomponentB(proc, "thread_configs_in_all_modes", mem, "{&the_sender_config,&the_receiver_config}", "ThreadConfigAddr", 2);
		addFivePropertyDataSubcomponentB(proc, "thread_number_per_mode", mem, "{}", "Unsigned_16", 0);
		addFivePropertyDataSubcomponentB(proc, "thread_configs_per_mode", mem, "{}", "ThreadConfigAddrArray", 0);
		addFivePropertyDataSubcomponentB(proc, "port_to_process", mem, "{}", "Integer_16", 0);
		addFivePropertyDataSubcomponentB(proc, "process_to_node", mem, "{0}", "Integer_16", 1);
		addFivePropertyDataSubcomponentB(proc, "nb_dst_per_port", mem, "{}", "Integer_16", 0);
		addFourPropertyDataSubcomponentB(proc, "global_to_local", mem, "InputProcessorPortAddr");
		
		FeatureInstance feat1 = proc.getAllComponentInstances().get(1).getAllFeatureInstances().get(0);
		feat1.setName("the_sender_context");
		FeatureInstance feat2 = proc.getAllComponentInstances().get(2).getAllFeatureInstances().get(0);
		feat2.setName("the_receiver_context");
		modifyRAMSESFeature(feat1, data1);
		modifyRAMSESFeature(feat2, data2);
		
		addRAMSESConnection(proc, "the_sender.the_sender_context -> the_sender_context", data1, feat1);
		addRAMSESConnection(proc, "the_receiver.the_receiver_context -> the_receiver_context", data2, feat2);
		EcoreUtil.remove(proc.getConnectionInstances().get(0));
	}
	
	private void addExecutionSlotModalProperty(PropertyAssociationInstance prop, Integer start, Integer stop, SystemOperationMode som, ComponentInstance core) {
		ModalPropertyValue modprop = prop.createOwnedValue();
		modprop.getInModes().add(som);
		RecordValue propval = (RecordValue) ((ListValue) modprop.createOwnedValue(Aadl2Package.eINSTANCE.getListValue())).createOwnedListElement(Aadl2Package.eINSTANCE.getRecordValue());
		BasicPropertyAssociation unitprop = propval.createOwnedFieldValue();
		unitprop.setProperty(((RecordType) ramsesResource.getOwnedPropertyTypes().get(0)).getOwnedFields().get(0));	
		((InstanceReferenceValue) unitprop.createOwnedValue(InstancePackage.eINSTANCE.getInstanceReferenceValue())).setReferencedInstanceObject(core);
		
		BasicPropertyAssociation startprop = propval.createOwnedFieldValue();
		startprop.setProperty(((RecordType) ramsesResource.getOwnedPropertyTypes().get(0)).getOwnedFields().get(1));
		IntegerLiteral startint = (IntegerLiteral) startprop.createOwnedValue(Aadl2Package.eINSTANCE.getIntegerLiteral());
		startint.setValue(start);
		startint.setBase(0);
		startint.setUnit(((UnitsType) aadlProjectResource.getOwnedPropertyTypes().get(16)).findLiteral("ms"));
		
		BasicPropertyAssociation endprop = propval.createOwnedFieldValue();
		endprop.setProperty(((RecordType) ramsesResource.getOwnedPropertyTypes().get(0)).getOwnedFields().get(2));
		IntegerLiteral endint = (IntegerLiteral) endprop.createOwnedValue(Aadl2Package.eINSTANCE.getIntegerLiteral());
		endint.setValue(stop);
		endint.setBase(0);
		endint.setUnit(((UnitsType) aadlProjectResource.getOwnedPropertyTypes().get(16)).findLiteral("ms"));
	}
	
	private void addExecutionSlotsProperty(ComponentInstance thread, Integer val1, Integer val2, SystemOperationMode som1, ComponentInstance core) {
		PropertyAssociationInstance prop = (PropertyAssociationInstance) thread.createOwnedPropertyAssociation();
		prop.setProperty(executionSlotsProperty);
		addExecutionSlotModalProperty(prop, val1, val2, som1, core);
	}
	
	private void addTwoExecutionSlotsProperty(ComponentInstance thread, Integer val1, Integer val2, Integer val3, Integer val4, SystemOperationMode som1, SystemOperationMode som2, ComponentInstance core) {
		PropertyAssociationInstance prop = (PropertyAssociationInstance) thread.createOwnedPropertyAssociation();
		prop.setProperty(executionSlotsProperty);
		addExecutionSlotModalProperty(prop, val1, val2, som1, core);
		addExecutionSlotModalProperty(prop, val3, val4, som2, core);
	}
	
	protected void addRAMSESConnection(ComponentInstance proc, String name, ComponentInstance data1, FeatureInstance feat1) {
		ConnectionInstance conn1 = proc.createConnectionInstance();
		conn1.setName(name);
		conn1.setBidirectional(false);
		conn1.setComplete(true);
		conn1.setKind(ConnectionKind.ACCESS_CONNECTION);
		conn1.setSource(data1);
		conn1.setDestination(feat1);
	}
	
	protected void modifyMCDAG(SystemInstance topSystemInst) {
		//Add Is_Processor property to main_impl_Instance > cpu
		ComponentInstance cpu = topSystemInst.getAllComponentInstances(ComponentCategory.SYSTEM).get(1);
		PropertyAssociationInstance prop1 = (PropertyAssociationInstance) cpu.createOwnedPropertyAssociation();
		prop1.setProperty(isProcessorProperty);
		((BooleanLiteral)((ModalPropertyValue) prop1.createOwnedValue()).createOwnedValue(Aadl2Package.eINSTANCE.getBooleanLiteral())).setValue(true);

		SystemOperationMode som1 = null, som2 = null;
		for (SystemOperationMode som : topSystemInst.getSystemOperationModes()) {
			if (som.getName().endsWith("1")) {
				som1 = som;
			} else {
				som2 = som;
			}
		}
		ComponentInstance core1 = null, core2 = null;
		for (ComponentInstance core : cpu.getAllComponentInstances(ComponentCategory.PROCESSOR)) {
			if (core.getName().endsWith("1")) {
				core1 = core;
			} else {
				core2 = core;
			}
		}
		
		for (ComponentInstance thread : topSystemInst.getAllComponentInstances(ComponentCategory.PROCESS).get(0).getAllComponentInstances(ComponentCategory.THREAD_GROUP).get(0).getAllComponentInstances(ComponentCategory.THREAD)) {
			if (thread.getName().contentEquals("Avoid")) {
				addTwoExecutionSlotsProperty(thread, 0, 300, 0, 300, som1, som2, core1);
			} else if (thread.getName().contentEquals("Nav")) {
				addTwoExecutionSlotsProperty(thread, 300, 900, 300, 1100, som1, som2, core1);
			} else if (thread.getName().contentEquals("Stab")) {
				addTwoExecutionSlotsProperty(thread, 900, 1100, 1100, 1600, som1, som2, core1);
			} else if (thread.getName().contentEquals("Log")) {
				addExecutionSlotsProperty(thread, 1100, 1300, som1, core1);
			} else if (thread.getName().contentEquals("Com")) {
				addExecutionSlotsProperty(thread, 1300, 1600, som1, core1);
			} else if (thread.getName().contentEquals("Video")) {
				addExecutionSlotsProperty(thread, 0, 600, som1, core2);
			} else if (thread.getName().contentEquals("GPS")) {
				addExecutionSlotsProperty(thread, 600, 800, som1, core2);
			} else if (thread.getName().contentEquals("Rec")) {
				addExecutionSlotsProperty(thread, 800, 1000, som1, core2);
			}
		}	
	}
}
