package fr.mem4csd.osatedim.tests;

import static org.junit.Assert.fail;

import java.io.File;
import java.io.IOException;
import java.nio.file.Paths;
import org.apache.commons.io.FileUtils;
import org.eclipse.core.runtime.NullProgressMonitor;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.viatra.query.patternlanguage.emf.EMFPatternLanguageStandaloneSetup;
import org.osate.aadl2.ComponentImplementation;
import org.osate.aadl2.PropertySet;
import org.osate.aadl2.instance.SystemInstance;
import org.osate.aadl2.instantiation.InstantiateModel;
import org.osate.aadl2.modelsupport.FileNameConstants;
import org.osate.aadl2.modelsupport.errorreporting.AnalysisErrorReporterManager;
import fr.tpt.mem4csd.utils.compare.IComparisonReport;
import fr.tpt.mem4csd.utils.compare.aadl.DefaultComparatorAADL;
import fr.tpt.mem4csd.utils.compare.emf.IComparatorEMF;
import fr.tpt.mem4csd.utils.osate.standalone.OsateStandaloneSetup;
import fr.tpt.mem4csd.utils.osate.standalone.StandaloneXtextResourceSet;

public class TestAbstract {
	
	protected static String refDirectory = "";
	protected final static URI BASE_URI = URI.createFileURI(new File("cases").getAbsolutePath());
	protected final static URI REF_URI = BASE_URI.trimSegments(2).appendSegment("fr.mem4csd.osatedim.tests.ref");
	protected final static URI ORACLE_URI = BASE_URI.trimSegments(2).appendSegment("fr.mem4csd.osatedim.tests.oracle");
	protected static ResourceSet resourceSet = new OsateStandaloneSetup(BASE_URI.trimSegments(1)).createResourceSet();
	protected static Resource aaxlResource;
	protected static Resource aadlResource;
	protected static SystemInstance topSystemInst;
	
	protected TestAbstract() {
		new EMFPatternLanguageStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
	
	protected static URI createInstanceURI(final String modelName) {
		return URI.createFileURI("cases/"+refDirectory+"/"+FileNameConstants.AADL_INSTANCES_DIR+"/"+modelName).resolve(BASE_URI).appendFileExtension(FileNameConstants.INSTANCE_FILE_EXT);
	}
	
	protected static URI createTestInstanceURI(final String modelName) {
		return createInstanceURI(modelName+"_test");
	}
	
	protected static void prepareTestResources(String inputModelName) throws IOException {
		final URI instanceModelUri = createInstanceURI(inputModelName);
		String basePath = URI.createFileURI("cases/"+refDirectory).resolve(BASE_URI).toFileString();
		FileUtils.deleteDirectory(new File(basePath));
		FileUtils.copyDirectory(Paths.get(REF_URI.appendSegments(refDirectory.split("/",3)).toFileString()).toFile(), Paths.get(basePath).toFile(), true);
		aaxlResource = resourceSet.getResource(instanceModelUri, true);
		topSystemInst = (SystemInstance) aaxlResource.getContents().get(0);
	}
	
	protected void doDeclarativeComparisonTest() throws IOException {
		Resource aadlTestResource = resourceSet.getResource(ORACLE_URI.appendSegments(refDirectory.split("/")).appendSegment(aadlResource.getURI().lastSegment().toString()), true); 
		compareResources(aadlResource,aadlTestResource);
	}
	
	protected void doInstanceComparisonTest(String inputModelName) throws Exception {
		Resource aaxlTestResource = instantiateWithTestURI(topSystemInst.getComponentImplementation(), inputModelName);
		aaxlTestResource.save(null);
		compareResources(aaxlResource, aaxlTestResource);
	}
	
	protected static Resource instantiateWithTestURI(ComponentImplementation ci, String inputModelName) throws Exception {
		URI instanceURI = createTestInstanceURI(inputModelName);
		Resource aaxlTestResource = resourceSet.createResource(instanceURI);
		final InstantiateModel instantiateModel = new InstantiateModel(new NullProgressMonitor(), AnalysisErrorReporterManager.NULL_ERROR_MANANGER);
		instantiateModel.createSystemInstanceInt(ci, aaxlTestResource, false);
		return aaxlTestResource;
	}
	
	protected void compareResources(Resource leftRes, Resource rightRes) 
	throws IOException {
		IComparatorEMF comparator = new DefaultComparatorAADL(aaxlResource.getResourceSet(), false);
		IComparisonReport report = comparator.compare(leftRes, rightRes);
		if (report.containsDiff()) {
			report.print();
			
			fail("DIM: Differences found");
		}
	}
	
	public static PropertySet getStandaloneDIMPropertySet(ResourceSet resSet) {
		URI dimPropertySetUri = URI.createURI( StandaloneXtextResourceSet.SCHEME + "osate_dim/DIM_Properties.aadl" ).appendFragment("DIM_Properties");
		return (PropertySet) resSet.getEObject(dimPropertySetUri, true);
	}
}