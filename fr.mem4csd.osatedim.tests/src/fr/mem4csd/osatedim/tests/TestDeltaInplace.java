package fr.mem4csd.osatedim.tests;

import java.io.IOException;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.emf.EMFScope;
import org.junit.Test;
import org.osate.aadl2.ComponentCategory;
import org.osate.aadl2.PropertySet;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.SystemInstance;
import fr.mem4csd.osatedim.preference.DIMPreferences;
import fr.mem4csd.osatedim.utils.DIMStandaloneLogger;
import fr.mem4csd.osatedim.viatra.transformations.DIMTransformationDeltaInplace;

public class TestDeltaInplace extends TestAbstract {
	
	public TestDeltaInplace() {
		refDirectory += "inplace/";
	}
	
	private InplaceModifications modifications;
	private InplaceModificationsWithUtils modificationsWithUtils;
	
	protected DIMTransformationDeltaInplace initiateDIMTransformationDeltaInplace(String inputModelName) throws IOException {
		prepareTestDeltaInplaceResources(inputModelName);
		final ViatraQueryEngine engine = ViatraQueryEngine.on(new EMFScope(aaxlResource.getResourceSet()));
		final PropertySet DIMPropertySet = getStandaloneDIMPropertySet(aaxlResource.getResourceSet());
		final DIMTransformationDeltaInplace transformation = new DIMTransformationDeltaInplace((SystemInstance) aaxlResource.getContents().get(0), engine, new DIMPreferences(false, false, false, false, false), DIMPropertySet, new DIMStandaloneLogger());
		transformation.execute();
		return transformation;
	}
	
	private void prepareTestDeltaInplaceResources(String inputModelName) throws IOException {
		prepareTestResources(inputModelName);
		aadlResource = resourceSet.getResource(((SystemInstance) aaxlResource.getContents().get(0)).getComponentImplementation().eResource().getURI(), true);
		modifications = new InplaceModifications((SystemInstance) aaxlResource.getContents().get(0));
		modificationsWithUtils = new InplaceModificationsWithUtils((SystemInstance) aaxlResource.getContents().get(0));
	}
	
	@Test
	public void testMCDAG() throws Exception {
		refDirectory += "mc-dag";
		DIMTransformationDeltaInplace transformation = initiateDIMTransformationDeltaInplace("declarative_main_impl_Instance");
		modifications.modifyMCDAG(transformation.getTopSystemInst());
		transformation.dispose();
		doDeclarativeComparisonTest();
//		doInstanceComparisonTest("declarative_main_impl_Instance");
	}
	
	@Test
	public void testRAMSES() throws Exception {
		refDirectory += "ramses-linux/";
		DIMTransformationDeltaInplace transformation = initiateDIMTransformationDeltaInplace("sampled-communications_main_linux_Instance");
		modifications.modifyRAMSES(transformation.getTopSystemInst());
		transformation.dispose();
		doDeclarativeComparisonTest();
//		doInstanceComparisonTest("sampled-communications_main_linux_Instance");
	}
	
	@Test
	public void testRAMSESWithUtils() throws Exception {
		refDirectory += "ramses-linux_with_utils/";
		DIMTransformationDeltaInplace transformation = initiateDIMTransformationDeltaInplace("sampled-communications_main_linux_Instance");
		modificationsWithUtils.modifyRAMSES(transformation.getTopSystemInst());
		transformation.dispose();
		doDeclarativeComparisonTest();
//		doInstanceComparisonTest("sampled-communications_main_linux_Instance");
	}
	
	@Test
	public void testExperimentComponentCreation() throws Exception {
		refDirectory += "experiment/";
		DIMTransformationDeltaInplace transformation = initiateDIMTransformationDeltaInplace("sampled-communications_main_linux_Instance");
		ComponentInstance proc = topSystemInst.getAllComponentInstances(ComponentCategory.PROCESS).get(0);
		ComponentInstance viewer = proc.createComponentInstance();
		viewer.setName("the_viewer");
		viewer.setCategory(ComponentCategory.THREAD);
		transformation.dispose();
		doDeclarativeComparisonTest();
//		doInstanceComparisonTest("sampled-communications_main_linux_Instance");
	}
	
	@Test
	public void testExperimentComponentUpdationName() throws Exception {
		DIMTransformationDeltaInplace transformation = initiateDIMTransformationDeltaInplace("sampled-communications_main_linux_Instance");
		ComponentInstance proc = topSystemInst.getAllComponentInstances(ComponentCategory.PROCESS).get(0);
		proc.setName("the_proc_renamed");
		transformation.dispose();
		doDeclarativeComparisonTest();
//		doInstanceComparisonTest("sampled-communications_main_linux_Instance");
	}
	
//	@Test
//	public void testExperimentComponentRefinementCategory() throws Exception {
//		DIMTransformationDeltaInplace transformation = initiateDIMTransformationDeltaInplace("sampled-communications_main_linux_Instance");
//		ComponentInstance proc = topSystemInst.getAllComponentInstances(ComponentCategory.PROCESS).get(0);
//		ComponentInstance viewer = proc.createComponentInstance();
//		viewer.setName("the_viewer");
//		viewer.setCategory(ComponentCategory.THREAD);
//		transformation.dispose();
//		doDeclarativeComparisonTest("component-refine-category");
////		doInstanceComparisonTest("sampled-communications_main_linux_Instance");
//	}
//	
//	@Test
//	public void testExperimentComponentUpdationCategory() throws Exception {
//		DIMTransformationDeltaInplace transformation = initiateDIMTransformationDeltaInplace("sampled-communications_main_linux_Instance");
//		ComponentInstance proc = topSystemInst.getAllComponentInstances(ComponentCategory.PROCESS).get(0);
//		ComponentInstance viewer = proc.createComponentInstance();
//		viewer.setName("the_viewer");
//		viewer.setCategory(ComponentCategory.THREAD);
//		viewer.setCategory(ComponentCategory.SYSTEM);
//		transformation.dispose();
//		doDeclarativeComparisonTest("component-update-category");
////		doInstanceComparisonTest("sampled-communications_main_linux_Instance");
//	}
//	
//	@Test
//	public void testExperimentComponentUpdationClassifier() throws Exception {
//		DIMTransformationDeltaInplace transformation = initiateDIMTransformationDeltaInplace("sampled-communications_main_linux_Instance");
//		modifications.updateComponent(transformation.getTopSystemInst());
//		transformation.dispose();
////		doInstanceComparisonTest("sampled-communications_main_linux_Instance");
//	}
//	
//	@Test
//	public void testExperimentComponentRefinementClassifier() throws Exception {
//		DIMTransformationDeltaInplace transformation = initiateDIMTransformationDeltaInplace("sampled-communications_main_linux_Instance");
//		modifications.updateComponent(transformation.getTopSystemInst());
//		transformation.dispose();
////		doInstanceComparisonTest("sampled-communications_main_linux_Instance");
//	}
//	
//	@Test
//	public void testExperimentComponentDeletion() throws Exception {
//		DIMTransformationDeltaInplace transformation = initiateDIMTransformationDeltaInplace("sampled-communications_main_linux_Instance");
//		modifications.deleteComponent(transformation.getTopSystemInst());
//		transformation.dispose();
////		doInstanceComparisonTest("sampled-communications_main_linux_Instance");
//	}
//	
//	@Test
//	public void testExperimentFeatureCreation() throws Exception {
//		DIMTransformationDeltaInplace transformation = initiateDIMTransformationDeltaInplace("sampled-communications_main_linux_Instance");
//		modifications.createFeature(transformation.getTopSystemInst());
//		transformation.dispose();
////		doInstanceComparisonTest("sampled-communications_main_linux_Instance");
//	}
//	
//	@Test
//	public void testExperimentFeatureUpdation() throws Exception {
//		DIMTransformationDeltaInplace transformation = initiateDIMTransformationDeltaInplace("sampled-communications_main_linux_Instance");
//		modifications.updateFeature(transformation.getTopSystemInst());
//		transformation.dispose();
////		doInstanceComparisonTest("sampled-communications_main_linux_Instance");
//	}
//	
//	@Test
//	public void testExperimentFeatureDeletion() throws Exception {
//		DIMTransformationDeltaInplace transformation = initiateDIMTransformationDeltaInplace("sampled-communications_main_linux_Instance");
//		modifications.deleteFeature(transformation.getTopSystemInst());
//		transformation.dispose();
////		doInstanceComparisonTest("sampled-communications_main_linux_Instance");
//	}
//	
//	@Test
//	public void testExperimentConnectionCreation() throws Exception {
//		DIMTransformationDeltaInplace transformation = initiateDIMTransformationDeltaInplace("sampled-communications_main_linux_Instance");
//		modifications.createConnection(transformation.getTopSystemInst());
//		transformation.dispose();
////		doInstanceComparisonTest("sampled-communications_main_linux_Instance");
//	}
//	
//	@Test
//	public void testExperimentConnectionUpdation() throws Exception {
//		DIMTransformationDeltaInplace transformation = initiateDIMTransformationDeltaInplace("sampled-communications_main_linux_Instance");
//		modifications.updateConnection(transformation.getTopSystemInst());
//		transformation.dispose();
////		doInstanceComparisonTest("sampled-communications_main_linux_Instance");
//	}
//	
//	@Test
//	public void testExperimentConnectionDeletion() throws Exception {
//		DIMTransformationDeltaInplace transformation = initiateDIMTransformationDeltaInplace("sampled-communications_main_linux_Instance");
//		modifications.deleteConnection(transformation.getTopSystemInst());
//		transformation.dispose();
////		doInstanceComparisonTest("sampled-communications_main_linux_Instance");
//	}
//	
//	@Test
//	public void testExperimentPropertyCreation() throws Exception {
//		DIMTransformationDeltaInplace transformation = initiateDIMTransformationDeltaInplace("sampled-communications_main_linux_Instance");
//		modifications.createFeature(transformation.getTopSystemInst());
//		transformation.dispose();
////		doInstanceComparisonTest("sampled-communications_main_linux_Instance");
//	}
//	
//	@Test
//	public void testExperimentPropertyValueUpdation() throws Exception {
//		DIMTransformationDeltaInplace transformation = initiateDIMTransformationDeltaInplace("sampled-communications_main_linux_Instance");
//		modifications.updateFeature(transformation.getTopSystemInst());
//		transformation.dispose();
////		doInstanceComparisonTest("sampled-communications_main_linux_Instance");
//	}
//	
//	@Test
//	public void testExperimentPropertyDeletion() throws Exception {
//		DIMTransformationDeltaInplace transformation = initiateDIMTransformationDeltaInplace("sampled-communications_main_linux_Instance");
//		modifications.deleteFeature(transformation.getTopSystemInst());
//		transformation.dispose();
////		doInstanceComparisonTest("sampled-communications_main_linux_Instance");
//	}
}