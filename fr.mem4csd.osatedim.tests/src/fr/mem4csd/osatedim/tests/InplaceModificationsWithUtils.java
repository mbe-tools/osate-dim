package fr.mem4csd.osatedim.tests;

import org.osate.aadl2.ComponentCategory;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.ConnectionKind;
import org.osate.aadl2.instance.FeatureInstance;
import org.osate.aadl2.instance.SystemInstance;
import fr.mem4csd.osatedim.manipulation.InstanceElementCreation;

public class InplaceModificationsWithUtils extends InplaceModifications {

	public InplaceModificationsWithUtils(SystemInstance topSystemInst) {
		super(topSystemInst);
	}
	
	@Override
	protected ComponentInstance createDataSubcomponent(ComponentInstance proc, String name, ComponentInstance mem) {
		ComponentInstance data = InstanceElementCreation.createComponentInstance(proc, name, ComponentCategory.DATA, null);
		addActualMemoryBindingProperty(data, mem);
		return data;
	}
	
	@Override
	protected void addRAMSESConnection(ComponentInstance proc, String name, ComponentInstance data1, FeatureInstance feat1) {
		InstanceElementCreation.createConnectionInstance(proc, name, data1, feat1, ConnectionKind.ACCESS_CONNECTION, false, true);
	}
}
