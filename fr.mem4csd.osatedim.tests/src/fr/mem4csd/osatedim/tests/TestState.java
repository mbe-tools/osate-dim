package fr.mem4csd.osatedim.tests;

import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;

import org.eclipse.emf.common.util.URI;
import org.junit.Test;
import org.osate.aadl2.AadlPackage;
import org.osate.aadl2.PropertySet;

import fr.mem4csd.osatedim.preference.DIMPreferences;
import fr.mem4csd.osatedim.ui.utils.DeinstantiationUtils;
import fr.mem4csd.osatedim.utils.DIMStandaloneLogger;
import fr.mem4csd.osatedim.utils.PackageUtils;
import fr.mem4csd.osatedim.utils.TransformationUtils;
import fr.mem4csd.osatedim.viatra.transformations.DIMTransformationState;

public class TestState extends TestAbstract {
	
	private static final Logger LOGGER = Logger.getLogger(TestState.class.getName());
	
	public TestState() {
		refDirectory += "state/";
	}
	
	protected void executeDIMTransformationState(final String inputModelName) throws IOException {
		prepareTestStateResources(inputModelName);
		AadlPackage aadlPackage = PackageUtils.configureAadlPackage(topSystemInst.eResource().getURI(), topSystemInst);
		aadlResource.getContents().add(aadlPackage);
		final PropertySet DIMPropertySet = getStandaloneDIMPropertySet(aaxlResource.getResourceSet());
		DIMTransformationState transformation = TransformationUtils.executeStateDIM(aaxlResource.getResourceSet(), topSystemInst, aadlPackage, aadlResource, new DIMPreferences(false, false, false, false, false), DIMPropertySet, new DIMStandaloneLogger());
		TransformationUtils.finishStateDIM(transformation);
		LOGGER.log(Level.INFO, "DIM: State-based DIMTransformation finished corresponding to "+inputModelName+" instance model.");
	}
	
	protected static void prepareTestStateResources(String inputModelName) throws IOException {
		prepareTestResources(inputModelName);
		final URI declarativeModelUri = DeinstantiationUtils.computeDeclarativeUri(aaxlResource.getURI(), topSystemInst);
		aadlResource = resourceSet.createResource(declarativeModelUri);
	}
	
	protected void testDIMTransformationState(final String inputModelName) throws Exception {
		executeDIMTransformationState(inputModelName);
		doDeclarativeComparisonTest();
//		doInstanceComparisonTest(inputModelName);
	}
	
	@Test
	public void testMCDAG() throws Exception {
		refDirectory += "mc-dag/";
		testDIMTransformationState("declarative_main_impl_Instance");
	}
	
	@Test
	public void testRAMSES() throws Exception {
		refDirectory += "ramses-linux/";
		testDIMTransformationState("sampled-communications_main_linux_Instance");
	}
}
