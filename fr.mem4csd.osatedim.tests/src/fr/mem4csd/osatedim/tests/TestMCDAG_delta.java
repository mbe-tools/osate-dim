package fr.mem4csd.osatedim.tests;

import java.io.File;
import java.io.IOException;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.viatra.query.patternlanguage.emf.EMFPatternLanguageStandaloneSetup;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.emf.EMFScope;
import org.junit.Test;

import fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlPackage;
import fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTraceSpec;
import fr.mem4csd.osatedim.viatra.transformations.DIMTransformationDeltaOutplace;
import fr.tpt.mem4csd.utils.osate.standalone.OsateStandaloneSetup;
import fr.tpt.mem4csd.utils.osate.standalone.StandaloneXtextResourceSet;

public class TestMCDAG_delta {
	
	protected static final URI BASE_URI = URI.createFileURI(new File(".").getAbsolutePath());
	
	private final String baseDir ;

	protected TestMCDAG_delta() {
		this.baseDir = "cases/mc-dag/instances/";
		configureResourceSet();
	}

	protected void configureResourceSet() {
		Aaxl2AaxlPackage.eINSTANCE.eClass();
		new EMFPatternLanguageStandaloneSetup().createInjectorAndDoEMFRegistration();
	}
	
	protected ResourceSet createResourceSet() {
		ResourceSet resourceSet = new OsateStandaloneSetup(BASE_URI).createResourceSet();
		/*If using RAMSES plugin (compilation problems with XText Builder)*/
		//URI ramsesURI = URI.createURI(StandaloneXtextResourceSet.SCHEME + "ramses/core/propertysets/RAMSES.aadl" );
		URI ramsesURI = URI.createURI(StandaloneXtextResourceSet.SCHEME + "fr.mem4csd.osatedim.viatra.utils/RAMSES.aadl");
		resourceSet.getResource(ramsesURI, true);
		return resourceSet; 
	}
	
	protected URI createURI(final String modelName, final String fileExtension) {
		final URI uri = URI.createFileURI(baseDir + modelName).resolve(BASE_URI);
		return fileExtension == null ? uri : uri.appendFileExtension(fileExtension);
	}
	
	protected Resource executeDIMTransformation_delta(final String inputModelName) throws IOException {
		final URI modelUri = createURI(inputModelName, Aaxl2AaxlPackage.eNAME);
		final ResourceSet resSet = createResourceSet();
		final Resource aaxlTraceResource = resSet.getResource(modelUri, true);
		Aaxl2AaxlTraceSpec aaxlTraceModel = (Aaxl2AaxlTraceSpec) aaxlTraceResource.getContents().get(0);
		
		ViatraQueryEngine engine = ViatraQueryEngine.on(new EMFScope(aaxlTraceModel.eResource().getResourceSet()));
		DIMTransformationDeltaOutplace transformation = new DIMTransformationDeltaOutplace();
		transformation.initialize(aaxlTraceModel, engine);
		transformation.execute();
		transformation.dispose();

		aaxlTraceModel.getLeftSystem().getComponentImplementation().eResource().save(null);
			
		System.out.println("DIM: Delta-based DIMTransformation finished corresponding to "+inputModelName+" deltatrace model.");
		return null;
	}
	
	@Test
	public void testMCDAG_delta () throws IOException {
		executeDIMTransformation_delta("scheduling");
	}

}
