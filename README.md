# OSATE-DIM 

### Open-Source AADL Tool Environment-based Declarative Instance Mapping

This project contains the source code associated with the OSATE-DIM tool.

The Core of the transformation is specified in:
- _fr.mem4csd.osatedim_

The User Interface of the tool is specified in:
- _fr.mem4csd.osatedim.ui_

The Test suite for validating the tool is provided in:
- _fr.mem4csd.osatedim.tests_ : The Test methods 
- _fr.mem4csd.osatedim.tests.oracle_ : The Oracle files used for comparison with OSATE-DIM generated results
- _fr.mem4csd.osatedim.tests.ref_ : The clean Reference test files, which are copied into the _\[.tests\]_ project in the beginning of each test.

The meta-model for the Aaxl2Aaxl trace model and its EMF-based editor is provided in:
- _fr.mem4csd.osatedim.deltatrace_
- _fr.mem4csd.osatedim.deltatrace.edit_
- _fr.mem4csd.osatedim.deltatrace.editor_
- _fr.mem4csd.osatedim.deltatrace.tests_

### Dependencies

This project has the following dependencies:
- _fr.tpt.mem4csd.utils.compare_ _\[.emf\]_ _\[.aadl\]_ : Comparison framework for the tests project.  
- _org.osate.aadl2_ : OSATE
- _fr.tpt.mem4csd.utils.osate.standalone_ : Required to run OSATE as a plain Java application (without the Eclipse platform and OSGI)
- _org.eclipse.xtext_ : Required to avoid verbose logs
- _org.eclipse.viatra_ : Graph-based model transformation tool used by OSATE-DIM to de-instantiate Instance models

### Installation

1. Installing into an existing Eclipse:

- Install OSATE from its update-site (menu Help –> Install new Software… and Add OSATE update-site): https://osate-build.sei.cmu.edu/download/osate/stable/latest/updates/
- Install OSATE-DIM from its update-site (menu Help –> Install new Software… and Add OSATE-DIM update-site): ​https://mem4csd.telecom-paristech.fr/download/update-site/osate-dim/
- Select all the projects and click Next > Finish

2. Downloading an OSATE-DIM Eclipse product :

- You can also download an Eclipse product release with OSATE-DIM from the Zenodo repository with DOI: 10.5281/zenodo.6971720
- The product release is available for Windows and Linux platforms.

3. Development Environment

- You may also setup an Eclipse environment to develop OSATE-DIM by pulling the source code from this repository.
- To test, you should then run an Eclipse application from within the Eclipse workspace that contains the source code plugin projects.


### Weblinks

More details about the tool can be found on the [tool homepage](https://mem4csd.telecom-paristech.fr/blog/index.php/osate-dim/)

### References

This work is used in the publication of

- Rakshit Mittal, Dominique Blouin, Anish Bhobe, and Soumyadip Bandyopadhyay. 2022. Solving the instance model-view update problem in AADL. In Proceedings of the 25th International Conference on Model Driven Engineering Languages and Systems (MODELS '22). Association for Computing Machinery, New York, NY, USA, 55–65. https://doi.org/10.1145/3550355.3552396

- Rakshit Mittal and Dominique Blouin. 2022. OSATE-DIM solves the instance model-view update problem in AADL. In Proceedings of the 25th International Conference on Model Driven Engineering Languages and Systems: Companion Proceedings (MODELS '22). Association for Computing Machinery, New York, NY, USA, 1–6. https://doi.org/10.1145/3550356.3559083

- Rakshit Mittal. 2022. The instance model-view update problem in AADL. In Proceedings of the 25th International Conference on Model Driven Engineering Languages and Systems: Companion Proceedings (MODELS '22). Association for Computing Machinery, New York, NY, USA, 221–224. https://doi.org/10.1145/3550356.3552373
