/**
 * Generated from platform:/resource/fr.mem4csd.osatedim/src/fr/mem4csd/osatedim/viatra/queries/DIMQueriesInplace.vql
 */
package fr.mem4csd.osatedim.viatra.queries;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.viatra.query.runtime.api.IPatternMatch;
import org.eclipse.viatra.query.runtime.api.IQuerySpecification;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.viatra.query.runtime.api.impl.BaseMatcher;
import org.eclipse.viatra.query.runtime.api.impl.BasePatternMatch;
import org.eclipse.viatra.query.runtime.emf.types.EClassTransitiveInstancesKey;
import org.eclipse.viatra.query.runtime.matchers.backend.QueryEvaluationHint;
import org.eclipse.viatra.query.runtime.matchers.psystem.PBody;
import org.eclipse.viatra.query.runtime.matchers.psystem.PVariable;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.TypeConstraint;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameterDirection;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PVisibility;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuple;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuples;
import org.eclipse.viatra.query.runtime.util.ViatraQueryLoggingUtil;
import org.osate.aadl2.instance.ComponentInstance;

/**
 * A pattern-specific query specification that can instantiate Matcher in a type-safe way.
 * 
 * <p>Original source:
 *         <code><pre>
 *         pattern findComponentInstance(compinst : ComponentInstance) 
 *         {
 *         	ComponentInstance(compinst);
 *         }
 * </pre></code>
 * 
 * @see Matcher
 * @see Match
 * 
 */
@SuppressWarnings("all")
public final class FindComponentInstance extends BaseGeneratedEMFQuerySpecification<FindComponentInstance.Matcher> {
  /**
   * Pattern-specific match representation of the fr.mem4csd.osatedim.viatra.queries.findComponentInstance pattern,
   * to be used in conjunction with {@link Matcher}.
   * 
   * <p>Class fields correspond to parameters of the pattern. Fields with value null are considered unassigned.
   * Each instance is a (possibly partial) substitution of pattern parameters,
   * usable to represent a match of the pattern in the result of a query,
   * or to specify the bound (fixed) input parameters when issuing a query.
   * 
   * @see Matcher
   * 
   */
  public static abstract class Match extends BasePatternMatch {
    private ComponentInstance fCompinst;

    private static List<String> parameterNames = makeImmutableList("compinst");

    private Match(final ComponentInstance pCompinst) {
      this.fCompinst = pCompinst;
    }

    @Override
    public Object get(final String parameterName) {
      switch(parameterName) {
          case "compinst": return this.fCompinst;
          default: return null;
      }
    }

    @Override
    public Object get(final int index) {
      switch(index) {
          case 0: return this.fCompinst;
          default: return null;
      }
    }

    public ComponentInstance getCompinst() {
      return this.fCompinst;
    }

    @Override
    public boolean set(final String parameterName, final Object newValue) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      if ("compinst".equals(parameterName) ) {
          this.fCompinst = (ComponentInstance) newValue;
          return true;
      }
      return false;
    }

    public void setCompinst(final ComponentInstance pCompinst) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fCompinst = pCompinst;
    }

    @Override
    public String patternName() {
      return "fr.mem4csd.osatedim.viatra.queries.findComponentInstance";
    }

    @Override
    public List<String> parameterNames() {
      return FindComponentInstance.Match.parameterNames;
    }

    @Override
    public Object[] toArray() {
      return new Object[]{fCompinst};
    }

    @Override
    public FindComponentInstance.Match toImmutable() {
      return isMutable() ? newMatch(fCompinst) : this;
    }

    @Override
    public String prettyPrint() {
      StringBuilder result = new StringBuilder();
      result.append("\"compinst\"=" + prettyPrintValue(fCompinst));
      return result.toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(fCompinst);
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj)
          return true;
      if (obj == null) {
          return false;
      }
      if ((obj instanceof FindComponentInstance.Match)) {
          FindComponentInstance.Match other = (FindComponentInstance.Match) obj;
          return Objects.equals(fCompinst, other.fCompinst);
      } else {
          // this should be infrequent
          if (!(obj instanceof IPatternMatch)) {
              return false;
          }
          IPatternMatch otherSig  = (IPatternMatch) obj;
          return Objects.equals(specification(), otherSig.specification()) && Arrays.deepEquals(toArray(), otherSig.toArray());
      }
    }

    @Override
    public FindComponentInstance specification() {
      return FindComponentInstance.instance();
    }

    /**
     * Returns an empty, mutable match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @return the empty match.
     * 
     */
    public static FindComponentInstance.Match newEmptyMatch() {
      return new Mutable(null);
    }

    /**
     * Returns a mutable (partial) match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @return the new, mutable (partial) match object.
     * 
     */
    public static FindComponentInstance.Match newMutableMatch(final ComponentInstance pCompinst) {
      return new Mutable(pCompinst);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public static FindComponentInstance.Match newMatch(final ComponentInstance pCompinst) {
      return new Immutable(pCompinst);
    }

    private static final class Mutable extends FindComponentInstance.Match {
      Mutable(final ComponentInstance pCompinst) {
        super(pCompinst);
      }

      @Override
      public boolean isMutable() {
        return true;
      }
    }

    private static final class Immutable extends FindComponentInstance.Match {
      Immutable(final ComponentInstance pCompinst) {
        super(pCompinst);
      }

      @Override
      public boolean isMutable() {
        return false;
      }
    }
  }

  /**
   * Generated pattern matcher API of the fr.mem4csd.osatedim.viatra.queries.findComponentInstance pattern,
   * providing pattern-specific query methods.
   * 
   * <p>Use the pattern matcher on a given model via {@link #on(ViatraQueryEngine)},
   * e.g. in conjunction with {@link ViatraQueryEngine#on(QueryScope)}.
   * 
   * <p>Matches of the pattern will be represented as {@link Match}.
   * 
   * <p>Original source:
   * <code><pre>
   * pattern findComponentInstance(compinst : ComponentInstance) 
   * {
   * 	ComponentInstance(compinst);
   * }
   * </pre></code>
   * 
   * @see Match
   * @see FindComponentInstance
   * 
   */
  public static class Matcher extends BaseMatcher<FindComponentInstance.Match> {
    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    public static FindComponentInstance.Matcher on(final ViatraQueryEngine engine) {
      // check if matcher already exists
      Matcher matcher = engine.getExistingMatcher(querySpecification());
      if (matcher == null) {
          matcher = (Matcher)engine.getMatcher(querySpecification());
      }
      return matcher;
    }

    /**
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * @return an initialized matcher
     * @noreference This method is for internal matcher initialization by the framework, do not call it manually.
     * 
     */
    public static FindComponentInstance.Matcher create() {
      return new Matcher();
    }

    private static final int POSITION_COMPINST = 0;

    private static final Logger LOGGER = ViatraQueryLoggingUtil.getLogger(FindComponentInstance.Matcher.class);

    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    private Matcher() {
      super(querySpecification());
    }

    /**
     * Returns the set of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @return matches represented as a Match object.
     * 
     */
    public Collection<FindComponentInstance.Match> getAllMatches(final ComponentInstance pCompinst) {
      return rawStreamAllMatches(new Object[]{pCompinst}).collect(Collectors.toSet());
    }

    /**
     * Returns a stream of all matches of the pattern that conform to the given fixed values of some parameters.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @return a stream of matches represented as a Match object.
     * 
     */
    public Stream<FindComponentInstance.Match> streamAllMatches(final ComponentInstance pCompinst) {
      return rawStreamAllMatches(new Object[]{pCompinst});
    }

    /**
     * Returns an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @return a match represented as a Match object, or null if no match is found.
     * 
     */
    public Optional<FindComponentInstance.Match> getOneArbitraryMatch(final ComponentInstance pCompinst) {
      return rawGetOneArbitraryMatch(new Object[]{pCompinst});
    }

    /**
     * Indicates whether the given combination of specified pattern parameters constitute a valid pattern match,
     * under any possible substitution of the unspecified parameters (if any).
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @return true if the input is a valid (partial) match of the pattern.
     * 
     */
    public boolean hasMatch(final ComponentInstance pCompinst) {
      return rawHasMatch(new Object[]{pCompinst});
    }

    /**
     * Returns the number of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @return the number of pattern matches found.
     * 
     */
    public int countMatches(final ComponentInstance pCompinst) {
      return rawCountMatches(new Object[]{pCompinst});
    }

    /**
     * Executes the given processor on an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param processor the action that will process the selected match.
     * @return true if the pattern has at least one match with the given parameter values, false if the processor was not invoked
     * 
     */
    public boolean forOneArbitraryMatch(final ComponentInstance pCompinst, final Consumer<? super FindComponentInstance.Match> processor) {
      return rawForOneArbitraryMatch(new Object[]{pCompinst}, processor);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public FindComponentInstance.Match newMatch(final ComponentInstance pCompinst) {
      return FindComponentInstance.Match.newMatch(pCompinst);
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ComponentInstance> rawStreamAllValuesOfcompinst(final Object[] parameters) {
      return rawStreamAllValues(POSITION_COMPINST, parameters).map(ComponentInstance.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst() {
      return rawStreamAllValuesOfcompinst(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst() {
      return rawStreamAllValuesOfcompinst(emptyArray());
    }

    @Override
    protected FindComponentInstance.Match tupleToMatch(final Tuple t) {
      try {
          return FindComponentInstance.Match.newMatch((ComponentInstance) t.get(POSITION_COMPINST));
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in tuple not properly typed!",e);
          return null;
      }
    }

    @Override
    protected FindComponentInstance.Match arrayToMatch(final Object[] match) {
      try {
          return FindComponentInstance.Match.newMatch((ComponentInstance) match[POSITION_COMPINST]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    @Override
    protected FindComponentInstance.Match arrayToMatchMutable(final Object[] match) {
      try {
          return FindComponentInstance.Match.newMutableMatch((ComponentInstance) match[POSITION_COMPINST]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    /**
     * @return the singleton instance of the query specification of this pattern
     * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
     * 
     */
    public static IQuerySpecification<FindComponentInstance.Matcher> querySpecification() {
      return FindComponentInstance.instance();
    }
  }

  private FindComponentInstance() {
    super(GeneratedPQuery.INSTANCE);
  }

  /**
   * @return the singleton instance of the query specification
   * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
   * 
   */
  public static FindComponentInstance instance() {
    try{
        return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
        throw processInitializerError(err);
    }
  }

  @Override
  protected FindComponentInstance.Matcher instantiate(final ViatraQueryEngine engine) {
    return FindComponentInstance.Matcher.on(engine);
  }

  @Override
  public FindComponentInstance.Matcher instantiate() {
    return FindComponentInstance.Matcher.create();
  }

  @Override
  public FindComponentInstance.Match newEmptyMatch() {
    return FindComponentInstance.Match.newEmptyMatch();
  }

  @Override
  public FindComponentInstance.Match newMatch(final Object... parameters) {
    return FindComponentInstance.Match.newMatch((org.osate.aadl2.instance.ComponentInstance) parameters[0]);
  }

  /**
   * Inner class allowing the singleton instance of {@link FindComponentInstance} to be created 
   *     <b>not</b> at the class load time of the outer class, 
   *     but rather at the first call to {@link FindComponentInstance#instance()}.
   * 
   * <p> This workaround is required e.g. to support recursion.
   * 
   */
  private static class LazyHolder {
    private static final FindComponentInstance INSTANCE = new FindComponentInstance();

    /**
     * Statically initializes the query specification <b>after</b> the field {@link #INSTANCE} is assigned.
     * This initialization order is required to support indirect recursion.
     * 
     * <p> The static initializer is defined using a helper field to work around limitations of the code generator.
     * 
     */
    private static final Object STATIC_INITIALIZER = ensureInitialized();

    public static Object ensureInitialized() {
      INSTANCE.ensureInitializedInternal();
      return null;
    }
  }

  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private static final FindComponentInstance.GeneratedPQuery INSTANCE = new GeneratedPQuery();

    private final PParameter parameter_compinst = new PParameter("compinst", "org.osate.aadl2.instance.ComponentInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ComponentInstance")), PParameterDirection.INOUT);

    private final List<PParameter> parameters = Arrays.asList(parameter_compinst);

    private GeneratedPQuery() {
      super(PVisibility.PUBLIC);
    }

    @Override
    public String getFullyQualifiedName() {
      return "fr.mem4csd.osatedim.viatra.queries.findComponentInstance";
    }

    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("compinst");
    }

    @Override
    public List<PParameter> getParameters() {
      return parameters;
    }

    @Override
    public Set<PBody> doGetContainedBodies() {
      setEvaluationHints(new QueryEvaluationHint(null, QueryEvaluationHint.BackendRequirement.UNSPECIFIED));
      Set<PBody> bodies = new LinkedHashSet<>();
      {
          PBody body = new PBody(this);
          PVariable var_compinst = body.getOrCreateVariableByName("compinst");
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_compinst, parameter_compinst)
          ));
          // 	ComponentInstance(compinst)
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          bodies.add(body);
      }
      return bodies;
    }
  }
}
