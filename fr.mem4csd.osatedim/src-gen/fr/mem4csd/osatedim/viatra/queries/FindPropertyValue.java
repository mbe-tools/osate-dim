/**
 * Generated from platform:/resource/fr.mem4csd.osatedim/src/fr/mem4csd/osatedim/viatra/queries/DIMQueriesInplace.vql
 */
package fr.mem4csd.osatedim.viatra.queries;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.viatra.query.runtime.api.IPatternMatch;
import org.eclipse.viatra.query.runtime.api.IQuerySpecification;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.viatra.query.runtime.api.impl.BaseMatcher;
import org.eclipse.viatra.query.runtime.api.impl.BasePatternMatch;
import org.eclipse.viatra.query.runtime.emf.types.EClassTransitiveInstancesKey;
import org.eclipse.viatra.query.runtime.matchers.backend.QueryEvaluationHint;
import org.eclipse.viatra.query.runtime.matchers.psystem.PBody;
import org.eclipse.viatra.query.runtime.matchers.psystem.PVariable;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.NegativePatternCall;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.TypeConstraint;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameterDirection;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PVisibility;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuple;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuples;
import org.eclipse.viatra.query.runtime.util.ViatraQueryLoggingUtil;
import org.osate.aadl2.PropertyValue;

/**
 * A pattern-specific query specification that can instantiate Matcher in a type-safe way.
 * 
 * <p>Original source:
 *         <code><pre>
 *         // Find property value (this pattern matches with property values on the declarative side as well)
 *         // Appropriate measures are added within the transformation rules to account for this, and only react to states of property
 *         // values on the instance side
 *         pattern findPropertyValue(propvalinst : PropertyValue) {
 *         	neg RecordValue(propvalinst);
 *         	PropertyValue(propvalinst);
 *         }
 * </pre></code>
 * 
 * @see Matcher
 * @see Match
 * 
 */
@SuppressWarnings("all")
public final class FindPropertyValue extends BaseGeneratedEMFQuerySpecification<FindPropertyValue.Matcher> {
  /**
   * Pattern-specific match representation of the fr.mem4csd.osatedim.viatra.queries.findPropertyValue pattern,
   * to be used in conjunction with {@link Matcher}.
   * 
   * <p>Class fields correspond to parameters of the pattern. Fields with value null are considered unassigned.
   * Each instance is a (possibly partial) substitution of pattern parameters,
   * usable to represent a match of the pattern in the result of a query,
   * or to specify the bound (fixed) input parameters when issuing a query.
   * 
   * @see Matcher
   * 
   */
  public static abstract class Match extends BasePatternMatch {
    private PropertyValue fPropvalinst;

    private static List<String> parameterNames = makeImmutableList("propvalinst");

    private Match(final PropertyValue pPropvalinst) {
      this.fPropvalinst = pPropvalinst;
    }

    @Override
    public Object get(final String parameterName) {
      switch(parameterName) {
          case "propvalinst": return this.fPropvalinst;
          default: return null;
      }
    }

    @Override
    public Object get(final int index) {
      switch(index) {
          case 0: return this.fPropvalinst;
          default: return null;
      }
    }

    public PropertyValue getPropvalinst() {
      return this.fPropvalinst;
    }

    @Override
    public boolean set(final String parameterName, final Object newValue) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      if ("propvalinst".equals(parameterName) ) {
          this.fPropvalinst = (PropertyValue) newValue;
          return true;
      }
      return false;
    }

    public void setPropvalinst(final PropertyValue pPropvalinst) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fPropvalinst = pPropvalinst;
    }

    @Override
    public String patternName() {
      return "fr.mem4csd.osatedim.viatra.queries.findPropertyValue";
    }

    @Override
    public List<String> parameterNames() {
      return FindPropertyValue.Match.parameterNames;
    }

    @Override
    public Object[] toArray() {
      return new Object[]{fPropvalinst};
    }

    @Override
    public FindPropertyValue.Match toImmutable() {
      return isMutable() ? newMatch(fPropvalinst) : this;
    }

    @Override
    public String prettyPrint() {
      StringBuilder result = new StringBuilder();
      result.append("\"propvalinst\"=" + prettyPrintValue(fPropvalinst));
      return result.toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(fPropvalinst);
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj)
          return true;
      if (obj == null) {
          return false;
      }
      if ((obj instanceof FindPropertyValue.Match)) {
          FindPropertyValue.Match other = (FindPropertyValue.Match) obj;
          return Objects.equals(fPropvalinst, other.fPropvalinst);
      } else {
          // this should be infrequent
          if (!(obj instanceof IPatternMatch)) {
              return false;
          }
          IPatternMatch otherSig  = (IPatternMatch) obj;
          return Objects.equals(specification(), otherSig.specification()) && Arrays.deepEquals(toArray(), otherSig.toArray());
      }
    }

    @Override
    public FindPropertyValue specification() {
      return FindPropertyValue.instance();
    }

    /**
     * Returns an empty, mutable match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @return the empty match.
     * 
     */
    public static FindPropertyValue.Match newEmptyMatch() {
      return new Mutable(null);
    }

    /**
     * Returns a mutable (partial) match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @param pPropvalinst the fixed value of pattern parameter propvalinst, or null if not bound.
     * @return the new, mutable (partial) match object.
     * 
     */
    public static FindPropertyValue.Match newMutableMatch(final PropertyValue pPropvalinst) {
      return new Mutable(pPropvalinst);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pPropvalinst the fixed value of pattern parameter propvalinst, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public static FindPropertyValue.Match newMatch(final PropertyValue pPropvalinst) {
      return new Immutable(pPropvalinst);
    }

    private static final class Mutable extends FindPropertyValue.Match {
      Mutable(final PropertyValue pPropvalinst) {
        super(pPropvalinst);
      }

      @Override
      public boolean isMutable() {
        return true;
      }
    }

    private static final class Immutable extends FindPropertyValue.Match {
      Immutable(final PropertyValue pPropvalinst) {
        super(pPropvalinst);
      }

      @Override
      public boolean isMutable() {
        return false;
      }
    }
  }

  /**
   * Generated pattern matcher API of the fr.mem4csd.osatedim.viatra.queries.findPropertyValue pattern,
   * providing pattern-specific query methods.
   * 
   * <p>Use the pattern matcher on a given model via {@link #on(ViatraQueryEngine)},
   * e.g. in conjunction with {@link ViatraQueryEngine#on(QueryScope)}.
   * 
   * <p>Matches of the pattern will be represented as {@link Match}.
   * 
   * <p>Original source:
   * <code><pre>
   * // Find property value (this pattern matches with property values on the declarative side as well)
   * // Appropriate measures are added within the transformation rules to account for this, and only react to states of property
   * // values on the instance side
   * pattern findPropertyValue(propvalinst : PropertyValue) {
   * 	neg RecordValue(propvalinst);
   * 	PropertyValue(propvalinst);
   * }
   * </pre></code>
   * 
   * @see Match
   * @see FindPropertyValue
   * 
   */
  public static class Matcher extends BaseMatcher<FindPropertyValue.Match> {
    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    public static FindPropertyValue.Matcher on(final ViatraQueryEngine engine) {
      // check if matcher already exists
      Matcher matcher = engine.getExistingMatcher(querySpecification());
      if (matcher == null) {
          matcher = (Matcher)engine.getMatcher(querySpecification());
      }
      return matcher;
    }

    /**
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * @return an initialized matcher
     * @noreference This method is for internal matcher initialization by the framework, do not call it manually.
     * 
     */
    public static FindPropertyValue.Matcher create() {
      return new Matcher();
    }

    private static final int POSITION_PROPVALINST = 0;

    private static final Logger LOGGER = ViatraQueryLoggingUtil.getLogger(FindPropertyValue.Matcher.class);

    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    private Matcher() {
      super(querySpecification());
    }

    /**
     * Returns the set of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pPropvalinst the fixed value of pattern parameter propvalinst, or null if not bound.
     * @return matches represented as a Match object.
     * 
     */
    public Collection<FindPropertyValue.Match> getAllMatches(final PropertyValue pPropvalinst) {
      return rawStreamAllMatches(new Object[]{pPropvalinst}).collect(Collectors.toSet());
    }

    /**
     * Returns a stream of all matches of the pattern that conform to the given fixed values of some parameters.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     * @param pPropvalinst the fixed value of pattern parameter propvalinst, or null if not bound.
     * @return a stream of matches represented as a Match object.
     * 
     */
    public Stream<FindPropertyValue.Match> streamAllMatches(final PropertyValue pPropvalinst) {
      return rawStreamAllMatches(new Object[]{pPropvalinst});
    }

    /**
     * Returns an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pPropvalinst the fixed value of pattern parameter propvalinst, or null if not bound.
     * @return a match represented as a Match object, or null if no match is found.
     * 
     */
    public Optional<FindPropertyValue.Match> getOneArbitraryMatch(final PropertyValue pPropvalinst) {
      return rawGetOneArbitraryMatch(new Object[]{pPropvalinst});
    }

    /**
     * Indicates whether the given combination of specified pattern parameters constitute a valid pattern match,
     * under any possible substitution of the unspecified parameters (if any).
     * @param pPropvalinst the fixed value of pattern parameter propvalinst, or null if not bound.
     * @return true if the input is a valid (partial) match of the pattern.
     * 
     */
    public boolean hasMatch(final PropertyValue pPropvalinst) {
      return rawHasMatch(new Object[]{pPropvalinst});
    }

    /**
     * Returns the number of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pPropvalinst the fixed value of pattern parameter propvalinst, or null if not bound.
     * @return the number of pattern matches found.
     * 
     */
    public int countMatches(final PropertyValue pPropvalinst) {
      return rawCountMatches(new Object[]{pPropvalinst});
    }

    /**
     * Executes the given processor on an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pPropvalinst the fixed value of pattern parameter propvalinst, or null if not bound.
     * @param processor the action that will process the selected match.
     * @return true if the pattern has at least one match with the given parameter values, false if the processor was not invoked
     * 
     */
    public boolean forOneArbitraryMatch(final PropertyValue pPropvalinst, final Consumer<? super FindPropertyValue.Match> processor) {
      return rawForOneArbitraryMatch(new Object[]{pPropvalinst}, processor);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pPropvalinst the fixed value of pattern parameter propvalinst, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public FindPropertyValue.Match newMatch(final PropertyValue pPropvalinst) {
      return FindPropertyValue.Match.newMatch(pPropvalinst);
    }

    /**
     * Retrieve the set of values that occur in matches for propvalinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<PropertyValue> rawStreamAllValuesOfpropvalinst(final Object[] parameters) {
      return rawStreamAllValues(POSITION_PROPVALINST, parameters).map(PropertyValue.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for propvalinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<PropertyValue> getAllValuesOfpropvalinst() {
      return rawStreamAllValuesOfpropvalinst(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for propvalinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<PropertyValue> streamAllValuesOfpropvalinst() {
      return rawStreamAllValuesOfpropvalinst(emptyArray());
    }

    @Override
    protected FindPropertyValue.Match tupleToMatch(final Tuple t) {
      try {
          return FindPropertyValue.Match.newMatch((PropertyValue) t.get(POSITION_PROPVALINST));
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in tuple not properly typed!",e);
          return null;
      }
    }

    @Override
    protected FindPropertyValue.Match arrayToMatch(final Object[] match) {
      try {
          return FindPropertyValue.Match.newMatch((PropertyValue) match[POSITION_PROPVALINST]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    @Override
    protected FindPropertyValue.Match arrayToMatchMutable(final Object[] match) {
      try {
          return FindPropertyValue.Match.newMutableMatch((PropertyValue) match[POSITION_PROPVALINST]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    /**
     * @return the singleton instance of the query specification of this pattern
     * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
     * 
     */
    public static IQuerySpecification<FindPropertyValue.Matcher> querySpecification() {
      return FindPropertyValue.instance();
    }
  }

  private FindPropertyValue() {
    super(GeneratedPQuery.INSTANCE);
  }

  /**
   * @return the singleton instance of the query specification
   * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
   * 
   */
  public static FindPropertyValue instance() {
    try{
        return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
        throw processInitializerError(err);
    }
  }

  @Override
  protected FindPropertyValue.Matcher instantiate(final ViatraQueryEngine engine) {
    return FindPropertyValue.Matcher.on(engine);
  }

  @Override
  public FindPropertyValue.Matcher instantiate() {
    return FindPropertyValue.Matcher.create();
  }

  @Override
  public FindPropertyValue.Match newEmptyMatch() {
    return FindPropertyValue.Match.newEmptyMatch();
  }

  @Override
  public FindPropertyValue.Match newMatch(final Object... parameters) {
    return FindPropertyValue.Match.newMatch((org.osate.aadl2.PropertyValue) parameters[0]);
  }

  /**
   * Inner class allowing the singleton instance of {@link FindPropertyValue} to be created 
   *     <b>not</b> at the class load time of the outer class, 
   *     but rather at the first call to {@link FindPropertyValue#instance()}.
   * 
   * <p> This workaround is required e.g. to support recursion.
   * 
   */
  private static class LazyHolder {
    private static final FindPropertyValue INSTANCE = new FindPropertyValue();

    /**
     * Statically initializes the query specification <b>after</b> the field {@link #INSTANCE} is assigned.
     * This initialization order is required to support indirect recursion.
     * 
     * <p> The static initializer is defined using a helper field to work around limitations of the code generator.
     * 
     */
    private static final Object STATIC_INITIALIZER = ensureInitialized();

    public static Object ensureInitialized() {
      INSTANCE.ensureInitializedInternal();
      return null;
    }
  }

  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private static final FindPropertyValue.GeneratedPQuery INSTANCE = new GeneratedPQuery();

    private final PParameter parameter_propvalinst = new PParameter("propvalinst", "org.osate.aadl2.PropertyValue", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0", "PropertyValue")), PParameterDirection.INOUT);

    private final List<PParameter> parameters = Arrays.asList(parameter_propvalinst);

    private class Embedded_1_RecordValue extends BaseGeneratedEMFPQuery {
      private final PParameter parameter_p0 = new PParameter("p0", "org.osate.aadl2.RecordValue", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0", "RecordValue")), PParameterDirection.INOUT);

      private final List<PParameter> embeddedParameters = Arrays.asList(parameter_p0);

      public Embedded_1_RecordValue() {
        super(PVisibility.EMBEDDED);
      }

      @Override
      public String getFullyQualifiedName() {
        return GeneratedPQuery.this.getFullyQualifiedName() + "$Embedded_1_RecordValue";
      }

      @Override
      public List<PParameter> getParameters() {
        return embeddedParameters;
      }

      @Override
      public Set<PBody> doGetContainedBodies() {
        PBody body = new PBody(this);
        PVariable var_p0 = body.getOrCreateVariableByName("p0");
        body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
           new ExportedParameter(body, var_p0, parameter_p0)
        ));
        //  RecordValue(propvalinst)
        new TypeConstraint(body, Tuples.flatTupleOf(var_p0), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "RecordValue")));
        return Collections.singleton(body);
      }
    }

    private GeneratedPQuery() {
      super(PVisibility.PUBLIC);
    }

    @Override
    public String getFullyQualifiedName() {
      return "fr.mem4csd.osatedim.viatra.queries.findPropertyValue";
    }

    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("propvalinst");
    }

    @Override
    public List<PParameter> getParameters() {
      return parameters;
    }

    @Override
    public Set<PBody> doGetContainedBodies() {
      setEvaluationHints(new QueryEvaluationHint(null, QueryEvaluationHint.BackendRequirement.UNSPECIFIED));
      Set<PBody> bodies = new LinkedHashSet<>();
      {
          PBody body = new PBody(this);
          PVariable var_propvalinst = body.getOrCreateVariableByName("propvalinst");
          new TypeConstraint(body, Tuples.flatTupleOf(var_propvalinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "PropertyValue")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_propvalinst, parameter_propvalinst)
          ));
          // 	neg RecordValue(propvalinst)
          new NegativePatternCall(body, Tuples.flatTupleOf(var_propvalinst), new FindPropertyValue.GeneratedPQuery.Embedded_1_RecordValue());
          // 	PropertyValue(propvalinst)
          new TypeConstraint(body, Tuples.flatTupleOf(var_propvalinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "PropertyValue")));
          bodies.add(body);
      }
      return bodies;
    }
  }
}
