/**
 * Generated from platform:/resource/fr.mem4csd.osatedim/src/fr/mem4csd/osatedim/viatra/queries/DIMQueriesOutplace.vql
 */
package fr.mem4csd.osatedim.viatra.queries;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.viatra.query.runtime.api.IPatternMatch;
import org.eclipse.viatra.query.runtime.api.IQuerySpecification;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.viatra.query.runtime.api.impl.BaseMatcher;
import org.eclipse.viatra.query.runtime.api.impl.BasePatternMatch;
import org.eclipse.viatra.query.runtime.emf.types.EClassTransitiveInstancesKey;
import org.eclipse.viatra.query.runtime.matchers.backend.QueryEvaluationHint;
import org.eclipse.viatra.query.runtime.matchers.psystem.PBody;
import org.eclipse.viatra.query.runtime.matchers.psystem.PVariable;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.TypeConstraint;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameterDirection;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PVisibility;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuple;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuples;
import org.eclipse.viatra.query.runtime.util.ViatraQueryLoggingUtil;
import org.osate.aadl2.instance.FeatureInstance;

/**
 * A pattern-specific query specification that can instantiate Matcher in a type-safe way.
 * 
 * <p>Original source:
 *         <code><pre>
 *         pattern find_Feature2Feature(featinst : FeatureInstance)
 *         {
 *         	FeatureInstance(featinst);
 *         }
 * </pre></code>
 * 
 * @see Matcher
 * @see Match
 * 
 */
@SuppressWarnings("all")
public final class Find_Feature2Feature extends BaseGeneratedEMFQuerySpecification<Find_Feature2Feature.Matcher> {
  /**
   * Pattern-specific match representation of the fr.mem4csd.osatedim.viatra.queries.find_Feature2Feature pattern,
   * to be used in conjunction with {@link Matcher}.
   * 
   * <p>Class fields correspond to parameters of the pattern. Fields with value null are considered unassigned.
   * Each instance is a (possibly partial) substitution of pattern parameters,
   * usable to represent a match of the pattern in the result of a query,
   * or to specify the bound (fixed) input parameters when issuing a query.
   * 
   * @see Matcher
   * 
   */
  public static abstract class Match extends BasePatternMatch {
    private FeatureInstance fFeatinst;

    private static List<String> parameterNames = makeImmutableList("featinst");

    private Match(final FeatureInstance pFeatinst) {
      this.fFeatinst = pFeatinst;
    }

    @Override
    public Object get(final String parameterName) {
      switch(parameterName) {
          case "featinst": return this.fFeatinst;
          default: return null;
      }
    }

    @Override
    public Object get(final int index) {
      switch(index) {
          case 0: return this.fFeatinst;
          default: return null;
      }
    }

    public FeatureInstance getFeatinst() {
      return this.fFeatinst;
    }

    @Override
    public boolean set(final String parameterName, final Object newValue) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      if ("featinst".equals(parameterName) ) {
          this.fFeatinst = (FeatureInstance) newValue;
          return true;
      }
      return false;
    }

    public void setFeatinst(final FeatureInstance pFeatinst) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fFeatinst = pFeatinst;
    }

    @Override
    public String patternName() {
      return "fr.mem4csd.osatedim.viatra.queries.find_Feature2Feature";
    }

    @Override
    public List<String> parameterNames() {
      return Find_Feature2Feature.Match.parameterNames;
    }

    @Override
    public Object[] toArray() {
      return new Object[]{fFeatinst};
    }

    @Override
    public Find_Feature2Feature.Match toImmutable() {
      return isMutable() ? newMatch(fFeatinst) : this;
    }

    @Override
    public String prettyPrint() {
      StringBuilder result = new StringBuilder();
      result.append("\"featinst\"=" + prettyPrintValue(fFeatinst));
      return result.toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(fFeatinst);
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj)
          return true;
      if (obj == null) {
          return false;
      }
      if ((obj instanceof Find_Feature2Feature.Match)) {
          Find_Feature2Feature.Match other = (Find_Feature2Feature.Match) obj;
          return Objects.equals(fFeatinst, other.fFeatinst);
      } else {
          // this should be infrequent
          if (!(obj instanceof IPatternMatch)) {
              return false;
          }
          IPatternMatch otherSig  = (IPatternMatch) obj;
          return Objects.equals(specification(), otherSig.specification()) && Arrays.deepEquals(toArray(), otherSig.toArray());
      }
    }

    @Override
    public Find_Feature2Feature specification() {
      return Find_Feature2Feature.instance();
    }

    /**
     * Returns an empty, mutable match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @return the empty match.
     * 
     */
    public static Find_Feature2Feature.Match newEmptyMatch() {
      return new Mutable(null);
    }

    /**
     * Returns a mutable (partial) match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @param pFeatinst the fixed value of pattern parameter featinst, or null if not bound.
     * @return the new, mutable (partial) match object.
     * 
     */
    public static Find_Feature2Feature.Match newMutableMatch(final FeatureInstance pFeatinst) {
      return new Mutable(pFeatinst);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pFeatinst the fixed value of pattern parameter featinst, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public static Find_Feature2Feature.Match newMatch(final FeatureInstance pFeatinst) {
      return new Immutable(pFeatinst);
    }

    private static final class Mutable extends Find_Feature2Feature.Match {
      Mutable(final FeatureInstance pFeatinst) {
        super(pFeatinst);
      }

      @Override
      public boolean isMutable() {
        return true;
      }
    }

    private static final class Immutable extends Find_Feature2Feature.Match {
      Immutable(final FeatureInstance pFeatinst) {
        super(pFeatinst);
      }

      @Override
      public boolean isMutable() {
        return false;
      }
    }
  }

  /**
   * Generated pattern matcher API of the fr.mem4csd.osatedim.viatra.queries.find_Feature2Feature pattern,
   * providing pattern-specific query methods.
   * 
   * <p>Use the pattern matcher on a given model via {@link #on(ViatraQueryEngine)},
   * e.g. in conjunction with {@link ViatraQueryEngine#on(QueryScope)}.
   * 
   * <p>Matches of the pattern will be represented as {@link Match}.
   * 
   * <p>Original source:
   * <code><pre>
   * pattern find_Feature2Feature(featinst : FeatureInstance)
   * {
   * 	FeatureInstance(featinst);
   * }
   * </pre></code>
   * 
   * @see Match
   * @see Find_Feature2Feature
   * 
   */
  public static class Matcher extends BaseMatcher<Find_Feature2Feature.Match> {
    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    public static Find_Feature2Feature.Matcher on(final ViatraQueryEngine engine) {
      // check if matcher already exists
      Matcher matcher = engine.getExistingMatcher(querySpecification());
      if (matcher == null) {
          matcher = (Matcher)engine.getMatcher(querySpecification());
      }
      return matcher;
    }

    /**
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * @return an initialized matcher
     * @noreference This method is for internal matcher initialization by the framework, do not call it manually.
     * 
     */
    public static Find_Feature2Feature.Matcher create() {
      return new Matcher();
    }

    private static final int POSITION_FEATINST = 0;

    private static final Logger LOGGER = ViatraQueryLoggingUtil.getLogger(Find_Feature2Feature.Matcher.class);

    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    private Matcher() {
      super(querySpecification());
    }

    /**
     * Returns the set of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pFeatinst the fixed value of pattern parameter featinst, or null if not bound.
     * @return matches represented as a Match object.
     * 
     */
    public Collection<Find_Feature2Feature.Match> getAllMatches(final FeatureInstance pFeatinst) {
      return rawStreamAllMatches(new Object[]{pFeatinst}).collect(Collectors.toSet());
    }

    /**
     * Returns a stream of all matches of the pattern that conform to the given fixed values of some parameters.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     * @param pFeatinst the fixed value of pattern parameter featinst, or null if not bound.
     * @return a stream of matches represented as a Match object.
     * 
     */
    public Stream<Find_Feature2Feature.Match> streamAllMatches(final FeatureInstance pFeatinst) {
      return rawStreamAllMatches(new Object[]{pFeatinst});
    }

    /**
     * Returns an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pFeatinst the fixed value of pattern parameter featinst, or null if not bound.
     * @return a match represented as a Match object, or null if no match is found.
     * 
     */
    public Optional<Find_Feature2Feature.Match> getOneArbitraryMatch(final FeatureInstance pFeatinst) {
      return rawGetOneArbitraryMatch(new Object[]{pFeatinst});
    }

    /**
     * Indicates whether the given combination of specified pattern parameters constitute a valid pattern match,
     * under any possible substitution of the unspecified parameters (if any).
     * @param pFeatinst the fixed value of pattern parameter featinst, or null if not bound.
     * @return true if the input is a valid (partial) match of the pattern.
     * 
     */
    public boolean hasMatch(final FeatureInstance pFeatinst) {
      return rawHasMatch(new Object[]{pFeatinst});
    }

    /**
     * Returns the number of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pFeatinst the fixed value of pattern parameter featinst, or null if not bound.
     * @return the number of pattern matches found.
     * 
     */
    public int countMatches(final FeatureInstance pFeatinst) {
      return rawCountMatches(new Object[]{pFeatinst});
    }

    /**
     * Executes the given processor on an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pFeatinst the fixed value of pattern parameter featinst, or null if not bound.
     * @param processor the action that will process the selected match.
     * @return true if the pattern has at least one match with the given parameter values, false if the processor was not invoked
     * 
     */
    public boolean forOneArbitraryMatch(final FeatureInstance pFeatinst, final Consumer<? super Find_Feature2Feature.Match> processor) {
      return rawForOneArbitraryMatch(new Object[]{pFeatinst}, processor);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pFeatinst the fixed value of pattern parameter featinst, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public Find_Feature2Feature.Match newMatch(final FeatureInstance pFeatinst) {
      return Find_Feature2Feature.Match.newMatch(pFeatinst);
    }

    /**
     * Retrieve the set of values that occur in matches for featinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<FeatureInstance> rawStreamAllValuesOffeatinst(final Object[] parameters) {
      return rawStreamAllValues(POSITION_FEATINST, parameters).map(FeatureInstance.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for featinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<FeatureInstance> getAllValuesOffeatinst() {
      return rawStreamAllValuesOffeatinst(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for featinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<FeatureInstance> streamAllValuesOffeatinst() {
      return rawStreamAllValuesOffeatinst(emptyArray());
    }

    @Override
    protected Find_Feature2Feature.Match tupleToMatch(final Tuple t) {
      try {
          return Find_Feature2Feature.Match.newMatch((FeatureInstance) t.get(POSITION_FEATINST));
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in tuple not properly typed!",e);
          return null;
      }
    }

    @Override
    protected Find_Feature2Feature.Match arrayToMatch(final Object[] match) {
      try {
          return Find_Feature2Feature.Match.newMatch((FeatureInstance) match[POSITION_FEATINST]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    @Override
    protected Find_Feature2Feature.Match arrayToMatchMutable(final Object[] match) {
      try {
          return Find_Feature2Feature.Match.newMutableMatch((FeatureInstance) match[POSITION_FEATINST]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    /**
     * @return the singleton instance of the query specification of this pattern
     * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
     * 
     */
    public static IQuerySpecification<Find_Feature2Feature.Matcher> querySpecification() {
      return Find_Feature2Feature.instance();
    }
  }

  private Find_Feature2Feature() {
    super(GeneratedPQuery.INSTANCE);
  }

  /**
   * @return the singleton instance of the query specification
   * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
   * 
   */
  public static Find_Feature2Feature instance() {
    try{
        return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
        throw processInitializerError(err);
    }
  }

  @Override
  protected Find_Feature2Feature.Matcher instantiate(final ViatraQueryEngine engine) {
    return Find_Feature2Feature.Matcher.on(engine);
  }

  @Override
  public Find_Feature2Feature.Matcher instantiate() {
    return Find_Feature2Feature.Matcher.create();
  }

  @Override
  public Find_Feature2Feature.Match newEmptyMatch() {
    return Find_Feature2Feature.Match.newEmptyMatch();
  }

  @Override
  public Find_Feature2Feature.Match newMatch(final Object... parameters) {
    return Find_Feature2Feature.Match.newMatch((org.osate.aadl2.instance.FeatureInstance) parameters[0]);
  }

  /**
   * Inner class allowing the singleton instance of {@link Find_Feature2Feature} to be created 
   *     <b>not</b> at the class load time of the outer class, 
   *     but rather at the first call to {@link Find_Feature2Feature#instance()}.
   * 
   * <p> This workaround is required e.g. to support recursion.
   * 
   */
  private static class LazyHolder {
    private static final Find_Feature2Feature INSTANCE = new Find_Feature2Feature();

    /**
     * Statically initializes the query specification <b>after</b> the field {@link #INSTANCE} is assigned.
     * This initialization order is required to support indirect recursion.
     * 
     * <p> The static initializer is defined using a helper field to work around limitations of the code generator.
     * 
     */
    private static final Object STATIC_INITIALIZER = ensureInitialized();

    public static Object ensureInitialized() {
      INSTANCE.ensureInitializedInternal();
      return null;
    }
  }

  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private static final Find_Feature2Feature.GeneratedPQuery INSTANCE = new GeneratedPQuery();

    private final PParameter parameter_featinst = new PParameter("featinst", "org.osate.aadl2.instance.FeatureInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "FeatureInstance")), PParameterDirection.INOUT);

    private final List<PParameter> parameters = Arrays.asList(parameter_featinst);

    private GeneratedPQuery() {
      super(PVisibility.PUBLIC);
    }

    @Override
    public String getFullyQualifiedName() {
      return "fr.mem4csd.osatedim.viatra.queries.find_Feature2Feature";
    }

    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("featinst");
    }

    @Override
    public List<PParameter> getParameters() {
      return parameters;
    }

    @Override
    public Set<PBody> doGetContainedBodies() {
      setEvaluationHints(new QueryEvaluationHint(null, QueryEvaluationHint.BackendRequirement.UNSPECIFIED));
      Set<PBody> bodies = new LinkedHashSet<>();
      {
          PBody body = new PBody(this);
          PVariable var_featinst = body.getOrCreateVariableByName("featinst");
          new TypeConstraint(body, Tuples.flatTupleOf(var_featinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "FeatureInstance")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_featinst, parameter_featinst)
          ));
          // 	FeatureInstance(featinst)
          new TypeConstraint(body, Tuples.flatTupleOf(var_featinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "FeatureInstance")));
          bodies.add(body);
      }
      return bodies;
    }
  }
}
