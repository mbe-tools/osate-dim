/**
 * Generated from platform:/resource/fr.mem4csd.osatedim/src/fr/mem4csd/osatedim/viatra/queries/DIMQueriesOutplace.vql
 */
package fr.mem4csd.osatedim.viatra.queries;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.viatra.query.runtime.api.IPatternMatch;
import org.eclipse.viatra.query.runtime.api.IQuerySpecification;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.viatra.query.runtime.api.impl.BaseMatcher;
import org.eclipse.viatra.query.runtime.api.impl.BasePatternMatch;
import org.eclipse.viatra.query.runtime.emf.types.EClassTransitiveInstancesKey;
import org.eclipse.viatra.query.runtime.emf.types.EStructuralFeatureInstancesKey;
import org.eclipse.viatra.query.runtime.matchers.backend.QueryEvaluationHint;
import org.eclipse.viatra.query.runtime.matchers.psystem.PBody;
import org.eclipse.viatra.query.runtime.matchers.psystem.PVariable;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.Equality;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.TypeConstraint;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameterDirection;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PVisibility;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuple;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuples;
import org.eclipse.viatra.query.runtime.util.ViatraQueryLoggingUtil;
import org.osate.aadl2.instance.ComponentInstance;

/**
 * A pattern-specific query specification that can instantiate Matcher in a type-safe way.
 * 
 * <p>Original source:
 *         <code><pre>
 *         // This pattern finds the component and its subcomponent on the original instance side 
 *         pattern is_subcomponent(component: ComponentInstance,
 *         					    subcomponent : ComponentInstance)
 *         {
 *         	ComponentInstance.componentInstance(component,subcomponent);	
 *         }
 * </pre></code>
 * 
 * @see Matcher
 * @see Match
 * 
 */
@SuppressWarnings("all")
public final class Is_subcomponent extends BaseGeneratedEMFQuerySpecification<Is_subcomponent.Matcher> {
  /**
   * Pattern-specific match representation of the fr.mem4csd.osatedim.viatra.queries.is_subcomponent pattern,
   * to be used in conjunction with {@link Matcher}.
   * 
   * <p>Class fields correspond to parameters of the pattern. Fields with value null are considered unassigned.
   * Each instance is a (possibly partial) substitution of pattern parameters,
   * usable to represent a match of the pattern in the result of a query,
   * or to specify the bound (fixed) input parameters when issuing a query.
   * 
   * @see Matcher
   * 
   */
  public static abstract class Match extends BasePatternMatch {
    private ComponentInstance fComponent;

    private ComponentInstance fSubcomponent;

    private static List<String> parameterNames = makeImmutableList("component", "subcomponent");

    private Match(final ComponentInstance pComponent, final ComponentInstance pSubcomponent) {
      this.fComponent = pComponent;
      this.fSubcomponent = pSubcomponent;
    }

    @Override
    public Object get(final String parameterName) {
      switch(parameterName) {
          case "component": return this.fComponent;
          case "subcomponent": return this.fSubcomponent;
          default: return null;
      }
    }

    @Override
    public Object get(final int index) {
      switch(index) {
          case 0: return this.fComponent;
          case 1: return this.fSubcomponent;
          default: return null;
      }
    }

    public ComponentInstance getComponent() {
      return this.fComponent;
    }

    public ComponentInstance getSubcomponent() {
      return this.fSubcomponent;
    }

    @Override
    public boolean set(final String parameterName, final Object newValue) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      if ("component".equals(parameterName) ) {
          this.fComponent = (ComponentInstance) newValue;
          return true;
      }
      if ("subcomponent".equals(parameterName) ) {
          this.fSubcomponent = (ComponentInstance) newValue;
          return true;
      }
      return false;
    }

    public void setComponent(final ComponentInstance pComponent) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fComponent = pComponent;
    }

    public void setSubcomponent(final ComponentInstance pSubcomponent) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fSubcomponent = pSubcomponent;
    }

    @Override
    public String patternName() {
      return "fr.mem4csd.osatedim.viatra.queries.is_subcomponent";
    }

    @Override
    public List<String> parameterNames() {
      return Is_subcomponent.Match.parameterNames;
    }

    @Override
    public Object[] toArray() {
      return new Object[]{fComponent, fSubcomponent};
    }

    @Override
    public Is_subcomponent.Match toImmutable() {
      return isMutable() ? newMatch(fComponent, fSubcomponent) : this;
    }

    @Override
    public String prettyPrint() {
      StringBuilder result = new StringBuilder();
      result.append("\"component\"=" + prettyPrintValue(fComponent) + ", ");
      result.append("\"subcomponent\"=" + prettyPrintValue(fSubcomponent));
      return result.toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(fComponent, fSubcomponent);
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj)
          return true;
      if (obj == null) {
          return false;
      }
      if ((obj instanceof Is_subcomponent.Match)) {
          Is_subcomponent.Match other = (Is_subcomponent.Match) obj;
          return Objects.equals(fComponent, other.fComponent) && Objects.equals(fSubcomponent, other.fSubcomponent);
      } else {
          // this should be infrequent
          if (!(obj instanceof IPatternMatch)) {
              return false;
          }
          IPatternMatch otherSig  = (IPatternMatch) obj;
          return Objects.equals(specification(), otherSig.specification()) && Arrays.deepEquals(toArray(), otherSig.toArray());
      }
    }

    @Override
    public Is_subcomponent specification() {
      return Is_subcomponent.instance();
    }

    /**
     * Returns an empty, mutable match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @return the empty match.
     * 
     */
    public static Is_subcomponent.Match newEmptyMatch() {
      return new Mutable(null, null);
    }

    /**
     * Returns a mutable (partial) match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @param pComponent the fixed value of pattern parameter component, or null if not bound.
     * @param pSubcomponent the fixed value of pattern parameter subcomponent, or null if not bound.
     * @return the new, mutable (partial) match object.
     * 
     */
    public static Is_subcomponent.Match newMutableMatch(final ComponentInstance pComponent, final ComponentInstance pSubcomponent) {
      return new Mutable(pComponent, pSubcomponent);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pComponent the fixed value of pattern parameter component, or null if not bound.
     * @param pSubcomponent the fixed value of pattern parameter subcomponent, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public static Is_subcomponent.Match newMatch(final ComponentInstance pComponent, final ComponentInstance pSubcomponent) {
      return new Immutable(pComponent, pSubcomponent);
    }

    private static final class Mutable extends Is_subcomponent.Match {
      Mutable(final ComponentInstance pComponent, final ComponentInstance pSubcomponent) {
        super(pComponent, pSubcomponent);
      }

      @Override
      public boolean isMutable() {
        return true;
      }
    }

    private static final class Immutable extends Is_subcomponent.Match {
      Immutable(final ComponentInstance pComponent, final ComponentInstance pSubcomponent) {
        super(pComponent, pSubcomponent);
      }

      @Override
      public boolean isMutable() {
        return false;
      }
    }
  }

  /**
   * Generated pattern matcher API of the fr.mem4csd.osatedim.viatra.queries.is_subcomponent pattern,
   * providing pattern-specific query methods.
   * 
   * <p>Use the pattern matcher on a given model via {@link #on(ViatraQueryEngine)},
   * e.g. in conjunction with {@link ViatraQueryEngine#on(QueryScope)}.
   * 
   * <p>Matches of the pattern will be represented as {@link Match}.
   * 
   * <p>Original source:
   * <code><pre>
   * // This pattern finds the component and its subcomponent on the original instance side 
   * pattern is_subcomponent(component: ComponentInstance,
   * 					    subcomponent : ComponentInstance)
   * {
   * 	ComponentInstance.componentInstance(component,subcomponent);	
   * }
   * </pre></code>
   * 
   * @see Match
   * @see Is_subcomponent
   * 
   */
  public static class Matcher extends BaseMatcher<Is_subcomponent.Match> {
    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    public static Is_subcomponent.Matcher on(final ViatraQueryEngine engine) {
      // check if matcher already exists
      Matcher matcher = engine.getExistingMatcher(querySpecification());
      if (matcher == null) {
          matcher = (Matcher)engine.getMatcher(querySpecification());
      }
      return matcher;
    }

    /**
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * @return an initialized matcher
     * @noreference This method is for internal matcher initialization by the framework, do not call it manually.
     * 
     */
    public static Is_subcomponent.Matcher create() {
      return new Matcher();
    }

    private static final int POSITION_COMPONENT = 0;

    private static final int POSITION_SUBCOMPONENT = 1;

    private static final Logger LOGGER = ViatraQueryLoggingUtil.getLogger(Is_subcomponent.Matcher.class);

    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    private Matcher() {
      super(querySpecification());
    }

    /**
     * Returns the set of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pComponent the fixed value of pattern parameter component, or null if not bound.
     * @param pSubcomponent the fixed value of pattern parameter subcomponent, or null if not bound.
     * @return matches represented as a Match object.
     * 
     */
    public Collection<Is_subcomponent.Match> getAllMatches(final ComponentInstance pComponent, final ComponentInstance pSubcomponent) {
      return rawStreamAllMatches(new Object[]{pComponent, pSubcomponent}).collect(Collectors.toSet());
    }

    /**
     * Returns a stream of all matches of the pattern that conform to the given fixed values of some parameters.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     * @param pComponent the fixed value of pattern parameter component, or null if not bound.
     * @param pSubcomponent the fixed value of pattern parameter subcomponent, or null if not bound.
     * @return a stream of matches represented as a Match object.
     * 
     */
    public Stream<Is_subcomponent.Match> streamAllMatches(final ComponentInstance pComponent, final ComponentInstance pSubcomponent) {
      return rawStreamAllMatches(new Object[]{pComponent, pSubcomponent});
    }

    /**
     * Returns an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pComponent the fixed value of pattern parameter component, or null if not bound.
     * @param pSubcomponent the fixed value of pattern parameter subcomponent, or null if not bound.
     * @return a match represented as a Match object, or null if no match is found.
     * 
     */
    public Optional<Is_subcomponent.Match> getOneArbitraryMatch(final ComponentInstance pComponent, final ComponentInstance pSubcomponent) {
      return rawGetOneArbitraryMatch(new Object[]{pComponent, pSubcomponent});
    }

    /**
     * Indicates whether the given combination of specified pattern parameters constitute a valid pattern match,
     * under any possible substitution of the unspecified parameters (if any).
     * @param pComponent the fixed value of pattern parameter component, or null if not bound.
     * @param pSubcomponent the fixed value of pattern parameter subcomponent, or null if not bound.
     * @return true if the input is a valid (partial) match of the pattern.
     * 
     */
    public boolean hasMatch(final ComponentInstance pComponent, final ComponentInstance pSubcomponent) {
      return rawHasMatch(new Object[]{pComponent, pSubcomponent});
    }

    /**
     * Returns the number of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pComponent the fixed value of pattern parameter component, or null if not bound.
     * @param pSubcomponent the fixed value of pattern parameter subcomponent, or null if not bound.
     * @return the number of pattern matches found.
     * 
     */
    public int countMatches(final ComponentInstance pComponent, final ComponentInstance pSubcomponent) {
      return rawCountMatches(new Object[]{pComponent, pSubcomponent});
    }

    /**
     * Executes the given processor on an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pComponent the fixed value of pattern parameter component, or null if not bound.
     * @param pSubcomponent the fixed value of pattern parameter subcomponent, or null if not bound.
     * @param processor the action that will process the selected match.
     * @return true if the pattern has at least one match with the given parameter values, false if the processor was not invoked
     * 
     */
    public boolean forOneArbitraryMatch(final ComponentInstance pComponent, final ComponentInstance pSubcomponent, final Consumer<? super Is_subcomponent.Match> processor) {
      return rawForOneArbitraryMatch(new Object[]{pComponent, pSubcomponent}, processor);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pComponent the fixed value of pattern parameter component, or null if not bound.
     * @param pSubcomponent the fixed value of pattern parameter subcomponent, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public Is_subcomponent.Match newMatch(final ComponentInstance pComponent, final ComponentInstance pSubcomponent) {
      return Is_subcomponent.Match.newMatch(pComponent, pSubcomponent);
    }

    /**
     * Retrieve the set of values that occur in matches for component.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ComponentInstance> rawStreamAllValuesOfcomponent(final Object[] parameters) {
      return rawStreamAllValues(POSITION_COMPONENT, parameters).map(ComponentInstance.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for component.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcomponent() {
      return rawStreamAllValuesOfcomponent(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for component.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcomponent() {
      return rawStreamAllValuesOfcomponent(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for component.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcomponent(final Is_subcomponent.Match partialMatch) {
      return rawStreamAllValuesOfcomponent(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for component.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcomponent(final ComponentInstance pSubcomponent) {
      return rawStreamAllValuesOfcomponent(new Object[]{null, pSubcomponent});
    }

    /**
     * Retrieve the set of values that occur in matches for component.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcomponent(final Is_subcomponent.Match partialMatch) {
      return rawStreamAllValuesOfcomponent(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for component.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcomponent(final ComponentInstance pSubcomponent) {
      return rawStreamAllValuesOfcomponent(new Object[]{null, pSubcomponent}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for subcomponent.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ComponentInstance> rawStreamAllValuesOfsubcomponent(final Object[] parameters) {
      return rawStreamAllValues(POSITION_SUBCOMPONENT, parameters).map(ComponentInstance.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for subcomponent.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfsubcomponent() {
      return rawStreamAllValuesOfsubcomponent(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for subcomponent.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfsubcomponent() {
      return rawStreamAllValuesOfsubcomponent(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for subcomponent.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfsubcomponent(final Is_subcomponent.Match partialMatch) {
      return rawStreamAllValuesOfsubcomponent(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for subcomponent.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfsubcomponent(final ComponentInstance pComponent) {
      return rawStreamAllValuesOfsubcomponent(new Object[]{pComponent, null});
    }

    /**
     * Retrieve the set of values that occur in matches for subcomponent.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfsubcomponent(final Is_subcomponent.Match partialMatch) {
      return rawStreamAllValuesOfsubcomponent(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for subcomponent.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfsubcomponent(final ComponentInstance pComponent) {
      return rawStreamAllValuesOfsubcomponent(new Object[]{pComponent, null}).collect(Collectors.toSet());
    }

    @Override
    protected Is_subcomponent.Match tupleToMatch(final Tuple t) {
      try {
          return Is_subcomponent.Match.newMatch((ComponentInstance) t.get(POSITION_COMPONENT), (ComponentInstance) t.get(POSITION_SUBCOMPONENT));
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in tuple not properly typed!",e);
          return null;
      }
    }

    @Override
    protected Is_subcomponent.Match arrayToMatch(final Object[] match) {
      try {
          return Is_subcomponent.Match.newMatch((ComponentInstance) match[POSITION_COMPONENT], (ComponentInstance) match[POSITION_SUBCOMPONENT]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    @Override
    protected Is_subcomponent.Match arrayToMatchMutable(final Object[] match) {
      try {
          return Is_subcomponent.Match.newMutableMatch((ComponentInstance) match[POSITION_COMPONENT], (ComponentInstance) match[POSITION_SUBCOMPONENT]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    /**
     * @return the singleton instance of the query specification of this pattern
     * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
     * 
     */
    public static IQuerySpecification<Is_subcomponent.Matcher> querySpecification() {
      return Is_subcomponent.instance();
    }
  }

  private Is_subcomponent() {
    super(GeneratedPQuery.INSTANCE);
  }

  /**
   * @return the singleton instance of the query specification
   * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
   * 
   */
  public static Is_subcomponent instance() {
    try{
        return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
        throw processInitializerError(err);
    }
  }

  @Override
  protected Is_subcomponent.Matcher instantiate(final ViatraQueryEngine engine) {
    return Is_subcomponent.Matcher.on(engine);
  }

  @Override
  public Is_subcomponent.Matcher instantiate() {
    return Is_subcomponent.Matcher.create();
  }

  @Override
  public Is_subcomponent.Match newEmptyMatch() {
    return Is_subcomponent.Match.newEmptyMatch();
  }

  @Override
  public Is_subcomponent.Match newMatch(final Object... parameters) {
    return Is_subcomponent.Match.newMatch((org.osate.aadl2.instance.ComponentInstance) parameters[0], (org.osate.aadl2.instance.ComponentInstance) parameters[1]);
  }

  /**
   * Inner class allowing the singleton instance of {@link Is_subcomponent} to be created 
   *     <b>not</b> at the class load time of the outer class, 
   *     but rather at the first call to {@link Is_subcomponent#instance()}.
   * 
   * <p> This workaround is required e.g. to support recursion.
   * 
   */
  private static class LazyHolder {
    private static final Is_subcomponent INSTANCE = new Is_subcomponent();

    /**
     * Statically initializes the query specification <b>after</b> the field {@link #INSTANCE} is assigned.
     * This initialization order is required to support indirect recursion.
     * 
     * <p> The static initializer is defined using a helper field to work around limitations of the code generator.
     * 
     */
    private static final Object STATIC_INITIALIZER = ensureInitialized();

    public static Object ensureInitialized() {
      INSTANCE.ensureInitializedInternal();
      return null;
    }
  }

  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private static final Is_subcomponent.GeneratedPQuery INSTANCE = new GeneratedPQuery();

    private final PParameter parameter_component = new PParameter("component", "org.osate.aadl2.instance.ComponentInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ComponentInstance")), PParameterDirection.INOUT);

    private final PParameter parameter_subcomponent = new PParameter("subcomponent", "org.osate.aadl2.instance.ComponentInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ComponentInstance")), PParameterDirection.INOUT);

    private final List<PParameter> parameters = Arrays.asList(parameter_component, parameter_subcomponent);

    private GeneratedPQuery() {
      super(PVisibility.PUBLIC);
    }

    @Override
    public String getFullyQualifiedName() {
      return "fr.mem4csd.osatedim.viatra.queries.is_subcomponent";
    }

    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("component","subcomponent");
    }

    @Override
    public List<PParameter> getParameters() {
      return parameters;
    }

    @Override
    public Set<PBody> doGetContainedBodies() {
      setEvaluationHints(new QueryEvaluationHint(null, QueryEvaluationHint.BackendRequirement.UNSPECIFIED));
      Set<PBody> bodies = new LinkedHashSet<>();
      {
          PBody body = new PBody(this);
          PVariable var_component = body.getOrCreateVariableByName("component");
          PVariable var_subcomponent = body.getOrCreateVariableByName("subcomponent");
          new TypeConstraint(body, Tuples.flatTupleOf(var_component), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_subcomponent), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_component, parameter_component),
             new ExportedParameter(body, var_subcomponent, parameter_subcomponent)
          ));
          // 	ComponentInstance.componentInstance(component,subcomponent)
          new TypeConstraint(body, Tuples.flatTupleOf(var_component), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_component, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance", "componentInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          new Equality(body, var__virtual_0_, var_subcomponent);
          bodies.add(body);
      }
      return bodies;
    }
  }
}
