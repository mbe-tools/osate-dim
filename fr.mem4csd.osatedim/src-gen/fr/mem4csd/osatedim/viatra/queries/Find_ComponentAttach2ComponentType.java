/**
 * Generated from platform:/resource/fr.mem4csd.osatedim/src/fr/mem4csd/osatedim/viatra/queries/DIMQueriesOutplace.vql
 */
package fr.mem4csd.osatedim.viatra.queries;

import fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.viatra.query.runtime.api.IPatternMatch;
import org.eclipse.viatra.query.runtime.api.IQuerySpecification;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.viatra.query.runtime.api.impl.BaseMatcher;
import org.eclipse.viatra.query.runtime.api.impl.BasePatternMatch;
import org.eclipse.viatra.query.runtime.emf.types.EClassTransitiveInstancesKey;
import org.eclipse.viatra.query.runtime.emf.types.EStructuralFeatureInstancesKey;
import org.eclipse.viatra.query.runtime.matchers.backend.QueryEvaluationHint;
import org.eclipse.viatra.query.runtime.matchers.psystem.PBody;
import org.eclipse.viatra.query.runtime.matchers.psystem.PVariable;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.Equality;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.PositivePatternCall;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.TypeConstraint;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameterDirection;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PVisibility;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuple;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuples;
import org.eclipse.viatra.query.runtime.util.ViatraQueryLoggingUtil;
import org.osate.aadl2.ComponentType;
import org.osate.aadl2.PublicPackageSection;
import org.osate.aadl2.Subcomponent;
import org.osate.aadl2.instance.ComponentInstance;

/**
 * A pattern-specific query specification that can instantiate Matcher in a type-safe way.
 * 
 * <p>Original source:
 *         <code><pre>
 *         // This pattern finds the component2Attach for a specific component which has a type definition classifier
 *         pattern find_ComponentAttach2ComponentType(compinst : ComponentInstance,
 *         								  	  	   trace : Aaxl2AaxlTrace,
 *         								           compinst2add : ComponentInstance,
 *         								           comptype : ComponentType,
 *         								           pack : PublicPackageSection,
 *         								           comp : Subcomponent)
 *         {
 *         	find is_in_trace(_,trace,compinst,_);
 *         	Aaxl2AaxlTrace.objectsToAttach(trace,compinst2add);
 *         	ComponentInstance.classifier(compinst,comptype);
 *         	ComponentInstance.subcomponent(compinst,comp);
 *         	PublicPackageSection.ownedClassifier(pack,comptype);
 *         }
 * </pre></code>
 * 
 * @see Matcher
 * @see Match
 * 
 */
@SuppressWarnings("all")
public final class Find_ComponentAttach2ComponentType extends BaseGeneratedEMFQuerySpecification<Find_ComponentAttach2ComponentType.Matcher> {
  /**
   * Pattern-specific match representation of the fr.mem4csd.osatedim.viatra.queries.find_ComponentAttach2ComponentType pattern,
   * to be used in conjunction with {@link Matcher}.
   * 
   * <p>Class fields correspond to parameters of the pattern. Fields with value null are considered unassigned.
   * Each instance is a (possibly partial) substitution of pattern parameters,
   * usable to represent a match of the pattern in the result of a query,
   * or to specify the bound (fixed) input parameters when issuing a query.
   * 
   * @see Matcher
   * 
   */
  public static abstract class Match extends BasePatternMatch {
    private ComponentInstance fCompinst;

    private Aaxl2AaxlTrace fTrace;

    private ComponentInstance fCompinst2add;

    private ComponentType fComptype;

    private PublicPackageSection fPack;

    private Subcomponent fComp;

    private static List<String> parameterNames = makeImmutableList("compinst", "trace", "compinst2add", "comptype", "pack", "comp");

    private Match(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentType pComptype, final PublicPackageSection pPack, final Subcomponent pComp) {
      this.fCompinst = pCompinst;
      this.fTrace = pTrace;
      this.fCompinst2add = pCompinst2add;
      this.fComptype = pComptype;
      this.fPack = pPack;
      this.fComp = pComp;
    }

    @Override
    public Object get(final String parameterName) {
      switch(parameterName) {
          case "compinst": return this.fCompinst;
          case "trace": return this.fTrace;
          case "compinst2add": return this.fCompinst2add;
          case "comptype": return this.fComptype;
          case "pack": return this.fPack;
          case "comp": return this.fComp;
          default: return null;
      }
    }

    @Override
    public Object get(final int index) {
      switch(index) {
          case 0: return this.fCompinst;
          case 1: return this.fTrace;
          case 2: return this.fCompinst2add;
          case 3: return this.fComptype;
          case 4: return this.fPack;
          case 5: return this.fComp;
          default: return null;
      }
    }

    public ComponentInstance getCompinst() {
      return this.fCompinst;
    }

    public Aaxl2AaxlTrace getTrace() {
      return this.fTrace;
    }

    public ComponentInstance getCompinst2add() {
      return this.fCompinst2add;
    }

    public ComponentType getComptype() {
      return this.fComptype;
    }

    public PublicPackageSection getPack() {
      return this.fPack;
    }

    public Subcomponent getComp() {
      return this.fComp;
    }

    @Override
    public boolean set(final String parameterName, final Object newValue) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      if ("compinst".equals(parameterName) ) {
          this.fCompinst = (ComponentInstance) newValue;
          return true;
      }
      if ("trace".equals(parameterName) ) {
          this.fTrace = (Aaxl2AaxlTrace) newValue;
          return true;
      }
      if ("compinst2add".equals(parameterName) ) {
          this.fCompinst2add = (ComponentInstance) newValue;
          return true;
      }
      if ("comptype".equals(parameterName) ) {
          this.fComptype = (ComponentType) newValue;
          return true;
      }
      if ("pack".equals(parameterName) ) {
          this.fPack = (PublicPackageSection) newValue;
          return true;
      }
      if ("comp".equals(parameterName) ) {
          this.fComp = (Subcomponent) newValue;
          return true;
      }
      return false;
    }

    public void setCompinst(final ComponentInstance pCompinst) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fCompinst = pCompinst;
    }

    public void setTrace(final Aaxl2AaxlTrace pTrace) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fTrace = pTrace;
    }

    public void setCompinst2add(final ComponentInstance pCompinst2add) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fCompinst2add = pCompinst2add;
    }

    public void setComptype(final ComponentType pComptype) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fComptype = pComptype;
    }

    public void setPack(final PublicPackageSection pPack) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fPack = pPack;
    }

    public void setComp(final Subcomponent pComp) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fComp = pComp;
    }

    @Override
    public String patternName() {
      return "fr.mem4csd.osatedim.viatra.queries.find_ComponentAttach2ComponentType";
    }

    @Override
    public List<String> parameterNames() {
      return Find_ComponentAttach2ComponentType.Match.parameterNames;
    }

    @Override
    public Object[] toArray() {
      return new Object[]{fCompinst, fTrace, fCompinst2add, fComptype, fPack, fComp};
    }

    @Override
    public Find_ComponentAttach2ComponentType.Match toImmutable() {
      return isMutable() ? newMatch(fCompinst, fTrace, fCompinst2add, fComptype, fPack, fComp) : this;
    }

    @Override
    public String prettyPrint() {
      StringBuilder result = new StringBuilder();
      result.append("\"compinst\"=" + prettyPrintValue(fCompinst) + ", ");
      result.append("\"trace\"=" + prettyPrintValue(fTrace) + ", ");
      result.append("\"compinst2add\"=" + prettyPrintValue(fCompinst2add) + ", ");
      result.append("\"comptype\"=" + prettyPrintValue(fComptype) + ", ");
      result.append("\"pack\"=" + prettyPrintValue(fPack) + ", ");
      result.append("\"comp\"=" + prettyPrintValue(fComp));
      return result.toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(fCompinst, fTrace, fCompinst2add, fComptype, fPack, fComp);
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj)
          return true;
      if (obj == null) {
          return false;
      }
      if ((obj instanceof Find_ComponentAttach2ComponentType.Match)) {
          Find_ComponentAttach2ComponentType.Match other = (Find_ComponentAttach2ComponentType.Match) obj;
          return Objects.equals(fCompinst, other.fCompinst) && Objects.equals(fTrace, other.fTrace) && Objects.equals(fCompinst2add, other.fCompinst2add) && Objects.equals(fComptype, other.fComptype) && Objects.equals(fPack, other.fPack) && Objects.equals(fComp, other.fComp);
      } else {
          // this should be infrequent
          if (!(obj instanceof IPatternMatch)) {
              return false;
          }
          IPatternMatch otherSig  = (IPatternMatch) obj;
          return Objects.equals(specification(), otherSig.specification()) && Arrays.deepEquals(toArray(), otherSig.toArray());
      }
    }

    @Override
    public Find_ComponentAttach2ComponentType specification() {
      return Find_ComponentAttach2ComponentType.instance();
    }

    /**
     * Returns an empty, mutable match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @return the empty match.
     * 
     */
    public static Find_ComponentAttach2ComponentType.Match newEmptyMatch() {
      return new Mutable(null, null, null, null, null, null);
    }

    /**
     * Returns a mutable (partial) match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pCompinst2add the fixed value of pattern parameter compinst2add, or null if not bound.
     * @param pComptype the fixed value of pattern parameter comptype, or null if not bound.
     * @param pPack the fixed value of pattern parameter pack, or null if not bound.
     * @param pComp the fixed value of pattern parameter comp, or null if not bound.
     * @return the new, mutable (partial) match object.
     * 
     */
    public static Find_ComponentAttach2ComponentType.Match newMutableMatch(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentType pComptype, final PublicPackageSection pPack, final Subcomponent pComp) {
      return new Mutable(pCompinst, pTrace, pCompinst2add, pComptype, pPack, pComp);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pCompinst2add the fixed value of pattern parameter compinst2add, or null if not bound.
     * @param pComptype the fixed value of pattern parameter comptype, or null if not bound.
     * @param pPack the fixed value of pattern parameter pack, or null if not bound.
     * @param pComp the fixed value of pattern parameter comp, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public static Find_ComponentAttach2ComponentType.Match newMatch(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentType pComptype, final PublicPackageSection pPack, final Subcomponent pComp) {
      return new Immutable(pCompinst, pTrace, pCompinst2add, pComptype, pPack, pComp);
    }

    private static final class Mutable extends Find_ComponentAttach2ComponentType.Match {
      Mutable(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentType pComptype, final PublicPackageSection pPack, final Subcomponent pComp) {
        super(pCompinst, pTrace, pCompinst2add, pComptype, pPack, pComp);
      }

      @Override
      public boolean isMutable() {
        return true;
      }
    }

    private static final class Immutable extends Find_ComponentAttach2ComponentType.Match {
      Immutable(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentType pComptype, final PublicPackageSection pPack, final Subcomponent pComp) {
        super(pCompinst, pTrace, pCompinst2add, pComptype, pPack, pComp);
      }

      @Override
      public boolean isMutable() {
        return false;
      }
    }
  }

  /**
   * Generated pattern matcher API of the fr.mem4csd.osatedim.viatra.queries.find_ComponentAttach2ComponentType pattern,
   * providing pattern-specific query methods.
   * 
   * <p>Use the pattern matcher on a given model via {@link #on(ViatraQueryEngine)},
   * e.g. in conjunction with {@link ViatraQueryEngine#on(QueryScope)}.
   * 
   * <p>Matches of the pattern will be represented as {@link Match}.
   * 
   * <p>Original source:
   * <code><pre>
   * // This pattern finds the component2Attach for a specific component which has a type definition classifier
   * pattern find_ComponentAttach2ComponentType(compinst : ComponentInstance,
   * 								  	  	   trace : Aaxl2AaxlTrace,
   * 								           compinst2add : ComponentInstance,
   * 								           comptype : ComponentType,
   * 								           pack : PublicPackageSection,
   * 								           comp : Subcomponent)
   * {
   * 	find is_in_trace(_,trace,compinst,_);
   * 	Aaxl2AaxlTrace.objectsToAttach(trace,compinst2add);
   * 	ComponentInstance.classifier(compinst,comptype);
   * 	ComponentInstance.subcomponent(compinst,comp);
   * 	PublicPackageSection.ownedClassifier(pack,comptype);
   * }
   * </pre></code>
   * 
   * @see Match
   * @see Find_ComponentAttach2ComponentType
   * 
   */
  public static class Matcher extends BaseMatcher<Find_ComponentAttach2ComponentType.Match> {
    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    public static Find_ComponentAttach2ComponentType.Matcher on(final ViatraQueryEngine engine) {
      // check if matcher already exists
      Matcher matcher = engine.getExistingMatcher(querySpecification());
      if (matcher == null) {
          matcher = (Matcher)engine.getMatcher(querySpecification());
      }
      return matcher;
    }

    /**
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * @return an initialized matcher
     * @noreference This method is for internal matcher initialization by the framework, do not call it manually.
     * 
     */
    public static Find_ComponentAttach2ComponentType.Matcher create() {
      return new Matcher();
    }

    private static final int POSITION_COMPINST = 0;

    private static final int POSITION_TRACE = 1;

    private static final int POSITION_COMPINST2ADD = 2;

    private static final int POSITION_COMPTYPE = 3;

    private static final int POSITION_PACK = 4;

    private static final int POSITION_COMP = 5;

    private static final Logger LOGGER = ViatraQueryLoggingUtil.getLogger(Find_ComponentAttach2ComponentType.Matcher.class);

    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    private Matcher() {
      super(querySpecification());
    }

    /**
     * Returns the set of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pCompinst2add the fixed value of pattern parameter compinst2add, or null if not bound.
     * @param pComptype the fixed value of pattern parameter comptype, or null if not bound.
     * @param pPack the fixed value of pattern parameter pack, or null if not bound.
     * @param pComp the fixed value of pattern parameter comp, or null if not bound.
     * @return matches represented as a Match object.
     * 
     */
    public Collection<Find_ComponentAttach2ComponentType.Match> getAllMatches(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentType pComptype, final PublicPackageSection pPack, final Subcomponent pComp) {
      return rawStreamAllMatches(new Object[]{pCompinst, pTrace, pCompinst2add, pComptype, pPack, pComp}).collect(Collectors.toSet());
    }

    /**
     * Returns a stream of all matches of the pattern that conform to the given fixed values of some parameters.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pCompinst2add the fixed value of pattern parameter compinst2add, or null if not bound.
     * @param pComptype the fixed value of pattern parameter comptype, or null if not bound.
     * @param pPack the fixed value of pattern parameter pack, or null if not bound.
     * @param pComp the fixed value of pattern parameter comp, or null if not bound.
     * @return a stream of matches represented as a Match object.
     * 
     */
    public Stream<Find_ComponentAttach2ComponentType.Match> streamAllMatches(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentType pComptype, final PublicPackageSection pPack, final Subcomponent pComp) {
      return rawStreamAllMatches(new Object[]{pCompinst, pTrace, pCompinst2add, pComptype, pPack, pComp});
    }

    /**
     * Returns an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pCompinst2add the fixed value of pattern parameter compinst2add, or null if not bound.
     * @param pComptype the fixed value of pattern parameter comptype, or null if not bound.
     * @param pPack the fixed value of pattern parameter pack, or null if not bound.
     * @param pComp the fixed value of pattern parameter comp, or null if not bound.
     * @return a match represented as a Match object, or null if no match is found.
     * 
     */
    public Optional<Find_ComponentAttach2ComponentType.Match> getOneArbitraryMatch(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentType pComptype, final PublicPackageSection pPack, final Subcomponent pComp) {
      return rawGetOneArbitraryMatch(new Object[]{pCompinst, pTrace, pCompinst2add, pComptype, pPack, pComp});
    }

    /**
     * Indicates whether the given combination of specified pattern parameters constitute a valid pattern match,
     * under any possible substitution of the unspecified parameters (if any).
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pCompinst2add the fixed value of pattern parameter compinst2add, or null if not bound.
     * @param pComptype the fixed value of pattern parameter comptype, or null if not bound.
     * @param pPack the fixed value of pattern parameter pack, or null if not bound.
     * @param pComp the fixed value of pattern parameter comp, or null if not bound.
     * @return true if the input is a valid (partial) match of the pattern.
     * 
     */
    public boolean hasMatch(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentType pComptype, final PublicPackageSection pPack, final Subcomponent pComp) {
      return rawHasMatch(new Object[]{pCompinst, pTrace, pCompinst2add, pComptype, pPack, pComp});
    }

    /**
     * Returns the number of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pCompinst2add the fixed value of pattern parameter compinst2add, or null if not bound.
     * @param pComptype the fixed value of pattern parameter comptype, or null if not bound.
     * @param pPack the fixed value of pattern parameter pack, or null if not bound.
     * @param pComp the fixed value of pattern parameter comp, or null if not bound.
     * @return the number of pattern matches found.
     * 
     */
    public int countMatches(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentType pComptype, final PublicPackageSection pPack, final Subcomponent pComp) {
      return rawCountMatches(new Object[]{pCompinst, pTrace, pCompinst2add, pComptype, pPack, pComp});
    }

    /**
     * Executes the given processor on an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pCompinst2add the fixed value of pattern parameter compinst2add, or null if not bound.
     * @param pComptype the fixed value of pattern parameter comptype, or null if not bound.
     * @param pPack the fixed value of pattern parameter pack, or null if not bound.
     * @param pComp the fixed value of pattern parameter comp, or null if not bound.
     * @param processor the action that will process the selected match.
     * @return true if the pattern has at least one match with the given parameter values, false if the processor was not invoked
     * 
     */
    public boolean forOneArbitraryMatch(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentType pComptype, final PublicPackageSection pPack, final Subcomponent pComp, final Consumer<? super Find_ComponentAttach2ComponentType.Match> processor) {
      return rawForOneArbitraryMatch(new Object[]{pCompinst, pTrace, pCompinst2add, pComptype, pPack, pComp}, processor);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pCompinst2add the fixed value of pattern parameter compinst2add, or null if not bound.
     * @param pComptype the fixed value of pattern parameter comptype, or null if not bound.
     * @param pPack the fixed value of pattern parameter pack, or null if not bound.
     * @param pComp the fixed value of pattern parameter comp, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public Find_ComponentAttach2ComponentType.Match newMatch(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentType pComptype, final PublicPackageSection pPack, final Subcomponent pComp) {
      return Find_ComponentAttach2ComponentType.Match.newMatch(pCompinst, pTrace, pCompinst2add, pComptype, pPack, pComp);
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ComponentInstance> rawStreamAllValuesOfcompinst(final Object[] parameters) {
      return rawStreamAllValues(POSITION_COMPINST, parameters).map(ComponentInstance.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst() {
      return rawStreamAllValuesOfcompinst(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst() {
      return rawStreamAllValuesOfcompinst(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst(final Find_ComponentAttach2ComponentType.Match partialMatch) {
      return rawStreamAllValuesOfcompinst(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst(final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentType pComptype, final PublicPackageSection pPack, final Subcomponent pComp) {
      return rawStreamAllValuesOfcompinst(new Object[]{null, pTrace, pCompinst2add, pComptype, pPack, pComp});
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst(final Find_ComponentAttach2ComponentType.Match partialMatch) {
      return rawStreamAllValuesOfcompinst(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst(final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentType pComptype, final PublicPackageSection pPack, final Subcomponent pComp) {
      return rawStreamAllValuesOfcompinst(new Object[]{null, pTrace, pCompinst2add, pComptype, pPack, pComp}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<Aaxl2AaxlTrace> rawStreamAllValuesOftrace(final Object[] parameters) {
      return rawStreamAllValues(POSITION_TRACE, parameters).map(Aaxl2AaxlTrace.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Aaxl2AaxlTrace> getAllValuesOftrace() {
      return rawStreamAllValuesOftrace(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<Aaxl2AaxlTrace> streamAllValuesOftrace() {
      return rawStreamAllValuesOftrace(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<Aaxl2AaxlTrace> streamAllValuesOftrace(final Find_ComponentAttach2ComponentType.Match partialMatch) {
      return rawStreamAllValuesOftrace(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<Aaxl2AaxlTrace> streamAllValuesOftrace(final ComponentInstance pCompinst, final ComponentInstance pCompinst2add, final ComponentType pComptype, final PublicPackageSection pPack, final Subcomponent pComp) {
      return rawStreamAllValuesOftrace(new Object[]{pCompinst, null, pCompinst2add, pComptype, pPack, pComp});
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Aaxl2AaxlTrace> getAllValuesOftrace(final Find_ComponentAttach2ComponentType.Match partialMatch) {
      return rawStreamAllValuesOftrace(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Aaxl2AaxlTrace> getAllValuesOftrace(final ComponentInstance pCompinst, final ComponentInstance pCompinst2add, final ComponentType pComptype, final PublicPackageSection pPack, final Subcomponent pComp) {
      return rawStreamAllValuesOftrace(new Object[]{pCompinst, null, pCompinst2add, pComptype, pPack, pComp}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst2add.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ComponentInstance> rawStreamAllValuesOfcompinst2add(final Object[] parameters) {
      return rawStreamAllValues(POSITION_COMPINST2ADD, parameters).map(ComponentInstance.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for compinst2add.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst2add() {
      return rawStreamAllValuesOfcompinst2add(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst2add.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst2add() {
      return rawStreamAllValuesOfcompinst2add(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst2add.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst2add(final Find_ComponentAttach2ComponentType.Match partialMatch) {
      return rawStreamAllValuesOfcompinst2add(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst2add.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst2add(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentType pComptype, final PublicPackageSection pPack, final Subcomponent pComp) {
      return rawStreamAllValuesOfcompinst2add(new Object[]{pCompinst, pTrace, null, pComptype, pPack, pComp});
    }

    /**
     * Retrieve the set of values that occur in matches for compinst2add.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst2add(final Find_ComponentAttach2ComponentType.Match partialMatch) {
      return rawStreamAllValuesOfcompinst2add(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst2add.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst2add(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentType pComptype, final PublicPackageSection pPack, final Subcomponent pComp) {
      return rawStreamAllValuesOfcompinst2add(new Object[]{pCompinst, pTrace, null, pComptype, pPack, pComp}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for comptype.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ComponentType> rawStreamAllValuesOfcomptype(final Object[] parameters) {
      return rawStreamAllValues(POSITION_COMPTYPE, parameters).map(ComponentType.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for comptype.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentType> getAllValuesOfcomptype() {
      return rawStreamAllValuesOfcomptype(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for comptype.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentType> streamAllValuesOfcomptype() {
      return rawStreamAllValuesOfcomptype(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for comptype.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentType> streamAllValuesOfcomptype(final Find_ComponentAttach2ComponentType.Match partialMatch) {
      return rawStreamAllValuesOfcomptype(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for comptype.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentType> streamAllValuesOfcomptype(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final PublicPackageSection pPack, final Subcomponent pComp) {
      return rawStreamAllValuesOfcomptype(new Object[]{pCompinst, pTrace, pCompinst2add, null, pPack, pComp});
    }

    /**
     * Retrieve the set of values that occur in matches for comptype.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentType> getAllValuesOfcomptype(final Find_ComponentAttach2ComponentType.Match partialMatch) {
      return rawStreamAllValuesOfcomptype(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for comptype.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentType> getAllValuesOfcomptype(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final PublicPackageSection pPack, final Subcomponent pComp) {
      return rawStreamAllValuesOfcomptype(new Object[]{pCompinst, pTrace, pCompinst2add, null, pPack, pComp}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for pack.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<PublicPackageSection> rawStreamAllValuesOfpack(final Object[] parameters) {
      return rawStreamAllValues(POSITION_PACK, parameters).map(PublicPackageSection.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for pack.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<PublicPackageSection> getAllValuesOfpack() {
      return rawStreamAllValuesOfpack(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for pack.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<PublicPackageSection> streamAllValuesOfpack() {
      return rawStreamAllValuesOfpack(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for pack.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<PublicPackageSection> streamAllValuesOfpack(final Find_ComponentAttach2ComponentType.Match partialMatch) {
      return rawStreamAllValuesOfpack(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for pack.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<PublicPackageSection> streamAllValuesOfpack(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentType pComptype, final Subcomponent pComp) {
      return rawStreamAllValuesOfpack(new Object[]{pCompinst, pTrace, pCompinst2add, pComptype, null, pComp});
    }

    /**
     * Retrieve the set of values that occur in matches for pack.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<PublicPackageSection> getAllValuesOfpack(final Find_ComponentAttach2ComponentType.Match partialMatch) {
      return rawStreamAllValuesOfpack(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for pack.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<PublicPackageSection> getAllValuesOfpack(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentType pComptype, final Subcomponent pComp) {
      return rawStreamAllValuesOfpack(new Object[]{pCompinst, pTrace, pCompinst2add, pComptype, null, pComp}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for comp.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<Subcomponent> rawStreamAllValuesOfcomp(final Object[] parameters) {
      return rawStreamAllValues(POSITION_COMP, parameters).map(Subcomponent.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for comp.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Subcomponent> getAllValuesOfcomp() {
      return rawStreamAllValuesOfcomp(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for comp.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<Subcomponent> streamAllValuesOfcomp() {
      return rawStreamAllValuesOfcomp(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for comp.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<Subcomponent> streamAllValuesOfcomp(final Find_ComponentAttach2ComponentType.Match partialMatch) {
      return rawStreamAllValuesOfcomp(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for comp.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<Subcomponent> streamAllValuesOfcomp(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentType pComptype, final PublicPackageSection pPack) {
      return rawStreamAllValuesOfcomp(new Object[]{pCompinst, pTrace, pCompinst2add, pComptype, pPack, null});
    }

    /**
     * Retrieve the set of values that occur in matches for comp.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Subcomponent> getAllValuesOfcomp(final Find_ComponentAttach2ComponentType.Match partialMatch) {
      return rawStreamAllValuesOfcomp(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for comp.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Subcomponent> getAllValuesOfcomp(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentType pComptype, final PublicPackageSection pPack) {
      return rawStreamAllValuesOfcomp(new Object[]{pCompinst, pTrace, pCompinst2add, pComptype, pPack, null}).collect(Collectors.toSet());
    }

    @Override
    protected Find_ComponentAttach2ComponentType.Match tupleToMatch(final Tuple t) {
      try {
          return Find_ComponentAttach2ComponentType.Match.newMatch((ComponentInstance) t.get(POSITION_COMPINST), (Aaxl2AaxlTrace) t.get(POSITION_TRACE), (ComponentInstance) t.get(POSITION_COMPINST2ADD), (ComponentType) t.get(POSITION_COMPTYPE), (PublicPackageSection) t.get(POSITION_PACK), (Subcomponent) t.get(POSITION_COMP));
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in tuple not properly typed!",e);
          return null;
      }
    }

    @Override
    protected Find_ComponentAttach2ComponentType.Match arrayToMatch(final Object[] match) {
      try {
          return Find_ComponentAttach2ComponentType.Match.newMatch((ComponentInstance) match[POSITION_COMPINST], (Aaxl2AaxlTrace) match[POSITION_TRACE], (ComponentInstance) match[POSITION_COMPINST2ADD], (ComponentType) match[POSITION_COMPTYPE], (PublicPackageSection) match[POSITION_PACK], (Subcomponent) match[POSITION_COMP]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    @Override
    protected Find_ComponentAttach2ComponentType.Match arrayToMatchMutable(final Object[] match) {
      try {
          return Find_ComponentAttach2ComponentType.Match.newMutableMatch((ComponentInstance) match[POSITION_COMPINST], (Aaxl2AaxlTrace) match[POSITION_TRACE], (ComponentInstance) match[POSITION_COMPINST2ADD], (ComponentType) match[POSITION_COMPTYPE], (PublicPackageSection) match[POSITION_PACK], (Subcomponent) match[POSITION_COMP]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    /**
     * @return the singleton instance of the query specification of this pattern
     * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
     * 
     */
    public static IQuerySpecification<Find_ComponentAttach2ComponentType.Matcher> querySpecification() {
      return Find_ComponentAttach2ComponentType.instance();
    }
  }

  private Find_ComponentAttach2ComponentType() {
    super(GeneratedPQuery.INSTANCE);
  }

  /**
   * @return the singleton instance of the query specification
   * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
   * 
   */
  public static Find_ComponentAttach2ComponentType instance() {
    try{
        return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
        throw processInitializerError(err);
    }
  }

  @Override
  protected Find_ComponentAttach2ComponentType.Matcher instantiate(final ViatraQueryEngine engine) {
    return Find_ComponentAttach2ComponentType.Matcher.on(engine);
  }

  @Override
  public Find_ComponentAttach2ComponentType.Matcher instantiate() {
    return Find_ComponentAttach2ComponentType.Matcher.create();
  }

  @Override
  public Find_ComponentAttach2ComponentType.Match newEmptyMatch() {
    return Find_ComponentAttach2ComponentType.Match.newEmptyMatch();
  }

  @Override
  public Find_ComponentAttach2ComponentType.Match newMatch(final Object... parameters) {
    return Find_ComponentAttach2ComponentType.Match.newMatch((org.osate.aadl2.instance.ComponentInstance) parameters[0], (fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace) parameters[1], (org.osate.aadl2.instance.ComponentInstance) parameters[2], (org.osate.aadl2.ComponentType) parameters[3], (org.osate.aadl2.PublicPackageSection) parameters[4], (org.osate.aadl2.Subcomponent) parameters[5]);
  }

  /**
   * Inner class allowing the singleton instance of {@link Find_ComponentAttach2ComponentType} to be created 
   *     <b>not</b> at the class load time of the outer class, 
   *     but rather at the first call to {@link Find_ComponentAttach2ComponentType#instance()}.
   * 
   * <p> This workaround is required e.g. to support recursion.
   * 
   */
  private static class LazyHolder {
    private static final Find_ComponentAttach2ComponentType INSTANCE = new Find_ComponentAttach2ComponentType();

    /**
     * Statically initializes the query specification <b>after</b> the field {@link #INSTANCE} is assigned.
     * This initialization order is required to support indirect recursion.
     * 
     * <p> The static initializer is defined using a helper field to work around limitations of the code generator.
     * 
     */
    private static final Object STATIC_INITIALIZER = ensureInitialized();

    public static Object ensureInitialized() {
      INSTANCE.ensureInitializedInternal();
      return null;
    }
  }

  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private static final Find_ComponentAttach2ComponentType.GeneratedPQuery INSTANCE = new GeneratedPQuery();

    private final PParameter parameter_compinst = new PParameter("compinst", "org.osate.aadl2.instance.ComponentInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ComponentInstance")), PParameterDirection.INOUT);

    private final PParameter parameter_trace = new PParameter("trace", "fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace")), PParameterDirection.INOUT);

    private final PParameter parameter_compinst2add = new PParameter("compinst2add", "org.osate.aadl2.instance.ComponentInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ComponentInstance")), PParameterDirection.INOUT);

    private final PParameter parameter_comptype = new PParameter("comptype", "org.osate.aadl2.ComponentType", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0", "ComponentType")), PParameterDirection.INOUT);

    private final PParameter parameter_pack = new PParameter("pack", "org.osate.aadl2.PublicPackageSection", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0", "PublicPackageSection")), PParameterDirection.INOUT);

    private final PParameter parameter_comp = new PParameter("comp", "org.osate.aadl2.Subcomponent", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0", "Subcomponent")), PParameterDirection.INOUT);

    private final List<PParameter> parameters = Arrays.asList(parameter_compinst, parameter_trace, parameter_compinst2add, parameter_comptype, parameter_pack, parameter_comp);

    private GeneratedPQuery() {
      super(PVisibility.PUBLIC);
    }

    @Override
    public String getFullyQualifiedName() {
      return "fr.mem4csd.osatedim.viatra.queries.find_ComponentAttach2ComponentType";
    }

    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("compinst","trace","compinst2add","comptype","pack","comp");
    }

    @Override
    public List<PParameter> getParameters() {
      return parameters;
    }

    @Override
    public Set<PBody> doGetContainedBodies() {
      setEvaluationHints(new QueryEvaluationHint(null, QueryEvaluationHint.BackendRequirement.UNSPECIFIED));
      Set<PBody> bodies = new LinkedHashSet<>();
      {
          PBody body = new PBody(this);
          PVariable var_compinst = body.getOrCreateVariableByName("compinst");
          PVariable var_trace = body.getOrCreateVariableByName("trace");
          PVariable var_compinst2add = body.getOrCreateVariableByName("compinst2add");
          PVariable var_comptype = body.getOrCreateVariableByName("comptype");
          PVariable var_pack = body.getOrCreateVariableByName("pack");
          PVariable var_comp = body.getOrCreateVariableByName("comp");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          PVariable var___1_ = body.getOrCreateVariableByName("_<1>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_trace), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst2add), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_comptype), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ComponentType")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_pack), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "PublicPackageSection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_comp), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Subcomponent")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_compinst, parameter_compinst),
             new ExportedParameter(body, var_trace, parameter_trace),
             new ExportedParameter(body, var_compinst2add, parameter_compinst2add),
             new ExportedParameter(body, var_comptype, parameter_comptype),
             new ExportedParameter(body, var_pack, parameter_pack),
             new ExportedParameter(body, var_comp, parameter_comp)
          ));
          // 	find is_in_trace(_,trace,compinst,_)
          new PositivePatternCall(body, Tuples.flatTupleOf(var___0_, var_trace, var_compinst, var___1_), Is_in_trace.instance().getInternalQueryRepresentation());
          // 	Aaxl2AaxlTrace.objectsToAttach(trace,compinst2add)
          new TypeConstraint(body, Tuples.flatTupleOf(var_trace), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_trace, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace", "objectsToAttach")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Element")));
          new Equality(body, var__virtual_0_, var_compinst2add);
          // 	ComponentInstance.classifier(compinst,comptype)
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance", "classifier")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ComponentClassifier")));
          new Equality(body, var__virtual_1_, var_comptype);
          // 	ComponentInstance.subcomponent(compinst,comp)
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance", "subcomponent")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Subcomponent")));
          new Equality(body, var__virtual_2_, var_comp);
          // 	PublicPackageSection.ownedClassifier(pack,comptype)
          new TypeConstraint(body, Tuples.flatTupleOf(var_pack), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "PublicPackageSection")));
          PVariable var__virtual_3_ = body.getOrCreateVariableByName(".virtual{3}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_pack, var__virtual_3_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "PackageSection", "ownedClassifier")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_3_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Classifier")));
          new Equality(body, var__virtual_3_, var_comptype);
          bodies.add(body);
      }
      return bodies;
    }
  }
}
