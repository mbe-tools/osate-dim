/**
 * Generated from platform:/resource/fr.mem4csd.osatedim/src/fr/mem4csd/osatedim/viatra/queries/DIMQueriesInplace.vql
 */
package fr.mem4csd.osatedim.viatra.queries;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.viatra.query.runtime.api.IPatternMatch;
import org.eclipse.viatra.query.runtime.api.IQuerySpecification;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.viatra.query.runtime.api.impl.BaseMatcher;
import org.eclipse.viatra.query.runtime.api.impl.BasePatternMatch;
import org.eclipse.viatra.query.runtime.emf.types.EClassTransitiveInstancesKey;
import org.eclipse.viatra.query.runtime.matchers.backend.QueryEvaluationHint;
import org.eclipse.viatra.query.runtime.matchers.psystem.PBody;
import org.eclipse.viatra.query.runtime.matchers.psystem.PVariable;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.TypeConstraint;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameterDirection;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PVisibility;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuple;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuples;
import org.eclipse.viatra.query.runtime.util.ViatraQueryLoggingUtil;
import org.osate.aadl2.instance.InstanceObject;

/**
 * A pattern-specific query specification that can instantiate Matcher in a type-safe way.
 * 
 * <p>Original source:
 *         <code><pre>
 *         // INPLACE TRANSFORMATION QUERY PATTERNS
 *         // The condition that the declarative element/s of the parent and referenced instance element/s has to be defined is unnecessary
 *         // except for component instances. This is controlled by the priority setting of different model manipulation functions. The 
 *         // condition in the query is only added as a second safe-guard.
 *         
 *         // Find an instance object, used to clean up aaxl2aadl traces
 *         pattern findInstanceObject(objinst : InstanceObject)
 *         {
 *         	InstanceObject(objinst);
 *         }
 * </pre></code>
 * 
 * @see Matcher
 * @see Match
 * 
 */
@SuppressWarnings("all")
public final class FindInstanceObject extends BaseGeneratedEMFQuerySpecification<FindInstanceObject.Matcher> {
  /**
   * Pattern-specific match representation of the fr.mem4csd.osatedim.viatra.queries.findInstanceObject pattern,
   * to be used in conjunction with {@link Matcher}.
   * 
   * <p>Class fields correspond to parameters of the pattern. Fields with value null are considered unassigned.
   * Each instance is a (possibly partial) substitution of pattern parameters,
   * usable to represent a match of the pattern in the result of a query,
   * or to specify the bound (fixed) input parameters when issuing a query.
   * 
   * @see Matcher
   * 
   */
  public static abstract class Match extends BasePatternMatch {
    private InstanceObject fObjinst;

    private static List<String> parameterNames = makeImmutableList("objinst");

    private Match(final InstanceObject pObjinst) {
      this.fObjinst = pObjinst;
    }

    @Override
    public Object get(final String parameterName) {
      switch(parameterName) {
          case "objinst": return this.fObjinst;
          default: return null;
      }
    }

    @Override
    public Object get(final int index) {
      switch(index) {
          case 0: return this.fObjinst;
          default: return null;
      }
    }

    public InstanceObject getObjinst() {
      return this.fObjinst;
    }

    @Override
    public boolean set(final String parameterName, final Object newValue) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      if ("objinst".equals(parameterName) ) {
          this.fObjinst = (InstanceObject) newValue;
          return true;
      }
      return false;
    }

    public void setObjinst(final InstanceObject pObjinst) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fObjinst = pObjinst;
    }

    @Override
    public String patternName() {
      return "fr.mem4csd.osatedim.viatra.queries.findInstanceObject";
    }

    @Override
    public List<String> parameterNames() {
      return FindInstanceObject.Match.parameterNames;
    }

    @Override
    public Object[] toArray() {
      return new Object[]{fObjinst};
    }

    @Override
    public FindInstanceObject.Match toImmutable() {
      return isMutable() ? newMatch(fObjinst) : this;
    }

    @Override
    public String prettyPrint() {
      StringBuilder result = new StringBuilder();
      result.append("\"objinst\"=" + prettyPrintValue(fObjinst));
      return result.toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(fObjinst);
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj)
          return true;
      if (obj == null) {
          return false;
      }
      if ((obj instanceof FindInstanceObject.Match)) {
          FindInstanceObject.Match other = (FindInstanceObject.Match) obj;
          return Objects.equals(fObjinst, other.fObjinst);
      } else {
          // this should be infrequent
          if (!(obj instanceof IPatternMatch)) {
              return false;
          }
          IPatternMatch otherSig  = (IPatternMatch) obj;
          return Objects.equals(specification(), otherSig.specification()) && Arrays.deepEquals(toArray(), otherSig.toArray());
      }
    }

    @Override
    public FindInstanceObject specification() {
      return FindInstanceObject.instance();
    }

    /**
     * Returns an empty, mutable match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @return the empty match.
     * 
     */
    public static FindInstanceObject.Match newEmptyMatch() {
      return new Mutable(null);
    }

    /**
     * Returns a mutable (partial) match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @param pObjinst the fixed value of pattern parameter objinst, or null if not bound.
     * @return the new, mutable (partial) match object.
     * 
     */
    public static FindInstanceObject.Match newMutableMatch(final InstanceObject pObjinst) {
      return new Mutable(pObjinst);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pObjinst the fixed value of pattern parameter objinst, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public static FindInstanceObject.Match newMatch(final InstanceObject pObjinst) {
      return new Immutable(pObjinst);
    }

    private static final class Mutable extends FindInstanceObject.Match {
      Mutable(final InstanceObject pObjinst) {
        super(pObjinst);
      }

      @Override
      public boolean isMutable() {
        return true;
      }
    }

    private static final class Immutable extends FindInstanceObject.Match {
      Immutable(final InstanceObject pObjinst) {
        super(pObjinst);
      }

      @Override
      public boolean isMutable() {
        return false;
      }
    }
  }

  /**
   * Generated pattern matcher API of the fr.mem4csd.osatedim.viatra.queries.findInstanceObject pattern,
   * providing pattern-specific query methods.
   * 
   * <p>Use the pattern matcher on a given model via {@link #on(ViatraQueryEngine)},
   * e.g. in conjunction with {@link ViatraQueryEngine#on(QueryScope)}.
   * 
   * <p>Matches of the pattern will be represented as {@link Match}.
   * 
   * <p>Original source:
   * <code><pre>
   * // INPLACE TRANSFORMATION QUERY PATTERNS
   * // The condition that the declarative element/s of the parent and referenced instance element/s has to be defined is unnecessary
   * // except for component instances. This is controlled by the priority setting of different model manipulation functions. The 
   * // condition in the query is only added as a second safe-guard.
   * 
   * // Find an instance object, used to clean up aaxl2aadl traces
   * pattern findInstanceObject(objinst : InstanceObject)
   * {
   * 	InstanceObject(objinst);
   * }
   * </pre></code>
   * 
   * @see Match
   * @see FindInstanceObject
   * 
   */
  public static class Matcher extends BaseMatcher<FindInstanceObject.Match> {
    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    public static FindInstanceObject.Matcher on(final ViatraQueryEngine engine) {
      // check if matcher already exists
      Matcher matcher = engine.getExistingMatcher(querySpecification());
      if (matcher == null) {
          matcher = (Matcher)engine.getMatcher(querySpecification());
      }
      return matcher;
    }

    /**
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * @return an initialized matcher
     * @noreference This method is for internal matcher initialization by the framework, do not call it manually.
     * 
     */
    public static FindInstanceObject.Matcher create() {
      return new Matcher();
    }

    private static final int POSITION_OBJINST = 0;

    private static final Logger LOGGER = ViatraQueryLoggingUtil.getLogger(FindInstanceObject.Matcher.class);

    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    private Matcher() {
      super(querySpecification());
    }

    /**
     * Returns the set of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pObjinst the fixed value of pattern parameter objinst, or null if not bound.
     * @return matches represented as a Match object.
     * 
     */
    public Collection<FindInstanceObject.Match> getAllMatches(final InstanceObject pObjinst) {
      return rawStreamAllMatches(new Object[]{pObjinst}).collect(Collectors.toSet());
    }

    /**
     * Returns a stream of all matches of the pattern that conform to the given fixed values of some parameters.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     * @param pObjinst the fixed value of pattern parameter objinst, or null if not bound.
     * @return a stream of matches represented as a Match object.
     * 
     */
    public Stream<FindInstanceObject.Match> streamAllMatches(final InstanceObject pObjinst) {
      return rawStreamAllMatches(new Object[]{pObjinst});
    }

    /**
     * Returns an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pObjinst the fixed value of pattern parameter objinst, or null if not bound.
     * @return a match represented as a Match object, or null if no match is found.
     * 
     */
    public Optional<FindInstanceObject.Match> getOneArbitraryMatch(final InstanceObject pObjinst) {
      return rawGetOneArbitraryMatch(new Object[]{pObjinst});
    }

    /**
     * Indicates whether the given combination of specified pattern parameters constitute a valid pattern match,
     * under any possible substitution of the unspecified parameters (if any).
     * @param pObjinst the fixed value of pattern parameter objinst, or null if not bound.
     * @return true if the input is a valid (partial) match of the pattern.
     * 
     */
    public boolean hasMatch(final InstanceObject pObjinst) {
      return rawHasMatch(new Object[]{pObjinst});
    }

    /**
     * Returns the number of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pObjinst the fixed value of pattern parameter objinst, or null if not bound.
     * @return the number of pattern matches found.
     * 
     */
    public int countMatches(final InstanceObject pObjinst) {
      return rawCountMatches(new Object[]{pObjinst});
    }

    /**
     * Executes the given processor on an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pObjinst the fixed value of pattern parameter objinst, or null if not bound.
     * @param processor the action that will process the selected match.
     * @return true if the pattern has at least one match with the given parameter values, false if the processor was not invoked
     * 
     */
    public boolean forOneArbitraryMatch(final InstanceObject pObjinst, final Consumer<? super FindInstanceObject.Match> processor) {
      return rawForOneArbitraryMatch(new Object[]{pObjinst}, processor);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pObjinst the fixed value of pattern parameter objinst, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public FindInstanceObject.Match newMatch(final InstanceObject pObjinst) {
      return FindInstanceObject.Match.newMatch(pObjinst);
    }

    /**
     * Retrieve the set of values that occur in matches for objinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<InstanceObject> rawStreamAllValuesOfobjinst(final Object[] parameters) {
      return rawStreamAllValues(POSITION_OBJINST, parameters).map(InstanceObject.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for objinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<InstanceObject> getAllValuesOfobjinst() {
      return rawStreamAllValuesOfobjinst(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for objinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<InstanceObject> streamAllValuesOfobjinst() {
      return rawStreamAllValuesOfobjinst(emptyArray());
    }

    @Override
    protected FindInstanceObject.Match tupleToMatch(final Tuple t) {
      try {
          return FindInstanceObject.Match.newMatch((InstanceObject) t.get(POSITION_OBJINST));
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in tuple not properly typed!",e);
          return null;
      }
    }

    @Override
    protected FindInstanceObject.Match arrayToMatch(final Object[] match) {
      try {
          return FindInstanceObject.Match.newMatch((InstanceObject) match[POSITION_OBJINST]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    @Override
    protected FindInstanceObject.Match arrayToMatchMutable(final Object[] match) {
      try {
          return FindInstanceObject.Match.newMutableMatch((InstanceObject) match[POSITION_OBJINST]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    /**
     * @return the singleton instance of the query specification of this pattern
     * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
     * 
     */
    public static IQuerySpecification<FindInstanceObject.Matcher> querySpecification() {
      return FindInstanceObject.instance();
    }
  }

  private FindInstanceObject() {
    super(GeneratedPQuery.INSTANCE);
  }

  /**
   * @return the singleton instance of the query specification
   * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
   * 
   */
  public static FindInstanceObject instance() {
    try{
        return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
        throw processInitializerError(err);
    }
  }

  @Override
  protected FindInstanceObject.Matcher instantiate(final ViatraQueryEngine engine) {
    return FindInstanceObject.Matcher.on(engine);
  }

  @Override
  public FindInstanceObject.Matcher instantiate() {
    return FindInstanceObject.Matcher.create();
  }

  @Override
  public FindInstanceObject.Match newEmptyMatch() {
    return FindInstanceObject.Match.newEmptyMatch();
  }

  @Override
  public FindInstanceObject.Match newMatch(final Object... parameters) {
    return FindInstanceObject.Match.newMatch((org.osate.aadl2.instance.InstanceObject) parameters[0]);
  }

  /**
   * Inner class allowing the singleton instance of {@link FindInstanceObject} to be created 
   *     <b>not</b> at the class load time of the outer class, 
   *     but rather at the first call to {@link FindInstanceObject#instance()}.
   * 
   * <p> This workaround is required e.g. to support recursion.
   * 
   */
  private static class LazyHolder {
    private static final FindInstanceObject INSTANCE = new FindInstanceObject();

    /**
     * Statically initializes the query specification <b>after</b> the field {@link #INSTANCE} is assigned.
     * This initialization order is required to support indirect recursion.
     * 
     * <p> The static initializer is defined using a helper field to work around limitations of the code generator.
     * 
     */
    private static final Object STATIC_INITIALIZER = ensureInitialized();

    public static Object ensureInitialized() {
      INSTANCE.ensureInitializedInternal();
      return null;
    }
  }

  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private static final FindInstanceObject.GeneratedPQuery INSTANCE = new GeneratedPQuery();

    private final PParameter parameter_objinst = new PParameter("objinst", "org.osate.aadl2.instance.InstanceObject", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "InstanceObject")), PParameterDirection.INOUT);

    private final List<PParameter> parameters = Arrays.asList(parameter_objinst);

    private GeneratedPQuery() {
      super(PVisibility.PUBLIC);
    }

    @Override
    public String getFullyQualifiedName() {
      return "fr.mem4csd.osatedim.viatra.queries.findInstanceObject";
    }

    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("objinst");
    }

    @Override
    public List<PParameter> getParameters() {
      return parameters;
    }

    @Override
    public Set<PBody> doGetContainedBodies() {
      setEvaluationHints(new QueryEvaluationHint(null, QueryEvaluationHint.BackendRequirement.UNSPECIFIED));
      Set<PBody> bodies = new LinkedHashSet<>();
      {
          PBody body = new PBody(this);
          PVariable var_objinst = body.getOrCreateVariableByName("objinst");
          new TypeConstraint(body, Tuples.flatTupleOf(var_objinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "InstanceObject")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_objinst, parameter_objinst)
          ));
          // 	InstanceObject(objinst)
          new TypeConstraint(body, Tuples.flatTupleOf(var_objinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "InstanceObject")));
          bodies.add(body);
      }
      return bodies;
    }
  }
}
