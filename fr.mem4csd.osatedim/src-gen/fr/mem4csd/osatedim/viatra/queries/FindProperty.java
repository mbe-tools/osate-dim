/**
 * Generated from platform:/resource/fr.mem4csd.osatedim/src/fr/mem4csd/osatedim/viatra/queries/DIMQueriesInplace.vql
 */
package fr.mem4csd.osatedim.viatra.queries;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.viatra.query.runtime.api.IPatternMatch;
import org.eclipse.viatra.query.runtime.api.IQuerySpecification;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.viatra.query.runtime.api.impl.BaseMatcher;
import org.eclipse.viatra.query.runtime.api.impl.BasePatternMatch;
import org.eclipse.viatra.query.runtime.emf.types.EClassTransitiveInstancesKey;
import org.eclipse.viatra.query.runtime.emf.types.EStructuralFeatureInstancesKey;
import org.eclipse.viatra.query.runtime.matchers.backend.QueryEvaluationHint;
import org.eclipse.viatra.query.runtime.matchers.psystem.PBody;
import org.eclipse.viatra.query.runtime.matchers.psystem.PVariable;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.Equality;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.TypeConstraint;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameterDirection;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PVisibility;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuple;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuples;
import org.eclipse.viatra.query.runtime.util.ViatraQueryLoggingUtil;
import org.osate.aadl2.instance.PropertyAssociationInstance;

/**
 * A pattern-specific query specification that can instantiate Matcher in a type-safe way.
 * 
 * <p>Original source:
 *         <code><pre>
 *         // Find property attached to an instance object
 *         pattern findProperty(propinst : PropertyAssociationInstance)
 *         {
 *         	InstanceObject.ownedPropertyAssociation(_,propinst);
 *         }
 * </pre></code>
 * 
 * @see Matcher
 * @see Match
 * 
 */
@SuppressWarnings("all")
public final class FindProperty extends BaseGeneratedEMFQuerySpecification<FindProperty.Matcher> {
  /**
   * Pattern-specific match representation of the fr.mem4csd.osatedim.viatra.queries.findProperty pattern,
   * to be used in conjunction with {@link Matcher}.
   * 
   * <p>Class fields correspond to parameters of the pattern. Fields with value null are considered unassigned.
   * Each instance is a (possibly partial) substitution of pattern parameters,
   * usable to represent a match of the pattern in the result of a query,
   * or to specify the bound (fixed) input parameters when issuing a query.
   * 
   * @see Matcher
   * 
   */
  public static abstract class Match extends BasePatternMatch {
    private PropertyAssociationInstance fPropinst;

    private static List<String> parameterNames = makeImmutableList("propinst");

    private Match(final PropertyAssociationInstance pPropinst) {
      this.fPropinst = pPropinst;
    }

    @Override
    public Object get(final String parameterName) {
      switch(parameterName) {
          case "propinst": return this.fPropinst;
          default: return null;
      }
    }

    @Override
    public Object get(final int index) {
      switch(index) {
          case 0: return this.fPropinst;
          default: return null;
      }
    }

    public PropertyAssociationInstance getPropinst() {
      return this.fPropinst;
    }

    @Override
    public boolean set(final String parameterName, final Object newValue) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      if ("propinst".equals(parameterName) ) {
          this.fPropinst = (PropertyAssociationInstance) newValue;
          return true;
      }
      return false;
    }

    public void setPropinst(final PropertyAssociationInstance pPropinst) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fPropinst = pPropinst;
    }

    @Override
    public String patternName() {
      return "fr.mem4csd.osatedim.viatra.queries.findProperty";
    }

    @Override
    public List<String> parameterNames() {
      return FindProperty.Match.parameterNames;
    }

    @Override
    public Object[] toArray() {
      return new Object[]{fPropinst};
    }

    @Override
    public FindProperty.Match toImmutable() {
      return isMutable() ? newMatch(fPropinst) : this;
    }

    @Override
    public String prettyPrint() {
      StringBuilder result = new StringBuilder();
      result.append("\"propinst\"=" + prettyPrintValue(fPropinst));
      return result.toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(fPropinst);
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj)
          return true;
      if (obj == null) {
          return false;
      }
      if ((obj instanceof FindProperty.Match)) {
          FindProperty.Match other = (FindProperty.Match) obj;
          return Objects.equals(fPropinst, other.fPropinst);
      } else {
          // this should be infrequent
          if (!(obj instanceof IPatternMatch)) {
              return false;
          }
          IPatternMatch otherSig  = (IPatternMatch) obj;
          return Objects.equals(specification(), otherSig.specification()) && Arrays.deepEquals(toArray(), otherSig.toArray());
      }
    }

    @Override
    public FindProperty specification() {
      return FindProperty.instance();
    }

    /**
     * Returns an empty, mutable match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @return the empty match.
     * 
     */
    public static FindProperty.Match newEmptyMatch() {
      return new Mutable(null);
    }

    /**
     * Returns a mutable (partial) match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @return the new, mutable (partial) match object.
     * 
     */
    public static FindProperty.Match newMutableMatch(final PropertyAssociationInstance pPropinst) {
      return new Mutable(pPropinst);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public static FindProperty.Match newMatch(final PropertyAssociationInstance pPropinst) {
      return new Immutable(pPropinst);
    }

    private static final class Mutable extends FindProperty.Match {
      Mutable(final PropertyAssociationInstance pPropinst) {
        super(pPropinst);
      }

      @Override
      public boolean isMutable() {
        return true;
      }
    }

    private static final class Immutable extends FindProperty.Match {
      Immutable(final PropertyAssociationInstance pPropinst) {
        super(pPropinst);
      }

      @Override
      public boolean isMutable() {
        return false;
      }
    }
  }

  /**
   * Generated pattern matcher API of the fr.mem4csd.osatedim.viatra.queries.findProperty pattern,
   * providing pattern-specific query methods.
   * 
   * <p>Use the pattern matcher on a given model via {@link #on(ViatraQueryEngine)},
   * e.g. in conjunction with {@link ViatraQueryEngine#on(QueryScope)}.
   * 
   * <p>Matches of the pattern will be represented as {@link Match}.
   * 
   * <p>Original source:
   * <code><pre>
   * // Find property attached to an instance object
   * pattern findProperty(propinst : PropertyAssociationInstance)
   * {
   * 	InstanceObject.ownedPropertyAssociation(_,propinst);
   * }
   * </pre></code>
   * 
   * @see Match
   * @see FindProperty
   * 
   */
  public static class Matcher extends BaseMatcher<FindProperty.Match> {
    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    public static FindProperty.Matcher on(final ViatraQueryEngine engine) {
      // check if matcher already exists
      Matcher matcher = engine.getExistingMatcher(querySpecification());
      if (matcher == null) {
          matcher = (Matcher)engine.getMatcher(querySpecification());
      }
      return matcher;
    }

    /**
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * @return an initialized matcher
     * @noreference This method is for internal matcher initialization by the framework, do not call it manually.
     * 
     */
    public static FindProperty.Matcher create() {
      return new Matcher();
    }

    private static final int POSITION_PROPINST = 0;

    private static final Logger LOGGER = ViatraQueryLoggingUtil.getLogger(FindProperty.Matcher.class);

    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    private Matcher() {
      super(querySpecification());
    }

    /**
     * Returns the set of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @return matches represented as a Match object.
     * 
     */
    public Collection<FindProperty.Match> getAllMatches(final PropertyAssociationInstance pPropinst) {
      return rawStreamAllMatches(new Object[]{pPropinst}).collect(Collectors.toSet());
    }

    /**
     * Returns a stream of all matches of the pattern that conform to the given fixed values of some parameters.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @return a stream of matches represented as a Match object.
     * 
     */
    public Stream<FindProperty.Match> streamAllMatches(final PropertyAssociationInstance pPropinst) {
      return rawStreamAllMatches(new Object[]{pPropinst});
    }

    /**
     * Returns an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @return a match represented as a Match object, or null if no match is found.
     * 
     */
    public Optional<FindProperty.Match> getOneArbitraryMatch(final PropertyAssociationInstance pPropinst) {
      return rawGetOneArbitraryMatch(new Object[]{pPropinst});
    }

    /**
     * Indicates whether the given combination of specified pattern parameters constitute a valid pattern match,
     * under any possible substitution of the unspecified parameters (if any).
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @return true if the input is a valid (partial) match of the pattern.
     * 
     */
    public boolean hasMatch(final PropertyAssociationInstance pPropinst) {
      return rawHasMatch(new Object[]{pPropinst});
    }

    /**
     * Returns the number of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @return the number of pattern matches found.
     * 
     */
    public int countMatches(final PropertyAssociationInstance pPropinst) {
      return rawCountMatches(new Object[]{pPropinst});
    }

    /**
     * Executes the given processor on an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @param processor the action that will process the selected match.
     * @return true if the pattern has at least one match with the given parameter values, false if the processor was not invoked
     * 
     */
    public boolean forOneArbitraryMatch(final PropertyAssociationInstance pPropinst, final Consumer<? super FindProperty.Match> processor) {
      return rawForOneArbitraryMatch(new Object[]{pPropinst}, processor);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public FindProperty.Match newMatch(final PropertyAssociationInstance pPropinst) {
      return FindProperty.Match.newMatch(pPropinst);
    }

    /**
     * Retrieve the set of values that occur in matches for propinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<PropertyAssociationInstance> rawStreamAllValuesOfpropinst(final Object[] parameters) {
      return rawStreamAllValues(POSITION_PROPINST, parameters).map(PropertyAssociationInstance.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for propinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<PropertyAssociationInstance> getAllValuesOfpropinst() {
      return rawStreamAllValuesOfpropinst(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for propinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<PropertyAssociationInstance> streamAllValuesOfpropinst() {
      return rawStreamAllValuesOfpropinst(emptyArray());
    }

    @Override
    protected FindProperty.Match tupleToMatch(final Tuple t) {
      try {
          return FindProperty.Match.newMatch((PropertyAssociationInstance) t.get(POSITION_PROPINST));
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in tuple not properly typed!",e);
          return null;
      }
    }

    @Override
    protected FindProperty.Match arrayToMatch(final Object[] match) {
      try {
          return FindProperty.Match.newMatch((PropertyAssociationInstance) match[POSITION_PROPINST]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    @Override
    protected FindProperty.Match arrayToMatchMutable(final Object[] match) {
      try {
          return FindProperty.Match.newMutableMatch((PropertyAssociationInstance) match[POSITION_PROPINST]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    /**
     * @return the singleton instance of the query specification of this pattern
     * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
     * 
     */
    public static IQuerySpecification<FindProperty.Matcher> querySpecification() {
      return FindProperty.instance();
    }
  }

  private FindProperty() {
    super(GeneratedPQuery.INSTANCE);
  }

  /**
   * @return the singleton instance of the query specification
   * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
   * 
   */
  public static FindProperty instance() {
    try{
        return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
        throw processInitializerError(err);
    }
  }

  @Override
  protected FindProperty.Matcher instantiate(final ViatraQueryEngine engine) {
    return FindProperty.Matcher.on(engine);
  }

  @Override
  public FindProperty.Matcher instantiate() {
    return FindProperty.Matcher.create();
  }

  @Override
  public FindProperty.Match newEmptyMatch() {
    return FindProperty.Match.newEmptyMatch();
  }

  @Override
  public FindProperty.Match newMatch(final Object... parameters) {
    return FindProperty.Match.newMatch((org.osate.aadl2.instance.PropertyAssociationInstance) parameters[0]);
  }

  /**
   * Inner class allowing the singleton instance of {@link FindProperty} to be created 
   *     <b>not</b> at the class load time of the outer class, 
   *     but rather at the first call to {@link FindProperty#instance()}.
   * 
   * <p> This workaround is required e.g. to support recursion.
   * 
   */
  private static class LazyHolder {
    private static final FindProperty INSTANCE = new FindProperty();

    /**
     * Statically initializes the query specification <b>after</b> the field {@link #INSTANCE} is assigned.
     * This initialization order is required to support indirect recursion.
     * 
     * <p> The static initializer is defined using a helper field to work around limitations of the code generator.
     * 
     */
    private static final Object STATIC_INITIALIZER = ensureInitialized();

    public static Object ensureInitialized() {
      INSTANCE.ensureInitializedInternal();
      return null;
    }
  }

  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private static final FindProperty.GeneratedPQuery INSTANCE = new GeneratedPQuery();

    private final PParameter parameter_propinst = new PParameter("propinst", "org.osate.aadl2.instance.PropertyAssociationInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "PropertyAssociationInstance")), PParameterDirection.INOUT);

    private final List<PParameter> parameters = Arrays.asList(parameter_propinst);

    private GeneratedPQuery() {
      super(PVisibility.PUBLIC);
    }

    @Override
    public String getFullyQualifiedName() {
      return "fr.mem4csd.osatedim.viatra.queries.findProperty";
    }

    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("propinst");
    }

    @Override
    public List<PParameter> getParameters() {
      return parameters;
    }

    @Override
    public Set<PBody> doGetContainedBodies() {
      setEvaluationHints(new QueryEvaluationHint(null, QueryEvaluationHint.BackendRequirement.UNSPECIFIED));
      Set<PBody> bodies = new LinkedHashSet<>();
      {
          PBody body = new PBody(this);
          PVariable var_propinst = body.getOrCreateVariableByName("propinst");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_propinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "PropertyAssociationInstance")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_propinst, parameter_propinst)
          ));
          // 	InstanceObject.ownedPropertyAssociation(_,propinst)
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "InstanceObject")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "NamedElement", "ownedPropertyAssociation")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "PropertyAssociation")));
          new Equality(body, var__virtual_0_, var_propinst);
          bodies.add(body);
      }
      return bodies;
    }
  }
}
