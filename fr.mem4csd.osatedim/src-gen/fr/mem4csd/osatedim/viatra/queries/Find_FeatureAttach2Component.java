/**
 * Generated from platform:/resource/fr.mem4csd.osatedim/src/fr/mem4csd/osatedim/viatra/queries/DIMQueriesOutplace.vql
 */
package fr.mem4csd.osatedim.viatra.queries;

import fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.viatra.query.runtime.api.IPatternMatch;
import org.eclipse.viatra.query.runtime.api.IQuerySpecification;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.viatra.query.runtime.api.impl.BaseMatcher;
import org.eclipse.viatra.query.runtime.api.impl.BasePatternMatch;
import org.eclipse.viatra.query.runtime.emf.types.EClassTransitiveInstancesKey;
import org.eclipse.viatra.query.runtime.emf.types.EStructuralFeatureInstancesKey;
import org.eclipse.viatra.query.runtime.matchers.backend.QueryEvaluationHint;
import org.eclipse.viatra.query.runtime.matchers.psystem.PBody;
import org.eclipse.viatra.query.runtime.matchers.psystem.PVariable;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.Equality;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.PositivePatternCall;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.TypeConstraint;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameterDirection;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PVisibility;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuple;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuples;
import org.eclipse.viatra.query.runtime.util.ViatraQueryLoggingUtil;
import org.osate.aadl2.ComponentType;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.FeatureInstance;

/**
 * A pattern-specific query specification that can instantiate Matcher in a type-safe way.
 * 
 * <p>Original source:
 *         <code><pre>
 *         // This pattern finds the feature2Attach for a specific component
 *         pattern find_FeatureAttach2Component(compinst : ComponentInstance,
 *         								     trace : Aaxl2AaxlTrace,
 *         								     featinst2add : FeatureInstance,
 *         								     comptype : ComponentType)
 *         {
 *         	find is_in_trace(_,trace,compinst,_);
 *         	Aaxl2AaxlTrace.objectsToAttach(trace,featinst2add);
 *         	ComponentInstance.classifier(compinst,comptype);
 *         }
 * </pre></code>
 * 
 * @see Matcher
 * @see Match
 * 
 */
@SuppressWarnings("all")
public final class Find_FeatureAttach2Component extends BaseGeneratedEMFQuerySpecification<Find_FeatureAttach2Component.Matcher> {
  /**
   * Pattern-specific match representation of the fr.mem4csd.osatedim.viatra.queries.find_FeatureAttach2Component pattern,
   * to be used in conjunction with {@link Matcher}.
   * 
   * <p>Class fields correspond to parameters of the pattern. Fields with value null are considered unassigned.
   * Each instance is a (possibly partial) substitution of pattern parameters,
   * usable to represent a match of the pattern in the result of a query,
   * or to specify the bound (fixed) input parameters when issuing a query.
   * 
   * @see Matcher
   * 
   */
  public static abstract class Match extends BasePatternMatch {
    private ComponentInstance fCompinst;

    private Aaxl2AaxlTrace fTrace;

    private FeatureInstance fFeatinst2add;

    private ComponentType fComptype;

    private static List<String> parameterNames = makeImmutableList("compinst", "trace", "featinst2add", "comptype");

    private Match(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final FeatureInstance pFeatinst2add, final ComponentType pComptype) {
      this.fCompinst = pCompinst;
      this.fTrace = pTrace;
      this.fFeatinst2add = pFeatinst2add;
      this.fComptype = pComptype;
    }

    @Override
    public Object get(final String parameterName) {
      switch(parameterName) {
          case "compinst": return this.fCompinst;
          case "trace": return this.fTrace;
          case "featinst2add": return this.fFeatinst2add;
          case "comptype": return this.fComptype;
          default: return null;
      }
    }

    @Override
    public Object get(final int index) {
      switch(index) {
          case 0: return this.fCompinst;
          case 1: return this.fTrace;
          case 2: return this.fFeatinst2add;
          case 3: return this.fComptype;
          default: return null;
      }
    }

    public ComponentInstance getCompinst() {
      return this.fCompinst;
    }

    public Aaxl2AaxlTrace getTrace() {
      return this.fTrace;
    }

    public FeatureInstance getFeatinst2add() {
      return this.fFeatinst2add;
    }

    public ComponentType getComptype() {
      return this.fComptype;
    }

    @Override
    public boolean set(final String parameterName, final Object newValue) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      if ("compinst".equals(parameterName) ) {
          this.fCompinst = (ComponentInstance) newValue;
          return true;
      }
      if ("trace".equals(parameterName) ) {
          this.fTrace = (Aaxl2AaxlTrace) newValue;
          return true;
      }
      if ("featinst2add".equals(parameterName) ) {
          this.fFeatinst2add = (FeatureInstance) newValue;
          return true;
      }
      if ("comptype".equals(parameterName) ) {
          this.fComptype = (ComponentType) newValue;
          return true;
      }
      return false;
    }

    public void setCompinst(final ComponentInstance pCompinst) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fCompinst = pCompinst;
    }

    public void setTrace(final Aaxl2AaxlTrace pTrace) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fTrace = pTrace;
    }

    public void setFeatinst2add(final FeatureInstance pFeatinst2add) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fFeatinst2add = pFeatinst2add;
    }

    public void setComptype(final ComponentType pComptype) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fComptype = pComptype;
    }

    @Override
    public String patternName() {
      return "fr.mem4csd.osatedim.viatra.queries.find_FeatureAttach2Component";
    }

    @Override
    public List<String> parameterNames() {
      return Find_FeatureAttach2Component.Match.parameterNames;
    }

    @Override
    public Object[] toArray() {
      return new Object[]{fCompinst, fTrace, fFeatinst2add, fComptype};
    }

    @Override
    public Find_FeatureAttach2Component.Match toImmutable() {
      return isMutable() ? newMatch(fCompinst, fTrace, fFeatinst2add, fComptype) : this;
    }

    @Override
    public String prettyPrint() {
      StringBuilder result = new StringBuilder();
      result.append("\"compinst\"=" + prettyPrintValue(fCompinst) + ", ");
      result.append("\"trace\"=" + prettyPrintValue(fTrace) + ", ");
      result.append("\"featinst2add\"=" + prettyPrintValue(fFeatinst2add) + ", ");
      result.append("\"comptype\"=" + prettyPrintValue(fComptype));
      return result.toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(fCompinst, fTrace, fFeatinst2add, fComptype);
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj)
          return true;
      if (obj == null) {
          return false;
      }
      if ((obj instanceof Find_FeatureAttach2Component.Match)) {
          Find_FeatureAttach2Component.Match other = (Find_FeatureAttach2Component.Match) obj;
          return Objects.equals(fCompinst, other.fCompinst) && Objects.equals(fTrace, other.fTrace) && Objects.equals(fFeatinst2add, other.fFeatinst2add) && Objects.equals(fComptype, other.fComptype);
      } else {
          // this should be infrequent
          if (!(obj instanceof IPatternMatch)) {
              return false;
          }
          IPatternMatch otherSig  = (IPatternMatch) obj;
          return Objects.equals(specification(), otherSig.specification()) && Arrays.deepEquals(toArray(), otherSig.toArray());
      }
    }

    @Override
    public Find_FeatureAttach2Component specification() {
      return Find_FeatureAttach2Component.instance();
    }

    /**
     * Returns an empty, mutable match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @return the empty match.
     * 
     */
    public static Find_FeatureAttach2Component.Match newEmptyMatch() {
      return new Mutable(null, null, null, null);
    }

    /**
     * Returns a mutable (partial) match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pFeatinst2add the fixed value of pattern parameter featinst2add, or null if not bound.
     * @param pComptype the fixed value of pattern parameter comptype, or null if not bound.
     * @return the new, mutable (partial) match object.
     * 
     */
    public static Find_FeatureAttach2Component.Match newMutableMatch(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final FeatureInstance pFeatinst2add, final ComponentType pComptype) {
      return new Mutable(pCompinst, pTrace, pFeatinst2add, pComptype);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pFeatinst2add the fixed value of pattern parameter featinst2add, or null if not bound.
     * @param pComptype the fixed value of pattern parameter comptype, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public static Find_FeatureAttach2Component.Match newMatch(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final FeatureInstance pFeatinst2add, final ComponentType pComptype) {
      return new Immutable(pCompinst, pTrace, pFeatinst2add, pComptype);
    }

    private static final class Mutable extends Find_FeatureAttach2Component.Match {
      Mutable(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final FeatureInstance pFeatinst2add, final ComponentType pComptype) {
        super(pCompinst, pTrace, pFeatinst2add, pComptype);
      }

      @Override
      public boolean isMutable() {
        return true;
      }
    }

    private static final class Immutable extends Find_FeatureAttach2Component.Match {
      Immutable(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final FeatureInstance pFeatinst2add, final ComponentType pComptype) {
        super(pCompinst, pTrace, pFeatinst2add, pComptype);
      }

      @Override
      public boolean isMutable() {
        return false;
      }
    }
  }

  /**
   * Generated pattern matcher API of the fr.mem4csd.osatedim.viatra.queries.find_FeatureAttach2Component pattern,
   * providing pattern-specific query methods.
   * 
   * <p>Use the pattern matcher on a given model via {@link #on(ViatraQueryEngine)},
   * e.g. in conjunction with {@link ViatraQueryEngine#on(QueryScope)}.
   * 
   * <p>Matches of the pattern will be represented as {@link Match}.
   * 
   * <p>Original source:
   * <code><pre>
   * // This pattern finds the feature2Attach for a specific component
   * pattern find_FeatureAttach2Component(compinst : ComponentInstance,
   * 								     trace : Aaxl2AaxlTrace,
   * 								     featinst2add : FeatureInstance,
   * 								     comptype : ComponentType)
   * {
   * 	find is_in_trace(_,trace,compinst,_);
   * 	Aaxl2AaxlTrace.objectsToAttach(trace,featinst2add);
   * 	ComponentInstance.classifier(compinst,comptype);
   * }
   * </pre></code>
   * 
   * @see Match
   * @see Find_FeatureAttach2Component
   * 
   */
  public static class Matcher extends BaseMatcher<Find_FeatureAttach2Component.Match> {
    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    public static Find_FeatureAttach2Component.Matcher on(final ViatraQueryEngine engine) {
      // check if matcher already exists
      Matcher matcher = engine.getExistingMatcher(querySpecification());
      if (matcher == null) {
          matcher = (Matcher)engine.getMatcher(querySpecification());
      }
      return matcher;
    }

    /**
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * @return an initialized matcher
     * @noreference This method is for internal matcher initialization by the framework, do not call it manually.
     * 
     */
    public static Find_FeatureAttach2Component.Matcher create() {
      return new Matcher();
    }

    private static final int POSITION_COMPINST = 0;

    private static final int POSITION_TRACE = 1;

    private static final int POSITION_FEATINST2ADD = 2;

    private static final int POSITION_COMPTYPE = 3;

    private static final Logger LOGGER = ViatraQueryLoggingUtil.getLogger(Find_FeatureAttach2Component.Matcher.class);

    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    private Matcher() {
      super(querySpecification());
    }

    /**
     * Returns the set of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pFeatinst2add the fixed value of pattern parameter featinst2add, or null if not bound.
     * @param pComptype the fixed value of pattern parameter comptype, or null if not bound.
     * @return matches represented as a Match object.
     * 
     */
    public Collection<Find_FeatureAttach2Component.Match> getAllMatches(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final FeatureInstance pFeatinst2add, final ComponentType pComptype) {
      return rawStreamAllMatches(new Object[]{pCompinst, pTrace, pFeatinst2add, pComptype}).collect(Collectors.toSet());
    }

    /**
     * Returns a stream of all matches of the pattern that conform to the given fixed values of some parameters.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pFeatinst2add the fixed value of pattern parameter featinst2add, or null if not bound.
     * @param pComptype the fixed value of pattern parameter comptype, or null if not bound.
     * @return a stream of matches represented as a Match object.
     * 
     */
    public Stream<Find_FeatureAttach2Component.Match> streamAllMatches(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final FeatureInstance pFeatinst2add, final ComponentType pComptype) {
      return rawStreamAllMatches(new Object[]{pCompinst, pTrace, pFeatinst2add, pComptype});
    }

    /**
     * Returns an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pFeatinst2add the fixed value of pattern parameter featinst2add, or null if not bound.
     * @param pComptype the fixed value of pattern parameter comptype, or null if not bound.
     * @return a match represented as a Match object, or null if no match is found.
     * 
     */
    public Optional<Find_FeatureAttach2Component.Match> getOneArbitraryMatch(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final FeatureInstance pFeatinst2add, final ComponentType pComptype) {
      return rawGetOneArbitraryMatch(new Object[]{pCompinst, pTrace, pFeatinst2add, pComptype});
    }

    /**
     * Indicates whether the given combination of specified pattern parameters constitute a valid pattern match,
     * under any possible substitution of the unspecified parameters (if any).
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pFeatinst2add the fixed value of pattern parameter featinst2add, or null if not bound.
     * @param pComptype the fixed value of pattern parameter comptype, or null if not bound.
     * @return true if the input is a valid (partial) match of the pattern.
     * 
     */
    public boolean hasMatch(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final FeatureInstance pFeatinst2add, final ComponentType pComptype) {
      return rawHasMatch(new Object[]{pCompinst, pTrace, pFeatinst2add, pComptype});
    }

    /**
     * Returns the number of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pFeatinst2add the fixed value of pattern parameter featinst2add, or null if not bound.
     * @param pComptype the fixed value of pattern parameter comptype, or null if not bound.
     * @return the number of pattern matches found.
     * 
     */
    public int countMatches(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final FeatureInstance pFeatinst2add, final ComponentType pComptype) {
      return rawCountMatches(new Object[]{pCompinst, pTrace, pFeatinst2add, pComptype});
    }

    /**
     * Executes the given processor on an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pFeatinst2add the fixed value of pattern parameter featinst2add, or null if not bound.
     * @param pComptype the fixed value of pattern parameter comptype, or null if not bound.
     * @param processor the action that will process the selected match.
     * @return true if the pattern has at least one match with the given parameter values, false if the processor was not invoked
     * 
     */
    public boolean forOneArbitraryMatch(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final FeatureInstance pFeatinst2add, final ComponentType pComptype, final Consumer<? super Find_FeatureAttach2Component.Match> processor) {
      return rawForOneArbitraryMatch(new Object[]{pCompinst, pTrace, pFeatinst2add, pComptype}, processor);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pFeatinst2add the fixed value of pattern parameter featinst2add, or null if not bound.
     * @param pComptype the fixed value of pattern parameter comptype, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public Find_FeatureAttach2Component.Match newMatch(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final FeatureInstance pFeatinst2add, final ComponentType pComptype) {
      return Find_FeatureAttach2Component.Match.newMatch(pCompinst, pTrace, pFeatinst2add, pComptype);
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ComponentInstance> rawStreamAllValuesOfcompinst(final Object[] parameters) {
      return rawStreamAllValues(POSITION_COMPINST, parameters).map(ComponentInstance.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst() {
      return rawStreamAllValuesOfcompinst(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst() {
      return rawStreamAllValuesOfcompinst(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst(final Find_FeatureAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfcompinst(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst(final Aaxl2AaxlTrace pTrace, final FeatureInstance pFeatinst2add, final ComponentType pComptype) {
      return rawStreamAllValuesOfcompinst(new Object[]{null, pTrace, pFeatinst2add, pComptype});
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst(final Find_FeatureAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfcompinst(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst(final Aaxl2AaxlTrace pTrace, final FeatureInstance pFeatinst2add, final ComponentType pComptype) {
      return rawStreamAllValuesOfcompinst(new Object[]{null, pTrace, pFeatinst2add, pComptype}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<Aaxl2AaxlTrace> rawStreamAllValuesOftrace(final Object[] parameters) {
      return rawStreamAllValues(POSITION_TRACE, parameters).map(Aaxl2AaxlTrace.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Aaxl2AaxlTrace> getAllValuesOftrace() {
      return rawStreamAllValuesOftrace(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<Aaxl2AaxlTrace> streamAllValuesOftrace() {
      return rawStreamAllValuesOftrace(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<Aaxl2AaxlTrace> streamAllValuesOftrace(final Find_FeatureAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOftrace(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<Aaxl2AaxlTrace> streamAllValuesOftrace(final ComponentInstance pCompinst, final FeatureInstance pFeatinst2add, final ComponentType pComptype) {
      return rawStreamAllValuesOftrace(new Object[]{pCompinst, null, pFeatinst2add, pComptype});
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Aaxl2AaxlTrace> getAllValuesOftrace(final Find_FeatureAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOftrace(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Aaxl2AaxlTrace> getAllValuesOftrace(final ComponentInstance pCompinst, final FeatureInstance pFeatinst2add, final ComponentType pComptype) {
      return rawStreamAllValuesOftrace(new Object[]{pCompinst, null, pFeatinst2add, pComptype}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for featinst2add.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<FeatureInstance> rawStreamAllValuesOffeatinst2add(final Object[] parameters) {
      return rawStreamAllValues(POSITION_FEATINST2ADD, parameters).map(FeatureInstance.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for featinst2add.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<FeatureInstance> getAllValuesOffeatinst2add() {
      return rawStreamAllValuesOffeatinst2add(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for featinst2add.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<FeatureInstance> streamAllValuesOffeatinst2add() {
      return rawStreamAllValuesOffeatinst2add(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for featinst2add.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<FeatureInstance> streamAllValuesOffeatinst2add(final Find_FeatureAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOffeatinst2add(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for featinst2add.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<FeatureInstance> streamAllValuesOffeatinst2add(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentType pComptype) {
      return rawStreamAllValuesOffeatinst2add(new Object[]{pCompinst, pTrace, null, pComptype});
    }

    /**
     * Retrieve the set of values that occur in matches for featinst2add.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<FeatureInstance> getAllValuesOffeatinst2add(final Find_FeatureAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOffeatinst2add(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for featinst2add.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<FeatureInstance> getAllValuesOffeatinst2add(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentType pComptype) {
      return rawStreamAllValuesOffeatinst2add(new Object[]{pCompinst, pTrace, null, pComptype}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for comptype.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ComponentType> rawStreamAllValuesOfcomptype(final Object[] parameters) {
      return rawStreamAllValues(POSITION_COMPTYPE, parameters).map(ComponentType.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for comptype.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentType> getAllValuesOfcomptype() {
      return rawStreamAllValuesOfcomptype(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for comptype.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentType> streamAllValuesOfcomptype() {
      return rawStreamAllValuesOfcomptype(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for comptype.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentType> streamAllValuesOfcomptype(final Find_FeatureAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfcomptype(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for comptype.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentType> streamAllValuesOfcomptype(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final FeatureInstance pFeatinst2add) {
      return rawStreamAllValuesOfcomptype(new Object[]{pCompinst, pTrace, pFeatinst2add, null});
    }

    /**
     * Retrieve the set of values that occur in matches for comptype.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentType> getAllValuesOfcomptype(final Find_FeatureAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfcomptype(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for comptype.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentType> getAllValuesOfcomptype(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final FeatureInstance pFeatinst2add) {
      return rawStreamAllValuesOfcomptype(new Object[]{pCompinst, pTrace, pFeatinst2add, null}).collect(Collectors.toSet());
    }

    @Override
    protected Find_FeatureAttach2Component.Match tupleToMatch(final Tuple t) {
      try {
          return Find_FeatureAttach2Component.Match.newMatch((ComponentInstance) t.get(POSITION_COMPINST), (Aaxl2AaxlTrace) t.get(POSITION_TRACE), (FeatureInstance) t.get(POSITION_FEATINST2ADD), (ComponentType) t.get(POSITION_COMPTYPE));
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in tuple not properly typed!",e);
          return null;
      }
    }

    @Override
    protected Find_FeatureAttach2Component.Match arrayToMatch(final Object[] match) {
      try {
          return Find_FeatureAttach2Component.Match.newMatch((ComponentInstance) match[POSITION_COMPINST], (Aaxl2AaxlTrace) match[POSITION_TRACE], (FeatureInstance) match[POSITION_FEATINST2ADD], (ComponentType) match[POSITION_COMPTYPE]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    @Override
    protected Find_FeatureAttach2Component.Match arrayToMatchMutable(final Object[] match) {
      try {
          return Find_FeatureAttach2Component.Match.newMutableMatch((ComponentInstance) match[POSITION_COMPINST], (Aaxl2AaxlTrace) match[POSITION_TRACE], (FeatureInstance) match[POSITION_FEATINST2ADD], (ComponentType) match[POSITION_COMPTYPE]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    /**
     * @return the singleton instance of the query specification of this pattern
     * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
     * 
     */
    public static IQuerySpecification<Find_FeatureAttach2Component.Matcher> querySpecification() {
      return Find_FeatureAttach2Component.instance();
    }
  }

  private Find_FeatureAttach2Component() {
    super(GeneratedPQuery.INSTANCE);
  }

  /**
   * @return the singleton instance of the query specification
   * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
   * 
   */
  public static Find_FeatureAttach2Component instance() {
    try{
        return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
        throw processInitializerError(err);
    }
  }

  @Override
  protected Find_FeatureAttach2Component.Matcher instantiate(final ViatraQueryEngine engine) {
    return Find_FeatureAttach2Component.Matcher.on(engine);
  }

  @Override
  public Find_FeatureAttach2Component.Matcher instantiate() {
    return Find_FeatureAttach2Component.Matcher.create();
  }

  @Override
  public Find_FeatureAttach2Component.Match newEmptyMatch() {
    return Find_FeatureAttach2Component.Match.newEmptyMatch();
  }

  @Override
  public Find_FeatureAttach2Component.Match newMatch(final Object... parameters) {
    return Find_FeatureAttach2Component.Match.newMatch((org.osate.aadl2.instance.ComponentInstance) parameters[0], (fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace) parameters[1], (org.osate.aadl2.instance.FeatureInstance) parameters[2], (org.osate.aadl2.ComponentType) parameters[3]);
  }

  /**
   * Inner class allowing the singleton instance of {@link Find_FeatureAttach2Component} to be created 
   *     <b>not</b> at the class load time of the outer class, 
   *     but rather at the first call to {@link Find_FeatureAttach2Component#instance()}.
   * 
   * <p> This workaround is required e.g. to support recursion.
   * 
   */
  private static class LazyHolder {
    private static final Find_FeatureAttach2Component INSTANCE = new Find_FeatureAttach2Component();

    /**
     * Statically initializes the query specification <b>after</b> the field {@link #INSTANCE} is assigned.
     * This initialization order is required to support indirect recursion.
     * 
     * <p> The static initializer is defined using a helper field to work around limitations of the code generator.
     * 
     */
    private static final Object STATIC_INITIALIZER = ensureInitialized();

    public static Object ensureInitialized() {
      INSTANCE.ensureInitializedInternal();
      return null;
    }
  }

  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private static final Find_FeatureAttach2Component.GeneratedPQuery INSTANCE = new GeneratedPQuery();

    private final PParameter parameter_compinst = new PParameter("compinst", "org.osate.aadl2.instance.ComponentInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ComponentInstance")), PParameterDirection.INOUT);

    private final PParameter parameter_trace = new PParameter("trace", "fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace")), PParameterDirection.INOUT);

    private final PParameter parameter_featinst2add = new PParameter("featinst2add", "org.osate.aadl2.instance.FeatureInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "FeatureInstance")), PParameterDirection.INOUT);

    private final PParameter parameter_comptype = new PParameter("comptype", "org.osate.aadl2.ComponentType", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0", "ComponentType")), PParameterDirection.INOUT);

    private final List<PParameter> parameters = Arrays.asList(parameter_compinst, parameter_trace, parameter_featinst2add, parameter_comptype);

    private GeneratedPQuery() {
      super(PVisibility.PUBLIC);
    }

    @Override
    public String getFullyQualifiedName() {
      return "fr.mem4csd.osatedim.viatra.queries.find_FeatureAttach2Component";
    }

    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("compinst","trace","featinst2add","comptype");
    }

    @Override
    public List<PParameter> getParameters() {
      return parameters;
    }

    @Override
    public Set<PBody> doGetContainedBodies() {
      setEvaluationHints(new QueryEvaluationHint(null, QueryEvaluationHint.BackendRequirement.UNSPECIFIED));
      Set<PBody> bodies = new LinkedHashSet<>();
      {
          PBody body = new PBody(this);
          PVariable var_compinst = body.getOrCreateVariableByName("compinst");
          PVariable var_trace = body.getOrCreateVariableByName("trace");
          PVariable var_featinst2add = body.getOrCreateVariableByName("featinst2add");
          PVariable var_comptype = body.getOrCreateVariableByName("comptype");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          PVariable var___1_ = body.getOrCreateVariableByName("_<1>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_trace), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_featinst2add), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "FeatureInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_comptype), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ComponentType")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_compinst, parameter_compinst),
             new ExportedParameter(body, var_trace, parameter_trace),
             new ExportedParameter(body, var_featinst2add, parameter_featinst2add),
             new ExportedParameter(body, var_comptype, parameter_comptype)
          ));
          // 	find is_in_trace(_,trace,compinst,_)
          new PositivePatternCall(body, Tuples.flatTupleOf(var___0_, var_trace, var_compinst, var___1_), Is_in_trace.instance().getInternalQueryRepresentation());
          // 	Aaxl2AaxlTrace.objectsToAttach(trace,featinst2add)
          new TypeConstraint(body, Tuples.flatTupleOf(var_trace), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_trace, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace", "objectsToAttach")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Element")));
          new Equality(body, var__virtual_0_, var_featinst2add);
          // 	ComponentInstance.classifier(compinst,comptype)
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance", "classifier")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ComponentClassifier")));
          new Equality(body, var__virtual_1_, var_comptype);
          bodies.add(body);
      }
      return bodies;
    }
  }
}
