/**
 * Generated from platform:/resource/fr.mem4csd.osatedim/src/fr/mem4csd/osatedim/viatra/queries/DIMQueriesInplace.vql
 */
package fr.mem4csd.osatedim.viatra.queries;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.viatra.query.runtime.api.IPatternMatch;
import org.eclipse.viatra.query.runtime.api.IQuerySpecification;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.viatra.query.runtime.api.impl.BaseMatcher;
import org.eclipse.viatra.query.runtime.api.impl.BasePatternMatch;
import org.eclipse.viatra.query.runtime.emf.types.EClassTransitiveInstancesKey;
import org.eclipse.viatra.query.runtime.emf.types.EStructuralFeatureInstancesKey;
import org.eclipse.viatra.query.runtime.matchers.backend.QueryEvaluationHint;
import org.eclipse.viatra.query.runtime.matchers.psystem.PBody;
import org.eclipse.viatra.query.runtime.matchers.psystem.PVariable;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.Equality;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.TypeConstraint;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameterDirection;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PVisibility;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuple;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuples;
import org.eclipse.viatra.query.runtime.util.ViatraQueryLoggingUtil;
import org.osate.aadl2.instance.ComponentInstance;

/**
 * A pattern-specific query specification that can instantiate Matcher in a type-safe way.
 * 
 * <p>Original source:
 *         <code><pre>
 *         // Find a subcomponent with a component that has a declared classifier
 *         pattern findSubcomponent(subcompinst : ComponentInstance, compinst : ComponentInstance)
 *         {
 *         	ComponentInstance.componentInstance(compinst,subcompinst);
 *         	ComponentInstance.classifier(compinst,_);
 *         } or {
 *         	ComponentInstance.componentInstance(compinst,subcompinst);
 *         	SystemInstance.componentImplementation(compinst,_);
 *         }
 * </pre></code>
 * 
 * @see Matcher
 * @see Match
 * 
 */
@SuppressWarnings("all")
public final class FindSubcomponent extends BaseGeneratedEMFQuerySpecification<FindSubcomponent.Matcher> {
  /**
   * Pattern-specific match representation of the fr.mem4csd.osatedim.viatra.queries.findSubcomponent pattern,
   * to be used in conjunction with {@link Matcher}.
   * 
   * <p>Class fields correspond to parameters of the pattern. Fields with value null are considered unassigned.
   * Each instance is a (possibly partial) substitution of pattern parameters,
   * usable to represent a match of the pattern in the result of a query,
   * or to specify the bound (fixed) input parameters when issuing a query.
   * 
   * @see Matcher
   * 
   */
  public static abstract class Match extends BasePatternMatch {
    private ComponentInstance fSubcompinst;

    private ComponentInstance fCompinst;

    private static List<String> parameterNames = makeImmutableList("subcompinst", "compinst");

    private Match(final ComponentInstance pSubcompinst, final ComponentInstance pCompinst) {
      this.fSubcompinst = pSubcompinst;
      this.fCompinst = pCompinst;
    }

    @Override
    public Object get(final String parameterName) {
      switch(parameterName) {
          case "subcompinst": return this.fSubcompinst;
          case "compinst": return this.fCompinst;
          default: return null;
      }
    }

    @Override
    public Object get(final int index) {
      switch(index) {
          case 0: return this.fSubcompinst;
          case 1: return this.fCompinst;
          default: return null;
      }
    }

    public ComponentInstance getSubcompinst() {
      return this.fSubcompinst;
    }

    public ComponentInstance getCompinst() {
      return this.fCompinst;
    }

    @Override
    public boolean set(final String parameterName, final Object newValue) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      if ("subcompinst".equals(parameterName) ) {
          this.fSubcompinst = (ComponentInstance) newValue;
          return true;
      }
      if ("compinst".equals(parameterName) ) {
          this.fCompinst = (ComponentInstance) newValue;
          return true;
      }
      return false;
    }

    public void setSubcompinst(final ComponentInstance pSubcompinst) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fSubcompinst = pSubcompinst;
    }

    public void setCompinst(final ComponentInstance pCompinst) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fCompinst = pCompinst;
    }

    @Override
    public String patternName() {
      return "fr.mem4csd.osatedim.viatra.queries.findSubcomponent";
    }

    @Override
    public List<String> parameterNames() {
      return FindSubcomponent.Match.parameterNames;
    }

    @Override
    public Object[] toArray() {
      return new Object[]{fSubcompinst, fCompinst};
    }

    @Override
    public FindSubcomponent.Match toImmutable() {
      return isMutable() ? newMatch(fSubcompinst, fCompinst) : this;
    }

    @Override
    public String prettyPrint() {
      StringBuilder result = new StringBuilder();
      result.append("\"subcompinst\"=" + prettyPrintValue(fSubcompinst) + ", ");
      result.append("\"compinst\"=" + prettyPrintValue(fCompinst));
      return result.toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(fSubcompinst, fCompinst);
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj)
          return true;
      if (obj == null) {
          return false;
      }
      if ((obj instanceof FindSubcomponent.Match)) {
          FindSubcomponent.Match other = (FindSubcomponent.Match) obj;
          return Objects.equals(fSubcompinst, other.fSubcompinst) && Objects.equals(fCompinst, other.fCompinst);
      } else {
          // this should be infrequent
          if (!(obj instanceof IPatternMatch)) {
              return false;
          }
          IPatternMatch otherSig  = (IPatternMatch) obj;
          return Objects.equals(specification(), otherSig.specification()) && Arrays.deepEquals(toArray(), otherSig.toArray());
      }
    }

    @Override
    public FindSubcomponent specification() {
      return FindSubcomponent.instance();
    }

    /**
     * Returns an empty, mutable match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @return the empty match.
     * 
     */
    public static FindSubcomponent.Match newEmptyMatch() {
      return new Mutable(null, null);
    }

    /**
     * Returns a mutable (partial) match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @param pSubcompinst the fixed value of pattern parameter subcompinst, or null if not bound.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @return the new, mutable (partial) match object.
     * 
     */
    public static FindSubcomponent.Match newMutableMatch(final ComponentInstance pSubcompinst, final ComponentInstance pCompinst) {
      return new Mutable(pSubcompinst, pCompinst);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pSubcompinst the fixed value of pattern parameter subcompinst, or null if not bound.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public static FindSubcomponent.Match newMatch(final ComponentInstance pSubcompinst, final ComponentInstance pCompinst) {
      return new Immutable(pSubcompinst, pCompinst);
    }

    private static final class Mutable extends FindSubcomponent.Match {
      Mutable(final ComponentInstance pSubcompinst, final ComponentInstance pCompinst) {
        super(pSubcompinst, pCompinst);
      }

      @Override
      public boolean isMutable() {
        return true;
      }
    }

    private static final class Immutable extends FindSubcomponent.Match {
      Immutable(final ComponentInstance pSubcompinst, final ComponentInstance pCompinst) {
        super(pSubcompinst, pCompinst);
      }

      @Override
      public boolean isMutable() {
        return false;
      }
    }
  }

  /**
   * Generated pattern matcher API of the fr.mem4csd.osatedim.viatra.queries.findSubcomponent pattern,
   * providing pattern-specific query methods.
   * 
   * <p>Use the pattern matcher on a given model via {@link #on(ViatraQueryEngine)},
   * e.g. in conjunction with {@link ViatraQueryEngine#on(QueryScope)}.
   * 
   * <p>Matches of the pattern will be represented as {@link Match}.
   * 
   * <p>Original source:
   * <code><pre>
   * // Find a subcomponent with a component that has a declared classifier
   * pattern findSubcomponent(subcompinst : ComponentInstance, compinst : ComponentInstance)
   * {
   * 	ComponentInstance.componentInstance(compinst,subcompinst);
   * 	ComponentInstance.classifier(compinst,_);
   * } or {
   * 	ComponentInstance.componentInstance(compinst,subcompinst);
   * 	SystemInstance.componentImplementation(compinst,_);
   * }
   * </pre></code>
   * 
   * @see Match
   * @see FindSubcomponent
   * 
   */
  public static class Matcher extends BaseMatcher<FindSubcomponent.Match> {
    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    public static FindSubcomponent.Matcher on(final ViatraQueryEngine engine) {
      // check if matcher already exists
      Matcher matcher = engine.getExistingMatcher(querySpecification());
      if (matcher == null) {
          matcher = (Matcher)engine.getMatcher(querySpecification());
      }
      return matcher;
    }

    /**
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * @return an initialized matcher
     * @noreference This method is for internal matcher initialization by the framework, do not call it manually.
     * 
     */
    public static FindSubcomponent.Matcher create() {
      return new Matcher();
    }

    private static final int POSITION_SUBCOMPINST = 0;

    private static final int POSITION_COMPINST = 1;

    private static final Logger LOGGER = ViatraQueryLoggingUtil.getLogger(FindSubcomponent.Matcher.class);

    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    private Matcher() {
      super(querySpecification());
    }

    /**
     * Returns the set of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pSubcompinst the fixed value of pattern parameter subcompinst, or null if not bound.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @return matches represented as a Match object.
     * 
     */
    public Collection<FindSubcomponent.Match> getAllMatches(final ComponentInstance pSubcompinst, final ComponentInstance pCompinst) {
      return rawStreamAllMatches(new Object[]{pSubcompinst, pCompinst}).collect(Collectors.toSet());
    }

    /**
     * Returns a stream of all matches of the pattern that conform to the given fixed values of some parameters.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     * @param pSubcompinst the fixed value of pattern parameter subcompinst, or null if not bound.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @return a stream of matches represented as a Match object.
     * 
     */
    public Stream<FindSubcomponent.Match> streamAllMatches(final ComponentInstance pSubcompinst, final ComponentInstance pCompinst) {
      return rawStreamAllMatches(new Object[]{pSubcompinst, pCompinst});
    }

    /**
     * Returns an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pSubcompinst the fixed value of pattern parameter subcompinst, or null if not bound.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @return a match represented as a Match object, or null if no match is found.
     * 
     */
    public Optional<FindSubcomponent.Match> getOneArbitraryMatch(final ComponentInstance pSubcompinst, final ComponentInstance pCompinst) {
      return rawGetOneArbitraryMatch(new Object[]{pSubcompinst, pCompinst});
    }

    /**
     * Indicates whether the given combination of specified pattern parameters constitute a valid pattern match,
     * under any possible substitution of the unspecified parameters (if any).
     * @param pSubcompinst the fixed value of pattern parameter subcompinst, or null if not bound.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @return true if the input is a valid (partial) match of the pattern.
     * 
     */
    public boolean hasMatch(final ComponentInstance pSubcompinst, final ComponentInstance pCompinst) {
      return rawHasMatch(new Object[]{pSubcompinst, pCompinst});
    }

    /**
     * Returns the number of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pSubcompinst the fixed value of pattern parameter subcompinst, or null if not bound.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @return the number of pattern matches found.
     * 
     */
    public int countMatches(final ComponentInstance pSubcompinst, final ComponentInstance pCompinst) {
      return rawCountMatches(new Object[]{pSubcompinst, pCompinst});
    }

    /**
     * Executes the given processor on an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pSubcompinst the fixed value of pattern parameter subcompinst, or null if not bound.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param processor the action that will process the selected match.
     * @return true if the pattern has at least one match with the given parameter values, false if the processor was not invoked
     * 
     */
    public boolean forOneArbitraryMatch(final ComponentInstance pSubcompinst, final ComponentInstance pCompinst, final Consumer<? super FindSubcomponent.Match> processor) {
      return rawForOneArbitraryMatch(new Object[]{pSubcompinst, pCompinst}, processor);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pSubcompinst the fixed value of pattern parameter subcompinst, or null if not bound.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public FindSubcomponent.Match newMatch(final ComponentInstance pSubcompinst, final ComponentInstance pCompinst) {
      return FindSubcomponent.Match.newMatch(pSubcompinst, pCompinst);
    }

    /**
     * Retrieve the set of values that occur in matches for subcompinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ComponentInstance> rawStreamAllValuesOfsubcompinst(final Object[] parameters) {
      return rawStreamAllValues(POSITION_SUBCOMPINST, parameters).map(ComponentInstance.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for subcompinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfsubcompinst() {
      return rawStreamAllValuesOfsubcompinst(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for subcompinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfsubcompinst() {
      return rawStreamAllValuesOfsubcompinst(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for subcompinst.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfsubcompinst(final FindSubcomponent.Match partialMatch) {
      return rawStreamAllValuesOfsubcompinst(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for subcompinst.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfsubcompinst(final ComponentInstance pCompinst) {
      return rawStreamAllValuesOfsubcompinst(new Object[]{null, pCompinst});
    }

    /**
     * Retrieve the set of values that occur in matches for subcompinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfsubcompinst(final FindSubcomponent.Match partialMatch) {
      return rawStreamAllValuesOfsubcompinst(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for subcompinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfsubcompinst(final ComponentInstance pCompinst) {
      return rawStreamAllValuesOfsubcompinst(new Object[]{null, pCompinst}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ComponentInstance> rawStreamAllValuesOfcompinst(final Object[] parameters) {
      return rawStreamAllValues(POSITION_COMPINST, parameters).map(ComponentInstance.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst() {
      return rawStreamAllValuesOfcompinst(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst() {
      return rawStreamAllValuesOfcompinst(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst(final FindSubcomponent.Match partialMatch) {
      return rawStreamAllValuesOfcompinst(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst(final ComponentInstance pSubcompinst) {
      return rawStreamAllValuesOfcompinst(new Object[]{pSubcompinst, null});
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst(final FindSubcomponent.Match partialMatch) {
      return rawStreamAllValuesOfcompinst(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst(final ComponentInstance pSubcompinst) {
      return rawStreamAllValuesOfcompinst(new Object[]{pSubcompinst, null}).collect(Collectors.toSet());
    }

    @Override
    protected FindSubcomponent.Match tupleToMatch(final Tuple t) {
      try {
          return FindSubcomponent.Match.newMatch((ComponentInstance) t.get(POSITION_SUBCOMPINST), (ComponentInstance) t.get(POSITION_COMPINST));
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in tuple not properly typed!",e);
          return null;
      }
    }

    @Override
    protected FindSubcomponent.Match arrayToMatch(final Object[] match) {
      try {
          return FindSubcomponent.Match.newMatch((ComponentInstance) match[POSITION_SUBCOMPINST], (ComponentInstance) match[POSITION_COMPINST]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    @Override
    protected FindSubcomponent.Match arrayToMatchMutable(final Object[] match) {
      try {
          return FindSubcomponent.Match.newMutableMatch((ComponentInstance) match[POSITION_SUBCOMPINST], (ComponentInstance) match[POSITION_COMPINST]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    /**
     * @return the singleton instance of the query specification of this pattern
     * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
     * 
     */
    public static IQuerySpecification<FindSubcomponent.Matcher> querySpecification() {
      return FindSubcomponent.instance();
    }
  }

  private FindSubcomponent() {
    super(GeneratedPQuery.INSTANCE);
  }

  /**
   * @return the singleton instance of the query specification
   * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
   * 
   */
  public static FindSubcomponent instance() {
    try{
        return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
        throw processInitializerError(err);
    }
  }

  @Override
  protected FindSubcomponent.Matcher instantiate(final ViatraQueryEngine engine) {
    return FindSubcomponent.Matcher.on(engine);
  }

  @Override
  public FindSubcomponent.Matcher instantiate() {
    return FindSubcomponent.Matcher.create();
  }

  @Override
  public FindSubcomponent.Match newEmptyMatch() {
    return FindSubcomponent.Match.newEmptyMatch();
  }

  @Override
  public FindSubcomponent.Match newMatch(final Object... parameters) {
    return FindSubcomponent.Match.newMatch((org.osate.aadl2.instance.ComponentInstance) parameters[0], (org.osate.aadl2.instance.ComponentInstance) parameters[1]);
  }

  /**
   * Inner class allowing the singleton instance of {@link FindSubcomponent} to be created 
   *     <b>not</b> at the class load time of the outer class, 
   *     but rather at the first call to {@link FindSubcomponent#instance()}.
   * 
   * <p> This workaround is required e.g. to support recursion.
   * 
   */
  private static class LazyHolder {
    private static final FindSubcomponent INSTANCE = new FindSubcomponent();

    /**
     * Statically initializes the query specification <b>after</b> the field {@link #INSTANCE} is assigned.
     * This initialization order is required to support indirect recursion.
     * 
     * <p> The static initializer is defined using a helper field to work around limitations of the code generator.
     * 
     */
    private static final Object STATIC_INITIALIZER = ensureInitialized();

    public static Object ensureInitialized() {
      INSTANCE.ensureInitializedInternal();
      return null;
    }
  }

  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private static final FindSubcomponent.GeneratedPQuery INSTANCE = new GeneratedPQuery();

    private final PParameter parameter_subcompinst = new PParameter("subcompinst", "org.osate.aadl2.instance.ComponentInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ComponentInstance")), PParameterDirection.INOUT);

    private final PParameter parameter_compinst = new PParameter("compinst", "org.osate.aadl2.instance.ComponentInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ComponentInstance")), PParameterDirection.INOUT);

    private final List<PParameter> parameters = Arrays.asList(parameter_subcompinst, parameter_compinst);

    private GeneratedPQuery() {
      super(PVisibility.PUBLIC);
    }

    @Override
    public String getFullyQualifiedName() {
      return "fr.mem4csd.osatedim.viatra.queries.findSubcomponent";
    }

    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("subcompinst","compinst");
    }

    @Override
    public List<PParameter> getParameters() {
      return parameters;
    }

    @Override
    public Set<PBody> doGetContainedBodies() {
      setEvaluationHints(new QueryEvaluationHint(null, QueryEvaluationHint.BackendRequirement.UNSPECIFIED));
      Set<PBody> bodies = new LinkedHashSet<>();
      {
          PBody body = new PBody(this);
          PVariable var_subcompinst = body.getOrCreateVariableByName("subcompinst");
          PVariable var_compinst = body.getOrCreateVariableByName("compinst");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_subcompinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_subcompinst, parameter_subcompinst),
             new ExportedParameter(body, var_compinst, parameter_compinst)
          ));
          // 	ComponentInstance.componentInstance(compinst,subcompinst)
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance", "componentInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          new Equality(body, var__virtual_0_, var_subcompinst);
          // 	ComponentInstance.classifier(compinst,_)
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance", "classifier")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ComponentClassifier")));
          new Equality(body, var__virtual_1_, var___0_);
          bodies.add(body);
      }
      {
          PBody body = new PBody(this);
          PVariable var_subcompinst = body.getOrCreateVariableByName("subcompinst");
          PVariable var_compinst = body.getOrCreateVariableByName("compinst");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_subcompinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_subcompinst, parameter_subcompinst),
             new ExportedParameter(body, var_compinst, parameter_compinst)
          ));
          // 	ComponentInstance.componentInstance(compinst,subcompinst)
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance", "componentInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          new Equality(body, var__virtual_0_, var_subcompinst);
          // 	SystemInstance.componentImplementation(compinst,_)
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "SystemInstance")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0/instance", "SystemInstance", "componentImplementation")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ComponentImplementation")));
          new Equality(body, var__virtual_1_, var___0_);
          bodies.add(body);
      }
      return bodies;
    }
  }
}
