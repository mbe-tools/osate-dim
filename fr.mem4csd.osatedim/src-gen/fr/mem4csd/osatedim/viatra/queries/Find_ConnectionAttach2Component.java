/**
 * Generated from platform:/resource/fr.mem4csd.osatedim/src/fr/mem4csd/osatedim/viatra/queries/DIMQueriesOutplace.vql
 */
package fr.mem4csd.osatedim.viatra.queries;

import fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.viatra.query.runtime.api.IPatternMatch;
import org.eclipse.viatra.query.runtime.api.IQuerySpecification;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.viatra.query.runtime.api.impl.BaseMatcher;
import org.eclipse.viatra.query.runtime.api.impl.BasePatternMatch;
import org.eclipse.viatra.query.runtime.emf.types.EClassTransitiveInstancesKey;
import org.eclipse.viatra.query.runtime.emf.types.EStructuralFeatureInstancesKey;
import org.eclipse.viatra.query.runtime.matchers.backend.QueryEvaluationHint;
import org.eclipse.viatra.query.runtime.matchers.psystem.PBody;
import org.eclipse.viatra.query.runtime.matchers.psystem.PVariable;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.Equality;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.PositivePatternCall;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.TypeConstraint;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameterDirection;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PVisibility;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuple;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuples;
import org.eclipse.viatra.query.runtime.util.ViatraQueryLoggingUtil;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.ConnectionInstance;
import org.osate.aadl2.instance.FeatureInstance;

/**
 * A pattern-specific query specification that can instantiate Matcher in a type-safe way.
 * 
 * <p>Original source:
 *         <code><pre>
 *         // This pattern finds the connection2Attach 
 *         pattern find_ConnectionAttach2Component(compinst : ComponentInstance,
 *         								  		trace : Aaxl2AaxlTrace,
 *         								  		conninst2add : ConnectionInstance,
 *         								  		srcfeatinst : FeatureInstance,
 *         								  		desfeatinst : FeatureInstance,
 *         								  		srcfeatcomp : ComponentInstance,
 *         								  		desfeatcomp : ComponentInstance)
 *         {
 *         	find is_in_trace(_,trace,compinst,_);
 *         	Aaxl2AaxlTrace.objectsToAttach(trace,conninst2add);
 *         	ConnectionInstance.source(conninst2add,srcfeatinst);
 *         	ConnectionInstance.destination(conninst2add,desfeatinst);
 *         	ComponentInstance.featureInstance(srcfeatcomp,srcfeatinst);
 *         	ComponentInstance.featureInstance(desfeatcomp,desfeatinst);
 *         }
 * </pre></code>
 * 
 * @see Matcher
 * @see Match
 * 
 */
@SuppressWarnings("all")
public final class Find_ConnectionAttach2Component extends BaseGeneratedEMFQuerySpecification<Find_ConnectionAttach2Component.Matcher> {
  /**
   * Pattern-specific match representation of the fr.mem4csd.osatedim.viatra.queries.find_ConnectionAttach2Component pattern,
   * to be used in conjunction with {@link Matcher}.
   * 
   * <p>Class fields correspond to parameters of the pattern. Fields with value null are considered unassigned.
   * Each instance is a (possibly partial) substitution of pattern parameters,
   * usable to represent a match of the pattern in the result of a query,
   * or to specify the bound (fixed) input parameters when issuing a query.
   * 
   * @see Matcher
   * 
   */
  public static abstract class Match extends BasePatternMatch {
    private ComponentInstance fCompinst;

    private Aaxl2AaxlTrace fTrace;

    private ConnectionInstance fConninst2add;

    private FeatureInstance fSrcfeatinst;

    private FeatureInstance fDesfeatinst;

    private ComponentInstance fSrcfeatcomp;

    private ComponentInstance fDesfeatcomp;

    private static List<String> parameterNames = makeImmutableList("compinst", "trace", "conninst2add", "srcfeatinst", "desfeatinst", "srcfeatcomp", "desfeatcomp");

    private Match(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ConnectionInstance pConninst2add, final FeatureInstance pSrcfeatinst, final FeatureInstance pDesfeatinst, final ComponentInstance pSrcfeatcomp, final ComponentInstance pDesfeatcomp) {
      this.fCompinst = pCompinst;
      this.fTrace = pTrace;
      this.fConninst2add = pConninst2add;
      this.fSrcfeatinst = pSrcfeatinst;
      this.fDesfeatinst = pDesfeatinst;
      this.fSrcfeatcomp = pSrcfeatcomp;
      this.fDesfeatcomp = pDesfeatcomp;
    }

    @Override
    public Object get(final String parameterName) {
      switch(parameterName) {
          case "compinst": return this.fCompinst;
          case "trace": return this.fTrace;
          case "conninst2add": return this.fConninst2add;
          case "srcfeatinst": return this.fSrcfeatinst;
          case "desfeatinst": return this.fDesfeatinst;
          case "srcfeatcomp": return this.fSrcfeatcomp;
          case "desfeatcomp": return this.fDesfeatcomp;
          default: return null;
      }
    }

    @Override
    public Object get(final int index) {
      switch(index) {
          case 0: return this.fCompinst;
          case 1: return this.fTrace;
          case 2: return this.fConninst2add;
          case 3: return this.fSrcfeatinst;
          case 4: return this.fDesfeatinst;
          case 5: return this.fSrcfeatcomp;
          case 6: return this.fDesfeatcomp;
          default: return null;
      }
    }

    public ComponentInstance getCompinst() {
      return this.fCompinst;
    }

    public Aaxl2AaxlTrace getTrace() {
      return this.fTrace;
    }

    public ConnectionInstance getConninst2add() {
      return this.fConninst2add;
    }

    public FeatureInstance getSrcfeatinst() {
      return this.fSrcfeatinst;
    }

    public FeatureInstance getDesfeatinst() {
      return this.fDesfeatinst;
    }

    public ComponentInstance getSrcfeatcomp() {
      return this.fSrcfeatcomp;
    }

    public ComponentInstance getDesfeatcomp() {
      return this.fDesfeatcomp;
    }

    @Override
    public boolean set(final String parameterName, final Object newValue) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      if ("compinst".equals(parameterName) ) {
          this.fCompinst = (ComponentInstance) newValue;
          return true;
      }
      if ("trace".equals(parameterName) ) {
          this.fTrace = (Aaxl2AaxlTrace) newValue;
          return true;
      }
      if ("conninst2add".equals(parameterName) ) {
          this.fConninst2add = (ConnectionInstance) newValue;
          return true;
      }
      if ("srcfeatinst".equals(parameterName) ) {
          this.fSrcfeatinst = (FeatureInstance) newValue;
          return true;
      }
      if ("desfeatinst".equals(parameterName) ) {
          this.fDesfeatinst = (FeatureInstance) newValue;
          return true;
      }
      if ("srcfeatcomp".equals(parameterName) ) {
          this.fSrcfeatcomp = (ComponentInstance) newValue;
          return true;
      }
      if ("desfeatcomp".equals(parameterName) ) {
          this.fDesfeatcomp = (ComponentInstance) newValue;
          return true;
      }
      return false;
    }

    public void setCompinst(final ComponentInstance pCompinst) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fCompinst = pCompinst;
    }

    public void setTrace(final Aaxl2AaxlTrace pTrace) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fTrace = pTrace;
    }

    public void setConninst2add(final ConnectionInstance pConninst2add) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fConninst2add = pConninst2add;
    }

    public void setSrcfeatinst(final FeatureInstance pSrcfeatinst) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fSrcfeatinst = pSrcfeatinst;
    }

    public void setDesfeatinst(final FeatureInstance pDesfeatinst) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fDesfeatinst = pDesfeatinst;
    }

    public void setSrcfeatcomp(final ComponentInstance pSrcfeatcomp) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fSrcfeatcomp = pSrcfeatcomp;
    }

    public void setDesfeatcomp(final ComponentInstance pDesfeatcomp) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fDesfeatcomp = pDesfeatcomp;
    }

    @Override
    public String patternName() {
      return "fr.mem4csd.osatedim.viatra.queries.find_ConnectionAttach2Component";
    }

    @Override
    public List<String> parameterNames() {
      return Find_ConnectionAttach2Component.Match.parameterNames;
    }

    @Override
    public Object[] toArray() {
      return new Object[]{fCompinst, fTrace, fConninst2add, fSrcfeatinst, fDesfeatinst, fSrcfeatcomp, fDesfeatcomp};
    }

    @Override
    public Find_ConnectionAttach2Component.Match toImmutable() {
      return isMutable() ? newMatch(fCompinst, fTrace, fConninst2add, fSrcfeatinst, fDesfeatinst, fSrcfeatcomp, fDesfeatcomp) : this;
    }

    @Override
    public String prettyPrint() {
      StringBuilder result = new StringBuilder();
      result.append("\"compinst\"=" + prettyPrintValue(fCompinst) + ", ");
      result.append("\"trace\"=" + prettyPrintValue(fTrace) + ", ");
      result.append("\"conninst2add\"=" + prettyPrintValue(fConninst2add) + ", ");
      result.append("\"srcfeatinst\"=" + prettyPrintValue(fSrcfeatinst) + ", ");
      result.append("\"desfeatinst\"=" + prettyPrintValue(fDesfeatinst) + ", ");
      result.append("\"srcfeatcomp\"=" + prettyPrintValue(fSrcfeatcomp) + ", ");
      result.append("\"desfeatcomp\"=" + prettyPrintValue(fDesfeatcomp));
      return result.toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(fCompinst, fTrace, fConninst2add, fSrcfeatinst, fDesfeatinst, fSrcfeatcomp, fDesfeatcomp);
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj)
          return true;
      if (obj == null) {
          return false;
      }
      if ((obj instanceof Find_ConnectionAttach2Component.Match)) {
          Find_ConnectionAttach2Component.Match other = (Find_ConnectionAttach2Component.Match) obj;
          return Objects.equals(fCompinst, other.fCompinst) && Objects.equals(fTrace, other.fTrace) && Objects.equals(fConninst2add, other.fConninst2add) && Objects.equals(fSrcfeatinst, other.fSrcfeatinst) && Objects.equals(fDesfeatinst, other.fDesfeatinst) && Objects.equals(fSrcfeatcomp, other.fSrcfeatcomp) && Objects.equals(fDesfeatcomp, other.fDesfeatcomp);
      } else {
          // this should be infrequent
          if (!(obj instanceof IPatternMatch)) {
              return false;
          }
          IPatternMatch otherSig  = (IPatternMatch) obj;
          return Objects.equals(specification(), otherSig.specification()) && Arrays.deepEquals(toArray(), otherSig.toArray());
      }
    }

    @Override
    public Find_ConnectionAttach2Component specification() {
      return Find_ConnectionAttach2Component.instance();
    }

    /**
     * Returns an empty, mutable match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @return the empty match.
     * 
     */
    public static Find_ConnectionAttach2Component.Match newEmptyMatch() {
      return new Mutable(null, null, null, null, null, null, null);
    }

    /**
     * Returns a mutable (partial) match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pConninst2add the fixed value of pattern parameter conninst2add, or null if not bound.
     * @param pSrcfeatinst the fixed value of pattern parameter srcfeatinst, or null if not bound.
     * @param pDesfeatinst the fixed value of pattern parameter desfeatinst, or null if not bound.
     * @param pSrcfeatcomp the fixed value of pattern parameter srcfeatcomp, or null if not bound.
     * @param pDesfeatcomp the fixed value of pattern parameter desfeatcomp, or null if not bound.
     * @return the new, mutable (partial) match object.
     * 
     */
    public static Find_ConnectionAttach2Component.Match newMutableMatch(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ConnectionInstance pConninst2add, final FeatureInstance pSrcfeatinst, final FeatureInstance pDesfeatinst, final ComponentInstance pSrcfeatcomp, final ComponentInstance pDesfeatcomp) {
      return new Mutable(pCompinst, pTrace, pConninst2add, pSrcfeatinst, pDesfeatinst, pSrcfeatcomp, pDesfeatcomp);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pConninst2add the fixed value of pattern parameter conninst2add, or null if not bound.
     * @param pSrcfeatinst the fixed value of pattern parameter srcfeatinst, or null if not bound.
     * @param pDesfeatinst the fixed value of pattern parameter desfeatinst, or null if not bound.
     * @param pSrcfeatcomp the fixed value of pattern parameter srcfeatcomp, or null if not bound.
     * @param pDesfeatcomp the fixed value of pattern parameter desfeatcomp, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public static Find_ConnectionAttach2Component.Match newMatch(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ConnectionInstance pConninst2add, final FeatureInstance pSrcfeatinst, final FeatureInstance pDesfeatinst, final ComponentInstance pSrcfeatcomp, final ComponentInstance pDesfeatcomp) {
      return new Immutable(pCompinst, pTrace, pConninst2add, pSrcfeatinst, pDesfeatinst, pSrcfeatcomp, pDesfeatcomp);
    }

    private static final class Mutable extends Find_ConnectionAttach2Component.Match {
      Mutable(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ConnectionInstance pConninst2add, final FeatureInstance pSrcfeatinst, final FeatureInstance pDesfeatinst, final ComponentInstance pSrcfeatcomp, final ComponentInstance pDesfeatcomp) {
        super(pCompinst, pTrace, pConninst2add, pSrcfeatinst, pDesfeatinst, pSrcfeatcomp, pDesfeatcomp);
      }

      @Override
      public boolean isMutable() {
        return true;
      }
    }

    private static final class Immutable extends Find_ConnectionAttach2Component.Match {
      Immutable(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ConnectionInstance pConninst2add, final FeatureInstance pSrcfeatinst, final FeatureInstance pDesfeatinst, final ComponentInstance pSrcfeatcomp, final ComponentInstance pDesfeatcomp) {
        super(pCompinst, pTrace, pConninst2add, pSrcfeatinst, pDesfeatinst, pSrcfeatcomp, pDesfeatcomp);
      }

      @Override
      public boolean isMutable() {
        return false;
      }
    }
  }

  /**
   * Generated pattern matcher API of the fr.mem4csd.osatedim.viatra.queries.find_ConnectionAttach2Component pattern,
   * providing pattern-specific query methods.
   * 
   * <p>Use the pattern matcher on a given model via {@link #on(ViatraQueryEngine)},
   * e.g. in conjunction with {@link ViatraQueryEngine#on(QueryScope)}.
   * 
   * <p>Matches of the pattern will be represented as {@link Match}.
   * 
   * <p>Original source:
   * <code><pre>
   * // This pattern finds the connection2Attach 
   * pattern find_ConnectionAttach2Component(compinst : ComponentInstance,
   * 								  		trace : Aaxl2AaxlTrace,
   * 								  		conninst2add : ConnectionInstance,
   * 								  		srcfeatinst : FeatureInstance,
   * 								  		desfeatinst : FeatureInstance,
   * 								  		srcfeatcomp : ComponentInstance,
   * 								  		desfeatcomp : ComponentInstance)
   * {
   * 	find is_in_trace(_,trace,compinst,_);
   * 	Aaxl2AaxlTrace.objectsToAttach(trace,conninst2add);
   * 	ConnectionInstance.source(conninst2add,srcfeatinst);
   * 	ConnectionInstance.destination(conninst2add,desfeatinst);
   * 	ComponentInstance.featureInstance(srcfeatcomp,srcfeatinst);
   * 	ComponentInstance.featureInstance(desfeatcomp,desfeatinst);
   * }
   * </pre></code>
   * 
   * @see Match
   * @see Find_ConnectionAttach2Component
   * 
   */
  public static class Matcher extends BaseMatcher<Find_ConnectionAttach2Component.Match> {
    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    public static Find_ConnectionAttach2Component.Matcher on(final ViatraQueryEngine engine) {
      // check if matcher already exists
      Matcher matcher = engine.getExistingMatcher(querySpecification());
      if (matcher == null) {
          matcher = (Matcher)engine.getMatcher(querySpecification());
      }
      return matcher;
    }

    /**
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * @return an initialized matcher
     * @noreference This method is for internal matcher initialization by the framework, do not call it manually.
     * 
     */
    public static Find_ConnectionAttach2Component.Matcher create() {
      return new Matcher();
    }

    private static final int POSITION_COMPINST = 0;

    private static final int POSITION_TRACE = 1;

    private static final int POSITION_CONNINST2ADD = 2;

    private static final int POSITION_SRCFEATINST = 3;

    private static final int POSITION_DESFEATINST = 4;

    private static final int POSITION_SRCFEATCOMP = 5;

    private static final int POSITION_DESFEATCOMP = 6;

    private static final Logger LOGGER = ViatraQueryLoggingUtil.getLogger(Find_ConnectionAttach2Component.Matcher.class);

    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    private Matcher() {
      super(querySpecification());
    }

    /**
     * Returns the set of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pConninst2add the fixed value of pattern parameter conninst2add, or null if not bound.
     * @param pSrcfeatinst the fixed value of pattern parameter srcfeatinst, or null if not bound.
     * @param pDesfeatinst the fixed value of pattern parameter desfeatinst, or null if not bound.
     * @param pSrcfeatcomp the fixed value of pattern parameter srcfeatcomp, or null if not bound.
     * @param pDesfeatcomp the fixed value of pattern parameter desfeatcomp, or null if not bound.
     * @return matches represented as a Match object.
     * 
     */
    public Collection<Find_ConnectionAttach2Component.Match> getAllMatches(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ConnectionInstance pConninst2add, final FeatureInstance pSrcfeatinst, final FeatureInstance pDesfeatinst, final ComponentInstance pSrcfeatcomp, final ComponentInstance pDesfeatcomp) {
      return rawStreamAllMatches(new Object[]{pCompinst, pTrace, pConninst2add, pSrcfeatinst, pDesfeatinst, pSrcfeatcomp, pDesfeatcomp}).collect(Collectors.toSet());
    }

    /**
     * Returns a stream of all matches of the pattern that conform to the given fixed values of some parameters.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pConninst2add the fixed value of pattern parameter conninst2add, or null if not bound.
     * @param pSrcfeatinst the fixed value of pattern parameter srcfeatinst, or null if not bound.
     * @param pDesfeatinst the fixed value of pattern parameter desfeatinst, or null if not bound.
     * @param pSrcfeatcomp the fixed value of pattern parameter srcfeatcomp, or null if not bound.
     * @param pDesfeatcomp the fixed value of pattern parameter desfeatcomp, or null if not bound.
     * @return a stream of matches represented as a Match object.
     * 
     */
    public Stream<Find_ConnectionAttach2Component.Match> streamAllMatches(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ConnectionInstance pConninst2add, final FeatureInstance pSrcfeatinst, final FeatureInstance pDesfeatinst, final ComponentInstance pSrcfeatcomp, final ComponentInstance pDesfeatcomp) {
      return rawStreamAllMatches(new Object[]{pCompinst, pTrace, pConninst2add, pSrcfeatinst, pDesfeatinst, pSrcfeatcomp, pDesfeatcomp});
    }

    /**
     * Returns an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pConninst2add the fixed value of pattern parameter conninst2add, or null if not bound.
     * @param pSrcfeatinst the fixed value of pattern parameter srcfeatinst, or null if not bound.
     * @param pDesfeatinst the fixed value of pattern parameter desfeatinst, or null if not bound.
     * @param pSrcfeatcomp the fixed value of pattern parameter srcfeatcomp, or null if not bound.
     * @param pDesfeatcomp the fixed value of pattern parameter desfeatcomp, or null if not bound.
     * @return a match represented as a Match object, or null if no match is found.
     * 
     */
    public Optional<Find_ConnectionAttach2Component.Match> getOneArbitraryMatch(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ConnectionInstance pConninst2add, final FeatureInstance pSrcfeatinst, final FeatureInstance pDesfeatinst, final ComponentInstance pSrcfeatcomp, final ComponentInstance pDesfeatcomp) {
      return rawGetOneArbitraryMatch(new Object[]{pCompinst, pTrace, pConninst2add, pSrcfeatinst, pDesfeatinst, pSrcfeatcomp, pDesfeatcomp});
    }

    /**
     * Indicates whether the given combination of specified pattern parameters constitute a valid pattern match,
     * under any possible substitution of the unspecified parameters (if any).
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pConninst2add the fixed value of pattern parameter conninst2add, or null if not bound.
     * @param pSrcfeatinst the fixed value of pattern parameter srcfeatinst, or null if not bound.
     * @param pDesfeatinst the fixed value of pattern parameter desfeatinst, or null if not bound.
     * @param pSrcfeatcomp the fixed value of pattern parameter srcfeatcomp, or null if not bound.
     * @param pDesfeatcomp the fixed value of pattern parameter desfeatcomp, or null if not bound.
     * @return true if the input is a valid (partial) match of the pattern.
     * 
     */
    public boolean hasMatch(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ConnectionInstance pConninst2add, final FeatureInstance pSrcfeatinst, final FeatureInstance pDesfeatinst, final ComponentInstance pSrcfeatcomp, final ComponentInstance pDesfeatcomp) {
      return rawHasMatch(new Object[]{pCompinst, pTrace, pConninst2add, pSrcfeatinst, pDesfeatinst, pSrcfeatcomp, pDesfeatcomp});
    }

    /**
     * Returns the number of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pConninst2add the fixed value of pattern parameter conninst2add, or null if not bound.
     * @param pSrcfeatinst the fixed value of pattern parameter srcfeatinst, or null if not bound.
     * @param pDesfeatinst the fixed value of pattern parameter desfeatinst, or null if not bound.
     * @param pSrcfeatcomp the fixed value of pattern parameter srcfeatcomp, or null if not bound.
     * @param pDesfeatcomp the fixed value of pattern parameter desfeatcomp, or null if not bound.
     * @return the number of pattern matches found.
     * 
     */
    public int countMatches(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ConnectionInstance pConninst2add, final FeatureInstance pSrcfeatinst, final FeatureInstance pDesfeatinst, final ComponentInstance pSrcfeatcomp, final ComponentInstance pDesfeatcomp) {
      return rawCountMatches(new Object[]{pCompinst, pTrace, pConninst2add, pSrcfeatinst, pDesfeatinst, pSrcfeatcomp, pDesfeatcomp});
    }

    /**
     * Executes the given processor on an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pConninst2add the fixed value of pattern parameter conninst2add, or null if not bound.
     * @param pSrcfeatinst the fixed value of pattern parameter srcfeatinst, or null if not bound.
     * @param pDesfeatinst the fixed value of pattern parameter desfeatinst, or null if not bound.
     * @param pSrcfeatcomp the fixed value of pattern parameter srcfeatcomp, or null if not bound.
     * @param pDesfeatcomp the fixed value of pattern parameter desfeatcomp, or null if not bound.
     * @param processor the action that will process the selected match.
     * @return true if the pattern has at least one match with the given parameter values, false if the processor was not invoked
     * 
     */
    public boolean forOneArbitraryMatch(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ConnectionInstance pConninst2add, final FeatureInstance pSrcfeatinst, final FeatureInstance pDesfeatinst, final ComponentInstance pSrcfeatcomp, final ComponentInstance pDesfeatcomp, final Consumer<? super Find_ConnectionAttach2Component.Match> processor) {
      return rawForOneArbitraryMatch(new Object[]{pCompinst, pTrace, pConninst2add, pSrcfeatinst, pDesfeatinst, pSrcfeatcomp, pDesfeatcomp}, processor);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pConninst2add the fixed value of pattern parameter conninst2add, or null if not bound.
     * @param pSrcfeatinst the fixed value of pattern parameter srcfeatinst, or null if not bound.
     * @param pDesfeatinst the fixed value of pattern parameter desfeatinst, or null if not bound.
     * @param pSrcfeatcomp the fixed value of pattern parameter srcfeatcomp, or null if not bound.
     * @param pDesfeatcomp the fixed value of pattern parameter desfeatcomp, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public Find_ConnectionAttach2Component.Match newMatch(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ConnectionInstance pConninst2add, final FeatureInstance pSrcfeatinst, final FeatureInstance pDesfeatinst, final ComponentInstance pSrcfeatcomp, final ComponentInstance pDesfeatcomp) {
      return Find_ConnectionAttach2Component.Match.newMatch(pCompinst, pTrace, pConninst2add, pSrcfeatinst, pDesfeatinst, pSrcfeatcomp, pDesfeatcomp);
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ComponentInstance> rawStreamAllValuesOfcompinst(final Object[] parameters) {
      return rawStreamAllValues(POSITION_COMPINST, parameters).map(ComponentInstance.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst() {
      return rawStreamAllValuesOfcompinst(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst() {
      return rawStreamAllValuesOfcompinst(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst(final Find_ConnectionAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfcompinst(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst(final Aaxl2AaxlTrace pTrace, final ConnectionInstance pConninst2add, final FeatureInstance pSrcfeatinst, final FeatureInstance pDesfeatinst, final ComponentInstance pSrcfeatcomp, final ComponentInstance pDesfeatcomp) {
      return rawStreamAllValuesOfcompinst(new Object[]{null, pTrace, pConninst2add, pSrcfeatinst, pDesfeatinst, pSrcfeatcomp, pDesfeatcomp});
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst(final Find_ConnectionAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfcompinst(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst(final Aaxl2AaxlTrace pTrace, final ConnectionInstance pConninst2add, final FeatureInstance pSrcfeatinst, final FeatureInstance pDesfeatinst, final ComponentInstance pSrcfeatcomp, final ComponentInstance pDesfeatcomp) {
      return rawStreamAllValuesOfcompinst(new Object[]{null, pTrace, pConninst2add, pSrcfeatinst, pDesfeatinst, pSrcfeatcomp, pDesfeatcomp}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<Aaxl2AaxlTrace> rawStreamAllValuesOftrace(final Object[] parameters) {
      return rawStreamAllValues(POSITION_TRACE, parameters).map(Aaxl2AaxlTrace.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Aaxl2AaxlTrace> getAllValuesOftrace() {
      return rawStreamAllValuesOftrace(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<Aaxl2AaxlTrace> streamAllValuesOftrace() {
      return rawStreamAllValuesOftrace(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<Aaxl2AaxlTrace> streamAllValuesOftrace(final Find_ConnectionAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOftrace(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<Aaxl2AaxlTrace> streamAllValuesOftrace(final ComponentInstance pCompinst, final ConnectionInstance pConninst2add, final FeatureInstance pSrcfeatinst, final FeatureInstance pDesfeatinst, final ComponentInstance pSrcfeatcomp, final ComponentInstance pDesfeatcomp) {
      return rawStreamAllValuesOftrace(new Object[]{pCompinst, null, pConninst2add, pSrcfeatinst, pDesfeatinst, pSrcfeatcomp, pDesfeatcomp});
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Aaxl2AaxlTrace> getAllValuesOftrace(final Find_ConnectionAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOftrace(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Aaxl2AaxlTrace> getAllValuesOftrace(final ComponentInstance pCompinst, final ConnectionInstance pConninst2add, final FeatureInstance pSrcfeatinst, final FeatureInstance pDesfeatinst, final ComponentInstance pSrcfeatcomp, final ComponentInstance pDesfeatcomp) {
      return rawStreamAllValuesOftrace(new Object[]{pCompinst, null, pConninst2add, pSrcfeatinst, pDesfeatinst, pSrcfeatcomp, pDesfeatcomp}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for conninst2add.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ConnectionInstance> rawStreamAllValuesOfconninst2add(final Object[] parameters) {
      return rawStreamAllValues(POSITION_CONNINST2ADD, parameters).map(ConnectionInstance.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for conninst2add.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstance> getAllValuesOfconninst2add() {
      return rawStreamAllValuesOfconninst2add(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for conninst2add.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstance> streamAllValuesOfconninst2add() {
      return rawStreamAllValuesOfconninst2add(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for conninst2add.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstance> streamAllValuesOfconninst2add(final Find_ConnectionAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfconninst2add(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for conninst2add.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstance> streamAllValuesOfconninst2add(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final FeatureInstance pSrcfeatinst, final FeatureInstance pDesfeatinst, final ComponentInstance pSrcfeatcomp, final ComponentInstance pDesfeatcomp) {
      return rawStreamAllValuesOfconninst2add(new Object[]{pCompinst, pTrace, null, pSrcfeatinst, pDesfeatinst, pSrcfeatcomp, pDesfeatcomp});
    }

    /**
     * Retrieve the set of values that occur in matches for conninst2add.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstance> getAllValuesOfconninst2add(final Find_ConnectionAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfconninst2add(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for conninst2add.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstance> getAllValuesOfconninst2add(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final FeatureInstance pSrcfeatinst, final FeatureInstance pDesfeatinst, final ComponentInstance pSrcfeatcomp, final ComponentInstance pDesfeatcomp) {
      return rawStreamAllValuesOfconninst2add(new Object[]{pCompinst, pTrace, null, pSrcfeatinst, pDesfeatinst, pSrcfeatcomp, pDesfeatcomp}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for srcfeatinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<FeatureInstance> rawStreamAllValuesOfsrcfeatinst(final Object[] parameters) {
      return rawStreamAllValues(POSITION_SRCFEATINST, parameters).map(FeatureInstance.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for srcfeatinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<FeatureInstance> getAllValuesOfsrcfeatinst() {
      return rawStreamAllValuesOfsrcfeatinst(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for srcfeatinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<FeatureInstance> streamAllValuesOfsrcfeatinst() {
      return rawStreamAllValuesOfsrcfeatinst(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for srcfeatinst.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<FeatureInstance> streamAllValuesOfsrcfeatinst(final Find_ConnectionAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfsrcfeatinst(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for srcfeatinst.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<FeatureInstance> streamAllValuesOfsrcfeatinst(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ConnectionInstance pConninst2add, final FeatureInstance pDesfeatinst, final ComponentInstance pSrcfeatcomp, final ComponentInstance pDesfeatcomp) {
      return rawStreamAllValuesOfsrcfeatinst(new Object[]{pCompinst, pTrace, pConninst2add, null, pDesfeatinst, pSrcfeatcomp, pDesfeatcomp});
    }

    /**
     * Retrieve the set of values that occur in matches for srcfeatinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<FeatureInstance> getAllValuesOfsrcfeatinst(final Find_ConnectionAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfsrcfeatinst(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for srcfeatinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<FeatureInstance> getAllValuesOfsrcfeatinst(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ConnectionInstance pConninst2add, final FeatureInstance pDesfeatinst, final ComponentInstance pSrcfeatcomp, final ComponentInstance pDesfeatcomp) {
      return rawStreamAllValuesOfsrcfeatinst(new Object[]{pCompinst, pTrace, pConninst2add, null, pDesfeatinst, pSrcfeatcomp, pDesfeatcomp}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for desfeatinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<FeatureInstance> rawStreamAllValuesOfdesfeatinst(final Object[] parameters) {
      return rawStreamAllValues(POSITION_DESFEATINST, parameters).map(FeatureInstance.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for desfeatinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<FeatureInstance> getAllValuesOfdesfeatinst() {
      return rawStreamAllValuesOfdesfeatinst(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for desfeatinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<FeatureInstance> streamAllValuesOfdesfeatinst() {
      return rawStreamAllValuesOfdesfeatinst(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for desfeatinst.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<FeatureInstance> streamAllValuesOfdesfeatinst(final Find_ConnectionAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfdesfeatinst(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for desfeatinst.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<FeatureInstance> streamAllValuesOfdesfeatinst(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ConnectionInstance pConninst2add, final FeatureInstance pSrcfeatinst, final ComponentInstance pSrcfeatcomp, final ComponentInstance pDesfeatcomp) {
      return rawStreamAllValuesOfdesfeatinst(new Object[]{pCompinst, pTrace, pConninst2add, pSrcfeatinst, null, pSrcfeatcomp, pDesfeatcomp});
    }

    /**
     * Retrieve the set of values that occur in matches for desfeatinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<FeatureInstance> getAllValuesOfdesfeatinst(final Find_ConnectionAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfdesfeatinst(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for desfeatinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<FeatureInstance> getAllValuesOfdesfeatinst(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ConnectionInstance pConninst2add, final FeatureInstance pSrcfeatinst, final ComponentInstance pSrcfeatcomp, final ComponentInstance pDesfeatcomp) {
      return rawStreamAllValuesOfdesfeatinst(new Object[]{pCompinst, pTrace, pConninst2add, pSrcfeatinst, null, pSrcfeatcomp, pDesfeatcomp}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for srcfeatcomp.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ComponentInstance> rawStreamAllValuesOfsrcfeatcomp(final Object[] parameters) {
      return rawStreamAllValues(POSITION_SRCFEATCOMP, parameters).map(ComponentInstance.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for srcfeatcomp.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfsrcfeatcomp() {
      return rawStreamAllValuesOfsrcfeatcomp(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for srcfeatcomp.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfsrcfeatcomp() {
      return rawStreamAllValuesOfsrcfeatcomp(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for srcfeatcomp.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfsrcfeatcomp(final Find_ConnectionAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfsrcfeatcomp(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for srcfeatcomp.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfsrcfeatcomp(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ConnectionInstance pConninst2add, final FeatureInstance pSrcfeatinst, final FeatureInstance pDesfeatinst, final ComponentInstance pDesfeatcomp) {
      return rawStreamAllValuesOfsrcfeatcomp(new Object[]{pCompinst, pTrace, pConninst2add, pSrcfeatinst, pDesfeatinst, null, pDesfeatcomp});
    }

    /**
     * Retrieve the set of values that occur in matches for srcfeatcomp.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfsrcfeatcomp(final Find_ConnectionAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfsrcfeatcomp(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for srcfeatcomp.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfsrcfeatcomp(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ConnectionInstance pConninst2add, final FeatureInstance pSrcfeatinst, final FeatureInstance pDesfeatinst, final ComponentInstance pDesfeatcomp) {
      return rawStreamAllValuesOfsrcfeatcomp(new Object[]{pCompinst, pTrace, pConninst2add, pSrcfeatinst, pDesfeatinst, null, pDesfeatcomp}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for desfeatcomp.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ComponentInstance> rawStreamAllValuesOfdesfeatcomp(final Object[] parameters) {
      return rawStreamAllValues(POSITION_DESFEATCOMP, parameters).map(ComponentInstance.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for desfeatcomp.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfdesfeatcomp() {
      return rawStreamAllValuesOfdesfeatcomp(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for desfeatcomp.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfdesfeatcomp() {
      return rawStreamAllValuesOfdesfeatcomp(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for desfeatcomp.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfdesfeatcomp(final Find_ConnectionAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfdesfeatcomp(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for desfeatcomp.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfdesfeatcomp(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ConnectionInstance pConninst2add, final FeatureInstance pSrcfeatinst, final FeatureInstance pDesfeatinst, final ComponentInstance pSrcfeatcomp) {
      return rawStreamAllValuesOfdesfeatcomp(new Object[]{pCompinst, pTrace, pConninst2add, pSrcfeatinst, pDesfeatinst, pSrcfeatcomp, null});
    }

    /**
     * Retrieve the set of values that occur in matches for desfeatcomp.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfdesfeatcomp(final Find_ConnectionAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfdesfeatcomp(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for desfeatcomp.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfdesfeatcomp(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ConnectionInstance pConninst2add, final FeatureInstance pSrcfeatinst, final FeatureInstance pDesfeatinst, final ComponentInstance pSrcfeatcomp) {
      return rawStreamAllValuesOfdesfeatcomp(new Object[]{pCompinst, pTrace, pConninst2add, pSrcfeatinst, pDesfeatinst, pSrcfeatcomp, null}).collect(Collectors.toSet());
    }

    @Override
    protected Find_ConnectionAttach2Component.Match tupleToMatch(final Tuple t) {
      try {
          return Find_ConnectionAttach2Component.Match.newMatch((ComponentInstance) t.get(POSITION_COMPINST), (Aaxl2AaxlTrace) t.get(POSITION_TRACE), (ConnectionInstance) t.get(POSITION_CONNINST2ADD), (FeatureInstance) t.get(POSITION_SRCFEATINST), (FeatureInstance) t.get(POSITION_DESFEATINST), (ComponentInstance) t.get(POSITION_SRCFEATCOMP), (ComponentInstance) t.get(POSITION_DESFEATCOMP));
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in tuple not properly typed!",e);
          return null;
      }
    }

    @Override
    protected Find_ConnectionAttach2Component.Match arrayToMatch(final Object[] match) {
      try {
          return Find_ConnectionAttach2Component.Match.newMatch((ComponentInstance) match[POSITION_COMPINST], (Aaxl2AaxlTrace) match[POSITION_TRACE], (ConnectionInstance) match[POSITION_CONNINST2ADD], (FeatureInstance) match[POSITION_SRCFEATINST], (FeatureInstance) match[POSITION_DESFEATINST], (ComponentInstance) match[POSITION_SRCFEATCOMP], (ComponentInstance) match[POSITION_DESFEATCOMP]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    @Override
    protected Find_ConnectionAttach2Component.Match arrayToMatchMutable(final Object[] match) {
      try {
          return Find_ConnectionAttach2Component.Match.newMutableMatch((ComponentInstance) match[POSITION_COMPINST], (Aaxl2AaxlTrace) match[POSITION_TRACE], (ConnectionInstance) match[POSITION_CONNINST2ADD], (FeatureInstance) match[POSITION_SRCFEATINST], (FeatureInstance) match[POSITION_DESFEATINST], (ComponentInstance) match[POSITION_SRCFEATCOMP], (ComponentInstance) match[POSITION_DESFEATCOMP]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    /**
     * @return the singleton instance of the query specification of this pattern
     * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
     * 
     */
    public static IQuerySpecification<Find_ConnectionAttach2Component.Matcher> querySpecification() {
      return Find_ConnectionAttach2Component.instance();
    }
  }

  private Find_ConnectionAttach2Component() {
    super(GeneratedPQuery.INSTANCE);
  }

  /**
   * @return the singleton instance of the query specification
   * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
   * 
   */
  public static Find_ConnectionAttach2Component instance() {
    try{
        return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
        throw processInitializerError(err);
    }
  }

  @Override
  protected Find_ConnectionAttach2Component.Matcher instantiate(final ViatraQueryEngine engine) {
    return Find_ConnectionAttach2Component.Matcher.on(engine);
  }

  @Override
  public Find_ConnectionAttach2Component.Matcher instantiate() {
    return Find_ConnectionAttach2Component.Matcher.create();
  }

  @Override
  public Find_ConnectionAttach2Component.Match newEmptyMatch() {
    return Find_ConnectionAttach2Component.Match.newEmptyMatch();
  }

  @Override
  public Find_ConnectionAttach2Component.Match newMatch(final Object... parameters) {
    return Find_ConnectionAttach2Component.Match.newMatch((org.osate.aadl2.instance.ComponentInstance) parameters[0], (fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace) parameters[1], (org.osate.aadl2.instance.ConnectionInstance) parameters[2], (org.osate.aadl2.instance.FeatureInstance) parameters[3], (org.osate.aadl2.instance.FeatureInstance) parameters[4], (org.osate.aadl2.instance.ComponentInstance) parameters[5], (org.osate.aadl2.instance.ComponentInstance) parameters[6]);
  }

  /**
   * Inner class allowing the singleton instance of {@link Find_ConnectionAttach2Component} to be created 
   *     <b>not</b> at the class load time of the outer class, 
   *     but rather at the first call to {@link Find_ConnectionAttach2Component#instance()}.
   * 
   * <p> This workaround is required e.g. to support recursion.
   * 
   */
  private static class LazyHolder {
    private static final Find_ConnectionAttach2Component INSTANCE = new Find_ConnectionAttach2Component();

    /**
     * Statically initializes the query specification <b>after</b> the field {@link #INSTANCE} is assigned.
     * This initialization order is required to support indirect recursion.
     * 
     * <p> The static initializer is defined using a helper field to work around limitations of the code generator.
     * 
     */
    private static final Object STATIC_INITIALIZER = ensureInitialized();

    public static Object ensureInitialized() {
      INSTANCE.ensureInitializedInternal();
      return null;
    }
  }

  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private static final Find_ConnectionAttach2Component.GeneratedPQuery INSTANCE = new GeneratedPQuery();

    private final PParameter parameter_compinst = new PParameter("compinst", "org.osate.aadl2.instance.ComponentInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ComponentInstance")), PParameterDirection.INOUT);

    private final PParameter parameter_trace = new PParameter("trace", "fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace")), PParameterDirection.INOUT);

    private final PParameter parameter_conninst2add = new PParameter("conninst2add", "org.osate.aadl2.instance.ConnectionInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ConnectionInstance")), PParameterDirection.INOUT);

    private final PParameter parameter_srcfeatinst = new PParameter("srcfeatinst", "org.osate.aadl2.instance.FeatureInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "FeatureInstance")), PParameterDirection.INOUT);

    private final PParameter parameter_desfeatinst = new PParameter("desfeatinst", "org.osate.aadl2.instance.FeatureInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "FeatureInstance")), PParameterDirection.INOUT);

    private final PParameter parameter_srcfeatcomp = new PParameter("srcfeatcomp", "org.osate.aadl2.instance.ComponentInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ComponentInstance")), PParameterDirection.INOUT);

    private final PParameter parameter_desfeatcomp = new PParameter("desfeatcomp", "org.osate.aadl2.instance.ComponentInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ComponentInstance")), PParameterDirection.INOUT);

    private final List<PParameter> parameters = Arrays.asList(parameter_compinst, parameter_trace, parameter_conninst2add, parameter_srcfeatinst, parameter_desfeatinst, parameter_srcfeatcomp, parameter_desfeatcomp);

    private GeneratedPQuery() {
      super(PVisibility.PUBLIC);
    }

    @Override
    public String getFullyQualifiedName() {
      return "fr.mem4csd.osatedim.viatra.queries.find_ConnectionAttach2Component";
    }

    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("compinst","trace","conninst2add","srcfeatinst","desfeatinst","srcfeatcomp","desfeatcomp");
    }

    @Override
    public List<PParameter> getParameters() {
      return parameters;
    }

    @Override
    public Set<PBody> doGetContainedBodies() {
      setEvaluationHints(new QueryEvaluationHint(null, QueryEvaluationHint.BackendRequirement.UNSPECIFIED));
      Set<PBody> bodies = new LinkedHashSet<>();
      {
          PBody body = new PBody(this);
          PVariable var_compinst = body.getOrCreateVariableByName("compinst");
          PVariable var_trace = body.getOrCreateVariableByName("trace");
          PVariable var_conninst2add = body.getOrCreateVariableByName("conninst2add");
          PVariable var_srcfeatinst = body.getOrCreateVariableByName("srcfeatinst");
          PVariable var_desfeatinst = body.getOrCreateVariableByName("desfeatinst");
          PVariable var_srcfeatcomp = body.getOrCreateVariableByName("srcfeatcomp");
          PVariable var_desfeatcomp = body.getOrCreateVariableByName("desfeatcomp");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          PVariable var___1_ = body.getOrCreateVariableByName("_<1>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_trace), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_conninst2add), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_srcfeatinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "FeatureInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_desfeatinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "FeatureInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_srcfeatcomp), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_desfeatcomp), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_compinst, parameter_compinst),
             new ExportedParameter(body, var_trace, parameter_trace),
             new ExportedParameter(body, var_conninst2add, parameter_conninst2add),
             new ExportedParameter(body, var_srcfeatinst, parameter_srcfeatinst),
             new ExportedParameter(body, var_desfeatinst, parameter_desfeatinst),
             new ExportedParameter(body, var_srcfeatcomp, parameter_srcfeatcomp),
             new ExportedParameter(body, var_desfeatcomp, parameter_desfeatcomp)
          ));
          // 	find is_in_trace(_,trace,compinst,_)
          new PositivePatternCall(body, Tuples.flatTupleOf(var___0_, var_trace, var_compinst, var___1_), Is_in_trace.instance().getInternalQueryRepresentation());
          // 	Aaxl2AaxlTrace.objectsToAttach(trace,conninst2add)
          new TypeConstraint(body, Tuples.flatTupleOf(var_trace), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_trace, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace", "objectsToAttach")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Element")));
          new Equality(body, var__virtual_0_, var_conninst2add);
          // 	ConnectionInstance.source(conninst2add,srcfeatinst)
          new TypeConstraint(body, Tuples.flatTupleOf(var_conninst2add), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstance")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_conninst2add, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstance", "source")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstanceEnd")));
          new Equality(body, var__virtual_1_, var_srcfeatinst);
          // 	ConnectionInstance.destination(conninst2add,desfeatinst)
          new TypeConstraint(body, Tuples.flatTupleOf(var_conninst2add), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstance")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_conninst2add, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstance", "destination")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstanceEnd")));
          new Equality(body, var__virtual_2_, var_desfeatinst);
          // 	ComponentInstance.featureInstance(srcfeatcomp,srcfeatinst)
          new TypeConstraint(body, Tuples.flatTupleOf(var_srcfeatcomp), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          PVariable var__virtual_3_ = body.getOrCreateVariableByName(".virtual{3}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_srcfeatcomp, var__virtual_3_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance", "featureInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_3_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "FeatureInstance")));
          new Equality(body, var__virtual_3_, var_srcfeatinst);
          // 	ComponentInstance.featureInstance(desfeatcomp,desfeatinst)
          new TypeConstraint(body, Tuples.flatTupleOf(var_desfeatcomp), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          PVariable var__virtual_4_ = body.getOrCreateVariableByName(".virtual{4}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_desfeatcomp, var__virtual_4_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance", "featureInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_4_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "FeatureInstance")));
          new Equality(body, var__virtual_4_, var_desfeatinst);
          bodies.add(body);
      }
      return bodies;
    }
  }
}
