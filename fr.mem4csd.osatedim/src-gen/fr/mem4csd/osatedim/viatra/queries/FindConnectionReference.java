/**
 * Generated from platform:/resource/fr.mem4csd.osatedim/src/fr/mem4csd/osatedim/viatra/queries/DIMQueriesInplace.vql
 */
package fr.mem4csd.osatedim.viatra.queries;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.emf.ecore.EDataType;
import org.eclipse.viatra.query.runtime.api.IPatternMatch;
import org.eclipse.viatra.query.runtime.api.IQuerySpecification;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.viatra.query.runtime.api.impl.BaseMatcher;
import org.eclipse.viatra.query.runtime.api.impl.BasePatternMatch;
import org.eclipse.viatra.query.runtime.emf.types.EClassTransitiveInstancesKey;
import org.eclipse.viatra.query.runtime.emf.types.EDataTypeInSlotsKey;
import org.eclipse.viatra.query.runtime.emf.types.EStructuralFeatureInstancesKey;
import org.eclipse.viatra.query.runtime.matchers.backend.QueryEvaluationHint;
import org.eclipse.viatra.query.runtime.matchers.psystem.PBody;
import org.eclipse.viatra.query.runtime.matchers.psystem.PVariable;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.Equality;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.PositivePatternCall;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.TypeConstraint;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameterDirection;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PVisibility;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuple;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuples;
import org.eclipse.viatra.query.runtime.util.ViatraQueryLoggingUtil;
import org.osate.aadl2.instance.ConnectionReference;

/**
 * A pattern-specific query specification that can instantiate Matcher in a type-safe way.
 * 
 * <p>Original source:
 *         <code><pre>
 *         // Find connection references of a connection with source and destination components that have declared classifiers
 *         pattern findConnectionReference(connref : ConnectionReference)
 *         {
 *         	ConnectionInstance.connectionReference(conninst, connref);
 *         	find findConnection(conninst);
 *         	ConnectionInstance.kind(conninst,_);
 *         	ConnectionReference.source(connref,_);
 *         	ConnectionReference.destination(connref,_);
 *         }
 * </pre></code>
 * 
 * @see Matcher
 * @see Match
 * 
 */
@SuppressWarnings("all")
public final class FindConnectionReference extends BaseGeneratedEMFQuerySpecification<FindConnectionReference.Matcher> {
  /**
   * Pattern-specific match representation of the fr.mem4csd.osatedim.viatra.queries.findConnectionReference pattern,
   * to be used in conjunction with {@link Matcher}.
   * 
   * <p>Class fields correspond to parameters of the pattern. Fields with value null are considered unassigned.
   * Each instance is a (possibly partial) substitution of pattern parameters,
   * usable to represent a match of the pattern in the result of a query,
   * or to specify the bound (fixed) input parameters when issuing a query.
   * 
   * @see Matcher
   * 
   */
  public static abstract class Match extends BasePatternMatch {
    private ConnectionReference fConnref;

    private static List<String> parameterNames = makeImmutableList("connref");

    private Match(final ConnectionReference pConnref) {
      this.fConnref = pConnref;
    }

    @Override
    public Object get(final String parameterName) {
      switch(parameterName) {
          case "connref": return this.fConnref;
          default: return null;
      }
    }

    @Override
    public Object get(final int index) {
      switch(index) {
          case 0: return this.fConnref;
          default: return null;
      }
    }

    public ConnectionReference getConnref() {
      return this.fConnref;
    }

    @Override
    public boolean set(final String parameterName, final Object newValue) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      if ("connref".equals(parameterName) ) {
          this.fConnref = (ConnectionReference) newValue;
          return true;
      }
      return false;
    }

    public void setConnref(final ConnectionReference pConnref) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fConnref = pConnref;
    }

    @Override
    public String patternName() {
      return "fr.mem4csd.osatedim.viatra.queries.findConnectionReference";
    }

    @Override
    public List<String> parameterNames() {
      return FindConnectionReference.Match.parameterNames;
    }

    @Override
    public Object[] toArray() {
      return new Object[]{fConnref};
    }

    @Override
    public FindConnectionReference.Match toImmutable() {
      return isMutable() ? newMatch(fConnref) : this;
    }

    @Override
    public String prettyPrint() {
      StringBuilder result = new StringBuilder();
      result.append("\"connref\"=" + prettyPrintValue(fConnref));
      return result.toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(fConnref);
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj)
          return true;
      if (obj == null) {
          return false;
      }
      if ((obj instanceof FindConnectionReference.Match)) {
          FindConnectionReference.Match other = (FindConnectionReference.Match) obj;
          return Objects.equals(fConnref, other.fConnref);
      } else {
          // this should be infrequent
          if (!(obj instanceof IPatternMatch)) {
              return false;
          }
          IPatternMatch otherSig  = (IPatternMatch) obj;
          return Objects.equals(specification(), otherSig.specification()) && Arrays.deepEquals(toArray(), otherSig.toArray());
      }
    }

    @Override
    public FindConnectionReference specification() {
      return FindConnectionReference.instance();
    }

    /**
     * Returns an empty, mutable match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @return the empty match.
     * 
     */
    public static FindConnectionReference.Match newEmptyMatch() {
      return new Mutable(null);
    }

    /**
     * Returns a mutable (partial) match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @param pConnref the fixed value of pattern parameter connref, or null if not bound.
     * @return the new, mutable (partial) match object.
     * 
     */
    public static FindConnectionReference.Match newMutableMatch(final ConnectionReference pConnref) {
      return new Mutable(pConnref);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pConnref the fixed value of pattern parameter connref, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public static FindConnectionReference.Match newMatch(final ConnectionReference pConnref) {
      return new Immutable(pConnref);
    }

    private static final class Mutable extends FindConnectionReference.Match {
      Mutable(final ConnectionReference pConnref) {
        super(pConnref);
      }

      @Override
      public boolean isMutable() {
        return true;
      }
    }

    private static final class Immutable extends FindConnectionReference.Match {
      Immutable(final ConnectionReference pConnref) {
        super(pConnref);
      }

      @Override
      public boolean isMutable() {
        return false;
      }
    }
  }

  /**
   * Generated pattern matcher API of the fr.mem4csd.osatedim.viatra.queries.findConnectionReference pattern,
   * providing pattern-specific query methods.
   * 
   * <p>Use the pattern matcher on a given model via {@link #on(ViatraQueryEngine)},
   * e.g. in conjunction with {@link ViatraQueryEngine#on(QueryScope)}.
   * 
   * <p>Matches of the pattern will be represented as {@link Match}.
   * 
   * <p>Original source:
   * <code><pre>
   * // Find connection references of a connection with source and destination components that have declared classifiers
   * pattern findConnectionReference(connref : ConnectionReference)
   * {
   * 	ConnectionInstance.connectionReference(conninst, connref);
   * 	find findConnection(conninst);
   * 	ConnectionInstance.kind(conninst,_);
   * 	ConnectionReference.source(connref,_);
   * 	ConnectionReference.destination(connref,_);
   * }
   * </pre></code>
   * 
   * @see Match
   * @see FindConnectionReference
   * 
   */
  public static class Matcher extends BaseMatcher<FindConnectionReference.Match> {
    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    public static FindConnectionReference.Matcher on(final ViatraQueryEngine engine) {
      // check if matcher already exists
      Matcher matcher = engine.getExistingMatcher(querySpecification());
      if (matcher == null) {
          matcher = (Matcher)engine.getMatcher(querySpecification());
      }
      return matcher;
    }

    /**
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * @return an initialized matcher
     * @noreference This method is for internal matcher initialization by the framework, do not call it manually.
     * 
     */
    public static FindConnectionReference.Matcher create() {
      return new Matcher();
    }

    private static final int POSITION_CONNREF = 0;

    private static final Logger LOGGER = ViatraQueryLoggingUtil.getLogger(FindConnectionReference.Matcher.class);

    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    private Matcher() {
      super(querySpecification());
    }

    /**
     * Returns the set of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pConnref the fixed value of pattern parameter connref, or null if not bound.
     * @return matches represented as a Match object.
     * 
     */
    public Collection<FindConnectionReference.Match> getAllMatches(final ConnectionReference pConnref) {
      return rawStreamAllMatches(new Object[]{pConnref}).collect(Collectors.toSet());
    }

    /**
     * Returns a stream of all matches of the pattern that conform to the given fixed values of some parameters.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     * @param pConnref the fixed value of pattern parameter connref, or null if not bound.
     * @return a stream of matches represented as a Match object.
     * 
     */
    public Stream<FindConnectionReference.Match> streamAllMatches(final ConnectionReference pConnref) {
      return rawStreamAllMatches(new Object[]{pConnref});
    }

    /**
     * Returns an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pConnref the fixed value of pattern parameter connref, or null if not bound.
     * @return a match represented as a Match object, or null if no match is found.
     * 
     */
    public Optional<FindConnectionReference.Match> getOneArbitraryMatch(final ConnectionReference pConnref) {
      return rawGetOneArbitraryMatch(new Object[]{pConnref});
    }

    /**
     * Indicates whether the given combination of specified pattern parameters constitute a valid pattern match,
     * under any possible substitution of the unspecified parameters (if any).
     * @param pConnref the fixed value of pattern parameter connref, or null if not bound.
     * @return true if the input is a valid (partial) match of the pattern.
     * 
     */
    public boolean hasMatch(final ConnectionReference pConnref) {
      return rawHasMatch(new Object[]{pConnref});
    }

    /**
     * Returns the number of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pConnref the fixed value of pattern parameter connref, or null if not bound.
     * @return the number of pattern matches found.
     * 
     */
    public int countMatches(final ConnectionReference pConnref) {
      return rawCountMatches(new Object[]{pConnref});
    }

    /**
     * Executes the given processor on an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pConnref the fixed value of pattern parameter connref, or null if not bound.
     * @param processor the action that will process the selected match.
     * @return true if the pattern has at least one match with the given parameter values, false if the processor was not invoked
     * 
     */
    public boolean forOneArbitraryMatch(final ConnectionReference pConnref, final Consumer<? super FindConnectionReference.Match> processor) {
      return rawForOneArbitraryMatch(new Object[]{pConnref}, processor);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pConnref the fixed value of pattern parameter connref, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public FindConnectionReference.Match newMatch(final ConnectionReference pConnref) {
      return FindConnectionReference.Match.newMatch(pConnref);
    }

    /**
     * Retrieve the set of values that occur in matches for connref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ConnectionReference> rawStreamAllValuesOfconnref(final Object[] parameters) {
      return rawStreamAllValues(POSITION_CONNREF, parameters).map(ConnectionReference.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for connref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionReference> getAllValuesOfconnref() {
      return rawStreamAllValuesOfconnref(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for connref.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionReference> streamAllValuesOfconnref() {
      return rawStreamAllValuesOfconnref(emptyArray());
    }

    @Override
    protected FindConnectionReference.Match tupleToMatch(final Tuple t) {
      try {
          return FindConnectionReference.Match.newMatch((ConnectionReference) t.get(POSITION_CONNREF));
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in tuple not properly typed!",e);
          return null;
      }
    }

    @Override
    protected FindConnectionReference.Match arrayToMatch(final Object[] match) {
      try {
          return FindConnectionReference.Match.newMatch((ConnectionReference) match[POSITION_CONNREF]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    @Override
    protected FindConnectionReference.Match arrayToMatchMutable(final Object[] match) {
      try {
          return FindConnectionReference.Match.newMutableMatch((ConnectionReference) match[POSITION_CONNREF]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    /**
     * @return the singleton instance of the query specification of this pattern
     * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
     * 
     */
    public static IQuerySpecification<FindConnectionReference.Matcher> querySpecification() {
      return FindConnectionReference.instance();
    }
  }

  private FindConnectionReference() {
    super(GeneratedPQuery.INSTANCE);
  }

  /**
   * @return the singleton instance of the query specification
   * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
   * 
   */
  public static FindConnectionReference instance() {
    try{
        return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
        throw processInitializerError(err);
    }
  }

  @Override
  protected FindConnectionReference.Matcher instantiate(final ViatraQueryEngine engine) {
    return FindConnectionReference.Matcher.on(engine);
  }

  @Override
  public FindConnectionReference.Matcher instantiate() {
    return FindConnectionReference.Matcher.create();
  }

  @Override
  public FindConnectionReference.Match newEmptyMatch() {
    return FindConnectionReference.Match.newEmptyMatch();
  }

  @Override
  public FindConnectionReference.Match newMatch(final Object... parameters) {
    return FindConnectionReference.Match.newMatch((org.osate.aadl2.instance.ConnectionReference) parameters[0]);
  }

  /**
   * Inner class allowing the singleton instance of {@link FindConnectionReference} to be created 
   *     <b>not</b> at the class load time of the outer class, 
   *     but rather at the first call to {@link FindConnectionReference#instance()}.
   * 
   * <p> This workaround is required e.g. to support recursion.
   * 
   */
  private static class LazyHolder {
    private static final FindConnectionReference INSTANCE = new FindConnectionReference();

    /**
     * Statically initializes the query specification <b>after</b> the field {@link #INSTANCE} is assigned.
     * This initialization order is required to support indirect recursion.
     * 
     * <p> The static initializer is defined using a helper field to work around limitations of the code generator.
     * 
     */
    private static final Object STATIC_INITIALIZER = ensureInitialized();

    public static Object ensureInitialized() {
      INSTANCE.ensureInitializedInternal();
      return null;
    }
  }

  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private static final FindConnectionReference.GeneratedPQuery INSTANCE = new GeneratedPQuery();

    private final PParameter parameter_connref = new PParameter("connref", "org.osate.aadl2.instance.ConnectionReference", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ConnectionReference")), PParameterDirection.INOUT);

    private final List<PParameter> parameters = Arrays.asList(parameter_connref);

    private GeneratedPQuery() {
      super(PVisibility.PUBLIC);
    }

    @Override
    public String getFullyQualifiedName() {
      return "fr.mem4csd.osatedim.viatra.queries.findConnectionReference";
    }

    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("connref");
    }

    @Override
    public List<PParameter> getParameters() {
      return parameters;
    }

    @Override
    public Set<PBody> doGetContainedBodies() {
      setEvaluationHints(new QueryEvaluationHint(null, QueryEvaluationHint.BackendRequirement.UNSPECIFIED));
      Set<PBody> bodies = new LinkedHashSet<>();
      {
          PBody body = new PBody(this);
          PVariable var_connref = body.getOrCreateVariableByName("connref");
          PVariable var_conninst = body.getOrCreateVariableByName("conninst");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          PVariable var___1_ = body.getOrCreateVariableByName("_<1>");
          PVariable var___2_ = body.getOrCreateVariableByName("_<2>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_connref), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionReference")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_connref, parameter_connref)
          ));
          // 	ConnectionInstance.connectionReference(conninst, connref)
          new TypeConstraint(body, Tuples.flatTupleOf(var_conninst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstance")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_conninst, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstance", "connectionReference")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionReference")));
          new Equality(body, var__virtual_0_, var_connref);
          // 	find findConnection(conninst)
          new PositivePatternCall(body, Tuples.flatTupleOf(var_conninst), FindConnection.instance().getInternalQueryRepresentation());
          // 	ConnectionInstance.kind(conninst,_)
          new TypeConstraint(body, Tuples.flatTupleOf(var_conninst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstance")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_conninst, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstance", "kind")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EDataTypeInSlotsKey((EDataType)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionKind")));
          new Equality(body, var__virtual_1_, var___0_);
          // 	ConnectionReference.source(connref,_)
          new TypeConstraint(body, Tuples.flatTupleOf(var_connref), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionReference")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_connref, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionReference", "source")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstanceEnd")));
          new Equality(body, var__virtual_2_, var___1_);
          // 	ConnectionReference.destination(connref,_)
          new TypeConstraint(body, Tuples.flatTupleOf(var_connref), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionReference")));
          PVariable var__virtual_3_ = body.getOrCreateVariableByName(".virtual{3}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_connref, var__virtual_3_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionReference", "destination")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_3_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstanceEnd")));
          new Equality(body, var__virtual_3_, var___2_);
          bodies.add(body);
      }
      return bodies;
    }
  }
}
