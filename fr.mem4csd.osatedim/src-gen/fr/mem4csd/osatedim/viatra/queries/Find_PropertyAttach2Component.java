/**
 * Generated from platform:/resource/fr.mem4csd.osatedim/src/fr/mem4csd/osatedim/viatra/queries/DIMQueriesOutplace.vql
 */
package fr.mem4csd.osatedim.viatra.queries;

import fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.viatra.query.runtime.api.IPatternMatch;
import org.eclipse.viatra.query.runtime.api.IQuerySpecification;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.viatra.query.runtime.api.impl.BaseMatcher;
import org.eclipse.viatra.query.runtime.api.impl.BasePatternMatch;
import org.eclipse.viatra.query.runtime.emf.types.EClassTransitiveInstancesKey;
import org.eclipse.viatra.query.runtime.emf.types.EStructuralFeatureInstancesKey;
import org.eclipse.viatra.query.runtime.matchers.backend.QueryEvaluationHint;
import org.eclipse.viatra.query.runtime.matchers.psystem.PBody;
import org.eclipse.viatra.query.runtime.matchers.psystem.PVariable;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.Equality;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.TypeConstraint;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameterDirection;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PVisibility;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuple;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuples;
import org.eclipse.viatra.query.runtime.util.ViatraQueryLoggingUtil;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.PropertyAssociationInstance;

/**
 * A pattern-specific query specification that can instantiate Matcher in a type-safe way.
 * 
 * <p>Original source:
 *         <code><pre>
 *         //This pattern finds properties2attach for a specific component instance
 *         pattern find_PropertyAttach2Component(propinst : PropertyAssociationInstance,
 *         							  		  compinst : ComponentInstance, 
 *         							  		  trace : Aaxl2AaxlTrace) 
 *         {
 *         	Aaxl2AaxlTrace.leftInstance(trace,compinst);
 *         	Aaxl2AaxlTrace.objectsToAttach(trace, propinst);
 *         }
 * </pre></code>
 * 
 * @see Matcher
 * @see Match
 * 
 */
@SuppressWarnings("all")
public final class Find_PropertyAttach2Component extends BaseGeneratedEMFQuerySpecification<Find_PropertyAttach2Component.Matcher> {
  /**
   * Pattern-specific match representation of the fr.mem4csd.osatedim.viatra.queries.find_PropertyAttach2Component pattern,
   * to be used in conjunction with {@link Matcher}.
   * 
   * <p>Class fields correspond to parameters of the pattern. Fields with value null are considered unassigned.
   * Each instance is a (possibly partial) substitution of pattern parameters,
   * usable to represent a match of the pattern in the result of a query,
   * or to specify the bound (fixed) input parameters when issuing a query.
   * 
   * @see Matcher
   * 
   */
  public static abstract class Match extends BasePatternMatch {
    private PropertyAssociationInstance fPropinst;

    private ComponentInstance fCompinst;

    private Aaxl2AaxlTrace fTrace;

    private static List<String> parameterNames = makeImmutableList("propinst", "compinst", "trace");

    private Match(final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      this.fPropinst = pPropinst;
      this.fCompinst = pCompinst;
      this.fTrace = pTrace;
    }

    @Override
    public Object get(final String parameterName) {
      switch(parameterName) {
          case "propinst": return this.fPropinst;
          case "compinst": return this.fCompinst;
          case "trace": return this.fTrace;
          default: return null;
      }
    }

    @Override
    public Object get(final int index) {
      switch(index) {
          case 0: return this.fPropinst;
          case 1: return this.fCompinst;
          case 2: return this.fTrace;
          default: return null;
      }
    }

    public PropertyAssociationInstance getPropinst() {
      return this.fPropinst;
    }

    public ComponentInstance getCompinst() {
      return this.fCompinst;
    }

    public Aaxl2AaxlTrace getTrace() {
      return this.fTrace;
    }

    @Override
    public boolean set(final String parameterName, final Object newValue) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      if ("propinst".equals(parameterName) ) {
          this.fPropinst = (PropertyAssociationInstance) newValue;
          return true;
      }
      if ("compinst".equals(parameterName) ) {
          this.fCompinst = (ComponentInstance) newValue;
          return true;
      }
      if ("trace".equals(parameterName) ) {
          this.fTrace = (Aaxl2AaxlTrace) newValue;
          return true;
      }
      return false;
    }

    public void setPropinst(final PropertyAssociationInstance pPropinst) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fPropinst = pPropinst;
    }

    public void setCompinst(final ComponentInstance pCompinst) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fCompinst = pCompinst;
    }

    public void setTrace(final Aaxl2AaxlTrace pTrace) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fTrace = pTrace;
    }

    @Override
    public String patternName() {
      return "fr.mem4csd.osatedim.viatra.queries.find_PropertyAttach2Component";
    }

    @Override
    public List<String> parameterNames() {
      return Find_PropertyAttach2Component.Match.parameterNames;
    }

    @Override
    public Object[] toArray() {
      return new Object[]{fPropinst, fCompinst, fTrace};
    }

    @Override
    public Find_PropertyAttach2Component.Match toImmutable() {
      return isMutable() ? newMatch(fPropinst, fCompinst, fTrace) : this;
    }

    @Override
    public String prettyPrint() {
      StringBuilder result = new StringBuilder();
      result.append("\"propinst\"=" + prettyPrintValue(fPropinst) + ", ");
      result.append("\"compinst\"=" + prettyPrintValue(fCompinst) + ", ");
      result.append("\"trace\"=" + prettyPrintValue(fTrace));
      return result.toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(fPropinst, fCompinst, fTrace);
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj)
          return true;
      if (obj == null) {
          return false;
      }
      if ((obj instanceof Find_PropertyAttach2Component.Match)) {
          Find_PropertyAttach2Component.Match other = (Find_PropertyAttach2Component.Match) obj;
          return Objects.equals(fPropinst, other.fPropinst) && Objects.equals(fCompinst, other.fCompinst) && Objects.equals(fTrace, other.fTrace);
      } else {
          // this should be infrequent
          if (!(obj instanceof IPatternMatch)) {
              return false;
          }
          IPatternMatch otherSig  = (IPatternMatch) obj;
          return Objects.equals(specification(), otherSig.specification()) && Arrays.deepEquals(toArray(), otherSig.toArray());
      }
    }

    @Override
    public Find_PropertyAttach2Component specification() {
      return Find_PropertyAttach2Component.instance();
    }

    /**
     * Returns an empty, mutable match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @return the empty match.
     * 
     */
    public static Find_PropertyAttach2Component.Match newEmptyMatch() {
      return new Mutable(null, null, null);
    }

    /**
     * Returns a mutable (partial) match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @return the new, mutable (partial) match object.
     * 
     */
    public static Find_PropertyAttach2Component.Match newMutableMatch(final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      return new Mutable(pPropinst, pCompinst, pTrace);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public static Find_PropertyAttach2Component.Match newMatch(final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      return new Immutable(pPropinst, pCompinst, pTrace);
    }

    private static final class Mutable extends Find_PropertyAttach2Component.Match {
      Mutable(final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
        super(pPropinst, pCompinst, pTrace);
      }

      @Override
      public boolean isMutable() {
        return true;
      }
    }

    private static final class Immutable extends Find_PropertyAttach2Component.Match {
      Immutable(final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
        super(pPropinst, pCompinst, pTrace);
      }

      @Override
      public boolean isMutable() {
        return false;
      }
    }
  }

  /**
   * Generated pattern matcher API of the fr.mem4csd.osatedim.viatra.queries.find_PropertyAttach2Component pattern,
   * providing pattern-specific query methods.
   * 
   * <p>Use the pattern matcher on a given model via {@link #on(ViatraQueryEngine)},
   * e.g. in conjunction with {@link ViatraQueryEngine#on(QueryScope)}.
   * 
   * <p>Matches of the pattern will be represented as {@link Match}.
   * 
   * <p>Original source:
   * <code><pre>
   * //This pattern finds properties2attach for a specific component instance
   * pattern find_PropertyAttach2Component(propinst : PropertyAssociationInstance,
   * 							  		  compinst : ComponentInstance, 
   * 							  		  trace : Aaxl2AaxlTrace) 
   * {
   * 	Aaxl2AaxlTrace.leftInstance(trace,compinst);
   * 	Aaxl2AaxlTrace.objectsToAttach(trace, propinst);
   * }
   * </pre></code>
   * 
   * @see Match
   * @see Find_PropertyAttach2Component
   * 
   */
  public static class Matcher extends BaseMatcher<Find_PropertyAttach2Component.Match> {
    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    public static Find_PropertyAttach2Component.Matcher on(final ViatraQueryEngine engine) {
      // check if matcher already exists
      Matcher matcher = engine.getExistingMatcher(querySpecification());
      if (matcher == null) {
          matcher = (Matcher)engine.getMatcher(querySpecification());
      }
      return matcher;
    }

    /**
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * @return an initialized matcher
     * @noreference This method is for internal matcher initialization by the framework, do not call it manually.
     * 
     */
    public static Find_PropertyAttach2Component.Matcher create() {
      return new Matcher();
    }

    private static final int POSITION_PROPINST = 0;

    private static final int POSITION_COMPINST = 1;

    private static final int POSITION_TRACE = 2;

    private static final Logger LOGGER = ViatraQueryLoggingUtil.getLogger(Find_PropertyAttach2Component.Matcher.class);

    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    private Matcher() {
      super(querySpecification());
    }

    /**
     * Returns the set of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @return matches represented as a Match object.
     * 
     */
    public Collection<Find_PropertyAttach2Component.Match> getAllMatches(final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      return rawStreamAllMatches(new Object[]{pPropinst, pCompinst, pTrace}).collect(Collectors.toSet());
    }

    /**
     * Returns a stream of all matches of the pattern that conform to the given fixed values of some parameters.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @return a stream of matches represented as a Match object.
     * 
     */
    public Stream<Find_PropertyAttach2Component.Match> streamAllMatches(final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      return rawStreamAllMatches(new Object[]{pPropinst, pCompinst, pTrace});
    }

    /**
     * Returns an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @return a match represented as a Match object, or null if no match is found.
     * 
     */
    public Optional<Find_PropertyAttach2Component.Match> getOneArbitraryMatch(final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      return rawGetOneArbitraryMatch(new Object[]{pPropinst, pCompinst, pTrace});
    }

    /**
     * Indicates whether the given combination of specified pattern parameters constitute a valid pattern match,
     * under any possible substitution of the unspecified parameters (if any).
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @return true if the input is a valid (partial) match of the pattern.
     * 
     */
    public boolean hasMatch(final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      return rawHasMatch(new Object[]{pPropinst, pCompinst, pTrace});
    }

    /**
     * Returns the number of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @return the number of pattern matches found.
     * 
     */
    public int countMatches(final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      return rawCountMatches(new Object[]{pPropinst, pCompinst, pTrace});
    }

    /**
     * Executes the given processor on an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param processor the action that will process the selected match.
     * @return true if the pattern has at least one match with the given parameter values, false if the processor was not invoked
     * 
     */
    public boolean forOneArbitraryMatch(final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final Consumer<? super Find_PropertyAttach2Component.Match> processor) {
      return rawForOneArbitraryMatch(new Object[]{pPropinst, pCompinst, pTrace}, processor);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public Find_PropertyAttach2Component.Match newMatch(final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      return Find_PropertyAttach2Component.Match.newMatch(pPropinst, pCompinst, pTrace);
    }

    /**
     * Retrieve the set of values that occur in matches for propinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<PropertyAssociationInstance> rawStreamAllValuesOfpropinst(final Object[] parameters) {
      return rawStreamAllValues(POSITION_PROPINST, parameters).map(PropertyAssociationInstance.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for propinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<PropertyAssociationInstance> getAllValuesOfpropinst() {
      return rawStreamAllValuesOfpropinst(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for propinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<PropertyAssociationInstance> streamAllValuesOfpropinst() {
      return rawStreamAllValuesOfpropinst(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for propinst.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<PropertyAssociationInstance> streamAllValuesOfpropinst(final Find_PropertyAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfpropinst(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for propinst.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<PropertyAssociationInstance> streamAllValuesOfpropinst(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      return rawStreamAllValuesOfpropinst(new Object[]{null, pCompinst, pTrace});
    }

    /**
     * Retrieve the set of values that occur in matches for propinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<PropertyAssociationInstance> getAllValuesOfpropinst(final Find_PropertyAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfpropinst(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for propinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<PropertyAssociationInstance> getAllValuesOfpropinst(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      return rawStreamAllValuesOfpropinst(new Object[]{null, pCompinst, pTrace}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ComponentInstance> rawStreamAllValuesOfcompinst(final Object[] parameters) {
      return rawStreamAllValues(POSITION_COMPINST, parameters).map(ComponentInstance.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst() {
      return rawStreamAllValuesOfcompinst(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst() {
      return rawStreamAllValuesOfcompinst(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst(final Find_PropertyAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfcompinst(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst(final PropertyAssociationInstance pPropinst, final Aaxl2AaxlTrace pTrace) {
      return rawStreamAllValuesOfcompinst(new Object[]{pPropinst, null, pTrace});
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst(final Find_PropertyAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfcompinst(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst(final PropertyAssociationInstance pPropinst, final Aaxl2AaxlTrace pTrace) {
      return rawStreamAllValuesOfcompinst(new Object[]{pPropinst, null, pTrace}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<Aaxl2AaxlTrace> rawStreamAllValuesOftrace(final Object[] parameters) {
      return rawStreamAllValues(POSITION_TRACE, parameters).map(Aaxl2AaxlTrace.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Aaxl2AaxlTrace> getAllValuesOftrace() {
      return rawStreamAllValuesOftrace(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<Aaxl2AaxlTrace> streamAllValuesOftrace() {
      return rawStreamAllValuesOftrace(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<Aaxl2AaxlTrace> streamAllValuesOftrace(final Find_PropertyAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOftrace(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<Aaxl2AaxlTrace> streamAllValuesOftrace(final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst) {
      return rawStreamAllValuesOftrace(new Object[]{pPropinst, pCompinst, null});
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Aaxl2AaxlTrace> getAllValuesOftrace(final Find_PropertyAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOftrace(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Aaxl2AaxlTrace> getAllValuesOftrace(final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst) {
      return rawStreamAllValuesOftrace(new Object[]{pPropinst, pCompinst, null}).collect(Collectors.toSet());
    }

    @Override
    protected Find_PropertyAttach2Component.Match tupleToMatch(final Tuple t) {
      try {
          return Find_PropertyAttach2Component.Match.newMatch((PropertyAssociationInstance) t.get(POSITION_PROPINST), (ComponentInstance) t.get(POSITION_COMPINST), (Aaxl2AaxlTrace) t.get(POSITION_TRACE));
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in tuple not properly typed!",e);
          return null;
      }
    }

    @Override
    protected Find_PropertyAttach2Component.Match arrayToMatch(final Object[] match) {
      try {
          return Find_PropertyAttach2Component.Match.newMatch((PropertyAssociationInstance) match[POSITION_PROPINST], (ComponentInstance) match[POSITION_COMPINST], (Aaxl2AaxlTrace) match[POSITION_TRACE]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    @Override
    protected Find_PropertyAttach2Component.Match arrayToMatchMutable(final Object[] match) {
      try {
          return Find_PropertyAttach2Component.Match.newMutableMatch((PropertyAssociationInstance) match[POSITION_PROPINST], (ComponentInstance) match[POSITION_COMPINST], (Aaxl2AaxlTrace) match[POSITION_TRACE]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    /**
     * @return the singleton instance of the query specification of this pattern
     * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
     * 
     */
    public static IQuerySpecification<Find_PropertyAttach2Component.Matcher> querySpecification() {
      return Find_PropertyAttach2Component.instance();
    }
  }

  private Find_PropertyAttach2Component() {
    super(GeneratedPQuery.INSTANCE);
  }

  /**
   * @return the singleton instance of the query specification
   * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
   * 
   */
  public static Find_PropertyAttach2Component instance() {
    try{
        return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
        throw processInitializerError(err);
    }
  }

  @Override
  protected Find_PropertyAttach2Component.Matcher instantiate(final ViatraQueryEngine engine) {
    return Find_PropertyAttach2Component.Matcher.on(engine);
  }

  @Override
  public Find_PropertyAttach2Component.Matcher instantiate() {
    return Find_PropertyAttach2Component.Matcher.create();
  }

  @Override
  public Find_PropertyAttach2Component.Match newEmptyMatch() {
    return Find_PropertyAttach2Component.Match.newEmptyMatch();
  }

  @Override
  public Find_PropertyAttach2Component.Match newMatch(final Object... parameters) {
    return Find_PropertyAttach2Component.Match.newMatch((org.osate.aadl2.instance.PropertyAssociationInstance) parameters[0], (org.osate.aadl2.instance.ComponentInstance) parameters[1], (fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace) parameters[2]);
  }

  /**
   * Inner class allowing the singleton instance of {@link Find_PropertyAttach2Component} to be created 
   *     <b>not</b> at the class load time of the outer class, 
   *     but rather at the first call to {@link Find_PropertyAttach2Component#instance()}.
   * 
   * <p> This workaround is required e.g. to support recursion.
   * 
   */
  private static class LazyHolder {
    private static final Find_PropertyAttach2Component INSTANCE = new Find_PropertyAttach2Component();

    /**
     * Statically initializes the query specification <b>after</b> the field {@link #INSTANCE} is assigned.
     * This initialization order is required to support indirect recursion.
     * 
     * <p> The static initializer is defined using a helper field to work around limitations of the code generator.
     * 
     */
    private static final Object STATIC_INITIALIZER = ensureInitialized();

    public static Object ensureInitialized() {
      INSTANCE.ensureInitializedInternal();
      return null;
    }
  }

  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private static final Find_PropertyAttach2Component.GeneratedPQuery INSTANCE = new GeneratedPQuery();

    private final PParameter parameter_propinst = new PParameter("propinst", "org.osate.aadl2.instance.PropertyAssociationInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "PropertyAssociationInstance")), PParameterDirection.INOUT);

    private final PParameter parameter_compinst = new PParameter("compinst", "org.osate.aadl2.instance.ComponentInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ComponentInstance")), PParameterDirection.INOUT);

    private final PParameter parameter_trace = new PParameter("trace", "fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace")), PParameterDirection.INOUT);

    private final List<PParameter> parameters = Arrays.asList(parameter_propinst, parameter_compinst, parameter_trace);

    private GeneratedPQuery() {
      super(PVisibility.PUBLIC);
    }

    @Override
    public String getFullyQualifiedName() {
      return "fr.mem4csd.osatedim.viatra.queries.find_PropertyAttach2Component";
    }

    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("propinst","compinst","trace");
    }

    @Override
    public List<PParameter> getParameters() {
      return parameters;
    }

    @Override
    public Set<PBody> doGetContainedBodies() {
      setEvaluationHints(new QueryEvaluationHint(null, QueryEvaluationHint.BackendRequirement.UNSPECIFIED));
      Set<PBody> bodies = new LinkedHashSet<>();
      {
          PBody body = new PBody(this);
          PVariable var_propinst = body.getOrCreateVariableByName("propinst");
          PVariable var_compinst = body.getOrCreateVariableByName("compinst");
          PVariable var_trace = body.getOrCreateVariableByName("trace");
          new TypeConstraint(body, Tuples.flatTupleOf(var_propinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "PropertyAssociationInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_trace), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_propinst, parameter_propinst),
             new ExportedParameter(body, var_compinst, parameter_compinst),
             new ExportedParameter(body, var_trace, parameter_trace)
          ));
          // 	Aaxl2AaxlTrace.leftInstance(trace,compinst)
          new TypeConstraint(body, Tuples.flatTupleOf(var_trace), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_trace, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace", "leftInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Element")));
          new Equality(body, var__virtual_0_, var_compinst);
          // 	Aaxl2AaxlTrace.objectsToAttach(trace, propinst)
          new TypeConstraint(body, Tuples.flatTupleOf(var_trace), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_trace, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace", "objectsToAttach")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Element")));
          new Equality(body, var__virtual_1_, var_propinst);
          bodies.add(body);
      }
      return bodies;
    }
  }
}
