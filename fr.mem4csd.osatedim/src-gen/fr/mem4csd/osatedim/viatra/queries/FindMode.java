/**
 * Generated from platform:/resource/fr.mem4csd.osatedim/src/fr/mem4csd/osatedim/viatra/queries/DIMQueriesInplace.vql
 */
package fr.mem4csd.osatedim.viatra.queries;

import java.util.Arrays;
import java.util.Collection;
import java.util.Collections;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.viatra.query.runtime.api.IPatternMatch;
import org.eclipse.viatra.query.runtime.api.IQuerySpecification;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.viatra.query.runtime.api.impl.BaseMatcher;
import org.eclipse.viatra.query.runtime.api.impl.BasePatternMatch;
import org.eclipse.viatra.query.runtime.emf.types.EClassTransitiveInstancesKey;
import org.eclipse.viatra.query.runtime.emf.types.EStructuralFeatureInstancesKey;
import org.eclipse.viatra.query.runtime.matchers.backend.QueryEvaluationHint;
import org.eclipse.viatra.query.runtime.matchers.psystem.PBody;
import org.eclipse.viatra.query.runtime.matchers.psystem.PVariable;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.Equality;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.NegativePatternCall;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.TypeConstraint;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameterDirection;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PVisibility;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuple;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuples;
import org.eclipse.viatra.query.runtime.util.ViatraQueryLoggingUtil;
import org.osate.aadl2.instance.ModeInstance;

/**
 * A pattern-specific query specification that can instantiate Matcher in a type-safe way.
 * 
 * <p>Original source:
 *         <code><pre>
 *         // Find mode instances
 *         pattern findMode(modeinst: ModeInstance)
 *         {
 *         	ComponentInstance.modeInstance(_,modeinst);
 *         	neg ModeInstance.parent(modeinst,_);
 *         }
 * </pre></code>
 * 
 * @see Matcher
 * @see Match
 * 
 */
@SuppressWarnings("all")
public final class FindMode extends BaseGeneratedEMFQuerySpecification<FindMode.Matcher> {
  /**
   * Pattern-specific match representation of the fr.mem4csd.osatedim.viatra.queries.findMode pattern,
   * to be used in conjunction with {@link Matcher}.
   * 
   * <p>Class fields correspond to parameters of the pattern. Fields with value null are considered unassigned.
   * Each instance is a (possibly partial) substitution of pattern parameters,
   * usable to represent a match of the pattern in the result of a query,
   * or to specify the bound (fixed) input parameters when issuing a query.
   * 
   * @see Matcher
   * 
   */
  public static abstract class Match extends BasePatternMatch {
    private ModeInstance fModeinst;

    private static List<String> parameterNames = makeImmutableList("modeinst");

    private Match(final ModeInstance pModeinst) {
      this.fModeinst = pModeinst;
    }

    @Override
    public Object get(final String parameterName) {
      switch(parameterName) {
          case "modeinst": return this.fModeinst;
          default: return null;
      }
    }

    @Override
    public Object get(final int index) {
      switch(index) {
          case 0: return this.fModeinst;
          default: return null;
      }
    }

    public ModeInstance getModeinst() {
      return this.fModeinst;
    }

    @Override
    public boolean set(final String parameterName, final Object newValue) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      if ("modeinst".equals(parameterName) ) {
          this.fModeinst = (ModeInstance) newValue;
          return true;
      }
      return false;
    }

    public void setModeinst(final ModeInstance pModeinst) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fModeinst = pModeinst;
    }

    @Override
    public String patternName() {
      return "fr.mem4csd.osatedim.viatra.queries.findMode";
    }

    @Override
    public List<String> parameterNames() {
      return FindMode.Match.parameterNames;
    }

    @Override
    public Object[] toArray() {
      return new Object[]{fModeinst};
    }

    @Override
    public FindMode.Match toImmutable() {
      return isMutable() ? newMatch(fModeinst) : this;
    }

    @Override
    public String prettyPrint() {
      StringBuilder result = new StringBuilder();
      result.append("\"modeinst\"=" + prettyPrintValue(fModeinst));
      return result.toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(fModeinst);
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj)
          return true;
      if (obj == null) {
          return false;
      }
      if ((obj instanceof FindMode.Match)) {
          FindMode.Match other = (FindMode.Match) obj;
          return Objects.equals(fModeinst, other.fModeinst);
      } else {
          // this should be infrequent
          if (!(obj instanceof IPatternMatch)) {
              return false;
          }
          IPatternMatch otherSig  = (IPatternMatch) obj;
          return Objects.equals(specification(), otherSig.specification()) && Arrays.deepEquals(toArray(), otherSig.toArray());
      }
    }

    @Override
    public FindMode specification() {
      return FindMode.instance();
    }

    /**
     * Returns an empty, mutable match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @return the empty match.
     * 
     */
    public static FindMode.Match newEmptyMatch() {
      return new Mutable(null);
    }

    /**
     * Returns a mutable (partial) match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @param pModeinst the fixed value of pattern parameter modeinst, or null if not bound.
     * @return the new, mutable (partial) match object.
     * 
     */
    public static FindMode.Match newMutableMatch(final ModeInstance pModeinst) {
      return new Mutable(pModeinst);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pModeinst the fixed value of pattern parameter modeinst, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public static FindMode.Match newMatch(final ModeInstance pModeinst) {
      return new Immutable(pModeinst);
    }

    private static final class Mutable extends FindMode.Match {
      Mutable(final ModeInstance pModeinst) {
        super(pModeinst);
      }

      @Override
      public boolean isMutable() {
        return true;
      }
    }

    private static final class Immutable extends FindMode.Match {
      Immutable(final ModeInstance pModeinst) {
        super(pModeinst);
      }

      @Override
      public boolean isMutable() {
        return false;
      }
    }
  }

  /**
   * Generated pattern matcher API of the fr.mem4csd.osatedim.viatra.queries.findMode pattern,
   * providing pattern-specific query methods.
   * 
   * <p>Use the pattern matcher on a given model via {@link #on(ViatraQueryEngine)},
   * e.g. in conjunction with {@link ViatraQueryEngine#on(QueryScope)}.
   * 
   * <p>Matches of the pattern will be represented as {@link Match}.
   * 
   * <p>Original source:
   * <code><pre>
   * // Find mode instances
   * pattern findMode(modeinst: ModeInstance)
   * {
   * 	ComponentInstance.modeInstance(_,modeinst);
   * 	neg ModeInstance.parent(modeinst,_);
   * }
   * </pre></code>
   * 
   * @see Match
   * @see FindMode
   * 
   */
  public static class Matcher extends BaseMatcher<FindMode.Match> {
    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    public static FindMode.Matcher on(final ViatraQueryEngine engine) {
      // check if matcher already exists
      Matcher matcher = engine.getExistingMatcher(querySpecification());
      if (matcher == null) {
          matcher = (Matcher)engine.getMatcher(querySpecification());
      }
      return matcher;
    }

    /**
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * @return an initialized matcher
     * @noreference This method is for internal matcher initialization by the framework, do not call it manually.
     * 
     */
    public static FindMode.Matcher create() {
      return new Matcher();
    }

    private static final int POSITION_MODEINST = 0;

    private static final Logger LOGGER = ViatraQueryLoggingUtil.getLogger(FindMode.Matcher.class);

    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    private Matcher() {
      super(querySpecification());
    }

    /**
     * Returns the set of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pModeinst the fixed value of pattern parameter modeinst, or null if not bound.
     * @return matches represented as a Match object.
     * 
     */
    public Collection<FindMode.Match> getAllMatches(final ModeInstance pModeinst) {
      return rawStreamAllMatches(new Object[]{pModeinst}).collect(Collectors.toSet());
    }

    /**
     * Returns a stream of all matches of the pattern that conform to the given fixed values of some parameters.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     * @param pModeinst the fixed value of pattern parameter modeinst, or null if not bound.
     * @return a stream of matches represented as a Match object.
     * 
     */
    public Stream<FindMode.Match> streamAllMatches(final ModeInstance pModeinst) {
      return rawStreamAllMatches(new Object[]{pModeinst});
    }

    /**
     * Returns an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pModeinst the fixed value of pattern parameter modeinst, or null if not bound.
     * @return a match represented as a Match object, or null if no match is found.
     * 
     */
    public Optional<FindMode.Match> getOneArbitraryMatch(final ModeInstance pModeinst) {
      return rawGetOneArbitraryMatch(new Object[]{pModeinst});
    }

    /**
     * Indicates whether the given combination of specified pattern parameters constitute a valid pattern match,
     * under any possible substitution of the unspecified parameters (if any).
     * @param pModeinst the fixed value of pattern parameter modeinst, or null if not bound.
     * @return true if the input is a valid (partial) match of the pattern.
     * 
     */
    public boolean hasMatch(final ModeInstance pModeinst) {
      return rawHasMatch(new Object[]{pModeinst});
    }

    /**
     * Returns the number of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pModeinst the fixed value of pattern parameter modeinst, or null if not bound.
     * @return the number of pattern matches found.
     * 
     */
    public int countMatches(final ModeInstance pModeinst) {
      return rawCountMatches(new Object[]{pModeinst});
    }

    /**
     * Executes the given processor on an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pModeinst the fixed value of pattern parameter modeinst, or null if not bound.
     * @param processor the action that will process the selected match.
     * @return true if the pattern has at least one match with the given parameter values, false if the processor was not invoked
     * 
     */
    public boolean forOneArbitraryMatch(final ModeInstance pModeinst, final Consumer<? super FindMode.Match> processor) {
      return rawForOneArbitraryMatch(new Object[]{pModeinst}, processor);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pModeinst the fixed value of pattern parameter modeinst, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public FindMode.Match newMatch(final ModeInstance pModeinst) {
      return FindMode.Match.newMatch(pModeinst);
    }

    /**
     * Retrieve the set of values that occur in matches for modeinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ModeInstance> rawStreamAllValuesOfmodeinst(final Object[] parameters) {
      return rawStreamAllValues(POSITION_MODEINST, parameters).map(ModeInstance.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for modeinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ModeInstance> getAllValuesOfmodeinst() {
      return rawStreamAllValuesOfmodeinst(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for modeinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ModeInstance> streamAllValuesOfmodeinst() {
      return rawStreamAllValuesOfmodeinst(emptyArray());
    }

    @Override
    protected FindMode.Match tupleToMatch(final Tuple t) {
      try {
          return FindMode.Match.newMatch((ModeInstance) t.get(POSITION_MODEINST));
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in tuple not properly typed!",e);
          return null;
      }
    }

    @Override
    protected FindMode.Match arrayToMatch(final Object[] match) {
      try {
          return FindMode.Match.newMatch((ModeInstance) match[POSITION_MODEINST]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    @Override
    protected FindMode.Match arrayToMatchMutable(final Object[] match) {
      try {
          return FindMode.Match.newMutableMatch((ModeInstance) match[POSITION_MODEINST]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    /**
     * @return the singleton instance of the query specification of this pattern
     * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
     * 
     */
    public static IQuerySpecification<FindMode.Matcher> querySpecification() {
      return FindMode.instance();
    }
  }

  private FindMode() {
    super(GeneratedPQuery.INSTANCE);
  }

  /**
   * @return the singleton instance of the query specification
   * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
   * 
   */
  public static FindMode instance() {
    try{
        return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
        throw processInitializerError(err);
    }
  }

  @Override
  protected FindMode.Matcher instantiate(final ViatraQueryEngine engine) {
    return FindMode.Matcher.on(engine);
  }

  @Override
  public FindMode.Matcher instantiate() {
    return FindMode.Matcher.create();
  }

  @Override
  public FindMode.Match newEmptyMatch() {
    return FindMode.Match.newEmptyMatch();
  }

  @Override
  public FindMode.Match newMatch(final Object... parameters) {
    return FindMode.Match.newMatch((org.osate.aadl2.instance.ModeInstance) parameters[0]);
  }

  /**
   * Inner class allowing the singleton instance of {@link FindMode} to be created 
   *     <b>not</b> at the class load time of the outer class, 
   *     but rather at the first call to {@link FindMode#instance()}.
   * 
   * <p> This workaround is required e.g. to support recursion.
   * 
   */
  private static class LazyHolder {
    private static final FindMode INSTANCE = new FindMode();

    /**
     * Statically initializes the query specification <b>after</b> the field {@link #INSTANCE} is assigned.
     * This initialization order is required to support indirect recursion.
     * 
     * <p> The static initializer is defined using a helper field to work around limitations of the code generator.
     * 
     */
    private static final Object STATIC_INITIALIZER = ensureInitialized();

    public static Object ensureInitialized() {
      INSTANCE.ensureInitializedInternal();
      return null;
    }
  }

  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private static final FindMode.GeneratedPQuery INSTANCE = new GeneratedPQuery();

    private final PParameter parameter_modeinst = new PParameter("modeinst", "org.osate.aadl2.instance.ModeInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ModeInstance")), PParameterDirection.INOUT);

    private final List<PParameter> parameters = Arrays.asList(parameter_modeinst);

    private class Embedded_1_ModeInstance_parent extends BaseGeneratedEMFPQuery {
      private final PParameter parameter_p0 = new PParameter("p0", "org.osate.aadl2.instance.ModeInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ModeInstance")), PParameterDirection.INOUT);

      private final PParameter parameter_p1 = new PParameter("p1", "org.osate.aadl2.instance.ModeInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ModeInstance")), PParameterDirection.INOUT);

      private final List<PParameter> embeddedParameters = Arrays.asList(parameter_p0, parameter_p1);

      public Embedded_1_ModeInstance_parent() {
        super(PVisibility.EMBEDDED);
      }

      @Override
      public String getFullyQualifiedName() {
        return GeneratedPQuery.this.getFullyQualifiedName() + "$Embedded_1_ModeInstance_parent";
      }

      @Override
      public List<PParameter> getParameters() {
        return embeddedParameters;
      }

      @Override
      public Set<PBody> doGetContainedBodies() {
        PBody body = new PBody(this);
        PVariable var_p0 = body.getOrCreateVariableByName("p0");
        PVariable var_p1 = body.getOrCreateVariableByName("p1");
        body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
           new ExportedParameter(body, var_p0, parameter_p0),
           new ExportedParameter(body, var_p1, parameter_p1)
        ));
        //  ModeInstance.parent(modeinst,_)
        new TypeConstraint(body, Tuples.flatTupleOf(var_p0), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ModeInstance")));
        PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
        new TypeConstraint(body, Tuples.flatTupleOf(var_p0, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0/instance", "ModeInstance", "parent")));
        new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ModeInstance")));
        new Equality(body, var__virtual_0_, var_p1);
        return Collections.singleton(body);
      }
    }

    private GeneratedPQuery() {
      super(PVisibility.PUBLIC);
    }

    @Override
    public String getFullyQualifiedName() {
      return "fr.mem4csd.osatedim.viatra.queries.findMode";
    }

    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("modeinst");
    }

    @Override
    public List<PParameter> getParameters() {
      return parameters;
    }

    @Override
    public Set<PBody> doGetContainedBodies() {
      setEvaluationHints(new QueryEvaluationHint(null, QueryEvaluationHint.BackendRequirement.UNSPECIFIED));
      Set<PBody> bodies = new LinkedHashSet<>();
      {
          PBody body = new PBody(this);
          PVariable var_modeinst = body.getOrCreateVariableByName("modeinst");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          PVariable var___1_ = body.getOrCreateVariableByName("_<1>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_modeinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ModeInstance")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_modeinst, parameter_modeinst)
          ));
          // 	ComponentInstance.modeInstance(_,modeinst)
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance", "modeInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ModeInstance")));
          new Equality(body, var__virtual_0_, var_modeinst);
          // 	neg ModeInstance.parent(modeinst,_)
          new NegativePatternCall(body, Tuples.flatTupleOf(var_modeinst, var___1_), new FindMode.GeneratedPQuery.Embedded_1_ModeInstance_parent());
          bodies.add(body);
      }
      return bodies;
    }
  }
}
