/**
 * Generated from platform:/resource/fr.mem4csd.osatedim/src/fr/mem4csd/osatedim/viatra/queries/DIMQueriesOutplace.vql
 */
package fr.mem4csd.osatedim.viatra.queries;

import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedPatternGroup;

/**
 * A pattern group formed of all public patterns defined in DIMQueriesOutplace.vql.
 * 
 * <p>Use the static instance as any {@link interface org.eclipse.viatra.query.runtime.api.IQueryGroup}, to conveniently prepare
 * a VIATRA Query engine for matching all patterns originally defined in file DIMQueriesOutplace.vql,
 * in order to achieve better performance than one-by-one on-demand matcher initialization.
 * 
 * <p> From package fr.mem4csd.osatedim.viatra.queries, the group contains the definition of the following patterns: <ul>
 * <li>is_in_trace</li>
 * <li>is_subcomponent</li>
 * <li>find_PropertyAttach2Component</li>
 * <li>find_PropertyValAttach2Component</li>
 * <li>find_PropertyRefAttach2Component</li>
 * <li>find_PropertyValAttach2Feature</li>
 * <li>find_PropertyRefAttach2Feature</li>
 * <li>find_PropertyValAttach2Connection</li>
 * <li>find_PropertyRefAttach2Connection</li>
 * <li>find_ComponentAttach2ComponentType</li>
 * <li>find_ComponentAttach2ComponentImp</li>
 * <li>find_ConnectionAttach2Component</li>
 * <li>find_FeatureAttach2Component</li>
 * <li>find_ComponentDetach2Component</li>
 * <li>find_FeatureDetach2Component</li>
 * <li>find_ConnectionDetach2Component</li>
 * <li>find_Component2Component</li>
 * <li>find_Connection2Connection</li>
 * <li>find_Feature2Feature</li>
 * <li>find_Property2Property</li>
 * </ul>
 * 
 * @see IQueryGroup
 * 
 */
@SuppressWarnings("all")
public final class DIMQueriesOutplace extends BaseGeneratedPatternGroup {
  /**
   * Access the pattern group.
   * 
   * @return the singleton instance of the group
   * @throws ViatraQueryRuntimeException if there was an error loading the generated code of pattern specifications
   * 
   */
  public static DIMQueriesOutplace instance() {
    if (INSTANCE == null) {
        INSTANCE = new DIMQueriesOutplace();
    }
    return INSTANCE;
  }

  private static DIMQueriesOutplace INSTANCE;

  private DIMQueriesOutplace() {
    querySpecifications.add(Is_in_trace.instance());
    querySpecifications.add(Is_subcomponent.instance());
    querySpecifications.add(Find_PropertyAttach2Component.instance());
    querySpecifications.add(Find_PropertyValAttach2Component.instance());
    querySpecifications.add(Find_PropertyRefAttach2Component.instance());
    querySpecifications.add(Find_PropertyValAttach2Feature.instance());
    querySpecifications.add(Find_PropertyRefAttach2Feature.instance());
    querySpecifications.add(Find_PropertyValAttach2Connection.instance());
    querySpecifications.add(Find_PropertyRefAttach2Connection.instance());
    querySpecifications.add(Find_ComponentAttach2ComponentType.instance());
    querySpecifications.add(Find_ComponentAttach2ComponentImp.instance());
    querySpecifications.add(Find_ConnectionAttach2Component.instance());
    querySpecifications.add(Find_FeatureAttach2Component.instance());
    querySpecifications.add(Find_ComponentDetach2Component.instance());
    querySpecifications.add(Find_FeatureDetach2Component.instance());
    querySpecifications.add(Find_ConnectionDetach2Component.instance());
    querySpecifications.add(Find_Component2Component.instance());
    querySpecifications.add(Find_Connection2Connection.instance());
    querySpecifications.add(Find_Feature2Feature.instance());
    querySpecifications.add(Find_Property2Property.instance());
  }

  public Is_in_trace getIs_in_trace() {
    return Is_in_trace.instance();
  }

  public Is_in_trace.Matcher getIs_in_trace(final ViatraQueryEngine engine) {
    return Is_in_trace.Matcher.on(engine);
  }

  public Is_subcomponent getIs_subcomponent() {
    return Is_subcomponent.instance();
  }

  public Is_subcomponent.Matcher getIs_subcomponent(final ViatraQueryEngine engine) {
    return Is_subcomponent.Matcher.on(engine);
  }

  public Find_PropertyAttach2Component getFind_PropertyAttach2Component() {
    return Find_PropertyAttach2Component.instance();
  }

  public Find_PropertyAttach2Component.Matcher getFind_PropertyAttach2Component(final ViatraQueryEngine engine) {
    return Find_PropertyAttach2Component.Matcher.on(engine);
  }

  public Find_PropertyValAttach2Component getFind_PropertyValAttach2Component() {
    return Find_PropertyValAttach2Component.instance();
  }

  public Find_PropertyValAttach2Component.Matcher getFind_PropertyValAttach2Component(final ViatraQueryEngine engine) {
    return Find_PropertyValAttach2Component.Matcher.on(engine);
  }

  public Find_PropertyRefAttach2Component getFind_PropertyRefAttach2Component() {
    return Find_PropertyRefAttach2Component.instance();
  }

  public Find_PropertyRefAttach2Component.Matcher getFind_PropertyRefAttach2Component(final ViatraQueryEngine engine) {
    return Find_PropertyRefAttach2Component.Matcher.on(engine);
  }

  public Find_PropertyValAttach2Feature getFind_PropertyValAttach2Feature() {
    return Find_PropertyValAttach2Feature.instance();
  }

  public Find_PropertyValAttach2Feature.Matcher getFind_PropertyValAttach2Feature(final ViatraQueryEngine engine) {
    return Find_PropertyValAttach2Feature.Matcher.on(engine);
  }

  public Find_PropertyRefAttach2Feature getFind_PropertyRefAttach2Feature() {
    return Find_PropertyRefAttach2Feature.instance();
  }

  public Find_PropertyRefAttach2Feature.Matcher getFind_PropertyRefAttach2Feature(final ViatraQueryEngine engine) {
    return Find_PropertyRefAttach2Feature.Matcher.on(engine);
  }

  public Find_PropertyValAttach2Connection getFind_PropertyValAttach2Connection() {
    return Find_PropertyValAttach2Connection.instance();
  }

  public Find_PropertyValAttach2Connection.Matcher getFind_PropertyValAttach2Connection(final ViatraQueryEngine engine) {
    return Find_PropertyValAttach2Connection.Matcher.on(engine);
  }

  public Find_PropertyRefAttach2Connection getFind_PropertyRefAttach2Connection() {
    return Find_PropertyRefAttach2Connection.instance();
  }

  public Find_PropertyRefAttach2Connection.Matcher getFind_PropertyRefAttach2Connection(final ViatraQueryEngine engine) {
    return Find_PropertyRefAttach2Connection.Matcher.on(engine);
  }

  public Find_ComponentAttach2ComponentType getFind_ComponentAttach2ComponentType() {
    return Find_ComponentAttach2ComponentType.instance();
  }

  public Find_ComponentAttach2ComponentType.Matcher getFind_ComponentAttach2ComponentType(final ViatraQueryEngine engine) {
    return Find_ComponentAttach2ComponentType.Matcher.on(engine);
  }

  public Find_ComponentAttach2ComponentImp getFind_ComponentAttach2ComponentImp() {
    return Find_ComponentAttach2ComponentImp.instance();
  }

  public Find_ComponentAttach2ComponentImp.Matcher getFind_ComponentAttach2ComponentImp(final ViatraQueryEngine engine) {
    return Find_ComponentAttach2ComponentImp.Matcher.on(engine);
  }

  public Find_ConnectionAttach2Component getFind_ConnectionAttach2Component() {
    return Find_ConnectionAttach2Component.instance();
  }

  public Find_ConnectionAttach2Component.Matcher getFind_ConnectionAttach2Component(final ViatraQueryEngine engine) {
    return Find_ConnectionAttach2Component.Matcher.on(engine);
  }

  public Find_FeatureAttach2Component getFind_FeatureAttach2Component() {
    return Find_FeatureAttach2Component.instance();
  }

  public Find_FeatureAttach2Component.Matcher getFind_FeatureAttach2Component(final ViatraQueryEngine engine) {
    return Find_FeatureAttach2Component.Matcher.on(engine);
  }

  public Find_ComponentDetach2Component getFind_ComponentDetach2Component() {
    return Find_ComponentDetach2Component.instance();
  }

  public Find_ComponentDetach2Component.Matcher getFind_ComponentDetach2Component(final ViatraQueryEngine engine) {
    return Find_ComponentDetach2Component.Matcher.on(engine);
  }

  public Find_FeatureDetach2Component getFind_FeatureDetach2Component() {
    return Find_FeatureDetach2Component.instance();
  }

  public Find_FeatureDetach2Component.Matcher getFind_FeatureDetach2Component(final ViatraQueryEngine engine) {
    return Find_FeatureDetach2Component.Matcher.on(engine);
  }

  public Find_ConnectionDetach2Component getFind_ConnectionDetach2Component() {
    return Find_ConnectionDetach2Component.instance();
  }

  public Find_ConnectionDetach2Component.Matcher getFind_ConnectionDetach2Component(final ViatraQueryEngine engine) {
    return Find_ConnectionDetach2Component.Matcher.on(engine);
  }

  public Find_Component2Component getFind_Component2Component() {
    return Find_Component2Component.instance();
  }

  public Find_Component2Component.Matcher getFind_Component2Component(final ViatraQueryEngine engine) {
    return Find_Component2Component.Matcher.on(engine);
  }

  public Find_Connection2Connection getFind_Connection2Connection() {
    return Find_Connection2Connection.instance();
  }

  public Find_Connection2Connection.Matcher getFind_Connection2Connection(final ViatraQueryEngine engine) {
    return Find_Connection2Connection.Matcher.on(engine);
  }

  public Find_Feature2Feature getFind_Feature2Feature() {
    return Find_Feature2Feature.instance();
  }

  public Find_Feature2Feature.Matcher getFind_Feature2Feature(final ViatraQueryEngine engine) {
    return Find_Feature2Feature.Matcher.on(engine);
  }

  public Find_Property2Property getFind_Property2Property() {
    return Find_Property2Property.instance();
  }

  public Find_Property2Property.Matcher getFind_Property2Property(final ViatraQueryEngine engine) {
    return Find_Property2Property.Matcher.on(engine);
  }
}
