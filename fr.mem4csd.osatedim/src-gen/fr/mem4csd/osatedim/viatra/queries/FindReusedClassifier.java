/**
 * Generated from platform:/resource/fr.mem4csd.osatedim/src/fr/mem4csd/osatedim/viatra/queries/DIMQueriesInplace.vql
 */
package fr.mem4csd.osatedim.viatra.queries;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.viatra.query.runtime.api.IPatternMatch;
import org.eclipse.viatra.query.runtime.api.IQuerySpecification;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.viatra.query.runtime.api.impl.BaseMatcher;
import org.eclipse.viatra.query.runtime.api.impl.BasePatternMatch;
import org.eclipse.viatra.query.runtime.emf.types.EClassTransitiveInstancesKey;
import org.eclipse.viatra.query.runtime.emf.types.EStructuralFeatureInstancesKey;
import org.eclipse.viatra.query.runtime.matchers.backend.QueryEvaluationHint;
import org.eclipse.viatra.query.runtime.matchers.psystem.PBody;
import org.eclipse.viatra.query.runtime.matchers.psystem.PVariable;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.Equality;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.Inequality;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.TypeConstraint;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameterDirection;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PVisibility;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuple;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuples;
import org.eclipse.viatra.query.runtime.util.ViatraQueryLoggingUtil;
import org.osate.aadl2.ComponentClassifier;
import org.osate.aadl2.instance.ComponentInstance;

/**
 * A pattern-specific query specification that can instantiate Matcher in a type-safe way.
 * 
 * <p>Original source:
 *         <code><pre>
 *         // Check for reused classifiers
 *         pattern findReusedClassifier(compinst1 : ComponentInstance, compclass : ComponentClassifier, compinst2 : ComponentInstance) 
 *         {
 *         	ComponentInstance.classifier(compinst1, compclass);
 *         	ComponentInstance.classifier(compinst2, compclass);
 *         	compinst1 != compinst2;
 *         }
 * </pre></code>
 * 
 * @see Matcher
 * @see Match
 * 
 */
@SuppressWarnings("all")
public final class FindReusedClassifier extends BaseGeneratedEMFQuerySpecification<FindReusedClassifier.Matcher> {
  /**
   * Pattern-specific match representation of the fr.mem4csd.osatedim.viatra.queries.findReusedClassifier pattern,
   * to be used in conjunction with {@link Matcher}.
   * 
   * <p>Class fields correspond to parameters of the pattern. Fields with value null are considered unassigned.
   * Each instance is a (possibly partial) substitution of pattern parameters,
   * usable to represent a match of the pattern in the result of a query,
   * or to specify the bound (fixed) input parameters when issuing a query.
   * 
   * @see Matcher
   * 
   */
  public static abstract class Match extends BasePatternMatch {
    private ComponentInstance fCompinst1;

    private ComponentClassifier fCompclass;

    private ComponentInstance fCompinst2;

    private static List<String> parameterNames = makeImmutableList("compinst1", "compclass", "compinst2");

    private Match(final ComponentInstance pCompinst1, final ComponentClassifier pCompclass, final ComponentInstance pCompinst2) {
      this.fCompinst1 = pCompinst1;
      this.fCompclass = pCompclass;
      this.fCompinst2 = pCompinst2;
    }

    @Override
    public Object get(final String parameterName) {
      switch(parameterName) {
          case "compinst1": return this.fCompinst1;
          case "compclass": return this.fCompclass;
          case "compinst2": return this.fCompinst2;
          default: return null;
      }
    }

    @Override
    public Object get(final int index) {
      switch(index) {
          case 0: return this.fCompinst1;
          case 1: return this.fCompclass;
          case 2: return this.fCompinst2;
          default: return null;
      }
    }

    public ComponentInstance getCompinst1() {
      return this.fCompinst1;
    }

    public ComponentClassifier getCompclass() {
      return this.fCompclass;
    }

    public ComponentInstance getCompinst2() {
      return this.fCompinst2;
    }

    @Override
    public boolean set(final String parameterName, final Object newValue) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      if ("compinst1".equals(parameterName) ) {
          this.fCompinst1 = (ComponentInstance) newValue;
          return true;
      }
      if ("compclass".equals(parameterName) ) {
          this.fCompclass = (ComponentClassifier) newValue;
          return true;
      }
      if ("compinst2".equals(parameterName) ) {
          this.fCompinst2 = (ComponentInstance) newValue;
          return true;
      }
      return false;
    }

    public void setCompinst1(final ComponentInstance pCompinst1) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fCompinst1 = pCompinst1;
    }

    public void setCompclass(final ComponentClassifier pCompclass) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fCompclass = pCompclass;
    }

    public void setCompinst2(final ComponentInstance pCompinst2) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fCompinst2 = pCompinst2;
    }

    @Override
    public String patternName() {
      return "fr.mem4csd.osatedim.viatra.queries.findReusedClassifier";
    }

    @Override
    public List<String> parameterNames() {
      return FindReusedClassifier.Match.parameterNames;
    }

    @Override
    public Object[] toArray() {
      return new Object[]{fCompinst1, fCompclass, fCompinst2};
    }

    @Override
    public FindReusedClassifier.Match toImmutable() {
      return isMutable() ? newMatch(fCompinst1, fCompclass, fCompinst2) : this;
    }

    @Override
    public String prettyPrint() {
      StringBuilder result = new StringBuilder();
      result.append("\"compinst1\"=" + prettyPrintValue(fCompinst1) + ", ");
      result.append("\"compclass\"=" + prettyPrintValue(fCompclass) + ", ");
      result.append("\"compinst2\"=" + prettyPrintValue(fCompinst2));
      return result.toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(fCompinst1, fCompclass, fCompinst2);
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj)
          return true;
      if (obj == null) {
          return false;
      }
      if ((obj instanceof FindReusedClassifier.Match)) {
          FindReusedClassifier.Match other = (FindReusedClassifier.Match) obj;
          return Objects.equals(fCompinst1, other.fCompinst1) && Objects.equals(fCompclass, other.fCompclass) && Objects.equals(fCompinst2, other.fCompinst2);
      } else {
          // this should be infrequent
          if (!(obj instanceof IPatternMatch)) {
              return false;
          }
          IPatternMatch otherSig  = (IPatternMatch) obj;
          return Objects.equals(specification(), otherSig.specification()) && Arrays.deepEquals(toArray(), otherSig.toArray());
      }
    }

    @Override
    public FindReusedClassifier specification() {
      return FindReusedClassifier.instance();
    }

    /**
     * Returns an empty, mutable match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @return the empty match.
     * 
     */
    public static FindReusedClassifier.Match newEmptyMatch() {
      return new Mutable(null, null, null);
    }

    /**
     * Returns a mutable (partial) match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @param pCompinst1 the fixed value of pattern parameter compinst1, or null if not bound.
     * @param pCompclass the fixed value of pattern parameter compclass, or null if not bound.
     * @param pCompinst2 the fixed value of pattern parameter compinst2, or null if not bound.
     * @return the new, mutable (partial) match object.
     * 
     */
    public static FindReusedClassifier.Match newMutableMatch(final ComponentInstance pCompinst1, final ComponentClassifier pCompclass, final ComponentInstance pCompinst2) {
      return new Mutable(pCompinst1, pCompclass, pCompinst2);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pCompinst1 the fixed value of pattern parameter compinst1, or null if not bound.
     * @param pCompclass the fixed value of pattern parameter compclass, or null if not bound.
     * @param pCompinst2 the fixed value of pattern parameter compinst2, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public static FindReusedClassifier.Match newMatch(final ComponentInstance pCompinst1, final ComponentClassifier pCompclass, final ComponentInstance pCompinst2) {
      return new Immutable(pCompinst1, pCompclass, pCompinst2);
    }

    private static final class Mutable extends FindReusedClassifier.Match {
      Mutable(final ComponentInstance pCompinst1, final ComponentClassifier pCompclass, final ComponentInstance pCompinst2) {
        super(pCompinst1, pCompclass, pCompinst2);
      }

      @Override
      public boolean isMutable() {
        return true;
      }
    }

    private static final class Immutable extends FindReusedClassifier.Match {
      Immutable(final ComponentInstance pCompinst1, final ComponentClassifier pCompclass, final ComponentInstance pCompinst2) {
        super(pCompinst1, pCompclass, pCompinst2);
      }

      @Override
      public boolean isMutable() {
        return false;
      }
    }
  }

  /**
   * Generated pattern matcher API of the fr.mem4csd.osatedim.viatra.queries.findReusedClassifier pattern,
   * providing pattern-specific query methods.
   * 
   * <p>Use the pattern matcher on a given model via {@link #on(ViatraQueryEngine)},
   * e.g. in conjunction with {@link ViatraQueryEngine#on(QueryScope)}.
   * 
   * <p>Matches of the pattern will be represented as {@link Match}.
   * 
   * <p>Original source:
   * <code><pre>
   * // Check for reused classifiers
   * pattern findReusedClassifier(compinst1 : ComponentInstance, compclass : ComponentClassifier, compinst2 : ComponentInstance) 
   * {
   * 	ComponentInstance.classifier(compinst1, compclass);
   * 	ComponentInstance.classifier(compinst2, compclass);
   * 	compinst1 != compinst2;
   * }
   * </pre></code>
   * 
   * @see Match
   * @see FindReusedClassifier
   * 
   */
  public static class Matcher extends BaseMatcher<FindReusedClassifier.Match> {
    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    public static FindReusedClassifier.Matcher on(final ViatraQueryEngine engine) {
      // check if matcher already exists
      Matcher matcher = engine.getExistingMatcher(querySpecification());
      if (matcher == null) {
          matcher = (Matcher)engine.getMatcher(querySpecification());
      }
      return matcher;
    }

    /**
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * @return an initialized matcher
     * @noreference This method is for internal matcher initialization by the framework, do not call it manually.
     * 
     */
    public static FindReusedClassifier.Matcher create() {
      return new Matcher();
    }

    private static final int POSITION_COMPINST1 = 0;

    private static final int POSITION_COMPCLASS = 1;

    private static final int POSITION_COMPINST2 = 2;

    private static final Logger LOGGER = ViatraQueryLoggingUtil.getLogger(FindReusedClassifier.Matcher.class);

    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    private Matcher() {
      super(querySpecification());
    }

    /**
     * Returns the set of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pCompinst1 the fixed value of pattern parameter compinst1, or null if not bound.
     * @param pCompclass the fixed value of pattern parameter compclass, or null if not bound.
     * @param pCompinst2 the fixed value of pattern parameter compinst2, or null if not bound.
     * @return matches represented as a Match object.
     * 
     */
    public Collection<FindReusedClassifier.Match> getAllMatches(final ComponentInstance pCompinst1, final ComponentClassifier pCompclass, final ComponentInstance pCompinst2) {
      return rawStreamAllMatches(new Object[]{pCompinst1, pCompclass, pCompinst2}).collect(Collectors.toSet());
    }

    /**
     * Returns a stream of all matches of the pattern that conform to the given fixed values of some parameters.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     * @param pCompinst1 the fixed value of pattern parameter compinst1, or null if not bound.
     * @param pCompclass the fixed value of pattern parameter compclass, or null if not bound.
     * @param pCompinst2 the fixed value of pattern parameter compinst2, or null if not bound.
     * @return a stream of matches represented as a Match object.
     * 
     */
    public Stream<FindReusedClassifier.Match> streamAllMatches(final ComponentInstance pCompinst1, final ComponentClassifier pCompclass, final ComponentInstance pCompinst2) {
      return rawStreamAllMatches(new Object[]{pCompinst1, pCompclass, pCompinst2});
    }

    /**
     * Returns an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pCompinst1 the fixed value of pattern parameter compinst1, or null if not bound.
     * @param pCompclass the fixed value of pattern parameter compclass, or null if not bound.
     * @param pCompinst2 the fixed value of pattern parameter compinst2, or null if not bound.
     * @return a match represented as a Match object, or null if no match is found.
     * 
     */
    public Optional<FindReusedClassifier.Match> getOneArbitraryMatch(final ComponentInstance pCompinst1, final ComponentClassifier pCompclass, final ComponentInstance pCompinst2) {
      return rawGetOneArbitraryMatch(new Object[]{pCompinst1, pCompclass, pCompinst2});
    }

    /**
     * Indicates whether the given combination of specified pattern parameters constitute a valid pattern match,
     * under any possible substitution of the unspecified parameters (if any).
     * @param pCompinst1 the fixed value of pattern parameter compinst1, or null if not bound.
     * @param pCompclass the fixed value of pattern parameter compclass, or null if not bound.
     * @param pCompinst2 the fixed value of pattern parameter compinst2, or null if not bound.
     * @return true if the input is a valid (partial) match of the pattern.
     * 
     */
    public boolean hasMatch(final ComponentInstance pCompinst1, final ComponentClassifier pCompclass, final ComponentInstance pCompinst2) {
      return rawHasMatch(new Object[]{pCompinst1, pCompclass, pCompinst2});
    }

    /**
     * Returns the number of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pCompinst1 the fixed value of pattern parameter compinst1, or null if not bound.
     * @param pCompclass the fixed value of pattern parameter compclass, or null if not bound.
     * @param pCompinst2 the fixed value of pattern parameter compinst2, or null if not bound.
     * @return the number of pattern matches found.
     * 
     */
    public int countMatches(final ComponentInstance pCompinst1, final ComponentClassifier pCompclass, final ComponentInstance pCompinst2) {
      return rawCountMatches(new Object[]{pCompinst1, pCompclass, pCompinst2});
    }

    /**
     * Executes the given processor on an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pCompinst1 the fixed value of pattern parameter compinst1, or null if not bound.
     * @param pCompclass the fixed value of pattern parameter compclass, or null if not bound.
     * @param pCompinst2 the fixed value of pattern parameter compinst2, or null if not bound.
     * @param processor the action that will process the selected match.
     * @return true if the pattern has at least one match with the given parameter values, false if the processor was not invoked
     * 
     */
    public boolean forOneArbitraryMatch(final ComponentInstance pCompinst1, final ComponentClassifier pCompclass, final ComponentInstance pCompinst2, final Consumer<? super FindReusedClassifier.Match> processor) {
      return rawForOneArbitraryMatch(new Object[]{pCompinst1, pCompclass, pCompinst2}, processor);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pCompinst1 the fixed value of pattern parameter compinst1, or null if not bound.
     * @param pCompclass the fixed value of pattern parameter compclass, or null if not bound.
     * @param pCompinst2 the fixed value of pattern parameter compinst2, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public FindReusedClassifier.Match newMatch(final ComponentInstance pCompinst1, final ComponentClassifier pCompclass, final ComponentInstance pCompinst2) {
      return FindReusedClassifier.Match.newMatch(pCompinst1, pCompclass, pCompinst2);
    }

    /**
     * Retrieve the set of values that occur in matches for compinst1.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ComponentInstance> rawStreamAllValuesOfcompinst1(final Object[] parameters) {
      return rawStreamAllValues(POSITION_COMPINST1, parameters).map(ComponentInstance.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for compinst1.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst1() {
      return rawStreamAllValuesOfcompinst1(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst1.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst1() {
      return rawStreamAllValuesOfcompinst1(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst1.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst1(final FindReusedClassifier.Match partialMatch) {
      return rawStreamAllValuesOfcompinst1(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst1.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst1(final ComponentClassifier pCompclass, final ComponentInstance pCompinst2) {
      return rawStreamAllValuesOfcompinst1(new Object[]{null, pCompclass, pCompinst2});
    }

    /**
     * Retrieve the set of values that occur in matches for compinst1.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst1(final FindReusedClassifier.Match partialMatch) {
      return rawStreamAllValuesOfcompinst1(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst1.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst1(final ComponentClassifier pCompclass, final ComponentInstance pCompinst2) {
      return rawStreamAllValuesOfcompinst1(new Object[]{null, pCompclass, pCompinst2}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compclass.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ComponentClassifier> rawStreamAllValuesOfcompclass(final Object[] parameters) {
      return rawStreamAllValues(POSITION_COMPCLASS, parameters).map(ComponentClassifier.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for compclass.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentClassifier> getAllValuesOfcompclass() {
      return rawStreamAllValuesOfcompclass(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compclass.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentClassifier> streamAllValuesOfcompclass() {
      return rawStreamAllValuesOfcompclass(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for compclass.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentClassifier> streamAllValuesOfcompclass(final FindReusedClassifier.Match partialMatch) {
      return rawStreamAllValuesOfcompclass(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for compclass.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentClassifier> streamAllValuesOfcompclass(final ComponentInstance pCompinst1, final ComponentInstance pCompinst2) {
      return rawStreamAllValuesOfcompclass(new Object[]{pCompinst1, null, pCompinst2});
    }

    /**
     * Retrieve the set of values that occur in matches for compclass.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentClassifier> getAllValuesOfcompclass(final FindReusedClassifier.Match partialMatch) {
      return rawStreamAllValuesOfcompclass(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compclass.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentClassifier> getAllValuesOfcompclass(final ComponentInstance pCompinst1, final ComponentInstance pCompinst2) {
      return rawStreamAllValuesOfcompclass(new Object[]{pCompinst1, null, pCompinst2}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst2.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ComponentInstance> rawStreamAllValuesOfcompinst2(final Object[] parameters) {
      return rawStreamAllValues(POSITION_COMPINST2, parameters).map(ComponentInstance.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for compinst2.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst2() {
      return rawStreamAllValuesOfcompinst2(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst2.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst2() {
      return rawStreamAllValuesOfcompinst2(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst2.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst2(final FindReusedClassifier.Match partialMatch) {
      return rawStreamAllValuesOfcompinst2(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst2.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst2(final ComponentInstance pCompinst1, final ComponentClassifier pCompclass) {
      return rawStreamAllValuesOfcompinst2(new Object[]{pCompinst1, pCompclass, null});
    }

    /**
     * Retrieve the set of values that occur in matches for compinst2.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst2(final FindReusedClassifier.Match partialMatch) {
      return rawStreamAllValuesOfcompinst2(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst2.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst2(final ComponentInstance pCompinst1, final ComponentClassifier pCompclass) {
      return rawStreamAllValuesOfcompinst2(new Object[]{pCompinst1, pCompclass, null}).collect(Collectors.toSet());
    }

    @Override
    protected FindReusedClassifier.Match tupleToMatch(final Tuple t) {
      try {
          return FindReusedClassifier.Match.newMatch((ComponentInstance) t.get(POSITION_COMPINST1), (ComponentClassifier) t.get(POSITION_COMPCLASS), (ComponentInstance) t.get(POSITION_COMPINST2));
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in tuple not properly typed!",e);
          return null;
      }
    }

    @Override
    protected FindReusedClassifier.Match arrayToMatch(final Object[] match) {
      try {
          return FindReusedClassifier.Match.newMatch((ComponentInstance) match[POSITION_COMPINST1], (ComponentClassifier) match[POSITION_COMPCLASS], (ComponentInstance) match[POSITION_COMPINST2]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    @Override
    protected FindReusedClassifier.Match arrayToMatchMutable(final Object[] match) {
      try {
          return FindReusedClassifier.Match.newMutableMatch((ComponentInstance) match[POSITION_COMPINST1], (ComponentClassifier) match[POSITION_COMPCLASS], (ComponentInstance) match[POSITION_COMPINST2]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    /**
     * @return the singleton instance of the query specification of this pattern
     * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
     * 
     */
    public static IQuerySpecification<FindReusedClassifier.Matcher> querySpecification() {
      return FindReusedClassifier.instance();
    }
  }

  private FindReusedClassifier() {
    super(GeneratedPQuery.INSTANCE);
  }

  /**
   * @return the singleton instance of the query specification
   * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
   * 
   */
  public static FindReusedClassifier instance() {
    try{
        return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
        throw processInitializerError(err);
    }
  }

  @Override
  protected FindReusedClassifier.Matcher instantiate(final ViatraQueryEngine engine) {
    return FindReusedClassifier.Matcher.on(engine);
  }

  @Override
  public FindReusedClassifier.Matcher instantiate() {
    return FindReusedClassifier.Matcher.create();
  }

  @Override
  public FindReusedClassifier.Match newEmptyMatch() {
    return FindReusedClassifier.Match.newEmptyMatch();
  }

  @Override
  public FindReusedClassifier.Match newMatch(final Object... parameters) {
    return FindReusedClassifier.Match.newMatch((org.osate.aadl2.instance.ComponentInstance) parameters[0], (org.osate.aadl2.ComponentClassifier) parameters[1], (org.osate.aadl2.instance.ComponentInstance) parameters[2]);
  }

  /**
   * Inner class allowing the singleton instance of {@link FindReusedClassifier} to be created 
   *     <b>not</b> at the class load time of the outer class, 
   *     but rather at the first call to {@link FindReusedClassifier#instance()}.
   * 
   * <p> This workaround is required e.g. to support recursion.
   * 
   */
  private static class LazyHolder {
    private static final FindReusedClassifier INSTANCE = new FindReusedClassifier();

    /**
     * Statically initializes the query specification <b>after</b> the field {@link #INSTANCE} is assigned.
     * This initialization order is required to support indirect recursion.
     * 
     * <p> The static initializer is defined using a helper field to work around limitations of the code generator.
     * 
     */
    private static final Object STATIC_INITIALIZER = ensureInitialized();

    public static Object ensureInitialized() {
      INSTANCE.ensureInitializedInternal();
      return null;
    }
  }

  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private static final FindReusedClassifier.GeneratedPQuery INSTANCE = new GeneratedPQuery();

    private final PParameter parameter_compinst1 = new PParameter("compinst1", "org.osate.aadl2.instance.ComponentInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ComponentInstance")), PParameterDirection.INOUT);

    private final PParameter parameter_compclass = new PParameter("compclass", "org.osate.aadl2.ComponentClassifier", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0", "ComponentClassifier")), PParameterDirection.INOUT);

    private final PParameter parameter_compinst2 = new PParameter("compinst2", "org.osate.aadl2.instance.ComponentInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ComponentInstance")), PParameterDirection.INOUT);

    private final List<PParameter> parameters = Arrays.asList(parameter_compinst1, parameter_compclass, parameter_compinst2);

    private GeneratedPQuery() {
      super(PVisibility.PUBLIC);
    }

    @Override
    public String getFullyQualifiedName() {
      return "fr.mem4csd.osatedim.viatra.queries.findReusedClassifier";
    }

    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("compinst1","compclass","compinst2");
    }

    @Override
    public List<PParameter> getParameters() {
      return parameters;
    }

    @Override
    public Set<PBody> doGetContainedBodies() {
      setEvaluationHints(new QueryEvaluationHint(null, QueryEvaluationHint.BackendRequirement.UNSPECIFIED));
      Set<PBody> bodies = new LinkedHashSet<>();
      {
          PBody body = new PBody(this);
          PVariable var_compinst1 = body.getOrCreateVariableByName("compinst1");
          PVariable var_compclass = body.getOrCreateVariableByName("compclass");
          PVariable var_compinst2 = body.getOrCreateVariableByName("compinst2");
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst1), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_compclass), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ComponentClassifier")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst2), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_compinst1, parameter_compinst1),
             new ExportedParameter(body, var_compclass, parameter_compclass),
             new ExportedParameter(body, var_compinst2, parameter_compinst2)
          ));
          // 	ComponentInstance.classifier(compinst1, compclass)
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst1), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst1, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance", "classifier")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ComponentClassifier")));
          new Equality(body, var__virtual_0_, var_compclass);
          // 	ComponentInstance.classifier(compinst2, compclass)
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst2), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst2, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance", "classifier")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ComponentClassifier")));
          new Equality(body, var__virtual_1_, var_compclass);
          // 	compinst1 != compinst2
          new Inequality(body, var_compinst1, var_compinst2);
          bodies.add(body);
      }
      return bodies;
    }
  }
}
