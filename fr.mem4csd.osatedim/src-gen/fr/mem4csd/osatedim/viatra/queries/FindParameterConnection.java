/**
 * Generated from platform:/resource/fr.mem4csd.osatedim/src/fr/mem4csd/osatedim/viatra/queries/DIMQueriesInplace.vql
 */
package fr.mem4csd.osatedim.viatra.queries;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.viatra.query.runtime.api.IPatternMatch;
import org.eclipse.viatra.query.runtime.api.IQuerySpecification;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.viatra.query.runtime.api.impl.BaseMatcher;
import org.eclipse.viatra.query.runtime.api.impl.BasePatternMatch;
import org.eclipse.viatra.query.runtime.emf.types.EClassTransitiveInstancesKey;
import org.eclipse.viatra.query.runtime.emf.types.EStructuralFeatureInstancesKey;
import org.eclipse.viatra.query.runtime.matchers.backend.QueryEvaluationHint;
import org.eclipse.viatra.query.runtime.matchers.psystem.PBody;
import org.eclipse.viatra.query.runtime.matchers.psystem.PVariable;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.Equality;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.TypeConstraint;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameterDirection;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PVisibility;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuple;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuples;
import org.eclipse.viatra.query.runtime.util.ViatraQueryLoggingUtil;
import org.osate.aadl2.Feature;
import org.osate.aadl2.ParameterConnection;

/**
 * A pattern-specific query specification that can instantiate Matcher in a type-safe way.
 * 
 * <p>Original source:
 *         <code><pre>
 *         // QUERIES FOR EXCEPTION HANDLING
 *         
 *         pattern findParameterConnection(parconn : ParameterConnection, feat : Feature) {
 *         	AbstractImplementation.ownedParameterConnection(_,parconn);
 *         	ParameterConnection.source.connectionEnd(parconn, feat); } or {
 *         	BusImplementation.ownedParameterConnection(_,parconn); 
 *         	ParameterConnection.source.connectionEnd(parconn, feat); } or {
 *         	DataImplementation.ownedParameterConnection(_,parconn); 
 *         	ParameterConnection.source.connectionEnd(parconn, feat); } or {
 *         	DeviceImplementation.ownedParameterConnection(_,parconn);
 *         	ParameterConnection.source.connectionEnd(parconn, feat); } or {
 *         	MemoryImplementation.ownedParameterConnection(_,parconn);
 *         	ParameterConnection.source.connectionEnd(parconn, feat); } or {
 *         	ProcessImplementation.ownedParameterConnection(_,parconn);
 *         	ParameterConnection.source.connectionEnd(parconn, feat); } or {
 *         	ProcessorImplementation.ownedParameterConnection(_,parconn);
 *         	ParameterConnection.source.connectionEnd(parconn, feat); } or {
 *         	SubprogramImplementation.ownedParameterConnection(_,parconn);
 *         	ParameterConnection.source.connectionEnd(parconn, feat); } or {
 *         	SubprogramGroupImplementation.ownedParameterConnection(_,parconn);
 *         	ParameterConnection.source.connectionEnd(parconn, feat); } or {
 *         	SystemImplementation.ownedParameterConnection(_,parconn);
 *         	ParameterConnection.source.connectionEnd(parconn, feat); } or {
 *         	ThreadImplementation.ownedParameterConnection(_,parconn);
 *         	ParameterConnection.source.connectionEnd(parconn, feat); } or {
 *         	ThreadGroupImplementation.ownedParameterConnection(_,parconn);
 *         	ParameterConnection.source.connectionEnd(parconn, feat); } or {
 *         	VirtualBusImplementation.ownedParameterConnection(_,parconn);
 *         	ParameterConnection.source.connectionEnd(parconn, feat); } or {
 *         	VirtualProcessorImplementation.ownedParameterConnection(_,parconn);
 *         	ParameterConnection.source.connectionEnd(parconn, feat); } or {
 *         	AbstractImplementation.ownedParameterConnection(_,parconn);
 *         	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
 *         	BusImplementation.ownedParameterConnection(_,parconn); 
 *         	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
 *         	DataImplementation.ownedParameterConnection(_,parconn); 
 *         	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
 *         	DeviceImplementation.ownedParameterConnection(_,parconn);
 *         	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
 *         	MemoryImplementation.ownedParameterConnection(_,parconn);
 *         	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
 *         	ProcessImplementation.ownedParameterConnection(_,parconn);
 *         	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
 *         	ProcessorImplementation.ownedParameterConnection(_,parconn);
 *         	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
 *         	SubprogramImplementation.ownedParameterConnection(_,parconn);
 *         	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
 *         	SubprogramGroupImplementation.ownedParameterConnection(_,parconn);
 *         	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
 *         	SystemImplementation.ownedParameterConnection(_,parconn);
 *         	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
 *         	ThreadImplementation.ownedParameterConnection(_,parconn);
 *         	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
 *         	ThreadGroupImplementation.ownedParameterConnection(_,parconn);
 *         	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
 *         	VirtualBusImplementation.ownedParameterConnection(_,parconn);
 *         	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
 *         	VirtualProcessorImplementation.ownedParameterConnection(_,parconn);
 *         	ParameterConnection.destination.connectionEnd(parconn, feat); 
 *         }
 * </pre></code>
 * 
 * @see Matcher
 * @see Match
 * 
 */
@SuppressWarnings("all")
public final class FindParameterConnection extends BaseGeneratedEMFQuerySpecification<FindParameterConnection.Matcher> {
  /**
   * Pattern-specific match representation of the fr.mem4csd.osatedim.viatra.queries.findParameterConnection pattern,
   * to be used in conjunction with {@link Matcher}.
   * 
   * <p>Class fields correspond to parameters of the pattern. Fields with value null are considered unassigned.
   * Each instance is a (possibly partial) substitution of pattern parameters,
   * usable to represent a match of the pattern in the result of a query,
   * or to specify the bound (fixed) input parameters when issuing a query.
   * 
   * @see Matcher
   * 
   */
  public static abstract class Match extends BasePatternMatch {
    private ParameterConnection fParconn;

    private Feature fFeat;

    private static List<String> parameterNames = makeImmutableList("parconn", "feat");

    private Match(final ParameterConnection pParconn, final Feature pFeat) {
      this.fParconn = pParconn;
      this.fFeat = pFeat;
    }

    @Override
    public Object get(final String parameterName) {
      switch(parameterName) {
          case "parconn": return this.fParconn;
          case "feat": return this.fFeat;
          default: return null;
      }
    }

    @Override
    public Object get(final int index) {
      switch(index) {
          case 0: return this.fParconn;
          case 1: return this.fFeat;
          default: return null;
      }
    }

    public ParameterConnection getParconn() {
      return this.fParconn;
    }

    public Feature getFeat() {
      return this.fFeat;
    }

    @Override
    public boolean set(final String parameterName, final Object newValue) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      if ("parconn".equals(parameterName) ) {
          this.fParconn = (ParameterConnection) newValue;
          return true;
      }
      if ("feat".equals(parameterName) ) {
          this.fFeat = (Feature) newValue;
          return true;
      }
      return false;
    }

    public void setParconn(final ParameterConnection pParconn) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fParconn = pParconn;
    }

    public void setFeat(final Feature pFeat) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fFeat = pFeat;
    }

    @Override
    public String patternName() {
      return "fr.mem4csd.osatedim.viatra.queries.findParameterConnection";
    }

    @Override
    public List<String> parameterNames() {
      return FindParameterConnection.Match.parameterNames;
    }

    @Override
    public Object[] toArray() {
      return new Object[]{fParconn, fFeat};
    }

    @Override
    public FindParameterConnection.Match toImmutable() {
      return isMutable() ? newMatch(fParconn, fFeat) : this;
    }

    @Override
    public String prettyPrint() {
      StringBuilder result = new StringBuilder();
      result.append("\"parconn\"=" + prettyPrintValue(fParconn) + ", ");
      result.append("\"feat\"=" + prettyPrintValue(fFeat));
      return result.toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(fParconn, fFeat);
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj)
          return true;
      if (obj == null) {
          return false;
      }
      if ((obj instanceof FindParameterConnection.Match)) {
          FindParameterConnection.Match other = (FindParameterConnection.Match) obj;
          return Objects.equals(fParconn, other.fParconn) && Objects.equals(fFeat, other.fFeat);
      } else {
          // this should be infrequent
          if (!(obj instanceof IPatternMatch)) {
              return false;
          }
          IPatternMatch otherSig  = (IPatternMatch) obj;
          return Objects.equals(specification(), otherSig.specification()) && Arrays.deepEquals(toArray(), otherSig.toArray());
      }
    }

    @Override
    public FindParameterConnection specification() {
      return FindParameterConnection.instance();
    }

    /**
     * Returns an empty, mutable match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @return the empty match.
     * 
     */
    public static FindParameterConnection.Match newEmptyMatch() {
      return new Mutable(null, null);
    }

    /**
     * Returns a mutable (partial) match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @param pParconn the fixed value of pattern parameter parconn, or null if not bound.
     * @param pFeat the fixed value of pattern parameter feat, or null if not bound.
     * @return the new, mutable (partial) match object.
     * 
     */
    public static FindParameterConnection.Match newMutableMatch(final ParameterConnection pParconn, final Feature pFeat) {
      return new Mutable(pParconn, pFeat);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pParconn the fixed value of pattern parameter parconn, or null if not bound.
     * @param pFeat the fixed value of pattern parameter feat, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public static FindParameterConnection.Match newMatch(final ParameterConnection pParconn, final Feature pFeat) {
      return new Immutable(pParconn, pFeat);
    }

    private static final class Mutable extends FindParameterConnection.Match {
      Mutable(final ParameterConnection pParconn, final Feature pFeat) {
        super(pParconn, pFeat);
      }

      @Override
      public boolean isMutable() {
        return true;
      }
    }

    private static final class Immutable extends FindParameterConnection.Match {
      Immutable(final ParameterConnection pParconn, final Feature pFeat) {
        super(pParconn, pFeat);
      }

      @Override
      public boolean isMutable() {
        return false;
      }
    }
  }

  /**
   * Generated pattern matcher API of the fr.mem4csd.osatedim.viatra.queries.findParameterConnection pattern,
   * providing pattern-specific query methods.
   * 
   * <p>Use the pattern matcher on a given model via {@link #on(ViatraQueryEngine)},
   * e.g. in conjunction with {@link ViatraQueryEngine#on(QueryScope)}.
   * 
   * <p>Matches of the pattern will be represented as {@link Match}.
   * 
   * <p>Original source:
   * <code><pre>
   * // QUERIES FOR EXCEPTION HANDLING
   * 
   * pattern findParameterConnection(parconn : ParameterConnection, feat : Feature) {
   * 	AbstractImplementation.ownedParameterConnection(_,parconn);
   * 	ParameterConnection.source.connectionEnd(parconn, feat); } or {
   * 	BusImplementation.ownedParameterConnection(_,parconn); 
   * 	ParameterConnection.source.connectionEnd(parconn, feat); } or {
   * 	DataImplementation.ownedParameterConnection(_,parconn); 
   * 	ParameterConnection.source.connectionEnd(parconn, feat); } or {
   * 	DeviceImplementation.ownedParameterConnection(_,parconn);
   * 	ParameterConnection.source.connectionEnd(parconn, feat); } or {
   * 	MemoryImplementation.ownedParameterConnection(_,parconn);
   * 	ParameterConnection.source.connectionEnd(parconn, feat); } or {
   * 	ProcessImplementation.ownedParameterConnection(_,parconn);
   * 	ParameterConnection.source.connectionEnd(parconn, feat); } or {
   * 	ProcessorImplementation.ownedParameterConnection(_,parconn);
   * 	ParameterConnection.source.connectionEnd(parconn, feat); } or {
   * 	SubprogramImplementation.ownedParameterConnection(_,parconn);
   * 	ParameterConnection.source.connectionEnd(parconn, feat); } or {
   * 	SubprogramGroupImplementation.ownedParameterConnection(_,parconn);
   * 	ParameterConnection.source.connectionEnd(parconn, feat); } or {
   * 	SystemImplementation.ownedParameterConnection(_,parconn);
   * 	ParameterConnection.source.connectionEnd(parconn, feat); } or {
   * 	ThreadImplementation.ownedParameterConnection(_,parconn);
   * 	ParameterConnection.source.connectionEnd(parconn, feat); } or {
   * 	ThreadGroupImplementation.ownedParameterConnection(_,parconn);
   * 	ParameterConnection.source.connectionEnd(parconn, feat); } or {
   * 	VirtualBusImplementation.ownedParameterConnection(_,parconn);
   * 	ParameterConnection.source.connectionEnd(parconn, feat); } or {
   * 	VirtualProcessorImplementation.ownedParameterConnection(_,parconn);
   * 	ParameterConnection.source.connectionEnd(parconn, feat); } or {
   * 	AbstractImplementation.ownedParameterConnection(_,parconn);
   * 	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
   * 	BusImplementation.ownedParameterConnection(_,parconn); 
   * 	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
   * 	DataImplementation.ownedParameterConnection(_,parconn); 
   * 	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
   * 	DeviceImplementation.ownedParameterConnection(_,parconn);
   * 	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
   * 	MemoryImplementation.ownedParameterConnection(_,parconn);
   * 	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
   * 	ProcessImplementation.ownedParameterConnection(_,parconn);
   * 	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
   * 	ProcessorImplementation.ownedParameterConnection(_,parconn);
   * 	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
   * 	SubprogramImplementation.ownedParameterConnection(_,parconn);
   * 	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
   * 	SubprogramGroupImplementation.ownedParameterConnection(_,parconn);
   * 	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
   * 	SystemImplementation.ownedParameterConnection(_,parconn);
   * 	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
   * 	ThreadImplementation.ownedParameterConnection(_,parconn);
   * 	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
   * 	ThreadGroupImplementation.ownedParameterConnection(_,parconn);
   * 	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
   * 	VirtualBusImplementation.ownedParameterConnection(_,parconn);
   * 	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
   * 	VirtualProcessorImplementation.ownedParameterConnection(_,parconn);
   * 	ParameterConnection.destination.connectionEnd(parconn, feat); 
   * }
   * </pre></code>
   * 
   * @see Match
   * @see FindParameterConnection
   * 
   */
  public static class Matcher extends BaseMatcher<FindParameterConnection.Match> {
    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    public static FindParameterConnection.Matcher on(final ViatraQueryEngine engine) {
      // check if matcher already exists
      Matcher matcher = engine.getExistingMatcher(querySpecification());
      if (matcher == null) {
          matcher = (Matcher)engine.getMatcher(querySpecification());
      }
      return matcher;
    }

    /**
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * @return an initialized matcher
     * @noreference This method is for internal matcher initialization by the framework, do not call it manually.
     * 
     */
    public static FindParameterConnection.Matcher create() {
      return new Matcher();
    }

    private static final int POSITION_PARCONN = 0;

    private static final int POSITION_FEAT = 1;

    private static final Logger LOGGER = ViatraQueryLoggingUtil.getLogger(FindParameterConnection.Matcher.class);

    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    private Matcher() {
      super(querySpecification());
    }

    /**
     * Returns the set of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pParconn the fixed value of pattern parameter parconn, or null if not bound.
     * @param pFeat the fixed value of pattern parameter feat, or null if not bound.
     * @return matches represented as a Match object.
     * 
     */
    public Collection<FindParameterConnection.Match> getAllMatches(final ParameterConnection pParconn, final Feature pFeat) {
      return rawStreamAllMatches(new Object[]{pParconn, pFeat}).collect(Collectors.toSet());
    }

    /**
     * Returns a stream of all matches of the pattern that conform to the given fixed values of some parameters.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     * @param pParconn the fixed value of pattern parameter parconn, or null if not bound.
     * @param pFeat the fixed value of pattern parameter feat, or null if not bound.
     * @return a stream of matches represented as a Match object.
     * 
     */
    public Stream<FindParameterConnection.Match> streamAllMatches(final ParameterConnection pParconn, final Feature pFeat) {
      return rawStreamAllMatches(new Object[]{pParconn, pFeat});
    }

    /**
     * Returns an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pParconn the fixed value of pattern parameter parconn, or null if not bound.
     * @param pFeat the fixed value of pattern parameter feat, or null if not bound.
     * @return a match represented as a Match object, or null if no match is found.
     * 
     */
    public Optional<FindParameterConnection.Match> getOneArbitraryMatch(final ParameterConnection pParconn, final Feature pFeat) {
      return rawGetOneArbitraryMatch(new Object[]{pParconn, pFeat});
    }

    /**
     * Indicates whether the given combination of specified pattern parameters constitute a valid pattern match,
     * under any possible substitution of the unspecified parameters (if any).
     * @param pParconn the fixed value of pattern parameter parconn, or null if not bound.
     * @param pFeat the fixed value of pattern parameter feat, or null if not bound.
     * @return true if the input is a valid (partial) match of the pattern.
     * 
     */
    public boolean hasMatch(final ParameterConnection pParconn, final Feature pFeat) {
      return rawHasMatch(new Object[]{pParconn, pFeat});
    }

    /**
     * Returns the number of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pParconn the fixed value of pattern parameter parconn, or null if not bound.
     * @param pFeat the fixed value of pattern parameter feat, or null if not bound.
     * @return the number of pattern matches found.
     * 
     */
    public int countMatches(final ParameterConnection pParconn, final Feature pFeat) {
      return rawCountMatches(new Object[]{pParconn, pFeat});
    }

    /**
     * Executes the given processor on an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pParconn the fixed value of pattern parameter parconn, or null if not bound.
     * @param pFeat the fixed value of pattern parameter feat, or null if not bound.
     * @param processor the action that will process the selected match.
     * @return true if the pattern has at least one match with the given parameter values, false if the processor was not invoked
     * 
     */
    public boolean forOneArbitraryMatch(final ParameterConnection pParconn, final Feature pFeat, final Consumer<? super FindParameterConnection.Match> processor) {
      return rawForOneArbitraryMatch(new Object[]{pParconn, pFeat}, processor);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pParconn the fixed value of pattern parameter parconn, or null if not bound.
     * @param pFeat the fixed value of pattern parameter feat, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public FindParameterConnection.Match newMatch(final ParameterConnection pParconn, final Feature pFeat) {
      return FindParameterConnection.Match.newMatch(pParconn, pFeat);
    }

    /**
     * Retrieve the set of values that occur in matches for parconn.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ParameterConnection> rawStreamAllValuesOfparconn(final Object[] parameters) {
      return rawStreamAllValues(POSITION_PARCONN, parameters).map(ParameterConnection.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for parconn.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ParameterConnection> getAllValuesOfparconn() {
      return rawStreamAllValuesOfparconn(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for parconn.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ParameterConnection> streamAllValuesOfparconn() {
      return rawStreamAllValuesOfparconn(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for parconn.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ParameterConnection> streamAllValuesOfparconn(final FindParameterConnection.Match partialMatch) {
      return rawStreamAllValuesOfparconn(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for parconn.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ParameterConnection> streamAllValuesOfparconn(final Feature pFeat) {
      return rawStreamAllValuesOfparconn(new Object[]{null, pFeat});
    }

    /**
     * Retrieve the set of values that occur in matches for parconn.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ParameterConnection> getAllValuesOfparconn(final FindParameterConnection.Match partialMatch) {
      return rawStreamAllValuesOfparconn(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for parconn.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ParameterConnection> getAllValuesOfparconn(final Feature pFeat) {
      return rawStreamAllValuesOfparconn(new Object[]{null, pFeat}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for feat.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<Feature> rawStreamAllValuesOffeat(final Object[] parameters) {
      return rawStreamAllValues(POSITION_FEAT, parameters).map(Feature.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for feat.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Feature> getAllValuesOffeat() {
      return rawStreamAllValuesOffeat(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for feat.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<Feature> streamAllValuesOffeat() {
      return rawStreamAllValuesOffeat(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for feat.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<Feature> streamAllValuesOffeat(final FindParameterConnection.Match partialMatch) {
      return rawStreamAllValuesOffeat(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for feat.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<Feature> streamAllValuesOffeat(final ParameterConnection pParconn) {
      return rawStreamAllValuesOffeat(new Object[]{pParconn, null});
    }

    /**
     * Retrieve the set of values that occur in matches for feat.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Feature> getAllValuesOffeat(final FindParameterConnection.Match partialMatch) {
      return rawStreamAllValuesOffeat(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for feat.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Feature> getAllValuesOffeat(final ParameterConnection pParconn) {
      return rawStreamAllValuesOffeat(new Object[]{pParconn, null}).collect(Collectors.toSet());
    }

    @Override
    protected FindParameterConnection.Match tupleToMatch(final Tuple t) {
      try {
          return FindParameterConnection.Match.newMatch((ParameterConnection) t.get(POSITION_PARCONN), (Feature) t.get(POSITION_FEAT));
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in tuple not properly typed!",e);
          return null;
      }
    }

    @Override
    protected FindParameterConnection.Match arrayToMatch(final Object[] match) {
      try {
          return FindParameterConnection.Match.newMatch((ParameterConnection) match[POSITION_PARCONN], (Feature) match[POSITION_FEAT]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    @Override
    protected FindParameterConnection.Match arrayToMatchMutable(final Object[] match) {
      try {
          return FindParameterConnection.Match.newMutableMatch((ParameterConnection) match[POSITION_PARCONN], (Feature) match[POSITION_FEAT]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    /**
     * @return the singleton instance of the query specification of this pattern
     * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
     * 
     */
    public static IQuerySpecification<FindParameterConnection.Matcher> querySpecification() {
      return FindParameterConnection.instance();
    }
  }

  private FindParameterConnection() {
    super(GeneratedPQuery.INSTANCE);
  }

  /**
   * @return the singleton instance of the query specification
   * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
   * 
   */
  public static FindParameterConnection instance() {
    try{
        return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
        throw processInitializerError(err);
    }
  }

  @Override
  protected FindParameterConnection.Matcher instantiate(final ViatraQueryEngine engine) {
    return FindParameterConnection.Matcher.on(engine);
  }

  @Override
  public FindParameterConnection.Matcher instantiate() {
    return FindParameterConnection.Matcher.create();
  }

  @Override
  public FindParameterConnection.Match newEmptyMatch() {
    return FindParameterConnection.Match.newEmptyMatch();
  }

  @Override
  public FindParameterConnection.Match newMatch(final Object... parameters) {
    return FindParameterConnection.Match.newMatch((org.osate.aadl2.ParameterConnection) parameters[0], (org.osate.aadl2.Feature) parameters[1]);
  }

  /**
   * Inner class allowing the singleton instance of {@link FindParameterConnection} to be created 
   *     <b>not</b> at the class load time of the outer class, 
   *     but rather at the first call to {@link FindParameterConnection#instance()}.
   * 
   * <p> This workaround is required e.g. to support recursion.
   * 
   */
  private static class LazyHolder {
    private static final FindParameterConnection INSTANCE = new FindParameterConnection();

    /**
     * Statically initializes the query specification <b>after</b> the field {@link #INSTANCE} is assigned.
     * This initialization order is required to support indirect recursion.
     * 
     * <p> The static initializer is defined using a helper field to work around limitations of the code generator.
     * 
     */
    private static final Object STATIC_INITIALIZER = ensureInitialized();

    public static Object ensureInitialized() {
      INSTANCE.ensureInitializedInternal();
      return null;
    }
  }

  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private static final FindParameterConnection.GeneratedPQuery INSTANCE = new GeneratedPQuery();

    private final PParameter parameter_parconn = new PParameter("parconn", "org.osate.aadl2.ParameterConnection", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0", "ParameterConnection")), PParameterDirection.INOUT);

    private final PParameter parameter_feat = new PParameter("feat", "org.osate.aadl2.Feature", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0", "Feature")), PParameterDirection.INOUT);

    private final List<PParameter> parameters = Arrays.asList(parameter_parconn, parameter_feat);

    private GeneratedPQuery() {
      super(PVisibility.PUBLIC);
    }

    @Override
    public String getFullyQualifiedName() {
      return "fr.mem4csd.osatedim.viatra.queries.findParameterConnection";
    }

    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("parconn","feat");
    }

    @Override
    public List<PParameter> getParameters() {
      return parameters;
    }

    @Override
    public Set<PBody> doGetContainedBodies() {
      setEvaluationHints(new QueryEvaluationHint(null, QueryEvaluationHint.BackendRequirement.UNSPECIFIED));
      Set<PBody> bodies = new LinkedHashSet<>();
      {
          PBody body = new PBody(this);
          PVariable var_parconn = body.getOrCreateVariableByName("parconn");
          PVariable var_feat = body.getOrCreateVariableByName("feat");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_feat), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Feature")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_parconn, parameter_parconn),
             new ExportedParameter(body, var_feat, parameter_feat)
          ));
          // 	AbstractImplementation.ownedParameterConnection(_,parconn)
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "AbstractImplementation")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ComponentImplementation", "ownedParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new Equality(body, var__virtual_0_, var_parconn);
          // 	ParameterConnection.source.connectionEnd(parconn, feat)
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "Connection", "source")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectedElement")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ConnectedElement", "connectionEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectionEnd")));
          new Equality(body, var__virtual_2_, var_feat);
          bodies.add(body);
      }
      {
          PBody body = new PBody(this);
          PVariable var_parconn = body.getOrCreateVariableByName("parconn");
          PVariable var_feat = body.getOrCreateVariableByName("feat");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_feat), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Feature")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_parconn, parameter_parconn),
             new ExportedParameter(body, var_feat, parameter_feat)
          ));
          // 	BusImplementation.ownedParameterConnection(_,parconn)
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "BusImplementation")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ComponentImplementation", "ownedParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new Equality(body, var__virtual_0_, var_parconn);
          //  	ParameterConnection.source.connectionEnd(parconn, feat)
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "Connection", "source")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectedElement")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ConnectedElement", "connectionEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectionEnd")));
          new Equality(body, var__virtual_2_, var_feat);
          bodies.add(body);
      }
      {
          PBody body = new PBody(this);
          PVariable var_parconn = body.getOrCreateVariableByName("parconn");
          PVariable var_feat = body.getOrCreateVariableByName("feat");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_feat), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Feature")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_parconn, parameter_parconn),
             new ExportedParameter(body, var_feat, parameter_feat)
          ));
          // 	DataImplementation.ownedParameterConnection(_,parconn)
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "DataImplementation")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ComponentImplementation", "ownedParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new Equality(body, var__virtual_0_, var_parconn);
          //  	ParameterConnection.source.connectionEnd(parconn, feat)
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "Connection", "source")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectedElement")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ConnectedElement", "connectionEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectionEnd")));
          new Equality(body, var__virtual_2_, var_feat);
          bodies.add(body);
      }
      {
          PBody body = new PBody(this);
          PVariable var_parconn = body.getOrCreateVariableByName("parconn");
          PVariable var_feat = body.getOrCreateVariableByName("feat");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_feat), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Feature")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_parconn, parameter_parconn),
             new ExportedParameter(body, var_feat, parameter_feat)
          ));
          // 	DeviceImplementation.ownedParameterConnection(_,parconn)
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "DeviceImplementation")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ComponentImplementation", "ownedParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new Equality(body, var__virtual_0_, var_parconn);
          // 	ParameterConnection.source.connectionEnd(parconn, feat)
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "Connection", "source")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectedElement")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ConnectedElement", "connectionEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectionEnd")));
          new Equality(body, var__virtual_2_, var_feat);
          bodies.add(body);
      }
      {
          PBody body = new PBody(this);
          PVariable var_parconn = body.getOrCreateVariableByName("parconn");
          PVariable var_feat = body.getOrCreateVariableByName("feat");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_feat), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Feature")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_parconn, parameter_parconn),
             new ExportedParameter(body, var_feat, parameter_feat)
          ));
          // 	MemoryImplementation.ownedParameterConnection(_,parconn)
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "MemoryImplementation")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ComponentImplementation", "ownedParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new Equality(body, var__virtual_0_, var_parconn);
          // 	ParameterConnection.source.connectionEnd(parconn, feat)
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "Connection", "source")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectedElement")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ConnectedElement", "connectionEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectionEnd")));
          new Equality(body, var__virtual_2_, var_feat);
          bodies.add(body);
      }
      {
          PBody body = new PBody(this);
          PVariable var_parconn = body.getOrCreateVariableByName("parconn");
          PVariable var_feat = body.getOrCreateVariableByName("feat");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_feat), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Feature")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_parconn, parameter_parconn),
             new ExportedParameter(body, var_feat, parameter_feat)
          ));
          // 	ProcessImplementation.ownedParameterConnection(_,parconn)
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ProcessImplementation")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ComponentImplementation", "ownedParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new Equality(body, var__virtual_0_, var_parconn);
          // 	ParameterConnection.source.connectionEnd(parconn, feat)
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "Connection", "source")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectedElement")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ConnectedElement", "connectionEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectionEnd")));
          new Equality(body, var__virtual_2_, var_feat);
          bodies.add(body);
      }
      {
          PBody body = new PBody(this);
          PVariable var_parconn = body.getOrCreateVariableByName("parconn");
          PVariable var_feat = body.getOrCreateVariableByName("feat");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_feat), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Feature")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_parconn, parameter_parconn),
             new ExportedParameter(body, var_feat, parameter_feat)
          ));
          // 	ProcessorImplementation.ownedParameterConnection(_,parconn)
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ProcessorImplementation")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ComponentImplementation", "ownedParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new Equality(body, var__virtual_0_, var_parconn);
          // 	ParameterConnection.source.connectionEnd(parconn, feat)
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "Connection", "source")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectedElement")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ConnectedElement", "connectionEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectionEnd")));
          new Equality(body, var__virtual_2_, var_feat);
          bodies.add(body);
      }
      {
          PBody body = new PBody(this);
          PVariable var_parconn = body.getOrCreateVariableByName("parconn");
          PVariable var_feat = body.getOrCreateVariableByName("feat");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_feat), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Feature")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_parconn, parameter_parconn),
             new ExportedParameter(body, var_feat, parameter_feat)
          ));
          // 	SubprogramImplementation.ownedParameterConnection(_,parconn)
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "SubprogramImplementation")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ComponentImplementation", "ownedParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new Equality(body, var__virtual_0_, var_parconn);
          // 	ParameterConnection.source.connectionEnd(parconn, feat)
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "Connection", "source")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectedElement")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ConnectedElement", "connectionEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectionEnd")));
          new Equality(body, var__virtual_2_, var_feat);
          bodies.add(body);
      }
      {
          PBody body = new PBody(this);
          PVariable var_parconn = body.getOrCreateVariableByName("parconn");
          PVariable var_feat = body.getOrCreateVariableByName("feat");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_feat), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Feature")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_parconn, parameter_parconn),
             new ExportedParameter(body, var_feat, parameter_feat)
          ));
          // 	SubprogramGroupImplementation.ownedParameterConnection(_,parconn)
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "SubprogramGroupImplementation")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ComponentImplementation", "ownedParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new Equality(body, var__virtual_0_, var_parconn);
          // 	ParameterConnection.source.connectionEnd(parconn, feat)
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "Connection", "source")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectedElement")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ConnectedElement", "connectionEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectionEnd")));
          new Equality(body, var__virtual_2_, var_feat);
          bodies.add(body);
      }
      {
          PBody body = new PBody(this);
          PVariable var_parconn = body.getOrCreateVariableByName("parconn");
          PVariable var_feat = body.getOrCreateVariableByName("feat");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_feat), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Feature")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_parconn, parameter_parconn),
             new ExportedParameter(body, var_feat, parameter_feat)
          ));
          // 	SystemImplementation.ownedParameterConnection(_,parconn)
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "SystemImplementation")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ComponentImplementation", "ownedParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new Equality(body, var__virtual_0_, var_parconn);
          // 	ParameterConnection.source.connectionEnd(parconn, feat)
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "Connection", "source")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectedElement")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ConnectedElement", "connectionEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectionEnd")));
          new Equality(body, var__virtual_2_, var_feat);
          bodies.add(body);
      }
      {
          PBody body = new PBody(this);
          PVariable var_parconn = body.getOrCreateVariableByName("parconn");
          PVariable var_feat = body.getOrCreateVariableByName("feat");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_feat), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Feature")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_parconn, parameter_parconn),
             new ExportedParameter(body, var_feat, parameter_feat)
          ));
          // 	ThreadImplementation.ownedParameterConnection(_,parconn)
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ThreadImplementation")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ComponentImplementation", "ownedParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new Equality(body, var__virtual_0_, var_parconn);
          // 	ParameterConnection.source.connectionEnd(parconn, feat)
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "Connection", "source")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectedElement")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ConnectedElement", "connectionEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectionEnd")));
          new Equality(body, var__virtual_2_, var_feat);
          bodies.add(body);
      }
      {
          PBody body = new PBody(this);
          PVariable var_parconn = body.getOrCreateVariableByName("parconn");
          PVariable var_feat = body.getOrCreateVariableByName("feat");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_feat), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Feature")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_parconn, parameter_parconn),
             new ExportedParameter(body, var_feat, parameter_feat)
          ));
          // 	ThreadGroupImplementation.ownedParameterConnection(_,parconn)
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ThreadGroupImplementation")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ComponentImplementation", "ownedParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new Equality(body, var__virtual_0_, var_parconn);
          // 	ParameterConnection.source.connectionEnd(parconn, feat)
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "Connection", "source")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectedElement")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ConnectedElement", "connectionEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectionEnd")));
          new Equality(body, var__virtual_2_, var_feat);
          bodies.add(body);
      }
      {
          PBody body = new PBody(this);
          PVariable var_parconn = body.getOrCreateVariableByName("parconn");
          PVariable var_feat = body.getOrCreateVariableByName("feat");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_feat), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Feature")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_parconn, parameter_parconn),
             new ExportedParameter(body, var_feat, parameter_feat)
          ));
          // 	VirtualBusImplementation.ownedParameterConnection(_,parconn)
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "VirtualBusImplementation")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ComponentImplementation", "ownedParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new Equality(body, var__virtual_0_, var_parconn);
          // 	ParameterConnection.source.connectionEnd(parconn, feat)
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "Connection", "source")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectedElement")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ConnectedElement", "connectionEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectionEnd")));
          new Equality(body, var__virtual_2_, var_feat);
          bodies.add(body);
      }
      {
          PBody body = new PBody(this);
          PVariable var_parconn = body.getOrCreateVariableByName("parconn");
          PVariable var_feat = body.getOrCreateVariableByName("feat");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_feat), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Feature")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_parconn, parameter_parconn),
             new ExportedParameter(body, var_feat, parameter_feat)
          ));
          // 	VirtualProcessorImplementation.ownedParameterConnection(_,parconn)
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "VirtualProcessorImplementation")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ComponentImplementation", "ownedParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new Equality(body, var__virtual_0_, var_parconn);
          // 	ParameterConnection.source.connectionEnd(parconn, feat)
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "Connection", "source")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectedElement")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ConnectedElement", "connectionEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectionEnd")));
          new Equality(body, var__virtual_2_, var_feat);
          bodies.add(body);
      }
      {
          PBody body = new PBody(this);
          PVariable var_parconn = body.getOrCreateVariableByName("parconn");
          PVariable var_feat = body.getOrCreateVariableByName("feat");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_feat), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Feature")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_parconn, parameter_parconn),
             new ExportedParameter(body, var_feat, parameter_feat)
          ));
          // 	AbstractImplementation.ownedParameterConnection(_,parconn)
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "AbstractImplementation")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ComponentImplementation", "ownedParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new Equality(body, var__virtual_0_, var_parconn);
          // 	ParameterConnection.destination.connectionEnd(parconn, feat)
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "Connection", "destination")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectedElement")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ConnectedElement", "connectionEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectionEnd")));
          new Equality(body, var__virtual_2_, var_feat);
          bodies.add(body);
      }
      {
          PBody body = new PBody(this);
          PVariable var_parconn = body.getOrCreateVariableByName("parconn");
          PVariable var_feat = body.getOrCreateVariableByName("feat");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_feat), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Feature")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_parconn, parameter_parconn),
             new ExportedParameter(body, var_feat, parameter_feat)
          ));
          // 	BusImplementation.ownedParameterConnection(_,parconn)
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "BusImplementation")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ComponentImplementation", "ownedParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new Equality(body, var__virtual_0_, var_parconn);
          //  	ParameterConnection.destination.connectionEnd(parconn, feat)
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "Connection", "destination")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectedElement")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ConnectedElement", "connectionEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectionEnd")));
          new Equality(body, var__virtual_2_, var_feat);
          bodies.add(body);
      }
      {
          PBody body = new PBody(this);
          PVariable var_parconn = body.getOrCreateVariableByName("parconn");
          PVariable var_feat = body.getOrCreateVariableByName("feat");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_feat), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Feature")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_parconn, parameter_parconn),
             new ExportedParameter(body, var_feat, parameter_feat)
          ));
          // 	DataImplementation.ownedParameterConnection(_,parconn)
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "DataImplementation")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ComponentImplementation", "ownedParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new Equality(body, var__virtual_0_, var_parconn);
          //  	ParameterConnection.destination.connectionEnd(parconn, feat)
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "Connection", "destination")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectedElement")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ConnectedElement", "connectionEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectionEnd")));
          new Equality(body, var__virtual_2_, var_feat);
          bodies.add(body);
      }
      {
          PBody body = new PBody(this);
          PVariable var_parconn = body.getOrCreateVariableByName("parconn");
          PVariable var_feat = body.getOrCreateVariableByName("feat");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_feat), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Feature")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_parconn, parameter_parconn),
             new ExportedParameter(body, var_feat, parameter_feat)
          ));
          // 	DeviceImplementation.ownedParameterConnection(_,parconn)
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "DeviceImplementation")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ComponentImplementation", "ownedParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new Equality(body, var__virtual_0_, var_parconn);
          // 	ParameterConnection.destination.connectionEnd(parconn, feat)
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "Connection", "destination")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectedElement")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ConnectedElement", "connectionEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectionEnd")));
          new Equality(body, var__virtual_2_, var_feat);
          bodies.add(body);
      }
      {
          PBody body = new PBody(this);
          PVariable var_parconn = body.getOrCreateVariableByName("parconn");
          PVariable var_feat = body.getOrCreateVariableByName("feat");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_feat), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Feature")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_parconn, parameter_parconn),
             new ExportedParameter(body, var_feat, parameter_feat)
          ));
          // 	MemoryImplementation.ownedParameterConnection(_,parconn)
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "MemoryImplementation")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ComponentImplementation", "ownedParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new Equality(body, var__virtual_0_, var_parconn);
          // 	ParameterConnection.destination.connectionEnd(parconn, feat)
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "Connection", "destination")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectedElement")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ConnectedElement", "connectionEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectionEnd")));
          new Equality(body, var__virtual_2_, var_feat);
          bodies.add(body);
      }
      {
          PBody body = new PBody(this);
          PVariable var_parconn = body.getOrCreateVariableByName("parconn");
          PVariable var_feat = body.getOrCreateVariableByName("feat");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_feat), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Feature")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_parconn, parameter_parconn),
             new ExportedParameter(body, var_feat, parameter_feat)
          ));
          // 	ProcessImplementation.ownedParameterConnection(_,parconn)
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ProcessImplementation")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ComponentImplementation", "ownedParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new Equality(body, var__virtual_0_, var_parconn);
          // 	ParameterConnection.destination.connectionEnd(parconn, feat)
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "Connection", "destination")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectedElement")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ConnectedElement", "connectionEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectionEnd")));
          new Equality(body, var__virtual_2_, var_feat);
          bodies.add(body);
      }
      {
          PBody body = new PBody(this);
          PVariable var_parconn = body.getOrCreateVariableByName("parconn");
          PVariable var_feat = body.getOrCreateVariableByName("feat");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_feat), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Feature")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_parconn, parameter_parconn),
             new ExportedParameter(body, var_feat, parameter_feat)
          ));
          // 	ProcessorImplementation.ownedParameterConnection(_,parconn)
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ProcessorImplementation")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ComponentImplementation", "ownedParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new Equality(body, var__virtual_0_, var_parconn);
          // 	ParameterConnection.destination.connectionEnd(parconn, feat)
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "Connection", "destination")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectedElement")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ConnectedElement", "connectionEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectionEnd")));
          new Equality(body, var__virtual_2_, var_feat);
          bodies.add(body);
      }
      {
          PBody body = new PBody(this);
          PVariable var_parconn = body.getOrCreateVariableByName("parconn");
          PVariable var_feat = body.getOrCreateVariableByName("feat");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_feat), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Feature")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_parconn, parameter_parconn),
             new ExportedParameter(body, var_feat, parameter_feat)
          ));
          // 	SubprogramImplementation.ownedParameterConnection(_,parconn)
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "SubprogramImplementation")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ComponentImplementation", "ownedParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new Equality(body, var__virtual_0_, var_parconn);
          // 	ParameterConnection.destination.connectionEnd(parconn, feat)
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "Connection", "destination")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectedElement")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ConnectedElement", "connectionEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectionEnd")));
          new Equality(body, var__virtual_2_, var_feat);
          bodies.add(body);
      }
      {
          PBody body = new PBody(this);
          PVariable var_parconn = body.getOrCreateVariableByName("parconn");
          PVariable var_feat = body.getOrCreateVariableByName("feat");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_feat), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Feature")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_parconn, parameter_parconn),
             new ExportedParameter(body, var_feat, parameter_feat)
          ));
          // 	SubprogramGroupImplementation.ownedParameterConnection(_,parconn)
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "SubprogramGroupImplementation")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ComponentImplementation", "ownedParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new Equality(body, var__virtual_0_, var_parconn);
          // 	ParameterConnection.destination.connectionEnd(parconn, feat)
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "Connection", "destination")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectedElement")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ConnectedElement", "connectionEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectionEnd")));
          new Equality(body, var__virtual_2_, var_feat);
          bodies.add(body);
      }
      {
          PBody body = new PBody(this);
          PVariable var_parconn = body.getOrCreateVariableByName("parconn");
          PVariable var_feat = body.getOrCreateVariableByName("feat");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_feat), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Feature")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_parconn, parameter_parconn),
             new ExportedParameter(body, var_feat, parameter_feat)
          ));
          // 	SystemImplementation.ownedParameterConnection(_,parconn)
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "SystemImplementation")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ComponentImplementation", "ownedParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new Equality(body, var__virtual_0_, var_parconn);
          // 	ParameterConnection.destination.connectionEnd(parconn, feat)
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "Connection", "destination")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectedElement")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ConnectedElement", "connectionEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectionEnd")));
          new Equality(body, var__virtual_2_, var_feat);
          bodies.add(body);
      }
      {
          PBody body = new PBody(this);
          PVariable var_parconn = body.getOrCreateVariableByName("parconn");
          PVariable var_feat = body.getOrCreateVariableByName("feat");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_feat), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Feature")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_parconn, parameter_parconn),
             new ExportedParameter(body, var_feat, parameter_feat)
          ));
          // 	ThreadImplementation.ownedParameterConnection(_,parconn)
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ThreadImplementation")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ComponentImplementation", "ownedParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new Equality(body, var__virtual_0_, var_parconn);
          // 	ParameterConnection.destination.connectionEnd(parconn, feat)
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "Connection", "destination")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectedElement")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ConnectedElement", "connectionEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectionEnd")));
          new Equality(body, var__virtual_2_, var_feat);
          bodies.add(body);
      }
      {
          PBody body = new PBody(this);
          PVariable var_parconn = body.getOrCreateVariableByName("parconn");
          PVariable var_feat = body.getOrCreateVariableByName("feat");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_feat), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Feature")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_parconn, parameter_parconn),
             new ExportedParameter(body, var_feat, parameter_feat)
          ));
          // 	ThreadGroupImplementation.ownedParameterConnection(_,parconn)
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ThreadGroupImplementation")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ComponentImplementation", "ownedParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new Equality(body, var__virtual_0_, var_parconn);
          // 	ParameterConnection.destination.connectionEnd(parconn, feat)
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "Connection", "destination")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectedElement")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ConnectedElement", "connectionEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectionEnd")));
          new Equality(body, var__virtual_2_, var_feat);
          bodies.add(body);
      }
      {
          PBody body = new PBody(this);
          PVariable var_parconn = body.getOrCreateVariableByName("parconn");
          PVariable var_feat = body.getOrCreateVariableByName("feat");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_feat), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Feature")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_parconn, parameter_parconn),
             new ExportedParameter(body, var_feat, parameter_feat)
          ));
          // 	VirtualBusImplementation.ownedParameterConnection(_,parconn)
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "VirtualBusImplementation")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ComponentImplementation", "ownedParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new Equality(body, var__virtual_0_, var_parconn);
          // 	ParameterConnection.destination.connectionEnd(parconn, feat)
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "Connection", "destination")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectedElement")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ConnectedElement", "connectionEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectionEnd")));
          new Equality(body, var__virtual_2_, var_feat);
          bodies.add(body);
      }
      {
          PBody body = new PBody(this);
          PVariable var_parconn = body.getOrCreateVariableByName("parconn");
          PVariable var_feat = body.getOrCreateVariableByName("feat");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_feat), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Feature")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_parconn, parameter_parconn),
             new ExportedParameter(body, var_feat, parameter_feat)
          ));
          // 	VirtualProcessorImplementation.ownedParameterConnection(_,parconn)
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "VirtualProcessorImplementation")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ComponentImplementation", "ownedParameterConnection")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          new Equality(body, var__virtual_0_, var_parconn);
          // 	ParameterConnection.destination.connectionEnd(parconn, feat)
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ParameterConnection")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_parconn, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "Connection", "destination")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectedElement")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ConnectedElement", "connectionEnd")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ConnectionEnd")));
          new Equality(body, var__virtual_2_, var_feat);
          bodies.add(body);
      }
      return bodies;
    }
  }
}
