/**
 * Generated from platform:/resource/fr.mem4csd.osatedim/src/fr/mem4csd/osatedim/viatra/queries/DIMQueriesOutplace.vql
 */
package fr.mem4csd.osatedim.viatra.queries;

import fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.viatra.query.runtime.api.IPatternMatch;
import org.eclipse.viatra.query.runtime.api.IQuerySpecification;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.viatra.query.runtime.api.impl.BaseMatcher;
import org.eclipse.viatra.query.runtime.api.impl.BasePatternMatch;
import org.eclipse.viatra.query.runtime.emf.types.EClassTransitiveInstancesKey;
import org.eclipse.viatra.query.runtime.emf.types.EStructuralFeatureInstancesKey;
import org.eclipse.viatra.query.runtime.matchers.backend.QueryEvaluationHint;
import org.eclipse.viatra.query.runtime.matchers.psystem.PBody;
import org.eclipse.viatra.query.runtime.matchers.psystem.PVariable;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.Equality;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.PositivePatternCall;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.TypeConstraint;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameterDirection;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PVisibility;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuple;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuples;
import org.eclipse.viatra.query.runtime.util.ViatraQueryLoggingUtil;
import org.osate.aadl2.ModalPropertyValue;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.FeatureInstance;
import org.osate.aadl2.instance.InstanceReferenceValue;
import org.osate.aadl2.instance.PropertyAssociationInstance;

/**
 * A pattern-specific query specification that can instantiate Matcher in a type-safe way.
 * 
 * <p>Original source:
 *         <code><pre>
 *         // This pattern finds the reference-based properties2Attach for a specific feature instance
 *         pattern find_PropertyRefAttach2Feature(compinst : ComponentInstance,
 *         							           featureinst : FeatureInstance,
 *         							           trace : Aaxl2AaxlTrace,
 *         							           propinst : PropertyAssociationInstance,
 *         							           propvalue : InstanceReferenceValue,
 *         									   modalprop : ModalPropertyValue)
 *         {
 *         	ComponentInstance.featureInstance(compinst, featureinst);
 *         	find is_in_trace(_,trace,featureinst,_);
 *         	Aaxl2AaxlTrace.objectsToAttach(trace, propinst);
 *         	PropertyAssociationInstance.ownedValue(propinst,modalprop);
 *         	ModalPropertyValue.ownedValue(modalprop,propvalue);
 *         }
 * </pre></code>
 * 
 * @see Matcher
 * @see Match
 * 
 */
@SuppressWarnings("all")
public final class Find_PropertyRefAttach2Feature extends BaseGeneratedEMFQuerySpecification<Find_PropertyRefAttach2Feature.Matcher> {
  /**
   * Pattern-specific match representation of the fr.mem4csd.osatedim.viatra.queries.find_PropertyRefAttach2Feature pattern,
   * to be used in conjunction with {@link Matcher}.
   * 
   * <p>Class fields correspond to parameters of the pattern. Fields with value null are considered unassigned.
   * Each instance is a (possibly partial) substitution of pattern parameters,
   * usable to represent a match of the pattern in the result of a query,
   * or to specify the bound (fixed) input parameters when issuing a query.
   * 
   * @see Matcher
   * 
   */
  public static abstract class Match extends BasePatternMatch {
    private ComponentInstance fCompinst;

    private FeatureInstance fFeatureinst;

    private Aaxl2AaxlTrace fTrace;

    private PropertyAssociationInstance fPropinst;

    private InstanceReferenceValue fPropvalue;

    private ModalPropertyValue fModalprop;

    private static List<String> parameterNames = makeImmutableList("compinst", "featureinst", "trace", "propinst", "propvalue", "modalprop");

    private Match(final ComponentInstance pCompinst, final FeatureInstance pFeatureinst, final Aaxl2AaxlTrace pTrace, final PropertyAssociationInstance pPropinst, final InstanceReferenceValue pPropvalue, final ModalPropertyValue pModalprop) {
      this.fCompinst = pCompinst;
      this.fFeatureinst = pFeatureinst;
      this.fTrace = pTrace;
      this.fPropinst = pPropinst;
      this.fPropvalue = pPropvalue;
      this.fModalprop = pModalprop;
    }

    @Override
    public Object get(final String parameterName) {
      switch(parameterName) {
          case "compinst": return this.fCompinst;
          case "featureinst": return this.fFeatureinst;
          case "trace": return this.fTrace;
          case "propinst": return this.fPropinst;
          case "propvalue": return this.fPropvalue;
          case "modalprop": return this.fModalprop;
          default: return null;
      }
    }

    @Override
    public Object get(final int index) {
      switch(index) {
          case 0: return this.fCompinst;
          case 1: return this.fFeatureinst;
          case 2: return this.fTrace;
          case 3: return this.fPropinst;
          case 4: return this.fPropvalue;
          case 5: return this.fModalprop;
          default: return null;
      }
    }

    public ComponentInstance getCompinst() {
      return this.fCompinst;
    }

    public FeatureInstance getFeatureinst() {
      return this.fFeatureinst;
    }

    public Aaxl2AaxlTrace getTrace() {
      return this.fTrace;
    }

    public PropertyAssociationInstance getPropinst() {
      return this.fPropinst;
    }

    public InstanceReferenceValue getPropvalue() {
      return this.fPropvalue;
    }

    public ModalPropertyValue getModalprop() {
      return this.fModalprop;
    }

    @Override
    public boolean set(final String parameterName, final Object newValue) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      if ("compinst".equals(parameterName) ) {
          this.fCompinst = (ComponentInstance) newValue;
          return true;
      }
      if ("featureinst".equals(parameterName) ) {
          this.fFeatureinst = (FeatureInstance) newValue;
          return true;
      }
      if ("trace".equals(parameterName) ) {
          this.fTrace = (Aaxl2AaxlTrace) newValue;
          return true;
      }
      if ("propinst".equals(parameterName) ) {
          this.fPropinst = (PropertyAssociationInstance) newValue;
          return true;
      }
      if ("propvalue".equals(parameterName) ) {
          this.fPropvalue = (InstanceReferenceValue) newValue;
          return true;
      }
      if ("modalprop".equals(parameterName) ) {
          this.fModalprop = (ModalPropertyValue) newValue;
          return true;
      }
      return false;
    }

    public void setCompinst(final ComponentInstance pCompinst) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fCompinst = pCompinst;
    }

    public void setFeatureinst(final FeatureInstance pFeatureinst) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fFeatureinst = pFeatureinst;
    }

    public void setTrace(final Aaxl2AaxlTrace pTrace) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fTrace = pTrace;
    }

    public void setPropinst(final PropertyAssociationInstance pPropinst) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fPropinst = pPropinst;
    }

    public void setPropvalue(final InstanceReferenceValue pPropvalue) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fPropvalue = pPropvalue;
    }

    public void setModalprop(final ModalPropertyValue pModalprop) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fModalprop = pModalprop;
    }

    @Override
    public String patternName() {
      return "fr.mem4csd.osatedim.viatra.queries.find_PropertyRefAttach2Feature";
    }

    @Override
    public List<String> parameterNames() {
      return Find_PropertyRefAttach2Feature.Match.parameterNames;
    }

    @Override
    public Object[] toArray() {
      return new Object[]{fCompinst, fFeatureinst, fTrace, fPropinst, fPropvalue, fModalprop};
    }

    @Override
    public Find_PropertyRefAttach2Feature.Match toImmutable() {
      return isMutable() ? newMatch(fCompinst, fFeatureinst, fTrace, fPropinst, fPropvalue, fModalprop) : this;
    }

    @Override
    public String prettyPrint() {
      StringBuilder result = new StringBuilder();
      result.append("\"compinst\"=" + prettyPrintValue(fCompinst) + ", ");
      result.append("\"featureinst\"=" + prettyPrintValue(fFeatureinst) + ", ");
      result.append("\"trace\"=" + prettyPrintValue(fTrace) + ", ");
      result.append("\"propinst\"=" + prettyPrintValue(fPropinst) + ", ");
      result.append("\"propvalue\"=" + prettyPrintValue(fPropvalue) + ", ");
      result.append("\"modalprop\"=" + prettyPrintValue(fModalprop));
      return result.toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(fCompinst, fFeatureinst, fTrace, fPropinst, fPropvalue, fModalprop);
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj)
          return true;
      if (obj == null) {
          return false;
      }
      if ((obj instanceof Find_PropertyRefAttach2Feature.Match)) {
          Find_PropertyRefAttach2Feature.Match other = (Find_PropertyRefAttach2Feature.Match) obj;
          return Objects.equals(fCompinst, other.fCompinst) && Objects.equals(fFeatureinst, other.fFeatureinst) && Objects.equals(fTrace, other.fTrace) && Objects.equals(fPropinst, other.fPropinst) && Objects.equals(fPropvalue, other.fPropvalue) && Objects.equals(fModalprop, other.fModalprop);
      } else {
          // this should be infrequent
          if (!(obj instanceof IPatternMatch)) {
              return false;
          }
          IPatternMatch otherSig  = (IPatternMatch) obj;
          return Objects.equals(specification(), otherSig.specification()) && Arrays.deepEquals(toArray(), otherSig.toArray());
      }
    }

    @Override
    public Find_PropertyRefAttach2Feature specification() {
      return Find_PropertyRefAttach2Feature.instance();
    }

    /**
     * Returns an empty, mutable match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @return the empty match.
     * 
     */
    public static Find_PropertyRefAttach2Feature.Match newEmptyMatch() {
      return new Mutable(null, null, null, null, null, null);
    }

    /**
     * Returns a mutable (partial) match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pFeatureinst the fixed value of pattern parameter featureinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @param pPropvalue the fixed value of pattern parameter propvalue, or null if not bound.
     * @param pModalprop the fixed value of pattern parameter modalprop, or null if not bound.
     * @return the new, mutable (partial) match object.
     * 
     */
    public static Find_PropertyRefAttach2Feature.Match newMutableMatch(final ComponentInstance pCompinst, final FeatureInstance pFeatureinst, final Aaxl2AaxlTrace pTrace, final PropertyAssociationInstance pPropinst, final InstanceReferenceValue pPropvalue, final ModalPropertyValue pModalprop) {
      return new Mutable(pCompinst, pFeatureinst, pTrace, pPropinst, pPropvalue, pModalprop);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pFeatureinst the fixed value of pattern parameter featureinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @param pPropvalue the fixed value of pattern parameter propvalue, or null if not bound.
     * @param pModalprop the fixed value of pattern parameter modalprop, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public static Find_PropertyRefAttach2Feature.Match newMatch(final ComponentInstance pCompinst, final FeatureInstance pFeatureinst, final Aaxl2AaxlTrace pTrace, final PropertyAssociationInstance pPropinst, final InstanceReferenceValue pPropvalue, final ModalPropertyValue pModalprop) {
      return new Immutable(pCompinst, pFeatureinst, pTrace, pPropinst, pPropvalue, pModalprop);
    }

    private static final class Mutable extends Find_PropertyRefAttach2Feature.Match {
      Mutable(final ComponentInstance pCompinst, final FeatureInstance pFeatureinst, final Aaxl2AaxlTrace pTrace, final PropertyAssociationInstance pPropinst, final InstanceReferenceValue pPropvalue, final ModalPropertyValue pModalprop) {
        super(pCompinst, pFeatureinst, pTrace, pPropinst, pPropvalue, pModalprop);
      }

      @Override
      public boolean isMutable() {
        return true;
      }
    }

    private static final class Immutable extends Find_PropertyRefAttach2Feature.Match {
      Immutable(final ComponentInstance pCompinst, final FeatureInstance pFeatureinst, final Aaxl2AaxlTrace pTrace, final PropertyAssociationInstance pPropinst, final InstanceReferenceValue pPropvalue, final ModalPropertyValue pModalprop) {
        super(pCompinst, pFeatureinst, pTrace, pPropinst, pPropvalue, pModalprop);
      }

      @Override
      public boolean isMutable() {
        return false;
      }
    }
  }

  /**
   * Generated pattern matcher API of the fr.mem4csd.osatedim.viatra.queries.find_PropertyRefAttach2Feature pattern,
   * providing pattern-specific query methods.
   * 
   * <p>Use the pattern matcher on a given model via {@link #on(ViatraQueryEngine)},
   * e.g. in conjunction with {@link ViatraQueryEngine#on(QueryScope)}.
   * 
   * <p>Matches of the pattern will be represented as {@link Match}.
   * 
   * <p>Original source:
   * <code><pre>
   * // This pattern finds the reference-based properties2Attach for a specific feature instance
   * pattern find_PropertyRefAttach2Feature(compinst : ComponentInstance,
   * 							           featureinst : FeatureInstance,
   * 							           trace : Aaxl2AaxlTrace,
   * 							           propinst : PropertyAssociationInstance,
   * 							           propvalue : InstanceReferenceValue,
   * 									   modalprop : ModalPropertyValue)
   * {
   * 	ComponentInstance.featureInstance(compinst, featureinst);
   * 	find is_in_trace(_,trace,featureinst,_);
   * 	Aaxl2AaxlTrace.objectsToAttach(trace, propinst);
   * 	PropertyAssociationInstance.ownedValue(propinst,modalprop);
   * 	ModalPropertyValue.ownedValue(modalprop,propvalue);
   * }
   * </pre></code>
   * 
   * @see Match
   * @see Find_PropertyRefAttach2Feature
   * 
   */
  public static class Matcher extends BaseMatcher<Find_PropertyRefAttach2Feature.Match> {
    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    public static Find_PropertyRefAttach2Feature.Matcher on(final ViatraQueryEngine engine) {
      // check if matcher already exists
      Matcher matcher = engine.getExistingMatcher(querySpecification());
      if (matcher == null) {
          matcher = (Matcher)engine.getMatcher(querySpecification());
      }
      return matcher;
    }

    /**
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * @return an initialized matcher
     * @noreference This method is for internal matcher initialization by the framework, do not call it manually.
     * 
     */
    public static Find_PropertyRefAttach2Feature.Matcher create() {
      return new Matcher();
    }

    private static final int POSITION_COMPINST = 0;

    private static final int POSITION_FEATUREINST = 1;

    private static final int POSITION_TRACE = 2;

    private static final int POSITION_PROPINST = 3;

    private static final int POSITION_PROPVALUE = 4;

    private static final int POSITION_MODALPROP = 5;

    private static final Logger LOGGER = ViatraQueryLoggingUtil.getLogger(Find_PropertyRefAttach2Feature.Matcher.class);

    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    private Matcher() {
      super(querySpecification());
    }

    /**
     * Returns the set of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pFeatureinst the fixed value of pattern parameter featureinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @param pPropvalue the fixed value of pattern parameter propvalue, or null if not bound.
     * @param pModalprop the fixed value of pattern parameter modalprop, or null if not bound.
     * @return matches represented as a Match object.
     * 
     */
    public Collection<Find_PropertyRefAttach2Feature.Match> getAllMatches(final ComponentInstance pCompinst, final FeatureInstance pFeatureinst, final Aaxl2AaxlTrace pTrace, final PropertyAssociationInstance pPropinst, final InstanceReferenceValue pPropvalue, final ModalPropertyValue pModalprop) {
      return rawStreamAllMatches(new Object[]{pCompinst, pFeatureinst, pTrace, pPropinst, pPropvalue, pModalprop}).collect(Collectors.toSet());
    }

    /**
     * Returns a stream of all matches of the pattern that conform to the given fixed values of some parameters.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pFeatureinst the fixed value of pattern parameter featureinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @param pPropvalue the fixed value of pattern parameter propvalue, or null if not bound.
     * @param pModalprop the fixed value of pattern parameter modalprop, or null if not bound.
     * @return a stream of matches represented as a Match object.
     * 
     */
    public Stream<Find_PropertyRefAttach2Feature.Match> streamAllMatches(final ComponentInstance pCompinst, final FeatureInstance pFeatureinst, final Aaxl2AaxlTrace pTrace, final PropertyAssociationInstance pPropinst, final InstanceReferenceValue pPropvalue, final ModalPropertyValue pModalprop) {
      return rawStreamAllMatches(new Object[]{pCompinst, pFeatureinst, pTrace, pPropinst, pPropvalue, pModalprop});
    }

    /**
     * Returns an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pFeatureinst the fixed value of pattern parameter featureinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @param pPropvalue the fixed value of pattern parameter propvalue, or null if not bound.
     * @param pModalprop the fixed value of pattern parameter modalprop, or null if not bound.
     * @return a match represented as a Match object, or null if no match is found.
     * 
     */
    public Optional<Find_PropertyRefAttach2Feature.Match> getOneArbitraryMatch(final ComponentInstance pCompinst, final FeatureInstance pFeatureinst, final Aaxl2AaxlTrace pTrace, final PropertyAssociationInstance pPropinst, final InstanceReferenceValue pPropvalue, final ModalPropertyValue pModalprop) {
      return rawGetOneArbitraryMatch(new Object[]{pCompinst, pFeatureinst, pTrace, pPropinst, pPropvalue, pModalprop});
    }

    /**
     * Indicates whether the given combination of specified pattern parameters constitute a valid pattern match,
     * under any possible substitution of the unspecified parameters (if any).
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pFeatureinst the fixed value of pattern parameter featureinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @param pPropvalue the fixed value of pattern parameter propvalue, or null if not bound.
     * @param pModalprop the fixed value of pattern parameter modalprop, or null if not bound.
     * @return true if the input is a valid (partial) match of the pattern.
     * 
     */
    public boolean hasMatch(final ComponentInstance pCompinst, final FeatureInstance pFeatureinst, final Aaxl2AaxlTrace pTrace, final PropertyAssociationInstance pPropinst, final InstanceReferenceValue pPropvalue, final ModalPropertyValue pModalprop) {
      return rawHasMatch(new Object[]{pCompinst, pFeatureinst, pTrace, pPropinst, pPropvalue, pModalprop});
    }

    /**
     * Returns the number of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pFeatureinst the fixed value of pattern parameter featureinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @param pPropvalue the fixed value of pattern parameter propvalue, or null if not bound.
     * @param pModalprop the fixed value of pattern parameter modalprop, or null if not bound.
     * @return the number of pattern matches found.
     * 
     */
    public int countMatches(final ComponentInstance pCompinst, final FeatureInstance pFeatureinst, final Aaxl2AaxlTrace pTrace, final PropertyAssociationInstance pPropinst, final InstanceReferenceValue pPropvalue, final ModalPropertyValue pModalprop) {
      return rawCountMatches(new Object[]{pCompinst, pFeatureinst, pTrace, pPropinst, pPropvalue, pModalprop});
    }

    /**
     * Executes the given processor on an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pFeatureinst the fixed value of pattern parameter featureinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @param pPropvalue the fixed value of pattern parameter propvalue, or null if not bound.
     * @param pModalprop the fixed value of pattern parameter modalprop, or null if not bound.
     * @param processor the action that will process the selected match.
     * @return true if the pattern has at least one match with the given parameter values, false if the processor was not invoked
     * 
     */
    public boolean forOneArbitraryMatch(final ComponentInstance pCompinst, final FeatureInstance pFeatureinst, final Aaxl2AaxlTrace pTrace, final PropertyAssociationInstance pPropinst, final InstanceReferenceValue pPropvalue, final ModalPropertyValue pModalprop, final Consumer<? super Find_PropertyRefAttach2Feature.Match> processor) {
      return rawForOneArbitraryMatch(new Object[]{pCompinst, pFeatureinst, pTrace, pPropinst, pPropvalue, pModalprop}, processor);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pFeatureinst the fixed value of pattern parameter featureinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @param pPropvalue the fixed value of pattern parameter propvalue, or null if not bound.
     * @param pModalprop the fixed value of pattern parameter modalprop, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public Find_PropertyRefAttach2Feature.Match newMatch(final ComponentInstance pCompinst, final FeatureInstance pFeatureinst, final Aaxl2AaxlTrace pTrace, final PropertyAssociationInstance pPropinst, final InstanceReferenceValue pPropvalue, final ModalPropertyValue pModalprop) {
      return Find_PropertyRefAttach2Feature.Match.newMatch(pCompinst, pFeatureinst, pTrace, pPropinst, pPropvalue, pModalprop);
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ComponentInstance> rawStreamAllValuesOfcompinst(final Object[] parameters) {
      return rawStreamAllValues(POSITION_COMPINST, parameters).map(ComponentInstance.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst() {
      return rawStreamAllValuesOfcompinst(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst() {
      return rawStreamAllValuesOfcompinst(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst(final Find_PropertyRefAttach2Feature.Match partialMatch) {
      return rawStreamAllValuesOfcompinst(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst(final FeatureInstance pFeatureinst, final Aaxl2AaxlTrace pTrace, final PropertyAssociationInstance pPropinst, final InstanceReferenceValue pPropvalue, final ModalPropertyValue pModalprop) {
      return rawStreamAllValuesOfcompinst(new Object[]{null, pFeatureinst, pTrace, pPropinst, pPropvalue, pModalprop});
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst(final Find_PropertyRefAttach2Feature.Match partialMatch) {
      return rawStreamAllValuesOfcompinst(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst(final FeatureInstance pFeatureinst, final Aaxl2AaxlTrace pTrace, final PropertyAssociationInstance pPropinst, final InstanceReferenceValue pPropvalue, final ModalPropertyValue pModalprop) {
      return rawStreamAllValuesOfcompinst(new Object[]{null, pFeatureinst, pTrace, pPropinst, pPropvalue, pModalprop}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for featureinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<FeatureInstance> rawStreamAllValuesOffeatureinst(final Object[] parameters) {
      return rawStreamAllValues(POSITION_FEATUREINST, parameters).map(FeatureInstance.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for featureinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<FeatureInstance> getAllValuesOffeatureinst() {
      return rawStreamAllValuesOffeatureinst(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for featureinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<FeatureInstance> streamAllValuesOffeatureinst() {
      return rawStreamAllValuesOffeatureinst(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for featureinst.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<FeatureInstance> streamAllValuesOffeatureinst(final Find_PropertyRefAttach2Feature.Match partialMatch) {
      return rawStreamAllValuesOffeatureinst(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for featureinst.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<FeatureInstance> streamAllValuesOffeatureinst(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final PropertyAssociationInstance pPropinst, final InstanceReferenceValue pPropvalue, final ModalPropertyValue pModalprop) {
      return rawStreamAllValuesOffeatureinst(new Object[]{pCompinst, null, pTrace, pPropinst, pPropvalue, pModalprop});
    }

    /**
     * Retrieve the set of values that occur in matches for featureinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<FeatureInstance> getAllValuesOffeatureinst(final Find_PropertyRefAttach2Feature.Match partialMatch) {
      return rawStreamAllValuesOffeatureinst(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for featureinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<FeatureInstance> getAllValuesOffeatureinst(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final PropertyAssociationInstance pPropinst, final InstanceReferenceValue pPropvalue, final ModalPropertyValue pModalprop) {
      return rawStreamAllValuesOffeatureinst(new Object[]{pCompinst, null, pTrace, pPropinst, pPropvalue, pModalprop}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<Aaxl2AaxlTrace> rawStreamAllValuesOftrace(final Object[] parameters) {
      return rawStreamAllValues(POSITION_TRACE, parameters).map(Aaxl2AaxlTrace.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Aaxl2AaxlTrace> getAllValuesOftrace() {
      return rawStreamAllValuesOftrace(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<Aaxl2AaxlTrace> streamAllValuesOftrace() {
      return rawStreamAllValuesOftrace(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<Aaxl2AaxlTrace> streamAllValuesOftrace(final Find_PropertyRefAttach2Feature.Match partialMatch) {
      return rawStreamAllValuesOftrace(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<Aaxl2AaxlTrace> streamAllValuesOftrace(final ComponentInstance pCompinst, final FeatureInstance pFeatureinst, final PropertyAssociationInstance pPropinst, final InstanceReferenceValue pPropvalue, final ModalPropertyValue pModalprop) {
      return rawStreamAllValuesOftrace(new Object[]{pCompinst, pFeatureinst, null, pPropinst, pPropvalue, pModalprop});
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Aaxl2AaxlTrace> getAllValuesOftrace(final Find_PropertyRefAttach2Feature.Match partialMatch) {
      return rawStreamAllValuesOftrace(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Aaxl2AaxlTrace> getAllValuesOftrace(final ComponentInstance pCompinst, final FeatureInstance pFeatureinst, final PropertyAssociationInstance pPropinst, final InstanceReferenceValue pPropvalue, final ModalPropertyValue pModalprop) {
      return rawStreamAllValuesOftrace(new Object[]{pCompinst, pFeatureinst, null, pPropinst, pPropvalue, pModalprop}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for propinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<PropertyAssociationInstance> rawStreamAllValuesOfpropinst(final Object[] parameters) {
      return rawStreamAllValues(POSITION_PROPINST, parameters).map(PropertyAssociationInstance.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for propinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<PropertyAssociationInstance> getAllValuesOfpropinst() {
      return rawStreamAllValuesOfpropinst(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for propinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<PropertyAssociationInstance> streamAllValuesOfpropinst() {
      return rawStreamAllValuesOfpropinst(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for propinst.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<PropertyAssociationInstance> streamAllValuesOfpropinst(final Find_PropertyRefAttach2Feature.Match partialMatch) {
      return rawStreamAllValuesOfpropinst(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for propinst.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<PropertyAssociationInstance> streamAllValuesOfpropinst(final ComponentInstance pCompinst, final FeatureInstance pFeatureinst, final Aaxl2AaxlTrace pTrace, final InstanceReferenceValue pPropvalue, final ModalPropertyValue pModalprop) {
      return rawStreamAllValuesOfpropinst(new Object[]{pCompinst, pFeatureinst, pTrace, null, pPropvalue, pModalprop});
    }

    /**
     * Retrieve the set of values that occur in matches for propinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<PropertyAssociationInstance> getAllValuesOfpropinst(final Find_PropertyRefAttach2Feature.Match partialMatch) {
      return rawStreamAllValuesOfpropinst(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for propinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<PropertyAssociationInstance> getAllValuesOfpropinst(final ComponentInstance pCompinst, final FeatureInstance pFeatureinst, final Aaxl2AaxlTrace pTrace, final InstanceReferenceValue pPropvalue, final ModalPropertyValue pModalprop) {
      return rawStreamAllValuesOfpropinst(new Object[]{pCompinst, pFeatureinst, pTrace, null, pPropvalue, pModalprop}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for propvalue.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<InstanceReferenceValue> rawStreamAllValuesOfpropvalue(final Object[] parameters) {
      return rawStreamAllValues(POSITION_PROPVALUE, parameters).map(InstanceReferenceValue.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for propvalue.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<InstanceReferenceValue> getAllValuesOfpropvalue() {
      return rawStreamAllValuesOfpropvalue(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for propvalue.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<InstanceReferenceValue> streamAllValuesOfpropvalue() {
      return rawStreamAllValuesOfpropvalue(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for propvalue.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<InstanceReferenceValue> streamAllValuesOfpropvalue(final Find_PropertyRefAttach2Feature.Match partialMatch) {
      return rawStreamAllValuesOfpropvalue(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for propvalue.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<InstanceReferenceValue> streamAllValuesOfpropvalue(final ComponentInstance pCompinst, final FeatureInstance pFeatureinst, final Aaxl2AaxlTrace pTrace, final PropertyAssociationInstance pPropinst, final ModalPropertyValue pModalprop) {
      return rawStreamAllValuesOfpropvalue(new Object[]{pCompinst, pFeatureinst, pTrace, pPropinst, null, pModalprop});
    }

    /**
     * Retrieve the set of values that occur in matches for propvalue.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<InstanceReferenceValue> getAllValuesOfpropvalue(final Find_PropertyRefAttach2Feature.Match partialMatch) {
      return rawStreamAllValuesOfpropvalue(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for propvalue.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<InstanceReferenceValue> getAllValuesOfpropvalue(final ComponentInstance pCompinst, final FeatureInstance pFeatureinst, final Aaxl2AaxlTrace pTrace, final PropertyAssociationInstance pPropinst, final ModalPropertyValue pModalprop) {
      return rawStreamAllValuesOfpropvalue(new Object[]{pCompinst, pFeatureinst, pTrace, pPropinst, null, pModalprop}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for modalprop.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ModalPropertyValue> rawStreamAllValuesOfmodalprop(final Object[] parameters) {
      return rawStreamAllValues(POSITION_MODALPROP, parameters).map(ModalPropertyValue.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for modalprop.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ModalPropertyValue> getAllValuesOfmodalprop() {
      return rawStreamAllValuesOfmodalprop(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for modalprop.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ModalPropertyValue> streamAllValuesOfmodalprop() {
      return rawStreamAllValuesOfmodalprop(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for modalprop.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ModalPropertyValue> streamAllValuesOfmodalprop(final Find_PropertyRefAttach2Feature.Match partialMatch) {
      return rawStreamAllValuesOfmodalprop(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for modalprop.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ModalPropertyValue> streamAllValuesOfmodalprop(final ComponentInstance pCompinst, final FeatureInstance pFeatureinst, final Aaxl2AaxlTrace pTrace, final PropertyAssociationInstance pPropinst, final InstanceReferenceValue pPropvalue) {
      return rawStreamAllValuesOfmodalprop(new Object[]{pCompinst, pFeatureinst, pTrace, pPropinst, pPropvalue, null});
    }

    /**
     * Retrieve the set of values that occur in matches for modalprop.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ModalPropertyValue> getAllValuesOfmodalprop(final Find_PropertyRefAttach2Feature.Match partialMatch) {
      return rawStreamAllValuesOfmodalprop(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for modalprop.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ModalPropertyValue> getAllValuesOfmodalprop(final ComponentInstance pCompinst, final FeatureInstance pFeatureinst, final Aaxl2AaxlTrace pTrace, final PropertyAssociationInstance pPropinst, final InstanceReferenceValue pPropvalue) {
      return rawStreamAllValuesOfmodalprop(new Object[]{pCompinst, pFeatureinst, pTrace, pPropinst, pPropvalue, null}).collect(Collectors.toSet());
    }

    @Override
    protected Find_PropertyRefAttach2Feature.Match tupleToMatch(final Tuple t) {
      try {
          return Find_PropertyRefAttach2Feature.Match.newMatch((ComponentInstance) t.get(POSITION_COMPINST), (FeatureInstance) t.get(POSITION_FEATUREINST), (Aaxl2AaxlTrace) t.get(POSITION_TRACE), (PropertyAssociationInstance) t.get(POSITION_PROPINST), (InstanceReferenceValue) t.get(POSITION_PROPVALUE), (ModalPropertyValue) t.get(POSITION_MODALPROP));
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in tuple not properly typed!",e);
          return null;
      }
    }

    @Override
    protected Find_PropertyRefAttach2Feature.Match arrayToMatch(final Object[] match) {
      try {
          return Find_PropertyRefAttach2Feature.Match.newMatch((ComponentInstance) match[POSITION_COMPINST], (FeatureInstance) match[POSITION_FEATUREINST], (Aaxl2AaxlTrace) match[POSITION_TRACE], (PropertyAssociationInstance) match[POSITION_PROPINST], (InstanceReferenceValue) match[POSITION_PROPVALUE], (ModalPropertyValue) match[POSITION_MODALPROP]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    @Override
    protected Find_PropertyRefAttach2Feature.Match arrayToMatchMutable(final Object[] match) {
      try {
          return Find_PropertyRefAttach2Feature.Match.newMutableMatch((ComponentInstance) match[POSITION_COMPINST], (FeatureInstance) match[POSITION_FEATUREINST], (Aaxl2AaxlTrace) match[POSITION_TRACE], (PropertyAssociationInstance) match[POSITION_PROPINST], (InstanceReferenceValue) match[POSITION_PROPVALUE], (ModalPropertyValue) match[POSITION_MODALPROP]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    /**
     * @return the singleton instance of the query specification of this pattern
     * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
     * 
     */
    public static IQuerySpecification<Find_PropertyRefAttach2Feature.Matcher> querySpecification() {
      return Find_PropertyRefAttach2Feature.instance();
    }
  }

  private Find_PropertyRefAttach2Feature() {
    super(GeneratedPQuery.INSTANCE);
  }

  /**
   * @return the singleton instance of the query specification
   * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
   * 
   */
  public static Find_PropertyRefAttach2Feature instance() {
    try{
        return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
        throw processInitializerError(err);
    }
  }

  @Override
  protected Find_PropertyRefAttach2Feature.Matcher instantiate(final ViatraQueryEngine engine) {
    return Find_PropertyRefAttach2Feature.Matcher.on(engine);
  }

  @Override
  public Find_PropertyRefAttach2Feature.Matcher instantiate() {
    return Find_PropertyRefAttach2Feature.Matcher.create();
  }

  @Override
  public Find_PropertyRefAttach2Feature.Match newEmptyMatch() {
    return Find_PropertyRefAttach2Feature.Match.newEmptyMatch();
  }

  @Override
  public Find_PropertyRefAttach2Feature.Match newMatch(final Object... parameters) {
    return Find_PropertyRefAttach2Feature.Match.newMatch((org.osate.aadl2.instance.ComponentInstance) parameters[0], (org.osate.aadl2.instance.FeatureInstance) parameters[1], (fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace) parameters[2], (org.osate.aadl2.instance.PropertyAssociationInstance) parameters[3], (org.osate.aadl2.instance.InstanceReferenceValue) parameters[4], (org.osate.aadl2.ModalPropertyValue) parameters[5]);
  }

  /**
   * Inner class allowing the singleton instance of {@link Find_PropertyRefAttach2Feature} to be created 
   *     <b>not</b> at the class load time of the outer class, 
   *     but rather at the first call to {@link Find_PropertyRefAttach2Feature#instance()}.
   * 
   * <p> This workaround is required e.g. to support recursion.
   * 
   */
  private static class LazyHolder {
    private static final Find_PropertyRefAttach2Feature INSTANCE = new Find_PropertyRefAttach2Feature();

    /**
     * Statically initializes the query specification <b>after</b> the field {@link #INSTANCE} is assigned.
     * This initialization order is required to support indirect recursion.
     * 
     * <p> The static initializer is defined using a helper field to work around limitations of the code generator.
     * 
     */
    private static final Object STATIC_INITIALIZER = ensureInitialized();

    public static Object ensureInitialized() {
      INSTANCE.ensureInitializedInternal();
      return null;
    }
  }

  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private static final Find_PropertyRefAttach2Feature.GeneratedPQuery INSTANCE = new GeneratedPQuery();

    private final PParameter parameter_compinst = new PParameter("compinst", "org.osate.aadl2.instance.ComponentInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ComponentInstance")), PParameterDirection.INOUT);

    private final PParameter parameter_featureinst = new PParameter("featureinst", "org.osate.aadl2.instance.FeatureInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "FeatureInstance")), PParameterDirection.INOUT);

    private final PParameter parameter_trace = new PParameter("trace", "fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace")), PParameterDirection.INOUT);

    private final PParameter parameter_propinst = new PParameter("propinst", "org.osate.aadl2.instance.PropertyAssociationInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "PropertyAssociationInstance")), PParameterDirection.INOUT);

    private final PParameter parameter_propvalue = new PParameter("propvalue", "org.osate.aadl2.instance.InstanceReferenceValue", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "InstanceReferenceValue")), PParameterDirection.INOUT);

    private final PParameter parameter_modalprop = new PParameter("modalprop", "org.osate.aadl2.ModalPropertyValue", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0", "ModalPropertyValue")), PParameterDirection.INOUT);

    private final List<PParameter> parameters = Arrays.asList(parameter_compinst, parameter_featureinst, parameter_trace, parameter_propinst, parameter_propvalue, parameter_modalprop);

    private GeneratedPQuery() {
      super(PVisibility.PUBLIC);
    }

    @Override
    public String getFullyQualifiedName() {
      return "fr.mem4csd.osatedim.viatra.queries.find_PropertyRefAttach2Feature";
    }

    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("compinst","featureinst","trace","propinst","propvalue","modalprop");
    }

    @Override
    public List<PParameter> getParameters() {
      return parameters;
    }

    @Override
    public Set<PBody> doGetContainedBodies() {
      setEvaluationHints(new QueryEvaluationHint(null, QueryEvaluationHint.BackendRequirement.UNSPECIFIED));
      Set<PBody> bodies = new LinkedHashSet<>();
      {
          PBody body = new PBody(this);
          PVariable var_compinst = body.getOrCreateVariableByName("compinst");
          PVariable var_featureinst = body.getOrCreateVariableByName("featureinst");
          PVariable var_trace = body.getOrCreateVariableByName("trace");
          PVariable var_propinst = body.getOrCreateVariableByName("propinst");
          PVariable var_propvalue = body.getOrCreateVariableByName("propvalue");
          PVariable var_modalprop = body.getOrCreateVariableByName("modalprop");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          PVariable var___1_ = body.getOrCreateVariableByName("_<1>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_featureinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "FeatureInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_trace), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_propinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "PropertyAssociationInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_propvalue), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "InstanceReferenceValue")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_modalprop), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ModalPropertyValue")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_compinst, parameter_compinst),
             new ExportedParameter(body, var_featureinst, parameter_featureinst),
             new ExportedParameter(body, var_trace, parameter_trace),
             new ExportedParameter(body, var_propinst, parameter_propinst),
             new ExportedParameter(body, var_propvalue, parameter_propvalue),
             new ExportedParameter(body, var_modalprop, parameter_modalprop)
          ));
          // 	ComponentInstance.featureInstance(compinst, featureinst)
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance", "featureInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "FeatureInstance")));
          new Equality(body, var__virtual_0_, var_featureinst);
          // 	find is_in_trace(_,trace,featureinst,_)
          new PositivePatternCall(body, Tuples.flatTupleOf(var___0_, var_trace, var_featureinst, var___1_), Is_in_trace.instance().getInternalQueryRepresentation());
          // 	Aaxl2AaxlTrace.objectsToAttach(trace, propinst)
          new TypeConstraint(body, Tuples.flatTupleOf(var_trace), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_trace, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace", "objectsToAttach")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Element")));
          new Equality(body, var__virtual_1_, var_propinst);
          // 	PropertyAssociationInstance.ownedValue(propinst,modalprop)
          new TypeConstraint(body, Tuples.flatTupleOf(var_propinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "PropertyAssociationInstance")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_propinst, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "PropertyAssociation", "ownedValue")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ModalPropertyValue")));
          new Equality(body, var__virtual_2_, var_modalprop);
          // 	ModalPropertyValue.ownedValue(modalprop,propvalue)
          new TypeConstraint(body, Tuples.flatTupleOf(var_modalprop), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ModalPropertyValue")));
          PVariable var__virtual_3_ = body.getOrCreateVariableByName(".virtual{3}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_modalprop, var__virtual_3_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ModalPropertyValue", "ownedValue")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_3_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "PropertyExpression")));
          new Equality(body, var__virtual_3_, var_propvalue);
          bodies.add(body);
      }
      return bodies;
    }
  }
}
