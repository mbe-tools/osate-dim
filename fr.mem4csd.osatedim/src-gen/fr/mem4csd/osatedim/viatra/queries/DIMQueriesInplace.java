/**
 * Generated from platform:/resource/fr.mem4csd.osatedim/src/fr/mem4csd/osatedim/viatra/queries/DIMQueriesInplace.vql
 */
package fr.mem4csd.osatedim.viatra.queries;

import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedPatternGroup;

/**
 * A pattern group formed of all public patterns defined in DIMQueriesInplace.vql.
 * 
 * <p>Use the static instance as any {@link interface org.eclipse.viatra.query.runtime.api.IQueryGroup}, to conveniently prepare
 * a VIATRA Query engine for matching all patterns originally defined in file DIMQueriesInplace.vql,
 * in order to achieve better performance than one-by-one on-demand matcher initialization.
 * 
 * <p> From package fr.mem4csd.osatedim.viatra.queries, the group contains the definition of the following patterns: <ul>
 * <li>findInstanceObject</li>
 * <li>findComponentInstance</li>
 * <li>findReusedClassifier</li>
 * <li>findExtraComponent</li>
 * <li>findSystem</li>
 * <li>findSubcomponent</li>
 * <li>findFeature</li>
 * <li>findConnection</li>
 * <li>findConnectionReference</li>
 * <li>findMode</li>
 * <li>findDerivedMode</li>
 * <li>findModeTransition</li>
 * <li>findProperty</li>
 * <li>findModalProperty</li>
 * <li>findPropertyValue</li>
 * <li>findPropertyReferencingElements</li>
 * <li>findParameterConnection</li>
 * </ul>
 * 
 * @see IQueryGroup
 * 
 */
@SuppressWarnings("all")
public final class DIMQueriesInplace extends BaseGeneratedPatternGroup {
  /**
   * Access the pattern group.
   * 
   * @return the singleton instance of the group
   * @throws ViatraQueryRuntimeException if there was an error loading the generated code of pattern specifications
   * 
   */
  public static DIMQueriesInplace instance() {
    if (INSTANCE == null) {
        INSTANCE = new DIMQueriesInplace();
    }
    return INSTANCE;
  }

  private static DIMQueriesInplace INSTANCE;

  private DIMQueriesInplace() {
    querySpecifications.add(FindInstanceObject.instance());
    querySpecifications.add(FindComponentInstance.instance());
    querySpecifications.add(FindReusedClassifier.instance());
    querySpecifications.add(FindExtraComponent.instance());
    querySpecifications.add(FindSystem.instance());
    querySpecifications.add(FindSubcomponent.instance());
    querySpecifications.add(FindFeature.instance());
    querySpecifications.add(FindConnection.instance());
    querySpecifications.add(FindConnectionReference.instance());
    querySpecifications.add(FindMode.instance());
    querySpecifications.add(FindDerivedMode.instance());
    querySpecifications.add(FindModeTransition.instance());
    querySpecifications.add(FindProperty.instance());
    querySpecifications.add(FindModalProperty.instance());
    querySpecifications.add(FindPropertyValue.instance());
    querySpecifications.add(FindPropertyReferencingElements.instance());
    querySpecifications.add(FindParameterConnection.instance());
  }

  public FindInstanceObject getFindInstanceObject() {
    return FindInstanceObject.instance();
  }

  public FindInstanceObject.Matcher getFindInstanceObject(final ViatraQueryEngine engine) {
    return FindInstanceObject.Matcher.on(engine);
  }

  public FindComponentInstance getFindComponentInstance() {
    return FindComponentInstance.instance();
  }

  public FindComponentInstance.Matcher getFindComponentInstance(final ViatraQueryEngine engine) {
    return FindComponentInstance.Matcher.on(engine);
  }

  public FindReusedClassifier getFindReusedClassifier() {
    return FindReusedClassifier.instance();
  }

  public FindReusedClassifier.Matcher getFindReusedClassifier(final ViatraQueryEngine engine) {
    return FindReusedClassifier.Matcher.on(engine);
  }

  public FindExtraComponent getFindExtraComponent() {
    return FindExtraComponent.instance();
  }

  public FindExtraComponent.Matcher getFindExtraComponent(final ViatraQueryEngine engine) {
    return FindExtraComponent.Matcher.on(engine);
  }

  public FindSystem getFindSystem() {
    return FindSystem.instance();
  }

  public FindSystem.Matcher getFindSystem(final ViatraQueryEngine engine) {
    return FindSystem.Matcher.on(engine);
  }

  public FindSubcomponent getFindSubcomponent() {
    return FindSubcomponent.instance();
  }

  public FindSubcomponent.Matcher getFindSubcomponent(final ViatraQueryEngine engine) {
    return FindSubcomponent.Matcher.on(engine);
  }

  public FindFeature getFindFeature() {
    return FindFeature.instance();
  }

  public FindFeature.Matcher getFindFeature(final ViatraQueryEngine engine) {
    return FindFeature.Matcher.on(engine);
  }

  public FindConnection getFindConnection() {
    return FindConnection.instance();
  }

  public FindConnection.Matcher getFindConnection(final ViatraQueryEngine engine) {
    return FindConnection.Matcher.on(engine);
  }

  public FindConnectionReference getFindConnectionReference() {
    return FindConnectionReference.instance();
  }

  public FindConnectionReference.Matcher getFindConnectionReference(final ViatraQueryEngine engine) {
    return FindConnectionReference.Matcher.on(engine);
  }

  public FindMode getFindMode() {
    return FindMode.instance();
  }

  public FindMode.Matcher getFindMode(final ViatraQueryEngine engine) {
    return FindMode.Matcher.on(engine);
  }

  public FindDerivedMode getFindDerivedMode() {
    return FindDerivedMode.instance();
  }

  public FindDerivedMode.Matcher getFindDerivedMode(final ViatraQueryEngine engine) {
    return FindDerivedMode.Matcher.on(engine);
  }

  public FindModeTransition getFindModeTransition() {
    return FindModeTransition.instance();
  }

  public FindModeTransition.Matcher getFindModeTransition(final ViatraQueryEngine engine) {
    return FindModeTransition.Matcher.on(engine);
  }

  public FindProperty getFindProperty() {
    return FindProperty.instance();
  }

  public FindProperty.Matcher getFindProperty(final ViatraQueryEngine engine) {
    return FindProperty.Matcher.on(engine);
  }

  public FindModalProperty getFindModalProperty() {
    return FindModalProperty.instance();
  }

  public FindModalProperty.Matcher getFindModalProperty(final ViatraQueryEngine engine) {
    return FindModalProperty.Matcher.on(engine);
  }

  public FindPropertyValue getFindPropertyValue() {
    return FindPropertyValue.instance();
  }

  public FindPropertyValue.Matcher getFindPropertyValue(final ViatraQueryEngine engine) {
    return FindPropertyValue.Matcher.on(engine);
  }

  public FindPropertyReferencingElements getFindPropertyReferencingElements() {
    return FindPropertyReferencingElements.instance();
  }

  public FindPropertyReferencingElements.Matcher getFindPropertyReferencingElements(final ViatraQueryEngine engine) {
    return FindPropertyReferencingElements.Matcher.on(engine);
  }

  public FindParameterConnection getFindParameterConnection() {
    return FindParameterConnection.instance();
  }

  public FindParameterConnection.Matcher getFindParameterConnection(final ViatraQueryEngine engine) {
    return FindParameterConnection.Matcher.on(engine);
  }
}
