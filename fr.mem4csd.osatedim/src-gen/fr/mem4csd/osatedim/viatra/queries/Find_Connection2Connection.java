/**
 * Generated from platform:/resource/fr.mem4csd.osatedim/src/fr/mem4csd/osatedim/viatra/queries/DIMQueriesOutplace.vql
 */
package fr.mem4csd.osatedim.viatra.queries;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.viatra.query.runtime.api.IPatternMatch;
import org.eclipse.viatra.query.runtime.api.IQuerySpecification;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.viatra.query.runtime.api.impl.BaseMatcher;
import org.eclipse.viatra.query.runtime.api.impl.BasePatternMatch;
import org.eclipse.viatra.query.runtime.emf.types.EClassTransitiveInstancesKey;
import org.eclipse.viatra.query.runtime.matchers.backend.QueryEvaluationHint;
import org.eclipse.viatra.query.runtime.matchers.psystem.PBody;
import org.eclipse.viatra.query.runtime.matchers.psystem.PVariable;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.TypeConstraint;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameterDirection;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PVisibility;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuple;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuples;
import org.eclipse.viatra.query.runtime.util.ViatraQueryLoggingUtil;
import org.osate.aadl2.instance.ConnectionInstance;

/**
 * A pattern-specific query specification that can instantiate Matcher in a type-safe way.
 * 
 * <p>Original source:
 *         <code><pre>
 *         pattern find_Connection2Connection(conninst : ConnectionInstance)
 *         {
 *         	ConnectionInstance(conninst);
 *         }
 * </pre></code>
 * 
 * @see Matcher
 * @see Match
 * 
 */
@SuppressWarnings("all")
public final class Find_Connection2Connection extends BaseGeneratedEMFQuerySpecification<Find_Connection2Connection.Matcher> {
  /**
   * Pattern-specific match representation of the fr.mem4csd.osatedim.viatra.queries.find_Connection2Connection pattern,
   * to be used in conjunction with {@link Matcher}.
   * 
   * <p>Class fields correspond to parameters of the pattern. Fields with value null are considered unassigned.
   * Each instance is a (possibly partial) substitution of pattern parameters,
   * usable to represent a match of the pattern in the result of a query,
   * or to specify the bound (fixed) input parameters when issuing a query.
   * 
   * @see Matcher
   * 
   */
  public static abstract class Match extends BasePatternMatch {
    private ConnectionInstance fConninst;

    private static List<String> parameterNames = makeImmutableList("conninst");

    private Match(final ConnectionInstance pConninst) {
      this.fConninst = pConninst;
    }

    @Override
    public Object get(final String parameterName) {
      switch(parameterName) {
          case "conninst": return this.fConninst;
          default: return null;
      }
    }

    @Override
    public Object get(final int index) {
      switch(index) {
          case 0: return this.fConninst;
          default: return null;
      }
    }

    public ConnectionInstance getConninst() {
      return this.fConninst;
    }

    @Override
    public boolean set(final String parameterName, final Object newValue) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      if ("conninst".equals(parameterName) ) {
          this.fConninst = (ConnectionInstance) newValue;
          return true;
      }
      return false;
    }

    public void setConninst(final ConnectionInstance pConninst) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fConninst = pConninst;
    }

    @Override
    public String patternName() {
      return "fr.mem4csd.osatedim.viatra.queries.find_Connection2Connection";
    }

    @Override
    public List<String> parameterNames() {
      return Find_Connection2Connection.Match.parameterNames;
    }

    @Override
    public Object[] toArray() {
      return new Object[]{fConninst};
    }

    @Override
    public Find_Connection2Connection.Match toImmutable() {
      return isMutable() ? newMatch(fConninst) : this;
    }

    @Override
    public String prettyPrint() {
      StringBuilder result = new StringBuilder();
      result.append("\"conninst\"=" + prettyPrintValue(fConninst));
      return result.toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(fConninst);
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj)
          return true;
      if (obj == null) {
          return false;
      }
      if ((obj instanceof Find_Connection2Connection.Match)) {
          Find_Connection2Connection.Match other = (Find_Connection2Connection.Match) obj;
          return Objects.equals(fConninst, other.fConninst);
      } else {
          // this should be infrequent
          if (!(obj instanceof IPatternMatch)) {
              return false;
          }
          IPatternMatch otherSig  = (IPatternMatch) obj;
          return Objects.equals(specification(), otherSig.specification()) && Arrays.deepEquals(toArray(), otherSig.toArray());
      }
    }

    @Override
    public Find_Connection2Connection specification() {
      return Find_Connection2Connection.instance();
    }

    /**
     * Returns an empty, mutable match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @return the empty match.
     * 
     */
    public static Find_Connection2Connection.Match newEmptyMatch() {
      return new Mutable(null);
    }

    /**
     * Returns a mutable (partial) match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @param pConninst the fixed value of pattern parameter conninst, or null if not bound.
     * @return the new, mutable (partial) match object.
     * 
     */
    public static Find_Connection2Connection.Match newMutableMatch(final ConnectionInstance pConninst) {
      return new Mutable(pConninst);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pConninst the fixed value of pattern parameter conninst, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public static Find_Connection2Connection.Match newMatch(final ConnectionInstance pConninst) {
      return new Immutable(pConninst);
    }

    private static final class Mutable extends Find_Connection2Connection.Match {
      Mutable(final ConnectionInstance pConninst) {
        super(pConninst);
      }

      @Override
      public boolean isMutable() {
        return true;
      }
    }

    private static final class Immutable extends Find_Connection2Connection.Match {
      Immutable(final ConnectionInstance pConninst) {
        super(pConninst);
      }

      @Override
      public boolean isMutable() {
        return false;
      }
    }
  }

  /**
   * Generated pattern matcher API of the fr.mem4csd.osatedim.viatra.queries.find_Connection2Connection pattern,
   * providing pattern-specific query methods.
   * 
   * <p>Use the pattern matcher on a given model via {@link #on(ViatraQueryEngine)},
   * e.g. in conjunction with {@link ViatraQueryEngine#on(QueryScope)}.
   * 
   * <p>Matches of the pattern will be represented as {@link Match}.
   * 
   * <p>Original source:
   * <code><pre>
   * pattern find_Connection2Connection(conninst : ConnectionInstance)
   * {
   * 	ConnectionInstance(conninst);
   * }
   * </pre></code>
   * 
   * @see Match
   * @see Find_Connection2Connection
   * 
   */
  public static class Matcher extends BaseMatcher<Find_Connection2Connection.Match> {
    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    public static Find_Connection2Connection.Matcher on(final ViatraQueryEngine engine) {
      // check if matcher already exists
      Matcher matcher = engine.getExistingMatcher(querySpecification());
      if (matcher == null) {
          matcher = (Matcher)engine.getMatcher(querySpecification());
      }
      return matcher;
    }

    /**
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * @return an initialized matcher
     * @noreference This method is for internal matcher initialization by the framework, do not call it manually.
     * 
     */
    public static Find_Connection2Connection.Matcher create() {
      return new Matcher();
    }

    private static final int POSITION_CONNINST = 0;

    private static final Logger LOGGER = ViatraQueryLoggingUtil.getLogger(Find_Connection2Connection.Matcher.class);

    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    private Matcher() {
      super(querySpecification());
    }

    /**
     * Returns the set of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pConninst the fixed value of pattern parameter conninst, or null if not bound.
     * @return matches represented as a Match object.
     * 
     */
    public Collection<Find_Connection2Connection.Match> getAllMatches(final ConnectionInstance pConninst) {
      return rawStreamAllMatches(new Object[]{pConninst}).collect(Collectors.toSet());
    }

    /**
     * Returns a stream of all matches of the pattern that conform to the given fixed values of some parameters.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     * @param pConninst the fixed value of pattern parameter conninst, or null if not bound.
     * @return a stream of matches represented as a Match object.
     * 
     */
    public Stream<Find_Connection2Connection.Match> streamAllMatches(final ConnectionInstance pConninst) {
      return rawStreamAllMatches(new Object[]{pConninst});
    }

    /**
     * Returns an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pConninst the fixed value of pattern parameter conninst, or null if not bound.
     * @return a match represented as a Match object, or null if no match is found.
     * 
     */
    public Optional<Find_Connection2Connection.Match> getOneArbitraryMatch(final ConnectionInstance pConninst) {
      return rawGetOneArbitraryMatch(new Object[]{pConninst});
    }

    /**
     * Indicates whether the given combination of specified pattern parameters constitute a valid pattern match,
     * under any possible substitution of the unspecified parameters (if any).
     * @param pConninst the fixed value of pattern parameter conninst, or null if not bound.
     * @return true if the input is a valid (partial) match of the pattern.
     * 
     */
    public boolean hasMatch(final ConnectionInstance pConninst) {
      return rawHasMatch(new Object[]{pConninst});
    }

    /**
     * Returns the number of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pConninst the fixed value of pattern parameter conninst, or null if not bound.
     * @return the number of pattern matches found.
     * 
     */
    public int countMatches(final ConnectionInstance pConninst) {
      return rawCountMatches(new Object[]{pConninst});
    }

    /**
     * Executes the given processor on an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pConninst the fixed value of pattern parameter conninst, or null if not bound.
     * @param processor the action that will process the selected match.
     * @return true if the pattern has at least one match with the given parameter values, false if the processor was not invoked
     * 
     */
    public boolean forOneArbitraryMatch(final ConnectionInstance pConninst, final Consumer<? super Find_Connection2Connection.Match> processor) {
      return rawForOneArbitraryMatch(new Object[]{pConninst}, processor);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pConninst the fixed value of pattern parameter conninst, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public Find_Connection2Connection.Match newMatch(final ConnectionInstance pConninst) {
      return Find_Connection2Connection.Match.newMatch(pConninst);
    }

    /**
     * Retrieve the set of values that occur in matches for conninst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ConnectionInstance> rawStreamAllValuesOfconninst(final Object[] parameters) {
      return rawStreamAllValues(POSITION_CONNINST, parameters).map(ConnectionInstance.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for conninst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ConnectionInstance> getAllValuesOfconninst() {
      return rawStreamAllValuesOfconninst(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for conninst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ConnectionInstance> streamAllValuesOfconninst() {
      return rawStreamAllValuesOfconninst(emptyArray());
    }

    @Override
    protected Find_Connection2Connection.Match tupleToMatch(final Tuple t) {
      try {
          return Find_Connection2Connection.Match.newMatch((ConnectionInstance) t.get(POSITION_CONNINST));
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in tuple not properly typed!",e);
          return null;
      }
    }

    @Override
    protected Find_Connection2Connection.Match arrayToMatch(final Object[] match) {
      try {
          return Find_Connection2Connection.Match.newMatch((ConnectionInstance) match[POSITION_CONNINST]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    @Override
    protected Find_Connection2Connection.Match arrayToMatchMutable(final Object[] match) {
      try {
          return Find_Connection2Connection.Match.newMutableMatch((ConnectionInstance) match[POSITION_CONNINST]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    /**
     * @return the singleton instance of the query specification of this pattern
     * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
     * 
     */
    public static IQuerySpecification<Find_Connection2Connection.Matcher> querySpecification() {
      return Find_Connection2Connection.instance();
    }
  }

  private Find_Connection2Connection() {
    super(GeneratedPQuery.INSTANCE);
  }

  /**
   * @return the singleton instance of the query specification
   * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
   * 
   */
  public static Find_Connection2Connection instance() {
    try{
        return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
        throw processInitializerError(err);
    }
  }

  @Override
  protected Find_Connection2Connection.Matcher instantiate(final ViatraQueryEngine engine) {
    return Find_Connection2Connection.Matcher.on(engine);
  }

  @Override
  public Find_Connection2Connection.Matcher instantiate() {
    return Find_Connection2Connection.Matcher.create();
  }

  @Override
  public Find_Connection2Connection.Match newEmptyMatch() {
    return Find_Connection2Connection.Match.newEmptyMatch();
  }

  @Override
  public Find_Connection2Connection.Match newMatch(final Object... parameters) {
    return Find_Connection2Connection.Match.newMatch((org.osate.aadl2.instance.ConnectionInstance) parameters[0]);
  }

  /**
   * Inner class allowing the singleton instance of {@link Find_Connection2Connection} to be created 
   *     <b>not</b> at the class load time of the outer class, 
   *     but rather at the first call to {@link Find_Connection2Connection#instance()}.
   * 
   * <p> This workaround is required e.g. to support recursion.
   * 
   */
  private static class LazyHolder {
    private static final Find_Connection2Connection INSTANCE = new Find_Connection2Connection();

    /**
     * Statically initializes the query specification <b>after</b> the field {@link #INSTANCE} is assigned.
     * This initialization order is required to support indirect recursion.
     * 
     * <p> The static initializer is defined using a helper field to work around limitations of the code generator.
     * 
     */
    private static final Object STATIC_INITIALIZER = ensureInitialized();

    public static Object ensureInitialized() {
      INSTANCE.ensureInitializedInternal();
      return null;
    }
  }

  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private static final Find_Connection2Connection.GeneratedPQuery INSTANCE = new GeneratedPQuery();

    private final PParameter parameter_conninst = new PParameter("conninst", "org.osate.aadl2.instance.ConnectionInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ConnectionInstance")), PParameterDirection.INOUT);

    private final List<PParameter> parameters = Arrays.asList(parameter_conninst);

    private GeneratedPQuery() {
      super(PVisibility.PUBLIC);
    }

    @Override
    public String getFullyQualifiedName() {
      return "fr.mem4csd.osatedim.viatra.queries.find_Connection2Connection";
    }

    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("conninst");
    }

    @Override
    public List<PParameter> getParameters() {
      return parameters;
    }

    @Override
    public Set<PBody> doGetContainedBodies() {
      setEvaluationHints(new QueryEvaluationHint(null, QueryEvaluationHint.BackendRequirement.UNSPECIFIED));
      Set<PBody> bodies = new LinkedHashSet<>();
      {
          PBody body = new PBody(this);
          PVariable var_conninst = body.getOrCreateVariableByName("conninst");
          new TypeConstraint(body, Tuples.flatTupleOf(var_conninst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstance")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_conninst, parameter_conninst)
          ));
          // 	ConnectionInstance(conninst)
          new TypeConstraint(body, Tuples.flatTupleOf(var_conninst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ConnectionInstance")));
          bodies.add(body);
      }
      return bodies;
    }
  }
}
