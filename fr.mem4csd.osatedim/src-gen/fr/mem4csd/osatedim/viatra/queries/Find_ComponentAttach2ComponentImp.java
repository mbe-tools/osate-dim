/**
 * Generated from platform:/resource/fr.mem4csd.osatedim/src/fr/mem4csd/osatedim/viatra/queries/DIMQueriesOutplace.vql
 */
package fr.mem4csd.osatedim.viatra.queries;

import fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.viatra.query.runtime.api.IPatternMatch;
import org.eclipse.viatra.query.runtime.api.IQuerySpecification;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.viatra.query.runtime.api.impl.BaseMatcher;
import org.eclipse.viatra.query.runtime.api.impl.BasePatternMatch;
import org.eclipse.viatra.query.runtime.emf.types.EClassTransitiveInstancesKey;
import org.eclipse.viatra.query.runtime.emf.types.EStructuralFeatureInstancesKey;
import org.eclipse.viatra.query.runtime.matchers.backend.QueryEvaluationHint;
import org.eclipse.viatra.query.runtime.matchers.psystem.PBody;
import org.eclipse.viatra.query.runtime.matchers.psystem.PVariable;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.Equality;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.PositivePatternCall;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.TypeConstraint;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameterDirection;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PVisibility;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuple;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuples;
import org.eclipse.viatra.query.runtime.util.ViatraQueryLoggingUtil;
import org.osate.aadl2.ComponentImplementation;
import org.osate.aadl2.instance.ComponentInstance;

/**
 * A pattern-specific query specification that can instantiate Matcher in a type-safe way.
 * 
 * <p>Original source:
 *         <code><pre>
 *         // This pattern finds the component2Attach for a specific component which has a implementation definition classifier
 *         pattern find_ComponentAttach2ComponentImp(compinst : ComponentInstance,
 *         								          trace : Aaxl2AaxlTrace,
 *         								          compinst2add : ComponentInstance,
 *         								          comp : ComponentImplementation)
 *         {
 *         	find is_in_trace(_,trace,compinst,_);
 *         	Aaxl2AaxlTrace.objectsToAttach(trace,compinst2add);
 *         	ComponentInstance.classifier(compinst,comp);
 *         }
 * </pre></code>
 * 
 * @see Matcher
 * @see Match
 * 
 */
@SuppressWarnings("all")
public final class Find_ComponentAttach2ComponentImp extends BaseGeneratedEMFQuerySpecification<Find_ComponentAttach2ComponentImp.Matcher> {
  /**
   * Pattern-specific match representation of the fr.mem4csd.osatedim.viatra.queries.find_ComponentAttach2ComponentImp pattern,
   * to be used in conjunction with {@link Matcher}.
   * 
   * <p>Class fields correspond to parameters of the pattern. Fields with value null are considered unassigned.
   * Each instance is a (possibly partial) substitution of pattern parameters,
   * usable to represent a match of the pattern in the result of a query,
   * or to specify the bound (fixed) input parameters when issuing a query.
   * 
   * @see Matcher
   * 
   */
  public static abstract class Match extends BasePatternMatch {
    private ComponentInstance fCompinst;

    private Aaxl2AaxlTrace fTrace;

    private ComponentInstance fCompinst2add;

    private ComponentImplementation fComp;

    private static List<String> parameterNames = makeImmutableList("compinst", "trace", "compinst2add", "comp");

    private Match(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentImplementation pComp) {
      this.fCompinst = pCompinst;
      this.fTrace = pTrace;
      this.fCompinst2add = pCompinst2add;
      this.fComp = pComp;
    }

    @Override
    public Object get(final String parameterName) {
      switch(parameterName) {
          case "compinst": return this.fCompinst;
          case "trace": return this.fTrace;
          case "compinst2add": return this.fCompinst2add;
          case "comp": return this.fComp;
          default: return null;
      }
    }

    @Override
    public Object get(final int index) {
      switch(index) {
          case 0: return this.fCompinst;
          case 1: return this.fTrace;
          case 2: return this.fCompinst2add;
          case 3: return this.fComp;
          default: return null;
      }
    }

    public ComponentInstance getCompinst() {
      return this.fCompinst;
    }

    public Aaxl2AaxlTrace getTrace() {
      return this.fTrace;
    }

    public ComponentInstance getCompinst2add() {
      return this.fCompinst2add;
    }

    public ComponentImplementation getComp() {
      return this.fComp;
    }

    @Override
    public boolean set(final String parameterName, final Object newValue) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      if ("compinst".equals(parameterName) ) {
          this.fCompinst = (ComponentInstance) newValue;
          return true;
      }
      if ("trace".equals(parameterName) ) {
          this.fTrace = (Aaxl2AaxlTrace) newValue;
          return true;
      }
      if ("compinst2add".equals(parameterName) ) {
          this.fCompinst2add = (ComponentInstance) newValue;
          return true;
      }
      if ("comp".equals(parameterName) ) {
          this.fComp = (ComponentImplementation) newValue;
          return true;
      }
      return false;
    }

    public void setCompinst(final ComponentInstance pCompinst) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fCompinst = pCompinst;
    }

    public void setTrace(final Aaxl2AaxlTrace pTrace) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fTrace = pTrace;
    }

    public void setCompinst2add(final ComponentInstance pCompinst2add) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fCompinst2add = pCompinst2add;
    }

    public void setComp(final ComponentImplementation pComp) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fComp = pComp;
    }

    @Override
    public String patternName() {
      return "fr.mem4csd.osatedim.viatra.queries.find_ComponentAttach2ComponentImp";
    }

    @Override
    public List<String> parameterNames() {
      return Find_ComponentAttach2ComponentImp.Match.parameterNames;
    }

    @Override
    public Object[] toArray() {
      return new Object[]{fCompinst, fTrace, fCompinst2add, fComp};
    }

    @Override
    public Find_ComponentAttach2ComponentImp.Match toImmutable() {
      return isMutable() ? newMatch(fCompinst, fTrace, fCompinst2add, fComp) : this;
    }

    @Override
    public String prettyPrint() {
      StringBuilder result = new StringBuilder();
      result.append("\"compinst\"=" + prettyPrintValue(fCompinst) + ", ");
      result.append("\"trace\"=" + prettyPrintValue(fTrace) + ", ");
      result.append("\"compinst2add\"=" + prettyPrintValue(fCompinst2add) + ", ");
      result.append("\"comp\"=" + prettyPrintValue(fComp));
      return result.toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(fCompinst, fTrace, fCompinst2add, fComp);
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj)
          return true;
      if (obj == null) {
          return false;
      }
      if ((obj instanceof Find_ComponentAttach2ComponentImp.Match)) {
          Find_ComponentAttach2ComponentImp.Match other = (Find_ComponentAttach2ComponentImp.Match) obj;
          return Objects.equals(fCompinst, other.fCompinst) && Objects.equals(fTrace, other.fTrace) && Objects.equals(fCompinst2add, other.fCompinst2add) && Objects.equals(fComp, other.fComp);
      } else {
          // this should be infrequent
          if (!(obj instanceof IPatternMatch)) {
              return false;
          }
          IPatternMatch otherSig  = (IPatternMatch) obj;
          return Objects.equals(specification(), otherSig.specification()) && Arrays.deepEquals(toArray(), otherSig.toArray());
      }
    }

    @Override
    public Find_ComponentAttach2ComponentImp specification() {
      return Find_ComponentAttach2ComponentImp.instance();
    }

    /**
     * Returns an empty, mutable match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @return the empty match.
     * 
     */
    public static Find_ComponentAttach2ComponentImp.Match newEmptyMatch() {
      return new Mutable(null, null, null, null);
    }

    /**
     * Returns a mutable (partial) match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pCompinst2add the fixed value of pattern parameter compinst2add, or null if not bound.
     * @param pComp the fixed value of pattern parameter comp, or null if not bound.
     * @return the new, mutable (partial) match object.
     * 
     */
    public static Find_ComponentAttach2ComponentImp.Match newMutableMatch(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentImplementation pComp) {
      return new Mutable(pCompinst, pTrace, pCompinst2add, pComp);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pCompinst2add the fixed value of pattern parameter compinst2add, or null if not bound.
     * @param pComp the fixed value of pattern parameter comp, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public static Find_ComponentAttach2ComponentImp.Match newMatch(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentImplementation pComp) {
      return new Immutable(pCompinst, pTrace, pCompinst2add, pComp);
    }

    private static final class Mutable extends Find_ComponentAttach2ComponentImp.Match {
      Mutable(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentImplementation pComp) {
        super(pCompinst, pTrace, pCompinst2add, pComp);
      }

      @Override
      public boolean isMutable() {
        return true;
      }
    }

    private static final class Immutable extends Find_ComponentAttach2ComponentImp.Match {
      Immutable(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentImplementation pComp) {
        super(pCompinst, pTrace, pCompinst2add, pComp);
      }

      @Override
      public boolean isMutable() {
        return false;
      }
    }
  }

  /**
   * Generated pattern matcher API of the fr.mem4csd.osatedim.viatra.queries.find_ComponentAttach2ComponentImp pattern,
   * providing pattern-specific query methods.
   * 
   * <p>Use the pattern matcher on a given model via {@link #on(ViatraQueryEngine)},
   * e.g. in conjunction with {@link ViatraQueryEngine#on(QueryScope)}.
   * 
   * <p>Matches of the pattern will be represented as {@link Match}.
   * 
   * <p>Original source:
   * <code><pre>
   * // This pattern finds the component2Attach for a specific component which has a implementation definition classifier
   * pattern find_ComponentAttach2ComponentImp(compinst : ComponentInstance,
   * 								          trace : Aaxl2AaxlTrace,
   * 								          compinst2add : ComponentInstance,
   * 								          comp : ComponentImplementation)
   * {
   * 	find is_in_trace(_,trace,compinst,_);
   * 	Aaxl2AaxlTrace.objectsToAttach(trace,compinst2add);
   * 	ComponentInstance.classifier(compinst,comp);
   * }
   * </pre></code>
   * 
   * @see Match
   * @see Find_ComponentAttach2ComponentImp
   * 
   */
  public static class Matcher extends BaseMatcher<Find_ComponentAttach2ComponentImp.Match> {
    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    public static Find_ComponentAttach2ComponentImp.Matcher on(final ViatraQueryEngine engine) {
      // check if matcher already exists
      Matcher matcher = engine.getExistingMatcher(querySpecification());
      if (matcher == null) {
          matcher = (Matcher)engine.getMatcher(querySpecification());
      }
      return matcher;
    }

    /**
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * @return an initialized matcher
     * @noreference This method is for internal matcher initialization by the framework, do not call it manually.
     * 
     */
    public static Find_ComponentAttach2ComponentImp.Matcher create() {
      return new Matcher();
    }

    private static final int POSITION_COMPINST = 0;

    private static final int POSITION_TRACE = 1;

    private static final int POSITION_COMPINST2ADD = 2;

    private static final int POSITION_COMP = 3;

    private static final Logger LOGGER = ViatraQueryLoggingUtil.getLogger(Find_ComponentAttach2ComponentImp.Matcher.class);

    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    private Matcher() {
      super(querySpecification());
    }

    /**
     * Returns the set of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pCompinst2add the fixed value of pattern parameter compinst2add, or null if not bound.
     * @param pComp the fixed value of pattern parameter comp, or null if not bound.
     * @return matches represented as a Match object.
     * 
     */
    public Collection<Find_ComponentAttach2ComponentImp.Match> getAllMatches(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentImplementation pComp) {
      return rawStreamAllMatches(new Object[]{pCompinst, pTrace, pCompinst2add, pComp}).collect(Collectors.toSet());
    }

    /**
     * Returns a stream of all matches of the pattern that conform to the given fixed values of some parameters.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pCompinst2add the fixed value of pattern parameter compinst2add, or null if not bound.
     * @param pComp the fixed value of pattern parameter comp, or null if not bound.
     * @return a stream of matches represented as a Match object.
     * 
     */
    public Stream<Find_ComponentAttach2ComponentImp.Match> streamAllMatches(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentImplementation pComp) {
      return rawStreamAllMatches(new Object[]{pCompinst, pTrace, pCompinst2add, pComp});
    }

    /**
     * Returns an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pCompinst2add the fixed value of pattern parameter compinst2add, or null if not bound.
     * @param pComp the fixed value of pattern parameter comp, or null if not bound.
     * @return a match represented as a Match object, or null if no match is found.
     * 
     */
    public Optional<Find_ComponentAttach2ComponentImp.Match> getOneArbitraryMatch(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentImplementation pComp) {
      return rawGetOneArbitraryMatch(new Object[]{pCompinst, pTrace, pCompinst2add, pComp});
    }

    /**
     * Indicates whether the given combination of specified pattern parameters constitute a valid pattern match,
     * under any possible substitution of the unspecified parameters (if any).
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pCompinst2add the fixed value of pattern parameter compinst2add, or null if not bound.
     * @param pComp the fixed value of pattern parameter comp, or null if not bound.
     * @return true if the input is a valid (partial) match of the pattern.
     * 
     */
    public boolean hasMatch(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentImplementation pComp) {
      return rawHasMatch(new Object[]{pCompinst, pTrace, pCompinst2add, pComp});
    }

    /**
     * Returns the number of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pCompinst2add the fixed value of pattern parameter compinst2add, or null if not bound.
     * @param pComp the fixed value of pattern parameter comp, or null if not bound.
     * @return the number of pattern matches found.
     * 
     */
    public int countMatches(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentImplementation pComp) {
      return rawCountMatches(new Object[]{pCompinst, pTrace, pCompinst2add, pComp});
    }

    /**
     * Executes the given processor on an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pCompinst2add the fixed value of pattern parameter compinst2add, or null if not bound.
     * @param pComp the fixed value of pattern parameter comp, or null if not bound.
     * @param processor the action that will process the selected match.
     * @return true if the pattern has at least one match with the given parameter values, false if the processor was not invoked
     * 
     */
    public boolean forOneArbitraryMatch(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentImplementation pComp, final Consumer<? super Find_ComponentAttach2ComponentImp.Match> processor) {
      return rawForOneArbitraryMatch(new Object[]{pCompinst, pTrace, pCompinst2add, pComp}, processor);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param pCompinst2add the fixed value of pattern parameter compinst2add, or null if not bound.
     * @param pComp the fixed value of pattern parameter comp, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public Find_ComponentAttach2ComponentImp.Match newMatch(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentImplementation pComp) {
      return Find_ComponentAttach2ComponentImp.Match.newMatch(pCompinst, pTrace, pCompinst2add, pComp);
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ComponentInstance> rawStreamAllValuesOfcompinst(final Object[] parameters) {
      return rawStreamAllValues(POSITION_COMPINST, parameters).map(ComponentInstance.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst() {
      return rawStreamAllValuesOfcompinst(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst() {
      return rawStreamAllValuesOfcompinst(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst(final Find_ComponentAttach2ComponentImp.Match partialMatch) {
      return rawStreamAllValuesOfcompinst(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst(final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentImplementation pComp) {
      return rawStreamAllValuesOfcompinst(new Object[]{null, pTrace, pCompinst2add, pComp});
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst(final Find_ComponentAttach2ComponentImp.Match partialMatch) {
      return rawStreamAllValuesOfcompinst(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst(final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add, final ComponentImplementation pComp) {
      return rawStreamAllValuesOfcompinst(new Object[]{null, pTrace, pCompinst2add, pComp}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<Aaxl2AaxlTrace> rawStreamAllValuesOftrace(final Object[] parameters) {
      return rawStreamAllValues(POSITION_TRACE, parameters).map(Aaxl2AaxlTrace.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Aaxl2AaxlTrace> getAllValuesOftrace() {
      return rawStreamAllValuesOftrace(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<Aaxl2AaxlTrace> streamAllValuesOftrace() {
      return rawStreamAllValuesOftrace(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<Aaxl2AaxlTrace> streamAllValuesOftrace(final Find_ComponentAttach2ComponentImp.Match partialMatch) {
      return rawStreamAllValuesOftrace(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<Aaxl2AaxlTrace> streamAllValuesOftrace(final ComponentInstance pCompinst, final ComponentInstance pCompinst2add, final ComponentImplementation pComp) {
      return rawStreamAllValuesOftrace(new Object[]{pCompinst, null, pCompinst2add, pComp});
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Aaxl2AaxlTrace> getAllValuesOftrace(final Find_ComponentAttach2ComponentImp.Match partialMatch) {
      return rawStreamAllValuesOftrace(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Aaxl2AaxlTrace> getAllValuesOftrace(final ComponentInstance pCompinst, final ComponentInstance pCompinst2add, final ComponentImplementation pComp) {
      return rawStreamAllValuesOftrace(new Object[]{pCompinst, null, pCompinst2add, pComp}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst2add.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ComponentInstance> rawStreamAllValuesOfcompinst2add(final Object[] parameters) {
      return rawStreamAllValues(POSITION_COMPINST2ADD, parameters).map(ComponentInstance.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for compinst2add.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst2add() {
      return rawStreamAllValuesOfcompinst2add(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst2add.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst2add() {
      return rawStreamAllValuesOfcompinst2add(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst2add.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst2add(final Find_ComponentAttach2ComponentImp.Match partialMatch) {
      return rawStreamAllValuesOfcompinst2add(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst2add.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst2add(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentImplementation pComp) {
      return rawStreamAllValuesOfcompinst2add(new Object[]{pCompinst, pTrace, null, pComp});
    }

    /**
     * Retrieve the set of values that occur in matches for compinst2add.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst2add(final Find_ComponentAttach2ComponentImp.Match partialMatch) {
      return rawStreamAllValuesOfcompinst2add(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst2add.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst2add(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentImplementation pComp) {
      return rawStreamAllValuesOfcompinst2add(new Object[]{pCompinst, pTrace, null, pComp}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for comp.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ComponentImplementation> rawStreamAllValuesOfcomp(final Object[] parameters) {
      return rawStreamAllValues(POSITION_COMP, parameters).map(ComponentImplementation.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for comp.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentImplementation> getAllValuesOfcomp() {
      return rawStreamAllValuesOfcomp(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for comp.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentImplementation> streamAllValuesOfcomp() {
      return rawStreamAllValuesOfcomp(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for comp.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentImplementation> streamAllValuesOfcomp(final Find_ComponentAttach2ComponentImp.Match partialMatch) {
      return rawStreamAllValuesOfcomp(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for comp.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentImplementation> streamAllValuesOfcomp(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add) {
      return rawStreamAllValuesOfcomp(new Object[]{pCompinst, pTrace, pCompinst2add, null});
    }

    /**
     * Retrieve the set of values that occur in matches for comp.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentImplementation> getAllValuesOfcomp(final Find_ComponentAttach2ComponentImp.Match partialMatch) {
      return rawStreamAllValuesOfcomp(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for comp.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentImplementation> getAllValuesOfcomp(final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final ComponentInstance pCompinst2add) {
      return rawStreamAllValuesOfcomp(new Object[]{pCompinst, pTrace, pCompinst2add, null}).collect(Collectors.toSet());
    }

    @Override
    protected Find_ComponentAttach2ComponentImp.Match tupleToMatch(final Tuple t) {
      try {
          return Find_ComponentAttach2ComponentImp.Match.newMatch((ComponentInstance) t.get(POSITION_COMPINST), (Aaxl2AaxlTrace) t.get(POSITION_TRACE), (ComponentInstance) t.get(POSITION_COMPINST2ADD), (ComponentImplementation) t.get(POSITION_COMP));
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in tuple not properly typed!",e);
          return null;
      }
    }

    @Override
    protected Find_ComponentAttach2ComponentImp.Match arrayToMatch(final Object[] match) {
      try {
          return Find_ComponentAttach2ComponentImp.Match.newMatch((ComponentInstance) match[POSITION_COMPINST], (Aaxl2AaxlTrace) match[POSITION_TRACE], (ComponentInstance) match[POSITION_COMPINST2ADD], (ComponentImplementation) match[POSITION_COMP]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    @Override
    protected Find_ComponentAttach2ComponentImp.Match arrayToMatchMutable(final Object[] match) {
      try {
          return Find_ComponentAttach2ComponentImp.Match.newMutableMatch((ComponentInstance) match[POSITION_COMPINST], (Aaxl2AaxlTrace) match[POSITION_TRACE], (ComponentInstance) match[POSITION_COMPINST2ADD], (ComponentImplementation) match[POSITION_COMP]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    /**
     * @return the singleton instance of the query specification of this pattern
     * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
     * 
     */
    public static IQuerySpecification<Find_ComponentAttach2ComponentImp.Matcher> querySpecification() {
      return Find_ComponentAttach2ComponentImp.instance();
    }
  }

  private Find_ComponentAttach2ComponentImp() {
    super(GeneratedPQuery.INSTANCE);
  }

  /**
   * @return the singleton instance of the query specification
   * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
   * 
   */
  public static Find_ComponentAttach2ComponentImp instance() {
    try{
        return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
        throw processInitializerError(err);
    }
  }

  @Override
  protected Find_ComponentAttach2ComponentImp.Matcher instantiate(final ViatraQueryEngine engine) {
    return Find_ComponentAttach2ComponentImp.Matcher.on(engine);
  }

  @Override
  public Find_ComponentAttach2ComponentImp.Matcher instantiate() {
    return Find_ComponentAttach2ComponentImp.Matcher.create();
  }

  @Override
  public Find_ComponentAttach2ComponentImp.Match newEmptyMatch() {
    return Find_ComponentAttach2ComponentImp.Match.newEmptyMatch();
  }

  @Override
  public Find_ComponentAttach2ComponentImp.Match newMatch(final Object... parameters) {
    return Find_ComponentAttach2ComponentImp.Match.newMatch((org.osate.aadl2.instance.ComponentInstance) parameters[0], (fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace) parameters[1], (org.osate.aadl2.instance.ComponentInstance) parameters[2], (org.osate.aadl2.ComponentImplementation) parameters[3]);
  }

  /**
   * Inner class allowing the singleton instance of {@link Find_ComponentAttach2ComponentImp} to be created 
   *     <b>not</b> at the class load time of the outer class, 
   *     but rather at the first call to {@link Find_ComponentAttach2ComponentImp#instance()}.
   * 
   * <p> This workaround is required e.g. to support recursion.
   * 
   */
  private static class LazyHolder {
    private static final Find_ComponentAttach2ComponentImp INSTANCE = new Find_ComponentAttach2ComponentImp();

    /**
     * Statically initializes the query specification <b>after</b> the field {@link #INSTANCE} is assigned.
     * This initialization order is required to support indirect recursion.
     * 
     * <p> The static initializer is defined using a helper field to work around limitations of the code generator.
     * 
     */
    private static final Object STATIC_INITIALIZER = ensureInitialized();

    public static Object ensureInitialized() {
      INSTANCE.ensureInitializedInternal();
      return null;
    }
  }

  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private static final Find_ComponentAttach2ComponentImp.GeneratedPQuery INSTANCE = new GeneratedPQuery();

    private final PParameter parameter_compinst = new PParameter("compinst", "org.osate.aadl2.instance.ComponentInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ComponentInstance")), PParameterDirection.INOUT);

    private final PParameter parameter_trace = new PParameter("trace", "fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace")), PParameterDirection.INOUT);

    private final PParameter parameter_compinst2add = new PParameter("compinst2add", "org.osate.aadl2.instance.ComponentInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ComponentInstance")), PParameterDirection.INOUT);

    private final PParameter parameter_comp = new PParameter("comp", "org.osate.aadl2.ComponentImplementation", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0", "ComponentImplementation")), PParameterDirection.INOUT);

    private final List<PParameter> parameters = Arrays.asList(parameter_compinst, parameter_trace, parameter_compinst2add, parameter_comp);

    private GeneratedPQuery() {
      super(PVisibility.PUBLIC);
    }

    @Override
    public String getFullyQualifiedName() {
      return "fr.mem4csd.osatedim.viatra.queries.find_ComponentAttach2ComponentImp";
    }

    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("compinst","trace","compinst2add","comp");
    }

    @Override
    public List<PParameter> getParameters() {
      return parameters;
    }

    @Override
    public Set<PBody> doGetContainedBodies() {
      setEvaluationHints(new QueryEvaluationHint(null, QueryEvaluationHint.BackendRequirement.UNSPECIFIED));
      Set<PBody> bodies = new LinkedHashSet<>();
      {
          PBody body = new PBody(this);
          PVariable var_compinst = body.getOrCreateVariableByName("compinst");
          PVariable var_trace = body.getOrCreateVariableByName("trace");
          PVariable var_compinst2add = body.getOrCreateVariableByName("compinst2add");
          PVariable var_comp = body.getOrCreateVariableByName("comp");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          PVariable var___1_ = body.getOrCreateVariableByName("_<1>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_trace), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst2add), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_comp), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ComponentImplementation")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_compinst, parameter_compinst),
             new ExportedParameter(body, var_trace, parameter_trace),
             new ExportedParameter(body, var_compinst2add, parameter_compinst2add),
             new ExportedParameter(body, var_comp, parameter_comp)
          ));
          // 	find is_in_trace(_,trace,compinst,_)
          new PositivePatternCall(body, Tuples.flatTupleOf(var___0_, var_trace, var_compinst, var___1_), Is_in_trace.instance().getInternalQueryRepresentation());
          // 	Aaxl2AaxlTrace.objectsToAttach(trace,compinst2add)
          new TypeConstraint(body, Tuples.flatTupleOf(var_trace), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_trace, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace", "objectsToAttach")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Element")));
          new Equality(body, var__virtual_0_, var_compinst2add);
          // 	ComponentInstance.classifier(compinst,comp)
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance", "classifier")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ComponentClassifier")));
          new Equality(body, var__virtual_1_, var_comp);
          bodies.add(body);
      }
      return bodies;
    }
  }
}
