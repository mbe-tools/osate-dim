/**
 * Generated from platform:/resource/fr.mem4csd.osatedim/src/fr/mem4csd/osatedim/viatra/queries/DIMQueriesOutplace.vql
 */
package fr.mem4csd.osatedim.viatra.queries;

import fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace;
import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.viatra.query.runtime.api.IPatternMatch;
import org.eclipse.viatra.query.runtime.api.IQuerySpecification;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.viatra.query.runtime.api.impl.BaseMatcher;
import org.eclipse.viatra.query.runtime.api.impl.BasePatternMatch;
import org.eclipse.viatra.query.runtime.emf.types.EClassTransitiveInstancesKey;
import org.eclipse.viatra.query.runtime.emf.types.EStructuralFeatureInstancesKey;
import org.eclipse.viatra.query.runtime.matchers.backend.QueryEvaluationHint;
import org.eclipse.viatra.query.runtime.matchers.psystem.PBody;
import org.eclipse.viatra.query.runtime.matchers.psystem.PVariable;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.Equality;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.TypeConstraint;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameterDirection;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PVisibility;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuple;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuples;
import org.eclipse.viatra.query.runtime.util.ViatraQueryLoggingUtil;
import org.osate.aadl2.BasicPropertyAssociation;
import org.osate.aadl2.ListValue;
import org.osate.aadl2.ModalPropertyValue;
import org.osate.aadl2.RecordValue;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.InstanceReferenceValue;
import org.osate.aadl2.instance.PropertyAssociationInstance;

/**
 * A pattern-specific query specification that can instantiate Matcher in a type-safe way.
 * 
 * <p>Original source:
 *         <code><pre>
 *         // This pattern finds the reference-based properties2Attach for a specific component instance
 *         pattern find_PropertyRefAttach2Component(propvalue : InstanceReferenceValue,
 *         										 prop : BasicPropertyAssociation,
 *         										 proplistelem : RecordValue,
 *         										 proplist : ListValue,
 *         									  	 modalprop : ModalPropertyValue,
 *         							  			 propinst : PropertyAssociationInstance,
 *         							  			 compinst : ComponentInstance, 
 *         							  			 trace : Aaxl2AaxlTrace)
 *         {
 *         	Aaxl2AaxlTrace.leftInstance(trace,compinst);
 *         	Aaxl2AaxlTrace.objectsToAttach(trace, propinst);
 *         	PropertyAssociationInstance.ownedValue(propinst,modalprop);
 *         	ModalPropertyValue.ownedValue(modalprop,proplist);
 *         	ListValue.ownedListElement(proplist,proplistelem);
 *         	RecordValue.ownedFieldValue(proplistelem,prop);
 *         	BasicPropertyAssociation.ownedValue(prop,propvalue);
 *         }
 * </pre></code>
 * 
 * @see Matcher
 * @see Match
 * 
 */
@SuppressWarnings("all")
public final class Find_PropertyRefAttach2Component extends BaseGeneratedEMFQuerySpecification<Find_PropertyRefAttach2Component.Matcher> {
  /**
   * Pattern-specific match representation of the fr.mem4csd.osatedim.viatra.queries.find_PropertyRefAttach2Component pattern,
   * to be used in conjunction with {@link Matcher}.
   * 
   * <p>Class fields correspond to parameters of the pattern. Fields with value null are considered unassigned.
   * Each instance is a (possibly partial) substitution of pattern parameters,
   * usable to represent a match of the pattern in the result of a query,
   * or to specify the bound (fixed) input parameters when issuing a query.
   * 
   * @see Matcher
   * 
   */
  public static abstract class Match extends BasePatternMatch {
    private InstanceReferenceValue fPropvalue;

    private BasicPropertyAssociation fProp;

    private RecordValue fProplistelem;

    private ListValue fProplist;

    private ModalPropertyValue fModalprop;

    private PropertyAssociationInstance fPropinst;

    private ComponentInstance fCompinst;

    private Aaxl2AaxlTrace fTrace;

    private static List<String> parameterNames = makeImmutableList("propvalue", "prop", "proplistelem", "proplist", "modalprop", "propinst", "compinst", "trace");

    private Match(final InstanceReferenceValue pPropvalue, final BasicPropertyAssociation pProp, final RecordValue pProplistelem, final ListValue pProplist, final ModalPropertyValue pModalprop, final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      this.fPropvalue = pPropvalue;
      this.fProp = pProp;
      this.fProplistelem = pProplistelem;
      this.fProplist = pProplist;
      this.fModalprop = pModalprop;
      this.fPropinst = pPropinst;
      this.fCompinst = pCompinst;
      this.fTrace = pTrace;
    }

    @Override
    public Object get(final String parameterName) {
      switch(parameterName) {
          case "propvalue": return this.fPropvalue;
          case "prop": return this.fProp;
          case "proplistelem": return this.fProplistelem;
          case "proplist": return this.fProplist;
          case "modalprop": return this.fModalprop;
          case "propinst": return this.fPropinst;
          case "compinst": return this.fCompinst;
          case "trace": return this.fTrace;
          default: return null;
      }
    }

    @Override
    public Object get(final int index) {
      switch(index) {
          case 0: return this.fPropvalue;
          case 1: return this.fProp;
          case 2: return this.fProplistelem;
          case 3: return this.fProplist;
          case 4: return this.fModalprop;
          case 5: return this.fPropinst;
          case 6: return this.fCompinst;
          case 7: return this.fTrace;
          default: return null;
      }
    }

    public InstanceReferenceValue getPropvalue() {
      return this.fPropvalue;
    }

    public BasicPropertyAssociation getProp() {
      return this.fProp;
    }

    public RecordValue getProplistelem() {
      return this.fProplistelem;
    }

    public ListValue getProplist() {
      return this.fProplist;
    }

    public ModalPropertyValue getModalprop() {
      return this.fModalprop;
    }

    public PropertyAssociationInstance getPropinst() {
      return this.fPropinst;
    }

    public ComponentInstance getCompinst() {
      return this.fCompinst;
    }

    public Aaxl2AaxlTrace getTrace() {
      return this.fTrace;
    }

    @Override
    public boolean set(final String parameterName, final Object newValue) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      if ("propvalue".equals(parameterName) ) {
          this.fPropvalue = (InstanceReferenceValue) newValue;
          return true;
      }
      if ("prop".equals(parameterName) ) {
          this.fProp = (BasicPropertyAssociation) newValue;
          return true;
      }
      if ("proplistelem".equals(parameterName) ) {
          this.fProplistelem = (RecordValue) newValue;
          return true;
      }
      if ("proplist".equals(parameterName) ) {
          this.fProplist = (ListValue) newValue;
          return true;
      }
      if ("modalprop".equals(parameterName) ) {
          this.fModalprop = (ModalPropertyValue) newValue;
          return true;
      }
      if ("propinst".equals(parameterName) ) {
          this.fPropinst = (PropertyAssociationInstance) newValue;
          return true;
      }
      if ("compinst".equals(parameterName) ) {
          this.fCompinst = (ComponentInstance) newValue;
          return true;
      }
      if ("trace".equals(parameterName) ) {
          this.fTrace = (Aaxl2AaxlTrace) newValue;
          return true;
      }
      return false;
    }

    public void setPropvalue(final InstanceReferenceValue pPropvalue) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fPropvalue = pPropvalue;
    }

    public void setProp(final BasicPropertyAssociation pProp) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fProp = pProp;
    }

    public void setProplistelem(final RecordValue pProplistelem) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fProplistelem = pProplistelem;
    }

    public void setProplist(final ListValue pProplist) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fProplist = pProplist;
    }

    public void setModalprop(final ModalPropertyValue pModalprop) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fModalprop = pModalprop;
    }

    public void setPropinst(final PropertyAssociationInstance pPropinst) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fPropinst = pPropinst;
    }

    public void setCompinst(final ComponentInstance pCompinst) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fCompinst = pCompinst;
    }

    public void setTrace(final Aaxl2AaxlTrace pTrace) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fTrace = pTrace;
    }

    @Override
    public String patternName() {
      return "fr.mem4csd.osatedim.viatra.queries.find_PropertyRefAttach2Component";
    }

    @Override
    public List<String> parameterNames() {
      return Find_PropertyRefAttach2Component.Match.parameterNames;
    }

    @Override
    public Object[] toArray() {
      return new Object[]{fPropvalue, fProp, fProplistelem, fProplist, fModalprop, fPropinst, fCompinst, fTrace};
    }

    @Override
    public Find_PropertyRefAttach2Component.Match toImmutable() {
      return isMutable() ? newMatch(fPropvalue, fProp, fProplistelem, fProplist, fModalprop, fPropinst, fCompinst, fTrace) : this;
    }

    @Override
    public String prettyPrint() {
      StringBuilder result = new StringBuilder();
      result.append("\"propvalue\"=" + prettyPrintValue(fPropvalue) + ", ");
      result.append("\"prop\"=" + prettyPrintValue(fProp) + ", ");
      result.append("\"proplistelem\"=" + prettyPrintValue(fProplistelem) + ", ");
      result.append("\"proplist\"=" + prettyPrintValue(fProplist) + ", ");
      result.append("\"modalprop\"=" + prettyPrintValue(fModalprop) + ", ");
      result.append("\"propinst\"=" + prettyPrintValue(fPropinst) + ", ");
      result.append("\"compinst\"=" + prettyPrintValue(fCompinst) + ", ");
      result.append("\"trace\"=" + prettyPrintValue(fTrace));
      return result.toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(fPropvalue, fProp, fProplistelem, fProplist, fModalprop, fPropinst, fCompinst, fTrace);
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj)
          return true;
      if (obj == null) {
          return false;
      }
      if ((obj instanceof Find_PropertyRefAttach2Component.Match)) {
          Find_PropertyRefAttach2Component.Match other = (Find_PropertyRefAttach2Component.Match) obj;
          return Objects.equals(fPropvalue, other.fPropvalue) && Objects.equals(fProp, other.fProp) && Objects.equals(fProplistelem, other.fProplistelem) && Objects.equals(fProplist, other.fProplist) && Objects.equals(fModalprop, other.fModalprop) && Objects.equals(fPropinst, other.fPropinst) && Objects.equals(fCompinst, other.fCompinst) && Objects.equals(fTrace, other.fTrace);
      } else {
          // this should be infrequent
          if (!(obj instanceof IPatternMatch)) {
              return false;
          }
          IPatternMatch otherSig  = (IPatternMatch) obj;
          return Objects.equals(specification(), otherSig.specification()) && Arrays.deepEquals(toArray(), otherSig.toArray());
      }
    }

    @Override
    public Find_PropertyRefAttach2Component specification() {
      return Find_PropertyRefAttach2Component.instance();
    }

    /**
     * Returns an empty, mutable match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @return the empty match.
     * 
     */
    public static Find_PropertyRefAttach2Component.Match newEmptyMatch() {
      return new Mutable(null, null, null, null, null, null, null, null);
    }

    /**
     * Returns a mutable (partial) match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @param pPropvalue the fixed value of pattern parameter propvalue, or null if not bound.
     * @param pProp the fixed value of pattern parameter prop, or null if not bound.
     * @param pProplistelem the fixed value of pattern parameter proplistelem, or null if not bound.
     * @param pProplist the fixed value of pattern parameter proplist, or null if not bound.
     * @param pModalprop the fixed value of pattern parameter modalprop, or null if not bound.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @return the new, mutable (partial) match object.
     * 
     */
    public static Find_PropertyRefAttach2Component.Match newMutableMatch(final InstanceReferenceValue pPropvalue, final BasicPropertyAssociation pProp, final RecordValue pProplistelem, final ListValue pProplist, final ModalPropertyValue pModalprop, final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      return new Mutable(pPropvalue, pProp, pProplistelem, pProplist, pModalprop, pPropinst, pCompinst, pTrace);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pPropvalue the fixed value of pattern parameter propvalue, or null if not bound.
     * @param pProp the fixed value of pattern parameter prop, or null if not bound.
     * @param pProplistelem the fixed value of pattern parameter proplistelem, or null if not bound.
     * @param pProplist the fixed value of pattern parameter proplist, or null if not bound.
     * @param pModalprop the fixed value of pattern parameter modalprop, or null if not bound.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public static Find_PropertyRefAttach2Component.Match newMatch(final InstanceReferenceValue pPropvalue, final BasicPropertyAssociation pProp, final RecordValue pProplistelem, final ListValue pProplist, final ModalPropertyValue pModalprop, final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      return new Immutable(pPropvalue, pProp, pProplistelem, pProplist, pModalprop, pPropinst, pCompinst, pTrace);
    }

    private static final class Mutable extends Find_PropertyRefAttach2Component.Match {
      Mutable(final InstanceReferenceValue pPropvalue, final BasicPropertyAssociation pProp, final RecordValue pProplistelem, final ListValue pProplist, final ModalPropertyValue pModalprop, final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
        super(pPropvalue, pProp, pProplistelem, pProplist, pModalprop, pPropinst, pCompinst, pTrace);
      }

      @Override
      public boolean isMutable() {
        return true;
      }
    }

    private static final class Immutable extends Find_PropertyRefAttach2Component.Match {
      Immutable(final InstanceReferenceValue pPropvalue, final BasicPropertyAssociation pProp, final RecordValue pProplistelem, final ListValue pProplist, final ModalPropertyValue pModalprop, final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
        super(pPropvalue, pProp, pProplistelem, pProplist, pModalprop, pPropinst, pCompinst, pTrace);
      }

      @Override
      public boolean isMutable() {
        return false;
      }
    }
  }

  /**
   * Generated pattern matcher API of the fr.mem4csd.osatedim.viatra.queries.find_PropertyRefAttach2Component pattern,
   * providing pattern-specific query methods.
   * 
   * <p>Use the pattern matcher on a given model via {@link #on(ViatraQueryEngine)},
   * e.g. in conjunction with {@link ViatraQueryEngine#on(QueryScope)}.
   * 
   * <p>Matches of the pattern will be represented as {@link Match}.
   * 
   * <p>Original source:
   * <code><pre>
   * // This pattern finds the reference-based properties2Attach for a specific component instance
   * pattern find_PropertyRefAttach2Component(propvalue : InstanceReferenceValue,
   * 										 prop : BasicPropertyAssociation,
   * 										 proplistelem : RecordValue,
   * 										 proplist : ListValue,
   * 									  	 modalprop : ModalPropertyValue,
   * 							  			 propinst : PropertyAssociationInstance,
   * 							  			 compinst : ComponentInstance, 
   * 							  			 trace : Aaxl2AaxlTrace)
   * {
   * 	Aaxl2AaxlTrace.leftInstance(trace,compinst);
   * 	Aaxl2AaxlTrace.objectsToAttach(trace, propinst);
   * 	PropertyAssociationInstance.ownedValue(propinst,modalprop);
   * 	ModalPropertyValue.ownedValue(modalprop,proplist);
   * 	ListValue.ownedListElement(proplist,proplistelem);
   * 	RecordValue.ownedFieldValue(proplistelem,prop);
   * 	BasicPropertyAssociation.ownedValue(prop,propvalue);
   * }
   * </pre></code>
   * 
   * @see Match
   * @see Find_PropertyRefAttach2Component
   * 
   */
  public static class Matcher extends BaseMatcher<Find_PropertyRefAttach2Component.Match> {
    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    public static Find_PropertyRefAttach2Component.Matcher on(final ViatraQueryEngine engine) {
      // check if matcher already exists
      Matcher matcher = engine.getExistingMatcher(querySpecification());
      if (matcher == null) {
          matcher = (Matcher)engine.getMatcher(querySpecification());
      }
      return matcher;
    }

    /**
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * @return an initialized matcher
     * @noreference This method is for internal matcher initialization by the framework, do not call it manually.
     * 
     */
    public static Find_PropertyRefAttach2Component.Matcher create() {
      return new Matcher();
    }

    private static final int POSITION_PROPVALUE = 0;

    private static final int POSITION_PROP = 1;

    private static final int POSITION_PROPLISTELEM = 2;

    private static final int POSITION_PROPLIST = 3;

    private static final int POSITION_MODALPROP = 4;

    private static final int POSITION_PROPINST = 5;

    private static final int POSITION_COMPINST = 6;

    private static final int POSITION_TRACE = 7;

    private static final Logger LOGGER = ViatraQueryLoggingUtil.getLogger(Find_PropertyRefAttach2Component.Matcher.class);

    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    private Matcher() {
      super(querySpecification());
    }

    /**
     * Returns the set of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pPropvalue the fixed value of pattern parameter propvalue, or null if not bound.
     * @param pProp the fixed value of pattern parameter prop, or null if not bound.
     * @param pProplistelem the fixed value of pattern parameter proplistelem, or null if not bound.
     * @param pProplist the fixed value of pattern parameter proplist, or null if not bound.
     * @param pModalprop the fixed value of pattern parameter modalprop, or null if not bound.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @return matches represented as a Match object.
     * 
     */
    public Collection<Find_PropertyRefAttach2Component.Match> getAllMatches(final InstanceReferenceValue pPropvalue, final BasicPropertyAssociation pProp, final RecordValue pProplistelem, final ListValue pProplist, final ModalPropertyValue pModalprop, final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      return rawStreamAllMatches(new Object[]{pPropvalue, pProp, pProplistelem, pProplist, pModalprop, pPropinst, pCompinst, pTrace}).collect(Collectors.toSet());
    }

    /**
     * Returns a stream of all matches of the pattern that conform to the given fixed values of some parameters.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     * @param pPropvalue the fixed value of pattern parameter propvalue, or null if not bound.
     * @param pProp the fixed value of pattern parameter prop, or null if not bound.
     * @param pProplistelem the fixed value of pattern parameter proplistelem, or null if not bound.
     * @param pProplist the fixed value of pattern parameter proplist, or null if not bound.
     * @param pModalprop the fixed value of pattern parameter modalprop, or null if not bound.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @return a stream of matches represented as a Match object.
     * 
     */
    public Stream<Find_PropertyRefAttach2Component.Match> streamAllMatches(final InstanceReferenceValue pPropvalue, final BasicPropertyAssociation pProp, final RecordValue pProplistelem, final ListValue pProplist, final ModalPropertyValue pModalprop, final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      return rawStreamAllMatches(new Object[]{pPropvalue, pProp, pProplistelem, pProplist, pModalprop, pPropinst, pCompinst, pTrace});
    }

    /**
     * Returns an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pPropvalue the fixed value of pattern parameter propvalue, or null if not bound.
     * @param pProp the fixed value of pattern parameter prop, or null if not bound.
     * @param pProplistelem the fixed value of pattern parameter proplistelem, or null if not bound.
     * @param pProplist the fixed value of pattern parameter proplist, or null if not bound.
     * @param pModalprop the fixed value of pattern parameter modalprop, or null if not bound.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @return a match represented as a Match object, or null if no match is found.
     * 
     */
    public Optional<Find_PropertyRefAttach2Component.Match> getOneArbitraryMatch(final InstanceReferenceValue pPropvalue, final BasicPropertyAssociation pProp, final RecordValue pProplistelem, final ListValue pProplist, final ModalPropertyValue pModalprop, final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      return rawGetOneArbitraryMatch(new Object[]{pPropvalue, pProp, pProplistelem, pProplist, pModalprop, pPropinst, pCompinst, pTrace});
    }

    /**
     * Indicates whether the given combination of specified pattern parameters constitute a valid pattern match,
     * under any possible substitution of the unspecified parameters (if any).
     * @param pPropvalue the fixed value of pattern parameter propvalue, or null if not bound.
     * @param pProp the fixed value of pattern parameter prop, or null if not bound.
     * @param pProplistelem the fixed value of pattern parameter proplistelem, or null if not bound.
     * @param pProplist the fixed value of pattern parameter proplist, or null if not bound.
     * @param pModalprop the fixed value of pattern parameter modalprop, or null if not bound.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @return true if the input is a valid (partial) match of the pattern.
     * 
     */
    public boolean hasMatch(final InstanceReferenceValue pPropvalue, final BasicPropertyAssociation pProp, final RecordValue pProplistelem, final ListValue pProplist, final ModalPropertyValue pModalprop, final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      return rawHasMatch(new Object[]{pPropvalue, pProp, pProplistelem, pProplist, pModalprop, pPropinst, pCompinst, pTrace});
    }

    /**
     * Returns the number of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pPropvalue the fixed value of pattern parameter propvalue, or null if not bound.
     * @param pProp the fixed value of pattern parameter prop, or null if not bound.
     * @param pProplistelem the fixed value of pattern parameter proplistelem, or null if not bound.
     * @param pProplist the fixed value of pattern parameter proplist, or null if not bound.
     * @param pModalprop the fixed value of pattern parameter modalprop, or null if not bound.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @return the number of pattern matches found.
     * 
     */
    public int countMatches(final InstanceReferenceValue pPropvalue, final BasicPropertyAssociation pProp, final RecordValue pProplistelem, final ListValue pProplist, final ModalPropertyValue pModalprop, final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      return rawCountMatches(new Object[]{pPropvalue, pProp, pProplistelem, pProplist, pModalprop, pPropinst, pCompinst, pTrace});
    }

    /**
     * Executes the given processor on an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pPropvalue the fixed value of pattern parameter propvalue, or null if not bound.
     * @param pProp the fixed value of pattern parameter prop, or null if not bound.
     * @param pProplistelem the fixed value of pattern parameter proplistelem, or null if not bound.
     * @param pProplist the fixed value of pattern parameter proplist, or null if not bound.
     * @param pModalprop the fixed value of pattern parameter modalprop, or null if not bound.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @param processor the action that will process the selected match.
     * @return true if the pattern has at least one match with the given parameter values, false if the processor was not invoked
     * 
     */
    public boolean forOneArbitraryMatch(final InstanceReferenceValue pPropvalue, final BasicPropertyAssociation pProp, final RecordValue pProplistelem, final ListValue pProplist, final ModalPropertyValue pModalprop, final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace, final Consumer<? super Find_PropertyRefAttach2Component.Match> processor) {
      return rawForOneArbitraryMatch(new Object[]{pPropvalue, pProp, pProplistelem, pProplist, pModalprop, pPropinst, pCompinst, pTrace}, processor);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pPropvalue the fixed value of pattern parameter propvalue, or null if not bound.
     * @param pProp the fixed value of pattern parameter prop, or null if not bound.
     * @param pProplistelem the fixed value of pattern parameter proplistelem, or null if not bound.
     * @param pProplist the fixed value of pattern parameter proplist, or null if not bound.
     * @param pModalprop the fixed value of pattern parameter modalprop, or null if not bound.
     * @param pPropinst the fixed value of pattern parameter propinst, or null if not bound.
     * @param pCompinst the fixed value of pattern parameter compinst, or null if not bound.
     * @param pTrace the fixed value of pattern parameter trace, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public Find_PropertyRefAttach2Component.Match newMatch(final InstanceReferenceValue pPropvalue, final BasicPropertyAssociation pProp, final RecordValue pProplistelem, final ListValue pProplist, final ModalPropertyValue pModalprop, final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      return Find_PropertyRefAttach2Component.Match.newMatch(pPropvalue, pProp, pProplistelem, pProplist, pModalprop, pPropinst, pCompinst, pTrace);
    }

    /**
     * Retrieve the set of values that occur in matches for propvalue.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<InstanceReferenceValue> rawStreamAllValuesOfpropvalue(final Object[] parameters) {
      return rawStreamAllValues(POSITION_PROPVALUE, parameters).map(InstanceReferenceValue.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for propvalue.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<InstanceReferenceValue> getAllValuesOfpropvalue() {
      return rawStreamAllValuesOfpropvalue(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for propvalue.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<InstanceReferenceValue> streamAllValuesOfpropvalue() {
      return rawStreamAllValuesOfpropvalue(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for propvalue.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<InstanceReferenceValue> streamAllValuesOfpropvalue(final Find_PropertyRefAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfpropvalue(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for propvalue.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<InstanceReferenceValue> streamAllValuesOfpropvalue(final BasicPropertyAssociation pProp, final RecordValue pProplistelem, final ListValue pProplist, final ModalPropertyValue pModalprop, final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      return rawStreamAllValuesOfpropvalue(new Object[]{null, pProp, pProplistelem, pProplist, pModalprop, pPropinst, pCompinst, pTrace});
    }

    /**
     * Retrieve the set of values that occur in matches for propvalue.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<InstanceReferenceValue> getAllValuesOfpropvalue(final Find_PropertyRefAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfpropvalue(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for propvalue.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<InstanceReferenceValue> getAllValuesOfpropvalue(final BasicPropertyAssociation pProp, final RecordValue pProplistelem, final ListValue pProplist, final ModalPropertyValue pModalprop, final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      return rawStreamAllValuesOfpropvalue(new Object[]{null, pProp, pProplistelem, pProplist, pModalprop, pPropinst, pCompinst, pTrace}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for prop.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<BasicPropertyAssociation> rawStreamAllValuesOfprop(final Object[] parameters) {
      return rawStreamAllValues(POSITION_PROP, parameters).map(BasicPropertyAssociation.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for prop.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<BasicPropertyAssociation> getAllValuesOfprop() {
      return rawStreamAllValuesOfprop(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for prop.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<BasicPropertyAssociation> streamAllValuesOfprop() {
      return rawStreamAllValuesOfprop(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for prop.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<BasicPropertyAssociation> streamAllValuesOfprop(final Find_PropertyRefAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfprop(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for prop.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<BasicPropertyAssociation> streamAllValuesOfprop(final InstanceReferenceValue pPropvalue, final RecordValue pProplistelem, final ListValue pProplist, final ModalPropertyValue pModalprop, final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      return rawStreamAllValuesOfprop(new Object[]{pPropvalue, null, pProplistelem, pProplist, pModalprop, pPropinst, pCompinst, pTrace});
    }

    /**
     * Retrieve the set of values that occur in matches for prop.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<BasicPropertyAssociation> getAllValuesOfprop(final Find_PropertyRefAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfprop(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for prop.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<BasicPropertyAssociation> getAllValuesOfprop(final InstanceReferenceValue pPropvalue, final RecordValue pProplistelem, final ListValue pProplist, final ModalPropertyValue pModalprop, final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      return rawStreamAllValuesOfprop(new Object[]{pPropvalue, null, pProplistelem, pProplist, pModalprop, pPropinst, pCompinst, pTrace}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for proplistelem.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<RecordValue> rawStreamAllValuesOfproplistelem(final Object[] parameters) {
      return rawStreamAllValues(POSITION_PROPLISTELEM, parameters).map(RecordValue.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for proplistelem.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<RecordValue> getAllValuesOfproplistelem() {
      return rawStreamAllValuesOfproplistelem(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for proplistelem.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<RecordValue> streamAllValuesOfproplistelem() {
      return rawStreamAllValuesOfproplistelem(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for proplistelem.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<RecordValue> streamAllValuesOfproplistelem(final Find_PropertyRefAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfproplistelem(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for proplistelem.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<RecordValue> streamAllValuesOfproplistelem(final InstanceReferenceValue pPropvalue, final BasicPropertyAssociation pProp, final ListValue pProplist, final ModalPropertyValue pModalprop, final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      return rawStreamAllValuesOfproplistelem(new Object[]{pPropvalue, pProp, null, pProplist, pModalprop, pPropinst, pCompinst, pTrace});
    }

    /**
     * Retrieve the set of values that occur in matches for proplistelem.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<RecordValue> getAllValuesOfproplistelem(final Find_PropertyRefAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfproplistelem(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for proplistelem.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<RecordValue> getAllValuesOfproplistelem(final InstanceReferenceValue pPropvalue, final BasicPropertyAssociation pProp, final ListValue pProplist, final ModalPropertyValue pModalprop, final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      return rawStreamAllValuesOfproplistelem(new Object[]{pPropvalue, pProp, null, pProplist, pModalprop, pPropinst, pCompinst, pTrace}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for proplist.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ListValue> rawStreamAllValuesOfproplist(final Object[] parameters) {
      return rawStreamAllValues(POSITION_PROPLIST, parameters).map(ListValue.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for proplist.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ListValue> getAllValuesOfproplist() {
      return rawStreamAllValuesOfproplist(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for proplist.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ListValue> streamAllValuesOfproplist() {
      return rawStreamAllValuesOfproplist(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for proplist.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ListValue> streamAllValuesOfproplist(final Find_PropertyRefAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfproplist(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for proplist.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ListValue> streamAllValuesOfproplist(final InstanceReferenceValue pPropvalue, final BasicPropertyAssociation pProp, final RecordValue pProplistelem, final ModalPropertyValue pModalprop, final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      return rawStreamAllValuesOfproplist(new Object[]{pPropvalue, pProp, pProplistelem, null, pModalprop, pPropinst, pCompinst, pTrace});
    }

    /**
     * Retrieve the set of values that occur in matches for proplist.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ListValue> getAllValuesOfproplist(final Find_PropertyRefAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfproplist(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for proplist.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ListValue> getAllValuesOfproplist(final InstanceReferenceValue pPropvalue, final BasicPropertyAssociation pProp, final RecordValue pProplistelem, final ModalPropertyValue pModalprop, final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      return rawStreamAllValuesOfproplist(new Object[]{pPropvalue, pProp, pProplistelem, null, pModalprop, pPropinst, pCompinst, pTrace}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for modalprop.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ModalPropertyValue> rawStreamAllValuesOfmodalprop(final Object[] parameters) {
      return rawStreamAllValues(POSITION_MODALPROP, parameters).map(ModalPropertyValue.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for modalprop.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ModalPropertyValue> getAllValuesOfmodalprop() {
      return rawStreamAllValuesOfmodalprop(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for modalprop.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ModalPropertyValue> streamAllValuesOfmodalprop() {
      return rawStreamAllValuesOfmodalprop(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for modalprop.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ModalPropertyValue> streamAllValuesOfmodalprop(final Find_PropertyRefAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfmodalprop(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for modalprop.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ModalPropertyValue> streamAllValuesOfmodalprop(final InstanceReferenceValue pPropvalue, final BasicPropertyAssociation pProp, final RecordValue pProplistelem, final ListValue pProplist, final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      return rawStreamAllValuesOfmodalprop(new Object[]{pPropvalue, pProp, pProplistelem, pProplist, null, pPropinst, pCompinst, pTrace});
    }

    /**
     * Retrieve the set of values that occur in matches for modalprop.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ModalPropertyValue> getAllValuesOfmodalprop(final Find_PropertyRefAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfmodalprop(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for modalprop.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ModalPropertyValue> getAllValuesOfmodalprop(final InstanceReferenceValue pPropvalue, final BasicPropertyAssociation pProp, final RecordValue pProplistelem, final ListValue pProplist, final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      return rawStreamAllValuesOfmodalprop(new Object[]{pPropvalue, pProp, pProplistelem, pProplist, null, pPropinst, pCompinst, pTrace}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for propinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<PropertyAssociationInstance> rawStreamAllValuesOfpropinst(final Object[] parameters) {
      return rawStreamAllValues(POSITION_PROPINST, parameters).map(PropertyAssociationInstance.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for propinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<PropertyAssociationInstance> getAllValuesOfpropinst() {
      return rawStreamAllValuesOfpropinst(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for propinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<PropertyAssociationInstance> streamAllValuesOfpropinst() {
      return rawStreamAllValuesOfpropinst(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for propinst.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<PropertyAssociationInstance> streamAllValuesOfpropinst(final Find_PropertyRefAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfpropinst(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for propinst.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<PropertyAssociationInstance> streamAllValuesOfpropinst(final InstanceReferenceValue pPropvalue, final BasicPropertyAssociation pProp, final RecordValue pProplistelem, final ListValue pProplist, final ModalPropertyValue pModalprop, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      return rawStreamAllValuesOfpropinst(new Object[]{pPropvalue, pProp, pProplistelem, pProplist, pModalprop, null, pCompinst, pTrace});
    }

    /**
     * Retrieve the set of values that occur in matches for propinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<PropertyAssociationInstance> getAllValuesOfpropinst(final Find_PropertyRefAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfpropinst(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for propinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<PropertyAssociationInstance> getAllValuesOfpropinst(final InstanceReferenceValue pPropvalue, final BasicPropertyAssociation pProp, final RecordValue pProplistelem, final ListValue pProplist, final ModalPropertyValue pModalprop, final ComponentInstance pCompinst, final Aaxl2AaxlTrace pTrace) {
      return rawStreamAllValuesOfpropinst(new Object[]{pPropvalue, pProp, pProplistelem, pProplist, pModalprop, null, pCompinst, pTrace}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ComponentInstance> rawStreamAllValuesOfcompinst(final Object[] parameters) {
      return rawStreamAllValues(POSITION_COMPINST, parameters).map(ComponentInstance.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst() {
      return rawStreamAllValuesOfcompinst(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst() {
      return rawStreamAllValuesOfcompinst(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst(final Find_PropertyRefAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfcompinst(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<ComponentInstance> streamAllValuesOfcompinst(final InstanceReferenceValue pPropvalue, final BasicPropertyAssociation pProp, final RecordValue pProplistelem, final ListValue pProplist, final ModalPropertyValue pModalprop, final PropertyAssociationInstance pPropinst, final Aaxl2AaxlTrace pTrace) {
      return rawStreamAllValuesOfcompinst(new Object[]{pPropvalue, pProp, pProplistelem, pProplist, pModalprop, pPropinst, null, pTrace});
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst(final Find_PropertyRefAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOfcompinst(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for compinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ComponentInstance> getAllValuesOfcompinst(final InstanceReferenceValue pPropvalue, final BasicPropertyAssociation pProp, final RecordValue pProplistelem, final ListValue pProplist, final ModalPropertyValue pModalprop, final PropertyAssociationInstance pPropinst, final Aaxl2AaxlTrace pTrace) {
      return rawStreamAllValuesOfcompinst(new Object[]{pPropvalue, pProp, pProplistelem, pProplist, pModalprop, pPropinst, null, pTrace}).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<Aaxl2AaxlTrace> rawStreamAllValuesOftrace(final Object[] parameters) {
      return rawStreamAllValues(POSITION_TRACE, parameters).map(Aaxl2AaxlTrace.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Aaxl2AaxlTrace> getAllValuesOftrace() {
      return rawStreamAllValuesOftrace(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<Aaxl2AaxlTrace> streamAllValuesOftrace() {
      return rawStreamAllValuesOftrace(emptyArray());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<Aaxl2AaxlTrace> streamAllValuesOftrace(final Find_PropertyRefAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOftrace(partialMatch.toArray());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     *      
     * @return the Stream of all values or empty set if there are no matches
     * 
     */
    public Stream<Aaxl2AaxlTrace> streamAllValuesOftrace(final InstanceReferenceValue pPropvalue, final BasicPropertyAssociation pProp, final RecordValue pProplistelem, final ListValue pProplist, final ModalPropertyValue pModalprop, final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst) {
      return rawStreamAllValuesOftrace(new Object[]{pPropvalue, pProp, pProplistelem, pProplist, pModalprop, pPropinst, pCompinst, null});
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Aaxl2AaxlTrace> getAllValuesOftrace(final Find_PropertyRefAttach2Component.Match partialMatch) {
      return rawStreamAllValuesOftrace(partialMatch.toArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for trace.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Aaxl2AaxlTrace> getAllValuesOftrace(final InstanceReferenceValue pPropvalue, final BasicPropertyAssociation pProp, final RecordValue pProplistelem, final ListValue pProplist, final ModalPropertyValue pModalprop, final PropertyAssociationInstance pPropinst, final ComponentInstance pCompinst) {
      return rawStreamAllValuesOftrace(new Object[]{pPropvalue, pProp, pProplistelem, pProplist, pModalprop, pPropinst, pCompinst, null}).collect(Collectors.toSet());
    }

    @Override
    protected Find_PropertyRefAttach2Component.Match tupleToMatch(final Tuple t) {
      try {
          return Find_PropertyRefAttach2Component.Match.newMatch((InstanceReferenceValue) t.get(POSITION_PROPVALUE), (BasicPropertyAssociation) t.get(POSITION_PROP), (RecordValue) t.get(POSITION_PROPLISTELEM), (ListValue) t.get(POSITION_PROPLIST), (ModalPropertyValue) t.get(POSITION_MODALPROP), (PropertyAssociationInstance) t.get(POSITION_PROPINST), (ComponentInstance) t.get(POSITION_COMPINST), (Aaxl2AaxlTrace) t.get(POSITION_TRACE));
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in tuple not properly typed!",e);
          return null;
      }
    }

    @Override
    protected Find_PropertyRefAttach2Component.Match arrayToMatch(final Object[] match) {
      try {
          return Find_PropertyRefAttach2Component.Match.newMatch((InstanceReferenceValue) match[POSITION_PROPVALUE], (BasicPropertyAssociation) match[POSITION_PROP], (RecordValue) match[POSITION_PROPLISTELEM], (ListValue) match[POSITION_PROPLIST], (ModalPropertyValue) match[POSITION_MODALPROP], (PropertyAssociationInstance) match[POSITION_PROPINST], (ComponentInstance) match[POSITION_COMPINST], (Aaxl2AaxlTrace) match[POSITION_TRACE]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    @Override
    protected Find_PropertyRefAttach2Component.Match arrayToMatchMutable(final Object[] match) {
      try {
          return Find_PropertyRefAttach2Component.Match.newMutableMatch((InstanceReferenceValue) match[POSITION_PROPVALUE], (BasicPropertyAssociation) match[POSITION_PROP], (RecordValue) match[POSITION_PROPLISTELEM], (ListValue) match[POSITION_PROPLIST], (ModalPropertyValue) match[POSITION_MODALPROP], (PropertyAssociationInstance) match[POSITION_PROPINST], (ComponentInstance) match[POSITION_COMPINST], (Aaxl2AaxlTrace) match[POSITION_TRACE]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    /**
     * @return the singleton instance of the query specification of this pattern
     * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
     * 
     */
    public static IQuerySpecification<Find_PropertyRefAttach2Component.Matcher> querySpecification() {
      return Find_PropertyRefAttach2Component.instance();
    }
  }

  private Find_PropertyRefAttach2Component() {
    super(GeneratedPQuery.INSTANCE);
  }

  /**
   * @return the singleton instance of the query specification
   * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
   * 
   */
  public static Find_PropertyRefAttach2Component instance() {
    try{
        return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
        throw processInitializerError(err);
    }
  }

  @Override
  protected Find_PropertyRefAttach2Component.Matcher instantiate(final ViatraQueryEngine engine) {
    return Find_PropertyRefAttach2Component.Matcher.on(engine);
  }

  @Override
  public Find_PropertyRefAttach2Component.Matcher instantiate() {
    return Find_PropertyRefAttach2Component.Matcher.create();
  }

  @Override
  public Find_PropertyRefAttach2Component.Match newEmptyMatch() {
    return Find_PropertyRefAttach2Component.Match.newEmptyMatch();
  }

  @Override
  public Find_PropertyRefAttach2Component.Match newMatch(final Object... parameters) {
    return Find_PropertyRefAttach2Component.Match.newMatch((org.osate.aadl2.instance.InstanceReferenceValue) parameters[0], (org.osate.aadl2.BasicPropertyAssociation) parameters[1], (org.osate.aadl2.RecordValue) parameters[2], (org.osate.aadl2.ListValue) parameters[3], (org.osate.aadl2.ModalPropertyValue) parameters[4], (org.osate.aadl2.instance.PropertyAssociationInstance) parameters[5], (org.osate.aadl2.instance.ComponentInstance) parameters[6], (fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace) parameters[7]);
  }

  /**
   * Inner class allowing the singleton instance of {@link Find_PropertyRefAttach2Component} to be created 
   *     <b>not</b> at the class load time of the outer class, 
   *     but rather at the first call to {@link Find_PropertyRefAttach2Component#instance()}.
   * 
   * <p> This workaround is required e.g. to support recursion.
   * 
   */
  private static class LazyHolder {
    private static final Find_PropertyRefAttach2Component INSTANCE = new Find_PropertyRefAttach2Component();

    /**
     * Statically initializes the query specification <b>after</b> the field {@link #INSTANCE} is assigned.
     * This initialization order is required to support indirect recursion.
     * 
     * <p> The static initializer is defined using a helper field to work around limitations of the code generator.
     * 
     */
    private static final Object STATIC_INITIALIZER = ensureInitialized();

    public static Object ensureInitialized() {
      INSTANCE.ensureInitializedInternal();
      return null;
    }
  }

  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private static final Find_PropertyRefAttach2Component.GeneratedPQuery INSTANCE = new GeneratedPQuery();

    private final PParameter parameter_propvalue = new PParameter("propvalue", "org.osate.aadl2.instance.InstanceReferenceValue", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "InstanceReferenceValue")), PParameterDirection.INOUT);

    private final PParameter parameter_prop = new PParameter("prop", "org.osate.aadl2.BasicPropertyAssociation", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0", "BasicPropertyAssociation")), PParameterDirection.INOUT);

    private final PParameter parameter_proplistelem = new PParameter("proplistelem", "org.osate.aadl2.RecordValue", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0", "RecordValue")), PParameterDirection.INOUT);

    private final PParameter parameter_proplist = new PParameter("proplist", "org.osate.aadl2.ListValue", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0", "ListValue")), PParameterDirection.INOUT);

    private final PParameter parameter_modalprop = new PParameter("modalprop", "org.osate.aadl2.ModalPropertyValue", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0", "ModalPropertyValue")), PParameterDirection.INOUT);

    private final PParameter parameter_propinst = new PParameter("propinst", "org.osate.aadl2.instance.PropertyAssociationInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "PropertyAssociationInstance")), PParameterDirection.INOUT);

    private final PParameter parameter_compinst = new PParameter("compinst", "org.osate.aadl2.instance.ComponentInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ComponentInstance")), PParameterDirection.INOUT);

    private final PParameter parameter_trace = new PParameter("trace", "fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace")), PParameterDirection.INOUT);

    private final List<PParameter> parameters = Arrays.asList(parameter_propvalue, parameter_prop, parameter_proplistelem, parameter_proplist, parameter_modalprop, parameter_propinst, parameter_compinst, parameter_trace);

    private GeneratedPQuery() {
      super(PVisibility.PUBLIC);
    }

    @Override
    public String getFullyQualifiedName() {
      return "fr.mem4csd.osatedim.viatra.queries.find_PropertyRefAttach2Component";
    }

    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("propvalue","prop","proplistelem","proplist","modalprop","propinst","compinst","trace");
    }

    @Override
    public List<PParameter> getParameters() {
      return parameters;
    }

    @Override
    public Set<PBody> doGetContainedBodies() {
      setEvaluationHints(new QueryEvaluationHint(null, QueryEvaluationHint.BackendRequirement.UNSPECIFIED));
      Set<PBody> bodies = new LinkedHashSet<>();
      {
          PBody body = new PBody(this);
          PVariable var_propvalue = body.getOrCreateVariableByName("propvalue");
          PVariable var_prop = body.getOrCreateVariableByName("prop");
          PVariable var_proplistelem = body.getOrCreateVariableByName("proplistelem");
          PVariable var_proplist = body.getOrCreateVariableByName("proplist");
          PVariable var_modalprop = body.getOrCreateVariableByName("modalprop");
          PVariable var_propinst = body.getOrCreateVariableByName("propinst");
          PVariable var_compinst = body.getOrCreateVariableByName("compinst");
          PVariable var_trace = body.getOrCreateVariableByName("trace");
          new TypeConstraint(body, Tuples.flatTupleOf(var_propvalue), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "InstanceReferenceValue")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_prop), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "BasicPropertyAssociation")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_proplistelem), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "RecordValue")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_proplist), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ListValue")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_modalprop), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ModalPropertyValue")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_propinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "PropertyAssociationInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_compinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var_trace), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_propvalue, parameter_propvalue),
             new ExportedParameter(body, var_prop, parameter_prop),
             new ExportedParameter(body, var_proplistelem, parameter_proplistelem),
             new ExportedParameter(body, var_proplist, parameter_proplist),
             new ExportedParameter(body, var_modalprop, parameter_modalprop),
             new ExportedParameter(body, var_propinst, parameter_propinst),
             new ExportedParameter(body, var_compinst, parameter_compinst),
             new ExportedParameter(body, var_trace, parameter_trace)
          ));
          // 	Aaxl2AaxlTrace.leftInstance(trace,compinst)
          new TypeConstraint(body, Tuples.flatTupleOf(var_trace), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_trace, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace", "leftInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Element")));
          new Equality(body, var__virtual_0_, var_compinst);
          // 	Aaxl2AaxlTrace.objectsToAttach(trace, propinst)
          new TypeConstraint(body, Tuples.flatTupleOf(var_trace), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace")));
          PVariable var__virtual_1_ = body.getOrCreateVariableByName(".virtual{1}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_trace, var__virtual_1_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://fr.mem4csd.osatedim.deltatrace/aaxl2aaxl", "Aaxl2AaxlTrace", "objectsToAttach")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_1_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Element")));
          new Equality(body, var__virtual_1_, var_propinst);
          // 	PropertyAssociationInstance.ownedValue(propinst,modalprop)
          new TypeConstraint(body, Tuples.flatTupleOf(var_propinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "PropertyAssociationInstance")));
          PVariable var__virtual_2_ = body.getOrCreateVariableByName(".virtual{2}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_propinst, var__virtual_2_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "PropertyAssociation", "ownedValue")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_2_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ModalPropertyValue")));
          new Equality(body, var__virtual_2_, var_modalprop);
          // 	ModalPropertyValue.ownedValue(modalprop,proplist)
          new TypeConstraint(body, Tuples.flatTupleOf(var_modalprop), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ModalPropertyValue")));
          PVariable var__virtual_3_ = body.getOrCreateVariableByName(".virtual{3}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_modalprop, var__virtual_3_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ModalPropertyValue", "ownedValue")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_3_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "PropertyExpression")));
          new Equality(body, var__virtual_3_, var_proplist);
          // 	ListValue.ownedListElement(proplist,proplistelem)
          new TypeConstraint(body, Tuples.flatTupleOf(var_proplist), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "ListValue")));
          PVariable var__virtual_4_ = body.getOrCreateVariableByName(".virtual{4}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_proplist, var__virtual_4_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "ListValue", "ownedListElement")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_4_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "PropertyExpression")));
          new Equality(body, var__virtual_4_, var_proplistelem);
          // 	RecordValue.ownedFieldValue(proplistelem,prop)
          new TypeConstraint(body, Tuples.flatTupleOf(var_proplistelem), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "RecordValue")));
          PVariable var__virtual_5_ = body.getOrCreateVariableByName(".virtual{5}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_proplistelem, var__virtual_5_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "RecordValue", "ownedFieldValue")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_5_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "BasicPropertyAssociation")));
          new Equality(body, var__virtual_5_, var_prop);
          // 	BasicPropertyAssociation.ownedValue(prop,propvalue)
          new TypeConstraint(body, Tuples.flatTupleOf(var_prop), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "BasicPropertyAssociation")));
          PVariable var__virtual_6_ = body.getOrCreateVariableByName(".virtual{6}");
          new TypeConstraint(body, Tuples.flatTupleOf(var_prop, var__virtual_6_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0", "BasicPropertyAssociation", "ownedValue")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_6_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "PropertyExpression")));
          new Equality(body, var__virtual_6_, var_propvalue);
          bodies.add(body);
      }
      return bodies;
    }
  }
}
