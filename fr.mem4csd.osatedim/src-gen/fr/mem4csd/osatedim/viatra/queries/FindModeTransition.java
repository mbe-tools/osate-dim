/**
 * Generated from platform:/resource/fr.mem4csd.osatedim/src/fr/mem4csd/osatedim/viatra/queries/DIMQueriesInplace.vql
 */
package fr.mem4csd.osatedim.viatra.queries;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.viatra.query.runtime.api.IPatternMatch;
import org.eclipse.viatra.query.runtime.api.IQuerySpecification;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.viatra.query.runtime.api.impl.BaseMatcher;
import org.eclipse.viatra.query.runtime.api.impl.BasePatternMatch;
import org.eclipse.viatra.query.runtime.emf.types.EClassTransitiveInstancesKey;
import org.eclipse.viatra.query.runtime.emf.types.EStructuralFeatureInstancesKey;
import org.eclipse.viatra.query.runtime.matchers.backend.QueryEvaluationHint;
import org.eclipse.viatra.query.runtime.matchers.psystem.PBody;
import org.eclipse.viatra.query.runtime.matchers.psystem.PVariable;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.Equality;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.TypeConstraint;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameterDirection;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PVisibility;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuple;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuples;
import org.eclipse.viatra.query.runtime.util.ViatraQueryLoggingUtil;
import org.osate.aadl2.instance.ModeTransitionInstance;

/**
 * A pattern-specific query specification that can instantiate Matcher in a type-safe way.
 * 
 * <p>Original source:
 *         <code><pre>
 *         // Find mode transition instances
 *         pattern findModeTransition(modetransinst: ModeTransitionInstance)
 *         {
 *         	ComponentInstance.modeTransitionInstance(_,modetransinst);
 *         }
 * </pre></code>
 * 
 * @see Matcher
 * @see Match
 * 
 */
@SuppressWarnings("all")
public final class FindModeTransition extends BaseGeneratedEMFQuerySpecification<FindModeTransition.Matcher> {
  /**
   * Pattern-specific match representation of the fr.mem4csd.osatedim.viatra.queries.findModeTransition pattern,
   * to be used in conjunction with {@link Matcher}.
   * 
   * <p>Class fields correspond to parameters of the pattern. Fields with value null are considered unassigned.
   * Each instance is a (possibly partial) substitution of pattern parameters,
   * usable to represent a match of the pattern in the result of a query,
   * or to specify the bound (fixed) input parameters when issuing a query.
   * 
   * @see Matcher
   * 
   */
  public static abstract class Match extends BasePatternMatch {
    private ModeTransitionInstance fModetransinst;

    private static List<String> parameterNames = makeImmutableList("modetransinst");

    private Match(final ModeTransitionInstance pModetransinst) {
      this.fModetransinst = pModetransinst;
    }

    @Override
    public Object get(final String parameterName) {
      switch(parameterName) {
          case "modetransinst": return this.fModetransinst;
          default: return null;
      }
    }

    @Override
    public Object get(final int index) {
      switch(index) {
          case 0: return this.fModetransinst;
          default: return null;
      }
    }

    public ModeTransitionInstance getModetransinst() {
      return this.fModetransinst;
    }

    @Override
    public boolean set(final String parameterName, final Object newValue) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      if ("modetransinst".equals(parameterName) ) {
          this.fModetransinst = (ModeTransitionInstance) newValue;
          return true;
      }
      return false;
    }

    public void setModetransinst(final ModeTransitionInstance pModetransinst) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fModetransinst = pModetransinst;
    }

    @Override
    public String patternName() {
      return "fr.mem4csd.osatedim.viatra.queries.findModeTransition";
    }

    @Override
    public List<String> parameterNames() {
      return FindModeTransition.Match.parameterNames;
    }

    @Override
    public Object[] toArray() {
      return new Object[]{fModetransinst};
    }

    @Override
    public FindModeTransition.Match toImmutable() {
      return isMutable() ? newMatch(fModetransinst) : this;
    }

    @Override
    public String prettyPrint() {
      StringBuilder result = new StringBuilder();
      result.append("\"modetransinst\"=" + prettyPrintValue(fModetransinst));
      return result.toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(fModetransinst);
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj)
          return true;
      if (obj == null) {
          return false;
      }
      if ((obj instanceof FindModeTransition.Match)) {
          FindModeTransition.Match other = (FindModeTransition.Match) obj;
          return Objects.equals(fModetransinst, other.fModetransinst);
      } else {
          // this should be infrequent
          if (!(obj instanceof IPatternMatch)) {
              return false;
          }
          IPatternMatch otherSig  = (IPatternMatch) obj;
          return Objects.equals(specification(), otherSig.specification()) && Arrays.deepEquals(toArray(), otherSig.toArray());
      }
    }

    @Override
    public FindModeTransition specification() {
      return FindModeTransition.instance();
    }

    /**
     * Returns an empty, mutable match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @return the empty match.
     * 
     */
    public static FindModeTransition.Match newEmptyMatch() {
      return new Mutable(null);
    }

    /**
     * Returns a mutable (partial) match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @param pModetransinst the fixed value of pattern parameter modetransinst, or null if not bound.
     * @return the new, mutable (partial) match object.
     * 
     */
    public static FindModeTransition.Match newMutableMatch(final ModeTransitionInstance pModetransinst) {
      return new Mutable(pModetransinst);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pModetransinst the fixed value of pattern parameter modetransinst, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public static FindModeTransition.Match newMatch(final ModeTransitionInstance pModetransinst) {
      return new Immutable(pModetransinst);
    }

    private static final class Mutable extends FindModeTransition.Match {
      Mutable(final ModeTransitionInstance pModetransinst) {
        super(pModetransinst);
      }

      @Override
      public boolean isMutable() {
        return true;
      }
    }

    private static final class Immutable extends FindModeTransition.Match {
      Immutable(final ModeTransitionInstance pModetransinst) {
        super(pModetransinst);
      }

      @Override
      public boolean isMutable() {
        return false;
      }
    }
  }

  /**
   * Generated pattern matcher API of the fr.mem4csd.osatedim.viatra.queries.findModeTransition pattern,
   * providing pattern-specific query methods.
   * 
   * <p>Use the pattern matcher on a given model via {@link #on(ViatraQueryEngine)},
   * e.g. in conjunction with {@link ViatraQueryEngine#on(QueryScope)}.
   * 
   * <p>Matches of the pattern will be represented as {@link Match}.
   * 
   * <p>Original source:
   * <code><pre>
   * // Find mode transition instances
   * pattern findModeTransition(modetransinst: ModeTransitionInstance)
   * {
   * 	ComponentInstance.modeTransitionInstance(_,modetransinst);
   * }
   * </pre></code>
   * 
   * @see Match
   * @see FindModeTransition
   * 
   */
  public static class Matcher extends BaseMatcher<FindModeTransition.Match> {
    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    public static FindModeTransition.Matcher on(final ViatraQueryEngine engine) {
      // check if matcher already exists
      Matcher matcher = engine.getExistingMatcher(querySpecification());
      if (matcher == null) {
          matcher = (Matcher)engine.getMatcher(querySpecification());
      }
      return matcher;
    }

    /**
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * @return an initialized matcher
     * @noreference This method is for internal matcher initialization by the framework, do not call it manually.
     * 
     */
    public static FindModeTransition.Matcher create() {
      return new Matcher();
    }

    private static final int POSITION_MODETRANSINST = 0;

    private static final Logger LOGGER = ViatraQueryLoggingUtil.getLogger(FindModeTransition.Matcher.class);

    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    private Matcher() {
      super(querySpecification());
    }

    /**
     * Returns the set of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pModetransinst the fixed value of pattern parameter modetransinst, or null if not bound.
     * @return matches represented as a Match object.
     * 
     */
    public Collection<FindModeTransition.Match> getAllMatches(final ModeTransitionInstance pModetransinst) {
      return rawStreamAllMatches(new Object[]{pModetransinst}).collect(Collectors.toSet());
    }

    /**
     * Returns a stream of all matches of the pattern that conform to the given fixed values of some parameters.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     * @param pModetransinst the fixed value of pattern parameter modetransinst, or null if not bound.
     * @return a stream of matches represented as a Match object.
     * 
     */
    public Stream<FindModeTransition.Match> streamAllMatches(final ModeTransitionInstance pModetransinst) {
      return rawStreamAllMatches(new Object[]{pModetransinst});
    }

    /**
     * Returns an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pModetransinst the fixed value of pattern parameter modetransinst, or null if not bound.
     * @return a match represented as a Match object, or null if no match is found.
     * 
     */
    public Optional<FindModeTransition.Match> getOneArbitraryMatch(final ModeTransitionInstance pModetransinst) {
      return rawGetOneArbitraryMatch(new Object[]{pModetransinst});
    }

    /**
     * Indicates whether the given combination of specified pattern parameters constitute a valid pattern match,
     * under any possible substitution of the unspecified parameters (if any).
     * @param pModetransinst the fixed value of pattern parameter modetransinst, or null if not bound.
     * @return true if the input is a valid (partial) match of the pattern.
     * 
     */
    public boolean hasMatch(final ModeTransitionInstance pModetransinst) {
      return rawHasMatch(new Object[]{pModetransinst});
    }

    /**
     * Returns the number of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pModetransinst the fixed value of pattern parameter modetransinst, or null if not bound.
     * @return the number of pattern matches found.
     * 
     */
    public int countMatches(final ModeTransitionInstance pModetransinst) {
      return rawCountMatches(new Object[]{pModetransinst});
    }

    /**
     * Executes the given processor on an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pModetransinst the fixed value of pattern parameter modetransinst, or null if not bound.
     * @param processor the action that will process the selected match.
     * @return true if the pattern has at least one match with the given parameter values, false if the processor was not invoked
     * 
     */
    public boolean forOneArbitraryMatch(final ModeTransitionInstance pModetransinst, final Consumer<? super FindModeTransition.Match> processor) {
      return rawForOneArbitraryMatch(new Object[]{pModetransinst}, processor);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pModetransinst the fixed value of pattern parameter modetransinst, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public FindModeTransition.Match newMatch(final ModeTransitionInstance pModetransinst) {
      return FindModeTransition.Match.newMatch(pModetransinst);
    }

    /**
     * Retrieve the set of values that occur in matches for modetransinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<ModeTransitionInstance> rawStreamAllValuesOfmodetransinst(final Object[] parameters) {
      return rawStreamAllValues(POSITION_MODETRANSINST, parameters).map(ModeTransitionInstance.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for modetransinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<ModeTransitionInstance> getAllValuesOfmodetransinst() {
      return rawStreamAllValuesOfmodetransinst(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for modetransinst.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<ModeTransitionInstance> streamAllValuesOfmodetransinst() {
      return rawStreamAllValuesOfmodetransinst(emptyArray());
    }

    @Override
    protected FindModeTransition.Match tupleToMatch(final Tuple t) {
      try {
          return FindModeTransition.Match.newMatch((ModeTransitionInstance) t.get(POSITION_MODETRANSINST));
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in tuple not properly typed!",e);
          return null;
      }
    }

    @Override
    protected FindModeTransition.Match arrayToMatch(final Object[] match) {
      try {
          return FindModeTransition.Match.newMatch((ModeTransitionInstance) match[POSITION_MODETRANSINST]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    @Override
    protected FindModeTransition.Match arrayToMatchMutable(final Object[] match) {
      try {
          return FindModeTransition.Match.newMutableMatch((ModeTransitionInstance) match[POSITION_MODETRANSINST]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    /**
     * @return the singleton instance of the query specification of this pattern
     * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
     * 
     */
    public static IQuerySpecification<FindModeTransition.Matcher> querySpecification() {
      return FindModeTransition.instance();
    }
  }

  private FindModeTransition() {
    super(GeneratedPQuery.INSTANCE);
  }

  /**
   * @return the singleton instance of the query specification
   * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
   * 
   */
  public static FindModeTransition instance() {
    try{
        return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
        throw processInitializerError(err);
    }
  }

  @Override
  protected FindModeTransition.Matcher instantiate(final ViatraQueryEngine engine) {
    return FindModeTransition.Matcher.on(engine);
  }

  @Override
  public FindModeTransition.Matcher instantiate() {
    return FindModeTransition.Matcher.create();
  }

  @Override
  public FindModeTransition.Match newEmptyMatch() {
    return FindModeTransition.Match.newEmptyMatch();
  }

  @Override
  public FindModeTransition.Match newMatch(final Object... parameters) {
    return FindModeTransition.Match.newMatch((org.osate.aadl2.instance.ModeTransitionInstance) parameters[0]);
  }

  /**
   * Inner class allowing the singleton instance of {@link FindModeTransition} to be created 
   *     <b>not</b> at the class load time of the outer class, 
   *     but rather at the first call to {@link FindModeTransition#instance()}.
   * 
   * <p> This workaround is required e.g. to support recursion.
   * 
   */
  private static class LazyHolder {
    private static final FindModeTransition INSTANCE = new FindModeTransition();

    /**
     * Statically initializes the query specification <b>after</b> the field {@link #INSTANCE} is assigned.
     * This initialization order is required to support indirect recursion.
     * 
     * <p> The static initializer is defined using a helper field to work around limitations of the code generator.
     * 
     */
    private static final Object STATIC_INITIALIZER = ensureInitialized();

    public static Object ensureInitialized() {
      INSTANCE.ensureInitializedInternal();
      return null;
    }
  }

  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private static final FindModeTransition.GeneratedPQuery INSTANCE = new GeneratedPQuery();

    private final PParameter parameter_modetransinst = new PParameter("modetransinst", "org.osate.aadl2.instance.ModeTransitionInstance", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0/instance", "ModeTransitionInstance")), PParameterDirection.INOUT);

    private final List<PParameter> parameters = Arrays.asList(parameter_modetransinst);

    private GeneratedPQuery() {
      super(PVisibility.PUBLIC);
    }

    @Override
    public String getFullyQualifiedName() {
      return "fr.mem4csd.osatedim.viatra.queries.findModeTransition";
    }

    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("modetransinst");
    }

    @Override
    public List<PParameter> getParameters() {
      return parameters;
    }

    @Override
    public Set<PBody> doGetContainedBodies() {
      setEvaluationHints(new QueryEvaluationHint(null, QueryEvaluationHint.BackendRequirement.UNSPECIFIED));
      Set<PBody> bodies = new LinkedHashSet<>();
      {
          PBody body = new PBody(this);
          PVariable var_modetransinst = body.getOrCreateVariableByName("modetransinst");
          PVariable var___0_ = body.getOrCreateVariableByName("_<0>");
          new TypeConstraint(body, Tuples.flatTupleOf(var_modetransinst), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ModeTransitionInstance")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_modetransinst, parameter_modetransinst)
          ));
          // 	ComponentInstance.modeTransitionInstance(_,modetransinst)
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance")));
          PVariable var__virtual_0_ = body.getOrCreateVariableByName(".virtual{0}");
          new TypeConstraint(body, Tuples.flatTupleOf(var___0_, var__virtual_0_), new EStructuralFeatureInstancesKey(getFeatureLiteral("http://aadl.info/AADL/2.0/instance", "ComponentInstance", "modeTransitionInstance")));
          new TypeConstraint(body, Tuples.flatTupleOf(var__virtual_0_), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "ModeTransitionInstance")));
          new Equality(body, var__virtual_0_, var_modetransinst);
          bodies.add(body);
      }
      return bodies;
    }
  }
}
