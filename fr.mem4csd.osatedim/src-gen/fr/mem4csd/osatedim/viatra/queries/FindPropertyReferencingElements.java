/**
 * Generated from platform:/resource/fr.mem4csd.osatedim/src/fr/mem4csd/osatedim/viatra/queries/DIMQueriesInplace.vql
 */
package fr.mem4csd.osatedim.viatra.queries;

import java.util.Arrays;
import java.util.Collection;
import java.util.LinkedHashSet;
import java.util.List;
import java.util.Objects;
import java.util.Optional;
import java.util.Set;
import java.util.function.Consumer;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.apache.log4j.Logger;
import org.eclipse.emf.ecore.EClass;
import org.eclipse.viatra.query.runtime.api.IPatternMatch;
import org.eclipse.viatra.query.runtime.api.IQuerySpecification;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFPQuery;
import org.eclipse.viatra.query.runtime.api.impl.BaseGeneratedEMFQuerySpecification;
import org.eclipse.viatra.query.runtime.api.impl.BaseMatcher;
import org.eclipse.viatra.query.runtime.api.impl.BasePatternMatch;
import org.eclipse.viatra.query.runtime.emf.types.EClassTransitiveInstancesKey;
import org.eclipse.viatra.query.runtime.matchers.backend.QueryEvaluationHint;
import org.eclipse.viatra.query.runtime.matchers.psystem.PBody;
import org.eclipse.viatra.query.runtime.matchers.psystem.PVariable;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicdeferred.ExportedParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.basicenumerables.TypeConstraint;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameter;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PParameterDirection;
import org.eclipse.viatra.query.runtime.matchers.psystem.queries.PVisibility;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuple;
import org.eclipse.viatra.query.runtime.matchers.tuple.Tuples;
import org.eclipse.viatra.query.runtime.util.ViatraQueryLoggingUtil;
import org.osate.aadl2.Element;

/**
 * A pattern-specific query specification that can instantiate Matcher in a type-safe way.
 * 
 * <p>Original source:
 *         <code><pre>
 *         pattern findPropertyReferencingElements(element: Element) {
 *         	PropertyAssociationInstance(element);
 *         } or {
 *         	BasicPropertyAssociation(element);
 *         }
 * </pre></code>
 * 
 * @see Matcher
 * @see Match
 * 
 */
@SuppressWarnings("all")
public final class FindPropertyReferencingElements extends BaseGeneratedEMFQuerySpecification<FindPropertyReferencingElements.Matcher> {
  /**
   * Pattern-specific match representation of the fr.mem4csd.osatedim.viatra.queries.findPropertyReferencingElements pattern,
   * to be used in conjunction with {@link Matcher}.
   * 
   * <p>Class fields correspond to parameters of the pattern. Fields with value null are considered unassigned.
   * Each instance is a (possibly partial) substitution of pattern parameters,
   * usable to represent a match of the pattern in the result of a query,
   * or to specify the bound (fixed) input parameters when issuing a query.
   * 
   * @see Matcher
   * 
   */
  public static abstract class Match extends BasePatternMatch {
    private Element fElement;

    private static List<String> parameterNames = makeImmutableList("element");

    private Match(final Element pElement) {
      this.fElement = pElement;
    }

    @Override
    public Object get(final String parameterName) {
      switch(parameterName) {
          case "element": return this.fElement;
          default: return null;
      }
    }

    @Override
    public Object get(final int index) {
      switch(index) {
          case 0: return this.fElement;
          default: return null;
      }
    }

    public Element getElement() {
      return this.fElement;
    }

    @Override
    public boolean set(final String parameterName, final Object newValue) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      if ("element".equals(parameterName) ) {
          this.fElement = (Element) newValue;
          return true;
      }
      return false;
    }

    public void setElement(final Element pElement) {
      if (!isMutable()) throw new java.lang.UnsupportedOperationException();
      this.fElement = pElement;
    }

    @Override
    public String patternName() {
      return "fr.mem4csd.osatedim.viatra.queries.findPropertyReferencingElements";
    }

    @Override
    public List<String> parameterNames() {
      return FindPropertyReferencingElements.Match.parameterNames;
    }

    @Override
    public Object[] toArray() {
      return new Object[]{fElement};
    }

    @Override
    public FindPropertyReferencingElements.Match toImmutable() {
      return isMutable() ? newMatch(fElement) : this;
    }

    @Override
    public String prettyPrint() {
      StringBuilder result = new StringBuilder();
      result.append("\"element\"=" + prettyPrintValue(fElement));
      return result.toString();
    }

    @Override
    public int hashCode() {
      return Objects.hash(fElement);
    }

    @Override
    public boolean equals(final Object obj) {
      if (this == obj)
          return true;
      if (obj == null) {
          return false;
      }
      if ((obj instanceof FindPropertyReferencingElements.Match)) {
          FindPropertyReferencingElements.Match other = (FindPropertyReferencingElements.Match) obj;
          return Objects.equals(fElement, other.fElement);
      } else {
          // this should be infrequent
          if (!(obj instanceof IPatternMatch)) {
              return false;
          }
          IPatternMatch otherSig  = (IPatternMatch) obj;
          return Objects.equals(specification(), otherSig.specification()) && Arrays.deepEquals(toArray(), otherSig.toArray());
      }
    }

    @Override
    public FindPropertyReferencingElements specification() {
      return FindPropertyReferencingElements.instance();
    }

    /**
     * Returns an empty, mutable match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @return the empty match.
     * 
     */
    public static FindPropertyReferencingElements.Match newEmptyMatch() {
      return new Mutable(null);
    }

    /**
     * Returns a mutable (partial) match.
     * Fields of the mutable match can be filled to create a partial match, usable as matcher input.
     * 
     * @param pElement the fixed value of pattern parameter element, or null if not bound.
     * @return the new, mutable (partial) match object.
     * 
     */
    public static FindPropertyReferencingElements.Match newMutableMatch(final Element pElement) {
      return new Mutable(pElement);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pElement the fixed value of pattern parameter element, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public static FindPropertyReferencingElements.Match newMatch(final Element pElement) {
      return new Immutable(pElement);
    }

    private static final class Mutable extends FindPropertyReferencingElements.Match {
      Mutable(final Element pElement) {
        super(pElement);
      }

      @Override
      public boolean isMutable() {
        return true;
      }
    }

    private static final class Immutable extends FindPropertyReferencingElements.Match {
      Immutable(final Element pElement) {
        super(pElement);
      }

      @Override
      public boolean isMutable() {
        return false;
      }
    }
  }

  /**
   * Generated pattern matcher API of the fr.mem4csd.osatedim.viatra.queries.findPropertyReferencingElements pattern,
   * providing pattern-specific query methods.
   * 
   * <p>Use the pattern matcher on a given model via {@link #on(ViatraQueryEngine)},
   * e.g. in conjunction with {@link ViatraQueryEngine#on(QueryScope)}.
   * 
   * <p>Matches of the pattern will be represented as {@link Match}.
   * 
   * <p>Original source:
   * <code><pre>
   * pattern findPropertyReferencingElements(element: Element) {
   * 	PropertyAssociationInstance(element);
   * } or {
   * 	BasicPropertyAssociation(element);
   * }
   * </pre></code>
   * 
   * @see Match
   * @see FindPropertyReferencingElements
   * 
   */
  public static class Matcher extends BaseMatcher<FindPropertyReferencingElements.Match> {
    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    public static FindPropertyReferencingElements.Matcher on(final ViatraQueryEngine engine) {
      // check if matcher already exists
      Matcher matcher = engine.getExistingMatcher(querySpecification());
      if (matcher == null) {
          matcher = (Matcher)engine.getMatcher(querySpecification());
      }
      return matcher;
    }

    /**
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * @return an initialized matcher
     * @noreference This method is for internal matcher initialization by the framework, do not call it manually.
     * 
     */
    public static FindPropertyReferencingElements.Matcher create() {
      return new Matcher();
    }

    private static final int POSITION_ELEMENT = 0;

    private static final Logger LOGGER = ViatraQueryLoggingUtil.getLogger(FindPropertyReferencingElements.Matcher.class);

    /**
     * Initializes the pattern matcher within an existing VIATRA Query engine.
     * If the pattern matcher is already constructed in the engine, only a light-weight reference is returned.
     * 
     * @param engine the existing VIATRA Query engine in which this matcher will be created.
     * @throws ViatraQueryRuntimeException if an error occurs during pattern matcher creation
     * 
     */
    private Matcher() {
      super(querySpecification());
    }

    /**
     * Returns the set of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pElement the fixed value of pattern parameter element, or null if not bound.
     * @return matches represented as a Match object.
     * 
     */
    public Collection<FindPropertyReferencingElements.Match> getAllMatches(final Element pElement) {
      return rawStreamAllMatches(new Object[]{pElement}).collect(Collectors.toSet());
    }

    /**
     * Returns a stream of all matches of the pattern that conform to the given fixed values of some parameters.
     * </p>
     * <strong>NOTE</strong>: It is important not to modify the source model while the stream is being processed.
     * If the match set of the pattern changes during processing, the contents of the stream is <strong>undefined</strong>.
     * In such cases, either rely on {@link #getAllMatches()} or collect the results of the stream in end-user code.
     * @param pElement the fixed value of pattern parameter element, or null if not bound.
     * @return a stream of matches represented as a Match object.
     * 
     */
    public Stream<FindPropertyReferencingElements.Match> streamAllMatches(final Element pElement) {
      return rawStreamAllMatches(new Object[]{pElement});
    }

    /**
     * Returns an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pElement the fixed value of pattern parameter element, or null if not bound.
     * @return a match represented as a Match object, or null if no match is found.
     * 
     */
    public Optional<FindPropertyReferencingElements.Match> getOneArbitraryMatch(final Element pElement) {
      return rawGetOneArbitraryMatch(new Object[]{pElement});
    }

    /**
     * Indicates whether the given combination of specified pattern parameters constitute a valid pattern match,
     * under any possible substitution of the unspecified parameters (if any).
     * @param pElement the fixed value of pattern parameter element, or null if not bound.
     * @return true if the input is a valid (partial) match of the pattern.
     * 
     */
    public boolean hasMatch(final Element pElement) {
      return rawHasMatch(new Object[]{pElement});
    }

    /**
     * Returns the number of all matches of the pattern that conform to the given fixed values of some parameters.
     * @param pElement the fixed value of pattern parameter element, or null if not bound.
     * @return the number of pattern matches found.
     * 
     */
    public int countMatches(final Element pElement) {
      return rawCountMatches(new Object[]{pElement});
    }

    /**
     * Executes the given processor on an arbitrarily chosen match of the pattern that conforms to the given fixed values of some parameters.
     * Neither determinism nor randomness of selection is guaranteed.
     * @param pElement the fixed value of pattern parameter element, or null if not bound.
     * @param processor the action that will process the selected match.
     * @return true if the pattern has at least one match with the given parameter values, false if the processor was not invoked
     * 
     */
    public boolean forOneArbitraryMatch(final Element pElement, final Consumer<? super FindPropertyReferencingElements.Match> processor) {
      return rawForOneArbitraryMatch(new Object[]{pElement}, processor);
    }

    /**
     * Returns a new (partial) match.
     * This can be used e.g. to call the matcher with a partial match.
     * <p>The returned match will be immutable. Use {@link #newEmptyMatch()} to obtain a mutable match object.
     * @param pElement the fixed value of pattern parameter element, or null if not bound.
     * @return the (partial) match object.
     * 
     */
    public FindPropertyReferencingElements.Match newMatch(final Element pElement) {
      return FindPropertyReferencingElements.Match.newMatch(pElement);
    }

    /**
     * Retrieve the set of values that occur in matches for element.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    protected Stream<Element> rawStreamAllValuesOfelement(final Object[] parameters) {
      return rawStreamAllValues(POSITION_ELEMENT, parameters).map(Element.class::cast);
    }

    /**
     * Retrieve the set of values that occur in matches for element.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Set<Element> getAllValuesOfelement() {
      return rawStreamAllValuesOfelement(emptyArray()).collect(Collectors.toSet());
    }

    /**
     * Retrieve the set of values that occur in matches for element.
     * @return the Set of all values or empty set if there are no matches
     * 
     */
    public Stream<Element> streamAllValuesOfelement() {
      return rawStreamAllValuesOfelement(emptyArray());
    }

    @Override
    protected FindPropertyReferencingElements.Match tupleToMatch(final Tuple t) {
      try {
          return FindPropertyReferencingElements.Match.newMatch((Element) t.get(POSITION_ELEMENT));
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in tuple not properly typed!",e);
          return null;
      }
    }

    @Override
    protected FindPropertyReferencingElements.Match arrayToMatch(final Object[] match) {
      try {
          return FindPropertyReferencingElements.Match.newMatch((Element) match[POSITION_ELEMENT]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    @Override
    protected FindPropertyReferencingElements.Match arrayToMatchMutable(final Object[] match) {
      try {
          return FindPropertyReferencingElements.Match.newMutableMatch((Element) match[POSITION_ELEMENT]);
      } catch(ClassCastException e) {
          LOGGER.error("Element(s) in array not properly typed!",e);
          return null;
      }
    }

    /**
     * @return the singleton instance of the query specification of this pattern
     * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
     * 
     */
    public static IQuerySpecification<FindPropertyReferencingElements.Matcher> querySpecification() {
      return FindPropertyReferencingElements.instance();
    }
  }

  private FindPropertyReferencingElements() {
    super(GeneratedPQuery.INSTANCE);
  }

  /**
   * @return the singleton instance of the query specification
   * @throws ViatraQueryRuntimeException if the pattern definition could not be loaded
   * 
   */
  public static FindPropertyReferencingElements instance() {
    try{
        return LazyHolder.INSTANCE;
    } catch (ExceptionInInitializerError err) {
        throw processInitializerError(err);
    }
  }

  @Override
  protected FindPropertyReferencingElements.Matcher instantiate(final ViatraQueryEngine engine) {
    return FindPropertyReferencingElements.Matcher.on(engine);
  }

  @Override
  public FindPropertyReferencingElements.Matcher instantiate() {
    return FindPropertyReferencingElements.Matcher.create();
  }

  @Override
  public FindPropertyReferencingElements.Match newEmptyMatch() {
    return FindPropertyReferencingElements.Match.newEmptyMatch();
  }

  @Override
  public FindPropertyReferencingElements.Match newMatch(final Object... parameters) {
    return FindPropertyReferencingElements.Match.newMatch((org.osate.aadl2.Element) parameters[0]);
  }

  /**
   * Inner class allowing the singleton instance of {@link FindPropertyReferencingElements} to be created 
   *     <b>not</b> at the class load time of the outer class, 
   *     but rather at the first call to {@link FindPropertyReferencingElements#instance()}.
   * 
   * <p> This workaround is required e.g. to support recursion.
   * 
   */
  private static class LazyHolder {
    private static final FindPropertyReferencingElements INSTANCE = new FindPropertyReferencingElements();

    /**
     * Statically initializes the query specification <b>after</b> the field {@link #INSTANCE} is assigned.
     * This initialization order is required to support indirect recursion.
     * 
     * <p> The static initializer is defined using a helper field to work around limitations of the code generator.
     * 
     */
    private static final Object STATIC_INITIALIZER = ensureInitialized();

    public static Object ensureInitialized() {
      INSTANCE.ensureInitializedInternal();
      return null;
    }
  }

  private static class GeneratedPQuery extends BaseGeneratedEMFPQuery {
    private static final FindPropertyReferencingElements.GeneratedPQuery INSTANCE = new GeneratedPQuery();

    private final PParameter parameter_element = new PParameter("element", "org.osate.aadl2.Element", new EClassTransitiveInstancesKey((EClass)getClassifierLiteralSafe("http://aadl.info/AADL/2.0", "Element")), PParameterDirection.INOUT);

    private final List<PParameter> parameters = Arrays.asList(parameter_element);

    private GeneratedPQuery() {
      super(PVisibility.PUBLIC);
    }

    @Override
    public String getFullyQualifiedName() {
      return "fr.mem4csd.osatedim.viatra.queries.findPropertyReferencingElements";
    }

    @Override
    public List<String> getParameterNames() {
      return Arrays.asList("element");
    }

    @Override
    public List<PParameter> getParameters() {
      return parameters;
    }

    @Override
    public Set<PBody> doGetContainedBodies() {
      setEvaluationHints(new QueryEvaluationHint(null, QueryEvaluationHint.BackendRequirement.UNSPECIFIED));
      Set<PBody> bodies = new LinkedHashSet<>();
      {
          PBody body = new PBody(this);
          PVariable var_element = body.getOrCreateVariableByName("element");
          new TypeConstraint(body, Tuples.flatTupleOf(var_element), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Element")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_element, parameter_element)
          ));
          // 	PropertyAssociationInstance(element)
          new TypeConstraint(body, Tuples.flatTupleOf(var_element), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0/instance", "PropertyAssociationInstance")));
          bodies.add(body);
      }
      {
          PBody body = new PBody(this);
          PVariable var_element = body.getOrCreateVariableByName("element");
          new TypeConstraint(body, Tuples.flatTupleOf(var_element), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "Element")));
          body.setSymbolicParameters(Arrays.<ExportedParameter>asList(
             new ExportedParameter(body, var_element, parameter_element)
          ));
          // 	BasicPropertyAssociation(element)
          new TypeConstraint(body, Tuples.flatTupleOf(var_element), new EClassTransitiveInstancesKey((EClass)getClassifierLiteral("http://aadl.info/AADL/2.0", "BasicPropertyAssociation")));
          bodies.add(body);
      }
      return bodies;
    }
  }
}
