package fr.mem4csd.osatedim.viatra.transformations;

import fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTraceSpec;
import fr.mem4csd.osatedim.utils.InstanceHierarchyUtils;
import fr.mem4csd.osatedim.utils.PropertyTypeUtils;
import fr.mem4csd.osatedim.utils.TraceUtils;
import fr.mem4csd.osatedim.viatra.queries.DIMQueriesOutplace;
import fr.mem4csd.osatedim.viatra.queries.Find_PropertyAttach2Component;
import java.util.function.Consumer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.transformation.evm.specific.Lifecycles;
import org.eclipse.viatra.transformation.evm.specific.crud.CRUDActivationStateEnum;
import org.eclipse.viatra.transformation.evm.specific.resolver.InvertedDisappearancePriorityConflictResolver;
import org.eclipse.viatra.transformation.runtime.emf.modelmanipulation.IModelManipulations;
import org.eclipse.viatra.transformation.runtime.emf.modelmanipulation.SimpleModelManipulations;
import org.eclipse.viatra.transformation.runtime.emf.rules.eventdriven.EventDrivenTransformationRule;
import org.eclipse.viatra.transformation.runtime.emf.rules.eventdriven.EventDrivenTransformationRuleFactory;
import org.eclipse.viatra.transformation.runtime.emf.transformation.eventdriven.EventDrivenTransformation;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.InputOutput;
import org.osate.aadl2.Aadl2Package;
import org.osate.aadl2.BasicPropertyAssociation;
import org.osate.aadl2.ListValue;
import org.osate.aadl2.ModalPropertyValue;
import org.osate.aadl2.Mode;
import org.osate.aadl2.PropertyExpression;
import org.osate.aadl2.RecordValue;
import org.osate.aadl2.Subcomponent;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.InstanceObject;
import org.osate.aadl2.instance.InstanceReferenceValue;
import org.osate.aadl2.instance.ModeInstance;
import org.osate.aadl2.instance.SystemInstance;
import org.osate.aadl2.instance.SystemOperationMode;

@SuppressWarnings("all")
public class DIMTransformationDeltaOutplace {
  /**
   * Transformation-related extensions
   */
  @Extension
  private EventDrivenTransformation transformation;

  /**
   * Transformation rule-related extensions
   */
  @Extension
  private EventDrivenTransformationRuleFactory _eventDrivenTransformationRuleFactory = new EventDrivenTransformationRuleFactory();

  @Extension
  private IModelManipulations manipulation;

  /**
   * VIATRA Query Pattern group
   */
  @Extension
  private DIMQueriesOutplace dim_queries = DIMQueriesOutplace.instance();

  /**
   * EMF metamodels
   */
  @Extension
  private Aadl2Package aadl2Package = Aadl2Package.eINSTANCE;

  private ViatraQueryEngine engine;

  protected Resource resource;

  private boolean initialized = false;

  public boolean initialize(final Aaxl2AaxlTraceSpec aaxl2aaxl, final ViatraQueryEngine engine) {
    boolean _xifexpression = false;
    if ((!this.initialized)) {
      boolean _xblockexpression = false;
      {
        this.engine = engine;
        this.dim_queries.prepare(engine);
        SimpleModelManipulations _simpleModelManipulations = new SimpleModelManipulations(engine);
        this.manipulation = _simpleModelManipulations;
        this.createTransformation();
        _xblockexpression = this.initialized = true;
      }
      _xifexpression = _xblockexpression;
    }
    return _xifexpression;
  }

  private EventDrivenTransformation createTransformation() {
    EventDrivenTransformation _xblockexpression = null;
    {
      final InvertedDisappearancePriorityConflictResolver fixedPriorityResolver = new InvertedDisappearancePriorityConflictResolver();
      fixedPriorityResolver.setPriority(this.propertyattach2component.getRuleSpecification(), 4);
      final EventDrivenTransformation.EventDrivenTransformationBuilder builder = EventDrivenTransformation.forEngine(this.engine).setConflictResolver(fixedPriorityResolver).addRule(this.propertyattach2component);
      _xblockexpression = this.transformation = builder.build();
    }
    return _xblockexpression;
  }

  public void execute() {
    this.transformation.getExecutionSchema().startUnscheduledExecution();
  }

  public void dispose() {
    if ((this.transformation != null)) {
      this.transformation.getExecutionSchema().dispose();
      this.transformation = null;
    }
    return;
  }

  private final EventDrivenTransformationRule<Find_PropertyAttach2Component.Match, Find_PropertyAttach2Component.Matcher> propertyattach2component = this._eventDrivenTransformationRuleFactory.<Find_PropertyAttach2Component.Match, Find_PropertyAttach2Component.Matcher>createRule(Find_PropertyAttach2Component.instance()).action(CRUDActivationStateEnum.CREATED, ((Consumer<Find_PropertyAttach2Component.Match>) (Find_PropertyAttach2Component.Match it) -> {
    try {
      Boolean _isSimpleRefProperty = PropertyTypeUtils.isSimpleRefProperty(it.getPropinst().getProperty().getPropertyType());
      if ((_isSimpleRefProperty).booleanValue()) {
        EObject prop2add = this.manipulation.createChild(it.getCompinst().getSubcomponent(), this.aadl2Package.getNamedElement_OwnedPropertyAssociation(), this.aadl2Package.getPropertyAssociation());
        this.manipulation.set(prop2add, this.aadl2Package.getPropertyAssociation_Property(), it.getPropinst().getProperty());
        EList<ModalPropertyValue> _ownedValues = it.getPropinst().getOwnedValues();
        for (final ModalPropertyValue modalprop : _ownedValues) {
          {
            final EObject modprop2add = this.manipulation.createChild(prop2add, this.aadl2Package.getPropertyAssociation_OwnedValue(), this.aadl2Package.getModalPropertyValue());
            boolean _isEmpty = modalprop.getInModes().isEmpty();
            boolean _not = (!_isEmpty);
            if (_not) {
              Mode _get = modalprop.getInModes().get(0);
              ModeInstance _get_1 = ((SystemOperationMode) _get).getCurrentModes().get(0);
              this.manipulation.add(modprop2add, this.aadl2Package.getModalElement_InMode(), ((ModeInstance) _get_1).getMode());
            }
            this.manipulation.set(modprop2add, this.aadl2Package.getModalPropertyValue_OwnedValue(), modalprop.getOwnedValue());
          }
        }
        String _name = it.getPropinst().getProperty().getName();
        String _plus = ("DIM: Simple Value Property " + _name);
        String _plus_1 = (_plus + " attached to Object ");
        String _name_1 = it.getCompinst().getName();
        String _plus_2 = (_plus_1 + _name_1);
        InputOutput.<String>println(_plus_2);
      } else {
        InstanceObject _leftInstanceObject = TraceUtils.getLeftInstanceObject(InstanceHierarchyUtils.getLCPCIForListRefProperty(it.getPropinst()), it.getTrace());
        final ComponentInstance compinst2att = ((ComponentInstance) _leftInstanceObject);
        Subcomponent _subcomponent = compinst2att.getSubcomponent();
        boolean _tripleEquals = (_subcomponent == null);
        if (_tripleEquals) {
          final EObject prop2add_1 = this.manipulation.createChild(((SystemInstance) compinst2att).getComponentImplementation(), this.aadl2Package.getNamedElement_OwnedPropertyAssociation(), this.aadl2Package.getPropertyAssociation());
          this.manipulation.set(prop2add_1, this.aadl2Package.getPropertyAssociation_Property(), it.getPropinst().getProperty());
          final EObject contelem = this.manipulation.createChild(prop2add_1, this.aadl2Package.getPropertyAssociation_AppliesTo(), this.aadl2Package.getContainedNamedElement());
          final EObject firstpath2add = this.manipulation.createChild(contelem, this.aadl2Package.getContainedNamedElement_Path(), this.aadl2Package.getContainmentPathElement());
          this.manipulation.set(firstpath2add, this.aadl2Package.getContainmentPathElement_NamedElement(), InstanceHierarchyUtils.getContainmentPath(compinst2att, it.getCompinst()).get(0).getSubcomponent());
          EObject path2add = firstpath2add;
          EList<ComponentInstance> _containmentPath = InstanceHierarchyUtils.getContainmentPath(compinst2att, it.getCompinst());
          for (final ComponentInstance pathcompinst : _containmentPath) {
            int _indexOf = InstanceHierarchyUtils.getContainmentPath(compinst2att, it.getCompinst()).indexOf(pathcompinst);
            boolean _equals = (_indexOf == 0);
            if (_equals) {
            } else {
              path2add = this.manipulation.createChild(path2add, this.aadl2Package.getContainmentPathElement_Path(), this.aadl2Package.getContainmentPathElement());
              this.manipulation.set(path2add, this.aadl2Package.getContainmentPathElement_NamedElement(), pathcompinst);
            }
          }
          EList<ModalPropertyValue> _ownedValues_1 = it.getPropinst().getOwnedValues();
          for (final ModalPropertyValue modalprop_1 : _ownedValues_1) {
            {
              final EObject modprop2add = this.manipulation.createChild(prop2add_1, this.aadl2Package.getPropertyAssociation_OwnedValue(), this.aadl2Package.getModalPropertyValue());
              boolean _isEmpty = modalprop_1.getInModes().isEmpty();
              boolean _not = (!_isEmpty);
              if (_not) {
                EList<Mode> _inModes = modalprop_1.getInModes();
                for (final Mode som : _inModes) {
                  ModeInstance _get = ((SystemOperationMode) som).getCurrentModes().get(0);
                  this.manipulation.add(modprop2add, this.aadl2Package.getModalElement_InMode(), ((ModeInstance) _get).getMode());
                }
              }
              final EObject listval2add = this.manipulation.createChild(modprop2add, this.aadl2Package.getModalPropertyValue_OwnedValue(), this.aadl2Package.getListValue());
              PropertyExpression _ownedValue = modalprop_1.getOwnedValue();
              EList<PropertyExpression> _ownedListElements = ((ListValue) _ownedValue).getOwnedListElements();
              for (final PropertyExpression recordval : _ownedListElements) {
                {
                  final EObject recordval2add = this.manipulation.createChild(listval2add, this.aadl2Package.getListValue_OwnedListElement(), this.aadl2Package.getRecordValue());
                  EList<BasicPropertyAssociation> _ownedFieldValues = ((RecordValue) recordval).getOwnedFieldValues();
                  for (final BasicPropertyAssociation basicpropassoc : _ownedFieldValues) {
                    {
                      final EObject bpa2add = this.manipulation.createChild(recordval2add, this.aadl2Package.getRecordValue_OwnedFieldValue(), this.aadl2Package.getBasicPropertyAssociation());
                      this.manipulation.set(bpa2add, this.aadl2Package.getBasicPropertyAssociation_Property(), basicpropassoc.getProperty());
                      PropertyExpression _ownedValue_1 = basicpropassoc.getOwnedValue();
                      if ((_ownedValue_1 instanceof InstanceReferenceValue)) {
                        final EObject val2add = this.manipulation.createChild(bpa2add, this.aadl2Package.getBasicPropertyAssociation_OwnedValue(), this.aadl2Package.getReferenceValue());
                        EObject firstrefpath2add = this.manipulation.createChild(val2add, this.aadl2Package.getContainedNamedElement_Path(), this.aadl2Package.getContainmentPathElement());
                        PropertyExpression _ownedValue_2 = basicpropassoc.getOwnedValue();
                        InstanceObject _referencedInstanceObject = ((InstanceReferenceValue) _ownedValue_2).getReferencedInstanceObject();
                        InstanceObject _leftInstanceObject_1 = TraceUtils.getLeftInstanceObject(((ComponentInstance) _referencedInstanceObject), it.getTrace());
                        final ComponentInstance refcompinst = ((ComponentInstance) _leftInstanceObject_1);
                        this.manipulation.set(firstrefpath2add, this.aadl2Package.getContainmentPathElement_NamedElement(), InstanceHierarchyUtils.getContainmentPath(compinst2att, refcompinst).get(0).getSubcomponent());
                        EObject refpath2add = firstrefpath2add;
                        EList<ComponentInstance> _containmentPath_1 = InstanceHierarchyUtils.getContainmentPath(compinst2att, refcompinst);
                        for (final ComponentInstance pathcompinst_1 : _containmentPath_1) {
                          int _indexOf_1 = InstanceHierarchyUtils.getContainmentPath(compinst2att, refcompinst).indexOf(pathcompinst_1);
                          boolean _equals_1 = (_indexOf_1 == 0);
                          if (_equals_1) {
                          } else {
                            refpath2add = this.manipulation.createChild(refpath2add, this.aadl2Package.getContainmentPathElement_Path(), this.aadl2Package.getContainmentPathElement());
                            this.manipulation.set(refpath2add, this.aadl2Package.getContainmentPathElement_NamedElement(), pathcompinst_1);
                          }
                        }
                      } else {
                        this.manipulation.set(bpa2add, this.aadl2Package.getBasicPropertyAssociation_OwnedValue(), basicpropassoc.getOwnedValue());
                      }
                    }
                  }
                }
              }
            }
          }
        } else {
          final EObject prop2add_2 = this.manipulation.createChild(((SystemInstance) compinst2att).getSubcomponent(), this.aadl2Package.getNamedElement_OwnedPropertyAssociation(), this.aadl2Package.getPropertyAssociation());
          this.manipulation.set(prop2add_2, this.aadl2Package.getPropertyAssociation_Property(), it.getPropinst().getProperty());
          final EObject contelem_1 = this.manipulation.createChild(prop2add_2, this.aadl2Package.getPropertyAssociation_AppliesTo(), this.aadl2Package.getContainedNamedElement());
          final EObject firstpath2add_1 = this.manipulation.createChild(contelem_1, this.aadl2Package.getContainedNamedElement_Path(), this.aadl2Package.getContainmentPathElement());
          this.manipulation.set(firstpath2add_1, this.aadl2Package.getContainmentPathElement_NamedElement(), InstanceHierarchyUtils.getContainmentPath(compinst2att, it.getCompinst()).get(0).getSubcomponent());
          EObject path2add_1 = firstpath2add_1;
          EList<ComponentInstance> _containmentPath_1 = InstanceHierarchyUtils.getContainmentPath(compinst2att, it.getCompinst());
          for (final ComponentInstance pathcompinst_1 : _containmentPath_1) {
            int _indexOf_1 = InstanceHierarchyUtils.getContainmentPath(compinst2att, it.getCompinst()).indexOf(pathcompinst_1);
            boolean _equals_1 = (_indexOf_1 == 0);
            if (_equals_1) {
            } else {
              path2add_1 = this.manipulation.createChild(path2add_1, this.aadl2Package.getContainmentPathElement_Path(), this.aadl2Package.getContainmentPathElement());
              this.manipulation.set(path2add_1, this.aadl2Package.getContainmentPathElement_NamedElement(), pathcompinst_1);
            }
          }
          EList<ModalPropertyValue> _ownedValues_2 = it.getPropinst().getOwnedValues();
          for (final ModalPropertyValue modalprop_2 : _ownedValues_2) {
            {
              final EObject modprop2add = this.manipulation.createChild(prop2add_2, this.aadl2Package.getPropertyAssociation_OwnedValue(), this.aadl2Package.getModalPropertyValue());
              boolean _isEmpty = modalprop_2.getInModes().isEmpty();
              boolean _not = (!_isEmpty);
              if (_not) {
                EList<Mode> _inModes = modalprop_2.getInModes();
                for (final Mode som : _inModes) {
                  ModeInstance _get = ((SystemOperationMode) som).getCurrentModes().get(0);
                  this.manipulation.add(modprop2add, this.aadl2Package.getModalElement_InMode(), ((ModeInstance) _get).getMode());
                }
              }
              final EObject listval2add = this.manipulation.createChild(modprop2add, this.aadl2Package.getModalPropertyValue_OwnedValue(), this.aadl2Package.getListValue());
              PropertyExpression _ownedValue = modalprop_2.getOwnedValue();
              EList<PropertyExpression> _ownedListElements = ((ListValue) _ownedValue).getOwnedListElements();
              for (final PropertyExpression recordval : _ownedListElements) {
                {
                  final EObject recordval2add = this.manipulation.createChild(listval2add, this.aadl2Package.getListValue_OwnedListElement(), this.aadl2Package.getRecordValue());
                  EList<BasicPropertyAssociation> _ownedFieldValues = ((RecordValue) recordval).getOwnedFieldValues();
                  for (final BasicPropertyAssociation basicpropassoc : _ownedFieldValues) {
                    {
                      final EObject bpa2add = this.manipulation.createChild(recordval2add, this.aadl2Package.getRecordValue_OwnedFieldValue(), this.aadl2Package.getBasicPropertyAssociation());
                      this.manipulation.set(bpa2add, this.aadl2Package.getBasicPropertyAssociation_Property(), basicpropassoc.getProperty());
                      PropertyExpression _ownedValue_1 = basicpropassoc.getOwnedValue();
                      if ((_ownedValue_1 instanceof InstanceReferenceValue)) {
                        final EObject val2add = this.manipulation.createChild(bpa2add, this.aadl2Package.getBasicPropertyAssociation_OwnedValue(), this.aadl2Package.getReferenceValue());
                        EObject firstrefpath2add = this.manipulation.createChild(val2add, this.aadl2Package.getContainedNamedElement_Path(), this.aadl2Package.getContainmentPathElement());
                        PropertyExpression _ownedValue_2 = basicpropassoc.getOwnedValue();
                        InstanceObject _referencedInstanceObject = ((InstanceReferenceValue) _ownedValue_2).getReferencedInstanceObject();
                        InstanceObject _leftInstanceObject_1 = TraceUtils.getLeftInstanceObject(((ComponentInstance) _referencedInstanceObject), it.getTrace());
                        final ComponentInstance refcompinst = ((ComponentInstance) _leftInstanceObject_1);
                        this.manipulation.set(firstrefpath2add, this.aadl2Package.getContainmentPathElement_NamedElement(), InstanceHierarchyUtils.getContainmentPath(compinst2att, refcompinst).get(0).getSubcomponent());
                        EObject refpath2add = firstrefpath2add;
                        EList<ComponentInstance> _containmentPath_2 = InstanceHierarchyUtils.getContainmentPath(compinst2att, refcompinst);
                        for (final ComponentInstance pathcompinst_2 : _containmentPath_2) {
                          int _indexOf_2 = InstanceHierarchyUtils.getContainmentPath(compinst2att, refcompinst).indexOf(pathcompinst_2);
                          boolean _equals_2 = (_indexOf_2 == 0);
                          if (_equals_2) {
                          } else {
                            refpath2add = this.manipulation.createChild(refpath2add, this.aadl2Package.getContainmentPathElement_Path(), this.aadl2Package.getContainmentPathElement());
                            this.manipulation.set(refpath2add, this.aadl2Package.getContainmentPathElement_NamedElement(), pathcompinst_2);
                          }
                        }
                      } else {
                        this.manipulation.set(bpa2add, this.aadl2Package.getBasicPropertyAssociation_OwnedValue(), basicpropassoc.getOwnedValue());
                      }
                    }
                  }
                }
              }
            }
          }
        }
        String _name_2 = it.getPropinst().getProperty().getName();
        String _plus_3 = ("DIM: Complex Reference Property " + _name_2);
        String _plus_4 = (_plus_3 + " attached to Object ");
        String _name_3 = it.getCompinst().getName();
        String _plus_5 = (_plus_4 + _name_3);
        InputOutput.<String>println(_plus_5);
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).action(CRUDActivationStateEnum.UPDATED, ((Consumer<Find_PropertyAttach2Component.Match>) (Find_PropertyAttach2Component.Match it) -> {
  })).action(CRUDActivationStateEnum.DELETED, ((Consumer<Find_PropertyAttach2Component.Match>) (Find_PropertyAttach2Component.Match it) -> {
  })).addLifeCycle(Lifecycles.getDefault(true, true)).build();
}
