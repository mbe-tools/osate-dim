package fr.mem4csd.osatedim.viatra.transformations;

import com.google.common.base.Objects;
import fr.mem4csd.osatedim.preference.DIMPreferences;
import fr.mem4csd.osatedim.utils.AnnexUtils;
import fr.mem4csd.osatedim.utils.DIMLogger;
import fr.mem4csd.osatedim.utils.PropertyUtils;
import fr.mem4csd.osatedim.viatra.queries.FindConnection;
import fr.mem4csd.osatedim.viatra.queries.FindConnectionReference;
import fr.mem4csd.osatedim.viatra.queries.FindDerivedMode;
import fr.mem4csd.osatedim.viatra.queries.FindExtraComponent;
import fr.mem4csd.osatedim.viatra.queries.FindFeature;
import fr.mem4csd.osatedim.viatra.queries.FindInstanceObject;
import fr.mem4csd.osatedim.viatra.queries.FindMode;
import fr.mem4csd.osatedim.viatra.queries.FindModeTransition;
import fr.mem4csd.osatedim.viatra.queries.FindProperty;
import fr.mem4csd.osatedim.viatra.queries.FindPropertyReferencingElements;
import fr.mem4csd.osatedim.viatra.queries.FindPropertyValue;
import fr.mem4csd.osatedim.viatra.queries.FindSubcomponent;
import fr.mem4csd.osatedim.viatra.queries.FindSystem;
import fr.mem4csd.osatedim.viatra.transformations.MScheduler;
import java.io.FileNotFoundException;
import java.util.function.Consumer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.InternalEObject;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.transformation.evm.specific.Lifecycles;
import org.eclipse.viatra.transformation.evm.specific.crud.CRUDActivationStateEnum;
import org.eclipse.viatra.transformation.evm.specific.resolver.InvertedDisappearancePriorityConflictResolver;
import org.eclipse.viatra.transformation.runtime.emf.modelmanipulation.SimpleModelManipulations;
import org.eclipse.viatra.transformation.runtime.emf.rules.eventdriven.EventDrivenTransformationRule;
import org.eclipse.viatra.transformation.runtime.emf.transformation.eventdriven.EventDrivenTransformation;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.Extension;
import org.osate.aadl2.AadlPackage;
import org.osate.aadl2.BasicProperty;
import org.osate.aadl2.BasicPropertyAssociation;
import org.osate.aadl2.ClassifierValue;
import org.osate.aadl2.ComponentCategory;
import org.osate.aadl2.Element;
import org.osate.aadl2.ModalPropertyValue;
import org.osate.aadl2.NamedElement;
import org.osate.aadl2.Property;
import org.osate.aadl2.PropertySet;
import org.osate.aadl2.PublicPackageSection;
import org.osate.aadl2.ReferenceValue;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.ConnectionInstance;
import org.osate.aadl2.instance.ConnectionReference;
import org.osate.aadl2.instance.FeatureInstance;
import org.osate.aadl2.instance.InstanceObject;
import org.osate.aadl2.instance.ModeInstance;
import org.osate.aadl2.instance.ModeTransitionInstance;
import org.osate.aadl2.instance.PropertyAssociationInstance;
import org.osate.aadl2.instance.SystemInstance;
import org.osate.aadl2.modelsupport.FileNameConstants;

@SuppressWarnings("all")
public class DIMTransformationState extends DIMTransformationRulesState {
  @Extension
  public EventDrivenTransformation transformation;

  public DIMTransformationState(final ViatraQueryEngine engine, final SystemInstance topSystemInst, final PublicPackageSection aadlPublicPackage, final DIMPreferences preferences, final PropertySet dimProperties, final DIMLogger logger) {
    this.topSystemInst = topSystemInst;
    this.aadlPublicPackage = aadlPublicPackage;
    this.engine = engine;
    this.preferences = preferences;
    this.dimPropertySet = dimProperties;
    this.LOGGER = logger;
    this.DIMQueries.prepare(engine);
    this.createTransformation();
  }

  public void execute() {
    this.transformation.getExecutionSchema().startUnscheduledExecution();
  }

  public void dispose() {
    if ((this.transformation != null)) {
      this.transformation.getExecutionSchema().dispose();
      this.transformation = null;
    }
    return;
  }

  private EventDrivenTransformation createTransformation() {
    EventDrivenTransformation _xblockexpression = null;
    {
      SimpleModelManipulations _simpleModelManipulations = new SimpleModelManipulations(this.engine);
      this.manipulation = _simpleModelManipulations;
      InvertedDisappearancePriorityConflictResolver fixedPriorityResolver = new InvertedDisappearancePriorityConflictResolver();
      fixedPriorityResolver.setPriority(this.checkInstancePropertyExistence.getRuleSpecification(), 1);
      fixedPriorityResolver.setPriority(this.cleanInstance2Declarative.getRuleSpecification(), 2);
      fixedPriorityResolver.setPriority(this.cleanPropertyInstance2Declarative.getRuleSpecification(), 2);
      fixedPriorityResolver.setPriority(this.extraComponentInstance2Declarative.getRuleSpecification(), 3);
      fixedPriorityResolver.setPriority(this.topSystemInstance2Declarative.getRuleSpecification(), 3);
      fixedPriorityResolver.setPriority(this.componentInstance2Declarative.getRuleSpecification(), 4);
      fixedPriorityResolver.setPriority(this.featureInstance2Declarative.getRuleSpecification(), 5);
      fixedPriorityResolver.setPriority(this.connectionInstance2Reference.getRuleSpecification(), 6);
      fixedPriorityResolver.setPriority(this.connectionReference2Declarative.getRuleSpecification(), 7);
      fixedPriorityResolver.setPriority(this.modeInstance2Declarative.getRuleSpecification(), 8);
      fixedPriorityResolver.setPriority(this.derivedModeInstance2Declarative.getRuleSpecification(), 9);
      fixedPriorityResolver.setPriority(this.modeTransitionInstance2Declarative.getRuleSpecification(), 10);
      fixedPriorityResolver.setPriority(this.propertyInstance2Declarative.getRuleSpecification(), 11);
      EventDrivenTransformation.EventDrivenTransformationBuilder builder = EventDrivenTransformation.forEngine(this.engine).setConflictResolver(fixedPriorityResolver).addRule(this.checkInstancePropertyExistence).addRule(this.cleanInstance2Declarative).addRule(this.cleanPropertyInstance2Declarative).addRule(this.extraComponentInstance2Declarative).addRule(this.topSystemInstance2Declarative).addRule(this.componentInstance2Declarative).addRule(this.featureInstance2Declarative).addRule(this.connectionInstance2Reference).addRule(this.connectionReference2Declarative).addRule(this.modeInstance2Declarative).addRule(this.derivedModeInstance2Declarative).addRule(this.modeTransitionInstance2Declarative).addRule(this.propertyInstance2Declarative);
      MScheduler.MSchedulerFactory _mSchedulerFactory = new MScheduler.MSchedulerFactory();
      builder.setSchedulerFactory(_mSchedulerFactory);
      _xblockexpression = this.transformation = builder.build();
    }
    return _xblockexpression;
  }

  protected final EventDrivenTransformationRule<FindPropertyReferencingElements.Match, FindPropertyReferencingElements.Matcher> checkInstancePropertyExistence = this._eventDrivenTransformationRuleFactory.<FindPropertyReferencingElements.Match, FindPropertyReferencingElements.Matcher>createRule(FindPropertyReferencingElements.instance()).action(CRUDActivationStateEnum.CREATED, ((Consumer<FindPropertyReferencingElements.Match>) (FindPropertyReferencingElements.Match it) -> {
    try {
      BasicProperty _xifexpression = null;
      Element _element = it.getElement();
      if ((_element instanceof PropertyAssociationInstance)) {
        Element _element_1 = it.getElement();
        Property _property = ((PropertyAssociationInstance) _element_1).getProperty();
        _xifexpression = ((BasicProperty) _property);
      } else {
        BasicProperty _xifexpression_1 = null;
        Element _element_2 = it.getElement();
        if ((_element_2 instanceof BasicPropertyAssociation)) {
          Element _element_3 = it.getElement();
          _xifexpression_1 = ((BasicPropertyAssociation) _element_3).getProperty();
        }
        _xifexpression = _xifexpression_1;
      }
      final BasicProperty property = _xifexpression;
      EObject _eContainer = property.eContainer();
      boolean _tripleEquals = (_eContainer == null);
      if (_tripleEquals) {
        String _string = ((InternalEObject) property).eProxyURI().toString();
        String _plus = (_string + " not found!");
        throw new FileNotFoundException(_plus);
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).addLifeCycle(Lifecycles.getDefault(true, true)).build();

  protected final EventDrivenTransformationRule<FindInstanceObject.Match, FindInstanceObject.Matcher> cleanInstance2Declarative = this._eventDrivenTransformationRuleFactory.<FindInstanceObject.Match, FindInstanceObject.Matcher>createRule(FindInstanceObject.instance()).action(CRUDActivationStateEnum.CREATED, ((Consumer<FindInstanceObject.Match>) (FindInstanceObject.Match it) -> {
    try {
      InstanceObject _objinst = it.getObjinst();
      if ((_objinst instanceof ComponentInstance)) {
        this.manipulation.set(it.getObjinst(), this.instPackage.getComponentInstance_Classifier(), null);
        String _name = it.getObjinst().getName();
        String _plus = ("DIM: Component instance " + _name);
        String _plus_1 = (_plus + " cleaned");
        this.LOGGER.logInfo(_plus_1);
      } else {
        InstanceObject _objinst_1 = it.getObjinst();
        if ((_objinst_1 instanceof FeatureInstance)) {
          this.manipulation.set(it.getObjinst(), this.instPackage.getFeatureInstance_Feature(), null);
          String _name_1 = it.getObjinst().getName();
          String _plus_2 = ("DIM: Feature instance " + _name_1);
          String _plus_3 = (_plus_2 + " cleaned");
          this.LOGGER.logInfo(_plus_3);
        } else {
          InstanceObject _objinst_2 = it.getObjinst();
          if ((_objinst_2 instanceof ConnectionInstance)) {
            InstanceObject _objinst_3 = it.getObjinst();
            EList<ConnectionReference> _connectionReferences = ((ConnectionInstance) _objinst_3).getConnectionReferences();
            for (final ConnectionReference connref : _connectionReferences) {
              this.manipulation.set(connref, this.instPackage.getConnectionReference_Connection(), null);
            }
            String _name_2 = it.getObjinst().getName();
            String _plus_4 = ("DIM: Connection instance " + _name_2);
            String _plus_5 = (_plus_4 + " cleaned");
            this.LOGGER.logInfo(_plus_5);
          } else {
            InstanceObject _objinst_4 = it.getObjinst();
            if ((_objinst_4 instanceof ModeInstance)) {
              this.manipulation.set(it.getObjinst(), this.instPackage.getModeInstance_Mode(), null);
              String _name_3 = it.getObjinst().getName();
              String _plus_6 = ("DIM: Mode instance " + _name_3);
              String _plus_7 = (_plus_6 + " cleaned");
              this.LOGGER.logInfo(_plus_7);
            } else {
              InstanceObject _objinst_5 = it.getObjinst();
              if ((_objinst_5 instanceof ModeTransitionInstance)) {
                this.manipulation.set(it.getObjinst(), this.instPackage.getModeTransitionInstance_ModeTransition(), null);
                String _name_4 = it.getObjinst().getName();
                String _plus_8 = ("DIM: Mode Transition instance " + _name_4);
                String _plus_9 = (_plus_8 + " cleaned");
                this.LOGGER.logInfo(_plus_9);
              } else {
                InstanceObject _objinst_6 = it.getObjinst();
                if ((_objinst_6 instanceof PropertyAssociationInstance)) {
                  this.manipulation.set(it.getObjinst(), this.instPackage.getPropertyAssociationInstance_PropertyAssociation(), null);
                  InstanceObject _objinst_7 = it.getObjinst();
                  String _name_5 = ((PropertyAssociationInstance) _objinst_7).getProperty().getName();
                  String _plus_10 = ("DIM: Property instance " + _name_5);
                  String _plus_11 = (_plus_10 + " cleaned");
                  this.LOGGER.logInfo(_plus_11);
                }
              }
            }
          }
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).addLifeCycle(Lifecycles.getDefault(true, true)).build();

  protected final EventDrivenTransformationRule<FindPropertyValue.Match, FindPropertyValue.Matcher> cleanPropertyInstance2Declarative = this._eventDrivenTransformationRuleFactory.<FindPropertyValue.Match, FindPropertyValue.Matcher>createRule(FindPropertyValue.instance()).action(CRUDActivationStateEnum.CREATED, ((Consumer<FindPropertyValue.Match>) (FindPropertyValue.Match it) -> {
    try {
      final ModalPropertyValue modpropinst = PropertyUtils.getContainingModalPropertyValue(it.getPropvalinst());
      if (((modpropinst != null) && (modpropinst.eContainer() instanceof PropertyAssociationInstance))) {
        if (((it.getPropvalinst() instanceof ClassifierValue) || (it.getPropvalinst() instanceof ReferenceValue))) {
          this.manipulation.remove(modpropinst.eContainer());
          EObject _eContainer = modpropinst.eContainer();
          String _name = ((PropertyAssociationInstance) _eContainer).getProperty().getName();
          String _plus = ("DIM: Property instance " + _name);
          String _plus_1 = (_plus + " removed since it has declarative references.");
          this.LOGGER.logInfo(_plus_1);
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).addLifeCycle(Lifecycles.getDefault(true, true)).build();

  protected final EventDrivenTransformationRule<FindExtraComponent.Match, FindExtraComponent.Matcher> extraComponentInstance2Declarative = this._eventDrivenTransformationRuleFactory.<FindExtraComponent.Match, FindExtraComponent.Matcher>createRule(FindExtraComponent.Matcher.querySpecification()).action(CRUDActivationStateEnum.CREATED, ((Consumer<FindExtraComponent.Match>) (FindExtraComponent.Match it) -> {
    try {
      ComponentCategory _category = it.getCompinst().getCategory();
      boolean _equals = Objects.equal(_category, ComponentCategory.DATA);
      if (_equals) {
        EObject datatype = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getDataType());
        boolean _contains = it.getCompinst().getName().contains(FileNameConstants.AADL_PACKAGE_SEPARATOR);
        if (_contains) {
          String _name = it.getCompinst().getName();
          int _indexOf = it.getCompinst().getName().indexOf(FileNameConstants.AADL_PACKAGE_SEPARATOR);
          int _plus = (_indexOf + 2);
          this.manipulation.set(datatype, this.aadl2Package.getNamedElement_Name(), _name.substring(_plus));
          final String packagename = it.getCompinst().getName().substring(0, it.getCompinst().getName().indexOf(FileNameConstants.AADL_PACKAGE_SEPARATOR));
          boolean _isAnnexContributionPackage = AnnexUtils.isAnnexContributionPackage(packagename);
          if (_isAnnexContributionPackage) {
            final AadlPackage pack = AnnexUtils.loadAnnexPackage(this.aadlPublicPackage.eResource().getResourceSet(), packagename);
            boolean _contains_1 = this.aadlPublicPackage.getImportedUnits().contains(pack);
            boolean _not = (!_contains_1);
            if (_not) {
              boolean _isNoAnnexes = this.aadlPublicPackage.isNoAnnexes();
              boolean _tripleEquals = (Boolean.valueOf(_isNoAnnexes) == Boolean.valueOf(true));
              if (_tripleEquals) {
                this.manipulation.set(this.aadlPublicPackage, this.aadl2Package.getPackageSection_NoAnnexes(), Boolean.valueOf(false));
              }
              this.manipulation.add(this.aadlPublicPackage, this.aadl2Package.getPackageSection_ImportedUnit(), pack);
            }
            this.manipulation.set(datatype, this.aadl2Package.getComponentType_Extended(), AnnexUtils.getClassifier(pack.getOwnedPublicSection(), ((NamedElement) datatype).getName()));
          }
        } else {
          this.manipulation.set(datatype, this.aadl2Package.getNamedElement_Name(), it.getCompinst().getName());
        }
        this.manipulation.set(it.getCompinst(), this.instPackage.getComponentInstance_Classifier(), datatype);
        String _name_1 = it.getCompinst().getName();
        String _plus_1 = ("DIM: Extra data component instance " + _name_1);
        String _plus_2 = (_plus_1 + " de-instantiated");
        this.LOGGER.logInfo(_plus_2);
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).addLifeCycle(Lifecycles.getDefault(true, true)).build();

  protected final EventDrivenTransformationRule<FindSystem.Match, FindSystem.Matcher> topSystemInstance2Declarative = this._eventDrivenTransformationRuleFactory.<FindSystem.Match, FindSystem.Matcher>createRule(FindSystem.instance()).action(CRUDActivationStateEnum.CREATED, ((Consumer<FindSystem.Match>) (FindSystem.Match it) -> {
    try {
      EObject systemtype = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getSystemType());
      this.manipulation.set(systemtype, this.aadl2Package.getNamedElement_Name(), it.getSysteminst().getName().substring(0, it.getSysteminst().getName().indexOf("_")));
      EObject systemimp = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getSystemImplementation());
      EAttribute _namedElement_Name = this.aadl2Package.getNamedElement_Name();
      String _substring = it.getSysteminst().getName().substring(0, it.getSysteminst().getName().indexOf("_"));
      String _plus = (_substring + ".");
      String _name = it.getSysteminst().getName();
      int _indexOf = it.getSysteminst().getName().indexOf("_");
      int _plus_1 = (_indexOf + 1);
      String _substring_1 = _name.substring(_plus_1, it.getSysteminst().getName().indexOf(FileNameConstants.INSTANCE_MODEL_POSTFIX));
      String _plus_2 = (_plus + _substring_1);
      this.manipulation.set(systemimp, _namedElement_Name, _plus_2);
      this.manipulation.set(systemimp, this.aadl2Package.getComponentImplementation_Type(), systemtype);
      this.manipulation.set(it.getSysteminst(), this.instPackage.getSystemInstance_ComponentImplementation(), systemimp);
      this.manipulation.set(it.getSysteminst(), this.instPackage.getComponentInstance_Classifier(), systemimp);
      String _name_1 = it.getSysteminst().getName();
      String _plus_3 = ("DIM: Top-level system instance " + _name_1);
      String _plus_4 = (_plus_3 + " de-instantiated");
      this.LOGGER.logInfo(_plus_4);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).addLifeCycle(Lifecycles.getDefault(true, true)).build();

  protected final EventDrivenTransformationRule<FindSubcomponent.Match, FindSubcomponent.Matcher> componentInstance2Declarative = this._eventDrivenTransformationRuleFactory.<FindSubcomponent.Match, FindSubcomponent.Matcher>createRule(FindSubcomponent.instance()).action(CRUDActivationStateEnum.CREATED, ((Consumer<FindSubcomponent.Match>) (FindSubcomponent.Match it) -> {
    this.componentInstanceCreatedDIM(it.getSubcompinst());
    String _name = it.getSubcompinst().getName();
    String _plus = ("DIM: Component instance " + _name);
    String _plus_1 = (_plus + " de-instantiated");
    this.LOGGER.logInfo(_plus_1);
  })).addLifeCycle(Lifecycles.getDefault(true, true)).build();

  protected final EventDrivenTransformationRule<FindFeature.Match, FindFeature.Matcher> featureInstance2Declarative = this._eventDrivenTransformationRuleFactory.<FindFeature.Match, FindFeature.Matcher>createRule(FindFeature.instance()).action(CRUDActivationStateEnum.CREATED, ((Consumer<FindFeature.Match>) (FindFeature.Match it) -> {
    this.featureInstanceCreatedDIM(it.getFeatinst());
    String _name = it.getFeatinst().getName();
    String _plus = ("DIM: Feature instance " + _name);
    String _plus_1 = (_plus + " de-instantiated");
    this.LOGGER.logInfo(_plus_1);
  })).addLifeCycle(Lifecycles.getDefault(true, true)).build();

  protected final EventDrivenTransformationRule<FindConnection.Match, FindConnection.Matcher> connectionInstance2Reference = this._eventDrivenTransformationRuleFactory.<FindConnection.Match, FindConnection.Matcher>createRule(FindConnection.instance()).action(CRUDActivationStateEnum.CREATED, ((Consumer<FindConnection.Match>) (FindConnection.Match it) -> {
    this.connectionInstanceCreatedDIM(it.getConninst());
    String _name = it.getConninst().getName();
    String _plus = ("DIM: Connection instance " + _name);
    String _plus_1 = (_plus + " defined as connection reference");
    this.LOGGER.logInfo(_plus_1);
  })).addLifeCycle(Lifecycles.getDefault(true, true)).build();

  protected final EventDrivenTransformationRule<FindConnectionReference.Match, FindConnectionReference.Matcher> connectionReference2Declarative = this._eventDrivenTransformationRuleFactory.<FindConnectionReference.Match, FindConnectionReference.Matcher>createRule(FindConnectionReference.instance()).action(CRUDActivationStateEnum.CREATED, ((Consumer<FindConnectionReference.Match>) (FindConnectionReference.Match it) -> {
    this.connectionReferenceFoundDIM(it.getConnref());
    String _name = it.getConnref().getConnection().getName();
    String _plus = ("DIM: Connection reference " + _name);
    String _plus_1 = (_plus + " de-instantiated");
    this.LOGGER.logInfo(_plus_1);
  })).addLifeCycle(Lifecycles.getDefault(true, true)).build();

  protected final EventDrivenTransformationRule<FindMode.Match, FindMode.Matcher> modeInstance2Declarative = this._eventDrivenTransformationRuleFactory.<FindMode.Match, FindMode.Matcher>createRule(FindMode.instance()).action(CRUDActivationStateEnum.CREATED, ((Consumer<FindMode.Match>) (FindMode.Match it) -> {
    this.modeInstanceCreatedDIM(it.getModeinst());
    String _name = it.getModeinst().getName();
    String _plus = ("DIM: Mode instance " + _name);
    String _plus_1 = (_plus + " for component ");
    EObject _eContainer = it.getModeinst().eContainer();
    String _name_1 = ((InstanceObject) _eContainer).getName();
    String _plus_2 = (_plus_1 + _name_1);
    String _plus_3 = (_plus_2 + " de-instantiated");
    this.LOGGER.logInfo(_plus_3);
  })).addLifeCycle(Lifecycles.getDefault(true, true)).build();

  protected final EventDrivenTransformationRule<FindDerivedMode.Match, FindDerivedMode.Matcher> derivedModeInstance2Declarative = this._eventDrivenTransformationRuleFactory.<FindDerivedMode.Match, FindDerivedMode.Matcher>createRule(FindDerivedMode.instance()).action(CRUDActivationStateEnum.CREATED, ((Consumer<FindDerivedMode.Match>) (FindDerivedMode.Match it) -> {
    this.modeInstanceCreatedDIM(it.getModeinst());
    String _name = it.getModeinst().getName();
    String _plus = ("DIM: Mode instance " + _name);
    String _plus_1 = (_plus + " for component ");
    EObject _eContainer = it.getModeinst().eContainer();
    String _name_1 = ((InstanceObject) _eContainer).getName();
    String _plus_2 = (_plus_1 + _name_1);
    String _plus_3 = (_plus_2 + " de-instantiated");
    this.LOGGER.logInfo(_plus_3);
  })).addLifeCycle(Lifecycles.getDefault(true, true)).build();

  protected final EventDrivenTransformationRule<FindModeTransition.Match, FindModeTransition.Matcher> modeTransitionInstance2Declarative = this._eventDrivenTransformationRuleFactory.<FindModeTransition.Match, FindModeTransition.Matcher>createRule(FindModeTransition.instance()).action(CRUDActivationStateEnum.CREATED, ((Consumer<FindModeTransition.Match>) (FindModeTransition.Match it) -> {
    this.modeTransitionInstanceCreatedDIM(it.getModetransinst());
    String _name = it.getModetransinst().getName();
    String _plus = ("DIM: ModeTransition instance " + _name);
    String _plus_1 = (_plus + " de-instantiated");
    this.LOGGER.logInfo(_plus_1);
  })).addLifeCycle(Lifecycles.getDefault(true, true)).build();

  protected final EventDrivenTransformationRule<FindProperty.Match, FindProperty.Matcher> propertyInstance2Declarative = this._eventDrivenTransformationRuleFactory.<FindProperty.Match, FindProperty.Matcher>createRule(FindProperty.instance()).action(CRUDActivationStateEnum.CREATED, ((Consumer<FindProperty.Match>) (FindProperty.Match it) -> {
    boolean _isInheritedProperty = PropertyUtils.isInheritedProperty(it.getPropinst());
    boolean _not = (!_isInheritedProperty);
    if (_not) {
      this.propertyInstanceFoundDIM(it.getPropinst(), this.aadlPublicPackage);
      String _name = it.getPropinst().getProperty().getName();
      String _plus = ("DIM: Property instance " + _name);
      String _plus_1 = (_plus + " attached to ");
      EObject _eContainer = it.getPropinst().eContainer();
      String _name_1 = ((InstanceObject) _eContainer).getName();
      String _plus_2 = (_plus_1 + _name_1);
      String _plus_3 = (_plus_2 + " de-instantiated");
      this.LOGGER.logInfo(_plus_3);
    } else {
      String _name_2 = it.getPropinst().getProperty().getName();
      String _plus_4 = ("DIM: Property instance " + _name_2);
      String _plus_5 = (_plus_4 + " attached to ");
      EObject _eContainer_1 = it.getPropinst().eContainer();
      String _name_3 = ((InstanceObject) _eContainer_1).getName();
      String _plus_6 = (_plus_5 + _name_3);
      String _plus_7 = (_plus_6 + " inherited");
      this.LOGGER.logInfo(_plus_7);
    }
  })).addLifeCycle(Lifecycles.getDefault(true, true)).build();
}
