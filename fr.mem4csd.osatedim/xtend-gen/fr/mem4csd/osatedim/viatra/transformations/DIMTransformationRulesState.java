package fr.mem4csd.osatedim.viatra.transformations;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.osate.aadl2.BasicPropertyAssociation;
import org.osate.aadl2.ListValue;
import org.osate.aadl2.ModalPropertyValue;
import org.osate.aadl2.PropertyExpression;
import org.osate.aadl2.PropertyValue;
import org.osate.aadl2.PublicPackageSection;
import org.osate.aadl2.RecordValue;
import org.osate.aadl2.instance.PropertyAssociationInstance;

@SuppressWarnings("all")
public class DIMTransformationRulesState extends DIMTransformationRules {
  protected void propertyInstanceFoundDIM(final PropertyAssociationInstance propinst, final PublicPackageSection aadlPackage) {
    try {
      this.propertyInstanceCreatedDIM(propinst, aadlPackage);
      EList<ModalPropertyValue> _ownedValues = propinst.getOwnedValues();
      for (final ModalPropertyValue modpropinst : _ownedValues) {
        {
          ModalPropertyValue modpropadd = this.modalPropertyCreatedDIM(modpropinst);
          if (((modpropinst.getOwnedValue() instanceof PropertyValue) && (!(modpropinst.getOwnedValue() instanceof RecordValue)))) {
            PropertyExpression _ownedValue = modpropinst.getOwnedValue();
            final PropertyValue propvalinst = ((PropertyValue) _ownedValue);
            this.simplePropertyValueCreation(propinst.getProperty().getPropertyType(), propvalinst, modpropadd);
          } else {
            PropertyExpression _ownedValue_1 = modpropinst.getOwnedValue();
            if ((_ownedValue_1 instanceof RecordValue)) {
              EObject recvaladd = this.manipulation.createChild(modpropadd, this.aadl2Package.getModalPropertyValue_OwnedValue(), this.aadl2Package.getRecordValue());
              PropertyExpression _ownedValue_2 = modpropinst.getOwnedValue();
              EList<BasicPropertyAssociation> _ownedFieldValues = ((RecordValue) _ownedValue_2).getOwnedFieldValues();
              for (final BasicPropertyAssociation basicpropinst : _ownedFieldValues) {
                {
                  EObject _createChild = this.manipulation.createChild(recvaladd, this.aadl2Package.getRecordValue_OwnedFieldValue(), this.aadl2Package.getBasicPropertyAssociation());
                  BasicPropertyAssociation basicpropadd = ((BasicPropertyAssociation) _createChild);
                  this.manipulation.set(basicpropadd, this.aadl2Package.getBasicPropertyAssociation_Property(), basicpropinst.getProperty());
                  PropertyExpression _ownedValue_3 = basicpropinst.getOwnedValue();
                  this.simplePropertyValueCreation(basicpropinst.getProperty().getPropertyType(), ((PropertyValue) _ownedValue_3), basicpropadd);
                }
              }
            } else {
              PropertyExpression _ownedValue_3 = modpropinst.getOwnedValue();
              ListValue listvalinst = ((ListValue) _ownedValue_3);
              EObject _createChild = this.manipulation.createChild(modpropadd, this.aadl2Package.getModalPropertyValue_OwnedValue(), this.aadl2Package.getListValue());
              ListValue listvaladd = ((ListValue) _createChild);
              PropertyExpression _get = listvalinst.getOwnedListElements().get(0);
              if ((_get instanceof RecordValue)) {
                EList<PropertyExpression> _ownedListElements = listvalinst.getOwnedListElements();
                for (final PropertyExpression recvalinst : _ownedListElements) {
                  {
                    EObject recvaladd_1 = this.manipulation.createChild(listvaladd, this.aadl2Package.getListValue_OwnedListElement(), this.aadl2Package.getRecordValue());
                    EList<BasicPropertyAssociation> _ownedFieldValues_1 = ((RecordValue) recvalinst).getOwnedFieldValues();
                    for (final BasicPropertyAssociation basicpropinst_1 : _ownedFieldValues_1) {
                      {
                        EObject _createChild_1 = this.manipulation.createChild(recvaladd_1, this.aadl2Package.getRecordValue_OwnedFieldValue(), this.aadl2Package.getBasicPropertyAssociation());
                        BasicPropertyAssociation basicpropadd = ((BasicPropertyAssociation) _createChild_1);
                        this.manipulation.set(basicpropadd, this.aadl2Package.getBasicPropertyAssociation_Property(), basicpropinst_1.getProperty());
                        PropertyExpression _ownedValue_4 = basicpropinst_1.getOwnedValue();
                        this.simplePropertyValueCreation(basicpropinst_1.getProperty().getPropertyType(), ((PropertyValue) _ownedValue_4), basicpropadd);
                      }
                    }
                  }
                }
              } else {
                EList<PropertyExpression> _ownedListElements_1 = listvalinst.getOwnedListElements();
                for (final PropertyExpression propvalinst_1 : _ownedListElements_1) {
                  this.simplePropertyValueCreation(propinst.getProperty().getPropertyType(), ((PropertyValue) propvalinst_1), listvaladd);
                }
              }
            }
          }
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
