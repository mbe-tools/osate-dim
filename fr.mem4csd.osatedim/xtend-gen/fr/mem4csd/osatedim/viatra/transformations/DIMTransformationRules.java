package fr.mem4csd.osatedim.viatra.transformations;

import com.google.common.base.Objects;
import fr.mem4csd.osatedim.preference.DIMPreferences;
import fr.mem4csd.osatedim.utils.ConnectionUtils;
import fr.mem4csd.osatedim.utils.DIMLogger;
import fr.mem4csd.osatedim.utils.InstanceHierarchyUtils;
import fr.mem4csd.osatedim.utils.LibraryUtils;
import fr.mem4csd.osatedim.utils.ModeUtils;
import fr.mem4csd.osatedim.utils.PropertyTypeUtils;
import fr.mem4csd.osatedim.utils.PropertyUtils;
import fr.mem4csd.osatedim.utils.TraceUtils;
import fr.mem4csd.osatedim.viatra.queries.DIMQueriesInplace;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.util.EcoreUtil;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.transformation.runtime.emf.modelmanipulation.IModelManipulations;
import org.eclipse.viatra.transformation.runtime.emf.rules.eventdriven.EventDrivenTransformationRuleFactory;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.ExclusiveRange;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.osate.aadl2.Aadl2Package;
import org.osate.aadl2.Access;
import org.osate.aadl2.AccessCategory;
import org.osate.aadl2.AccessConnection;
import org.osate.aadl2.AccessType;
import org.osate.aadl2.BasicPropertyAssociation;
import org.osate.aadl2.BooleanLiteral;
import org.osate.aadl2.ClassifierValue;
import org.osate.aadl2.ComponentCategory;
import org.osate.aadl2.ComponentClassifier;
import org.osate.aadl2.ComponentImplementation;
import org.osate.aadl2.ComponentType;
import org.osate.aadl2.ComputedValue;
import org.osate.aadl2.ConnectedElement;
import org.osate.aadl2.Connection;
import org.osate.aadl2.ContainedNamedElement;
import org.osate.aadl2.DataAccess;
import org.osate.aadl2.DirectedFeature;
import org.osate.aadl2.DirectionType;
import org.osate.aadl2.Element;
import org.osate.aadl2.Feature;
import org.osate.aadl2.IntegerLiteral;
import org.osate.aadl2.ListValue;
import org.osate.aadl2.ModalPropertyValue;
import org.osate.aadl2.Mode;
import org.osate.aadl2.ModeTransition;
import org.osate.aadl2.ModelUnit;
import org.osate.aadl2.NamedValue;
import org.osate.aadl2.Property;
import org.osate.aadl2.PropertyAssociation;
import org.osate.aadl2.PropertyExpression;
import org.osate.aadl2.PropertySet;
import org.osate.aadl2.PropertyType;
import org.osate.aadl2.PropertyValue;
import org.osate.aadl2.PublicPackageSection;
import org.osate.aadl2.RangeValue;
import org.osate.aadl2.RealLiteral;
import org.osate.aadl2.StringLiteral;
import org.osate.aadl2.Subcomponent;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.ConnectionInstance;
import org.osate.aadl2.instance.ConnectionInstanceEnd;
import org.osate.aadl2.instance.ConnectionKind;
import org.osate.aadl2.instance.ConnectionReference;
import org.osate.aadl2.instance.FeatureCategory;
import org.osate.aadl2.instance.FeatureInstance;
import org.osate.aadl2.instance.InstanceObject;
import org.osate.aadl2.instance.InstancePackage;
import org.osate.aadl2.instance.InstanceReferenceValue;
import org.osate.aadl2.instance.ModeInstance;
import org.osate.aadl2.instance.ModeTransitionInstance;
import org.osate.aadl2.instance.PropertyAssociationInstance;
import org.osate.aadl2.instance.SystemInstance;
import org.osate.aadl2.instance.SystemOperationMode;
import org.osate.aadl2.modelsupport.util.AadlUtil;

@SuppressWarnings("all")
public class DIMTransformationRules {
  /**
   * VIATRA Query Pattern group
   */
  @Extension
  protected final DIMQueriesInplace DIMQueries = DIMQueriesInplace.instance();

  protected DIMLogger LOGGER = null;

  /**
   * Transformation-related extensions
   */
  @Extension
  protected IModelManipulations manipulation;

  @Extension
  protected EventDrivenTransformationRuleFactory _eventDrivenTransformationRuleFactory = new EventDrivenTransformationRuleFactory();

  /**
   * EMF metamodels
   */
  @Extension
  protected InstancePackage instPackage = InstancePackage.eINSTANCE;

  @Extension
  protected Aadl2Package aadl2Package = Aadl2Package.eINSTANCE;

  protected SystemInstance topSystemInst;

  protected ViatraQueryEngine engine;

  protected DIMPreferences preferences;

  protected PublicPackageSection aadlPublicPackage;

  protected PropertySet dimPropertySet;

  public SystemInstance getTopSystemInst() {
    return this.topSystemInst;
  }

  public PublicPackageSection getAadlPublickPackage() {
    return this.aadlPublicPackage;
  }

  protected void componentInstanceCreatedDIM(final ComponentInstance subcompinst) {
    try {
      EObject _eContainer = subcompinst.eContainer();
      final ComponentInstance compinst = ((ComponentInstance) _eContainer);
      final ComponentClassifier classifier = compinst.getClassifier();
      if (((classifier instanceof ComponentType) || (LibraryUtils.isAffectingClassifier(classifier, this.aadlPublicPackage, this.engine, Boolean.valueOf(this.preferences.isModifyReused()))).booleanValue())) {
        ComponentImplementation compimp = this.createComponentImplementation(compinst.getCategory());
        this.classifierCreationDIMPropertyAddition(compimp);
        EAttribute _namedElement_Name = this.aadl2Package.getNamedElement_Name();
        String _name = compinst.getName();
        String _plus = (_name + ".impl");
        this.manipulation.set(compimp, _namedElement_Name, _plus);
        if ((classifier instanceof ComponentType)) {
          this.manipulation.set(compimp, this.aadl2Package.getComponentImplementation_Type(), classifier);
        } else {
          this.manipulation.set(compimp, this.aadl2Package.getComponentImplementation_Extended(), classifier);
        }
        Boolean _isLibraryClassifier = LibraryUtils.isLibraryClassifier(classifier, this.aadlPublicPackage);
        if ((_isLibraryClassifier).booleanValue()) {
          EObject _eContainer_1 = classifier.eContainer().eContainer();
          this.manipulation.add(this.aadlPublicPackage, this.aadl2Package.getPackageSection_ImportedUnit(), ((ModelUnit) _eContainer_1));
        }
        this.manipulation.set(compinst, this.instPackage.getComponentInstance_Classifier(), compimp);
      }
      if ((compinst != this.topSystemInst)) {
        EObject _eContainer_2 = compinst.eContainer();
        boolean _isAffectingInstanceObject = LibraryUtils.isAffectingInstanceObject(compinst, ((ComponentInstance) _eContainer_2), this.aadlPublicPackage, this.engine, this.preferences.isModifyReused(), false);
        if (_isAffectingInstanceObject) {
          EObject _eContainer_3 = compinst.eContainer();
          this.denyImplementnUpdatePropagn(compinst, ((ComponentInstance) _eContainer_3), false);
        } else {
          this.setSubcomponentType(compinst, compinst.getClassifier());
        }
      }
      ComponentClassifier _classifier = compinst.getClassifier();
      Subcomponent subcomp = this.createSubcomponentInsideComponentImplementation(((ComponentImplementation) _classifier), subcompinst);
      this.manipulation.set(subcompinst, this.instPackage.getComponentInstance_Subcomponent(), subcomp);
      this.manipulation.set(subcomp, this.aadl2Package.getSubcomponent_Refined(), null);
      ComponentClassifier _xifexpression = null;
      ComponentClassifier _classifier_1 = subcompinst.getClassifier();
      boolean _tripleEquals = (_classifier_1 == null);
      if (_tripleEquals) {
        _xifexpression = this.createComponentType(subcompinst.getCategory());
      } else {
        _xifexpression = subcompinst.getClassifier();
      }
      ComponentClassifier subcomptype = _xifexpression;
      String _name_1 = subcompinst.getName();
      boolean _tripleNotEquals = (_name_1 != null);
      if (_tripleNotEquals) {
        this.manipulation.set(subcomp, this.aadl2Package.getNamedElement_Name(), subcompinst.getName());
        this.manipulation.set(subcomptype, this.aadl2Package.getNamedElement_Name(), subcompinst.getName());
      } else {
        this.manipulation.set(subcomptype, this.aadl2Package.getNamedElement_Name(), "null_ext");
      }
      ComponentClassifier _classifier_2 = subcompinst.getClassifier();
      boolean _tripleEquals_1 = (_classifier_2 == null);
      if (_tripleEquals_1) {
        this.classifierCreationDIMPropertyAddition(((ComponentClassifier) subcomptype));
      }
      this.manipulation.set(subcompinst, this.instPackage.getComponentInstance_Classifier(), subcomptype);
      this.setSubcomponentType(subcompinst, ((ComponentClassifier) subcomptype));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected void denyImplementnUpdatePropagn(final InstanceObject childinst, final ComponentInstance parentcompinst, final boolean refinement) {
    try {
      EList<ComponentClassifier> _xifexpression = null;
      if ((childinst instanceof ComponentInstance)) {
        _xifexpression = LibraryUtils.getAllInheritingParentClassifiers(((ComponentInstance)childinst).getSubcomponent(), parentcompinst, refinement);
      } else {
        _xifexpression = LibraryUtils.getAllInheritingParentClassifiers(((ConnectionReference) childinst), parentcompinst, refinement);
      }
      EList<? extends ComponentClassifier> extensionList = _xifexpression;
      if (((!Objects.equal(parentcompinst, this.topSystemInst)) && LibraryUtils.isAffectingInstanceObject(parentcompinst, ((ComponentInstance) parentcompinst.eContainer()), this.aadlPublicPackage, this.engine, this.preferences.isModifyReused(), false))) {
        ComponentImplementation previousImp = null;
        List<? extends ComponentClassifier> _reverse = ListExtensions.reverse(extensionList);
        for (final ComponentClassifier currentcompimp : _reverse) {
          {
            ComponentClassifier _copyClassifier = this.copyClassifier(currentcompimp, previousImp, extensionList, childinst);
            previousImp = ((ComponentImplementation) _copyClassifier);
            int _indexOf = extensionList.indexOf(currentcompimp);
            int _size = extensionList.size();
            int _minus = (_size - 1);
            boolean _tripleEquals = (_indexOf == _minus);
            if (_tripleEquals) {
              this.manipulation.set(parentcompinst, this.instPackage.getComponentInstance_Classifier(), previousImp);
              EObject _eContainer = parentcompinst.eContainer();
              this.denyImplementnUpdatePropagn(parentcompinst, ((ComponentInstance) _eContainer), false);
            }
          }
        }
      } else {
        Integer extensionIndex = LibraryUtils.computeExtensionIndex(extensionList, this.aadlPublicPackage, this.engine, Boolean.valueOf(this.preferences.isModifyReused()));
        ComponentImplementation previousImp_1 = null;
        List<? extends ComponentClassifier> _reverse_1 = ListExtensions.reverse(extensionList);
        for (final ComponentClassifier currentcompimp_1 : _reverse_1) {
          int _indexOf = extensionList.indexOf(currentcompimp_1);
          int _size = extensionList.size();
          int _minus = (_size - (extensionIndex).intValue());
          boolean _lessThan = (_indexOf < _minus);
          if (_lessThan) {
            ComponentClassifier _copyClassifier = this.copyClassifier(currentcompimp_1, previousImp_1, extensionList, childinst);
            previousImp_1 = ((ComponentImplementation) _copyClassifier);
            int _indexOf_1 = extensionList.indexOf(currentcompimp_1);
            int _size_1 = extensionList.size();
            int _minus_1 = (_size_1 - 1);
            boolean _equals = (_indexOf_1 == _minus_1);
            if (_equals) {
              this.manipulation.set(parentcompinst, this.instPackage.getComponentInstance_Classifier(), previousImp_1);
            }
          } else {
            int _indexOf_2 = extensionList.indexOf(currentcompimp_1);
            int _size_2 = extensionList.size();
            int _minus_2 = (_size_2 - (extensionIndex).intValue());
            boolean _equals_1 = (_indexOf_2 == _minus_2);
            if (_equals_1) {
              this.manipulation.set(currentcompimp_1, this.aadl2Package.getComponentImplementation_Extended(), previousImp_1);
            }
          }
        }
      }
      if ((childinst instanceof ComponentInstance)) {
        this.setSubcomponentType(((ComponentInstance)childinst), ((ComponentInstance)childinst).getClassifier());
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected ComponentClassifier copyClassifier(final ComponentClassifier currentCompClass, final ComponentClassifier previousCompClass, final EList<? extends ComponentClassifier> extensionList, final InstanceObject childInst) {
    try {
      ComponentClassifier _copy = EcoreUtil.<ComponentClassifier>copy(currentCompClass);
      ComponentClassifier compClassCopy = ((ComponentClassifier) _copy);
      this.classifierCreationDIMPropertyAddition(compClassCopy);
      this.manipulation.moveTo(compClassCopy, this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier());
      EAttribute _namedElement_Name = this.aadl2Package.getNamedElement_Name();
      String _name = currentCompClass.getName();
      String _plus = (_name + "_copy");
      this.manipulation.set(compClassCopy, _namedElement_Name, _plus);
      if (((extensionList.indexOf(currentCompClass) == 0) && (childInst != null))) {
        if ((childInst instanceof ComponentInstance)) {
          Subcomponent newSubcomp = LibraryUtils.getCopySubcomponent(((ComponentInstance)childInst).getSubcomponent(), ((ComponentImplementation) compClassCopy));
          this.manipulation.set(childInst, this.instPackage.getComponentInstance_Subcomponent(), newSubcomp);
        } else {
          if ((childInst instanceof FeatureInstance)) {
            Feature newFeat = LibraryUtils.getCopyFeature(((FeatureInstance)childInst).getFeature(), ((ComponentType) compClassCopy));
            this.manipulation.set(childInst, this.instPackage.getFeatureInstance_Feature(), newFeat);
          } else {
            if ((childInst instanceof ConnectionReference)) {
              Connection newConn = LibraryUtils.getCopyConnection(((ConnectionReference)childInst).getConnection(), ((ComponentImplementation) compClassCopy));
              this.manipulation.set(childInst, this.instPackage.getConnectionReference_Connection(), newConn);
            }
          }
        }
      } else {
        if ((currentCompClass instanceof ComponentType)) {
          this.manipulation.set(compClassCopy, this.aadl2Package.getComponentType_Extended(), previousCompClass);
        } else {
          this.manipulation.set(compClassCopy, this.aadl2Package.getComponentImplementation_Extended(), previousCompClass);
        }
      }
      return compClassCopy;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected ComponentImplementation createComponentImplementation(final ComponentCategory category) {
    try {
      EObject _switchResult = null;
      if (category != null) {
        switch (category) {
          case ABSTRACT:
            _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getAbstractImplementation());
            break;
          case BUS:
            _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getBusImplementation());
            break;
          case DATA:
            _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getDataImplementation());
            break;
          case DEVICE:
            _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getDeviceImplementation());
            break;
          case MEMORY:
            _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getMemoryImplementation());
            break;
          case PROCESS:
            _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getProcessImplementation());
            break;
          case PROCESSOR:
            _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getProcessorImplementation());
            break;
          case SUBPROGRAM:
            _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getSubprogramImplementation());
            break;
          case SUBPROGRAM_GROUP:
            _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getSubprogramGroupImplementation());
            break;
          case SYSTEM:
            _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getSystemImplementation());
            break;
          case THREAD:
            _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getThreadImplementation());
            break;
          case THREAD_GROUP:
            _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getThreadGroupImplementation());
            break;
          case VIRTUAL_BUS:
            _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getVirtualBusImplementation());
            break;
          case VIRTUAL_PROCESSOR:
            _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getVirtualProcessorImplementation());
            break;
          default:
            _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getAbstractImplementation());
            break;
        }
      } else {
        _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getAbstractImplementation());
      }
      return ((ComponentImplementation) _switchResult);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected ComponentType createComponentType(final ComponentCategory category) {
    try {
      EObject _switchResult = null;
      if (category != null) {
        switch (category) {
          case ABSTRACT:
            _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getAbstractType());
            break;
          case BUS:
            _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getBusType());
            break;
          case DATA:
            _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getDataType());
            break;
          case DEVICE:
            _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getDeviceType());
            break;
          case MEMORY:
            _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getMemoryType());
            break;
          case PROCESS:
            _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getProcessType());
            break;
          case PROCESSOR:
            _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getProcessorType());
            break;
          case SUBPROGRAM:
            _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getSubprogramType());
            break;
          case SUBPROGRAM_GROUP:
            _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getSubprogramGroupType());
            break;
          case SYSTEM:
            _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getSystemType());
            break;
          case THREAD:
            _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getThreadType());
            break;
          case THREAD_GROUP:
            _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getThreadGroupType());
            break;
          case VIRTUAL_BUS:
            _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getVirtualBusType());
            break;
          case VIRTUAL_PROCESSOR:
            _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getVirtualProcessorType());
            break;
          default:
            _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getAbstractType());
            break;
        }
      } else {
        _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getAbstractType());
      }
      return ((ComponentType) _switchResult);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected Subcomponent createSubcomponentInsideComponentImplementation(final ComponentImplementation compimp, final ComponentInstance subcompinst) {
    try {
      EObject _switchResult = null;
      ComponentCategory _category = compimp.getCategory();
      if (_category != null) {
        switch (_category) {
          case ABSTRACT:
            EObject _switchResult_1 = null;
            ComponentCategory _category_1 = subcompinst.getCategory();
            if (_category_1 != null) {
              switch (_category_1) {
                case ABSTRACT:
                  _switchResult_1 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
                  break;
                case BUS:
                  _switchResult_1 = this.manipulation.createChild(compimp, this.aadl2Package.getAbstractImplementation_OwnedBusSubcomponent(), this.aadl2Package.getBusSubcomponent());
                  break;
                case DATA:
                  _switchResult_1 = this.manipulation.createChild(compimp, this.aadl2Package.getAbstractImplementation_OwnedDataSubcomponent(), this.aadl2Package.getDataSubcomponent());
                  break;
                case DEVICE:
                  _switchResult_1 = this.manipulation.createChild(compimp, this.aadl2Package.getAbstractImplementation_OwnedDeviceSubcomponent(), this.aadl2Package.getDeviceSubcomponent());
                  break;
                case MEMORY:
                  _switchResult_1 = this.manipulation.createChild(compimp, this.aadl2Package.getAbstractImplementation_OwnedMemorySubcomponent(), this.aadl2Package.getMemorySubcomponent());
                  break;
                case PROCESS:
                  _switchResult_1 = this.manipulation.createChild(compimp, this.aadl2Package.getAbstractImplementation_OwnedProcessSubcomponent(), this.aadl2Package.getProcessSubcomponent());
                  break;
                case PROCESSOR:
                  _switchResult_1 = this.manipulation.createChild(compimp, this.aadl2Package.getAbstractImplementation_OwnedProcessorSubcomponent(), this.aadl2Package.getProcessorSubcomponent());
                  break;
                case SUBPROGRAM:
                  _switchResult_1 = this.manipulation.createChild(compimp, this.aadl2Package.getAbstractImplementation_OwnedSubprogramSubcomponent(), this.aadl2Package.getSubprogramSubcomponent());
                  break;
                case SUBPROGRAM_GROUP:
                  _switchResult_1 = this.manipulation.createChild(compimp, this.aadl2Package.getAbstractImplementation_OwnedSubprogramGroupSubcomponent(), this.aadl2Package.getSubprogramGroupSubcomponent());
                  break;
                case THREAD:
                  _switchResult_1 = this.manipulation.createChild(compimp, this.aadl2Package.getAbstractImplementation_OwnedThreadSubcomponent(), this.aadl2Package.getThreadSubcomponent());
                  break;
                case THREAD_GROUP:
                  _switchResult_1 = this.manipulation.createChild(compimp, this.aadl2Package.getAbstractImplementation_OwnedThreadGroupSubcomponent(), this.aadl2Package.getThreadGroupSubcomponent());
                  break;
                case SYSTEM:
                  _switchResult_1 = this.manipulation.createChild(compimp, this.aadl2Package.getAbstractImplementation_OwnedSystemSubcomponent(), this.aadl2Package.getSystemSubcomponent());
                  break;
                case VIRTUAL_BUS:
                  _switchResult_1 = this.manipulation.createChild(compimp, this.aadl2Package.getAbstractImplementation_OwnedVirtualBusSubcomponent(), this.aadl2Package.getVirtualBusSubcomponent());
                  break;
                case VIRTUAL_PROCESSOR:
                  _switchResult_1 = this.manipulation.createChild(compimp, this.aadl2Package.getAbstractImplementation_OwnedVirtualProcessorSubcomponent(), this.aadl2Package.getVirtualProcessorSubcomponent());
                  break;
                default:
                  _switchResult_1 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
                  break;
              }
            } else {
              _switchResult_1 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
            }
            _switchResult = _switchResult_1;
            break;
          case BUS:
            EObject _switchResult_2 = null;
            ComponentCategory _category_2 = subcompinst.getCategory();
            if (_category_2 != null) {
              switch (_category_2) {
                case ABSTRACT:
                  _switchResult_2 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
                  break;
                case VIRTUAL_BUS:
                  _switchResult_2 = this.manipulation.createChild(compimp, this.aadl2Package.getBusImplementation_OwnedVirtualBusSubcomponent(), this.aadl2Package.getVirtualBusSubcomponent());
                  break;
                default:
                  _switchResult_2 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
                  break;
              }
            } else {
              _switchResult_2 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
            }
            _switchResult = _switchResult_2;
            break;
          case DATA:
            EObject _switchResult_3 = null;
            ComponentCategory _category_3 = subcompinst.getCategory();
            if (_category_3 != null) {
              switch (_category_3) {
                case ABSTRACT:
                  _switchResult_3 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
                  break;
                case DATA:
                  _switchResult_3 = this.manipulation.createChild(compimp, this.aadl2Package.getDataImplementation_OwnedDataSubcomponent(), this.aadl2Package.getDataSubcomponent());
                  break;
                case SUBPROGRAM:
                  _switchResult_3 = this.manipulation.createChild(compimp, this.aadl2Package.getDataImplementation_OwnedSubprogramSubcomponent(), this.aadl2Package.getSubprogramSubcomponent());
                  break;
                default:
                  _switchResult_3 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
                  break;
              }
            } else {
              _switchResult_3 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
            }
            _switchResult = _switchResult_3;
            break;
          case DEVICE:
            EObject _switchResult_4 = null;
            ComponentCategory _category_4 = subcompinst.getCategory();
            if (_category_4 != null) {
              switch (_category_4) {
                case ABSTRACT:
                  _switchResult_4 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
                  break;
                case BUS:
                  _switchResult_4 = this.manipulation.createChild(compimp, this.aadl2Package.getDeviceImplementation_OwnedBusSubcomponent(), this.aadl2Package.getBusSubcomponent());
                  break;
                case DATA:
                  _switchResult_4 = this.manipulation.createChild(compimp, this.aadl2Package.getDeviceImplementation_OwnedDataSubcomponent(), this.aadl2Package.getDataSubcomponent());
                  break;
                case VIRTUAL_BUS:
                  _switchResult_4 = this.manipulation.createChild(compimp, this.aadl2Package.getDeviceImplementation_OwnedVirtualBusSubcomponent(), this.aadl2Package.getVirtualBusSubcomponent());
                  break;
                default:
                  _switchResult_4 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
                  break;
              }
            } else {
              _switchResult_4 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
            }
            _switchResult = _switchResult_4;
            break;
          case MEMORY:
            EObject _switchResult_5 = null;
            ComponentCategory _category_5 = subcompinst.getCategory();
            if (_category_5 != null) {
              switch (_category_5) {
                case ABSTRACT:
                  _switchResult_5 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
                  break;
                case BUS:
                  _switchResult_5 = this.manipulation.createChild(compimp, this.aadl2Package.getMemoryImplementation_OwnedBusSubcomponent(), this.aadl2Package.getBusSubcomponent());
                  break;
                case MEMORY:
                  _switchResult_5 = this.manipulation.createChild(compimp, this.aadl2Package.getMemoryImplementation_OwnedMemorySubcomponent(), this.aadl2Package.getMemorySubcomponent());
                  break;
                default:
                  _switchResult_5 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
                  break;
              }
            } else {
              _switchResult_5 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
            }
            _switchResult = _switchResult_5;
            break;
          case PROCESS:
            EObject _switchResult_6 = null;
            ComponentCategory _category_6 = subcompinst.getCategory();
            if (_category_6 != null) {
              switch (_category_6) {
                case ABSTRACT:
                  _switchResult_6 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
                  break;
                case DATA:
                  _switchResult_6 = this.manipulation.createChild(compimp, this.aadl2Package.getProcessImplementation_OwnedDataSubcomponent(), this.aadl2Package.getDataSubcomponent());
                  break;
                case SUBPROGRAM:
                  _switchResult_6 = this.manipulation.createChild(compimp, this.aadl2Package.getProcessImplementation_OwnedSubprogramSubcomponent(), this.aadl2Package.getSubprogramSubcomponent());
                  break;
                case SUBPROGRAM_GROUP:
                  _switchResult_6 = this.manipulation.createChild(compimp, this.aadl2Package.getProcessImplementation_OwnedSubprogramGroupSubcomponent(), this.aadl2Package.getSubprogramGroupSubcomponent());
                  break;
                case THREAD:
                  _switchResult_6 = this.manipulation.createChild(compimp, this.aadl2Package.getProcessImplementation_OwnedThreadSubcomponent(), this.aadl2Package.getThreadSubcomponent());
                  break;
                case THREAD_GROUP:
                  _switchResult_6 = this.manipulation.createChild(compimp, this.aadl2Package.getProcessImplementation_OwnedThreadGroupSubcomponent(), this.aadl2Package.getThreadGroupSubcomponent());
                  break;
                default:
                  _switchResult_6 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
                  break;
              }
            } else {
              _switchResult_6 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
            }
            _switchResult = _switchResult_6;
            break;
          case PROCESSOR:
            EObject _switchResult_7 = null;
            ComponentCategory _category_7 = subcompinst.getCategory();
            if (_category_7 != null) {
              switch (_category_7) {
                case ABSTRACT:
                  _switchResult_7 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
                  break;
                case BUS:
                  _switchResult_7 = this.manipulation.createChild(compimp, this.aadl2Package.getProcessorImplementation_OwnedBusSubcomponent(), this.aadl2Package.getBusSubcomponent());
                  break;
                case MEMORY:
                  _switchResult_7 = this.manipulation.createChild(compimp, this.aadl2Package.getProcessorImplementation_OwnedMemorySubcomponent(), this.aadl2Package.getMemorySubcomponent());
                  break;
                case VIRTUAL_BUS:
                  _switchResult_7 = this.manipulation.createChild(compimp, this.aadl2Package.getProcessorImplementation_OwnedVirtualBusSubcomponent(), this.aadl2Package.getVirtualBusSubcomponent());
                  break;
                case VIRTUAL_PROCESSOR:
                  _switchResult_7 = this.manipulation.createChild(compimp, this.aadl2Package.getProcessorImplementation_OwnedVirtualProcessorSubcomponent(), this.aadl2Package.getVirtualProcessorSubcomponent());
                  break;
                default:
                  _switchResult_7 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
                  break;
              }
            } else {
              _switchResult_7 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
            }
            _switchResult = _switchResult_7;
            break;
          case SUBPROGRAM:
            EObject _switchResult_8 = null;
            ComponentCategory _category_8 = subcompinst.getCategory();
            if (_category_8 != null) {
              switch (_category_8) {
                case ABSTRACT:
                  _switchResult_8 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
                  break;
                case DATA:
                  _switchResult_8 = this.manipulation.createChild(compimp, this.aadl2Package.getSubprogramImplementation_OwnedDataSubcomponent(), this.aadl2Package.getDataSubcomponent());
                  break;
                case SUBPROGRAM:
                  _switchResult_8 = this.manipulation.createChild(compimp, this.aadl2Package.getSubprogramImplementation_OwnedSubprogramSubcomponent(), this.aadl2Package.getSubprogramSubcomponent());
                  break;
                default:
                  _switchResult_8 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
                  break;
              }
            } else {
              _switchResult_8 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
            }
            _switchResult = _switchResult_8;
            break;
          case SUBPROGRAM_GROUP:
            EObject _switchResult_9 = null;
            ComponentCategory _category_9 = subcompinst.getCategory();
            if (_category_9 != null) {
              switch (_category_9) {
                case ABSTRACT:
                  _switchResult_9 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
                  break;
                case DATA:
                  _switchResult_9 = this.manipulation.createChild(compimp, this.aadl2Package.getSubprogramGroupImplementation_OwnedDataSubcomponent(), this.aadl2Package.getDataSubcomponent());
                  break;
                case SUBPROGRAM:
                  _switchResult_9 = this.manipulation.createChild(compimp, this.aadl2Package.getSubprogramGroupImplementation_OwnedSubprogramSubcomponent(), this.aadl2Package.getSubprogramSubcomponent());
                  break;
                case SUBPROGRAM_GROUP:
                  _switchResult_9 = this.manipulation.createChild(compimp, this.aadl2Package.getSubprogramGroupImplementation_OwnedSubprogramGroupSubcomponent(), this.aadl2Package.getSubprogramGroupSubcomponent());
                  break;
                default:
                  _switchResult_9 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
                  break;
              }
            } else {
              _switchResult_9 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
            }
            _switchResult = _switchResult_9;
            break;
          case SYSTEM:
            EObject _switchResult_10 = null;
            ComponentCategory _category_10 = subcompinst.getCategory();
            if (_category_10 != null) {
              switch (_category_10) {
                case ABSTRACT:
                  _switchResult_10 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
                  break;
                case BUS:
                  _switchResult_10 = this.manipulation.createChild(compimp, this.aadl2Package.getSystemImplementation_OwnedBusSubcomponent(), this.aadl2Package.getBusSubcomponent());
                  break;
                case DATA:
                  _switchResult_10 = this.manipulation.createChild(compimp, this.aadl2Package.getSystemImplementation_OwnedDataSubcomponent(), this.aadl2Package.getDataSubcomponent());
                  break;
                case DEVICE:
                  _switchResult_10 = this.manipulation.createChild(compimp, this.aadl2Package.getSystemImplementation_OwnedDeviceSubcomponent(), this.aadl2Package.getDeviceSubcomponent());
                  break;
                case MEMORY:
                  _switchResult_10 = this.manipulation.createChild(compimp, this.aadl2Package.getSystemImplementation_OwnedMemorySubcomponent(), this.aadl2Package.getMemorySubcomponent());
                  break;
                case PROCESS:
                  _switchResult_10 = this.manipulation.createChild(compimp, this.aadl2Package.getSystemImplementation_OwnedProcessSubcomponent(), this.aadl2Package.getProcessSubcomponent());
                  break;
                case PROCESSOR:
                  _switchResult_10 = this.manipulation.createChild(compimp, this.aadl2Package.getSystemImplementation_OwnedProcessorSubcomponent(), this.aadl2Package.getProcessorSubcomponent());
                  break;
                case SUBPROGRAM:
                  _switchResult_10 = this.manipulation.createChild(compimp, this.aadl2Package.getSystemImplementation_OwnedSubprogramSubcomponent(), this.aadl2Package.getSubprogramSubcomponent());
                  break;
                case SUBPROGRAM_GROUP:
                  _switchResult_10 = this.manipulation.createChild(compimp, this.aadl2Package.getSystemImplementation_OwnedSubprogramGroupSubcomponent(), this.aadl2Package.getSubprogramGroupSubcomponent());
                  break;
                case SYSTEM:
                  _switchResult_10 = this.manipulation.createChild(compimp, this.aadl2Package.getSystemImplementation_OwnedSystemSubcomponent(), this.aadl2Package.getSystemSubcomponent());
                  break;
                case VIRTUAL_BUS:
                  _switchResult_10 = this.manipulation.createChild(compimp, this.aadl2Package.getSystemImplementation_OwnedVirtualBusSubcomponent(), this.aadl2Package.getVirtualBusSubcomponent());
                  break;
                case VIRTUAL_PROCESSOR:
                  _switchResult_10 = this.manipulation.createChild(compimp, this.aadl2Package.getSystemImplementation_OwnedVirtualProcessorSubcomponent(), this.aadl2Package.getVirtualProcessorSubcomponent());
                  break;
                default:
                  _switchResult_10 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
                  break;
              }
            } else {
              _switchResult_10 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
            }
            _switchResult = _switchResult_10;
            break;
          case THREAD:
            EObject _switchResult_11 = null;
            ComponentCategory _category_11 = subcompinst.getCategory();
            if (_category_11 != null) {
              switch (_category_11) {
                case ABSTRACT:
                  _switchResult_11 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
                  break;
                case DATA:
                  _switchResult_11 = this.manipulation.createChild(compimp, this.aadl2Package.getThreadImplementation_OwnedDataSubcomponent(), this.aadl2Package.getDataSubcomponent());
                  break;
                case SUBPROGRAM:
                  _switchResult_11 = this.manipulation.createChild(compimp, this.aadl2Package.getThreadImplementation_OwnedSubprogramSubcomponent(), this.aadl2Package.getSubprogramSubcomponent());
                  break;
                case SUBPROGRAM_GROUP:
                  _switchResult_11 = this.manipulation.createChild(compimp, this.aadl2Package.getThreadImplementation_OwnedSubprogramGroupSubcomponent(), this.aadl2Package.getSubprogramGroupSubcomponent());
                  break;
                default:
                  _switchResult_11 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
                  break;
              }
            } else {
              _switchResult_11 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
            }
            _switchResult = _switchResult_11;
            break;
          case THREAD_GROUP:
            EObject _switchResult_12 = null;
            ComponentCategory _category_12 = subcompinst.getCategory();
            if (_category_12 != null) {
              switch (_category_12) {
                case ABSTRACT:
                  _switchResult_12 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
                  break;
                case DATA:
                  _switchResult_12 = this.manipulation.createChild(compimp, this.aadl2Package.getThreadGroupImplementation_OwnedDataSubcomponent(), this.aadl2Package.getDataSubcomponent());
                  break;
                case SUBPROGRAM:
                  _switchResult_12 = this.manipulation.createChild(compimp, this.aadl2Package.getThreadGroupImplementation_OwnedSubprogramSubcomponent(), this.aadl2Package.getSubprogramSubcomponent());
                  break;
                case SUBPROGRAM_GROUP:
                  _switchResult_12 = this.manipulation.createChild(compimp, this.aadl2Package.getThreadGroupImplementation_OwnedSubprogramGroupSubcomponent(), this.aadl2Package.getSubprogramGroupSubcomponent());
                  break;
                case THREAD:
                  _switchResult_12 = this.manipulation.createChild(compimp, this.aadl2Package.getThreadGroupImplementation_OwnedThreadSubcomponent(), this.aadl2Package.getThreadSubcomponent());
                  break;
                case THREAD_GROUP:
                  _switchResult_12 = this.manipulation.createChild(compimp, this.aadl2Package.getThreadGroupImplementation_OwnedThreadGroupSubcomponent(), this.aadl2Package.getThreadGroupSubcomponent());
                  break;
                default:
                  _switchResult_12 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
                  break;
              }
            } else {
              _switchResult_12 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
            }
            _switchResult = _switchResult_12;
            break;
          case VIRTUAL_BUS:
            EObject _switchResult_13 = null;
            ComponentCategory _category_13 = subcompinst.getCategory();
            if (_category_13 != null) {
              switch (_category_13) {
                case ABSTRACT:
                  _switchResult_13 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
                  break;
                case VIRTUAL_BUS:
                  _switchResult_13 = this.manipulation.createChild(compimp, this.aadl2Package.getVirtualBusImplementation_OwnedVirtualBusSubcomponent(), this.aadl2Package.getVirtualBusSubcomponent());
                  break;
                default:
                  _switchResult_13 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
                  break;
              }
            } else {
              _switchResult_13 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
            }
            _switchResult = _switchResult_13;
            break;
          case VIRTUAL_PROCESSOR:
            EObject _switchResult_14 = null;
            ComponentCategory _category_14 = subcompinst.getCategory();
            if (_category_14 != null) {
              switch (_category_14) {
                case ABSTRACT:
                  _switchResult_14 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
                  break;
                case VIRTUAL_BUS:
                  _switchResult_14 = this.manipulation.createChild(compimp, this.aadl2Package.getVirtualProcessorImplementation_OwnedVirtualBusSubcomponent(), this.aadl2Package.getVirtualBusSubcomponent());
                  break;
                case VIRTUAL_PROCESSOR:
                  _switchResult_14 = this.manipulation.createChild(compimp, this.aadl2Package.getVirtualProcessorImplementation_OwnedVirtualProcessorSubcomponent(), this.aadl2Package.getVirtualProcessorSubcomponent());
                  break;
                default:
                  _switchResult_14 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
                  break;
              }
            } else {
              _switchResult_14 = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), this.aadl2Package.getAbstractSubcomponent());
            }
            _switchResult = _switchResult_14;
            break;
          default:
            break;
        }
      }
      return ((Subcomponent) _switchResult);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected void setSubcomponentType(final ComponentInstance compinst, final ComponentClassifier compclass) {
    try {
      final Subcomponent subcomponent = compinst.getSubcomponent();
      ComponentCategory _category = compclass.getCategory();
      if (_category != null) {
        switch (_category) {
          case ABSTRACT:
            this.manipulation.set(subcomponent, this.aadl2Package.getAbstractSubcomponent_AbstractSubcomponentType(), compclass);
            break;
          case BUS:
            this.manipulation.set(subcomponent, this.aadl2Package.getBusSubcomponent_BusSubcomponentType(), compclass);
            break;
          case DATA:
            this.manipulation.set(subcomponent, this.aadl2Package.getDataSubcomponent_DataSubcomponentType(), compclass);
            break;
          case DEVICE:
            this.manipulation.set(subcomponent, this.aadl2Package.getDeviceSubcomponent_DeviceSubcomponentType(), compclass);
            break;
          case MEMORY:
            this.manipulation.set(subcomponent, this.aadl2Package.getMemorySubcomponent_MemorySubcomponentType(), compclass);
            break;
          case PROCESS:
            this.manipulation.set(subcomponent, this.aadl2Package.getProcessSubcomponent_ProcessSubcomponentType(), compclass);
            break;
          case PROCESSOR:
            this.manipulation.set(subcomponent, this.aadl2Package.getProcessorSubcomponent_ProcessorSubcomponentType(), compclass);
            break;
          case SUBPROGRAM:
            this.manipulation.set(subcomponent, this.aadl2Package.getSubprogramSubcomponent_SubprogramSubcomponentType(), compclass);
            break;
          case SUBPROGRAM_GROUP:
            this.manipulation.set(subcomponent, this.aadl2Package.getSubprogramGroupSubcomponent_SubprogramGroupSubcomponentType(), compclass);
            break;
          case SYSTEM:
            this.manipulation.set(subcomponent, this.aadl2Package.getSystemSubcomponent_SystemSubcomponentType(), compclass);
            break;
          case THREAD:
            this.manipulation.set(subcomponent, this.aadl2Package.getThreadSubcomponent_ThreadSubcomponentType(), compclass);
            break;
          case THREAD_GROUP:
            this.manipulation.set(subcomponent, this.aadl2Package.getThreadGroupSubcomponent_ThreadGroupSubcomponentType(), compclass);
            break;
          case VIRTUAL_BUS:
            this.manipulation.set(subcomponent, this.aadl2Package.getVirtualBusSubcomponent_VirtualBusSubcomponentType(), compclass);
            break;
          case VIRTUAL_PROCESSOR:
            this.manipulation.set(subcomponent, this.aadl2Package.getVirtualProcessorSubcomponent_VirtualProcessorSubcomponentType(), compclass);
            break;
          default:
            break;
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  public Integer max(final Integer int1, final Integer int2) {
    boolean _greaterThan = (int1.compareTo(int2) > 0);
    if (_greaterThan) {
      return int1;
    } else {
      return int2;
    }
  }

  protected void elementCreationPropertyInheritance(final InstanceObject instanceObj) {
    try {
      boolean _isInheritProperty = this.preferences.isInheritProperty();
      if (_isInheritProperty) {
        EObject _eContainer = instanceObj.eContainer();
        EList<PropertyAssociation> _ownedPropertyAssociations = ((InstanceObject) _eContainer).getOwnedPropertyAssociations();
        for (final PropertyAssociation propinst : _ownedPropertyAssociations) {
          if ((propinst.getProperty().isInherit() && instanceObj.acceptsProperty(propinst.getProperty()))) {
            PropertyAssociation propadd = EcoreUtil.<PropertyAssociation>copy(propinst);
            this.manipulation.add(instanceObj, this.aadl2Package.getNamedElement_OwnedPropertyAssociation(), propadd);
          }
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected void componentCreationModeInheritance(final ComponentInstance subcompinst) {
    try {
      boolean _isInheritMode = this.preferences.isInheritMode();
      if (_isInheritMode) {
        EObject _eContainer = subcompinst.eContainer();
        EList<ModeInstance> _inModes = ((ComponentInstance) _eContainer).getInModes();
        for (final ModeInstance modeinst : _inModes) {
          {
            ModeInstance modeadd = EcoreUtil.<ModeInstance>copy(modeinst);
            this.manipulation.add(subcompinst, this.instPackage.getComponentInstance_InMode(), modeadd);
          }
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected void connectionCreationModeInheritance(final ConnectionInstance conninst) {
    try {
      boolean _isInheritMode = this.preferences.isInheritMode();
      if (_isInheritMode) {
        EObject _eContainer = conninst.eContainer();
        EList<ModeInstance> _inModes = ((ComponentInstance) _eContainer).getInModes();
        for (final ModeInstance modeinst : _inModes) {
          EList<SystemOperationMode> _systemOperationModes = this.topSystemInst.getSystemOperationModes();
          for (final SystemOperationMode som : _systemOperationModes) {
            boolean _contains = som.getCurrentModes().contains(modeinst);
            if (_contains) {
              this.manipulation.add(conninst, this.instPackage.getConnectionInstance_InSystemOperationMode(), som);
            }
          }
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected void classifierCreationDIMPropertyAddition(final ComponentClassifier compclass) {
    try {
      boolean _isAddClassifierProperty = this.preferences.isAddClassifierProperty();
      if (_isAddClassifierProperty) {
        EObject propadd = this.manipulation.createChild(compclass, this.aadl2Package.getNamedElement_OwnedPropertyAssociation(), this.aadl2Package.getPropertyAssociation());
        this.manipulation.set(propadd, this.aadl2Package.getPropertyAssociation_Property(), this.dimPropertySet.findNamedElement("DIM_Classifier"));
        EObject modpropadd = this.manipulation.createChild(propadd, this.aadl2Package.getPropertyAssociation_OwnedValue(), this.aadl2Package.getModalPropertyValue());
        EObject propvaladd = this.manipulation.createChild(modpropadd, this.aadl2Package.getModalPropertyValue_OwnedValue(), this.aadl2Package.getBooleanLiteral());
        this.manipulation.set(propvaladd, this.aadl2Package.getBooleanLiteral_Value(), Boolean.valueOf(true));
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected void featureInstanceCreatedDIM(final FeatureInstance featinst) {
    try {
      EObject _eContainer = featinst.eContainer();
      ComponentInstance compinst = ((ComponentInstance) _eContainer);
      ComponentClassifier _xifexpression = null;
      boolean _equals = Objects.equal(compinst, this.topSystemInst);
      if (_equals) {
        _xifexpression = ((SystemInstance) compinst).getComponentImplementation();
      } else {
        _xifexpression = compinst.getClassifier();
      }
      ComponentClassifier compClassifier = _xifexpression;
      ComponentType comptype = null;
      if ((compClassifier instanceof ComponentImplementation)) {
        comptype = ((ComponentImplementation) compClassifier).getType();
        Boolean _isAffectingClassifier = LibraryUtils.isAffectingClassifier(compClassifier, this.aadlPublicPackage, this.engine, Boolean.valueOf(this.preferences.isModifyReused()));
        if ((_isAffectingClassifier).booleanValue()) {
          ComponentImplementation compimp = this.createComponentImplementation(compinst.getCategory());
          this.classifierCreationDIMPropertyAddition(compimp);
          EAttribute _namedElement_Name = this.aadl2Package.getNamedElement_Name();
          String _name = ((ComponentImplementation)compClassifier).getName();
          String _plus = (_name + "_ext");
          this.manipulation.set(compimp, _namedElement_Name, _plus);
          this.manipulation.set(compimp, this.aadl2Package.getComponentImplementation_Type(), comptype);
          this.manipulation.set(compimp, this.aadl2Package.getComponentImplementation_Extended(), compClassifier);
          Boolean _isLibraryClassifier = LibraryUtils.isLibraryClassifier(compClassifier, this.aadlPublicPackage);
          if ((_isLibraryClassifier).booleanValue()) {
            EObject _eContainer_1 = compinst.getClassifier().eContainer().eContainer();
            this.manipulation.add(this.aadlPublicPackage, this.aadl2Package.getPackageSection_ImportedUnit(), ((ModelUnit) _eContainer_1));
          }
          this.manipulation.set(compinst, this.instPackage.getComponentInstance_Classifier(), compimp);
        }
      } else {
        ComponentClassifier _classifier = compinst.getClassifier();
        comptype = ((ComponentType) _classifier);
      }
      Boolean _isAffectingClassifier_1 = LibraryUtils.isAffectingClassifier(comptype, this.aadlPublicPackage, this.engine, Boolean.valueOf(this.preferences.isModifyReused()));
      if ((_isAffectingClassifier_1).booleanValue()) {
        ComponentType newcomptype = this.createComponentType(compinst.getCategory());
        this.classifierCreationDIMPropertyAddition(newcomptype);
        EAttribute _namedElement_Name_1 = this.aadl2Package.getNamedElement_Name();
        String _name_1 = comptype.getName();
        String _plus_1 = (_name_1 + "_ext");
        this.manipulation.set(newcomptype, _namedElement_Name_1, _plus_1);
        this.manipulation.set(newcomptype, this.aadl2Package.getComponentType_Extended(), comptype);
        if ((compClassifier instanceof ComponentImplementation)) {
          this.manipulation.set(compinst.getClassifier(), this.aadl2Package.getComponentImplementation_Type(), newcomptype);
        } else {
          this.manipulation.set(compinst, this.instPackage.getComponentInstance_Classifier(), newcomptype);
        }
        comptype = newcomptype;
      }
      if ((compinst != this.topSystemInst)) {
        EObject _eContainer_2 = compinst.eContainer();
        boolean _isAffectingInstanceObject = LibraryUtils.isAffectingInstanceObject(compinst, ((ComponentInstance) _eContainer_2), this.aadlPublicPackage, this.engine, this.preferences.isModifyReused(), false);
        if (_isAffectingInstanceObject) {
          EObject _eContainer_3 = compinst.eContainer();
          this.denyImplementnUpdatePropagn(compinst, ((ComponentInstance) _eContainer_3), false);
        } else {
          this.setSubcomponentType(compinst, compinst.getClassifier());
        }
      }
      EObject featadd = this.createFeature(compinst, comptype, featinst);
      String _name_2 = featinst.getName();
      boolean _tripleNotEquals = (_name_2 != null);
      if (_tripleNotEquals) {
        this.manipulation.set(featadd, this.aadl2Package.getNamedElement_Name(), featinst.getName());
      }
      if (((featadd instanceof DirectedFeature) && (featinst.getDirection() != null))) {
        DirectionType _direction = featinst.getDirection();
        boolean _equals_1 = Objects.equal(_direction, DirectionType.IN);
        if (_equals_1) {
          this.manipulation.set(featadd, this.aadl2Package.getDirectedFeature_In(), Boolean.valueOf(true));
          this.manipulation.set(featadd, this.aadl2Package.getDirectedFeature_Out(), Boolean.valueOf(false));
        } else {
          DirectionType _direction_1 = featinst.getDirection();
          boolean _equals_2 = Objects.equal(_direction_1, DirectionType.OUT);
          if (_equals_2) {
            this.manipulation.set(featadd, this.aadl2Package.getDirectedFeature_In(), Boolean.valueOf(false));
            this.manipulation.set(featadd, this.aadl2Package.getDirectedFeature_Out(), Boolean.valueOf(true));
          } else {
            this.manipulation.set(featadd, this.aadl2Package.getDirectedFeature_In(), Boolean.valueOf(true));
            this.manipulation.set(featadd, this.aadl2Package.getDirectedFeature_Out(), Boolean.valueOf(true));
          }
        }
      }
      ComponentInstance _type = featinst.getType();
      boolean _tripleNotEquals_1 = (_type != null);
      if (_tripleNotEquals_1) {
        if ((featadd instanceof DataAccess)) {
          ComponentClassifier _classifier_1 = featinst.getType().getClassifier();
          boolean _tripleNotEquals_2 = (_classifier_1 != null);
          if (_tripleNotEquals_2) {
            this.manipulation.set(featadd, this.aadl2Package.getDataAccess_DataFeatureClassifier(), featinst.getType().getClassifier());
          }
        }
      }
      this.manipulation.set(featinst, this.instPackage.getFeatureInstance_Feature(), featadd);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected EObject createFeature(final ComponentInstance compinst, final ComponentType comptype, final FeatureInstance featinst) {
    try {
      EObject _switchResult = null;
      ComponentCategory _category = compinst.getCategory();
      if (_category != null) {
        switch (_category) {
          case ABSTRACT:
            EObject _switchResult_1 = null;
            FeatureCategory _category_1 = featinst.getCategory();
            if (_category_1 != null) {
              switch (_category_1) {
                case DATA_PORT:
                  _switchResult_1 = this.manipulation.createChild(comptype, this.aadl2Package.getAbstractType_OwnedDataPort(), this.aadl2Package.getDataPort());
                  break;
                case EVENT_PORT:
                  _switchResult_1 = this.manipulation.createChild(comptype, this.aadl2Package.getAbstractType_OwnedEventPort(), this.aadl2Package.getEventPort());
                  break;
                case EVENT_DATA_PORT:
                  _switchResult_1 = this.manipulation.createChild(comptype, this.aadl2Package.getAbstractType_OwnedEventDataPort(), this.aadl2Package.getEventDataPort());
                  break;
                case BUS_ACCESS:
                  _switchResult_1 = this.manipulation.createChild(comptype, this.aadl2Package.getAbstractType_OwnedBusAccess(), this.aadl2Package.getBusAccess());
                  break;
                case DATA_ACCESS:
                  _switchResult_1 = this.manipulation.createChild(comptype, this.aadl2Package.getAbstractType_OwnedDataAccess(), this.aadl2Package.getDataAccess());
                  break;
                case SUBPROGRAM_ACCESS:
                  _switchResult_1 = this.manipulation.createChild(comptype, this.aadl2Package.getAbstractType_OwnedSubprogramAccess(), this.aadl2Package.getSubprogramAccess());
                  break;
                case SUBPROGRAM_GROUP_ACCESS:
                  _switchResult_1 = this.manipulation.createChild(comptype, this.aadl2Package.getAbstractType_OwnedSubprogramGroupAccess(), this.aadl2Package.getSubprogramGroupAccess());
                  break;
                case FEATURE_GROUP:
                  _switchResult_1 = this.manipulation.createChild(comptype, this.aadl2Package.getComponentType_OwnedFeatureGroup(), this.aadl2Package.getFeatureGroup());
                  break;
                case ABSTRACT_FEATURE:
                  _switchResult_1 = this.manipulation.createChild(comptype, this.aadl2Package.getComponentType_OwnedAbstractFeature(), this.aadl2Package.getAbstractFeature());
                  break;
                default:
                  _switchResult_1 = null;
                  break;
              }
            } else {
              _switchResult_1 = null;
            }
            _switchResult = _switchResult_1;
            break;
          case BUS:
            EObject _switchResult_2 = null;
            FeatureCategory _category_2 = featinst.getCategory();
            if (_category_2 != null) {
              switch (_category_2) {
                case DATA_PORT:
                  _switchResult_2 = this.manipulation.createChild(comptype, this.aadl2Package.getBusType_OwnedDataPort(), this.aadl2Package.getDataPort());
                  break;
                case EVENT_PORT:
                  _switchResult_2 = this.manipulation.createChild(comptype, this.aadl2Package.getBusType_OwnedEventPort(), this.aadl2Package.getEventPort());
                  break;
                case EVENT_DATA_PORT:
                  _switchResult_2 = this.manipulation.createChild(comptype, this.aadl2Package.getBusType_OwnedEventDataPort(), this.aadl2Package.getEventDataPort());
                  break;
                case BUS_ACCESS:
                  _switchResult_2 = this.manipulation.createChild(comptype, this.aadl2Package.getBusType_OwnedBusAccess(), this.aadl2Package.getBusAccess());
                  break;
                case FEATURE_GROUP:
                  _switchResult_2 = this.manipulation.createChild(comptype, this.aadl2Package.getComponentType_OwnedFeatureGroup(), this.aadl2Package.getFeatureGroup());
                  break;
                case ABSTRACT_FEATURE:
                  _switchResult_2 = this.manipulation.createChild(comptype, this.aadl2Package.getComponentType_OwnedAbstractFeature(), this.aadl2Package.getAbstractFeature());
                  break;
                default:
                  _switchResult_2 = null;
                  break;
              }
            } else {
              _switchResult_2 = null;
            }
            _switchResult = _switchResult_2;
            break;
          case DATA:
            EObject _switchResult_3 = null;
            FeatureCategory _category_3 = featinst.getCategory();
            if (_category_3 != null) {
              switch (_category_3) {
                case DATA_ACCESS:
                  _switchResult_3 = this.manipulation.createChild(comptype, this.aadl2Package.getDataType_OwnedDataAccess(), this.aadl2Package.getDataAccess());
                  break;
                case SUBPROGRAM_ACCESS:
                  _switchResult_3 = this.manipulation.createChild(comptype, this.aadl2Package.getDataType_OwnedSubprogramAccess(), this.aadl2Package.getSubprogramAccess());
                  break;
                case SUBPROGRAM_GROUP_ACCESS:
                  _switchResult_3 = this.manipulation.createChild(comptype, this.aadl2Package.getDataType_OwnedSubprogramGroupAccess(), this.aadl2Package.getSubprogramGroupAccess());
                  break;
                case FEATURE_GROUP:
                  _switchResult_3 = this.manipulation.createChild(comptype, this.aadl2Package.getComponentType_OwnedFeatureGroup(), this.aadl2Package.getFeatureGroup());
                  break;
                case ABSTRACT_FEATURE:
                  _switchResult_3 = this.manipulation.createChild(comptype, this.aadl2Package.getComponentType_OwnedAbstractFeature(), this.aadl2Package.getAbstractFeature());
                  break;
                default:
                  _switchResult_3 = null;
                  break;
              }
            } else {
              _switchResult_3 = null;
            }
            _switchResult = _switchResult_3;
            break;
          case DEVICE:
            EObject _switchResult_4 = null;
            FeatureCategory _category_4 = featinst.getCategory();
            if (_category_4 != null) {
              switch (_category_4) {
                case DATA_PORT:
                  _switchResult_4 = this.manipulation.createChild(comptype, this.aadl2Package.getDeviceType_OwnedDataPort(), this.aadl2Package.getDataPort());
                  break;
                case EVENT_PORT:
                  _switchResult_4 = this.manipulation.createChild(comptype, this.aadl2Package.getDeviceType_OwnedEventPort(), this.aadl2Package.getEventPort());
                  break;
                case EVENT_DATA_PORT:
                  _switchResult_4 = this.manipulation.createChild(comptype, this.aadl2Package.getDeviceType_OwnedEventDataPort(), this.aadl2Package.getEventDataPort());
                  break;
                case BUS_ACCESS:
                  _switchResult_4 = this.manipulation.createChild(comptype, this.aadl2Package.getDeviceType_OwnedBusAccess(), this.aadl2Package.getBusAccess());
                  break;
                case SUBPROGRAM_ACCESS:
                  _switchResult_4 = this.manipulation.createChild(comptype, this.aadl2Package.getDeviceType_OwnedSubprogramAccess(), this.aadl2Package.getSubprogramAccess());
                  break;
                case SUBPROGRAM_GROUP_ACCESS:
                  _switchResult_4 = this.manipulation.createChild(comptype, this.aadl2Package.getDeviceType_OwnedSubprogramGroupAccess(), this.aadl2Package.getSubprogramGroupAccess());
                  break;
                case FEATURE_GROUP:
                  _switchResult_4 = this.manipulation.createChild(comptype, this.aadl2Package.getComponentType_OwnedFeatureGroup(), this.aadl2Package.getFeatureGroup());
                  break;
                case ABSTRACT_FEATURE:
                  _switchResult_4 = this.manipulation.createChild(comptype, this.aadl2Package.getComponentType_OwnedAbstractFeature(), this.aadl2Package.getAbstractFeature());
                  break;
                default:
                  _switchResult_4 = null;
                  break;
              }
            } else {
              _switchResult_4 = null;
            }
            _switchResult = _switchResult_4;
            break;
          case MEMORY:
            EObject _switchResult_5 = null;
            FeatureCategory _category_5 = featinst.getCategory();
            if (_category_5 != null) {
              switch (_category_5) {
                case DATA_PORT:
                  _switchResult_5 = this.manipulation.createChild(comptype, this.aadl2Package.getMemoryType_OwnedDataPort(), this.aadl2Package.getDataPort());
                  break;
                case EVENT_PORT:
                  _switchResult_5 = this.manipulation.createChild(comptype, this.aadl2Package.getMemoryType_OwnedEventPort(), this.aadl2Package.getEventPort());
                  break;
                case EVENT_DATA_PORT:
                  _switchResult_5 = this.manipulation.createChild(comptype, this.aadl2Package.getMemoryType_OwnedEventDataPort(), this.aadl2Package.getEventDataPort());
                  break;
                case BUS_ACCESS:
                  _switchResult_5 = this.manipulation.createChild(comptype, this.aadl2Package.getMemoryType_OwnedBusAccess(), this.aadl2Package.getBusAccess());
                  break;
                case FEATURE_GROUP:
                  _switchResult_5 = this.manipulation.createChild(comptype, this.aadl2Package.getComponentType_OwnedFeatureGroup(), this.aadl2Package.getFeatureGroup());
                  break;
                case ABSTRACT_FEATURE:
                  _switchResult_5 = this.manipulation.createChild(comptype, this.aadl2Package.getComponentType_OwnedAbstractFeature(), this.aadl2Package.getAbstractFeature());
                  break;
                default:
                  _switchResult_5 = null;
                  break;
              }
            } else {
              _switchResult_5 = null;
            }
            _switchResult = _switchResult_5;
            break;
          case PROCESS:
            EObject _switchResult_6 = null;
            FeatureCategory _category_6 = featinst.getCategory();
            if (_category_6 != null) {
              switch (_category_6) {
                case DATA_PORT:
                  _switchResult_6 = this.manipulation.createChild(comptype, this.aadl2Package.getProcessType_OwnedDataPort(), this.aadl2Package.getDataPort());
                  break;
                case EVENT_PORT:
                  _switchResult_6 = this.manipulation.createChild(comptype, this.aadl2Package.getProcessType_OwnedEventPort(), this.aadl2Package.getEventPort());
                  break;
                case EVENT_DATA_PORT:
                  _switchResult_6 = this.manipulation.createChild(comptype, this.aadl2Package.getProcessType_OwnedEventDataPort(), this.aadl2Package.getEventDataPort());
                  break;
                case DATA_ACCESS:
                  _switchResult_6 = this.manipulation.createChild(comptype, this.aadl2Package.getProcessType_OwnedDataAccess(), this.aadl2Package.getDataAccess());
                  break;
                case SUBPROGRAM_ACCESS:
                  _switchResult_6 = this.manipulation.createChild(comptype, this.aadl2Package.getProcessType_OwnedSubprogramAccess(), this.aadl2Package.getSubprogramAccess());
                  break;
                case SUBPROGRAM_GROUP_ACCESS:
                  _switchResult_6 = this.manipulation.createChild(comptype, this.aadl2Package.getProcessType_OwnedSubprogramGroupAccess(), this.aadl2Package.getSubprogramGroupAccess());
                  break;
                case FEATURE_GROUP:
                  _switchResult_6 = this.manipulation.createChild(comptype, this.aadl2Package.getComponentType_OwnedFeatureGroup(), this.aadl2Package.getFeatureGroup());
                  break;
                case ABSTRACT_FEATURE:
                  _switchResult_6 = this.manipulation.createChild(comptype, this.aadl2Package.getComponentType_OwnedAbstractFeature(), this.aadl2Package.getAbstractFeature());
                  break;
                default:
                  _switchResult_6 = null;
                  break;
              }
            } else {
              _switchResult_6 = null;
            }
            _switchResult = _switchResult_6;
            break;
          case PROCESSOR:
            EObject _switchResult_7 = null;
            FeatureCategory _category_7 = featinst.getCategory();
            if (_category_7 != null) {
              switch (_category_7) {
                case DATA_PORT:
                  _switchResult_7 = this.manipulation.createChild(comptype, this.aadl2Package.getProcessorType_OwnedDataPort(), this.aadl2Package.getDataPort());
                  break;
                case EVENT_PORT:
                  _switchResult_7 = this.manipulation.createChild(comptype, this.aadl2Package.getProcessorType_OwnedEventPort(), this.aadl2Package.getEventPort());
                  break;
                case EVENT_DATA_PORT:
                  _switchResult_7 = this.manipulation.createChild(comptype, this.aadl2Package.getProcessorType_OwnedEventDataPort(), this.aadl2Package.getEventDataPort());
                  break;
                case BUS_ACCESS:
                  _switchResult_7 = this.manipulation.createChild(comptype, this.aadl2Package.getProcessorType_OwnedBusAccess(), this.aadl2Package.getBusAccess());
                  break;
                case SUBPROGRAM_ACCESS:
                  _switchResult_7 = this.manipulation.createChild(comptype, this.aadl2Package.getProcessorType_OwnedSubprogramAccess(), this.aadl2Package.getSubprogramAccess());
                  break;
                case SUBPROGRAM_GROUP_ACCESS:
                  _switchResult_7 = this.manipulation.createChild(comptype, this.aadl2Package.getProcessorType_OwnedSubprogramGroupAccess(), this.aadl2Package.getSubprogramGroupAccess());
                  break;
                case FEATURE_GROUP:
                  _switchResult_7 = this.manipulation.createChild(comptype, this.aadl2Package.getComponentType_OwnedFeatureGroup(), this.aadl2Package.getFeatureGroup());
                  break;
                case ABSTRACT_FEATURE:
                  _switchResult_7 = this.manipulation.createChild(comptype, this.aadl2Package.getComponentType_OwnedAbstractFeature(), this.aadl2Package.getAbstractFeature());
                  break;
                default:
                  _switchResult_7 = null;
                  break;
              }
            } else {
              _switchResult_7 = null;
            }
            _switchResult = _switchResult_7;
            break;
          case SUBPROGRAM:
            EObject _switchResult_8 = null;
            FeatureCategory _category_8 = featinst.getCategory();
            if (_category_8 != null) {
              switch (_category_8) {
                case EVENT_PORT:
                  _switchResult_8 = this.manipulation.createChild(comptype, this.aadl2Package.getSubprogramType_OwnedEventPort(), this.aadl2Package.getEventPort());
                  break;
                case EVENT_DATA_PORT:
                  _switchResult_8 = this.manipulation.createChild(comptype, this.aadl2Package.getSubprogramType_OwnedEventDataPort(), this.aadl2Package.getEventDataPort());
                  break;
                case PARAMETER:
                  _switchResult_8 = this.manipulation.createChild(comptype, this.aadl2Package.getSubprogramType_OwnedParameter(), this.aadl2Package.getParameter());
                  break;
                case DATA_ACCESS:
                  _switchResult_8 = this.manipulation.createChild(comptype, this.aadl2Package.getSubprogramType_OwnedDataAccess(), this.aadl2Package.getDataAccess());
                  break;
                case SUBPROGRAM_ACCESS:
                  _switchResult_8 = this.manipulation.createChild(comptype, this.aadl2Package.getSubprogramType_OwnedSubprogramAccess(), this.aadl2Package.getSubprogramAccess());
                  break;
                case SUBPROGRAM_GROUP_ACCESS:
                  _switchResult_8 = this.manipulation.createChild(comptype, this.aadl2Package.getSubprogramType_OwnedSubprogramGroupAccess(), this.aadl2Package.getSubprogramGroupAccess());
                  break;
                case FEATURE_GROUP:
                  _switchResult_8 = this.manipulation.createChild(comptype, this.aadl2Package.getComponentType_OwnedFeatureGroup(), this.aadl2Package.getFeatureGroup());
                  break;
                case ABSTRACT_FEATURE:
                  _switchResult_8 = this.manipulation.createChild(comptype, this.aadl2Package.getComponentType_OwnedAbstractFeature(), this.aadl2Package.getAbstractFeature());
                  break;
                default:
                  _switchResult_8 = null;
                  break;
              }
            } else {
              _switchResult_8 = null;
            }
            _switchResult = _switchResult_8;
            break;
          case SUBPROGRAM_GROUP:
            EObject _switchResult_9 = null;
            FeatureCategory _category_9 = featinst.getCategory();
            if (_category_9 != null) {
              switch (_category_9) {
                case SUBPROGRAM_ACCESS:
                  _switchResult_9 = this.manipulation.createChild(comptype, this.aadl2Package.getSubprogramGroupType_OwnedSubprogramAccess(), this.aadl2Package.getSubprogramAccess());
                  break;
                case SUBPROGRAM_GROUP_ACCESS:
                  _switchResult_9 = this.manipulation.createChild(comptype, this.aadl2Package.getSubprogramGroupType_OwnedSubprogramGroupAccess(), this.aadl2Package.getSubprogramGroupAccess());
                  break;
                case FEATURE_GROUP:
                  _switchResult_9 = this.manipulation.createChild(comptype, this.aadl2Package.getComponentType_OwnedFeatureGroup(), this.aadl2Package.getFeatureGroup());
                  break;
                case ABSTRACT_FEATURE:
                  _switchResult_9 = this.manipulation.createChild(comptype, this.aadl2Package.getComponentType_OwnedAbstractFeature(), this.aadl2Package.getAbstractFeature());
                  break;
                default:
                  _switchResult_9 = null;
                  break;
              }
            } else {
              _switchResult_9 = null;
            }
            _switchResult = _switchResult_9;
            break;
          case SYSTEM:
            EObject _switchResult_10 = null;
            FeatureCategory _category_10 = featinst.getCategory();
            if (_category_10 != null) {
              switch (_category_10) {
                case DATA_PORT:
                  _switchResult_10 = this.manipulation.createChild(comptype, this.aadl2Package.getSystemType_OwnedDataPort(), this.aadl2Package.getDataPort());
                  break;
                case EVENT_PORT:
                  _switchResult_10 = this.manipulation.createChild(comptype, this.aadl2Package.getSystemType_OwnedEventPort(), this.aadl2Package.getEventPort());
                  break;
                case EVENT_DATA_PORT:
                  _switchResult_10 = this.manipulation.createChild(comptype, this.aadl2Package.getSystemType_OwnedEventDataPort(), this.aadl2Package.getEventDataPort());
                  break;
                case BUS_ACCESS:
                  _switchResult_10 = this.manipulation.createChild(comptype, this.aadl2Package.getSystemType_OwnedBusAccess(), this.aadl2Package.getBusAccess());
                  break;
                case DATA_ACCESS:
                  _switchResult_10 = this.manipulation.createChild(comptype, this.aadl2Package.getSystemType_OwnedDataAccess(), this.aadl2Package.getDataAccess());
                  break;
                case SUBPROGRAM_ACCESS:
                  _switchResult_10 = this.manipulation.createChild(comptype, this.aadl2Package.getSystemType_OwnedSubprogramAccess(), this.aadl2Package.getSubprogramAccess());
                  break;
                case SUBPROGRAM_GROUP_ACCESS:
                  _switchResult_10 = this.manipulation.createChild(comptype, this.aadl2Package.getSystemType_OwnedSubprogramGroupAccess(), this.aadl2Package.getSubprogramGroupAccess());
                  break;
                case FEATURE_GROUP:
                  _switchResult_10 = this.manipulation.createChild(comptype, this.aadl2Package.getComponentType_OwnedFeatureGroup(), this.aadl2Package.getFeatureGroup());
                  break;
                case ABSTRACT_FEATURE:
                  _switchResult_10 = this.manipulation.createChild(comptype, this.aadl2Package.getComponentType_OwnedAbstractFeature(), this.aadl2Package.getAbstractFeature());
                  break;
                default:
                  _switchResult_10 = null;
                  break;
              }
            } else {
              _switchResult_10 = null;
            }
            _switchResult = _switchResult_10;
            break;
          case THREAD:
            EObject _switchResult_11 = null;
            FeatureCategory _category_11 = featinst.getCategory();
            if (_category_11 != null) {
              switch (_category_11) {
                case DATA_PORT:
                  _switchResult_11 = this.manipulation.createChild(comptype, this.aadl2Package.getThreadType_OwnedDataPort(), this.aadl2Package.getDataPort());
                  break;
                case EVENT_PORT:
                  _switchResult_11 = this.manipulation.createChild(comptype, this.aadl2Package.getThreadType_OwnedEventPort(), this.aadl2Package.getEventPort());
                  break;
                case EVENT_DATA_PORT:
                  _switchResult_11 = this.manipulation.createChild(comptype, this.aadl2Package.getThreadType_OwnedEventDataPort(), this.aadl2Package.getEventDataPort());
                  break;
                case DATA_ACCESS:
                  _switchResult_11 = this.manipulation.createChild(comptype, this.aadl2Package.getThreadType_OwnedDataAccess(), this.aadl2Package.getDataAccess());
                  break;
                case SUBPROGRAM_ACCESS:
                  _switchResult_11 = this.manipulation.createChild(comptype, this.aadl2Package.getThreadType_OwnedSubprogramAccess(), this.aadl2Package.getSubprogramAccess());
                  break;
                case SUBPROGRAM_GROUP_ACCESS:
                  _switchResult_11 = this.manipulation.createChild(comptype, this.aadl2Package.getThreadType_OwnedSubprogramGroupAccess(), this.aadl2Package.getSubprogramGroupAccess());
                  break;
                case FEATURE_GROUP:
                  _switchResult_11 = this.manipulation.createChild(comptype, this.aadl2Package.getComponentType_OwnedFeatureGroup(), this.aadl2Package.getFeatureGroup());
                  break;
                case ABSTRACT_FEATURE:
                  _switchResult_11 = this.manipulation.createChild(comptype, this.aadl2Package.getComponentType_OwnedAbstractFeature(), this.aadl2Package.getAbstractFeature());
                  break;
                default:
                  _switchResult_11 = null;
                  break;
              }
            } else {
              _switchResult_11 = null;
            }
            _switchResult = _switchResult_11;
            break;
          case THREAD_GROUP:
            EObject _switchResult_12 = null;
            FeatureCategory _category_12 = featinst.getCategory();
            if (_category_12 != null) {
              switch (_category_12) {
                case DATA_PORT:
                  _switchResult_12 = this.manipulation.createChild(comptype, this.aadl2Package.getThreadGroupType_OwnedDataPort(), this.aadl2Package.getDataPort());
                  break;
                case EVENT_PORT:
                  _switchResult_12 = this.manipulation.createChild(comptype, this.aadl2Package.getThreadGroupType_OwnedEventPort(), this.aadl2Package.getEventPort());
                  break;
                case EVENT_DATA_PORT:
                  _switchResult_12 = this.manipulation.createChild(comptype, this.aadl2Package.getThreadGroupType_OwnedEventDataPort(), this.aadl2Package.getEventDataPort());
                  break;
                case DATA_ACCESS:
                  _switchResult_12 = this.manipulation.createChild(comptype, this.aadl2Package.getThreadGroupType_OwnedDataAccess(), this.aadl2Package.getDataAccess());
                  break;
                case SUBPROGRAM_ACCESS:
                  _switchResult_12 = this.manipulation.createChild(comptype, this.aadl2Package.getThreadGroupType_OwnedSubprogramAccess(), this.aadl2Package.getSubprogramAccess());
                  break;
                case SUBPROGRAM_GROUP_ACCESS:
                  _switchResult_12 = this.manipulation.createChild(comptype, this.aadl2Package.getThreadGroupType_OwnedSubprogramGroupAccess(), this.aadl2Package.getSubprogramGroupAccess());
                  break;
                case FEATURE_GROUP:
                  _switchResult_12 = this.manipulation.createChild(comptype, this.aadl2Package.getComponentType_OwnedFeatureGroup(), this.aadl2Package.getFeatureGroup());
                  break;
                case ABSTRACT_FEATURE:
                  _switchResult_12 = this.manipulation.createChild(comptype, this.aadl2Package.getComponentType_OwnedAbstractFeature(), this.aadl2Package.getAbstractFeature());
                  break;
                default:
                  _switchResult_12 = null;
                  break;
              }
            } else {
              _switchResult_12 = null;
            }
            _switchResult = _switchResult_12;
            break;
          case VIRTUAL_BUS:
            EObject _switchResult_13 = null;
            FeatureCategory _category_13 = featinst.getCategory();
            if (_category_13 != null) {
              switch (_category_13) {
                case DATA_PORT:
                  _switchResult_13 = this.manipulation.createChild(comptype, this.aadl2Package.getVirtualBusType_OwnedDataPort(), this.aadl2Package.getDataPort());
                  break;
                case EVENT_PORT:
                  _switchResult_13 = this.manipulation.createChild(comptype, this.aadl2Package.getVirtualBusType_OwnedEventPort(), this.aadl2Package.getEventPort());
                  break;
                case EVENT_DATA_PORT:
                  _switchResult_13 = this.manipulation.createChild(comptype, this.aadl2Package.getVirtualBusType_OwnedEventDataPort(), this.aadl2Package.getEventDataPort());
                  break;
                case BUS_ACCESS:
                  _switchResult_13 = this.manipulation.createChild(comptype, this.aadl2Package.getVirtualBusType_OwnedBusAccess(), this.aadl2Package.getBusAccess());
                  break;
                case FEATURE_GROUP:
                  _switchResult_13 = this.manipulation.createChild(comptype, this.aadl2Package.getComponentType_OwnedFeatureGroup(), this.aadl2Package.getFeatureGroup());
                  break;
                case ABSTRACT_FEATURE:
                  _switchResult_13 = this.manipulation.createChild(comptype, this.aadl2Package.getComponentType_OwnedAbstractFeature(), this.aadl2Package.getAbstractFeature());
                  break;
                default:
                  _switchResult_13 = null;
                  break;
              }
            } else {
              _switchResult_13 = null;
            }
            _switchResult = _switchResult_13;
            break;
          case VIRTUAL_PROCESSOR:
            EObject _switchResult_14 = null;
            FeatureCategory _category_14 = featinst.getCategory();
            if (_category_14 != null) {
              switch (_category_14) {
                case DATA_PORT:
                  _switchResult_14 = this.manipulation.createChild(comptype, this.aadl2Package.getVirtualProcessorType_OwnedDataPort(), this.aadl2Package.getDataPort());
                  break;
                case EVENT_PORT:
                  _switchResult_14 = this.manipulation.createChild(comptype, this.aadl2Package.getVirtualProcessorType_OwnedEventPort(), this.aadl2Package.getEventPort());
                  break;
                case EVENT_DATA_PORT:
                  _switchResult_14 = this.manipulation.createChild(comptype, this.aadl2Package.getVirtualProcessorType_OwnedEventDataPort(), this.aadl2Package.getEventDataPort());
                  break;
                case BUS_ACCESS:
                  _switchResult_14 = this.manipulation.createChild(comptype, this.aadl2Package.getVirtualProcessorType_OwnedBusAccess(), this.aadl2Package.getBusAccess());
                  break;
                case SUBPROGRAM_ACCESS:
                  _switchResult_14 = this.manipulation.createChild(comptype, this.aadl2Package.getVirtualProcessorType_OwnedSubprogramAccess(), this.aadl2Package.getSubprogramAccess());
                  break;
                case SUBPROGRAM_GROUP_ACCESS:
                  _switchResult_14 = this.manipulation.createChild(comptype, this.aadl2Package.getVirtualProcessorType_OwnedSubprogramGroupAccess(), this.aadl2Package.getSubprogramGroupAccess());
                  break;
                case FEATURE_GROUP:
                  _switchResult_14 = this.manipulation.createChild(comptype, this.aadl2Package.getComponentType_OwnedFeatureGroup(), this.aadl2Package.getFeatureGroup());
                  break;
                case ABSTRACT_FEATURE:
                  _switchResult_14 = this.manipulation.createChild(comptype, this.aadl2Package.getComponentType_OwnedAbstractFeature(), this.aadl2Package.getAbstractFeature());
                  break;
                default:
                  _switchResult_14 = null;
                  break;
              }
            } else {
              _switchResult_14 = null;
            }
            _switchResult = _switchResult_14;
            break;
          default:
            break;
        }
      }
      return _switchResult;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected void connectionInstanceCreatedDIM(final ConnectionInstance conninst) {
    try {
      EList<ComponentInstance> complist = ConnectionUtils.getPathBetweenConnectionInstanceEnds(conninst.getSource(), conninst.getDestination());
      boolean _isNullOrEmpty = IterableExtensions.isNullOrEmpty(conninst.getConnectionReferences());
      if (_isNullOrEmpty) {
        this.manipulation.createChild(conninst, this.instPackage.getConnectionInstance_ConnectionReference(), this.instPackage.getConnectionReference());
      }
      final EList<ConnectionReference> connreflist = conninst.getConnectionReferences();
      Integer _max = this.max(Integer.valueOf(connreflist.size()), Integer.valueOf(complist.size()));
      ExclusiveRange _doubleDotLessThan = new ExclusiveRange(0, (_max).intValue(), true);
      for (final Integer i : _doubleDotLessThan) {
        {
          if ((((i).intValue() >= complist.size()) && (connreflist.get((i).intValue()) != null))) {
            this.manipulation.remove(conninst, this.instPackage.getConnectionInstance_ConnectionReference(), connreflist.get((i).intValue()));
          } else {
            EObject _xifexpression = null;
            ConnectionReference _get = connreflist.get((i).intValue());
            boolean _tripleNotEquals = (_get != null);
            if (_tripleNotEquals) {
              _xifexpression = connreflist.get((i).intValue());
            } else {
              EObject _xblockexpression = null;
              {
                String _string = i.toString();
                String _plus = ("DIM: " + _string);
                String _plus_1 = (_plus + "th connection reference for connection ");
                String _name = conninst.getName();
                String _plus_2 = (_plus_1 + _name);
                String _plus_3 = (_plus_2 + " created");
                this.LOGGER.logInfo(_plus_3);
                _xblockexpression = this.manipulation.createChild(conninst, this.instPackage.getConnectionInstance_ConnectionReference(), this.instPackage.getConnectionReference());
              }
              _xifexpression = _xblockexpression;
            }
            ConnectionReference connref = ((ConnectionReference) _xifexpression);
            ComponentInstance _context = connref.getContext();
            boolean _tripleEquals = (_context == null);
            if (_tripleEquals) {
              this.manipulation.set(connref, this.instPackage.getConnectionReference_Context(), complist.get((i).intValue()));
              String _string = i.toString();
              String _plus = ("DIM: Context of the " + _string);
              String _plus_1 = (_plus + "th connection reference for connection ");
              String _name = conninst.getName();
              String _plus_2 = (_plus_1 + _name);
              String _plus_3 = (_plus_2 + " added");
              this.LOGGER.logInfo(_plus_3);
            } else {
              ComponentInstance _context_1 = connref.getContext();
              ComponentInstance _get_1 = complist.get((i).intValue());
              boolean _tripleNotEquals_1 = (_context_1 != _get_1);
              if (_tripleNotEquals_1) {
                this.manipulation.set(connref, this.instPackage.getConnectionReference_Context(), complist.get((i).intValue()));
                String _string_1 = i.toString();
                String _plus_4 = ("DIM: Context of the " + _string_1);
                String _plus_5 = (_plus_4 + "th connection reference for connection ");
                String _name_1 = conninst.getName();
                String _plus_6 = (_plus_5 + _name_1);
                String _plus_7 = (_plus_6 + " changed");
                this.LOGGER.logInfo(_plus_7);
                this.manipulation.set(connref, this.instPackage.getConnectionReference_Source(), null);
                this.manipulation.set(connref, this.instPackage.getConnectionReference_Destination(), null);
              }
            }
            ConnectionInstanceEnd _source = connref.getSource();
            boolean _tripleEquals_1 = (_source == null);
            if (_tripleEquals_1) {
              if (((i).intValue() == 0)) {
                this.manipulation.set(connref, this.instPackage.getConnectionReference_Source(), conninst.getSource());
                String _string_2 = i.toString();
                String _plus_8 = ("DIM: Source of the " + _string_2);
                String _plus_9 = (_plus_8 + "th connection reference for connection ");
                String _name_2 = conninst.getName();
                String _plus_10 = (_plus_9 + _name_2);
                String _plus_11 = (_plus_10 + " added");
                this.LOGGER.logInfo(_plus_11);
              } else {
                ConnectionInstanceEnd _destination = conninst.getConnectionReferences().get(((i).intValue() - 1)).getDestination();
                boolean _tripleNotEquals_2 = (_destination != null);
                if (_tripleNotEquals_2) {
                  this.manipulation.set(connref, this.instPackage.getConnectionReference_Source(), conninst.getConnectionReferences().get(((i).intValue() - 1)).getDestination());
                  String _string_3 = i.toString();
                  String _plus_12 = ("DIM: Source of the " + _string_3);
                  String _plus_13 = (_plus_12 + "th connection reference for connection ");
                  String _name_3 = conninst.getName();
                  String _plus_14 = (_plus_13 + _name_3);
                  String _plus_15 = (_plus_14 + " added");
                  this.LOGGER.logInfo(_plus_15);
                }
              }
            } else {
              if ((((i).intValue() == 0) && (connref.getSource() != conninst.getSource()))) {
                this.manipulation.set(connref, this.instPackage.getConnectionReference_Source(), conninst.getSource());
                String _string_4 = i.toString();
                String _plus_16 = ("DIM: Source of the " + _string_4);
                String _plus_17 = (_plus_16 + "th connection reference for connection ");
                String _name_4 = conninst.getName();
                String _plus_18 = (_plus_17 + _name_4);
                String _plus_19 = (_plus_18 + " changed");
                this.LOGGER.logWarning(_plus_19);
              } else {
                if (((((i).intValue() != 0) && (conninst.getConnectionReferences().get(((i).intValue() - 1)).getDestination() != null)) && (connref.getSource() != conninst.getConnectionReferences().get(((i).intValue() - 1)).getDestination()))) {
                  this.manipulation.set(connref, this.instPackage.getConnectionReference_Source(), conninst.getConnectionReferences().get(((i).intValue() - 1)).getDestination());
                  String _string_5 = i.toString();
                  String _plus_20 = ("DIM: Source of the " + _string_5);
                  String _plus_21 = (_plus_20 + "th connection reference for connection ");
                  String _name_5 = conninst.getName();
                  String _plus_22 = (_plus_21 + _name_5);
                  String _plus_23 = (_plus_22 + " changed");
                  this.LOGGER.logWarning(_plus_23);
                }
              }
            }
            ConnectionInstanceEnd _destination_1 = connref.getDestination();
            boolean _tripleEquals_2 = (_destination_1 == null);
            if (_tripleEquals_2) {
              int _size = complist.size();
              int _minus = (_size - 1);
              boolean _equals = ((i).intValue() == _minus);
              if (_equals) {
                this.manipulation.set(connref, this.instPackage.getConnectionReference_Destination(), conninst.getDestination());
                String _string_6 = i.toString();
                String _plus_24 = ("DIM: Destination of the " + _string_6);
                String _plus_25 = (_plus_24 + "th connection reference for connection ");
                String _name_6 = conninst.getName();
                String _plus_26 = (_plus_25 + _name_6);
                String _plus_27 = (_plus_26 + " added");
                this.LOGGER.logInfo(_plus_27);
              } else {
                boolean _isCreateInterFeatures = this.preferences.isCreateInterFeatures();
                if (_isCreateInterFeatures) {
                  Object _xifexpression_1 = null;
                  EObject _eContainer = complist.get(((i).intValue() + 1)).eContainer();
                  ComponentInstance _get_2 = complist.get((i).intValue());
                  boolean _equals_1 = Objects.equal(_eContainer, _get_2);
                  if (_equals_1) {
                    _xifexpression_1 = complist.get(((i).intValue() + 1));
                  } else {
                    _xifexpression_1 = complist;
                  }
                  FeatureInstance feataddcompinst = ((FeatureInstance) _xifexpression_1);
                  EObject _createChild = this.manipulation.createChild(feataddcompinst, this.instPackage.getComponentInstance_FeatureInstance(), this.instPackage.getFeatureInstance());
                  FeatureInstance dstfeatinst = ((FeatureInstance) _createChild);
                  ConnectionInstanceEnd _source_1 = conninst.getSource();
                  if ((_source_1 instanceof FeatureInstance)) {
                    ConnectionInstanceEnd _source_2 = conninst.getSource();
                    this.manipulation.set(dstfeatinst, this.instPackage.getFeatureInstance_Category(), ((FeatureInstance) _source_2).getCategory());
                  } else {
                    ConnectionInstanceEnd _destination_2 = conninst.getDestination();
                    this.manipulation.set(dstfeatinst, this.instPackage.getFeatureInstance_Category(), ((FeatureInstance) _destination_2).getCategory());
                  }
                  boolean _isBidirectional = conninst.isBidirectional();
                  boolean _equals_2 = (_isBidirectional == true);
                  if (_equals_2) {
                    this.manipulation.set(dstfeatinst, this.instPackage.getFeatureInstance_Direction(), DirectionType.IN_OUT);
                  } else {
                    EObject _eContainer_1 = complist.get(((i).intValue() + 1)).eContainer();
                    ComponentInstance _get_3 = complist.get((i).intValue());
                    boolean _equals_3 = Objects.equal(_eContainer_1, _get_3);
                    if (_equals_3) {
                      this.manipulation.set(dstfeatinst, this.instPackage.getFeatureInstance_Direction(), DirectionType.IN);
                    } else {
                      this.manipulation.set(dstfeatinst, this.instPackage.getFeatureInstance_Direction(), DirectionType.OUT);
                    }
                  }
                  EAttribute _namedElement_Name = this.aadl2Package.getNamedElement_Name();
                  String _name_7 = conninst.getName();
                  String _plus_28 = (_name_7 + "_feat");
                  this.manipulation.set(dstfeatinst, _namedElement_Name, _plus_28);
                  this.manipulation.set(connref, this.instPackage.getConnectionReference_Destination(), dstfeatinst);
                  String _string_7 = i.toString();
                  String _plus_29 = ("DIM: Destination of the " + _string_7);
                  String _plus_30 = (_plus_29 + "th connection reference for connection ");
                  String _name_8 = conninst.getName();
                  String _plus_31 = (_plus_30 + _name_8);
                  String _plus_32 = (_plus_31 + " created");
                  this.LOGGER.logInfo(_plus_32);
                }
              }
            } else {
              if ((((i).intValue() == (complist.size() - 1)) && (connref.getDestination() != conninst.getDestination()))) {
                this.manipulation.set(connref, this.instPackage.getConnectionReference_Destination(), conninst.getDestination());
                this.LOGGER.logWarning("DIM: Destination of the connection reference changed");
              }
            }
          }
          String _name_9 = conninst.getName();
          String _plus_33 = ("DIM: Connection Instance " + _name_9);
          String _plus_34 = (_plus_33 + " transformed to connection reference");
          this.LOGGER.logInfo(_plus_34);
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected Connection createConnection(final ConnectionKind connkind, final ComponentImplementation compimp) {
    try {
      EObject _switchResult = null;
      if (connkind != null) {
        switch (connkind) {
          case ACCESS_CONNECTION:
            _switchResult = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedAccessConnection(), this.aadl2Package.getAccessConnection());
            break;
          case FEATURE_CONNECTION:
            _switchResult = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedFeatureConnection(), this.aadl2Package.getFeatureConnection());
            break;
          case FEATURE_GROUP_CONNECTION:
            _switchResult = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedFeatureGroupConnection(), this.aadl2Package.getFeatureGroupConnection());
            break;
          case PARAMETER_CONNECTION:
            _switchResult = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedParameterConnection(), this.aadl2Package.getParameterConnection());
            break;
          case PORT_CONNECTION:
            _switchResult = this.manipulation.createChild(compimp, this.aadl2Package.getComponentImplementation_OwnedPortConnection(), this.aadl2Package.getPortConnection());
            break;
          default:
            _switchResult = null;
            break;
        }
      } else {
        _switchResult = null;
      }
      return ((Connection) _switchResult);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected void populateSourceConnectedElement(final ConnectedElement connaddsrcelem, final ConnectionReference connref, final Connection connadd) {
    try {
      this.manipulation.set(connaddsrcelem, this.aadl2Package.getConnectedElement_ConnectionEnd(), TraceUtils.getDeclarativeElement(connref.getSource()));
      EObject _eContainer = connref.getSource().eContainer();
      ComponentInstance _context = connref.getContext();
      boolean _tripleNotEquals = (_eContainer != _context);
      if (_tripleNotEquals) {
        EObject _eContainer_1 = connref.getSource().eContainer();
        this.manipulation.set(connaddsrcelem, this.aadl2Package.getConnectedElement_Context(), ((ComponentInstance) _eContainer_1).getSubcomponent());
      } else {
        this.manipulation.set(connaddsrcelem, this.aadl2Package.getConnectedElement_Context(), null);
      }
      if (((connref.getSource() instanceof FeatureInstance) && (connadd instanceof AccessConnection))) {
        ConnectionInstanceEnd _source = connref.getSource();
        Feature srcconnelem = ((FeatureInstance) _source).getFeature();
        if ((srcconnelem instanceof Access)) {
          EObject _eContainer_2 = connref.getSource().eContainer();
          ComponentInstance _context_1 = connref.getContext();
          boolean _notEquals = (!Objects.equal(_eContainer_2, _context_1));
          if (_notEquals) {
            this.manipulation.set(srcconnelem, this.aadl2Package.getAccess_Kind(), AccessType.PROVIDES);
          } else {
            this.manipulation.set(srcconnelem, this.aadl2Package.getAccess_Kind(), AccessType.REQUIRES);
          }
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected void populateDestinationConnectedElement(final ConnectedElement connadddestelem, final ConnectionReference connref, final Connection connadd) {
    try {
      this.manipulation.set(connadddestelem, this.aadl2Package.getConnectedElement_ConnectionEnd(), TraceUtils.getDeclarativeElement(connref.getDestination()));
      EObject _eContainer = connref.getDestination().eContainer();
      ComponentInstance _context = connref.getContext();
      boolean _tripleNotEquals = (_eContainer != _context);
      if (_tripleNotEquals) {
        EObject _eContainer_1 = connref.getDestination().eContainer();
        this.manipulation.set(connadddestelem, this.aadl2Package.getConnectedElement_Context(), ((ComponentInstance) _eContainer_1).getSubcomponent());
      } else {
        this.manipulation.set(connadddestelem, this.aadl2Package.getConnectedElement_Context(), null);
      }
      if (((connref.getDestination() instanceof FeatureInstance) && (connadd instanceof AccessConnection))) {
        ConnectionInstanceEnd _destination = connref.getDestination();
        Feature destconnelem = ((FeatureInstance) _destination).getFeature();
        if ((destconnelem instanceof Access)) {
          ComponentInstance _context_1 = connref.getContext();
          EObject _eContainer_2 = connref.getDestination().eContainer();
          boolean _equals = Objects.equal(_context_1, _eContainer_2);
          if (_equals) {
            this.manipulation.set(destconnelem, this.aadl2Package.getAccess_Kind(), AccessType.PROVIDES);
          } else {
            this.manipulation.set(destconnelem, this.aadl2Package.getAccess_Kind(), AccessType.REQUIRES);
          }
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected void populateNewConnection(final Connection connadd, final ConnectionInstance conninst, final ConnectionReference connref) {
    try {
      this.manipulation.set(connadd, this.aadl2Package.getConnection_Bidirectional(), Boolean.valueOf(conninst.isBidirectional()));
      if ((connadd instanceof AccessConnection)) {
        ConnectionInstanceEnd _source = conninst.getSource();
        if ((_source instanceof FeatureInstance)) {
          ConnectionInstanceEnd _source_1 = conninst.getSource();
          FeatureCategory _category = ((FeatureInstance) _source_1).getCategory();
          if (_category != null) {
            switch (_category) {
              case BUS_ACCESS:
                this.manipulation.set(((AccessConnection) connadd), this.aadl2Package.getAccessConnection_AccessCategory(), AccessCategory.BUS);
                break;
              case DATA_ACCESS:
                this.manipulation.set(((AccessConnection) connadd), this.aadl2Package.getAccessConnection_AccessCategory(), AccessCategory.DATA);
                break;
              case SUBPROGRAM_ACCESS:
                this.manipulation.set(((AccessConnection) connadd), this.aadl2Package.getAccessConnection_AccessCategory(), AccessCategory.SUBPROGRAM);
                break;
              case SUBPROGRAM_GROUP_ACCESS:
                this.manipulation.set(((AccessConnection) connadd), this.aadl2Package.getAccessConnection_AccessCategory(), AccessCategory.SUBPROGRAM_GROUP);
                break;
              default:
                break;
            }
          } else {
          }
        } else {
          ConnectionInstanceEnd _source_2 = conninst.getSource();
          if ((_source_2 instanceof ComponentInstance)) {
            ConnectionInstanceEnd _source_3 = conninst.getSource();
            ComponentCategory _category_1 = ((ComponentInstance) _source_3).getCategory();
            if (_category_1 != null) {
              switch (_category_1) {
                case BUS:
                  this.manipulation.set(((AccessConnection) connadd), this.aadl2Package.getAccessConnection_AccessCategory(), AccessCategory.BUS);
                  break;
                case DATA:
                  this.manipulation.set(((AccessConnection) connadd), this.aadl2Package.getAccessConnection_AccessCategory(), AccessCategory.DATA);
                  break;
                case SUBPROGRAM:
                  this.manipulation.set(((AccessConnection) connadd), this.aadl2Package.getAccessConnection_AccessCategory(), AccessCategory.SUBPROGRAM);
                  break;
                case SUBPROGRAM_GROUP:
                  this.manipulation.set(((AccessConnection) connadd), this.aadl2Package.getAccessConnection_AccessCategory(), AccessCategory.SUBPROGRAM_GROUP);
                  break;
                default:
                  break;
              }
            } else {
            }
          }
        }
      }
      EObject _createChild = this.manipulation.createChild(connadd, this.aadl2Package.getConnection_Source(), this.aadl2Package.getConnectedElement());
      ConnectedElement connaddsrcelem = ((ConnectedElement) _createChild);
      this.populateSourceConnectedElement(connaddsrcelem, connref, connadd);
      EObject _createChild_1 = this.manipulation.createChild(connadd, this.aadl2Package.getConnection_Destination(), this.aadl2Package.getConnectedElement());
      ConnectedElement connadddestelem = ((ConnectedElement) _createChild_1);
      this.populateDestinationConnectedElement(connadddestelem, connref, connadd);
      this.manipulation.set(connref, this.instPackage.getConnectionReference_Connection(), connadd);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected void connectionAdditionPreparation(final ComponentInstance compinst) {
    try {
      ComponentClassifier _xifexpression = null;
      boolean _equals = Objects.equal(compinst, this.topSystemInst);
      if (_equals) {
        _xifexpression = ((SystemInstance) compinst).getComponentImplementation();
      } else {
        _xifexpression = compinst.getClassifier();
      }
      final ComponentClassifier classifier = _xifexpression;
      if ((((classifier instanceof ComponentType) || (LibraryUtils.isLibraryClassifier(classifier, this.aadlPublicPackage)).booleanValue()) || ((LibraryUtils.isReusedClassifier(classifier, this.engine)).booleanValue() && (!this.preferences.isModifyReused())))) {
        ComponentImplementation compimp = this.createComponentImplementation(compinst.getCategory());
        this.classifierCreationDIMPropertyAddition(compimp);
        EAttribute _namedElement_Name = this.aadl2Package.getNamedElement_Name();
        String _name = compinst.getName();
        String _plus = (_name + ".impl");
        this.manipulation.set(compimp, _namedElement_Name, _plus);
        if ((classifier instanceof ComponentType)) {
          this.manipulation.set(compimp, this.aadl2Package.getComponentImplementation_Type(), classifier);
        } else {
          this.manipulation.set(compimp, this.aadl2Package.getComponentImplementation_Extended(), classifier);
        }
        Boolean _isLibraryClassifier = LibraryUtils.isLibraryClassifier(classifier, this.aadlPublicPackage);
        if ((_isLibraryClassifier).booleanValue()) {
          EObject _eContainer = classifier.eContainer().eContainer();
          this.manipulation.add(this.aadlPublicPackage, this.aadl2Package.getPackageSection_ImportedUnit(), ((ModelUnit) _eContainer));
        }
        this.manipulation.set(compinst, this.instPackage.getComponentInstance_Classifier(), compimp);
      }
      if ((compinst != this.topSystemInst)) {
        EObject _eContainer_1 = compinst.eContainer();
        boolean _isAffectingInstanceObject = LibraryUtils.isAffectingInstanceObject(compinst, ((ComponentInstance) _eContainer_1), this.aadlPublicPackage, this.engine, this.preferences.isModifyReused(), false);
        if (_isAffectingInstanceObject) {
          EObject _eContainer_2 = compinst.eContainer();
          this.denyImplementnUpdatePropagn(compinst, ((ComponentInstance) _eContainer_2), false);
        } else {
          this.setSubcomponentType(compinst, compinst.getClassifier());
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected void connectionReferenceFoundDIM(final ConnectionReference connref) {
    try {
      final ComponentInstance compinst = connref.getContext();
      this.connectionAdditionPreparation(compinst);
      EObject _eContainer = connref.eContainer();
      final ConnectionInstance conninst = ((ConnectionInstance) _eContainer);
      ComponentClassifier _classifier = compinst.getClassifier();
      Connection connadd = this.createConnection(conninst.getKind(), ((ComponentImplementation) _classifier));
      this.manipulation.set(connadd, this.aadl2Package.getNamedElement_Name(), ConnectionUtils.getDeclarativeConnectionName(connref));
      this.populateNewConnection(connadd, conninst, connref);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected void modeInstanceCreatedDIM(final ModeInstance modeinst) {
    try {
      EObject _eContainer = modeinst.eContainer();
      final ComponentInstance compinst = ((ComponentInstance) _eContainer);
      if (((LibraryUtils.isLibraryClassifier(compinst.getClassifier(), this.aadlPublicPackage)).booleanValue() || ((LibraryUtils.isReusedClassifier(compinst.getClassifier(), this.engine)).booleanValue() && (!this.preferences.isModifyReused())))) {
        ComponentClassifier _xifexpression = null;
        ComponentClassifier _classifier = compinst.getClassifier();
        if ((_classifier instanceof ComponentImplementation)) {
          EObject _switchResult = null;
          ComponentCategory _category = compinst.getCategory();
          if (_category != null) {
            switch (_category) {
              case ABSTRACT:
                _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getAbstractImplementation());
                break;
              case BUS:
                _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getBusImplementation());
                break;
              case DATA:
                _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getDataImplementation());
                break;
              case DEVICE:
                _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getDeviceImplementation());
                break;
              case MEMORY:
                _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getMemoryImplementation());
                break;
              case PROCESS:
                _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getProcessImplementation());
                break;
              case PROCESSOR:
                _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getProcessorImplementation());
                break;
              case SUBPROGRAM:
                _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getSubprogramImplementation());
                break;
              case SUBPROGRAM_GROUP:
                _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getSubprogramGroupImplementation());
                break;
              case SYSTEM:
                _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getSystemImplementation());
                break;
              case THREAD:
                _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getThreadImplementation());
                break;
              case THREAD_GROUP:
                _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getThreadGroupImplementation());
                break;
              case VIRTUAL_BUS:
                _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getVirtualBusImplementation());
                break;
              case VIRTUAL_PROCESSOR:
                _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getVirtualProcessorImplementation());
                break;
              default:
                break;
            }
          }
          _xifexpression = ((ComponentImplementation) _switchResult);
        } else {
          EObject _switchResult_1 = null;
          ComponentCategory _category_1 = compinst.getCategory();
          if (_category_1 != null) {
            switch (_category_1) {
              case ABSTRACT:
                _switchResult_1 = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getAbstractType());
                break;
              case BUS:
                _switchResult_1 = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getBusType());
                break;
              case DATA:
                _switchResult_1 = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getDataType());
                break;
              case DEVICE:
                _switchResult_1 = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getDeviceType());
                break;
              case MEMORY:
                _switchResult_1 = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getMemoryType());
                break;
              case PROCESS:
                _switchResult_1 = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getProcessType());
                break;
              case PROCESSOR:
                _switchResult_1 = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getProcessorType());
                break;
              case SUBPROGRAM:
                _switchResult_1 = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getSubprogramType());
                break;
              case SUBPROGRAM_GROUP:
                _switchResult_1 = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getSubprogramGroupType());
                break;
              case SYSTEM:
                _switchResult_1 = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getSystemType());
                break;
              case THREAD:
                _switchResult_1 = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getThreadType());
                break;
              case THREAD_GROUP:
                _switchResult_1 = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getThreadGroupType());
                break;
              case VIRTUAL_BUS:
                _switchResult_1 = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getVirtualBusType());
                break;
              case VIRTUAL_PROCESSOR:
                _switchResult_1 = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getVirtualProcessorType());
                break;
              default:
                break;
            }
          }
          _xifexpression = ((ComponentType) _switchResult_1);
        }
        final ComponentClassifier classifier = _xifexpression;
        this.classifierCreationDIMPropertyAddition(classifier);
        EAttribute _namedElement_Name = this.aadl2Package.getNamedElement_Name();
        String _name = compinst.getName();
        String _plus = (_name + ".impl");
        this.manipulation.set(classifier, _namedElement_Name, _plus);
        if ((classifier instanceof ComponentType)) {
          this.manipulation.set(classifier, this.aadl2Package.getComponentType_Extended(), compinst.getClassifier());
        } else {
          this.manipulation.set(classifier, this.aadl2Package.getComponentImplementation_Extended(), compinst.getClassifier());
        }
        Boolean _isLibraryClassifier = LibraryUtils.isLibraryClassifier(compinst.getClassifier(), this.aadlPublicPackage);
        if ((_isLibraryClassifier).booleanValue()) {
          EObject _eContainer_1 = compinst.getClassifier().eContainer().eContainer();
          this.manipulation.add(this.aadlPublicPackage, this.aadl2Package.getPackageSection_ImportedUnit(), ((ModelUnit) _eContainer_1));
        }
        this.manipulation.set(compinst, this.instPackage.getComponentInstance_Classifier(), classifier);
      }
      if ((compinst != this.topSystemInst)) {
        EObject _eContainer_2 = compinst.eContainer();
        boolean _isAffectingInstanceObject = LibraryUtils.isAffectingInstanceObject(compinst, ((ComponentInstance) _eContainer_2), this.aadlPublicPackage, this.engine, this.preferences.isModifyReused(), false);
        if (_isAffectingInstanceObject) {
          EObject _eContainer_3 = compinst.eContainer();
          this.denyImplementnUpdatePropagn(compinst, ((ComponentInstance) _eContainer_3), false);
        }
      }
      ComponentClassifier classifier_1 = compinst.getClassifier();
      EObject modeadd = this.manipulation.createChild(classifier_1, this.aadl2Package.getComponentClassifier_OwnedMode(), this.aadl2Package.getMode());
      String _name_1 = modeinst.getName();
      boolean _tripleNotEquals = (_name_1 != null);
      if (_tripleNotEquals) {
        this.manipulation.set(modeadd, this.aadl2Package.getNamedElement_Name(), modeinst.getName());
      }
      boolean _isInitial = modeinst.isInitial();
      if (_isInitial) {
        this.manipulation.set(modeadd, this.aadl2Package.getMode_Initial(), Boolean.valueOf(true));
      } else {
        this.manipulation.set(modeadd, this.aadl2Package.getMode_Initial(), Boolean.valueOf(false));
      }
      boolean _isDerived = modeinst.isDerived();
      boolean _equals = (_isDerived == true);
      if (_equals) {
        EObject _eContainer_4 = modeadd.eContainer();
        this.manipulation.set(((ComponentClassifier) _eContainer_4), this.aadl2Package.getComponentClassifier_DerivedModes(), Boolean.valueOf(true));
        EObject modebind = this.manipulation.createChild(compinst.getSubcomponent(), this.aadl2Package.getSubcomponent_OwnedModeBinding(), this.aadl2Package.getModeBinding());
        this.manipulation.set(modebind, this.aadl2Package.getModeBinding_DerivedMode(), modeadd);
        boolean _isEmpty = modeinst.getParents().isEmpty();
        boolean _not = (!_isEmpty);
        if (_not) {
          this.manipulation.set(modebind, this.aadl2Package.getModeBinding_ParentMode(), modeinst.getParents().get(0).getMode());
        }
      }
      this.manipulation.set(modeinst, this.instPackage.getModeInstance_Mode(), modeadd);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected void modeChangedParentSubcomponentDefinition(final ModeInstance modeinst) {
    try {
      EObject _eContainer = modeinst.eContainer();
      ComponentInstance compinst = ((ComponentInstance) _eContainer);
      EList<? extends ComponentClassifier> extensionList = LibraryUtils.getAllInheritingParentClassifiers(modeinst.getMode(), compinst, false);
      Integer extensionIndex = LibraryUtils.computeExtensionIndex(extensionList, this.aadlPublicPackage, this.engine, Boolean.valueOf(this.preferences.isModifyReused()));
      if (((extensionIndex).intValue() != (-1))) {
        ComponentClassifier previousClass = null;
        List<? extends ComponentClassifier> _reverse = ListExtensions.reverse(extensionList);
        for (final ComponentClassifier currentcompclass : _reverse) {
          int _indexOf = extensionList.indexOf(currentcompclass);
          int _size = extensionList.size();
          int _minus = (_size - (extensionIndex).intValue());
          boolean _lessThan = (_indexOf < _minus);
          if (_lessThan) {
            ComponentClassifier compclasscopy = EcoreUtil.<ComponentClassifier>copy(currentcompclass);
            this.classifierCreationDIMPropertyAddition(compclasscopy);
            this.manipulation.moveTo(compclasscopy, this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier());
            EAttribute _namedElement_Name = this.aadl2Package.getNamedElement_Name();
            String _name = currentcompclass.getName();
            String _plus = (_name + "_dimcopy");
            this.manipulation.set(compclasscopy, _namedElement_Name, _plus);
            int _indexOf_1 = extensionList.indexOf(currentcompclass);
            boolean _equals = (_indexOf_1 == 0);
            if (_equals) {
              Mode newMode = LibraryUtils.getCopyMode(modeinst.getMode(), compclasscopy);
              this.manipulation.set(modeinst, this.instPackage.getModeInstance_Mode(), newMode);
            } else {
              if ((compclasscopy instanceof ComponentType)) {
                this.manipulation.set(compclasscopy, this.aadl2Package.getComponentType_Extended(), previousClass);
              } else {
                this.manipulation.set(compclasscopy, this.aadl2Package.getComponentImplementation_Extended(), previousClass);
              }
            }
            previousClass = compclasscopy;
          } else {
            if ((Objects.equal(compinst, this.topSystemInst) || (!LibraryUtils.isAffectingInstanceObject(compinst, ((ComponentInstance) compinst.eContainer()), this.aadlPublicPackage, this.engine, this.preferences.isModifyReused(), false)))) {
              int _indexOf_2 = extensionList.indexOf(currentcompclass);
              int _size_1 = extensionList.size();
              int _minus_1 = (_size_1 - (extensionIndex).intValue());
              boolean _equals_1 = (_indexOf_2 == _minus_1);
              if (_equals_1) {
                if ((currentcompclass instanceof ComponentType)) {
                  this.manipulation.set(currentcompclass, this.aadl2Package.getComponentType_Extended(), previousClass);
                } else {
                  this.manipulation.set(currentcompclass, this.aadl2Package.getComponentImplementation_Extended(), previousClass);
                }
              }
            } else {
              ComponentClassifier compclasscopy_1 = EcoreUtil.<ComponentClassifier>copy(currentcompclass);
              this.classifierCreationDIMPropertyAddition(compclasscopy_1);
              this.manipulation.moveTo(compclasscopy_1, this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier());
              EAttribute _namedElement_Name_1 = this.aadl2Package.getNamedElement_Name();
              String _name_1 = currentcompclass.getName();
              String _plus_1 = (_name_1 + "_dimcopy");
              this.manipulation.set(compclasscopy_1, _namedElement_Name_1, _plus_1);
              this.manipulation.set(compclasscopy_1, this.aadl2Package.getComponentImplementation_Extended(), previousClass);
              previousClass = compclasscopy_1;
              int _indexOf_3 = extensionList.indexOf(currentcompclass);
              int _size_2 = extensionList.size();
              int _minus_2 = (_size_2 - 1);
              boolean _tripleEquals = (_indexOf_3 == _minus_2);
              if (_tripleEquals) {
                this.manipulation.set(compinst, this.instPackage.getComponentInstance_Classifier(), compclasscopy_1);
                EObject _eContainer_1 = compinst.eContainer();
                this.denyImplementnUpdatePropagn(compinst, ((ComponentInstance) _eContainer_1), false);
              }
            }
          }
        }
      } else {
        if (((!Objects.equal(compinst, this.topSystemInst)) && LibraryUtils.isAffectingInstanceObject(compinst, ((ComponentInstance) compinst.eContainer()), this.aadlPublicPackage, this.engine, this.preferences.isModifyReused(), false))) {
          EObject _eContainer_2 = compinst.eContainer();
          this.denyImplementnUpdatePropagn(compinst, ((ComponentInstance) _eContainer_2), false);
        }
      }
      this.setSubcomponentType(compinst, compinst.getClassifier());
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected void modeTransChangedParentSubcomponentDefinition(final ModeTransitionInstance modetransinst) {
    try {
      EObject _eContainer = modetransinst.eContainer();
      ComponentInstance compinst = ((ComponentInstance) _eContainer);
      EList<? extends ComponentClassifier> extensionList = LibraryUtils.getAllInheritingParentClassifiers(modetransinst.getModeTransition(), compinst, false);
      Integer libraryIndex = LibraryUtils.getClosestLibraryClassifierIndex(extensionList, this.aadlPublicPackage);
      Integer reusedIndex = LibraryUtils.getClosestReusedClassifierIndex(extensionList, this.engine);
      Integer _xifexpression = null;
      boolean _isModifyReused = this.preferences.isModifyReused();
      if (_isModifyReused) {
        _xifexpression = libraryIndex;
      } else {
        Integer _xifexpression_1 = null;
        if (((libraryIndex).intValue() == (-1))) {
          _xifexpression_1 = reusedIndex;
        } else {
          Integer _xifexpression_2 = null;
          if (((reusedIndex).intValue() == (-1))) {
            _xifexpression_2 = libraryIndex;
          } else {
            _xifexpression_2 = Integer.valueOf(Math.min((reusedIndex).intValue(), (libraryIndex).intValue()));
          }
          _xifexpression_1 = _xifexpression_2;
        }
        _xifexpression = _xifexpression_1;
      }
      Integer extensionIndex = _xifexpression;
      if (((extensionIndex).intValue() != (-1))) {
        ComponentClassifier previousClass = null;
        List<? extends ComponentClassifier> _reverse = ListExtensions.reverse(extensionList);
        for (final ComponentClassifier currentcompclass : _reverse) {
          int _indexOf = extensionList.indexOf(currentcompclass);
          int _size = extensionList.size();
          int _minus = (_size - (extensionIndex).intValue());
          boolean _lessThan = (_indexOf < _minus);
          if (_lessThan) {
            ComponentClassifier compclasscopy = EcoreUtil.<ComponentClassifier>copy(currentcompclass);
            this.classifierCreationDIMPropertyAddition(compclasscopy);
            this.manipulation.add(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), compclasscopy);
            EAttribute _namedElement_Name = this.aadl2Package.getNamedElement_Name();
            String _name = currentcompclass.getName();
            String _plus = (_name + "_dimcopy");
            this.manipulation.set(compclasscopy, _namedElement_Name, _plus);
            int _indexOf_1 = extensionList.indexOf(currentcompclass);
            boolean _equals = (_indexOf_1 == 0);
            if (_equals) {
              ModeTransition newModeTrans = LibraryUtils.getCopyModeTransition(modetransinst.getModeTransition(), compclasscopy);
              this.manipulation.set(modetransinst, this.instPackage.getModeTransitionInstance_ModeTransition(), newModeTrans);
            } else {
              if ((compclasscopy instanceof ComponentType)) {
                this.manipulation.set(compclasscopy, this.aadl2Package.getComponentType_Extended(), previousClass);
              } else {
                this.manipulation.set(compclasscopy, this.aadl2Package.getComponentImplementation_Extended(), previousClass);
              }
            }
            previousClass = compclasscopy;
          } else {
            if ((Objects.equal(compinst, this.topSystemInst) || (!LibraryUtils.isAffectingInstanceObject(compinst, ((ComponentInstance) compinst.eContainer()), this.aadlPublicPackage, this.engine, this.preferences.isModifyReused(), false)))) {
              int _indexOf_2 = extensionList.indexOf(currentcompclass);
              int _size_1 = extensionList.size();
              int _minus_1 = (_size_1 - (extensionIndex).intValue());
              boolean _equals_1 = (_indexOf_2 == _minus_1);
              if (_equals_1) {
                if ((currentcompclass instanceof ComponentType)) {
                  this.manipulation.set(currentcompclass, this.aadl2Package.getComponentType_Extended(), previousClass);
                } else {
                  this.manipulation.set(currentcompclass, this.aadl2Package.getComponentImplementation_Extended(), previousClass);
                }
              }
            } else {
              ComponentClassifier compclasscopy_1 = EcoreUtil.<ComponentClassifier>copy(currentcompclass);
              this.classifierCreationDIMPropertyAddition(compclasscopy_1);
              this.manipulation.add(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), compclasscopy_1);
              EAttribute _namedElement_Name_1 = this.aadl2Package.getNamedElement_Name();
              String _name_1 = currentcompclass.getName();
              String _plus_1 = (_name_1 + "_dimcopy");
              this.manipulation.set(compclasscopy_1, _namedElement_Name_1, _plus_1);
              this.manipulation.set(compclasscopy_1, this.aadl2Package.getComponentImplementation_Extended(), previousClass);
              previousClass = compclasscopy_1;
              int _indexOf_3 = extensionList.indexOf(currentcompclass);
              int _size_2 = extensionList.size();
              int _minus_2 = (_size_2 - 1);
              boolean _tripleEquals = (_indexOf_3 == _minus_2);
              if (_tripleEquals) {
                this.manipulation.set(compinst, this.instPackage.getComponentInstance_Classifier(), compclasscopy_1);
                EObject _eContainer_1 = compinst.eContainer();
                this.denyImplementnUpdatePropagn(compinst, ((ComponentInstance) _eContainer_1), false);
              }
            }
          }
        }
      } else {
        if (((!Objects.equal(compinst, this.topSystemInst)) && LibraryUtils.isAffectingInstanceObject(compinst, ((ComponentInstance) compinst.eContainer()), this.aadlPublicPackage, this.engine, this.preferences.isModifyReused(), false))) {
          EObject _eContainer_2 = compinst.eContainer();
          this.denyImplementnUpdatePropagn(compinst, ((ComponentInstance) _eContainer_2), false);
        }
      }
      this.setSubcomponentType(compinst, compinst.getClassifier());
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected void modeTransitionInstanceCreatedDIM(final ModeTransitionInstance modetransinst) {
    try {
      EObject _eContainer = modetransinst.eContainer();
      ComponentInstance compinst = ((ComponentInstance) _eContainer);
      if (((LibraryUtils.isLibraryClassifier(compinst.getClassifier(), this.aadlPublicPackage)).booleanValue() || ((LibraryUtils.isReusedClassifier(compinst.getClassifier(), this.engine)).booleanValue() && (!this.preferences.isModifyReused())))) {
        ComponentClassifier _xifexpression = null;
        ComponentClassifier _classifier = compinst.getClassifier();
        if ((_classifier instanceof ComponentImplementation)) {
          EObject _switchResult = null;
          ComponentCategory _category = compinst.getCategory();
          if (_category != null) {
            switch (_category) {
              case ABSTRACT:
                _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getAbstractImplementation());
                break;
              case BUS:
                _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getBusImplementation());
                break;
              case DATA:
                _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getDataImplementation());
                break;
              case DEVICE:
                _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getDeviceImplementation());
                break;
              case MEMORY:
                _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getMemoryImplementation());
                break;
              case PROCESS:
                _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getProcessImplementation());
                break;
              case PROCESSOR:
                _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getProcessorImplementation());
                break;
              case SUBPROGRAM:
                _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getSubprogramImplementation());
                break;
              case SUBPROGRAM_GROUP:
                _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getSubprogramGroupImplementation());
                break;
              case SYSTEM:
                _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getSystemImplementation());
                break;
              case THREAD:
                _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getThreadImplementation());
                break;
              case THREAD_GROUP:
                _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getThreadGroupImplementation());
                break;
              case VIRTUAL_BUS:
                _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getVirtualBusImplementation());
                break;
              case VIRTUAL_PROCESSOR:
                _switchResult = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getVirtualProcessorImplementation());
                break;
              default:
                break;
            }
          }
          _xifexpression = ((ComponentImplementation) _switchResult);
        } else {
          EObject _switchResult_1 = null;
          ComponentCategory _category_1 = compinst.getCategory();
          if (_category_1 != null) {
            switch (_category_1) {
              case ABSTRACT:
                _switchResult_1 = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getAbstractType());
                break;
              case BUS:
                _switchResult_1 = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getBusType());
                break;
              case DATA:
                _switchResult_1 = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getDataType());
                break;
              case DEVICE:
                _switchResult_1 = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getDeviceType());
                break;
              case MEMORY:
                _switchResult_1 = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getMemoryType());
                break;
              case PROCESS:
                _switchResult_1 = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getProcessType());
                break;
              case PROCESSOR:
                _switchResult_1 = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getProcessorType());
                break;
              case SUBPROGRAM:
                _switchResult_1 = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getSubprogramType());
                break;
              case SUBPROGRAM_GROUP:
                _switchResult_1 = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getSubprogramGroupType());
                break;
              case SYSTEM:
                _switchResult_1 = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getSystemType());
                break;
              case THREAD:
                _switchResult_1 = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getThreadType());
                break;
              case THREAD_GROUP:
                _switchResult_1 = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getThreadGroupType());
                break;
              case VIRTUAL_BUS:
                _switchResult_1 = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getVirtualBusType());
                break;
              case VIRTUAL_PROCESSOR:
                _switchResult_1 = this.manipulation.createChild(this.aadlPublicPackage, this.aadl2Package.getPackageSection_OwnedClassifier(), this.aadl2Package.getVirtualProcessorType());
                break;
              default:
                break;
            }
          }
          _xifexpression = ((ComponentType) _switchResult_1);
        }
        final ComponentClassifier classifier = _xifexpression;
        this.classifierCreationDIMPropertyAddition(classifier);
        EAttribute _namedElement_Name = this.aadl2Package.getNamedElement_Name();
        String _name = compinst.getName();
        String _plus = (_name + ".impl");
        this.manipulation.set(classifier, _namedElement_Name, _plus);
        if ((classifier instanceof ComponentType)) {
          this.manipulation.set(classifier, this.aadl2Package.getComponentType_Extended(), compinst.getClassifier());
        } else {
          this.manipulation.set(classifier, this.aadl2Package.getComponentImplementation_Extended(), compinst.getClassifier());
        }
        Boolean _isLibraryClassifier = LibraryUtils.isLibraryClassifier(compinst.getClassifier(), this.aadlPublicPackage);
        if ((_isLibraryClassifier).booleanValue()) {
          EObject _eContainer_1 = compinst.getClassifier().eContainer().eContainer();
          this.manipulation.add(this.aadlPublicPackage, this.aadl2Package.getPackageSection_ImportedUnit(), ((ModelUnit) _eContainer_1));
        }
        this.manipulation.set(compinst, this.instPackage.getComponentInstance_Classifier(), classifier);
      }
      if ((compinst != this.topSystemInst)) {
        EObject _eContainer_2 = compinst.eContainer();
        boolean _isAffectingInstanceObject = LibraryUtils.isAffectingInstanceObject(compinst, ((ComponentInstance) _eContainer_2), this.aadlPublicPackage, this.engine, this.preferences.isModifyReused(), false);
        if (_isAffectingInstanceObject) {
          EObject _eContainer_3 = compinst.eContainer();
          this.denyImplementnUpdatePropagn(compinst, ((ComponentInstance) _eContainer_3), false);
        }
      }
      EObject modetransadd = this.manipulation.createChild(compinst.getClassifier(), this.aadl2Package.getComponentClassifier_OwnedModeTransition(), this.aadl2Package.getModeTransition());
      String _name_1 = modetransinst.getName();
      boolean _tripleNotEquals = (_name_1 != null);
      if (_tripleNotEquals) {
        this.manipulation.set(modetransadd, this.aadl2Package.getNamedElement_Name(), modetransinst.getName());
      }
      ModeInstance _source = modetransinst.getSource();
      boolean _tripleNotEquals_1 = (_source != null);
      if (_tripleNotEquals_1) {
        ModeInstance _source_1 = modetransinst.getSource();
        this.manipulation.set(modetransadd, this.aadl2Package.getModeTransition_Source(), ((ModeInstance) _source_1).getMode());
      }
      ModeInstance _destination = modetransinst.getDestination();
      boolean _tripleNotEquals_2 = (_destination != null);
      if (_tripleNotEquals_2) {
        ModeInstance _destination_1 = modetransinst.getDestination();
        this.manipulation.set(modetransadd, this.aadl2Package.getModeTransition_Destination(), ((ModeInstance) _destination_1).getMode());
      }
      EList<FeatureInstance> _triggers = modetransinst.getTriggers();
      for (final FeatureInstance trigger : _triggers) {
        {
          EObject modetrigadd = this.manipulation.createChild(modetransadd, this.aadl2Package.getModeTransition_OwnedTrigger(), this.aadl2Package.getModeTransitionTrigger());
          this.manipulation.set(modetrigadd, this.aadl2Package.getModeTransitionTrigger_TriggerPort(), trigger.getFeature());
        }
      }
      this.manipulation.set(modetransinst, this.instPackage.getModeTransitionInstance_ModeTransition(), modetransadd);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected void propertyInstanceCreatedDIM(final PropertyAssociationInstance propinst, final PublicPackageSection aadlPackage) {
    try {
      Property _property = propinst.getProperty();
      boolean _tripleNotEquals = (_property != null);
      if (_tripleNotEquals) {
        boolean _contains = aadlPackage.getImportedUnits().contains(propinst.getProperty().eContainer());
        boolean _not = (!_contains);
        if (_not) {
          boolean _isNoAnnexes = aadlPackage.isNoAnnexes();
          boolean _tripleEquals = (Boolean.valueOf(_isNoAnnexes) == Boolean.valueOf(true));
          if (_tripleEquals) {
            this.manipulation.set(aadlPackage, this.aadl2Package.getPackageSection_NoAnnexes(), Boolean.valueOf(false));
          }
          EObject _eContainer = propinst.getProperty().eContainer();
          boolean _isPredeclaredPropertySet = AadlUtil.isPredeclaredPropertySet(((PropertySet) _eContainer).getName());
          boolean _not_1 = (!_isPredeclaredPropertySet);
          if (_not_1) {
            this.manipulation.add(aadlPackage, this.aadl2Package.getPackageSection_ImportedUnit(), propinst.getProperty().eContainer());
          }
        }
      }
      EObject _xifexpression = null;
      Property _property_1 = propinst.getProperty();
      boolean _tripleNotEquals_1 = (_property_1 != null);
      if (_tripleNotEquals_1) {
        EObject _xifexpression_1 = null;
        Boolean _isValueProperty = PropertyTypeUtils.isValueProperty(propinst.getProperty().getPropertyType());
        if ((_isValueProperty).booleanValue()) {
          _xifexpression_1 = propinst.eContainer();
        } else {
          _xifexpression_1 = InstanceHierarchyUtils.getRefPropertyLowestCommonParentInstance(propinst);
        }
        _xifexpression = _xifexpression_1;
      } else {
        _xifexpression = propinst.eContainer();
      }
      EObject objinst = _xifexpression;
      EObject _xifexpression_2 = null;
      if (((objinst instanceof SystemInstance) && (((ComponentInstance) objinst).getSubcomponent() == null))) {
        _xifexpression_2 = this.manipulation.createChild(((SystemInstance) objinst).getComponentImplementation(), this.aadl2Package.getNamedElement_OwnedPropertyAssociation(), this.aadl2Package.getPropertyAssociation());
      } else {
        EObject _xifexpression_3 = null;
        if ((objinst instanceof ComponentInstance)) {
          EObject _xifexpression_4 = null;
          Subcomponent _subcomponent = ((ComponentInstance)objinst).getSubcomponent();
          boolean _tripleNotEquals_2 = (_subcomponent != null);
          if (_tripleNotEquals_2) {
            _xifexpression_4 = this.manipulation.createChild(((ComponentInstance)objinst).getSubcomponent(), this.aadl2Package.getNamedElement_OwnedPropertyAssociation(), this.aadl2Package.getPropertyAssociation());
          } else {
            EObject _xifexpression_5 = null;
            ComponentClassifier _classifier = ((ComponentInstance)objinst).getClassifier();
            boolean _tripleNotEquals_3 = (_classifier != null);
            if (_tripleNotEquals_3) {
              _xifexpression_5 = this.manipulation.createChild(((ComponentInstance)objinst).getClassifier(), this.aadl2Package.getNamedElement_OwnedPropertyAssociation(), this.aadl2Package.getPropertyAssociation());
            }
            _xifexpression_4 = _xifexpression_5;
          }
          _xifexpression_3 = _xifexpression_4;
        } else {
          EObject _xifexpression_6 = null;
          if ((objinst instanceof FeatureInstance)) {
            _xifexpression_6 = this.manipulation.createChild(((FeatureInstance)objinst).getFeature(), this.aadl2Package.getNamedElement_OwnedPropertyAssociation(), this.aadl2Package.getPropertyAssociation());
          } else {
            EObject _xifexpression_7 = null;
            if ((objinst instanceof ConnectionInstance)) {
              EObject _xifexpression_8 = null;
              int _size = ((ConnectionInstance)objinst).getConnectionReferences().size();
              boolean _equals = (_size == 1);
              if (_equals) {
                _xifexpression_8 = this.manipulation.createChild(((ConnectionInstance)objinst).getConnectionReferences().get(0).getConnection(), this.aadl2Package.getNamedElement_OwnedPropertyAssociation(), this.aadl2Package.getPropertyAssociation());
              } else {
                _xifexpression_8 = null;
              }
              _xifexpression_7 = _xifexpression_8;
            } else {
              EObject _xifexpression_9 = null;
              if ((objinst instanceof ModeInstance)) {
                _xifexpression_9 = this.manipulation.createChild(((ModeInstance)objinst).getMode(), this.aadl2Package.getNamedElement_OwnedPropertyAssociation(), this.aadl2Package.getPropertyAssociation());
              } else {
                EObject _xifexpression_10 = null;
                if ((objinst instanceof ModeTransitionInstance)) {
                  _xifexpression_10 = this.manipulation.createChild(((ModeTransitionInstance)objinst).getModeTransition(), this.aadl2Package.getNamedElement_OwnedPropertyAssociation(), this.aadl2Package.getPropertyAssociation());
                }
                _xifexpression_9 = _xifexpression_10;
              }
              _xifexpression_7 = _xifexpression_9;
            }
            _xifexpression_6 = _xifexpression_7;
          }
          _xifexpression_3 = _xifexpression_6;
        }
        _xifexpression_2 = _xifexpression_3;
      }
      EObject propadd = _xifexpression_2;
      if ((propadd == null)) {
        String _name = propinst.getProperty().getName();
        String _plus = ("DIM: Property instance " + _name);
        String _plus_1 = (_plus + " not attached to connection due to multiple connection references");
        this.LOGGER.logWarning(_plus_1);
      } else {
        this.manipulation.set(((PropertyAssociation) propadd), this.aadl2Package.getPropertyAssociation_Property(), propinst.getProperty());
        EObject _eContainer_1 = propinst.eContainer();
        boolean _notEquals = (!Objects.equal(objinst, _eContainer_1));
        if (_notEquals) {
          EObject contelem = this.manipulation.createChild(((PropertyAssociation) propadd), this.aadl2Package.getPropertyAssociation_AppliesTo(), this.aadl2Package.getContainedNamedElement());
          EObject _eContainer_2 = propinst.eContainer();
          this.appliesToPathDefinition(((ContainedNamedElement) contelem), ((InstanceObject) objinst), ((InstanceObject) _eContainer_2));
        }
        this.manipulation.set(propinst, this.instPackage.getPropertyAssociationInstance_PropertyAssociation(), propadd);
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected ModalPropertyValue modalPropertyCreatedDIM(final ModalPropertyValue modpropinst) {
    try {
      EObject _eContainer = modpropinst.eContainer();
      EObject modpropadd = this.manipulation.createChild(((PropertyAssociationInstance) _eContainer).getPropertyAssociation(), this.aadl2Package.getPropertyAssociation_OwnedValue(), this.aadl2Package.getModalPropertyValue());
      boolean _isEmpty = modpropinst.getInModes().isEmpty();
      boolean _not = (!_isEmpty);
      if (_not) {
        EList<Mode> _inModes = modpropinst.getInModes();
        for (final Mode modpropmode : _inModes) {
          EObject _eContainer_1 = modpropinst.eContainer().eContainer();
          this.manipulation.add(modpropadd, this.aadl2Package.getModalElement_InMode(), ModeUtils.findObjectModeInstInSOM(((InstanceObject) _eContainer_1), ((SystemOperationMode) modpropmode)).getMode());
        }
      }
      return ((ModalPropertyValue) modpropadd);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected void appliesToPathDefinition(final ContainedNamedElement refvaladd, final InstanceObject objinst, final InstanceObject refinstobj) {
    try {
      EObject refpathadd = this.manipulation.createChild(refvaladd, this.aadl2Package.getContainedNamedElement_Path(), this.aadl2Package.getContainmentPathElement());
      EList<InstanceObject> containmentPath = InstanceHierarchyUtils.getContainmentPath(((InstanceObject) objinst), refinstobj);
      this.manipulation.set(refpathadd, this.aadl2Package.getContainmentPathElement_NamedElement(), TraceUtils.getDeclarativeElement(containmentPath.get(0)));
      for (final InstanceObject refpathinstobj : containmentPath) {
        int _indexOf = InstanceHierarchyUtils.getContainmentPath(((InstanceObject) objinst), refinstobj).indexOf(refpathinstobj);
        boolean _notEquals = (_indexOf != 0);
        if (_notEquals) {
          refpathadd = this.manipulation.createChild(refpathadd, this.aadl2Package.getContainmentPathElement_Path(), this.aadl2Package.getContainmentPathElement());
          this.manipulation.set(refpathadd, this.aadl2Package.getContainmentPathElement_NamedElement(), TraceUtils.getDeclarativeElement(refpathinstobj));
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected void rangeValueDefinition(final RangeValue rangeval, final RangeValue rangevaladd) {
    try {
      PropertyExpression _maximum = rangeval.getMaximum();
      if ((_maximum instanceof IntegerLiteral)) {
        EObject maxvaladd = this.manipulation.createChild(rangevaladd, this.aadl2Package.getRangeValue_Maximum(), this.aadl2Package.getIntegerLiteral());
        PropertyExpression _maximum_1 = rangeval.getMaximum();
        this.manipulation.set(maxvaladd, this.aadl2Package.getIntegerLiteral_Value(), Long.valueOf(((IntegerLiteral) _maximum_1).getValue()));
        PropertyExpression _maximum_2 = rangeval.getMaximum();
        this.manipulation.set(maxvaladd, this.aadl2Package.getIntegerLiteral_Base(), Long.valueOf(((IntegerLiteral) _maximum_2).getBase()));
        PropertyExpression _maximum_3 = rangeval.getMaximum();
        this.manipulation.set(maxvaladd, this.aadl2Package.getNumberValue_Unit(), ((IntegerLiteral) _maximum_3).getUnit());
        EObject minvaladd = this.manipulation.createChild(rangevaladd, this.aadl2Package.getRangeValue_Minimum(), this.aadl2Package.getIntegerLiteral());
        PropertyExpression _minimum = rangeval.getMinimum();
        this.manipulation.set(minvaladd, this.aadl2Package.getIntegerLiteral_Value(), Long.valueOf(((IntegerLiteral) _minimum).getValue()));
        PropertyExpression _minimum_1 = rangeval.getMinimum();
        this.manipulation.set(minvaladd, this.aadl2Package.getIntegerLiteral_Base(), Long.valueOf(((IntegerLiteral) _minimum_1).getBase()));
        PropertyExpression _minimum_2 = rangeval.getMinimum();
        this.manipulation.set(minvaladd, this.aadl2Package.getNumberValue_Unit(), ((IntegerLiteral) _minimum_2).getUnit());
      } else {
        EObject maxvaladd_1 = this.manipulation.createChild(rangevaladd, this.aadl2Package.getRangeValue_Maximum(), this.aadl2Package.getRealLiteral());
        PropertyExpression _maximum_4 = rangeval.getMaximum();
        this.manipulation.set(maxvaladd_1, this.aadl2Package.getRealLiteral_Value(), Double.valueOf(((RealLiteral) _maximum_4).getValue()));
        PropertyExpression _maximum_5 = rangeval.getMaximum();
        this.manipulation.set(maxvaladd_1, this.aadl2Package.getNumberValue_Unit(), ((RealLiteral) _maximum_5).getUnit());
        EObject minvaladd_1 = this.manipulation.createChild(rangevaladd, this.aadl2Package.getRangeValue_Minimum(), this.aadl2Package.getRealLiteral());
        PropertyExpression _minimum_3 = rangeval.getMinimum();
        this.manipulation.set(minvaladd_1, this.aadl2Package.getRealLiteral_Value(), Double.valueOf(((RealLiteral) _minimum_3).getValue()));
        PropertyExpression _minimum_4 = rangeval.getMinimum();
        this.manipulation.set(minvaladd_1, this.aadl2Package.getNumberValue_Unit(), ((RealLiteral) _minimum_4).getUnit());
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected void simplePropertyValueCreation(final PropertyType proptype, final PropertyValue propvalinst, final Element propvalcontainer) {
    try {
      Boolean _isValueProperty = PropertyTypeUtils.isValueProperty(proptype);
      if ((_isValueProperty).booleanValue()) {
        this.valuePropertyValueCreation(propvalinst, propvalcontainer);
      } else {
        final PropertyAssociationInstance propassocinst = PropertyUtils.getContainingPropertyAssociationInstance(propvalinst);
        EObject refvaladd = this.manipulation.createChild(propvalcontainer, this.propertyValueContainmentReference(propvalcontainer), this.aadl2Package.getReferenceValue());
        InstanceObject _referencedInstanceObject = ((InstanceReferenceValue) propvalinst).getReferencedInstanceObject();
        boolean _tripleNotEquals = (_referencedInstanceObject != null);
        if (_tripleNotEquals) {
          boolean _contains = InstanceHierarchyUtils.getAllParentInstanceObject(((InstanceReferenceValue) propvalinst).getReferencedInstanceObject()).contains(propassocinst.eContainer());
          if (_contains) {
            EObject _eContainer = propassocinst.eContainer();
            this.appliesToPathDefinition(((ContainedNamedElement) refvaladd), ((InstanceObject) _eContainer), ((InstanceReferenceValue) propvalinst).getReferencedInstanceObject());
          } else {
            PropertyAssociation _propertyAssociation = propassocinst.getPropertyAssociation();
            PropertyAssociation propassoc = ((PropertyAssociation) _propertyAssociation);
            EObject _eContainer_1 = propassocinst.eContainer();
            InstanceObject objinst = InstanceHierarchyUtils.getLowestCommonParentInstanceObject(((InstanceReferenceValue) propvalinst).getReferencedInstanceObject(), ((InstanceObject) _eContainer_1));
            this.manipulation.moveTo(propassoc, TraceUtils.getDeclarativeElement(objinst), this.aadl2Package.getNamedElement_OwnedPropertyAssociation());
            propassoc.getAppliesTos().clear();
            EObject contelem = this.manipulation.createChild(((PropertyAssociation) propassoc), this.aadl2Package.getPropertyAssociation_AppliesTo(), this.aadl2Package.getContainedNamedElement());
            EObject _eContainer_2 = propassocinst.eContainer();
            this.appliesToPathDefinition(((ContainedNamedElement) contelem), objinst, ((InstanceObject) _eContainer_2));
            this.appliesToPathDefinition(((ContainedNamedElement) refvaladd), objinst, ((InstanceReferenceValue) propvalinst).getReferencedInstanceObject());
            String _name = propassoc.getProperty().getName();
            String _plus = ("DIM: Property association of property " + _name);
            String _plus_1 = (_plus + " moved due to change in ReferenceValue LCPCI position");
            this.LOGGER.logInfo(_plus_1);
          }
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  private EReference propertyValueContainmentReference(final Element propvalcontainer) {
    EReference _xifexpression = null;
    if ((propvalcontainer instanceof BasicPropertyAssociation)) {
      _xifexpression = this.aadl2Package.getBasicPropertyAssociation_OwnedValue();
    } else {
      EReference _xifexpression_1 = null;
      if ((propvalcontainer instanceof ModalPropertyValue)) {
        _xifexpression_1 = this.aadl2Package.getModalPropertyValue_OwnedValue();
      } else {
        EReference _xifexpression_2 = null;
        if ((propvalcontainer instanceof ListValue)) {
          _xifexpression_2 = this.aadl2Package.getListValue_OwnedListElement();
        } else {
          _xifexpression_2 = null;
        }
        _xifexpression_1 = _xifexpression_2;
      }
      _xifexpression = _xifexpression_1;
    }
    return _xifexpression;
  }

  private void valuePropertyValueCreation(final PropertyValue propval, final Element propvalcontainer) {
    try {
      EReference reference = this.propertyValueContainmentReference(propvalcontainer);
      if ((propval instanceof StringLiteral)) {
        EObject propvaladd = this.manipulation.createChild(propvalcontainer, reference, this.aadl2Package.getStringLiteral());
        this.manipulation.set(propvaladd, this.aadl2Package.getStringLiteral_Value(), ((StringLiteral) propval).getValue());
      } else {
        if ((propval instanceof IntegerLiteral)) {
          EObject propvaladd_1 = this.manipulation.createChild(propvalcontainer, reference, this.aadl2Package.getIntegerLiteral());
          this.manipulation.set(propvaladd_1, this.aadl2Package.getIntegerLiteral_Value(), Long.valueOf(((IntegerLiteral) propval).getValue()));
          this.manipulation.set(propvaladd_1, this.aadl2Package.getIntegerLiteral_Base(), Long.valueOf(((IntegerLiteral) propval).getBase()));
          this.manipulation.set(propvaladd_1, this.aadl2Package.getNumberValue_Unit(), ((IntegerLiteral) propval).getUnit());
        } else {
          if ((propval instanceof RealLiteral)) {
            EObject propvaladd_2 = this.manipulation.createChild(propvalcontainer, reference, this.aadl2Package.getRealLiteral());
            this.manipulation.set(propvaladd_2, this.aadl2Package.getRealLiteral_Value(), Double.valueOf(((RealLiteral) propval).getValue()));
            this.manipulation.set(propvaladd_2, this.aadl2Package.getNumberValue_Unit(), ((RealLiteral) propval).getUnit());
          } else {
            if ((propval instanceof ClassifierValue)) {
              EObject propvaladd_3 = this.manipulation.createChild(propvalcontainer, reference, this.aadl2Package.getClassifierValue());
              if (((((ClassifierValue) propval).getClassifier() != null) && (LibraryUtils.isLibraryClassifier(((ComponentClassifier) ((ClassifierValue) propval).getClassifier()), this.aadlPublicPackage)).booleanValue())) {
                EObject _eContainer = ((ClassifierValue) propval).getClassifier().eContainer().eContainer();
                this.manipulation.add(this.aadlPublicPackage, this.aadl2Package.getPackageSection_ImportedUnit(), ((ModelUnit) _eContainer));
              }
              this.manipulation.set(propvaladd_3, this.aadl2Package.getClassifierValue_Classifier(), ((ClassifierValue) propval).getClassifier());
            } else {
              if ((propval instanceof BooleanLiteral)) {
                EObject propvaladd_4 = this.manipulation.createChild(propvalcontainer, reference, this.aadl2Package.getBooleanLiteral());
                this.manipulation.set(propvaladd_4, this.aadl2Package.getBooleanLiteral_Value(), Boolean.valueOf(((BooleanLiteral) propval).getValue()));
              } else {
                if ((propval instanceof RangeValue)) {
                  EObject propvaladd_5 = this.manipulation.createChild(propvalcontainer, reference, this.aadl2Package.getRangeValue());
                  this.manipulation.set(propvaladd_5, this.aadl2Package.getRangeValue_Delta(), ((RangeValue) propval).getDelta());
                  this.rangeValueDefinition(((RangeValue)propval), ((RangeValue) propvaladd_5));
                } else {
                  if ((propval instanceof ComputedValue)) {
                    EObject propvaladd_6 = this.manipulation.createChild(propvalcontainer, reference, this.aadl2Package.getComputedValue());
                    this.manipulation.set(propvaladd_6, this.aadl2Package.getComputedValue_Function(), ((ComputedValue) propval).getFunction());
                  } else {
                    if ((propval instanceof NamedValue)) {
                      EObject propvaladd_7 = this.manipulation.createChild(propvalcontainer, reference, this.aadl2Package.getNamedValue());
                      this.manipulation.set(propvaladd_7, this.aadl2Package.getNamedValue_NamedValue(), ((NamedValue) propval).getNamedValue());
                    }
                  }
                }
              }
            }
          }
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
