package fr.mem4csd.osatedim.viatra.transformations;

import com.google.common.base.Objects;
import fr.mem4csd.osatedim.utils.FeatureUtils;
import fr.mem4csd.osatedim.utils.InstanceHierarchyUtils;
import fr.mem4csd.osatedim.utils.LibraryUtils;
import fr.mem4csd.osatedim.utils.PropertyTypeUtils;
import fr.mem4csd.osatedim.utils.PropertyUtils;
import fr.mem4csd.osatedim.utils.TraceUtils;
import java.util.List;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.osate.aadl2.AbstractImplementation;
import org.osate.aadl2.AbstractType;
import org.osate.aadl2.BasicPropertyAssociation;
import org.osate.aadl2.BooleanLiteral;
import org.osate.aadl2.BusImplementation;
import org.osate.aadl2.BusType;
import org.osate.aadl2.ClassifierValue;
import org.osate.aadl2.ComponentCategory;
import org.osate.aadl2.ComponentClassifier;
import org.osate.aadl2.ComponentImplementation;
import org.osate.aadl2.ComponentType;
import org.osate.aadl2.ComputedValue;
import org.osate.aadl2.Connection;
import org.osate.aadl2.ContainedNamedElement;
import org.osate.aadl2.ContainmentPathElement;
import org.osate.aadl2.DataImplementation;
import org.osate.aadl2.DataType;
import org.osate.aadl2.DeviceImplementation;
import org.osate.aadl2.DeviceType;
import org.osate.aadl2.Feature;
import org.osate.aadl2.IntegerLiteral;
import org.osate.aadl2.ListValue;
import org.osate.aadl2.MemoryImplementation;
import org.osate.aadl2.MemoryType;
import org.osate.aadl2.ModalPropertyValue;
import org.osate.aadl2.ModelUnit;
import org.osate.aadl2.NamedValue;
import org.osate.aadl2.ProcessImplementation;
import org.osate.aadl2.ProcessType;
import org.osate.aadl2.ProcessorImplementation;
import org.osate.aadl2.ProcessorType;
import org.osate.aadl2.PropertyAssociation;
import org.osate.aadl2.PropertyExpression;
import org.osate.aadl2.PropertyType;
import org.osate.aadl2.PropertyValue;
import org.osate.aadl2.PublicPackageSection;
import org.osate.aadl2.RangeValue;
import org.osate.aadl2.RealLiteral;
import org.osate.aadl2.RecordType;
import org.osate.aadl2.RecordValue;
import org.osate.aadl2.StringLiteral;
import org.osate.aadl2.Subcomponent;
import org.osate.aadl2.SubprogramGroupImplementation;
import org.osate.aadl2.SubprogramGroupType;
import org.osate.aadl2.SubprogramImplementation;
import org.osate.aadl2.SubprogramType;
import org.osate.aadl2.SystemImplementation;
import org.osate.aadl2.SystemType;
import org.osate.aadl2.ThreadGroupImplementation;
import org.osate.aadl2.ThreadGroupType;
import org.osate.aadl2.ThreadImplementation;
import org.osate.aadl2.ThreadType;
import org.osate.aadl2.VirtualBusImplementation;
import org.osate.aadl2.VirtualBusType;
import org.osate.aadl2.VirtualProcessorImplementation;
import org.osate.aadl2.VirtualProcessorType;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.ConnectionInstance;
import org.osate.aadl2.instance.ConnectionKind;
import org.osate.aadl2.instance.ConnectionReference;
import org.osate.aadl2.instance.FeatureCategory;
import org.osate.aadl2.instance.FeatureInstance;
import org.osate.aadl2.instance.InstanceObject;
import org.osate.aadl2.instance.InstanceReferenceValue;
import org.osate.aadl2.instance.PropertyAssociationInstance;
import org.osate.aadl2.instance.SystemInstance;

@SuppressWarnings("all")
public class DIMTransformationRulesDelta extends DIMTransformationRules {
  protected Object componentInstanceDeletedDIM(final Subcomponent subcomp) {
    try {
      Object _xblockexpression = null;
      {
        EObject parentcompimp = subcomp.eContainer();
        Object _xifexpression = null;
        if ((parentcompimp instanceof AbstractImplementation)) {
          ComponentCategory _category = subcomp.getCategory();
          if (_category != null) {
            switch (_category) {
              case ABSTRACT:
                this.manipulation.remove(parentcompimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), subcomp);
                break;
              case BUS:
                this.manipulation.remove(parentcompimp, this.aadl2Package.getAbstractImplementation_OwnedBusSubcomponent(), subcomp);
                break;
              case DATA:
                this.manipulation.remove(parentcompimp, this.aadl2Package.getAbstractImplementation_OwnedDataSubcomponent(), subcomp);
                break;
              case DEVICE:
                this.manipulation.remove(parentcompimp, this.aadl2Package.getAbstractImplementation_OwnedDeviceSubcomponent(), subcomp);
                break;
              case MEMORY:
                this.manipulation.remove(parentcompimp, this.aadl2Package.getAbstractImplementation_OwnedMemorySubcomponent(), subcomp);
                break;
              case PROCESS:
                this.manipulation.remove(parentcompimp, this.aadl2Package.getAbstractImplementation_OwnedProcessSubcomponent(), subcomp);
                break;
              case PROCESSOR:
                this.manipulation.remove(parentcompimp, this.aadl2Package.getAbstractImplementation_OwnedProcessorSubcomponent(), subcomp);
                break;
              case SUBPROGRAM:
                this.manipulation.remove(parentcompimp, this.aadl2Package.getAbstractImplementation_OwnedSubprogramSubcomponent(), subcomp);
                break;
              case SUBPROGRAM_GROUP:
                this.manipulation.remove(parentcompimp, this.aadl2Package.getAbstractImplementation_OwnedSubprogramGroupSubcomponent(), subcomp);
                break;
              case THREAD:
                this.manipulation.remove(parentcompimp, this.aadl2Package.getAbstractImplementation_OwnedThreadSubcomponent(), subcomp);
                break;
              case THREAD_GROUP:
                this.manipulation.remove(parentcompimp, this.aadl2Package.getAbstractImplementation_OwnedThreadGroupSubcomponent(), subcomp);
                break;
              case SYSTEM:
                this.manipulation.remove(parentcompimp, this.aadl2Package.getAbstractImplementation_OwnedSystemSubcomponent(), subcomp);
                break;
              case VIRTUAL_BUS:
                this.manipulation.remove(parentcompimp, this.aadl2Package.getAbstractImplementation_OwnedVirtualBusSubcomponent(), subcomp);
                break;
              case VIRTUAL_PROCESSOR:
                this.manipulation.remove(parentcompimp, this.aadl2Package.getAbstractImplementation_OwnedVirtualProcessorSubcomponent(), subcomp);
                break;
              default:
                break;
            }
          }
        } else {
          Object _xifexpression_1 = null;
          if ((parentcompimp instanceof BusImplementation)) {
            Object _switchResult_1 = null;
            ComponentCategory _category_1 = subcomp.getCategory();
            if (_category_1 != null) {
              switch (_category_1) {
                case ABSTRACT:
                  this.manipulation.remove(parentcompimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), subcomp);
                  break;
                case VIRTUAL_BUS:
                  this.manipulation.remove(parentcompimp, this.aadl2Package.getBusImplementation_OwnedVirtualBusSubcomponent(), subcomp);
                  break;
                default:
                  _switchResult_1 = null;
                  break;
              }
            } else {
              _switchResult_1 = null;
            }
            _xifexpression_1 = _switchResult_1;
          } else {
            Object _xifexpression_2 = null;
            if ((parentcompimp instanceof DataImplementation)) {
              Object _switchResult_2 = null;
              ComponentCategory _category_2 = subcomp.getCategory();
              if (_category_2 != null) {
                switch (_category_2) {
                  case ABSTRACT:
                    this.manipulation.remove(parentcompimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), subcomp);
                    break;
                  case DATA:
                    this.manipulation.remove(parentcompimp, this.aadl2Package.getDataImplementation_OwnedDataSubcomponent(), subcomp);
                    break;
                  case SUBPROGRAM:
                    this.manipulation.remove(parentcompimp, this.aadl2Package.getDataImplementation_OwnedSubprogramSubcomponent(), subcomp);
                    break;
                  default:
                    _switchResult_2 = null;
                    break;
                }
              } else {
                _switchResult_2 = null;
              }
              _xifexpression_2 = _switchResult_2;
            } else {
              Object _xifexpression_3 = null;
              if ((parentcompimp instanceof DeviceImplementation)) {
                Object _switchResult_3 = null;
                ComponentCategory _category_3 = subcomp.getCategory();
                if (_category_3 != null) {
                  switch (_category_3) {
                    case ABSTRACT:
                      this.manipulation.remove(parentcompimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), subcomp);
                      break;
                    case BUS:
                      this.manipulation.remove(parentcompimp, this.aadl2Package.getDeviceImplementation_OwnedBusSubcomponent(), subcomp);
                      break;
                    case DATA:
                      this.manipulation.remove(parentcompimp, this.aadl2Package.getDeviceImplementation_OwnedDataSubcomponent(), subcomp);
                      break;
                    case VIRTUAL_BUS:
                      this.manipulation.remove(parentcompimp, this.aadl2Package.getDeviceImplementation_OwnedVirtualBusSubcomponent(), subcomp);
                      break;
                    default:
                      _switchResult_3 = null;
                      break;
                  }
                } else {
                  _switchResult_3 = null;
                }
                _xifexpression_3 = _switchResult_3;
              } else {
                Object _xifexpression_4 = null;
                if ((parentcompimp instanceof MemoryImplementation)) {
                  Object _switchResult_4 = null;
                  ComponentCategory _category_4 = subcomp.getCategory();
                  if (_category_4 != null) {
                    switch (_category_4) {
                      case ABSTRACT:
                        this.manipulation.remove(parentcompimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), subcomp);
                        break;
                      case BUS:
                        this.manipulation.remove(parentcompimp, this.aadl2Package.getMemoryImplementation_OwnedBusSubcomponent(), subcomp);
                        break;
                      case MEMORY:
                        this.manipulation.remove(parentcompimp, this.aadl2Package.getMemoryImplementation_OwnedMemorySubcomponent(), subcomp);
                        break;
                      default:
                        _switchResult_4 = null;
                        break;
                    }
                  } else {
                    _switchResult_4 = null;
                  }
                  _xifexpression_4 = _switchResult_4;
                } else {
                  Object _xifexpression_5 = null;
                  if ((parentcompimp instanceof ProcessImplementation)) {
                    Object _switchResult_5 = null;
                    ComponentCategory _category_5 = subcomp.getCategory();
                    if (_category_5 != null) {
                      switch (_category_5) {
                        case ABSTRACT:
                          this.manipulation.remove(parentcompimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), subcomp);
                          break;
                        case DATA:
                          this.manipulation.remove(parentcompimp, this.aadl2Package.getProcessImplementation_OwnedDataSubcomponent(), subcomp);
                          break;
                        case SUBPROGRAM:
                          this.manipulation.remove(parentcompimp, this.aadl2Package.getProcessImplementation_OwnedSubprogramSubcomponent(), subcomp);
                          break;
                        case SUBPROGRAM_GROUP:
                          this.manipulation.remove(parentcompimp, this.aadl2Package.getProcessImplementation_OwnedSubprogramGroupSubcomponent(), subcomp);
                          break;
                        case THREAD:
                          this.manipulation.remove(parentcompimp, this.aadl2Package.getProcessImplementation_OwnedThreadSubcomponent(), subcomp);
                          break;
                        case THREAD_GROUP:
                          this.manipulation.remove(parentcompimp, this.aadl2Package.getProcessImplementation_OwnedThreadGroupSubcomponent(), subcomp);
                          break;
                        default:
                          _switchResult_5 = null;
                          break;
                      }
                    } else {
                      _switchResult_5 = null;
                    }
                    _xifexpression_5 = _switchResult_5;
                  } else {
                    Object _xifexpression_6 = null;
                    if ((parentcompimp instanceof ProcessorImplementation)) {
                      Object _switchResult_6 = null;
                      ComponentCategory _category_6 = subcomp.getCategory();
                      if (_category_6 != null) {
                        switch (_category_6) {
                          case ABSTRACT:
                            this.manipulation.remove(parentcompimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), subcomp);
                            break;
                          case BUS:
                            this.manipulation.remove(parentcompimp, this.aadl2Package.getProcessorImplementation_OwnedBusSubcomponent(), subcomp);
                            break;
                          case MEMORY:
                            this.manipulation.remove(parentcompimp, this.aadl2Package.getProcessorImplementation_OwnedMemorySubcomponent(), subcomp);
                            break;
                          case VIRTUAL_BUS:
                            this.manipulation.remove(parentcompimp, this.aadl2Package.getProcessorImplementation_OwnedVirtualBusSubcomponent(), subcomp);
                            break;
                          case VIRTUAL_PROCESSOR:
                            this.manipulation.remove(parentcompimp, this.aadl2Package.getProcessorImplementation_OwnedVirtualProcessorSubcomponent(), subcomp);
                            break;
                          default:
                            _switchResult_6 = null;
                            break;
                        }
                      } else {
                        _switchResult_6 = null;
                      }
                      _xifexpression_6 = _switchResult_6;
                    } else {
                      Object _xifexpression_7 = null;
                      if ((parentcompimp instanceof SubprogramImplementation)) {
                        Object _switchResult_7 = null;
                        ComponentCategory _category_7 = subcomp.getCategory();
                        if (_category_7 != null) {
                          switch (_category_7) {
                            case ABSTRACT:
                              this.manipulation.remove(parentcompimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), subcomp);
                              break;
                            case DATA:
                              this.manipulation.remove(parentcompimp, this.aadl2Package.getSubprogramImplementation_OwnedDataSubcomponent(), subcomp);
                              break;
                            case SUBPROGRAM:
                              this.manipulation.remove(parentcompimp, this.aadl2Package.getSubprogramImplementation_OwnedSubprogramSubcomponent(), subcomp);
                              break;
                            default:
                              _switchResult_7 = null;
                              break;
                          }
                        } else {
                          _switchResult_7 = null;
                        }
                        _xifexpression_7 = _switchResult_7;
                      } else {
                        Object _xifexpression_8 = null;
                        if ((parentcompimp instanceof SubprogramGroupImplementation)) {
                          Object _switchResult_8 = null;
                          ComponentCategory _category_8 = subcomp.getCategory();
                          if (_category_8 != null) {
                            switch (_category_8) {
                              case ABSTRACT:
                                this.manipulation.remove(parentcompimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), subcomp);
                                break;
                              case DATA:
                                this.manipulation.remove(parentcompimp, this.aadl2Package.getSubprogramGroupImplementation_OwnedDataSubcomponent(), subcomp);
                                break;
                              case SUBPROGRAM:
                                this.manipulation.remove(parentcompimp, this.aadl2Package.getSubprogramGroupImplementation_OwnedSubprogramSubcomponent(), subcomp);
                                break;
                              case SUBPROGRAM_GROUP:
                                this.manipulation.remove(parentcompimp, this.aadl2Package.getSubprogramGroupImplementation_OwnedSubprogramGroupSubcomponent(), subcomp);
                                break;
                              default:
                                _switchResult_8 = null;
                                break;
                            }
                          } else {
                            _switchResult_8 = null;
                          }
                          _xifexpression_8 = _switchResult_8;
                        } else {
                          Object _xifexpression_9 = null;
                          if ((parentcompimp instanceof SystemImplementation)) {
                            Object _switchResult_9 = null;
                            ComponentCategory _category_9 = subcomp.getCategory();
                            if (_category_9 != null) {
                              switch (_category_9) {
                                case ABSTRACT:
                                  this.manipulation.remove(parentcompimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), subcomp);
                                  break;
                                case BUS:
                                  this.manipulation.remove(parentcompimp, this.aadl2Package.getSystemImplementation_OwnedBusSubcomponent(), subcomp);
                                  break;
                                case DATA:
                                  this.manipulation.remove(parentcompimp, this.aadl2Package.getSystemImplementation_OwnedDataSubcomponent(), subcomp);
                                  break;
                                case DEVICE:
                                  this.manipulation.remove(parentcompimp, this.aadl2Package.getSystemImplementation_OwnedDeviceSubcomponent(), subcomp);
                                  break;
                                case MEMORY:
                                  this.manipulation.remove(parentcompimp, this.aadl2Package.getSystemImplementation_OwnedMemorySubcomponent(), subcomp);
                                  break;
                                case PROCESS:
                                  this.manipulation.remove(parentcompimp, this.aadl2Package.getSystemImplementation_OwnedProcessSubcomponent(), subcomp);
                                  break;
                                case PROCESSOR:
                                  this.manipulation.remove(parentcompimp, this.aadl2Package.getSystemImplementation_OwnedProcessorSubcomponent(), subcomp);
                                  break;
                                case SUBPROGRAM:
                                  this.manipulation.remove(parentcompimp, this.aadl2Package.getSystemImplementation_OwnedSubprogramSubcomponent(), subcomp);
                                  break;
                                case SUBPROGRAM_GROUP:
                                  this.manipulation.remove(parentcompimp, this.aadl2Package.getSystemImplementation_OwnedSubprogramGroupSubcomponent(), subcomp);
                                  break;
                                case SYSTEM:
                                  this.manipulation.remove(parentcompimp, this.aadl2Package.getSystemImplementation_OwnedSystemSubcomponent(), subcomp);
                                  break;
                                case VIRTUAL_BUS:
                                  this.manipulation.remove(parentcompimp, this.aadl2Package.getSystemImplementation_OwnedVirtualBusSubcomponent(), subcomp);
                                  break;
                                case VIRTUAL_PROCESSOR:
                                  this.manipulation.remove(parentcompimp, this.aadl2Package.getSystemImplementation_OwnedVirtualProcessorSubcomponent(), subcomp);
                                  break;
                                default:
                                  _switchResult_9 = null;
                                  break;
                              }
                            } else {
                              _switchResult_9 = null;
                            }
                            _xifexpression_9 = _switchResult_9;
                          } else {
                            Object _xifexpression_10 = null;
                            if ((parentcompimp instanceof ThreadImplementation)) {
                              Object _switchResult_10 = null;
                              ComponentCategory _category_10 = subcomp.getCategory();
                              if (_category_10 != null) {
                                switch (_category_10) {
                                  case ABSTRACT:
                                    this.manipulation.remove(parentcompimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), subcomp);
                                    break;
                                  case DATA:
                                    this.manipulation.remove(parentcompimp, this.aadl2Package.getThreadImplementation_OwnedDataSubcomponent(), subcomp);
                                    break;
                                  case SUBPROGRAM:
                                    this.manipulation.remove(parentcompimp, this.aadl2Package.getThreadImplementation_OwnedSubprogramSubcomponent(), subcomp);
                                    break;
                                  case SUBPROGRAM_GROUP:
                                    this.manipulation.remove(parentcompimp, this.aadl2Package.getThreadImplementation_OwnedSubprogramGroupSubcomponent(), subcomp);
                                    break;
                                  default:
                                    _switchResult_10 = null;
                                    break;
                                }
                              } else {
                                _switchResult_10 = null;
                              }
                              _xifexpression_10 = _switchResult_10;
                            } else {
                              Object _xifexpression_11 = null;
                              if ((parentcompimp instanceof ThreadGroupImplementation)) {
                                Object _switchResult_11 = null;
                                ComponentCategory _category_11 = subcomp.getCategory();
                                if (_category_11 != null) {
                                  switch (_category_11) {
                                    case ABSTRACT:
                                      this.manipulation.remove(parentcompimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), subcomp);
                                      break;
                                    case DATA:
                                      this.manipulation.remove(parentcompimp, this.aadl2Package.getThreadGroupImplementation_OwnedDataSubcomponent(), subcomp);
                                      break;
                                    case SUBPROGRAM:
                                      this.manipulation.remove(parentcompimp, this.aadl2Package.getThreadGroupImplementation_OwnedSubprogramSubcomponent(), subcomp);
                                      break;
                                    case SUBPROGRAM_GROUP:
                                      this.manipulation.remove(parentcompimp, this.aadl2Package.getThreadGroupImplementation_OwnedSubprogramGroupSubcomponent(), subcomp);
                                      break;
                                    case THREAD:
                                      this.manipulation.remove(parentcompimp, this.aadl2Package.getThreadGroupImplementation_OwnedThreadSubcomponent(), subcomp);
                                      break;
                                    case THREAD_GROUP:
                                      this.manipulation.remove(parentcompimp, this.aadl2Package.getThreadGroupImplementation_OwnedThreadGroupSubcomponent(), subcomp);
                                      break;
                                    default:
                                      _switchResult_11 = null;
                                      break;
                                  }
                                } else {
                                  _switchResult_11 = null;
                                }
                                _xifexpression_11 = _switchResult_11;
                              } else {
                                Object _xifexpression_12 = null;
                                if ((parentcompimp instanceof VirtualBusImplementation)) {
                                  Object _switchResult_12 = null;
                                  ComponentCategory _category_12 = subcomp.getCategory();
                                  if (_category_12 != null) {
                                    switch (_category_12) {
                                      case ABSTRACT:
                                        this.manipulation.remove(parentcompimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), subcomp);
                                        break;
                                      case VIRTUAL_BUS:
                                        this.manipulation.remove(parentcompimp, this.aadl2Package.getVirtualBusImplementation_OwnedVirtualBusSubcomponent(), subcomp);
                                        break;
                                      default:
                                        _switchResult_12 = null;
                                        break;
                                    }
                                  } else {
                                    _switchResult_12 = null;
                                  }
                                  _xifexpression_12 = _switchResult_12;
                                } else {
                                  Object _xifexpression_13 = null;
                                  if ((parentcompimp instanceof VirtualProcessorImplementation)) {
                                    Object _switchResult_13 = null;
                                    ComponentCategory _category_13 = subcomp.getCategory();
                                    if (_category_13 != null) {
                                      switch (_category_13) {
                                        case ABSTRACT:
                                          this.manipulation.remove(parentcompimp, this.aadl2Package.getComponentImplementation_OwnedAbstractSubcomponent(), subcomp);
                                          break;
                                        case VIRTUAL_BUS:
                                          this.manipulation.remove(parentcompimp, this.aadl2Package.getVirtualProcessorImplementation_OwnedVirtualBusSubcomponent(), subcomp);
                                          break;
                                        case VIRTUAL_PROCESSOR:
                                          this.manipulation.remove(parentcompimp, this.aadl2Package.getVirtualProcessorImplementation_OwnedVirtualProcessorSubcomponent(), subcomp);
                                          break;
                                        default:
                                          _switchResult_13 = null;
                                          break;
                                      }
                                    } else {
                                      _switchResult_13 = null;
                                    }
                                    _xifexpression_13 = _switchResult_13;
                                  }
                                  _xifexpression_12 = _xifexpression_13;
                                }
                                _xifexpression_11 = _xifexpression_12;
                              }
                              _xifexpression_10 = _xifexpression_11;
                            }
                            _xifexpression_9 = _xifexpression_10;
                          }
                          _xifexpression_8 = _xifexpression_9;
                        }
                        _xifexpression_7 = _xifexpression_8;
                      }
                      _xifexpression_6 = _xifexpression_7;
                    }
                    _xifexpression_5 = _xifexpression_6;
                  }
                  _xifexpression_4 = _xifexpression_5;
                }
                _xifexpression_3 = _xifexpression_4;
              }
              _xifexpression_2 = _xifexpression_3;
            }
            _xifexpression_1 = _xifexpression_2;
          }
          _xifexpression = _xifexpression_1;
        }
        _xblockexpression = _xifexpression;
      }
      return _xblockexpression;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected ComponentImplementation extendParentImplementationForRefinement(final ComponentInstance subcompinst) {
    try {
      EObject _eContainer = subcompinst.eContainer();
      ComponentInstance compinst = ((ComponentInstance) _eContainer);
      if ((compinst != this.topSystemInst)) {
        EObject _eContainer_1 = compinst.eContainer();
        boolean _isAffectingInstanceObject = LibraryUtils.isAffectingInstanceObject(compinst, ((ComponentInstance) _eContainer_1), this.aadlPublicPackage, this.engine, this.preferences.isModifyReused(), false);
        if (_isAffectingInstanceObject) {
          EObject _eContainer_2 = compinst.eContainer();
          this.denyImplementnUpdatePropagn(compinst, ((ComponentInstance) _eContainer_2), false);
        }
      }
      ComponentImplementation newcompimp = this.createComponentImplementation(compinst.getCategory());
      this.classifierCreationDIMPropertyAddition(newcompimp);
      ComponentClassifier _xifexpression = null;
      boolean _equals = Objects.equal(compinst, this.topSystemInst);
      if (_equals) {
        _xifexpression = ((SystemInstance) compinst).getComponentImplementation();
      } else {
        _xifexpression = compinst.getClassifier();
      }
      ComponentImplementation oldcompimp = ((ComponentImplementation) _xifexpression);
      this.manipulation.set(newcompimp, this.aadl2Package.getComponentImplementation_Extended(), oldcompimp);
      this.manipulation.set(newcompimp, this.aadl2Package.getComponentImplementation_Type(), oldcompimp.getType());
      EAttribute _namedElement_Name = this.aadl2Package.getNamedElement_Name();
      String _name = oldcompimp.getName();
      String _plus = (_name + "_ext");
      this.manipulation.set(newcompimp, _namedElement_Name, _plus);
      this.manipulation.set(compinst, this.instPackage.getComponentInstance_Classifier(), newcompimp);
      if ((compinst != this.topSystemInst)) {
        this.setSubcomponentType(compinst, compinst.getClassifier());
      } else {
        this.manipulation.set(((SystemInstance) compinst), this.instPackage.getSystemInstance_ComponentImplementation(), newcompimp);
      }
      return newcompimp;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected void denyFeatureUpdatePropagn(final FeatureInstance featinst) {
    try {
      EObject _eContainer = featinst.eContainer();
      ComponentInstance compinst = ((ComponentInstance) _eContainer);
      boolean _isAffectingInstanceObject = LibraryUtils.isAffectingInstanceObject(featinst, compinst, this.aadlPublicPackage, this.engine, this.preferences.isModifyReused(), true);
      if (_isAffectingInstanceObject) {
        ComponentClassifier _classifier = compinst.getClassifier();
        if ((_classifier instanceof ComponentImplementation)) {
          EObject _eContainer_1 = featinst.getFeature().eContainer();
          ComponentClassifier _classifier_1 = compinst.getClassifier();
          EList<ComponentImplementation> implList = LibraryUtils.getAllTypeInheritingImplementations(((ComponentType) _eContainer_1), ((ComponentImplementation) _classifier_1));
          Integer implIndex = LibraryUtils.computeExtensionIndex(implList, this.aadlPublicPackage, this.engine, Boolean.valueOf(this.preferences.isModifyReused()));
          if ((implIndex != null)) {
            ComponentImplementation previousImpl = null;
            List<ComponentImplementation> _reverse = ListExtensions.<ComponentImplementation>reverse(implList);
            for (final ComponentClassifier currentCompImpl : _reverse) {
              {
                int _indexOf = implList.indexOf(currentCompImpl);
                int _size = implList.size();
                int _minus = (_size - (implIndex).intValue());
                boolean _lessThan = (_indexOf < _minus);
                if (_lessThan) {
                  ComponentClassifier _copyClassifier = this.copyClassifier(currentCompImpl, previousImpl, implList, featinst);
                  previousImpl = ((ComponentImplementation) _copyClassifier);
                } else {
                  int _indexOf_1 = implList.indexOf(currentCompImpl);
                  int _size_1 = implList.size();
                  int _minus_1 = (_size_1 - (implIndex).intValue());
                  boolean _equals = (_indexOf_1 == _minus_1);
                  if (_equals) {
                    this.manipulation.set(currentCompImpl, this.aadl2Package.getComponentImplementation_Extended(), previousImpl);
                  }
                }
                int _indexOf_2 = implList.indexOf(currentCompImpl);
                int _size_2 = implList.size();
                int _minus_2 = (_size_2 - 1);
                boolean _equals_1 = (_indexOf_2 == _minus_2);
                if (_equals_1) {
                  this.manipulation.set(compinst, this.instPackage.getComponentInstance_Classifier(), previousImpl);
                  if ((compinst != this.topSystemInst)) {
                    EObject _eContainer_2 = compinst.eContainer();
                    boolean _isAffectingInstanceObject_1 = LibraryUtils.isAffectingInstanceObject(compinst, ((ComponentInstance) _eContainer_2), this.aadlPublicPackage, this.engine, this.preferences.isModifyReused(), false);
                    if (_isAffectingInstanceObject_1) {
                      EObject _eContainer_3 = compinst.eContainer();
                      this.denyImplementnUpdatePropagn(compinst, ((ComponentInstance) _eContainer_3), false);
                    }
                  }
                }
              }
            }
          }
          EList<? extends ComponentClassifier> typeList = LibraryUtils.getAllInheritingParentClassifiers(featinst.getFeature(), compinst, true);
          Integer typeIndex = LibraryUtils.computeExtensionIndex(typeList, this.aadlPublicPackage, this.engine, Boolean.valueOf(this.preferences.isModifyReused()));
          if ((typeIndex != null)) {
            ComponentType previousType = null;
            List<? extends ComponentClassifier> _reverse_1 = ListExtensions.reverse(typeList);
            for (final ComponentClassifier currentCompType : _reverse_1) {
              int _indexOf = typeList.indexOf(currentCompType);
              int _size = typeList.size();
              int _minus = (_size - (typeIndex).intValue());
              boolean _lessThan = (_indexOf < _minus);
              if (_lessThan) {
                ComponentClassifier _copyClassifier = this.copyClassifier(currentCompType, previousType, typeList, featinst);
                previousType = ((ComponentType) _copyClassifier);
                for (final ComponentImplementation newCompImpl : implList) {
                  ComponentType _type = newCompImpl.getType();
                  boolean _equals = Objects.equal(_type, currentCompType);
                  if (_equals) {
                    this.manipulation.set(newCompImpl, this.aadl2Package.getComponentImplementation_Type(), previousType);
                  }
                }
              }
            }
          }
        } else {
          EList<? extends ComponentClassifier> typeList_1 = LibraryUtils.getAllInheritingParentClassifiers(featinst.getFeature(), compinst, true);
          Integer typeIndex_1 = LibraryUtils.computeExtensionIndex(typeList_1, this.aadlPublicPackage, this.engine, Boolean.valueOf(this.preferences.isModifyReused()));
          ComponentType previousType_1 = null;
          List<? extends ComponentClassifier> _reverse_2 = ListExtensions.reverse(typeList_1);
          for (final ComponentClassifier currentCompType_1 : _reverse_2) {
            {
              int _indexOf_1 = typeList_1.indexOf(currentCompType_1);
              int _size_1 = typeList_1.size();
              int _minus_1 = (_size_1 - (typeIndex_1).intValue());
              boolean _lessThan_1 = (_indexOf_1 < _minus_1);
              if (_lessThan_1) {
                ComponentClassifier _copyClassifier_1 = this.copyClassifier(currentCompType_1, previousType_1, typeList_1, featinst);
                previousType_1 = ((ComponentType) _copyClassifier_1);
              } else {
                int _indexOf_2 = typeList_1.indexOf(currentCompType_1);
                int _size_2 = typeList_1.size();
                int _minus_2 = (_size_2 - (typeIndex_1).intValue());
                boolean _equals_1 = (_indexOf_2 == _minus_2);
                if (_equals_1) {
                  this.manipulation.set(currentCompType_1, this.aadl2Package.getComponentType_Extended(), previousType_1);
                }
              }
              int _indexOf_3 = typeList_1.indexOf(currentCompType_1);
              int _size_3 = typeList_1.size();
              int _minus_3 = (_size_3 - 1);
              boolean _equals_2 = (_indexOf_3 == _minus_3);
              if (_equals_2) {
                this.manipulation.set(compinst, this.instPackage.getComponentInstance_Classifier(), previousType_1);
                if ((compinst != this.topSystemInst)) {
                  EObject _eContainer_2 = compinst.eContainer();
                  boolean _isAffectingInstanceObject_1 = LibraryUtils.isAffectingInstanceObject(compinst, ((ComponentInstance) _eContainer_2), this.aadlPublicPackage, this.engine, this.preferences.isModifyReused(), false);
                  if (_isAffectingInstanceObject_1) {
                    EObject _eContainer_3 = compinst.eContainer();
                    this.denyImplementnUpdatePropagn(compinst, ((ComponentInstance) _eContainer_3), false);
                  }
                }
              }
            }
          }
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected Object featureInstanceDeletedDIM(final Feature feature) {
    try {
      Object _xblockexpression = null;
      {
        EObject _eContainer = feature.eContainer();
        ComponentType comptype = ((ComponentType) _eContainer);
        FeatureCategory category = FeatureUtils.getFeatureCategory(feature);
        if ((comptype instanceof AbstractType)) {
          if (category != null) {
            switch (category) {
              case DATA_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getAbstractType_OwnedDataPort(), feature);
                break;
              case EVENT_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getAbstractType_OwnedEventPort(), feature);
                break;
              case EVENT_DATA_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getAbstractType_OwnedEventDataPort(), feature);
                break;
              case BUS_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getAbstractType_OwnedBusAccess(), feature);
                break;
              case DATA_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getAbstractType_OwnedDataAccess(), feature);
                break;
              case SUBPROGRAM_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getAbstractType_OwnedSubprogramAccess(), feature);
                break;
              case SUBPROGRAM_GROUP_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getAbstractType_OwnedSubprogramGroupAccess(), feature);
                break;
              case FEATURE_GROUP:
                this.manipulation.remove(comptype, this.aadl2Package.getComponentType_OwnedFeatureGroup(), feature);
                break;
              case ABSTRACT_FEATURE:
                this.manipulation.remove(comptype, this.aadl2Package.getComponentType_OwnedAbstractFeature(), feature);
                break;
              default:
                break;
            }
          } else {
          }
        }
        if ((comptype instanceof BusType)) {
          if (category != null) {
            switch (category) {
              case DATA_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getBusType_OwnedDataPort(), feature);
                break;
              case EVENT_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getBusType_OwnedEventPort(), feature);
                break;
              case EVENT_DATA_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getBusType_OwnedEventDataPort(), feature);
                break;
              case BUS_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getBusType_OwnedBusAccess(), feature);
                break;
              case FEATURE_GROUP:
                this.manipulation.remove(comptype, this.aadl2Package.getComponentType_OwnedFeatureGroup(), feature);
                break;
              case ABSTRACT_FEATURE:
                this.manipulation.remove(comptype, this.aadl2Package.getComponentType_OwnedAbstractFeature(), feature);
                break;
              default:
                break;
            }
          } else {
          }
        }
        if ((comptype instanceof DataType)) {
          if (category != null) {
            switch (category) {
              case DATA_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getDataType_OwnedDataAccess(), feature);
                break;
              case SUBPROGRAM_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getDataType_OwnedSubprogramAccess(), feature);
                break;
              case SUBPROGRAM_GROUP_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getDataType_OwnedSubprogramGroupAccess(), feature);
                break;
              case FEATURE_GROUP:
                this.manipulation.remove(comptype, this.aadl2Package.getComponentType_OwnedFeatureGroup(), feature);
                break;
              case ABSTRACT_FEATURE:
                this.manipulation.remove(comptype, this.aadl2Package.getComponentType_OwnedAbstractFeature(), feature);
                break;
              default:
                break;
            }
          } else {
          }
        }
        if ((comptype instanceof DeviceType)) {
          if (category != null) {
            switch (category) {
              case DATA_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getDeviceType_OwnedDataPort(), feature);
                break;
              case EVENT_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getDeviceType_OwnedEventPort(), feature);
                break;
              case EVENT_DATA_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getDeviceType_OwnedEventDataPort(), feature);
                break;
              case BUS_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getDeviceType_OwnedBusAccess(), feature);
                break;
              case SUBPROGRAM_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getDeviceType_OwnedSubprogramAccess(), feature);
                break;
              case SUBPROGRAM_GROUP_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getDeviceType_OwnedSubprogramGroupAccess(), feature);
                break;
              case FEATURE_GROUP:
                this.manipulation.remove(comptype, this.aadl2Package.getComponentType_OwnedFeatureGroup(), feature);
                break;
              case ABSTRACT_FEATURE:
                this.manipulation.remove(comptype, this.aadl2Package.getComponentType_OwnedAbstractFeature(), feature);
                break;
              default:
                break;
            }
          } else {
          }
        }
        if ((comptype instanceof MemoryType)) {
          if (category != null) {
            switch (category) {
              case DATA_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getMemoryType_OwnedDataPort(), feature);
                break;
              case EVENT_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getMemoryType_OwnedEventPort(), feature);
                break;
              case EVENT_DATA_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getMemoryType_OwnedEventDataPort(), feature);
                break;
              case BUS_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getMemoryType_OwnedBusAccess(), feature);
                break;
              case FEATURE_GROUP:
                this.manipulation.remove(comptype, this.aadl2Package.getComponentType_OwnedFeatureGroup(), feature);
                break;
              case ABSTRACT_FEATURE:
                this.manipulation.remove(comptype, this.aadl2Package.getComponentType_OwnedAbstractFeature(), feature);
                break;
              default:
                break;
            }
          } else {
          }
        }
        if ((comptype instanceof ProcessType)) {
          if (category != null) {
            switch (category) {
              case DATA_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getProcessType_OwnedDataPort(), feature);
                break;
              case EVENT_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getProcessType_OwnedEventPort(), feature);
                break;
              case EVENT_DATA_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getProcessType_OwnedEventDataPort(), feature);
                break;
              case DATA_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getProcessType_OwnedDataAccess(), feature);
                break;
              case SUBPROGRAM_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getProcessType_OwnedSubprogramAccess(), feature);
                break;
              case SUBPROGRAM_GROUP_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getProcessType_OwnedSubprogramGroupAccess(), feature);
                break;
              case FEATURE_GROUP:
                this.manipulation.remove(comptype, this.aadl2Package.getComponentType_OwnedFeatureGroup(), feature);
                break;
              case ABSTRACT_FEATURE:
                this.manipulation.remove(comptype, this.aadl2Package.getComponentType_OwnedAbstractFeature(), feature);
                break;
              default:
                break;
            }
          } else {
          }
        }
        if ((comptype instanceof ProcessorType)) {
          if (category != null) {
            switch (category) {
              case DATA_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getProcessorType_OwnedDataPort(), feature);
                break;
              case EVENT_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getProcessorType_OwnedEventPort(), feature);
                break;
              case EVENT_DATA_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getProcessorType_OwnedEventDataPort(), feature);
                break;
              case BUS_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getProcessorType_OwnedBusAccess(), feature);
                break;
              case SUBPROGRAM_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getProcessorType_OwnedSubprogramAccess(), feature);
                break;
              case SUBPROGRAM_GROUP_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getProcessorType_OwnedSubprogramGroupAccess(), feature);
                break;
              case FEATURE_GROUP:
                this.manipulation.remove(comptype, this.aadl2Package.getComponentType_OwnedFeatureGroup(), feature);
                break;
              case ABSTRACT_FEATURE:
                this.manipulation.remove(comptype, this.aadl2Package.getComponentType_OwnedAbstractFeature(), feature);
                break;
              default:
                break;
            }
          } else {
          }
        }
        if ((comptype instanceof SubprogramType)) {
          if (category != null) {
            switch (category) {
              case EVENT_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getSubprogramType_OwnedEventPort(), feature);
                break;
              case EVENT_DATA_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getSubprogramType_OwnedEventDataPort(), feature);
                break;
              case PARAMETER:
                this.manipulation.remove(comptype, this.aadl2Package.getSubprogramType_OwnedParameter(), feature);
                break;
              case DATA_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getSubprogramType_OwnedDataAccess(), feature);
                break;
              case SUBPROGRAM_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getSubprogramType_OwnedSubprogramAccess(), feature);
                break;
              case SUBPROGRAM_GROUP_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getSubprogramType_OwnedSubprogramGroupAccess(), feature);
                break;
              case FEATURE_GROUP:
                this.manipulation.remove(comptype, this.aadl2Package.getComponentType_OwnedFeatureGroup(), feature);
                break;
              case ABSTRACT_FEATURE:
                this.manipulation.remove(comptype, this.aadl2Package.getComponentType_OwnedAbstractFeature(), feature);
                break;
              default:
                break;
            }
          } else {
          }
        }
        if ((comptype instanceof SubprogramGroupType)) {
          if (category != null) {
            switch (category) {
              case SUBPROGRAM_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getSubprogramGroupType_OwnedSubprogramAccess(), feature);
                break;
              case SUBPROGRAM_GROUP_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getSubprogramGroupType_OwnedSubprogramGroupAccess(), feature);
                break;
              case FEATURE_GROUP:
                this.manipulation.remove(comptype, this.aadl2Package.getComponentType_OwnedFeatureGroup(), feature);
                break;
              case ABSTRACT_FEATURE:
                this.manipulation.remove(comptype, this.aadl2Package.getComponentType_OwnedAbstractFeature(), feature);
                break;
              default:
                break;
            }
          } else {
          }
        }
        if ((comptype instanceof SystemType)) {
          if (category != null) {
            switch (category) {
              case DATA_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getSystemType_OwnedDataPort(), feature);
                break;
              case EVENT_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getSystemType_OwnedEventPort(), feature);
                break;
              case EVENT_DATA_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getSystemType_OwnedEventDataPort(), feature);
                break;
              case BUS_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getSystemType_OwnedBusAccess(), feature);
                break;
              case DATA_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getSystemType_OwnedDataAccess(), feature);
                break;
              case SUBPROGRAM_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getSystemType_OwnedSubprogramAccess(), feature);
                break;
              case SUBPROGRAM_GROUP_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getSystemType_OwnedSubprogramGroupAccess(), feature);
                break;
              case FEATURE_GROUP:
                this.manipulation.remove(comptype, this.aadl2Package.getComponentType_OwnedFeatureGroup(), feature);
                break;
              case ABSTRACT_FEATURE:
                this.manipulation.remove(comptype, this.aadl2Package.getComponentType_OwnedAbstractFeature(), feature);
                break;
              default:
                break;
            }
          } else {
          }
        }
        if ((comptype instanceof ThreadType)) {
          if (category != null) {
            switch (category) {
              case DATA_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getThreadType_OwnedDataPort(), feature);
                break;
              case EVENT_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getThreadType_OwnedEventPort(), feature);
                break;
              case EVENT_DATA_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getThreadType_OwnedEventDataPort(), feature);
                break;
              case DATA_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getThreadType_OwnedDataAccess(), feature);
                break;
              case SUBPROGRAM_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getThreadType_OwnedSubprogramAccess(), feature);
                break;
              case SUBPROGRAM_GROUP_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getThreadType_OwnedSubprogramGroupAccess(), feature);
                break;
              case FEATURE_GROUP:
                this.manipulation.remove(comptype, this.aadl2Package.getComponentType_OwnedFeatureGroup(), feature);
                break;
              case ABSTRACT_FEATURE:
                this.manipulation.remove(comptype, this.aadl2Package.getComponentType_OwnedAbstractFeature(), feature);
                break;
              default:
                break;
            }
          } else {
          }
        }
        if ((comptype instanceof ThreadGroupType)) {
          if (category != null) {
            switch (category) {
              case DATA_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getThreadGroupType_OwnedDataPort(), feature);
                break;
              case EVENT_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getThreadGroupType_OwnedEventPort(), feature);
                break;
              case EVENT_DATA_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getThreadGroupType_OwnedEventDataPort(), feature);
                break;
              case DATA_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getThreadGroupType_OwnedDataAccess(), feature);
                break;
              case SUBPROGRAM_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getThreadGroupType_OwnedSubprogramAccess(), feature);
                break;
              case SUBPROGRAM_GROUP_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getThreadGroupType_OwnedSubprogramGroupAccess(), feature);
                break;
              case FEATURE_GROUP:
                this.manipulation.remove(comptype, this.aadl2Package.getComponentType_OwnedFeatureGroup(), feature);
                break;
              case ABSTRACT_FEATURE:
                this.manipulation.remove(comptype, this.aadl2Package.getComponentType_OwnedAbstractFeature(), feature);
                break;
              default:
                break;
            }
          } else {
          }
        }
        if ((comptype instanceof VirtualBusType)) {
          if (category != null) {
            switch (category) {
              case DATA_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getVirtualBusType_OwnedDataPort(), feature);
                break;
              case EVENT_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getVirtualBusType_OwnedEventPort(), feature);
                break;
              case EVENT_DATA_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getVirtualBusType_OwnedEventDataPort(), feature);
                break;
              case BUS_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getVirtualBusType_OwnedBusAccess(), feature);
                break;
              case FEATURE_GROUP:
                this.manipulation.remove(comptype, this.aadl2Package.getComponentType_OwnedFeatureGroup(), feature);
                break;
              case ABSTRACT_FEATURE:
                this.manipulation.remove(comptype, this.aadl2Package.getComponentType_OwnedAbstractFeature(), feature);
                break;
              default:
                break;
            }
          } else {
          }
        }
        Object _xifexpression = null;
        if ((comptype instanceof VirtualProcessorType)) {
          Object _switchResult_13 = null;
          if (category != null) {
            switch (category) {
              case DATA_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getVirtualProcessorType_OwnedDataPort(), feature);
                break;
              case EVENT_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getVirtualProcessorType_OwnedEventPort(), feature);
                break;
              case EVENT_DATA_PORT:
                this.manipulation.remove(comptype, this.aadl2Package.getVirtualProcessorType_OwnedEventDataPort(), feature);
                break;
              case BUS_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getVirtualProcessorType_OwnedBusAccess(), feature);
                break;
              case SUBPROGRAM_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getVirtualProcessorType_OwnedSubprogramAccess(), feature);
                break;
              case SUBPROGRAM_GROUP_ACCESS:
                this.manipulation.remove(comptype, this.aadl2Package.getVirtualProcessorType_OwnedSubprogramGroupAccess(), feature);
                break;
              case FEATURE_GROUP:
                this.manipulation.remove(comptype, this.aadl2Package.getComponentType_OwnedFeatureGroup(), feature);
                break;
              case ABSTRACT_FEATURE:
                this.manipulation.remove(comptype, this.aadl2Package.getComponentType_OwnedAbstractFeature(), feature);
                break;
              default:
                _switchResult_13 = null;
                break;
            }
          } else {
            _switchResult_13 = null;
          }
          _xifexpression = _switchResult_13;
        }
        _xblockexpression = _xifexpression;
      }
      return _xblockexpression;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected void refineConnection(final ConnectionInstance conninst) {
    try {
      EList<ConnectionReference> _connectionReferences = conninst.getConnectionReferences();
      for (final ConnectionReference connref : _connectionReferences) {
        {
          ComponentInstance compinst = connref.getContext();
          if (((compinst != this.topSystemInst) && LibraryUtils.isAffectingInstanceObject(compinst, ((ComponentInstance) compinst.eContainer()), this.aadlPublicPackage, this.engine, this.preferences.isModifyReused(), false))) {
            EObject _eContainer = compinst.eContainer();
            this.denyImplementnUpdatePropagn(compinst, ((ComponentInstance) _eContainer), false);
          }
          ComponentClassifier _classifier = compinst.getClassifier();
          ComponentImplementation compimp = ((ComponentImplementation) _classifier);
          ComponentImplementation refimp = this.createComponentImplementation(compinst.getCategory());
          EAttribute _namedElement_Name = this.aadl2Package.getNamedElement_Name();
          String _name = compimp.getName();
          String _plus = (_name + "_ext");
          this.manipulation.set(refimp, _namedElement_Name, _plus);
          this.manipulation.set(refimp, this.aadl2Package.getComponentImplementation_Extended(), compimp);
          Connection refconn = this.createConnection(conninst.getKind(), refimp);
          this.manipulation.set(refconn, this.aadl2Package.getConnection_Refined(), connref.getConnection());
          this.manipulation.set(refconn, this.aadl2Package.getNamedElement_Name(), connref.getConnection().getName());
          this.populateNewConnection(refconn, conninst, connref);
          this.manipulation.set(compinst, this.instPackage.getComponentInstance_Classifier(), refimp);
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected Object connectionInstanceDeletedDIM(final ConnectionReference connref, final Connection conn) {
    try {
      Object _xblockexpression = null;
      {
        boolean _isAffectingInstanceObject = LibraryUtils.isAffectingInstanceObject(connref, connref.getContext(), this.aadlPublicPackage, this.engine, this.preferences.isModifyReused(), true);
        if (_isAffectingInstanceObject) {
          this.denyImplementnUpdatePropagn(connref, connref.getContext(), true);
        }
        Object _switchResult = null;
        EObject _eContainer = connref.eContainer();
        ConnectionKind _kind = ((ConnectionInstance) _eContainer).getKind();
        if (_kind != null) {
          switch (_kind) {
            case ACCESS_CONNECTION:
              EObject _eContainer_1 = conn.eContainer();
              this.manipulation.remove(((ComponentImplementation) _eContainer_1), this.aadl2Package.getComponentImplementation_OwnedAccessConnection(), conn);
              break;
            case FEATURE_CONNECTION:
              EObject _eContainer_2 = conn.eContainer();
              this.manipulation.remove(((ComponentImplementation) _eContainer_2), this.aadl2Package.getComponentImplementation_OwnedFeatureConnection(), conn);
              break;
            case FEATURE_GROUP_CONNECTION:
              EObject _eContainer_3 = conn.eContainer();
              this.manipulation.remove(((ComponentImplementation) _eContainer_3), this.aadl2Package.getComponentImplementation_OwnedFeatureGroupConnection(), conn);
              break;
            case PARAMETER_CONNECTION:
              EObject _eContainer_4 = conn.eContainer();
              this.manipulation.remove(((ComponentImplementation) _eContainer_4), this.aadl2Package.getComponentImplementation_OwnedParameterConnection(), conn);
              break;
            case PORT_CONNECTION:
              EObject _eContainer_5 = conn.eContainer();
              this.manipulation.remove(((ComponentImplementation) _eContainer_5), this.aadl2Package.getComponentImplementation_OwnedPortConnection(), conn);
              break;
            default:
              _switchResult = null;
              break;
          }
        } else {
          _switchResult = null;
        }
        _xblockexpression = _switchResult;
      }
      return _xblockexpression;
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected void propertyValueCreatedDIM(final PropertyAssociationInstance propinst, final ModalPropertyValue modpropinst, final PropertyValue propvalinst) {
    try {
      final ModalPropertyValue modpropadd = PropertyUtils.getDeclarativeModalPropertyValue(modpropinst);
      Boolean _isSimpleProperty = PropertyTypeUtils.isSimpleProperty(propinst.getProperty().getPropertyType());
      if ((_isSimpleProperty).booleanValue()) {
        this.simplePropertyValueCreation(propinst.getProperty().getPropertyType(), propvalinst, modpropadd);
      } else {
        PropertyType _propertyType = propinst.getProperty().getPropertyType();
        if ((_propertyType instanceof RecordType)) {
          EObject _xifexpression = null;
          PropertyExpression _ownedValue = modpropadd.getOwnedValue();
          boolean _tripleEquals = (_ownedValue == null);
          if (_tripleEquals) {
            _xifexpression = this.manipulation.createChild(modpropadd, this.aadl2Package.getModalPropertyValue_OwnedValue(), this.aadl2Package.getRecordValue());
          } else {
            EObject _xifexpression_1 = null;
            PropertyExpression _ownedValue_1 = modpropadd.getOwnedValue();
            boolean _not = (!(_ownedValue_1 instanceof RecordValue));
            if (_not) {
              EObject _xblockexpression = null;
              {
                this.manipulation.remove(modpropadd.getOwnedValue());
                _xblockexpression = this.manipulation.createChild(modpropadd, this.aadl2Package.getModalPropertyValue_OwnedValue(), this.aadl2Package.getRecordValue());
              }
              _xifexpression_1 = _xblockexpression;
            } else {
              _xifexpression_1 = ((RecordValue) modpropadd.getOwnedValue());
            }
            _xifexpression = _xifexpression_1;
          }
          EObject recvaladd = _xifexpression;
          EObject _eContainer = propvalinst.eContainer();
          BasicPropertyAssociation basicpropinst = ((BasicPropertyAssociation) _eContainer);
          EObject _createChild = this.manipulation.createChild(recvaladd, this.aadl2Package.getRecordValue_OwnedFieldValue(), this.aadl2Package.getBasicPropertyAssociation());
          BasicPropertyAssociation basicpropadd = ((BasicPropertyAssociation) _createChild);
          this.manipulation.set(basicpropadd, this.aadl2Package.getBasicPropertyAssociation_Property(), basicpropinst.getProperty());
          this.simplePropertyValueCreation(basicpropinst.getProperty().getPropertyType(), propvalinst, basicpropadd);
        } else {
          EObject _xifexpression_2 = null;
          PropertyExpression _ownedValue_2 = modpropadd.getOwnedValue();
          boolean _tripleEquals_1 = (_ownedValue_2 == null);
          if (_tripleEquals_1) {
            _xifexpression_2 = this.manipulation.createChild(modpropadd, this.aadl2Package.getModalPropertyValue_OwnedValue(), this.aadl2Package.getListValue());
          } else {
            EObject _xifexpression_3 = null;
            PropertyExpression _ownedValue_3 = modpropadd.getOwnedValue();
            boolean _not_1 = (!(_ownedValue_3 instanceof ListValue));
            if (_not_1) {
              EObject _xblockexpression_1 = null;
              {
                this.manipulation.remove(modpropadd.getOwnedValue());
                _xblockexpression_1 = this.manipulation.createChild(modpropadd, this.aadl2Package.getModalPropertyValue_OwnedValue(), this.aadl2Package.getListValue());
              }
              _xifexpression_3 = _xblockexpression_1;
            } else {
              _xifexpression_3 = modpropadd.getOwnedValue();
            }
            _xifexpression_2 = _xifexpression_3;
          }
          ListValue listvaladd = ((ListValue) _xifexpression_2);
          EObject _eContainer_1 = propvalinst.eContainer();
          if ((_eContainer_1 instanceof BasicPropertyAssociation)) {
            PropertyExpression _ownedValue_4 = modpropinst.getOwnedValue();
            ListValue listvalinst = ((ListValue) _ownedValue_4);
            EObject _eContainer_2 = propvalinst.eContainer();
            BasicPropertyAssociation basicpropinst_1 = ((BasicPropertyAssociation) _eContainer_2);
            EObject _eContainer_3 = basicpropinst_1.eContainer();
            RecordValue recvalinst = ((RecordValue) _eContainer_3);
            EObject _xifexpression_4 = null;
            int _indexOf = listvalinst.getOwnedListElements().indexOf(recvalinst);
            int _size = listvaladd.getOwnedListElements().size();
            boolean _lessThan = (_indexOf < _size);
            if (_lessThan) {
              _xifexpression_4 = listvaladd.getOwnedListElements().get(listvalinst.getOwnedListElements().indexOf(recvalinst));
            } else {
              _xifexpression_4 = this.manipulation.createChild(listvaladd, this.aadl2Package.getListValue_OwnedListElement(), this.aadl2Package.getRecordValue());
            }
            EObject recvaladd_1 = _xifexpression_4;
            EObject _createChild_1 = this.manipulation.createChild(recvaladd_1, this.aadl2Package.getRecordValue_OwnedFieldValue(), this.aadl2Package.getBasicPropertyAssociation());
            BasicPropertyAssociation basicpropadd_1 = ((BasicPropertyAssociation) _createChild_1);
            this.manipulation.set(basicpropadd_1, this.aadl2Package.getBasicPropertyAssociation_Property(), basicpropinst_1.getProperty());
            this.simplePropertyValueCreation(basicpropinst_1.getProperty().getPropertyType(), propvalinst, basicpropadd_1);
          } else {
            this.simplePropertyValueCreation(propinst.getProperty().getPropertyType(), propvalinst, listvaladd);
          }
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  protected void propertyValueUpdatedDIM(final PropertyValue propvalinst, final PublicPackageSection aadlPackage) {
    try {
      PropertyValue propval = PropertyUtils.getDeclarativePropertyValue(propvalinst);
      if ((propvalinst instanceof StringLiteral)) {
        this.manipulation.set(propval, this.aadl2Package.getStringLiteral_Value(), ((StringLiteral) propvalinst).getValue());
      } else {
        if ((propvalinst instanceof IntegerLiteral)) {
          this.manipulation.set(propval, this.aadl2Package.getIntegerLiteral_Value(), Long.valueOf(((IntegerLiteral) propvalinst).getValue()));
          this.manipulation.set(propval, this.aadl2Package.getIntegerLiteral_Base(), Long.valueOf(((IntegerLiteral) propvalinst).getBase()));
          this.manipulation.set(propval, this.aadl2Package.getNumberValue_Unit(), ((IntegerLiteral) propvalinst).getUnit());
        } else {
          if ((propvalinst instanceof RealLiteral)) {
            this.manipulation.set(propval, this.aadl2Package.getRealLiteral_Value(), Double.valueOf(((RealLiteral) propvalinst).getValue()));
            this.manipulation.set(propval, this.aadl2Package.getNumberValue_Unit(), ((RealLiteral) propvalinst).getUnit());
          } else {
            if ((propvalinst instanceof ClassifierValue)) {
              if (((((ClassifierValue) propvalinst).getClassifier() != null) && (LibraryUtils.isLibraryClassifier(((ComponentClassifier) ((ClassifierValue) propvalinst).getClassifier()), this.aadlPublicPackage)).booleanValue())) {
                EObject _eContainer = ((ClassifierValue) propvalinst).getClassifier().eContainer().eContainer();
                this.manipulation.add(this.aadlPublicPackage, this.aadl2Package.getPackageSection_ImportedUnit(), ((ModelUnit) _eContainer));
              }
              this.manipulation.set(propval, this.aadl2Package.getClassifierValue_Classifier(), ((ClassifierValue) propvalinst).getClassifier());
            } else {
              if ((propvalinst instanceof BooleanLiteral)) {
                this.manipulation.set(propval, this.aadl2Package.getBooleanLiteral_Value(), Boolean.valueOf(((BooleanLiteral) propvalinst).getValue()));
              } else {
                if ((propvalinst instanceof RangeValue)) {
                  this.manipulation.set(propval, this.aadl2Package.getRangeValue_Delta(), ((RangeValue) propvalinst).getDelta());
                  this.rangeValueDefinition(((RangeValue)propvalinst), ((RangeValue) propval));
                } else {
                  if ((propvalinst instanceof ComputedValue)) {
                    this.manipulation.set(propval, this.aadl2Package.getComputedValue_Function(), ((ComputedValue) propvalinst).getFunction());
                  } else {
                    if ((propvalinst instanceof NamedValue)) {
                      this.manipulation.set(propval, this.aadl2Package.getNamedValue_NamedValue(), ((NamedValue) propvalinst).getNamedValue());
                    } else {
                      if ((propvalinst instanceof InstanceReferenceValue)) {
                        final PropertyAssociationInstance propassocinst = PropertyUtils.getContainingPropertyAssociationInstance(propvalinst);
                        boolean _contains = InstanceHierarchyUtils.getAllParentInstanceObject(((InstanceReferenceValue) propvalinst).getReferencedInstanceObject()).contains(propassocinst.eContainer());
                        if (_contains) {
                          ContainmentPathElement _path = ((ContainedNamedElement) propval).getPath();
                          boolean _tripleNotEquals = (_path != null);
                          if (_tripleNotEquals) {
                            this.manipulation.remove(((ContainedNamedElement) propval).getPath());
                          }
                          EObject _eContainer_1 = propassocinst.eContainer();
                          this.appliesToPathDefinition(((ContainedNamedElement) propval), ((InstanceObject) _eContainer_1), ((InstanceReferenceValue) propvalinst).getReferencedInstanceObject());
                        } else {
                          PropertyAssociation _propertyAssociation = propassocinst.getPropertyAssociation();
                          PropertyAssociation propassoc = ((PropertyAssociation) _propertyAssociation);
                          EObject _eContainer_2 = propassocinst.eContainer();
                          InstanceObject objinst = InstanceHierarchyUtils.getLowestCommonParentInstanceObject(((InstanceReferenceValue) propvalinst).getReferencedInstanceObject(), ((InstanceObject) _eContainer_2));
                          this.manipulation.moveTo(propassoc, TraceUtils.getDeclarativeElement(objinst), this.aadl2Package.getNamedElement_OwnedPropertyAssociation());
                          propassoc.getAppliesTos().clear();
                          EObject contelem = this.manipulation.createChild(((PropertyAssociation) propassoc), this.aadl2Package.getPropertyAssociation_AppliesTo(), this.aadl2Package.getContainedNamedElement());
                          EObject _eContainer_3 = propassocinst.eContainer();
                          this.appliesToPathDefinition(((ContainedNamedElement) contelem), objinst, ((InstanceObject) _eContainer_3));
                          ContainmentPathElement _path_1 = ((ContainedNamedElement) propval).getPath();
                          boolean _tripleNotEquals_1 = (_path_1 != null);
                          if (_tripleNotEquals_1) {
                            this.manipulation.remove(((ContainedNamedElement) propval).getPath());
                          }
                          this.appliesToPathDefinition(((ContainedNamedElement) propval), objinst, ((InstanceReferenceValue) propvalinst).getReferencedInstanceObject());
                          String _name = propassoc.getProperty().getName();
                          String _plus = ("DIM: Property association of property " + _name);
                          String _plus_1 = (_plus + " moved due to change in ReferenceValue LCPCI position");
                          this.LOGGER.logInfo(_plus_1);
                        }
                      }
                    }
                  }
                }
              }
            }
          }
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }
}
