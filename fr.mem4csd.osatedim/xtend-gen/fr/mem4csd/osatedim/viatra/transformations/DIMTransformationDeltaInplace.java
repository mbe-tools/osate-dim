package fr.mem4csd.osatedim.viatra.transformations;

import com.google.common.base.Objects;
import fr.mem4csd.osatedim.preference.DIMPreferences;
import fr.mem4csd.osatedim.utils.ConnectionUtils;
import fr.mem4csd.osatedim.utils.DIMLogger;
import fr.mem4csd.osatedim.utils.FeatureUtils;
import fr.mem4csd.osatedim.utils.LibraryUtils;
import fr.mem4csd.osatedim.utils.ModeUtils;
import fr.mem4csd.osatedim.utils.PropertyUtils;
import fr.mem4csd.osatedim.viatra.queries.FindConnection;
import fr.mem4csd.osatedim.viatra.queries.FindConnectionReference;
import fr.mem4csd.osatedim.viatra.queries.FindDerivedMode;
import fr.mem4csd.osatedim.viatra.queries.FindFeature;
import fr.mem4csd.osatedim.viatra.queries.FindModalProperty;
import fr.mem4csd.osatedim.viatra.queries.FindMode;
import fr.mem4csd.osatedim.viatra.queries.FindModeTransition;
import fr.mem4csd.osatedim.viatra.queries.FindProperty;
import fr.mem4csd.osatedim.viatra.queries.FindPropertyValue;
import fr.mem4csd.osatedim.viatra.queries.FindSubcomponent;
import fr.mem4csd.osatedim.viatra.queries.FindSystem;
import java.util.List;
import java.util.function.Consumer;
import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EAttribute;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.EReference;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.transformation.evm.specific.Lifecycles;
import org.eclipse.viatra.transformation.evm.specific.crud.CRUDActivationStateEnum;
import org.eclipse.viatra.transformation.evm.specific.resolver.InvertedDisappearancePriorityConflictResolver;
import org.eclipse.viatra.transformation.runtime.emf.modelmanipulation.SimpleModelManipulations;
import org.eclipse.viatra.transformation.runtime.emf.rules.eventdriven.EventDrivenTransformationRule;
import org.eclipse.viatra.transformation.runtime.emf.transformation.eventdriven.EventDrivenTransformation;
import org.eclipse.xtext.xbase.lib.Exceptions;
import org.eclipse.xtext.xbase.lib.ExclusiveRange;
import org.eclipse.xtext.xbase.lib.Extension;
import org.eclipse.xtext.xbase.lib.IterableExtensions;
import org.eclipse.xtext.xbase.lib.ListExtensions;
import org.eclipse.xtext.xbase.lib.StringExtensions;
import org.osate.aadl2.AbstractFeature;
import org.osate.aadl2.AbstractImplementation;
import org.osate.aadl2.AbstractType;
import org.osate.aadl2.ComponentCategory;
import org.osate.aadl2.ComponentClassifier;
import org.osate.aadl2.ComponentImplementation;
import org.osate.aadl2.ComponentType;
import org.osate.aadl2.Connection;
import org.osate.aadl2.DataAccess;
import org.osate.aadl2.DataSubcomponentType;
import org.osate.aadl2.DirectedFeature;
import org.osate.aadl2.DirectionType;
import org.osate.aadl2.Element;
import org.osate.aadl2.Feature;
import org.osate.aadl2.FeatureConnection;
import org.osate.aadl2.ModalPropertyValue;
import org.osate.aadl2.Mode;
import org.osate.aadl2.ModeBinding;
import org.osate.aadl2.ModeTransition;
import org.osate.aadl2.Property;
import org.osate.aadl2.PropertyAssociation;
import org.osate.aadl2.PropertySet;
import org.osate.aadl2.PropertyValue;
import org.osate.aadl2.PublicPackageSection;
import org.osate.aadl2.Subcomponent;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.ConnectionInstance;
import org.osate.aadl2.instance.ConnectionInstanceEnd;
import org.osate.aadl2.instance.ConnectionKind;
import org.osate.aadl2.instance.ConnectionReference;
import org.osate.aadl2.instance.FeatureCategory;
import org.osate.aadl2.instance.FeatureInstance;
import org.osate.aadl2.instance.InstanceObject;
import org.osate.aadl2.instance.PropertyAssociationInstance;
import org.osate.aadl2.instance.SystemInstance;
import org.osate.aadl2.instance.SystemOperationMode;
import org.osate.aadl2.modelsupport.FileNameConstants;
import org.osate.aadl2.modelsupport.util.AadlUtil;

@SuppressWarnings("all")
public class DIMTransformationDeltaInplace extends DIMTransformationRulesDelta {
  @Extension
  protected EventDrivenTransformation transformation;

  public DIMTransformationDeltaInplace(final SystemInstance topSystemInst, final ViatraQueryEngine engine, final DIMPreferences preferences, final PropertySet dimProperties, final DIMLogger logger) {
    this.topSystemInst = topSystemInst;
    Element _owner = topSystemInst.getComponentClassifier().getOwner();
    this.aadlPublicPackage = ((PublicPackageSection) _owner);
    this.engine = engine;
    this.preferences = preferences;
    this.dimPropertySet = dimProperties;
    this.LOGGER = logger;
    this.DIMQueries.prepare(engine);
    this.createTransformation();
  }

  public void execute() {
    this.transformation.getExecutionSchema().startUnscheduledExecution();
  }

  public void dispose() {
    try {
      if ((this.transformation != null)) {
        this.transformation.getExecutionSchema().dispose();
      }
      this.topSystemInst.eResource().save(null);
      this.aadlPublicPackage.eResource().save(null);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  }

  private EventDrivenTransformation createTransformation() {
    EventDrivenTransformation _xblockexpression = null;
    {
      SimpleModelManipulations _simpleModelManipulations = new SimpleModelManipulations(this.engine);
      this.manipulation = _simpleModelManipulations;
      InvertedDisappearancePriorityConflictResolver fixedPriorityResolver = new InvertedDisappearancePriorityConflictResolver();
      fixedPriorityResolver.setPriority(this.topSystemInstance2Declarative.getRuleSpecification(), 1);
      fixedPriorityResolver.setPriority(this.componentInstance2Declarative.getRuleSpecification(), 2);
      fixedPriorityResolver.setPriority(this.featureInstance2Declarative.getRuleSpecification(), 3);
      fixedPriorityResolver.setPriority(this.connectionInstance2ConnectionReferences.getRuleSpecification(), 4);
      fixedPriorityResolver.setPriority(this.connectionReference2Declarative.getRuleSpecification(), 5);
      fixedPriorityResolver.setPriority(this.modeInstance2Declarative.getRuleSpecification(), 6);
      fixedPriorityResolver.setPriority(this.derivedModeInstance2Declarative.getRuleSpecification(), 7);
      fixedPriorityResolver.setPriority(this.modeTransitionInstance2Declarative.getRuleSpecification(), 8);
      fixedPriorityResolver.setPriority(this.propertyInstance2Declarative.getRuleSpecification(), 9);
      fixedPriorityResolver.setPriority(this.modalProperty2Declarative.getRuleSpecification(), 10);
      fixedPriorityResolver.setPriority(this.propertyval2declarative.getRuleSpecification(), 11);
      _xblockexpression = this.transformation = EventDrivenTransformation.forEngine(this.engine).setConflictResolver(fixedPriorityResolver).addRule(this.topSystemInstance2Declarative).addRule(this.componentInstance2Declarative).addRule(this.featureInstance2Declarative).addRule(this.connectionInstance2ConnectionReferences).addRule(this.connectionReference2Declarative).addRule(this.modeInstance2Declarative).addRule(this.derivedModeInstance2Declarative).addRule(this.modeTransitionInstance2Declarative).addRule(this.propertyInstance2Declarative).addRule(this.modalProperty2Declarative).addRule(this.propertyval2declarative).build();
    }
    return _xblockexpression;
  }

  private final EventDrivenTransformationRule<FindSystem.Match, FindSystem.Matcher> topSystemInstance2Declarative = this._eventDrivenTransformationRuleFactory.<FindSystem.Match, FindSystem.Matcher>createRule(FindSystem.Matcher.querySpecification()).action(CRUDActivationStateEnum.UPDATED, ((Consumer<FindSystem.Match>) (FindSystem.Match it) -> {
    try {
      final ComponentImplementation compimp = it.getSysteminst().getComponentImplementation();
      String _typeName = compimp.getTypeName();
      String _plus = (_typeName + "_");
      String _implementationName = compimp.getImplementationName();
      String _plus_1 = (_plus + _implementationName);
      String _plus_2 = (_plus_1 + FileNameConstants.INSTANCE_MODEL_POSTFIX);
      String _name = it.getSysteminst().getName();
      boolean _tripleNotEquals = (_plus_2 != _name);
      if (_tripleNotEquals) {
        if ((it.getSysteminst().getName().startsWith((compimp.getTypeName() + "_")) && it.getSysteminst().getName().endsWith(FileNameConstants.INSTANCE_MODEL_POSTFIX))) {
          EAttribute _namedElement_Name = this.aadl2Package.getNamedElement_Name();
          String _typeName_1 = compimp.getTypeName();
          String _plus_3 = (_typeName_1 + ".");
          String _name_1 = it.getSysteminst().getName();
          int _length = compimp.getTypeName().length();
          int _plus_4 = (_length + 1);
          int _length_1 = it.getSysteminst().getName().length();
          int _length_2 = FileNameConstants.INSTANCE_MODEL_POSTFIX.length();
          int _minus = (_length_1 - _length_2);
          String _substring = _name_1.substring(_plus_4, _minus);
          String _plus_5 = (_plus_3 + _substring);
          this.manipulation.set(compimp, _namedElement_Name, _plus_5);
        } else {
          String _name_2 = it.getSysteminst().getName();
          String _plus_6 = ("DIM: Updated name of top-level system instance " + _name_2);
          String _plus_7 = (_plus_6 + " is not legal");
          this.LOGGER.logWarning(_plus_7);
          SystemInstance _systeminst = it.getSysteminst();
          EAttribute _namedElement_Name_1 = this.aadl2Package.getNamedElement_Name();
          String _typeName_2 = compimp.getTypeName();
          String _plus_8 = (_typeName_2 + "_");
          String _implementationName_1 = compimp.getImplementationName();
          String _plus_9 = (_plus_8 + _implementationName_1);
          String _plus_10 = (_plus_9 + FileNameConstants.INSTANCE_MODEL_POSTFIX);
          this.manipulation.set(_systeminst, _namedElement_Name_1, _plus_10);
          String _name_3 = it.getSysteminst().getName();
          String _plus_11 = ("DIM: WARNING: Top-level system instance name reverted to " + _name_3);
          this.LOGGER.logCancel(_plus_11);
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).addLifeCycle(Lifecycles.getDefault(true, true)).build();

  private final EventDrivenTransformationRule<FindSubcomponent.Match, FindSubcomponent.Matcher> componentInstance2Declarative = this._eventDrivenTransformationRuleFactory.<FindSubcomponent.Match, FindSubcomponent.Matcher>createRule(FindSubcomponent.Matcher.querySpecification()).action(CRUDActivationStateEnum.CREATED, ((Consumer<FindSubcomponent.Match>) (FindSubcomponent.Match it) -> {
    Subcomponent _subcomponent = it.getSubcompinst().getSubcomponent();
    boolean _tripleEquals = (_subcomponent == null);
    if (_tripleEquals) {
      this.componentInstanceCreatedDIM(it.getSubcompinst());
      this.elementCreationPropertyInheritance(it.getSubcompinst());
      this.componentCreationModeInheritance(it.getSubcompinst());
      String _name = it.getSubcompinst().getName();
      String _plus = ("DIM: Component instance " + _name);
      String _plus_1 = (_plus + " de-instantiated");
      this.LOGGER.logInfo(_plus_1);
    } else {
      String _name_1 = it.getSubcompinst().getName();
      String _plus_2 = ("DIM: Declarative of component instance " + _name_1);
      String _plus_3 = (_plus_2 + " already exists!");
      this.LOGGER.logInfo(_plus_3);
    }
  })).action(CRUDActivationStateEnum.UPDATED, ((Consumer<FindSubcomponent.Match>) (FindSubcomponent.Match it) -> {
    try {
      String name = it.getSubcompinst().getName();
      boolean _isNullOrEmpty = StringExtensions.isNullOrEmpty(name);
      boolean _not = (!_isNullOrEmpty);
      if (_not) {
        Subcomponent subcomp = it.getSubcompinst().getSubcomponent();
        if ((StringExtensions.isNullOrEmpty(subcomp.getName()) || (!name.contentEquals(it.getSubcompinst().getSubcomponent().getName())))) {
          boolean _isAffectingInstanceObject = LibraryUtils.isAffectingInstanceObject(it.getSubcompinst(), it.getCompinst(), this.aadlPublicPackage, this.engine, this.preferences.isModifyReused(), true);
          if (_isAffectingInstanceObject) {
            this.denyImplementnUpdatePropagn(it.getSubcompinst(), it.getCompinst(), true);
          }
          do {
            {
              this.manipulation.set(subcomp, this.aadl2Package.getNamedElement_Name(), name);
              subcomp = subcomp.getRefined();
            }
          } while((subcomp != null));
          ComponentClassifier classifier = it.getSubcompinst().getClassifier();
          if (((classifier.getName() != null) && classifier.getName().contentEquals("null_ext"))) {
            if ((classifier instanceof ComponentType)) {
              this.manipulation.set(classifier, this.aadl2Package.getNamedElement_Name(), name);
            } else {
              this.manipulation.set(classifier, this.aadl2Package.getNamedElement_Name(), (name + ".impl"));
            }
          }
          this.LOGGER.logInfo((("DIM: Name of component instance " + name) + " updated"));
        }
      }
      ComponentCategory _category = it.getSubcompinst().getCategory();
      ComponentCategory _category_1 = it.getSubcompinst().getSubcomponent().getCategory();
      boolean _tripleNotEquals = (_category != _category_1);
      if (_tripleNotEquals) {
        ComponentCategory _category_2 = it.getSubcompinst().getSubcomponent().getCategory();
        boolean _equals = Objects.equal(_category_2, ComponentCategory.ABSTRACT);
        if (_equals) {
          ComponentImplementation newcompimp = this.extendParentImplementationForRefinement(it.getSubcompinst());
          Subcomponent newsubcomp = this.createSubcomponentInsideComponentImplementation(newcompimp, it.getSubcompinst());
          this.manipulation.set(newsubcomp, this.aadl2Package.getNamedElement_Name(), it.getSubcompinst().getSubcomponent().getName());
          this.manipulation.set(newsubcomp, this.aadl2Package.getSubcomponent_Refined(), it.getSubcompinst().getSubcomponent());
          ComponentClassifier _xifexpression = null;
          ComponentClassifier _classifier = it.getSubcompinst().getClassifier();
          if ((_classifier instanceof ComponentType)) {
            _xifexpression = this.createComponentType(it.getSubcompinst().getCategory());
          } else {
            _xifexpression = this.createComponentImplementation(it.getSubcompinst().getCategory());
          }
          ComponentClassifier newsubcompclass = ((ComponentClassifier) _xifexpression);
          this.classifierCreationDIMPropertyAddition(newsubcompclass);
          EAttribute _namedElement_Name = this.aadl2Package.getNamedElement_Name();
          String _name = it.getSubcompinst().getClassifier().getName();
          String _plus = (_name + "_ext");
          this.manipulation.set(newsubcompclass, _namedElement_Name, _plus);
          ComponentClassifier _classifier_1 = it.getSubcompinst().getClassifier();
          if ((_classifier_1 instanceof AbstractType)) {
            this.manipulation.set(newsubcompclass, this.aadl2Package.getComponentType_Extended(), it.getSubcompinst().getClassifier());
          } else {
            ComponentClassifier _classifier_2 = it.getSubcompinst().getClassifier();
            if ((_classifier_2 instanceof AbstractImplementation)) {
              this.manipulation.set(newsubcompclass, this.aadl2Package.getComponentImplementation_Extended(), it.getSubcompinst().getClassifier());
              ComponentClassifier _classifier_3 = it.getSubcompinst().getClassifier();
              this.manipulation.set(newsubcompclass, this.aadl2Package.getComponentImplementation_Type(), ((ComponentImplementation) _classifier_3).getType());
            }
          }
          this.manipulation.set(it.getSubcompinst(), this.instPackage.getComponentInstance_Classifier(), newsubcompclass);
          this.manipulation.set(it.getSubcompinst(), this.instPackage.getComponentInstance_Subcomponent(), newsubcomp);
          this.setSubcomponentType(it.getSubcompinst(), newsubcompclass);
          String _name_1 = it.getSubcompinst().getName();
          String _plus_1 = ("DIM: Category of component instance " + _name_1);
          String _plus_2 = (_plus_1 + " refined");
          this.LOGGER.logInfo(_plus_2);
        } else {
          this.manipulation.set(it.getSubcompinst(), this.instPackage.getComponentInstance_Category(), it.getSubcompinst().getSubcomponent().getCategory());
          String _name_2 = it.getSubcompinst().getName();
          String _plus_3 = ("DIM: Category update of component instance " + _name_2);
          String _plus_4 = (_plus_3 + " not refine-able! Reverted back to original");
          this.LOGGER.logWarning(_plus_4);
        }
      }
      ComponentClassifier _classifier_4 = it.getSubcompinst().getClassifier();
      ComponentClassifier _classifier_5 = it.getSubcompinst().getSubcomponent().getClassifier();
      boolean _tripleNotEquals_1 = (_classifier_4 != _classifier_5);
      if (_tripleNotEquals_1) {
        boolean _isClassifierRefinableFrom = LibraryUtils.isClassifierRefinableFrom(it.getSubcompinst().getClassifier(), it.getSubcompinst().getSubcomponent().getClassifier());
        if (_isClassifierRefinableFrom) {
          ComponentCategory _category_3 = it.getSubcompinst().getCategory();
          ComponentCategory _category_4 = it.getSubcompinst().getClassifier().getCategory();
          boolean _tripleNotEquals_2 = (_category_3 != _category_4);
          if (_tripleNotEquals_2) {
            this.manipulation.set(it.getSubcompinst(), this.instPackage.getComponentInstance_Category(), it.getSubcompinst().getClassifier().getCategory());
          }
          ComponentImplementation newcompimp_1 = this.extendParentImplementationForRefinement(it.getSubcompinst());
          Subcomponent newsubcomp_1 = this.createSubcomponentInsideComponentImplementation(newcompimp_1, it.getSubcompinst());
          this.manipulation.set(newsubcomp_1, this.aadl2Package.getNamedElement_Name(), it.getSubcompinst().getSubcomponent().getName());
          this.manipulation.set(newsubcomp_1, this.aadl2Package.getSubcomponent_Refined(), it.getSubcompinst().getSubcomponent());
          this.manipulation.set(it.getSubcompinst(), this.instPackage.getComponentInstance_Subcomponent(), newsubcomp_1);
          this.setSubcomponentType(it.getSubcompinst(), it.getSubcompinst().getClassifier());
          String _name_3 = it.getSubcompinst().getName();
          String _plus_5 = ("DIM: Classifier of component instance " + _name_3);
          String _plus_6 = (_plus_5 + " refined");
          this.LOGGER.logInfo(_plus_6);
        } else {
          this.manipulation.set(it.getSubcompinst(), this.instPackage.getComponentInstance_Classifier(), it.getSubcompinst().getSubcomponent().getClassifier());
          String _name_4 = it.getSubcompinst().getName();
          String _plus_7 = ("DIM: Classifier update of component instance " + _name_4);
          String _plus_8 = (_plus_7 + " not refine-able! Reverted back to original");
          this.LOGGER.logWarning(_plus_8);
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).action(CRUDActivationStateEnum.DELETED, ((Consumer<FindSubcomponent.Match>) (FindSubcomponent.Match it) -> {
    boolean _isAffectingInstanceObject = LibraryUtils.isAffectingInstanceObject(it.getSubcompinst(), it.getCompinst(), this.aadlPublicPackage, this.engine, this.preferences.isModifyReused(), true);
    if (_isAffectingInstanceObject) {
      EObject _eContainer = it.getSubcompinst().eContainer();
      this.denyImplementnUpdatePropagn(it.getSubcompinst(), ((ComponentInstance) _eContainer), true);
    }
    Subcomponent subcomp = it.getSubcompinst().getSubcomponent();
    do {
      {
        this.componentInstanceDeletedDIM(subcomp);
        subcomp = subcomp.getRefined();
      }
    } while((subcomp != null));
    String _name = it.getSubcompinst().getName();
    String _plus = ("DIM: Component instance " + _name);
    String _plus_1 = (_plus + " deleted");
    this.LOGGER.logInfo(_plus_1);
  })).addLifeCycle(Lifecycles.getDefault(true, true)).build();

  private final EventDrivenTransformationRule<FindFeature.Match, FindFeature.Matcher> featureInstance2Declarative = this._eventDrivenTransformationRuleFactory.<FindFeature.Match, FindFeature.Matcher>createRule(FindFeature.Matcher.querySpecification()).action(CRUDActivationStateEnum.CREATED, ((Consumer<FindFeature.Match>) (FindFeature.Match it) -> {
    Feature _feature = it.getFeatinst().getFeature();
    boolean _tripleEquals = (_feature == null);
    if (_tripleEquals) {
      this.featureInstanceCreatedDIM(it.getFeatinst());
      this.elementCreationPropertyInheritance(it.getFeatinst());
      String _name = it.getFeatinst().getName();
      String _plus = ("DIM: Feature instance " + _name);
      String _plus_1 = (_plus + " de-instantiated");
      this.LOGGER.logInfo(_plus_1);
    } else {
      String _name_1 = it.getFeatinst().getName();
      String _plus_2 = ("DIM: Declarative of feature instance " + _name_1);
      String _plus_3 = (_plus_2 + " already exists!");
      this.LOGGER.logInfo(_plus_3);
    }
  })).action(CRUDActivationStateEnum.UPDATED, ((Consumer<FindFeature.Match>) (FindFeature.Match it) -> {
    try {
      String _name = it.getFeatinst().getFeature().getName();
      String _name_1 = it.getFeatinst().getName();
      boolean _tripleNotEquals = (_name != _name_1);
      if (_tripleNotEquals) {
        this.denyFeatureUpdatePropagn(it.getFeatinst());
        Feature feat = it.getFeatinst().getFeature();
        do {
          {
            this.manipulation.set(feat, this.aadl2Package.getNamedElement_Name(), it.getFeatinst().getName());
            feat = feat.getRefined();
          }
        } while((feat != null));
        String _name_2 = it.getFeatinst().getName();
        String _plus = ("DIM: Name of feature instance " + _name_2);
        String _plus_1 = (_plus + " updated");
        this.LOGGER.logInfo(_plus_1);
      }
      Feature _feature = it.getFeatinst().getFeature();
      if ((_feature instanceof DirectedFeature)) {
        DirectionType _direction = it.getFeatinst().getDirection();
        boolean _tripleNotEquals_1 = (_direction != null);
        if (_tripleNotEquals_1) {
          if ((((Objects.equal(it.getFeatinst().getDirection(), DirectionType.IN) && (!(((DirectedFeature) it.getFeatinst().getFeature()).isIn() && (!((DirectedFeature) it.getFeatinst().getFeature()).isOut())))) || (Objects.equal(it.getFeatinst().getDirection(), DirectionType.OUT) && (!((!((DirectedFeature) it.getFeatinst().getFeature()).isIn()) && ((DirectedFeature) it.getFeatinst().getFeature()).isOut())))) || (Objects.equal(it.getFeatinst().getDirection(), DirectionType.IN_OUT) && (!(((DirectedFeature) it.getFeatinst().getFeature()).isIn() && ((DirectedFeature) it.getFeatinst().getFeature()).isOut()))))) {
            EObject _eContainer = it.getFeatinst().eContainer();
            ComponentInstance compinst = ((ComponentInstance) _eContainer);
            Feature _feature_1 = it.getFeatinst().getFeature();
            Boolean _isRefinedFeatureDirection = FeatureUtils.isRefinedFeatureDirection(it.getFeatinst().getDirection(), FeatureUtils.getFeatureDirection(((DirectedFeature) _feature_1)));
            if ((_isRefinedFeatureDirection).booleanValue()) {
              if (((compinst != this.topSystemInst) && LibraryUtils.isAffectingInstanceObject(compinst, ((ComponentInstance) compinst.eContainer()), this.aadlPublicPackage, this.engine, this.preferences.isModifyReused(), false))) {
                EObject _eContainer_1 = compinst.eContainer();
                this.denyImplementnUpdatePropagn(compinst, ((ComponentInstance) _eContainer_1), false);
              }
              ComponentClassifier _switchResult = null;
              ComponentClassifier _classifier = compinst.getClassifier();
              boolean _matched = false;
              if (Objects.equal(_classifier, ComponentType.class)) {
                _matched=true;
                _switchResult = compinst.getClassifier();
              }
              if (!_matched) {
                if (Objects.equal(_classifier, ComponentImplementation.class)) {
                  _matched=true;
                  ComponentClassifier _classifier_1 = compinst.getClassifier();
                  _switchResult = ((ComponentImplementation) _classifier_1).getType();
                }
              }
              final ComponentClassifier comptype = _switchResult;
              final ComponentType reftype = this.createComponentType(compinst.getCategory());
              EAttribute _namedElement_Name = this.aadl2Package.getNamedElement_Name();
              String _name_3 = comptype.getName();
              String _plus_2 = (_name_3 + "_ext");
              this.manipulation.set(reftype, _namedElement_Name, _plus_2);
              this.manipulation.set(reftype, this.aadl2Package.getComponentType_Extended(), comptype);
              EObject reffeat = this.createFeature(compinst, reftype, it.getFeatinst());
              this.manipulation.set(reffeat, this.aadl2Package.getNamedElement_Name(), it.getFeatinst().getFeature().getName());
              this.manipulation.set(reffeat, this.aadl2Package.getFeature_Refined(), it.getFeatinst().getFeature());
              DirectionType _direction_1 = it.getFeatinst().getDirection();
              boolean _equals = Objects.equal(_direction_1, DirectionType.IN);
              if (_equals) {
                this.manipulation.set(reffeat, this.aadl2Package.getDirectedFeature_In(), Boolean.valueOf(true));
                this.manipulation.set(reffeat, this.aadl2Package.getDirectedFeature_Out(), Boolean.valueOf(false));
              } else {
                DirectionType _direction_2 = it.getFeatinst().getDirection();
                boolean _equals_1 = Objects.equal(_direction_2, DirectionType.OUT);
                if (_equals_1) {
                  this.manipulation.set(reffeat, this.aadl2Package.getDirectedFeature_In(), Boolean.valueOf(false));
                  this.manipulation.set(reffeat, this.aadl2Package.getDirectedFeature_Out(), Boolean.valueOf(true));
                } else {
                  this.manipulation.set(reffeat, this.aadl2Package.getDirectedFeature_In(), Boolean.valueOf(true));
                  this.manipulation.set(reffeat, this.aadl2Package.getDirectedFeature_Out(), Boolean.valueOf(true));
                }
              }
              this.manipulation.set(it.getFeatinst(), this.instPackage.getFeatureInstance_Feature(), reftype);
              ComponentClassifier _classifier_2 = compinst.getClassifier();
              boolean _matched_1 = false;
              if (Objects.equal(_classifier_2, ComponentType.class)) {
                _matched_1=true;
                this.manipulation.set(compinst, this.instPackage.getComponentInstance_Classifier(), reftype);
                this.setSubcomponentType(compinst, reftype);
              }
              if (!_matched_1) {
                if (Objects.equal(_classifier_2, ComponentImplementation.class)) {
                  _matched_1=true;
                  this.manipulation.set(compinst.getClassifier(), this.aadl2Package.getComponentImplementation_Type(), reftype);
                }
              }
              String _name_4 = it.getFeatinst().getName();
              String _plus_3 = ("DIM: Direction of feature instance " + _name_4);
              String _plus_4 = (_plus_3 + " refined");
              this.LOGGER.logInfo(_plus_4);
            } else {
              this.denyFeatureUpdatePropagn(it.getFeatinst());
              Feature currfeat = it.getFeatinst().getFeature();
              do {
                {
                  DirectionType _direction_3 = it.getFeatinst().getDirection();
                  boolean _equals_2 = Objects.equal(_direction_3, DirectionType.IN);
                  if (_equals_2) {
                    this.manipulation.set(currfeat, this.aadl2Package.getDirectedFeature_In(), Boolean.valueOf(true));
                    this.manipulation.set(currfeat, this.aadl2Package.getDirectedFeature_Out(), Boolean.valueOf(false));
                  } else {
                    DirectionType _direction_4 = it.getFeatinst().getDirection();
                    boolean _equals_3 = Objects.equal(_direction_4, DirectionType.OUT);
                    if (_equals_3) {
                      this.manipulation.set(currfeat, this.aadl2Package.getDirectedFeature_In(), Boolean.valueOf(false));
                      this.manipulation.set(currfeat, this.aadl2Package.getDirectedFeature_Out(), Boolean.valueOf(true));
                    } else {
                      this.manipulation.set(currfeat, this.aadl2Package.getDirectedFeature_In(), Boolean.valueOf(true));
                      this.manipulation.set(currfeat, this.aadl2Package.getDirectedFeature_Out(), Boolean.valueOf(true));
                    }
                  }
                  currfeat = currfeat.getRefined();
                }
              } while(((currfeat != null) && (!(FeatureUtils.isRefinedFeatureDirection(it.getFeatinst().getDirection(), FeatureUtils.getFeatureDirection(((DirectedFeature) currfeat)))).booleanValue())));
              String _name_5 = it.getFeatinst().getName();
              String _plus_5 = ("DIM: Direction of feature instance " + _name_5);
              String _plus_6 = (_plus_5 + " updated");
              this.LOGGER.logInfo(_plus_6);
            }
          }
        }
      }
      FeatureCategory _category = it.getFeatinst().getCategory();
      FeatureCategory _featureCategory = FeatureUtils.getFeatureCategory(it.getFeatinst().getFeature());
      boolean _tripleNotEquals_2 = (_category != _featureCategory);
      if (_tripleNotEquals_2) {
        EObject _eContainer_2 = it.getFeatinst().eContainer();
        ComponentInstance compinst_1 = ((ComponentInstance) _eContainer_2);
        Feature _feature_2 = it.getFeatinst().getFeature();
        if ((_feature_2 instanceof AbstractFeature)) {
          if (((compinst_1 != this.topSystemInst) && LibraryUtils.isAffectingInstanceObject(compinst_1, ((ComponentInstance) compinst_1.eContainer()), this.aadlPublicPackage, this.engine, this.preferences.isModifyReused(), false))) {
            EObject _eContainer_3 = compinst_1.eContainer();
            this.denyImplementnUpdatePropagn(compinst_1, ((ComponentInstance) _eContainer_3), false);
          }
          ComponentClassifier _switchResult_2 = null;
          ComponentClassifier _classifier_3 = compinst_1.getClassifier();
          boolean _matched_2 = false;
          if (Objects.equal(_classifier_3, ComponentType.class)) {
            _matched_2=true;
            _switchResult_2 = compinst_1.getClassifier();
          }
          if (!_matched_2) {
            if (Objects.equal(_classifier_3, ComponentImplementation.class)) {
              _matched_2=true;
              ComponentClassifier _classifier_4 = compinst_1.getClassifier();
              _switchResult_2 = ((ComponentImplementation) _classifier_4).getType();
            }
          }
          final ComponentClassifier comptype_1 = _switchResult_2;
          final ComponentType reftype_1 = this.createComponentType(compinst_1.getCategory());
          EAttribute _namedElement_Name_1 = this.aadl2Package.getNamedElement_Name();
          String _name_6 = comptype_1.getName();
          String _plus_7 = (_name_6 + "_ext");
          this.manipulation.set(reftype_1, _namedElement_Name_1, _plus_7);
          this.manipulation.set(reftype_1, this.aadl2Package.getComponentType_Extended(), comptype_1);
          EObject reffeat_1 = this.createFeature(compinst_1, reftype_1, it.getFeatinst());
          this.manipulation.set(reffeat_1, this.aadl2Package.getNamedElement_Name(), it.getFeatinst().getFeature().getName());
          this.manipulation.set(reffeat_1, this.aadl2Package.getFeature_Refined(), it.getFeatinst().getFeature());
          Feature _feature_3 = it.getFeatinst().getFeature();
          this.manipulation.set(reffeat_1, this.aadl2Package.getDirectedFeature_In(), Boolean.valueOf(((DirectedFeature) _feature_3).isIn()));
          Feature _feature_4 = it.getFeatinst().getFeature();
          this.manipulation.set(reffeat_1, this.aadl2Package.getDirectedFeature_Out(), Boolean.valueOf(((DirectedFeature) _feature_4).isOut()));
          this.manipulation.set(it.getFeatinst(), this.instPackage.getFeatureInstance_Feature(), reftype_1);
          ComponentClassifier _classifier_5 = compinst_1.getClassifier();
          boolean _matched_3 = false;
          if (Objects.equal(_classifier_5, ComponentType.class)) {
            _matched_3=true;
            this.manipulation.set(compinst_1, this.instPackage.getComponentInstance_Classifier(), reftype_1);
            this.setSubcomponentType(compinst_1, reftype_1);
          }
          if (!_matched_3) {
            if (Objects.equal(_classifier_5, ComponentImplementation.class)) {
              _matched_3=true;
              this.manipulation.set(compinst_1.getClassifier(), this.aadl2Package.getComponentImplementation_Type(), reftype_1);
            }
          }
          String _name_7 = it.getFeatinst().getName();
          String _plus_8 = ("DIM: Category of feature instance " + _name_7);
          String _plus_9 = (_plus_8 + " refined");
          this.LOGGER.logInfo(_plus_9);
        } else {
          this.denyFeatureUpdatePropagn(it.getFeatinst());
          Feature newrefineefeat = it.getFeatinst().getFeature();
          List<Feature> _reverse = ListExtensions.<Feature>reverse(it.getFeatinst().getFeature().getAllFeatureRefinements());
          for (final Feature oldfeat : _reverse) {
            {
              EObject _eContainer_4 = oldfeat.eContainer();
              final ComponentType feattype = ((ComponentType) _eContainer_4);
              EObject newfeat = this.createFeature(compinst_1, feattype, it.getFeatinst());
              if (((oldfeat instanceof DirectedFeature) && (newfeat instanceof DirectedFeature))) {
                this.manipulation.set(newfeat, this.aadl2Package.getDirectedFeature_In(), Boolean.valueOf(((DirectedFeature) oldfeat).isIn()));
                this.manipulation.set(newfeat, this.aadl2Package.getDirectedFeature_Out(), Boolean.valueOf(((DirectedFeature) oldfeat).isOut()));
              }
              EList<PropertyAssociation> _ownedPropertyAssociations = oldfeat.getOwnedPropertyAssociations();
              for (final PropertyAssociation propassoc : _ownedPropertyAssociations) {
                this.manipulation.moveTo(propassoc, ((Resource) newfeat));
              }
              Feature _feature_5 = it.getFeatinst().getFeature();
              boolean _equals_2 = Objects.equal(oldfeat, _feature_5);
              if (_equals_2) {
                this.manipulation.set(it.getFeatinst(), this.instPackage.getFeatureInstance_Feature(), newfeat);
              } else {
                this.manipulation.set(newfeat, this.aadl2Package.getFeature_Refined(), newrefineefeat);
                newrefineefeat = ((Feature) newfeat);
              }
              this.featureInstanceDeletedDIM(oldfeat);
            }
          }
          String _name_8 = it.getFeatinst().getName();
          String _plus_10 = ("DIM: Category of feature instance " + _name_8);
          String _plus_11 = (_plus_10 + " updated");
          this.LOGGER.logInfo(_plus_11);
        }
      }
      if ((Objects.equal(it.getFeatinst().getCategory(), FeatureCategory.DATA_ACCESS) && (it.getFeatinst().getType() != null))) {
        ComponentClassifier _classifier_6 = it.getFeatinst().getType().getClassifier();
        Feature _feature_5 = it.getFeatinst().getFeature();
        DataSubcomponentType _dataFeatureClassifier = ((DataAccess) _feature_5).getDataFeatureClassifier();
        boolean _tripleNotEquals_3 = (_classifier_6 != _dataFeatureClassifier);
        if (_tripleNotEquals_3) {
          this.denyFeatureUpdatePropagn(it.getFeatinst());
          Feature feat_1 = it.getFeatinst().getFeature();
          do {
            {
              this.manipulation.set(((DataAccess) feat_1), this.aadl2Package.getDataAccess_DataFeatureClassifier(), it.getFeatinst().getType().getClassifier());
              feat_1 = feat_1.getRefined();
            }
          } while(((feat_1 != null) && (feat_1 instanceof DataAccess)));
          String _name_9 = it.getFeatinst().getName();
          String _plus_12 = ("DIM: Classifier of dataAccess feature instance " + _name_9);
          String _plus_13 = (_plus_12 + " updated");
          this.LOGGER.logInfo(_plus_13);
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).action(CRUDActivationStateEnum.DELETED, ((Consumer<FindFeature.Match>) (FindFeature.Match it) -> {
    this.denyFeatureUpdatePropagn(it.getFeatinst());
    Feature feat = it.getFeatinst().getFeature();
    do {
      {
        this.featureInstanceDeletedDIM(feat);
        feat = feat.getRefined();
      }
    } while((feat != null));
    String _name = it.getFeatinst().getName();
    String _plus = ("DIM: Feature instance " + _name);
    String _plus_1 = (_plus + " deleted");
    this.LOGGER.logInfo(_plus_1);
  })).addLifeCycle(Lifecycles.getDefault(true, true)).build();

  private final EventDrivenTransformationRule<FindConnection.Match, FindConnection.Matcher> connectionInstance2ConnectionReferences = this._eventDrivenTransformationRuleFactory.<FindConnection.Match, FindConnection.Matcher>createRule(FindConnection.Matcher.querySpecification()).action(CRUDActivationStateEnum.CREATED, ((Consumer<FindConnection.Match>) (FindConnection.Match it) -> {
    this.connectionInstanceCreatedDIM(it.getConninst());
    this.elementCreationPropertyInheritance(it.getConninst());
    this.connectionCreationModeInheritance(it.getConninst());
  })).action(CRUDActivationStateEnum.UPDATED, ((Consumer<FindConnection.Match>) (FindConnection.Match it) -> {
    try {
      ConnectionInstanceEnd _destination = it.getConninst().getDestination();
      ConnectionInstanceEnd _destination_1 = IterableExtensions.<ConnectionReference>last(it.getConninst().getConnectionReferences()).getDestination();
      boolean _tripleNotEquals = (_destination != _destination_1);
      if (_tripleNotEquals) {
        this.connectionInstanceCreatedDIM(it.getConninst());
        String _name = it.getConninst().getName();
        String _plus = ("DIM: Destination of Connection Instance " + _name);
        String _plus_1 = (_plus + " updated");
        this.LOGGER.logInfo(_plus_1);
      } else {
        ConnectionInstanceEnd _source = it.getConninst().getSource();
        ConnectionInstanceEnd _source_1 = it.getConninst().getConnectionReferences().get(0).getSource();
        boolean _tripleNotEquals_1 = (_source != _source_1);
        if (_tripleNotEquals_1) {
          EList<ComponentInstance> complist = ConnectionUtils.getPathBetweenConnectionInstanceEnds(it.getConninst().getDestination(), it.getConninst().getSource());
          boolean _isNullOrEmpty = IterableExtensions.isNullOrEmpty(it.getConninst().getConnectionReferences());
          if (_isNullOrEmpty) {
            this.manipulation.createChild(it.getConninst(), this.instPackage.getConnectionInstance_ConnectionReference(), this.instPackage.getConnectionReference());
          }
          List<ConnectionReference> connreflist = ListExtensions.<ConnectionReference>reverse(it.getConninst().getConnectionReferences());
          Integer _max = this.max(Integer.valueOf(connreflist.size()), Integer.valueOf(complist.size()));
          ExclusiveRange _doubleDotLessThan = new ExclusiveRange(0, (_max).intValue(), true);
          for (final Integer i : _doubleDotLessThan) {
            {
              if ((((i).intValue() >= complist.size()) && (connreflist.get((i).intValue()) != null))) {
                this.manipulation.remove(it.getConninst(), this.instPackage.getConnectionInstance_ConnectionReference(), connreflist.get((i).intValue()));
              } else {
                EObject _xifexpression = null;
                ConnectionReference _get = connreflist.get((i).intValue());
                boolean _tripleNotEquals_2 = (_get != null);
                if (_tripleNotEquals_2) {
                  _xifexpression = connreflist.get((i).intValue());
                } else {
                  EObject _xblockexpression = null;
                  {
                    EObject connreflocal = this.manipulation.createChild(it.getConninst(), this.instPackage.getConnectionInstance_ConnectionReference(), this.instPackage.getConnectionReference());
                    ConnectionInstance _conninst = it.getConninst();
                    EReference _connectionInstance_ConnectionReference = this.instPackage.getConnectionInstance_ConnectionReference();
                    int _size = it.getConninst().getConnectionReferences().size();
                    int _minus = (_size - 1);
                    this.manipulation.changeIndex(_conninst, _connectionInstance_ConnectionReference, _minus, 0);
                    _xblockexpression = connreflocal;
                  }
                  _xifexpression = _xblockexpression;
                }
                ConnectionReference connref = ((ConnectionReference) _xifexpression);
                ComponentInstance _context = connref.getContext();
                boolean _tripleEquals = (_context == null);
                if (_tripleEquals) {
                  this.manipulation.set(connref, this.instPackage.getConnectionReference_Context(), complist.get((i).intValue()));
                  String _string = i.toString();
                  String _plus_2 = ("DIM: Context of the " + _string);
                  String _plus_3 = (_plus_2 + "th (reverse) connection reference for connection ");
                  String _name_1 = it.getConninst().getName();
                  String _plus_4 = (_plus_3 + _name_1);
                  String _plus_5 = (_plus_4 + " added");
                  this.LOGGER.logInfo(_plus_5);
                } else {
                  ComponentInstance _context_1 = connref.getContext();
                  ComponentInstance _get_1 = complist.get((i).intValue());
                  boolean _tripleNotEquals_3 = (_context_1 != _get_1);
                  if (_tripleNotEquals_3) {
                    this.manipulation.set(connref, this.instPackage.getConnectionReference_Context(), complist.get((i).intValue()));
                    String _string_1 = i.toString();
                    String _plus_6 = ("DIM: Context of the " + _string_1);
                    String _plus_7 = (_plus_6 + "th (reverse) connection reference for connection ");
                    String _name_2 = it.getConninst().getName();
                    String _plus_8 = (_plus_7 + _name_2);
                    String _plus_9 = (_plus_8 + " changed");
                    this.LOGGER.logInfo(_plus_9);
                    this.manipulation.set(connref, this.instPackage.getConnectionReference_Source(), null);
                    this.manipulation.set(connref, this.instPackage.getConnectionReference_Destination(), null);
                  }
                }
                ConnectionInstanceEnd _destination_2 = connref.getDestination();
                boolean _tripleEquals_1 = (_destination_2 == null);
                if (_tripleEquals_1) {
                  if (((i).intValue() == 0)) {
                    this.manipulation.set(connref, this.instPackage.getConnectionReference_Destination(), it.getConninst().getDestination());
                    String _string_2 = i.toString();
                    String _plus_10 = ("DIM: Destination of the " + _string_2);
                    String _plus_11 = (_plus_10 + "th (reverse) connection reference for connection ");
                    String _name_3 = it.getConninst().getName();
                    String _plus_12 = (_plus_11 + _name_3);
                    String _plus_13 = (_plus_12 + " added");
                    this.LOGGER.logInfo(_plus_13);
                  } else {
                    EList<ConnectionReference> _connectionReferences = it.getConninst().getConnectionReferences();
                    int _indexOf = it.getConninst().getConnectionReferences().indexOf(connref);
                    int _plus_14 = (_indexOf + 1);
                    ConnectionInstanceEnd _source_2 = _connectionReferences.get(_plus_14).getSource();
                    boolean _tripleNotEquals_4 = (_source_2 != null);
                    if (_tripleNotEquals_4) {
                      EList<ConnectionReference> _connectionReferences_1 = it.getConninst().getConnectionReferences();
                      int _indexOf_1 = it.getConninst().getConnectionReferences().indexOf(connref);
                      int _plus_15 = (_indexOf_1 + 1);
                      this.manipulation.set(connref, this.instPackage.getConnectionReference_Destination(), _connectionReferences_1.get(_plus_15).getSource());
                      String _string_3 = i.toString();
                      String _plus_16 = ("DIM: Destination of the " + _string_3);
                      String _plus_17 = (_plus_16 + "th (reverse) connection reference for connection ");
                      String _name_4 = it.getConninst().getName();
                      String _plus_18 = (_plus_17 + _name_4);
                      String _plus_19 = (_plus_18 + " added");
                      this.LOGGER.logInfo(_plus_19);
                    }
                  }
                } else {
                  if ((((i).intValue() == 0) && (connref.getDestination() != it.getConninst().getDestination()))) {
                    this.manipulation.set(connref, this.instPackage.getConnectionReference_Destination(), it.getConninst().getDestination());
                    String _string_4 = i.toString();
                    String _plus_20 = ("DIM: Destination of the " + _string_4);
                    String _plus_21 = (_plus_20 + "th (reverse) connection reference for connection ");
                    String _name_5 = it.getConninst().getName();
                    String _plus_22 = (_plus_21 + _name_5);
                    String _plus_23 = (_plus_22 + " changed");
                    this.LOGGER.logWarning(_plus_23);
                  } else {
                    if (((((i).intValue() != 0) && (it.getConninst().getConnectionReferences().get((it.getConninst().getConnectionReferences().indexOf(connref) + 1)).getSource() != null)) && (connref.getSource() != it.getConninst().getConnectionReferences().get((it.getConninst().getConnectionReferences().indexOf(connref) + 1)).getSource()))) {
                      EList<ConnectionReference> _connectionReferences_2 = it.getConninst().getConnectionReferences();
                      int _indexOf_2 = it.getConninst().getConnectionReferences().indexOf(connref);
                      int _plus_24 = (_indexOf_2 + 1);
                      this.manipulation.set(connref, this.instPackage.getConnectionReference_Destination(), _connectionReferences_2.get(_plus_24).getSource());
                      String _string_5 = i.toString();
                      String _plus_25 = ("DIM: Destination of the " + _string_5);
                      String _plus_26 = (_plus_25 + "th (reverse) connection reference for connection ");
                      String _name_6 = it.getConninst().getName();
                      String _plus_27 = (_plus_26 + _name_6);
                      String _plus_28 = (_plus_27 + " changed");
                      this.LOGGER.logWarning(_plus_28);
                    }
                  }
                }
                ConnectionInstanceEnd _source_3 = connref.getSource();
                boolean _tripleEquals_2 = (_source_3 == null);
                if (_tripleEquals_2) {
                  int _size = complist.size();
                  int _minus = (_size - 1);
                  boolean _equals = ((i).intValue() == _minus);
                  if (_equals) {
                    this.manipulation.set(connref, this.instPackage.getConnectionReference_Source(), it.getConninst().getSource());
                    String _string_6 = i.toString();
                    String _plus_29 = ("DIM: Source of the " + _string_6);
                    String _plus_30 = (_plus_29 + "th connection reference for connection ");
                    String _name_7 = it.getConninst().getName();
                    String _plus_31 = (_plus_30 + _name_7);
                    String _plus_32 = (_plus_31 + " added");
                    this.LOGGER.logInfo(_plus_32);
                  } else {
                    boolean _isCreateInterFeatures = this.preferences.isCreateInterFeatures();
                    if (_isCreateInterFeatures) {
                      Object _xifexpression_1 = null;
                      EObject _eContainer = complist.get(((i).intValue() + 1)).eContainer();
                      ComponentInstance _get_2 = complist.get((i).intValue());
                      boolean _equals_1 = Objects.equal(_eContainer, _get_2);
                      if (_equals_1) {
                        _xifexpression_1 = complist.get(((i).intValue() + 1));
                      } else {
                        _xifexpression_1 = complist;
                      }
                      FeatureInstance feataddcompinst = ((FeatureInstance) _xifexpression_1);
                      EObject _createChild = this.manipulation.createChild(feataddcompinst, this.instPackage.getComponentInstance_FeatureInstance(), this.instPackage.getFeatureInstance());
                      FeatureInstance srcfeatinst = ((FeatureInstance) _createChild);
                      ConnectionInstanceEnd _source_4 = it.getConninst().getSource();
                      if ((_source_4 instanceof FeatureInstance)) {
                        ConnectionInstanceEnd _source_5 = it.getConninst().getSource();
                        this.manipulation.set(srcfeatinst, this.instPackage.getFeatureInstance_Category(), ((FeatureInstance) _source_5).getCategory());
                      } else {
                        ConnectionInstanceEnd _destination_3 = it.getConninst().getDestination();
                        this.manipulation.set(srcfeatinst, this.instPackage.getFeatureInstance_Category(), ((FeatureInstance) _destination_3).getCategory());
                      }
                      boolean _isBidirectional = it.getConninst().isBidirectional();
                      boolean _equals_2 = (_isBidirectional == true);
                      if (_equals_2) {
                        this.manipulation.set(srcfeatinst, this.instPackage.getFeatureInstance_Direction(), DirectionType.IN_OUT);
                      } else {
                        EObject _eContainer_1 = complist.get(((i).intValue() + 1)).eContainer();
                        ComponentInstance _get_3 = complist.get((i).intValue());
                        boolean _equals_3 = Objects.equal(_eContainer_1, _get_3);
                        if (_equals_3) {
                          this.manipulation.set(srcfeatinst, this.instPackage.getFeatureInstance_Direction(), DirectionType.IN);
                        } else {
                          this.manipulation.set(srcfeatinst, this.instPackage.getFeatureInstance_Direction(), DirectionType.OUT);
                        }
                      }
                      EAttribute _namedElement_Name = this.aadl2Package.getNamedElement_Name();
                      String _name_8 = it.getConninst().getName();
                      String _plus_33 = (_name_8 + "_feat");
                      this.manipulation.set(srcfeatinst, _namedElement_Name, _plus_33);
                      this.manipulation.set(connref, this.instPackage.getConnectionReference_Source(), srcfeatinst);
                      String _string_7 = i.toString();
                      String _plus_34 = ("DIM: Source of the " + _string_7);
                      String _plus_35 = (_plus_34 + "th connection reference for connection ");
                      String _name_9 = it.getConninst().getName();
                      String _plus_36 = (_plus_35 + _name_9);
                      String _plus_37 = (_plus_36 + " created");
                      this.LOGGER.logInfo(_plus_37);
                    }
                  }
                } else {
                  if ((((i).intValue() == (complist.size() - 1)) && (connref.getSource() != it.getConninst().getSource()))) {
                    this.manipulation.set(connref, this.instPackage.getConnectionReference_Source(), it.getConninst().getSource());
                    this.LOGGER.logWarning("DIM: Destination of the connection reference changed");
                  }
                }
              }
              String _name_10 = it.getConninst().getName();
              String _plus_38 = ("DIM: Connection Instance " + _name_10);
              String _plus_39 = (_plus_38 + " transformed to connection reference");
              this.LOGGER.logInfo(_plus_39);
            }
          }
          String _name_1 = it.getConninst().getName();
          String _plus_2 = ("DIM: Source of Connection Instance " + _name_1);
          String _plus_3 = (_plus_2 + " updated");
          this.LOGGER.logInfo(_plus_3);
        }
      }
      Connection _connection = it.getConninst().getConnectionReferences().get(0).getConnection();
      boolean _tripleNotEquals_2 = (_connection != null);
      if (_tripleNotEquals_2) {
        ConnectionKind _kind = it.getConninst().getKind();
        ConnectionKind _connectionKind = ConnectionUtils.getConnectionKind(it.getConninst().getConnectionReferences().get(0).getConnection());
        boolean _tripleNotEquals_3 = (_kind != _connectionKind);
        if (_tripleNotEquals_3) {
          Connection _connection_1 = it.getConninst().getConnectionReferences().get(0).getConnection();
          if ((_connection_1 instanceof FeatureConnection)) {
            this.refineConnection(it.getConninst());
            String _name_2 = it.getConninst().getName();
            String _plus_4 = ("DIM: Kind of Connection Instance " + _name_2);
            String _plus_5 = (_plus_4 + " refined");
            this.LOGGER.logInfo(_plus_5);
          } else {
            EList<ConnectionReference> _connectionReferences = it.getConninst().getConnectionReferences();
            for (final ConnectionReference connref : _connectionReferences) {
              {
                boolean _isAffectingInstanceObject = LibraryUtils.isAffectingInstanceObject(connref, connref.getContext(), this.aadlPublicPackage, this.engine, this.preferences.isModifyReused(), true);
                if (_isAffectingInstanceObject) {
                  this.denyImplementnUpdatePropagn(connref, connref.getContext(), true);
                }
                final Connection oldconn = connref.getConnection();
                EObject _eContainer = oldconn.eContainer();
                final ComponentImplementation connimp = ((ComponentImplementation) _eContainer);
                Connection newconn = this.createConnection(it.getConninst().getKind(), connimp);
                this.manipulation.set(newconn, this.aadl2Package.getNamedElement_Name(), oldconn.getName());
                this.populateNewConnection(newconn, it.getConninst(), connref);
                EList<PropertyAssociation> _ownedPropertyAssociations = oldconn.getOwnedPropertyAssociations();
                for (final PropertyAssociation propassoc : _ownedPropertyAssociations) {
                  this.manipulation.moveTo(propassoc, ((Resource) newconn));
                }
                this.connectionInstanceDeletedDIM(connref, oldconn);
              }
            }
          }
        }
        boolean _isBidirectional = it.getConninst().isBidirectional();
        boolean _isBidirectional_1 = it.getConninst().getConnectionReferences().get(0).getConnection().isBidirectional();
        boolean _tripleNotEquals_4 = (Boolean.valueOf(_isBidirectional) != Boolean.valueOf(_isBidirectional_1));
        if (_tripleNotEquals_4) {
          boolean _isBidirectional_2 = it.getConninst().getConnectionReferences().get(0).getConnection().isBidirectional();
          boolean _equals = (_isBidirectional_2 == true);
          if (_equals) {
            this.refineConnection(it.getConninst());
            String _name_3 = it.getConninst().getName();
            String _plus_6 = ("DIM: Bidirectional of Connection Instance " + _name_3);
            String _plus_7 = (_plus_6 + " refined");
            this.LOGGER.logInfo(_plus_7);
          } else {
            this.manipulation.set(it.getConninst(), this.instPackage.getConnectionInstance_Bidirectional(), Boolean.valueOf(it.getConninst().getConnectionReferences().get(0).getConnection().isBidirectional()));
            String _name_4 = it.getConninst().getName();
            String _plus_8 = ("DIM: WARNING: Bidirectional update of Connection Instance " + _name_4);
            String _plus_9 = (_plus_8 + " is not refine-able! Reverted back to original");
            this.LOGGER.logWarning(_plus_9);
          }
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).action(CRUDActivationStateEnum.DELETED, ((Consumer<FindConnection.Match>) (FindConnection.Match it) -> {
    String _name = it.getConninst().getName();
    String _plus = ("DIM: Connection " + _name);
    String _plus_1 = (_plus + " deleted");
    this.LOGGER.logInfo(_plus_1);
  })).addLifeCycle(Lifecycles.getDefault(true, true)).build();

  private final EventDrivenTransformationRule<FindConnectionReference.Match, FindConnectionReference.Matcher> connectionReference2Declarative = this._eventDrivenTransformationRuleFactory.<FindConnectionReference.Match, FindConnectionReference.Matcher>createRule(FindConnectionReference.Matcher.querySpecification()).action(CRUDActivationStateEnum.CREATED, ((Consumer<FindConnectionReference.Match>) (FindConnectionReference.Match it) -> {
    Connection _connection = it.getConnref().getConnection();
    boolean _tripleEquals = (_connection == null);
    if (_tripleEquals) {
      this.connectionReferenceFoundDIM(it.getConnref());
      EObject _eContainer = it.getConnref().eContainer();
      String _name = ((ConnectionInstance) _eContainer).getName();
      String _plus = ("DIM: Connection reference " + _name);
      String _plus_1 = (_plus + " de-instantiated");
      this.LOGGER.logInfo(_plus_1);
    }
  })).action(CRUDActivationStateEnum.DELETED, ((Consumer<FindConnectionReference.Match>) (FindConnectionReference.Match it) -> {
    this.connectionInstanceDeletedDIM(it.getConnref(), it.getConnref().getConnection());
    EObject _eContainer = it.getConnref().eContainer();
    String _name = ((ConnectionInstance) _eContainer).getName();
    String _plus = ("DIM: Connection reference " + _name);
    String _plus_1 = (_plus + " deleted");
    this.LOGGER.logInfo(_plus_1);
  })).addLifeCycle(Lifecycles.getDefault(true, true)).build();

  private final EventDrivenTransformationRule<FindMode.Match, FindMode.Matcher> modeInstance2Declarative = this._eventDrivenTransformationRuleFactory.<FindMode.Match, FindMode.Matcher>createRule(FindMode.Matcher.querySpecification()).action(CRUDActivationStateEnum.CREATED, ((Consumer<FindMode.Match>) (FindMode.Match it) -> {
    Mode _mode = it.getModeinst().getMode();
    boolean _tripleEquals = (_mode == null);
    if (_tripleEquals) {
      this.modeInstanceCreatedDIM(it.getModeinst());
      String _name = it.getModeinst().getName();
      String _plus = ("DIM: Mode instance " + _name);
      String _plus_1 = (_plus + " de-instantiated");
      this.LOGGER.logInfo(_plus_1);
    } else {
      String _name_1 = it.getModeinst().getName();
      String _plus_2 = ("DIM: Declarative of mode instance " + _name_1);
      String _plus_3 = (_plus_2 + " already exists!");
      this.LOGGER.logInfo(_plus_3);
    }
  })).action(CRUDActivationStateEnum.UPDATED, ((Consumer<FindMode.Match>) (FindMode.Match it) -> {
    try {
      String _name = it.getModeinst().getName();
      String _name_1 = it.getModeinst().getMode().getName();
      boolean _tripleNotEquals = (_name != _name_1);
      if (_tripleNotEquals) {
        this.modeChangedParentSubcomponentDefinition(it.getModeinst());
        this.manipulation.set(it.getModeinst().getMode(), this.aadl2Package.getNamedElement_Name(), it.getModeinst().getName());
        String _name_2 = it.getModeinst().getName();
        String _plus = ("DIM: Name of mode instance " + _name_2);
        String _plus_1 = (_plus + " updated");
        this.LOGGER.logInfo(_plus_1);
      }
      boolean _isInitial = it.getModeinst().isInitial();
      boolean _isInitial_1 = it.getModeinst().getMode().isInitial();
      boolean _tripleNotEquals_1 = (Boolean.valueOf(_isInitial) != Boolean.valueOf(_isInitial_1));
      if (_tripleNotEquals_1) {
        this.modeChangedParentSubcomponentDefinition(it.getModeinst());
        this.manipulation.set(it.getModeinst().getMode(), this.aadl2Package.getMode_Initial(), Boolean.valueOf(it.getModeinst().isInitial()));
        String _name_3 = it.getModeinst().getName();
        String _plus_2 = ("DIM: Initial value of mode instance " + _name_3);
        String _plus_3 = (_plus_2 + " updated");
        this.LOGGER.logInfo(_plus_3);
      }
      EObject _eContainer = it.getModeinst().eContainer();
      ComponentInstance compinst = ((ComponentInstance) _eContainer);
      boolean _isDerived = it.getModeinst().isDerived();
      boolean _isDerivedModes = compinst.getClassifier().isDerivedModes();
      boolean _tripleNotEquals_2 = (Boolean.valueOf(_isDerived) != Boolean.valueOf(_isDerivedModes));
      if (_tripleNotEquals_2) {
        this.modeChangedParentSubcomponentDefinition(it.getModeinst());
        boolean _isDerived_1 = it.getModeinst().isDerived();
        boolean _equals = (_isDerived_1 == true);
        if (_equals) {
          EObject _eContainer_1 = it.getModeinst().getMode().eContainer();
          this.manipulation.set(((ComponentClassifier) _eContainer_1), this.aadl2Package.getComponentClassifier_DerivedModes(), Boolean.valueOf(true));
          EObject modebind = this.manipulation.createChild(compinst.getSubcomponent(), this.aadl2Package.getSubcomponent_OwnedModeBinding(), this.aadl2Package.getModeBinding());
          this.manipulation.set(modebind, this.aadl2Package.getModeBinding_DerivedMode(), it.getModeinst().getMode());
          boolean _isEmpty = it.getModeinst().getParents().isEmpty();
          boolean _not = (!_isEmpty);
          if (_not) {
            this.manipulation.set(modebind, this.aadl2Package.getModeBinding_ParentMode(), it.getModeinst().getParents().get(0).getMode());
          }
        } else {
          EObject _eContainer_2 = it.getModeinst().getMode().eContainer();
          this.manipulation.set(((ComponentClassifier) _eContainer_2), this.aadl2Package.getComponentClassifier_DerivedModes(), Boolean.valueOf(false));
          EList<ModeBinding> _ownedModeBindings = compinst.getSubcomponent().getOwnedModeBindings();
          for (final ModeBinding modeBind : _ownedModeBindings) {
            Mode _derivedMode = modeBind.getDerivedMode();
            Mode _mode = it.getModeinst().getMode();
            boolean _equals_1 = Objects.equal(_derivedMode, _mode);
            if (_equals_1) {
              this.manipulation.remove(modeBind.eContainer(), this.aadl2Package.getSubcomponent_OwnedModeBinding(), modeBind);
            }
          }
        }
        String _name_4 = it.getModeinst().getName();
        String _plus_4 = ("DIM: Derived value of mode instance " + _name_4);
        String _plus_5 = (_plus_4 + " updated");
        this.LOGGER.logInfo(_plus_5);
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).action(CRUDActivationStateEnum.DELETED, ((Consumer<FindMode.Match>) (FindMode.Match it) -> {
    try {
      this.modeChangedParentSubcomponentDefinition(it.getModeinst());
      EObject _eContainer = it.getModeinst().getMode().eContainer();
      this.manipulation.remove(((ComponentClassifier) _eContainer), this.aadl2Package.getComponentClassifier_OwnedMode(), it.getModeinst().getMode());
      String _name = it.getModeinst().getName();
      String _plus = ("DIM: Mode instance " + _name);
      String _plus_1 = (_plus + " deleted");
      this.LOGGER.logInfo(_plus_1);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).addLifeCycle(Lifecycles.getDefault(true, true)).build();

  private final EventDrivenTransformationRule<FindDerivedMode.Match, FindDerivedMode.Matcher> derivedModeInstance2Declarative = this._eventDrivenTransformationRuleFactory.<FindDerivedMode.Match, FindDerivedMode.Matcher>createRule(FindDerivedMode.Matcher.querySpecification()).action(CRUDActivationStateEnum.CREATED, ((Consumer<FindDerivedMode.Match>) (FindDerivedMode.Match it) -> {
    Mode _mode = it.getModeinst().getMode();
    boolean _tripleEquals = (_mode == null);
    if (_tripleEquals) {
      this.modeInstanceCreatedDIM(it.getModeinst());
      String _name = it.getModeinst().getName();
      String _plus = ("DIM: Mode instance " + _name);
      String _plus_1 = (_plus + " de-instantiated");
      this.LOGGER.logInfo(_plus_1);
    } else {
      String _name_1 = it.getModeinst().getName();
      String _plus_2 = ("DIM: Declarative of mode instance " + _name_1);
      String _plus_3 = (_plus_2 + " already exists!");
      this.LOGGER.logInfo(_plus_3);
    }
  })).action(CRUDActivationStateEnum.UPDATED, ((Consumer<FindDerivedMode.Match>) (FindDerivedMode.Match it) -> {
    try {
      this.modeChangedParentSubcomponentDefinition(it.getModeinst());
      String _name = it.getModeinst().getName();
      String _name_1 = it.getModeinst().getMode().getName();
      boolean _tripleNotEquals = (_name != _name_1);
      if (_tripleNotEquals) {
        this.manipulation.set(it.getModeinst().getMode(), this.aadl2Package.getNamedElement_Name(), it.getModeinst().getName());
        String _name_2 = it.getModeinst().getName();
        String _plus = ("DIM: Name of mode instance " + _name_2);
        String _plus_1 = (_plus + " updated");
        this.LOGGER.logInfo(_plus_1);
      }
      boolean _isInitial = it.getModeinst().isInitial();
      boolean _isInitial_1 = it.getModeinst().getMode().isInitial();
      boolean _tripleNotEquals_1 = (Boolean.valueOf(_isInitial) != Boolean.valueOf(_isInitial_1));
      if (_tripleNotEquals_1) {
        this.manipulation.set(it.getModeinst().getMode(), this.aadl2Package.getMode_Initial(), Boolean.valueOf(it.getModeinst().isInitial()));
        String _name_3 = it.getModeinst().getName();
        String _plus_2 = ("DIM: Initial value of mode instance " + _name_3);
        String _plus_3 = (_plus_2 + " updated");
        this.LOGGER.logInfo(_plus_3);
      }
      EObject _eContainer = it.getModeinst().eContainer();
      ComponentInstance compinst = ((ComponentInstance) _eContainer);
      boolean _isDerived = it.getModeinst().isDerived();
      boolean _isDerivedModes = compinst.getClassifier().isDerivedModes();
      boolean _tripleNotEquals_2 = (Boolean.valueOf(_isDerived) != Boolean.valueOf(_isDerivedModes));
      if (_tripleNotEquals_2) {
        boolean _isDerived_1 = it.getModeinst().isDerived();
        boolean _equals = (_isDerived_1 == true);
        if (_equals) {
          EObject _eContainer_1 = it.getModeinst().getMode().eContainer();
          this.manipulation.set(((ComponentClassifier) _eContainer_1), this.aadl2Package.getComponentClassifier_DerivedModes(), Boolean.valueOf(true));
          EObject modebind = this.manipulation.createChild(compinst.getSubcomponent(), this.aadl2Package.getSubcomponent_OwnedModeBinding(), this.aadl2Package.getModeBinding());
          this.manipulation.set(modebind, this.aadl2Package.getModeBinding_DerivedMode(), it.getModeinst().getMode());
          boolean _isEmpty = it.getModeinst().getParents().isEmpty();
          boolean _not = (!_isEmpty);
          if (_not) {
            this.manipulation.set(modebind, this.aadl2Package.getModeBinding_ParentMode(), it.getModeinst().getParents().get(0).getMode());
          }
        } else {
          EObject _eContainer_2 = it.getModeinst().getMode().eContainer();
          this.manipulation.set(((ComponentClassifier) _eContainer_2), this.aadl2Package.getComponentClassifier_DerivedModes(), Boolean.valueOf(false));
          EList<ModeBinding> _ownedModeBindings = compinst.getSubcomponent().getOwnedModeBindings();
          for (final ModeBinding modeBind : _ownedModeBindings) {
            Mode _derivedMode = modeBind.getDerivedMode();
            Mode _mode = it.getModeinst().getMode();
            boolean _equals_1 = Objects.equal(_derivedMode, _mode);
            if (_equals_1) {
              this.manipulation.remove(modeBind.eContainer(), this.aadl2Package.getSubcomponent_OwnedModeBinding(), modeBind);
            }
          }
        }
        String _name_4 = it.getModeinst().getName();
        String _plus_4 = ("DIM: Derived value of mode instance " + _name_4);
        String _plus_5 = (_plus_4 + " updated");
        this.LOGGER.logInfo(_plus_5);
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).action(CRUDActivationStateEnum.DELETED, ((Consumer<FindDerivedMode.Match>) (FindDerivedMode.Match it) -> {
    try {
      this.modeChangedParentSubcomponentDefinition(it.getModeinst());
      EObject _eContainer = it.getModeinst().getMode().eContainer();
      this.manipulation.remove(((ComponentClassifier) _eContainer), this.aadl2Package.getComponentClassifier_OwnedMode(), it.getModeinst().getMode());
      String _name = it.getModeinst().getName();
      String _plus = ("DIM: Mode instance " + _name);
      String _plus_1 = (_plus + " deleted");
      this.LOGGER.logInfo(_plus_1);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).addLifeCycle(Lifecycles.getDefault(true, true)).build();

  private final EventDrivenTransformationRule<FindModeTransition.Match, FindModeTransition.Matcher> modeTransitionInstance2Declarative = this._eventDrivenTransformationRuleFactory.<FindModeTransition.Match, FindModeTransition.Matcher>createRule(FindModeTransition.Matcher.querySpecification()).action(CRUDActivationStateEnum.CREATED, ((Consumer<FindModeTransition.Match>) (FindModeTransition.Match it) -> {
    ModeTransition _modeTransition = it.getModetransinst().getModeTransition();
    boolean _tripleEquals = (_modeTransition == null);
    if (_tripleEquals) {
      this.modeTransitionInstanceCreatedDIM(it.getModetransinst());
      boolean _isInheritProperty = this.preferences.isInheritProperty();
      if (_isInheritProperty) {
        this.elementCreationPropertyInheritance(it.getModetransinst());
      }
      String _name = it.getModetransinst().getName();
      String _plus = ("DIM: ModeTransition instance " + _name);
      String _plus_1 = (_plus + " de-instantiated");
      this.LOGGER.logInfo(_plus_1);
    } else {
      String _name_1 = it.getModetransinst().getName();
      String _plus_2 = ("DIM: Declarative of mode transition instance " + _name_1);
      String _plus_3 = (_plus_2 + " already exists!");
      this.LOGGER.logInfo(_plus_3);
    }
  })).action(CRUDActivationStateEnum.UPDATED, ((Consumer<FindModeTransition.Match>) (FindModeTransition.Match it) -> {
    try {
      this.modeTransChangedParentSubcomponentDefinition(it.getModetransinst());
      String _name = it.getModetransinst().getName();
      String _name_1 = it.getModetransinst().getModeTransition().getName();
      boolean _tripleNotEquals = (_name != _name_1);
      if (_tripleNotEquals) {
        this.manipulation.set(it.getModetransinst().getModeTransition(), this.aadl2Package.getNamedElement_Name(), it.getModetransinst().getName());
        String _name_2 = it.getModetransinst().getName();
        String _plus = ("DIM: Name of mode transition instance " + _name_2);
        String _plus_1 = (_plus + " updated");
        this.LOGGER.logInfo(_plus_1);
      }
      Mode _mode = it.getModetransinst().getSource().getMode();
      Mode _source = it.getModetransinst().getModeTransition().getSource();
      boolean _tripleNotEquals_1 = (_mode != _source);
      if (_tripleNotEquals_1) {
        this.manipulation.set(it.getModetransinst().getModeTransition(), this.aadl2Package.getModeTransition_Source(), it.getModetransinst().getSource().getMode());
        String _name_3 = it.getModetransinst().getName();
        String _plus_2 = ("DIM: Source of mode transition instance " + _name_3);
        String _plus_3 = (_plus_2 + " updated");
        this.LOGGER.logInfo(_plus_3);
      }
      Mode _mode_1 = it.getModetransinst().getDestination().getMode();
      Mode _destination = it.getModetransinst().getModeTransition().getDestination();
      boolean _tripleNotEquals_2 = (_mode_1 != _destination);
      if (_tripleNotEquals_2) {
        this.manipulation.set(it.getModetransinst().getModeTransition(), this.aadl2Package.getModeTransition_Destination(), it.getModetransinst().getDestination().getMode());
        String _name_4 = it.getModetransinst().getName();
        String _plus_4 = ("DIM: Destination of mode transition instance " + _name_4);
        String _plus_5 = (_plus_4 + " updated");
        this.LOGGER.logInfo(_plus_5);
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).action(CRUDActivationStateEnum.DELETED, ((Consumer<FindModeTransition.Match>) (FindModeTransition.Match it) -> {
    try {
      this.modeTransChangedParentSubcomponentDefinition(it.getModetransinst());
      EObject _eContainer = it.getModetransinst().getModeTransition().eContainer();
      this.manipulation.remove(((ComponentClassifier) _eContainer), this.aadl2Package.getComponentClassifier_OwnedModeTransition(), it.getModetransinst().getModeTransition());
      String _name = it.getModetransinst().getName();
      String _plus = ("DIM: ModeTransition instance " + _name);
      String _plus_1 = (_plus + " deleted");
      this.LOGGER.logInfo(_plus_1);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).addLifeCycle(Lifecycles.getDefault(true, true)).build();

  private final EventDrivenTransformationRule<FindProperty.Match, FindProperty.Matcher> propertyInstance2Declarative = this._eventDrivenTransformationRuleFactory.<FindProperty.Match, FindProperty.Matcher>createRule(FindProperty.Matcher.querySpecification()).action(CRUDActivationStateEnum.CREATED, ((Consumer<FindProperty.Match>) (FindProperty.Match it) -> {
    PropertyAssociation _propertyAssociation = it.getPropinst().getPropertyAssociation();
    boolean _tripleEquals = (_propertyAssociation == null);
    if (_tripleEquals) {
      boolean _isInheritedProperty = PropertyUtils.isInheritedProperty(it.getPropinst());
      boolean _not = (!_isInheritedProperty);
      if (_not) {
        this.propertyInstanceCreatedDIM(it.getPropinst(), this.aadlPublicPackage);
        Property _property = it.getPropinst().getProperty();
        boolean _tripleNotEquals = (_property != null);
        if (_tripleNotEquals) {
          String _name = it.getPropinst().getProperty().getName();
          String _plus = ("DIM: Property instance " + _name);
          String _plus_1 = (_plus + " de-instantiated");
          this.LOGGER.logInfo(_plus_1);
        } else {
          EObject _eContainer = it.getPropinst().eContainer();
          String _name_1 = ((InstanceObject) _eContainer).getName();
          String _plus_2 = ("DIM: Property instance in object " + _name_1);
          String _plus_3 = (_plus_2 + " de-instantiated");
          this.LOGGER.logInfo(_plus_3);
        }
      } else {
        String _name_2 = it.getPropinst().getProperty().getName();
        String _plus_4 = ("DIM: Property instance " + _name_2);
        String _plus_5 = (_plus_4 + " inherited");
        this.LOGGER.logInfo(_plus_5);
      }
    } else {
      String _name_3 = it.getPropinst().getProperty().getName();
      String _plus_6 = ("DIM: Declarative of property instance " + _name_3);
      String _plus_7 = (_plus_6 + " already exists!");
      this.LOGGER.logInfo(_plus_7);
    }
  })).action(CRUDActivationStateEnum.UPDATED, 
    ((Consumer<FindProperty.Match>) (FindProperty.Match it) -> {
      try {
        Property _property = it.getPropinst().getProperty();
        Property _property_1 = it.getPropinst().getPropertyAssociation().getProperty();
        boolean _tripleNotEquals = (_property != _property_1);
        if (_tripleNotEquals) {
          boolean _contains = this.aadlPublicPackage.getImportedUnits().contains(it.getPropinst().getProperty().eContainer());
          boolean _not = (!_contains);
          if (_not) {
            boolean _isNoAnnexes = this.aadlPublicPackage.isNoAnnexes();
            boolean _tripleEquals = (Boolean.valueOf(_isNoAnnexes) == Boolean.valueOf(true));
            if (_tripleEquals) {
              this.manipulation.set(this.aadlPublicPackage, this.aadl2Package.getPackageSection_NoAnnexes(), Boolean.valueOf(false));
            }
            EObject _eContainer = it.getPropinst().getProperty().eContainer();
            boolean _isPredeclaredPropertySet = AadlUtil.isPredeclaredPropertySet(((PropertySet) _eContainer).getName());
            boolean _not_1 = (!_isPredeclaredPropertySet);
            if (_not_1) {
              this.manipulation.add(this.aadlPublicPackage, this.aadl2Package.getPackageSection_ImportedUnit(), it.getPropinst().getProperty().eContainer());
            }
          }
          this.manipulation.set(it.getPropinst().getPropertyAssociation(), this.aadl2Package.getPropertyAssociation_Property(), it.getPropinst().getProperty());
          String _name = it.getPropinst().getProperty().getName();
          String _plus = ("DIM: Property of property instance " + _name);
          String _plus_1 = (_plus + " updated");
          this.LOGGER.logInfo(_plus_1);
        }
      } catch (Throwable _e) {
        throw Exceptions.sneakyThrow(_e);
      }
    })).action(CRUDActivationStateEnum.DELETED, ((Consumer<FindProperty.Match>) (FindProperty.Match it) -> {
    try {
      this.manipulation.remove(it.getPropinst().getPropertyAssociation().eContainer(), this.aadl2Package.getNamedElement_OwnedPropertyAssociation(), it.getPropinst().getPropertyAssociation());
      String _name = it.getPropinst().getProperty().getName();
      String _plus = ("DIM: Property instance " + _name);
      String _plus_1 = (_plus + " deleted");
      this.LOGGER.logInfo(_plus_1);
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).addLifeCycle(Lifecycles.getDefault(true, true)).build();

  private final EventDrivenTransformationRule<FindModalProperty.Match, FindModalProperty.Matcher> modalProperty2Declarative = this._eventDrivenTransformationRuleFactory.<FindModalProperty.Match, FindModalProperty.Matcher>createRule(FindModalProperty.Matcher.querySpecification()).action(CRUDActivationStateEnum.CREATED, ((Consumer<FindModalProperty.Match>) (FindModalProperty.Match it) -> {
    EObject _eContainer = it.getModpropinst().eContainer();
    final PropertyAssociationInstance propinst = ((PropertyAssociationInstance) _eContainer);
    ModalPropertyValue _declarativeModalPropertyValue = PropertyUtils.getDeclarativeModalPropertyValue(it.getModpropinst());
    boolean _tripleEquals = (_declarativeModalPropertyValue == null);
    if (_tripleEquals) {
      boolean _isInheritedProperty = PropertyUtils.isInheritedProperty(propinst);
      boolean _not = (!_isInheritedProperty);
      if (_not) {
        this.modalPropertyCreatedDIM(it.getModpropinst());
        String _name = propinst.getProperty().getName();
        String _plus = ("DIM: Modal property instance for property " + _name);
        String _plus_1 = (_plus + " (for ");
        EObject _eContainer_1 = propinst.eContainer();
        String _name_1 = ((InstanceObject) _eContainer_1).getName();
        String _plus_2 = (_plus_1 + _name_1);
        String _plus_3 = (_plus_2 + ") de-instantiated");
        this.LOGGER.logInfo(_plus_3);
      }
    } else {
      String _name_2 = propinst.getProperty().getName();
      String _plus_4 = ("DIM: Declarative of modal property value in property instance " + _name_2);
      String _plus_5 = (_plus_4 + " (for ");
      EObject _eContainer_2 = propinst.eContainer();
      String _name_3 = ((InstanceObject) _eContainer_2).getName();
      String _plus_6 = (_plus_5 + _name_3);
      String _plus_7 = (_plus_6 + ") already exists!");
      this.LOGGER.logInfo(_plus_7);
    }
  })).action(CRUDActivationStateEnum.UPDATED, ((Consumer<FindModalProperty.Match>) (FindModalProperty.Match it) -> {
    try {
      EObject _eContainer = it.getModpropinst().eContainer();
      final PropertyAssociationInstance propinst = ((PropertyAssociationInstance) _eContainer);
      boolean _isEmpty = it.getModpropinst().getInModes().isEmpty();
      boolean _not = (!_isEmpty);
      if (_not) {
        final ModalPropertyValue modprop = PropertyUtils.getDeclarativeModalPropertyValue(it.getModpropinst());
        modprop.getInModes().clear();
        EList<Mode> _inModes = it.getModpropinst().getInModes();
        for (final Mode modpropmode : _inModes) {
          EObject _eContainer_1 = it.getModpropinst().eContainer().eContainer();
          this.manipulation.add(modprop, this.aadl2Package.getModalElement_InMode(), ModeUtils.findObjectModeInstInSOM(((InstanceObject) _eContainer_1), ((SystemOperationMode) modpropmode)).getMode());
        }
        String _name = propinst.getProperty().getName();
        String _plus = ("DIM: Modes in modal property instance for property " + _name);
        String _plus_1 = (_plus + " (for ");
        EObject _eContainer_2 = propinst.eContainer();
        String _name_1 = ((InstanceObject) _eContainer_2).getName();
        String _plus_2 = (_plus_1 + _name_1);
        String _plus_3 = (_plus_2 + ") refreshed");
        this.LOGGER.logInfo(_plus_3);
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).action(CRUDActivationStateEnum.DELETED, ((Consumer<FindModalProperty.Match>) (FindModalProperty.Match it) -> {
    try {
      this.manipulation.remove(it.getModpropinst().eContainer(), this.aadl2Package.getPropertyAssociation_OwnedValue(), PropertyUtils.getDeclarativeModalPropertyValue(it.getModpropinst()));
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).addLifeCycle(Lifecycles.getDefault(true, true)).build();

  private final EventDrivenTransformationRule<FindPropertyValue.Match, FindPropertyValue.Matcher> propertyval2declarative = this._eventDrivenTransformationRuleFactory.<FindPropertyValue.Match, FindPropertyValue.Matcher>createRule(FindPropertyValue.Matcher.querySpecification()).action(CRUDActivationStateEnum.CREATED, 
    ((Consumer<FindPropertyValue.Match>) (FindPropertyValue.Match it) -> {
      final ModalPropertyValue modpropinst = PropertyUtils.getContainingModalPropertyValue(it.getPropvalinst());
      if (((modpropinst != null) && (modpropinst.eContainer() instanceof PropertyAssociationInstance))) {
        EObject _eContainer = modpropinst.eContainer();
        final PropertyAssociationInstance propinst = ((PropertyAssociationInstance) _eContainer);
        boolean _isInheritedProperty = PropertyUtils.isInheritedProperty(propinst);
        boolean _not = (!_isInheritedProperty);
        if (_not) {
          PropertyValue _declarativePropertyValue = PropertyUtils.getDeclarativePropertyValue(it.getPropvalinst());
          boolean _tripleEquals = (_declarativePropertyValue == null);
          if (_tripleEquals) {
            this.propertyValueCreatedDIM(propinst, modpropinst, it.getPropvalinst());
            String _name = propinst.getProperty().getName();
            String _plus = ("DIM: Value of property " + _name);
            String _plus_1 = (_plus + " (for ");
            EObject _eContainer_1 = propinst.eContainer();
            String _name_1 = ((InstanceObject) _eContainer_1).getName();
            String _plus_2 = (_plus_1 + _name_1);
            String _plus_3 = (_plus_2 + ") de-instantiated");
            this.LOGGER.logInfo(_plus_3);
          } else {
            String _name_2 = propinst.getProperty().getName();
            String _plus_4 = ("DIM: Value of property " + _name_2);
            String _plus_5 = (_plus_4 + " (for ");
            EObject _eContainer_2 = propinst.eContainer();
            String _name_3 = ((InstanceObject) _eContainer_2).getName();
            String _plus_6 = (_plus_5 + _name_3);
            String _plus_7 = (_plus_6 + ") already exists!");
            this.LOGGER.logInfo(_plus_7);
          }
        }
      }
    })).action(CRUDActivationStateEnum.UPDATED, ((Consumer<FindPropertyValue.Match>) (FindPropertyValue.Match it) -> {
    try {
      final ModalPropertyValue modpropinst = PropertyUtils.getContainingModalPropertyValue(it.getPropvalinst());
      if (((modpropinst != null) && (modpropinst.eContainer() instanceof PropertyAssociationInstance))) {
        final ModalPropertyValue modprop = PropertyUtils.getDeclarativeModalPropertyValue(modpropinst);
        if (((modprop.getOwnedValue() == null) || (!PropertyUtils.checkAaxl2AadlPropertyValueEquality(modpropinst.getOwnedValue(), modprop.getOwnedValue())))) {
          EObject _eContainer = modpropinst.eContainer();
          final PropertyAssociationInstance propinst = ((PropertyAssociationInstance) _eContainer);
          boolean _isReceivedProperty = PropertyUtils.isReceivedProperty(propinst);
          if (_isReceivedProperty) {
            boolean _isInheritedProperty = PropertyUtils.isInheritedProperty(propinst);
            if (_isInheritedProperty) {
              if (((modprop.getOwnedValue() == null) || (!PropertyUtils.checkAaxl2AadlPropertyValueEquality(modpropinst.getOwnedValue(), modprop.getOwnedValue())))) {
                this.manipulation.remove(propinst.getPropertyAssociation().eContainer(), this.aadl2Package.getNamedElement_OwnedPropertyAssociation(), propinst.getPropertyAssociation());
                this.manipulation.set(propinst, this.instPackage.getPropertyAssociationInstance_PropertyAssociation(), PropertyUtils.getParentPropertyAssociationInstance(propinst));
                String _name = propinst.getProperty().getName();
                String _plus = ("DIM: Property instance " + _name);
                String _plus_1 = (_plus + " value updated is inherited, hence corresponding Property Association deleted.");
                this.LOGGER.logInfo(_plus_1);
              }
            } else {
              if (((modprop.getOwnedValue() != null) && PropertyUtils.checkAaxl2AadlPropertyValueEquality(PropertyUtils.getParentModalPropertyValue(modpropinst).getOwnedValue(), modprop.getOwnedValue()))) {
                this.propertyInstanceCreatedDIM(propinst, this.aadlPublicPackage);
                String _name_1 = propinst.getProperty().getName();
                String _plus_2 = ("DIM: Inherited property instance " + _name_1);
                String _plus_3 = (_plus_2 + " value updated");
                this.LOGGER.logInfo(_plus_3);
              } else {
                this.propertyValueUpdatedDIM(it.getPropvalinst(), this.aadlPublicPackage);
                String _name_2 = PropertyUtils.getContainingPropertyAssociationInstance(it.getPropvalinst()).getProperty().getName();
                String _plus_4 = ("DIM: Value of property " + _name_2);
                String _plus_5 = (_plus_4 + " updated");
                this.LOGGER.logInfo(_plus_5);
              }
            }
          } else {
            this.propertyValueUpdatedDIM(it.getPropvalinst(), this.aadlPublicPackage);
            String _name_3 = PropertyUtils.getContainingPropertyAssociationInstance(it.getPropvalinst()).getProperty().getName();
            String _plus_6 = ("DIM: Value of property " + _name_3);
            String _plus_7 = (_plus_6 + " updated");
            this.LOGGER.logInfo(_plus_7);
          }
        }
      }
    } catch (Throwable _e) {
      throw Exceptions.sneakyThrow(_e);
    }
  })).addLifeCycle(Lifecycles.getDefault(true, true)).build();
}
