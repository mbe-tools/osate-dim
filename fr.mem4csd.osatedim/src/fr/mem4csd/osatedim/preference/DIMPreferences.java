package fr.mem4csd.osatedim.preference;

public class DIMPreferences {
	private final boolean inheritProperty, inheritMode, addClassifierProperty, modifyReused, createInterFeatures;
//	private final String extendClassifier;
	
	public DIMPreferences(boolean inheritProperty, boolean inheritMode, boolean createInterFeatures, boolean modifyReused, boolean addClassifierProperty/*, String extendClassifier*/) {
		this.addClassifierProperty = addClassifierProperty;
		this.inheritMode = inheritMode;
		this.inheritProperty = inheritProperty;
		this.modifyReused = modifyReused;
		this.createInterFeatures = createInterFeatures;
//		this.extendClassifier = extendClassifier;
	}
	
	public boolean isInheritProperty() {
		return inheritProperty;
	}
	
	public boolean isInheritMode() {
		return inheritMode;
	}
	
	public boolean isCreateInterFeatures() {
		return createInterFeatures;
	}

	public boolean isAddClassifierProperty() {
		return addClassifierProperty;
	}

//	public String getExtendClassifier() {
//		return extendClassifier;
//	}
	
	public boolean isModifyReused() {
		return modifyReused;
	}
	
}
