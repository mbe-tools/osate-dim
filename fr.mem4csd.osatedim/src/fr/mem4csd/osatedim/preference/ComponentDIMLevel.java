package fr.mem4csd.osatedim.preference;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.eclipse.emf.common.util.Enumerator;

public enum ComponentDIMLevel implements Enumerator {
	SUBCOMPONENT(0, "subcomponent", "subcomponent"),
	IMPLEMENTATION(1,"implementation","implementation"),
	TYPE(2,"type","type"),
	PARENT_IMPLEMENTATION(3,"parent implementation","parent implementation"),
	TOP_SYSTEM(4,"top system","top system");
	
	public static final int SUBCOMPONENT_VALUE = 0;
	public static final int IMPLEMENTATION_VALUE = 1;
	public static final int TYPE_VALUE = 2;
	public static final int PARENT_IMPLEMENTATION_VALUE = 3;
	public static final int TOP_SYSTEM_VALUE = 4;
	
	private static final ComponentDIMLevel[] VALUES_ARRAY = new ComponentDIMLevel[] { SUBCOMPONENT,
			IMPLEMENTATION, TYPE, PARENT_IMPLEMENTATION, TOP_SYSTEM};
	
	public static final List<ComponentDIMLevel> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));
	
	public static ComponentDIMLevel get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ComponentDIMLevel result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}
	
	public static ComponentDIMLevel getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ComponentDIMLevel result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}
	
	public static ComponentDIMLevel get(int value) {
		switch(value) {
		case SUBCOMPONENT_VALUE:
			return SUBCOMPONENT;
		case IMPLEMENTATION_VALUE:
			return IMPLEMENTATION;
		case TYPE_VALUE:
			return TYPE;
		case PARENT_IMPLEMENTATION_VALUE:
			return PARENT_IMPLEMENTATION;
		case TOP_SYSTEM_VALUE:
			return TOP_SYSTEM;
		}
		return null;
	}
	
	private final int value;
	private final String name;
	private final String literal;
	private ComponentDIMLevel(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}
	
	public int getValue() {
		return value;
	}

	public String getName() {
		return name;
	}

	public String getLiteral() {
		return literal;
	}

	public String toString() {
		return literal;
	}
}
