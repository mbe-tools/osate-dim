package fr.mem4csd.osatedim.preference;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import org.eclipse.emf.common.util.Enumerator;

public enum ClassifierExtensionPreference implements Enumerator {
	NONE_EXTENSION(0,"none","none"),
	REQUIRED_EXTENSION(1,"required","required"),
	ALWAYS_EXTENSION(2,"always","always");
	
	public static final int NONE_EXTENSION_VALUE = 0;
	public static final int REQUIRED_EXTENSION_VALUE = 1;
	public static final int ALWAYS_EXTENSION_VALUE = 2;
	
	private static final ClassifierExtensionPreference[] VALUES_ARRAY = new ClassifierExtensionPreference[] {NONE_EXTENSION, REQUIRED_EXTENSION, ALWAYS_EXTENSION};
	
	public static final List<ClassifierExtensionPreference> VALUES = Collections.unmodifiableList(Arrays.asList(VALUES_ARRAY));
	
	public static ClassifierExtensionPreference get(String literal) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ClassifierExtensionPreference result = VALUES_ARRAY[i];
			if (result.toString().equals(literal)) {
				return result;
			}
		}
		return null;
	}
	
	public static ClassifierExtensionPreference getByName(String name) {
		for (int i = 0; i < VALUES_ARRAY.length; ++i) {
			ClassifierExtensionPreference result = VALUES_ARRAY[i];
			if (result.getName().equals(name)) {
				return result;
			}
		}
		return null;
	}
	
	public static ClassifierExtensionPreference get(int value) {
		switch(value) {
		case NONE_EXTENSION_VALUE:
			return NONE_EXTENSION;
		case REQUIRED_EXTENSION_VALUE:
			return REQUIRED_EXTENSION;
		case ALWAYS_EXTENSION_VALUE:
			return ALWAYS_EXTENSION;
		}
		return null;
	}
	
	private final int value;
	private final String name;
	private final String literal;
	private ClassifierExtensionPreference(int value, String name, String literal) {
		this.value = value;
		this.name = name;
		this.literal = literal;
	}
	
	@Override
	public String getName() {
		return name;
	}

	@Override
	public int getValue() {
		return value;
	}

	@Override
	public String getLiteral() {
		return literal;
	}
	

}
