package fr.mem4csd.osatedim.utils;

import org.eclipse.emf.common.util.EList;
import org.eclipse.emf.ecore.EObject;
import org.osate.aadl2.BasicPropertyAssociation;
import org.osate.aadl2.BooleanLiteral;
import org.osate.aadl2.ClassifierValue;
import org.osate.aadl2.ComputedValue;
import org.osate.aadl2.ContainmentPathElement;
import org.osate.aadl2.Element;
import org.osate.aadl2.IntegerLiteral;
import org.osate.aadl2.ListType;
import org.osate.aadl2.ListValue;
import org.osate.aadl2.ModalPropertyValue;
import org.osate.aadl2.NamedElement;
import org.osate.aadl2.NamedValue;
import org.osate.aadl2.PropertyAssociation;
import org.osate.aadl2.PropertyExpression;
import org.osate.aadl2.PropertyValue;
import org.osate.aadl2.RangeValue;
import org.osate.aadl2.RealLiteral;
import org.osate.aadl2.RecordType;
import org.osate.aadl2.RecordValue;
import org.osate.aadl2.ReferenceValue;
import org.osate.aadl2.StringLiteral;
import org.osate.aadl2.instance.InstanceObject;
import org.osate.aadl2.instance.InstanceReferenceValue;
import org.osate.aadl2.instance.PropertyAssociationInstance;

public class PropertyUtils {

	private PropertyUtils() {
		super();
	}
	
	public static boolean isInheritedProperty(PropertyAssociation propinst) {
		final EObject parentComponent  =  propinst.eContainer().eContainer();
		if (parentComponent instanceof InstanceObject) {
			for (PropertyAssociation parentpropinst : ((InstanceObject) parentComponent).getOwnedPropertyAssociations()) {
				if (parentpropinst.getProperty().equals(propinst.getProperty())) {
					//Corresponding propertyAssociation found, check if values are equal
					for (ModalPropertyValue modprop : propinst.getOwnedValues()) {
						if (modprop.getOwnedValue() == null || !checkPropertyValueInstanceEquality(getParentModalPropertyValue(modprop,parentpropinst).getOwnedValue(), modprop.getOwnedValue())) {
							return false;
						}
					}
					return true;
				}
			}
		}
		return false;
	}
	
	public static boolean isReceivedProperty(PropertyAssociation propinst) {
		final EObject parentComponent  =  propinst.eContainer().eContainer();
		if (parentComponent instanceof InstanceObject) {
			for (PropertyAssociation parentpropinst : ((InstanceObject) parentComponent).getOwnedPropertyAssociations()) {
				if (parentpropinst.getProperty().equals(propinst.getProperty())) {
					return true;
				}
			}
		}
		return false;
	}
	
	public static PropertyAssociationInstance getParentPropertyAssociationInstance(PropertyAssociationInstance propinst) {
		final EObject parentComponent  =  propinst.eContainer().eContainer();
		if (parentComponent instanceof InstanceObject) {
			for (PropertyAssociation parentpropinst : ((InstanceObject) parentComponent).getOwnedPropertyAssociations()) {
				if (parentpropinst.getProperty().equals(propinst.getProperty())) {
					return (PropertyAssociationInstance) parentpropinst;
				}
			}
		}
		return null;
	}
	
	public static ModalPropertyValue getParentModalPropertyValue(ModalPropertyValue modprop) {
		final PropertyAssociationInstance parentpropinst = getParentPropertyAssociationInstance((PropertyAssociationInstance) modprop.eContainer()); 
		return getParentModalPropertyValue(modprop, parentpropinst);
	}
	
	public static ModalPropertyValue getParentModalPropertyValue(ModalPropertyValue modprop, PropertyAssociation parentpropinst) {
		for (ModalPropertyValue parentmodprop : parentpropinst.getOwnedValues()) {
			if (parentmodprop.getInModes().containsAll(modprop.getInModes()) && modprop.getInModes().containsAll(parentmodprop.getInModes())) {
				return parentmodprop;
			}
		}
		return null;
	}
	
	public static BasicPropertyAssociation getParentBasicPropertyAssociation(BasicPropertyAssociation basicprop, RecordValue parentmodprop) {
		for (BasicPropertyAssociation parentbasicprop : parentmodprop.getOwnedFieldValues()) {
			if (parentbasicprop.getProperty() == basicprop.getProperty()) {
				return parentbasicprop;
			}
		}
		return null;
	}
	
	@SuppressWarnings("null")
	public static boolean checkAaxl2AadlPropertyValueEquality(PropertyExpression modpropinst, PropertyExpression modprop) {
		if (PropertyTypeUtils.isSimpleProperty(getContainingPropertyAssociationInstance(modpropinst).getProperty().getPropertyType()) || (modpropinst instanceof PropertyValue && !(modpropinst instanceof RecordValue))) {
			if (PropertyTypeUtils.isValueProperty(PropertyTypeUtils.getPropertyType(modpropinst))) {
				return checkSimplePropertyValueEquality(modpropinst, modprop);
			} 
			if (modpropinst instanceof InstanceReferenceValue) {
				if (getReferenceValue((ReferenceValue) modprop) != TraceUtils.getDeclarativeElement(((InstanceReferenceValue) modpropinst).getReferencedInstanceObject())) {
					return false;
				} else {
					return true;
				}
			}
			return (Boolean) null;
		} else {
			 if (modpropinst instanceof RecordValue) {
				EList<BasicPropertyAssociation> basicproplistinst = ((RecordValue) modpropinst).getOwnedFieldValues();
				EList<BasicPropertyAssociation> basicproplist = ((RecordValue) modprop).getOwnedFieldValues();
				if (basicproplist.size() != basicproplistinst.size()) {
					return false;
				}
				for (BasicPropertyAssociation basicpropinst : basicproplistinst) {
					if (!checkAaxl2AadlPropertyValueEquality(basicpropinst.getOwnedValue(), basicproplist.get(basicproplistinst.indexOf(basicpropinst)).getOwnedValue())) {
						return false;
					}
				}
			} else if (modpropinst instanceof ListValue) {
				EList<PropertyExpression> listinst = ((ListValue) modpropinst).getOwnedListElements();
				EList<PropertyExpression> list = ((ListValue) modprop).getOwnedListElements();
				if (list.size() != listinst.size()) {
					return false;
				}
				for (PropertyExpression listeleminst : listinst) {
					if (!checkAaxl2AadlPropertyValueEquality(listeleminst, list.get(listinst.indexOf(listeleminst)))) {
						return false;
					}
				}
			}
			return true;
		}
	}
	
	public static NamedElement getReferenceValue(ReferenceValue refval) {
		ContainmentPathElement refpath = refval.getPath();
		if (refpath == null) {
			return null;
		}
		while (refpath.getPath() != null) {
			refpath = refpath.getPath();
		}
		return refpath.getNamedElement();
	}
	
	public static boolean checkSimplePropertyValueEquality(PropertyExpression propexp1, PropertyExpression propexp2) {
		if (propexp1 instanceof StringLiteral) {
			if (((StringLiteral) propexp1).getValue() != ((StringLiteral) propexp2).getValue()) {
				return false;
			}
		} else if (propexp1 instanceof IntegerLiteral) {
			if (((IntegerLiteral) propexp1).getValue() != ((IntegerLiteral) propexp2).getValue() || ((IntegerLiteral) propexp1).getBase() != ((IntegerLiteral) propexp2).getBase() || ((IntegerLiteral) propexp1).getUnit() != ((IntegerLiteral) propexp2).getUnit()) {
				return false;
			} 
		} else if (propexp1 instanceof RealLiteral) {
			if (((RealLiteral) propexp1).getValue() != ((RealLiteral) propexp2).getValue() || ((RealLiteral) propexp1).getUnit() != ((RealLiteral) propexp2).getUnit()) {
				return false;
			}
		} else if (propexp1 instanceof ClassifierValue) {
			if (((ClassifierValue) propexp1).getClassifier() != ((ClassifierValue) propexp2).getClassifier()) {
				return false;
			}
		} else if (propexp1 instanceof BooleanLiteral) {
			if (((BooleanLiteral) propexp1).getValue() != ((BooleanLiteral) propexp2).getValue()) {
				return false;
			}
		} else if (propexp1 instanceof RangeValue) {
			if (((RangeValue) propexp1).getDelta() != ((RangeValue) propexp2).getDelta() || checkPropertyValueInstanceEquality(((RangeValue) propexp1).getMaximum(), ((RangeValue) propexp2).getMaximum()) || checkPropertyValueInstanceEquality(((RangeValue) propexp1).getMinimum(), ((RangeValue) propexp2).getMinimum())) {
				return false;
			}
		} else if (propexp1 instanceof ComputedValue) {
			if (((ComputedValue) propexp1).getFunction() != ((ComputedValue) propexp2).getFunction()) {
				return false;
			}
		} else if (propexp1 instanceof NamedValue) {
			if (((NamedValue) propexp1).getNamedValue() != ((NamedValue) propexp2).getNamedValue()) {
				return false;
			}
		}
		return true;
	}
	
	public static boolean checkPropertyValueInstanceEquality(PropertyExpression parentmodprop, PropertyExpression modprop) {
		if (PropertyTypeUtils.isValueProperty(getContainingPropertyAssociationInstance(modprop).getProperty().getPropertyType()) || (modprop instanceof PropertyValue && !(modprop instanceof RecordValue))) {
			return checkSimplePropertyValueEquality(parentmodprop, modprop);
		} else {
			if (modprop instanceof InstanceReferenceValue) {
				if (((InstanceReferenceValue) modprop).getReferencedInstanceObject() != ((InstanceReferenceValue) parentmodprop).getReferencedInstanceObject()) {
					return false;
				}
			} else if (modprop instanceof RecordValue) {
				for (BasicPropertyAssociation basicprop : ((RecordValue) modprop).getOwnedFieldValues()) {
					if (!checkPropertyValueInstanceEquality(basicprop.getOwnedValue(), getParentBasicPropertyAssociation(basicprop, (RecordValue) parentmodprop).getOwnedValue())) {
						return false;
					}
				}
			} else {
				for (PropertyExpression listelem : ((ListValue) modprop).getOwnedListElements()) {
					if (!checkPropertyValueInstanceEquality(listelem,((ListValue) parentmodprop).getOwnedListElements().get(((ListValue) modprop).getOwnedListElements().indexOf(listelem)))) {
						return false;
					}
				}
			}
			return true;
		}
	}
	
	public static Element getContainingBasicPropertyAssociation(PropertyExpression propexp) {
		EObject returnobj = propexp.eContainer();
		while (returnobj != null) {
			if (returnobj instanceof BasicPropertyAssociation || returnobj instanceof PropertyAssociationInstance) {
				return (Element) returnobj;
			} else {
				returnobj = returnobj.eContainer();
			}
		}
		return null;
	}

	public static PropertyAssociationInstance getContainingPropertyAssociationInstance(PropertyExpression propexp) {
		EObject returnobj = propexp.eContainer().eContainer();
		while (returnobj != null) {
			if (returnobj instanceof PropertyAssociationInstance) {
				return (PropertyAssociationInstance) returnobj;
			} else {
				returnobj = returnobj.eContainer();
			}
		}
		return null;
	}
	
	public static ModalPropertyValue getContainingModalPropertyValue(PropertyExpression propexp) {
		EObject returnobj = propexp.eContainer();
		while (returnobj != null) {
			if (returnobj instanceof ModalPropertyValue) {
				return (ModalPropertyValue) returnobj;
			} else {
				returnobj = returnobj.eContainer();
			}
		}
		return null;
	}

	public static PropertyValue getDeclarativePropertyValue(PropertyValue propvalinst) {
	  ModalPropertyValue modprop = getDeclarativeModalPropertyValue(getContainingModalPropertyValue(propvalinst));
	  if (modprop.getOwnedValue() == null) {
		  return null;
	  }
	  if (getContainingPropertyAssociationInstance(propvalinst).getProperty().getPropertyType() instanceof ListType) {
		  if (propvalinst.eContainer() instanceof ListValue) {
			  EList<PropertyExpression> proplist = ((ListValue) modprop.getOwnedValue()).getOwnedListElements();
			  Integer propvalindex = ((ListValue) propvalinst.eContainer()).getOwnedListElements().indexOf(propvalinst);
			  if (propvalindex >= proplist.size()) {
				return null;  
			  } 
			  return (PropertyValue) proplist.get(propvalindex);
		  } else {
			  RecordValue recordvalinst = (RecordValue) propvalinst.eContainer().eContainer();
			  RecordValue recordval = (RecordValue) ((ListValue) modprop.getOwnedValue()).getOwnedListElements().get(((ListValue) recordvalinst.eContainer()).getOwnedListElements().indexOf(recordvalinst));
			  BasicPropertyAssociation basicpropinst = (BasicPropertyAssociation) propvalinst.eContainer();
			  if (recordvalinst.getOwnedFieldValues().indexOf(basicpropinst) >= recordval.getOwnedFieldValues().size()) {
				  return null;
			  }
			  BasicPropertyAssociation basicprop = recordval.getOwnedFieldValues().get(recordvalinst.getOwnedFieldValues().indexOf(basicpropinst));
			  return (PropertyValue) basicprop.getOwnedValue();
		  }
	  } else if (getContainingPropertyAssociationInstance(propvalinst).getProperty().getPropertyType() instanceof RecordType) {
		  BasicPropertyAssociation basicpropinst = (BasicPropertyAssociation) propvalinst.eContainer();
		  if (((RecordValue) basicpropinst.eContainer()).getOwnedFieldValues().indexOf(basicpropinst) >= ((RecordValue) modprop.getOwnedValue()).getOwnedFieldValues().size()) {
			 return null; 
		  }
		  BasicPropertyAssociation basicprop = ((RecordValue) modprop.getOwnedValue()).getOwnedFieldValues().get(((RecordValue) basicpropinst.eContainer()).getOwnedFieldValues().indexOf(basicpropinst));
		  return (PropertyValue) basicprop.getOwnedValue();
	  } else {
		  return (PropertyValue) modprop.getOwnedValue();
	  }
	}
	
	public static ModalPropertyValue getDeclarativeModalPropertyValue(ModalPropertyValue modpropinst) {
		PropertyAssociation propassoc = ((PropertyAssociationInstance) modpropinst.eContainer()).getPropertyAssociation();
		if (propassoc.getOwnedValues().size() == 1) {
			return propassoc.getOwnedValues().get(0);
		}
		for (ModalPropertyValue modprop : propassoc.getOwnedValues()) {
			if (modprop.getInModes().containsAll(ModeUtils.getAllDeclarativeModes(modpropinst)) && ModeUtils.getAllDeclarativeModes(modpropinst).containsAll(modprop.getInModes())) {
				return modprop;
			}
		}
		return null;
	}
}