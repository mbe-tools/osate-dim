package fr.mem4csd.osatedim.utils;

import java.util.Collections;
import java.util.HashSet;
import java.util.Set;

import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.osate.aadl2.AadlPackage;
import org.osate.aadl2.Classifier;
import org.osate.aadl2.PackageSection;
import org.osate.aadl2.modelsupport.FileNameConstants;

public class AnnexUtils {
	
	private AnnexUtils() {
		// empty!
	}

	private static final Set<String> ANNEX_CONTRIBUTION_PROPERTY_SET_NAMES;
	private static final Set<String> ANNEX_CONTRIBUTION_PACKAGE_NAMES;

	static {
		HashSet<String> annexContributionPropertySetNames = new HashSet<String>();
		annexContributionPropertySetNames.add("ARINC429");
		annexContributionPropertySetNames.add("ARINC653");
		annexContributionPropertySetNames.add("Code_Generation_Properties");
		annexContributionPropertySetNames.add("Data_Model");
		annexContributionPropertySetNames.add("Physical");
		annexContributionPropertySetNames.add("SEI");
		ANNEX_CONTRIBUTION_PROPERTY_SET_NAMES = Collections.unmodifiableSet(annexContributionPropertySetNames);
		HashSet<String> annexContributionPackageNames = new HashSet<String>();
		annexContributionPackageNames.add("Base_Types");
		annexContributionPackageNames.add("PhysicalResources");
		ANNEX_CONTRIBUTION_PACKAGE_NAMES = Collections.unmodifiableSet(annexContributionPackageNames);
	}

	public static Set<String> getAnnexContributionPropertySetNames() {
		return ANNEX_CONTRIBUTION_PROPERTY_SET_NAMES;
	}
	
	public static Set<String> getAnnexContributionPackageNames() {
		return ANNEX_CONTRIBUTION_PACKAGE_NAMES;
	}

	public static boolean isAnnexContributionPropertySet(String psname) {
		for (String predeclaredPSName : ANNEX_CONTRIBUTION_PROPERTY_SET_NAMES) {
			if (predeclaredPSName.equalsIgnoreCase(psname)) {
				return true;
			}
		}
		return false;
	}
	
	public static boolean isAnnexContributionPackage(String psname) {
		for (String predeclaredPSName : ANNEX_CONTRIBUTION_PACKAGE_NAMES) {
			if (predeclaredPSName.equalsIgnoreCase(psname)) {
				return true;
			}
		}
		return false;
	}
	
	public static AadlPackage loadAnnexPackage(ResourceSet resSet, String psname) {
		URI resURI = URI.createPlatformPluginURI("org.osate.contribution.sei/resources/packages/"+psname+"."+FileNameConstants.SOURCE_FILE_EXT, true); 
		return (AadlPackage) resSet.getResource(resURI, true).getContents().get(0);
	}
	
	public static Classifier getClassifier(PackageSection pack, String name) {
		for (Classifier classifier : pack.getOwnedClassifiers()) {
			if (classifier.getName().matches(name)) {
				return classifier;
			}
		}
		return null;
	}

}
