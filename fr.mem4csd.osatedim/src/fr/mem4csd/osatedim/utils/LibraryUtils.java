package fr.mem4csd.osatedim.utils;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.osate.aadl2.AbstractImplementation;
import org.osate.aadl2.AbstractType;
import org.osate.aadl2.ComponentClassifier;
import org.osate.aadl2.ComponentImplementation;
import org.osate.aadl2.ComponentType;
import org.osate.aadl2.Connection;
import org.osate.aadl2.Feature;
import org.osate.aadl2.Mode;
import org.osate.aadl2.ModeTransition;
import org.osate.aadl2.NamedElement;
import org.osate.aadl2.PackageSection;
import org.osate.aadl2.Subcomponent;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.ConnectionReference;
import org.osate.aadl2.instance.FeatureInstance;
import org.osate.aadl2.instance.InstanceObject;
import org.osate.aadl2.instance.SystemInstance;

import fr.mem4csd.osatedim.viatra.queries.FindReusedClassifier;

public class LibraryUtils {

	private LibraryUtils() {
		super();
	}
	
	public static ComponentClassifier getComponentClassifier(ComponentInstance compinst) {
		if (compinst.getClassifier() == null) {
			return ((SystemInstance) compinst).getComponentImplementation();
		} else {
			return compinst.getClassifier();
		}
	}
	
	public static Integer computeExtensionIndex(EList<? extends ComponentClassifier> extensionList, PackageSection aadlPublicPackage, ViatraQueryEngine engine, Boolean modifyReused) {
		final Integer libraryIndex = LibraryUtils.getClosestLibraryClassifierIndex(extensionList, aadlPublicPackage);
		final Integer reusedIndex = LibraryUtils.getClosestReusedClassifierIndex(extensionList, engine) ;
		if (modifyReused) {
			return libraryIndex;
		} else {
			return Math.min(reusedIndex, libraryIndex);
		}
	}
	
	public static Integer getClosestLibraryClassifierIndex(EList<? extends ComponentClassifier> classifierList, PackageSection aadlPackageSection) {
		Integer returnValue = 0;
		for (ComponentClassifier classifier: classifierList) {
			if (isLibraryClassifier(classifier, aadlPackageSection)) {
				return returnValue;
			} else {
				returnValue += 1;
			}
		}
		return null;
	}
	
	public static Integer getClosestReusedClassifierIndex(EList<? extends ComponentClassifier> classifierList, ViatraQueryEngine engine) {
		Integer returnValue = 0;
		for (ComponentClassifier classifier: classifierList) {
			if (isReusedClassifier(classifier, engine)) {
				return returnValue;
			} else {
				returnValue += 1;
			}
		}
		return null;
	}
	
	/**
	 * To get all classifiers of the immediate parent instance element, which will inherit changes made to the declarative equivalent of the element
	 * 
	 * @param element A ComponentInstance, FeatureInstance, or ConnectionInstance which has to be updated
	 * @param compinst The ComponentInstance that contains element
	 * @param refinement The Boolean which defines if the direct  declarative equivalent, or its source refinee should be considered in computing the list of classifiers
	 */
	public static EList<ComponentClassifier> getAllInheritingParentClassifiers(NamedElement element, ComponentInstance compinst, boolean refinement) {
		if (element instanceof ComponentInstance) {
			element = ((ComponentInstance) element).getSubcomponent();
			if (refinement == true) {
				while (((Subcomponent) element).getRefined() != null) {
					element = ((Subcomponent) element).getRefined();
				}
			}
		} else if (element instanceof FeatureInstance) {
			element = ((FeatureInstance) element).getFeature();
			if (refinement == true) {
				while (((Feature) element).getRefined() != null) {
					element = ((Feature) element).getRefined();
				}
			}
		} else if (element instanceof ConnectionReference) {
			element = ((ConnectionReference) element).getConnection();
			if (refinement == true) {
				while (((Connection) element).getRefined() != null) {
					element = ((Connection) element).getRefined();
				}
			}
		}
		final EList<ComponentClassifier> returnList = new BasicEList<ComponentClassifier>();
		ComponentClassifier compclass;
		if (compinst.getClassifier() == null) {
			compclass = ((SystemInstance) compinst).getComponentImplementation();
		} else {
			compclass = compinst.getClassifier();
		}
		if (element instanceof Feature && compclass instanceof ComponentImplementation) {
			compclass = ((ComponentImplementation) compclass).getType();
		}
		returnList.add(compclass);
		while (compclass != element.eContainer() && compclass != null) {
			compclass = (ComponentClassifier) compclass.getExtended();
			returnList.add(compclass);
		}
		return returnList;
	}
	
	public static Boolean isAffectingClassifier(ComponentClassifier classifier, PackageSection aadlPackageSection, ViatraQueryEngine engine, Boolean modifyReused) {
		if (isLibraryClassifier(classifier, aadlPackageSection)) {
			return true;
		}
		if (isReusedClassifier(classifier, engine)) {
			if (!modifyReused) {
				return true;
			}
		}
		return false;
	}
	
	public static Boolean isLibraryClassifier(ComponentClassifier classifier, PackageSection aadlPackageSection) {
		if (classifier.eContainer().eContainer() != aadlPackageSection.eContainer()) {
			return true;
		} else {
			return false;
		}
	}
	
	public static Boolean isReusedClassifier(ComponentClassifier classifier, ViatraQueryEngine engine) {
		FindReusedClassifier.Matcher matcher = FindReusedClassifier.Matcher.on(engine);
		if (matcher.countMatches(null,classifier,null) > 0) {
			return true;
		}
		return false;
	}
	
	public static Boolean hasContributingClassifier(ComponentInstance compinst, ComponentClassifier classifier) {
		ComponentClassifier currentclass;
		if (classifier instanceof ComponentType) {
			if (compinst.getClassifier() instanceof ComponentType) {
				currentclass = compinst.getClassifier();
			} else {
				currentclass = ((ComponentImplementation) compinst.getClassifier()).getType();
			}
		} else {
			if (compinst.getClassifier() instanceof ComponentType) {
				return false;
			} else {
				currentclass = compinst.getClassifier();
			}
		}
		while (currentclass != null) {
			if (currentclass == classifier) {
				return true;
			} else {
				currentclass = (ComponentClassifier) currentclass.getExtended();
			}
		}
		return false;
	}

	public static Subcomponent getCopySubcomponent(Subcomponent oldSubcomp, ComponentImplementation newImp) {
		for (Subcomponent subcomp : newImp.getAllSubcomponents()) {
			if (subcomp.getName() == oldSubcomp.getName()) {
				return subcomp;
			}
		}
		return null;
	}
	
	public static Feature getCopyFeature(Feature oldFeat, ComponentType newType) {
		for (Feature feat : newType.getAllFeatures()) {
			if (feat.getName() == oldFeat.getName()) {
				return feat;
			}
		}
		return null;
	}
	
	public static Connection getCopyConnection(Connection oldConn, ComponentImplementation newImp) {
		for (Connection conn : newImp.getAllConnections()) {
			if (conn.getName() == oldConn.getName()) {
				return conn;
			}
		}
		return null;
	}
	
	public static Mode getCopyMode(Mode oldMode, ComponentClassifier newClass) {
		for (Mode mode : newClass.getAllModes()) {
			if (mode.getName() == oldMode.getName()) {
				return mode;
			}
		}
		return null;
	}
	
	public static ModeTransition getCopyModeTransition(ModeTransition oldModeTrans, ComponentClassifier newClass) {
		for (ModeTransition modeTrans : newClass.getAllModeTransitions()) {
			if (modeTrans.getName() == oldModeTrans.getName()) {
				return modeTrans;
			}
		}
		return null;
	}
	
	/**
	 * Checks if an update in an instance object can be unknowingly propagated elsewhere in the aadlPackageSection.
	 * 
	 * @param childInst The component instance
	 * @param parentInst The parent of the component instance
	 * @param aadlPackageSection The AADL Package which will be checked
	 * @param engine The VIATRA Query Engine in use
	 * @param modifyReused The Boolean to define if a classifier that is used in multiple declarations can be modified
	 * @param refinement The Boolean to define if refinees of the direct declarative equivalent of childInst should be considered
	 */
	public static boolean isAffectingInstanceObject(InstanceObject childInst, ComponentInstance parentInst, PackageSection aadlPackageSection, ViatraQueryEngine engine, boolean modifyReused, boolean refinement) {
		if (isInstanceObjectAffectingLibrary(childInst, parentInst, aadlPackageSection, refinement)) {
			return true;
		}
		if (isInstanceObjectAffectingReusedClassifier(childInst, parentInst, engine, refinement) && !modifyReused) {
			return true;
		}
		return false;
	}
	
	public static boolean isInstanceObjectAffectingReusedClassifier(FeatureInstance childInst, ComponentInstance parentInst, ViatraQueryEngine engine, boolean refinement) {
		if (getClosestReusedClassifierIndex(getAllInheritingParentClassifiers(childInst, parentInst, refinement), engine) == null) {
			if (parentInst.eContainer() instanceof InstanceObject) {
				if (isInstanceObjectAffectingReusedClassifier(parentInst, (ComponentInstance) parentInst.eContainer(), engine, false)) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return true;
		}
	}

	public static boolean isInstanceObjectAffectingReusedClassifier(InstanceObject childInst, ComponentInstance parentInst, ViatraQueryEngine engine, boolean refinement) {
		if (getClosestReusedClassifierIndex(getAllInheritingParentClassifiers(childInst, parentInst, refinement), engine) == null) {
			if (childInst instanceof FeatureInstance && parentInst.getComponentClassifier() instanceof ComponentImplementation && getClosestReusedClassifierIndex(getAllTypeInheritingImplementations((ComponentType) ((FeatureInstance) childInst).getFeature().eContainer(), (ComponentImplementation) parentInst.getComponentClassifier()), engine) != null) {
				return true;
			}
			if (parentInst.eContainer() instanceof InstanceObject) {
				if (isInstanceObjectAffectingReusedClassifier(parentInst, (ComponentInstance) parentInst.eContainer(), engine, false)) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return true;
		}
	}
	
	public static boolean isInstanceObjectAffectingLibrary(InstanceObject childInst, ComponentInstance parentInst, PackageSection aadlPackageSection, boolean refinement) {
		if (getClosestLibraryClassifierIndex(getAllInheritingParentClassifiers(childInst,parentInst, refinement),aadlPackageSection) == null) {
			if (childInst instanceof FeatureInstance && parentInst.getComponentClassifier() instanceof ComponentImplementation && getClosestLibraryClassifierIndex(getAllTypeInheritingImplementations((ComponentType) ((FeatureInstance) childInst).getFeature().eContainer(), (ComponentImplementation) parentInst.getComponentClassifier()), aadlPackageSection) != null) {
				return true;
			}
			if (parentInst.eContainer() instanceof InstanceObject) {
				if (isInstanceObjectAffectingLibrary(parentInst,(ComponentInstance) parentInst.eContainer(),aadlPackageSection, false)) {
					return true;
				} else {
					return false;
				}
			} else {
				return false;
			}
		} else {
			return true;
		}
	}
	
	public static boolean isClassifierExtendedFrom(ComponentClassifier extensionClassifier, ComponentClassifier sourceClassifier) {
		if (extensionClassifier == sourceClassifier) {
			return true;
		}
		while (extensionClassifier != null) {
			if (extensionClassifier == sourceClassifier) {
				return true;
			} else {
				extensionClassifier = (ComponentClassifier) extensionClassifier.getExtended();
			}
		}
		return false;
	}
	
	public static EList<ComponentImplementation> getAllTypeInheritingImplementations(ComponentType comptype, ComponentImplementation compimp) {
		EList<ComponentImplementation> returnList = new BasicEList<ComponentImplementation>();
		returnList.add(compimp);
		while (compimp != null && isClassifierExtendedFrom(compimp.getType(),comptype)) {
			returnList.add(compimp);	
			compimp = compimp.getExtended();
		}
		return returnList;
	}
	
	
	
	public static boolean isClassifierRefinableFrom(ComponentClassifier newClassifier, ComponentClassifier oldClassifier) {
		if ((oldClassifier instanceof AbstractType || oldClassifier instanceof AbstractImplementation) && !(newClassifier instanceof AbstractType || newClassifier instanceof AbstractImplementation)) {
			return true;
		}
		if ((isClassifierExtendedFrom(newClassifier, oldClassifier))) {
			return true;
		}
		if (newClassifier instanceof ComponentImplementation && oldClassifier instanceof ComponentType && isClassifierExtendedFrom(((ComponentImplementation) newClassifier).getType(), oldClassifier)) {
			return true;
		}
		return false;
	}
}