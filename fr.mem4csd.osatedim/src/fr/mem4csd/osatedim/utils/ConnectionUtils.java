package fr.mem4csd.osatedim.utils;

import org.eclipse.emf.common.util.EList;
import org.osate.aadl2.AccessConnection;
import org.osate.aadl2.Connection;
import org.osate.aadl2.ConnectionEnd;
import org.osate.aadl2.Feature;
import org.osate.aadl2.FeatureConnection;
import org.osate.aadl2.FeatureGroupConnection;
import org.osate.aadl2.NamedElement;
import org.osate.aadl2.ParameterConnection;
import org.osate.aadl2.PortConnection;
import org.osate.aadl2.Subcomponent;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.ConnectionInstanceEnd;
import org.osate.aadl2.instance.ConnectionKind;
import org.osate.aadl2.instance.ConnectionReference;
import org.osate.aadl2.instance.FeatureInstance;

public class ConnectionUtils {
	
	private ConnectionUtils() {
		super();
	}
	
	public static ConnectionKind getConnectionKind(Connection conn) {
		if (conn instanceof AccessConnection) {
			return ConnectionKind.ACCESS_CONNECTION;
		} else if (conn instanceof FeatureConnection) {
			return ConnectionKind.FEATURE_CONNECTION;
		} else if (conn instanceof FeatureGroupConnection) {
			return ConnectionKind.FEATURE_GROUP_CONNECTION;
		} else if (conn instanceof ParameterConnection) {
			return ConnectionKind.PARAMETER_CONNECTION;
		} else if (conn instanceof PortConnection) {
			return ConnectionKind.PORT_CONNECTION;
		}
		return null;
	}
	
	public static Boolean hasContributingConnectionEnd(ConnectionInstanceEnd connendinst, ConnectionEnd connend) {
		if (connendinst instanceof FeatureInstance) {
			return FeatureUtils.hasContributingFeature((FeatureInstance) connendinst, (Feature) connend);
		} else if (connendinst instanceof ComponentInstance) {
			return LibraryUtils.hasContributingClassifier((ComponentInstance) connendinst, ((Subcomponent) connend).getClassifier());
		}
		return null;
	}
	
	public static String getDeclarativeConnectionName(ConnectionReference connref) {
		String name = "";
		if (connref.getSource().eContainer() != connref.getContext()) {
			name=name+((NamedElement) connref.getSource().eContainer()).getName()+"_";
		}
		name = name+connref.getSource().getName()+"_";
		if (connref.getDestination().eContainer() != connref.getContext()) {
			name=name+((NamedElement) connref.getDestination().eContainer()).getName()+"_";
		}
		name = name+connref.getDestination().getName();
		return name;
	}
	
	public static ConnectionEnd getConnectedEndUltimateRefinee(ConnectionInstanceEnd connendinst) {
		if (connendinst instanceof FeatureInstance) {
			Feature connend = ((FeatureInstance) connendinst).getFeature();
			while (connend.getRefined() != null) {
				connend = connend.getRefined();
			}
			return connend;
		} else if (connendinst instanceof ComponentInstance) {
			Subcomponent connend = ((ComponentInstance) connendinst).getSubcomponent();
			while (connend.getRefined() != null) {
				connend = connend.getRefined();
			}
			return (ConnectionEnd) connend;
		}
		return null;
	}
	
	public static EList<ComponentInstance> getPathBetweenConnectionInstanceEnds(ConnectionInstanceEnd conninstsrc, ConnectionInstanceEnd conninstdes) {
		ComponentInstance conninstsrccomp;
		ComponentInstance conninstdescomp;
		if(conninstsrc instanceof ComponentInstance) {
			conninstsrccomp = (ComponentInstance) conninstsrc;
		} else {
			conninstsrccomp = conninstsrc.getContainingComponentInstance();
		}
		if (conninstdes instanceof ComponentInstance) {
			conninstdescomp = (ComponentInstance) conninstdes;
		} else {
			conninstdescomp = conninstdes.getContainingComponentInstance();
		}
		return InstanceHierarchyUtils.getPathBetweenComponentInstance(conninstsrccomp, conninstdescomp);
	}
}
