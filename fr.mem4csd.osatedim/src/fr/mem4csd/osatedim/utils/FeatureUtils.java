package fr.mem4csd.osatedim.utils;

import org.osate.aadl2.AbstractFeature;
import org.osate.aadl2.BusAccess;
import org.osate.aadl2.DataAccess;
import org.osate.aadl2.DataPort;
import org.osate.aadl2.DirectedFeature;
import org.osate.aadl2.DirectionType;
import org.osate.aadl2.EventDataPort;
import org.osate.aadl2.EventPort;
import org.osate.aadl2.Feature;
import org.osate.aadl2.FeatureGroup;
import org.osate.aadl2.Parameter;
import org.osate.aadl2.SubprogramAccess;
import org.osate.aadl2.SubprogramGroupAccess;
import org.osate.aadl2.instance.FeatureCategory;
import org.osate.aadl2.instance.FeatureInstance;

public class FeatureUtils {

	private FeatureUtils() {
		super();
	}
	
	public static FeatureCategory getFeatureCategory(Feature feat) {
		if (feat instanceof AbstractFeature) {
			return FeatureCategory.ABSTRACT_FEATURE;
		} else if (feat instanceof DataPort) {
			return FeatureCategory.DATA_PORT;
		} else if (feat instanceof EventPort) {
			return FeatureCategory.EVENT_PORT;
		} else if (feat instanceof EventDataPort) {
			return FeatureCategory.EVENT_DATA_PORT;
		} else if (feat instanceof Parameter) {
			return FeatureCategory.PARAMETER;
		} else if (feat instanceof BusAccess) {
			return FeatureCategory.BUS_ACCESS;
		} else if (feat instanceof DataAccess) {
			return FeatureCategory.DATA_ACCESS;
		} else if (feat instanceof SubprogramAccess) {
			return FeatureCategory.SUBPROGRAM_ACCESS;
		} else if (feat instanceof SubprogramGroupAccess) {
			return FeatureCategory.SUBPROGRAM_GROUP_ACCESS;
		} else if (feat instanceof FeatureGroup) {
			return FeatureCategory.FEATURE_GROUP;
		} 
		return null;
	}
	
	public static Boolean hasContributingFeature(FeatureInstance featinst, Feature feat) {
		Feature currentfeat = featinst.getFeature(); 
		do {
			if (currentfeat == feat) {
				return true;
			} else {
				currentfeat = currentfeat.getRefined();
			}
		} while (currentfeat != null);
		return false;	
	}
	
	public static Boolean isRefinedFeatureDirection(DirectionType instDir, DirectionType declDir) {
		if (declDir != null) {
			if (declDir == DirectionType.IN_OUT) {
				return true;
			} else {
				return false;
			}
		} else {
			return true;
		}
	}
	
	public static DirectionType getFeatureDirection(DirectedFeature feat) {
		if (feat.isOut()) {
			if (feat.isIn()) {
				return DirectionType.IN_OUT;
			} else {
				return DirectionType.OUT;
			}
		} else {
			if (feat.isIn()) {
				return DirectionType.IN;
			} else {
				return null;
			}
		}
	}
}