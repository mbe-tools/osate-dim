package fr.mem4csd.osatedim.utils;

import java.util.logging.Level;
import java.util.logging.Logger;

public class DIMStandaloneLogger implements DIMLogger {

	static final Logger LOGGER = Logger.getLogger(DIMStandaloneLogger.class.getName());
	
	@Override
	public void log(int status, String msg) {
		LOGGER.log(getLoggerLevel(status), msg);
	}

	@Override
	public void logInfo(String msg) {
		LOGGER.log(Level.INFO, msg);
	}

	@Override
	public void logWarning(String msg) {
		LOGGER.log(Level.WARNING, msg);
	}

	@Override
	public void logError(String msg) {
		LOGGER.log(Level.SEVERE, msg);
	}

	@Override
	public void logCancel(String msg) {
		logError(msg);
	}
	
	private Level getLoggerLevel(int status) {
		if (status == 0x01) {
			return Level.INFO;
		} else if (status == 0x02) {
			return Level.WARNING;
		} else if (status == 0x04) {
			return Level.SEVERE;
		} else if (status == 0x08) {
			return Level.SEVERE;
		} else {
			return null;
		}
	}
}
