package fr.mem4csd.osatedim.utils;

public interface DIMLogger {
	
	public abstract void log(int status, String msg);

	public abstract void logInfo(String msg);
	
	public abstract void logWarning(String msg);

	public abstract void logError(String msg);

	public abstract void logCancel(String msg);
}
