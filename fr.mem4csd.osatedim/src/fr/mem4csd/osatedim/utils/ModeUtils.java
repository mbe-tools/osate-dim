package fr.mem4csd.osatedim.utils;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.osate.aadl2.ModalPropertyValue;
import org.osate.aadl2.Mode;
import org.osate.aadl2.instance.InstanceObject;
import org.osate.aadl2.instance.ModeInstance;
import org.osate.aadl2.instance.SystemOperationMode;

public class ModeUtils {

	private ModeUtils() {
		super();
	}
	
	public static ModeInstance findObjectModeInstInSOM(InstanceObject objinst, SystemOperationMode som) {
		for (ModeInstance modeinst : som.getCurrentModes()) {
			if (modeinst.eContainer() == objinst) {
				return modeinst;
			}
		}
		return null;
	}

	public static EList<Mode> getAllDeclarativeModes(ModalPropertyValue modpropinst) {
		EList<Mode> returnlist = new BasicEList<Mode>();
		for (Mode som : modpropinst.getInModes()) {
			returnlist.add(findObjectModeInstInSOM((InstanceObject) modpropinst.eContainer().eContainer(), (SystemOperationMode) som).getMode());
		}
		return returnlist;
	}
}