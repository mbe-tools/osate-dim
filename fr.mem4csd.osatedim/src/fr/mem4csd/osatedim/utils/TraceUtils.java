package fr.mem4csd.osatedim.utils;

import org.osate.aadl2.NamedElement;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.ConnectionInstance;
import org.osate.aadl2.instance.FeatureInstance;
import org.osate.aadl2.instance.InstanceObject;
import org.osate.aadl2.instance.SystemInstance;

import fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTrace;
import fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTraceSpec;

public class TraceUtils {

	private TraceUtils() {
		super();
	}
	
	public static InstanceObject getLeftInstanceObject (InstanceObject rightinstobj, Aaxl2AaxlTrace trace) {
		for (Aaxl2AaxlTrace currenttrace : ((Aaxl2AaxlTraceSpec) trace.eContainer()).getTraces()) {
			if (currenttrace.getRightInstance().contains(rightinstobj)) {
				return (InstanceObject) currenttrace.getLeftInstance().get(0);
			}
		}
		return null;
	}
	
	public static NamedElement getDeclarativeElement (InstanceObject instobj) {
		if (instobj instanceof SystemInstance && ((ComponentInstance) instobj).getSubcomponent() == null) {
			return ((SystemInstance) instobj).getComponentImplementation();
		} else if (instobj instanceof ComponentInstance) {
			return ((ComponentInstance) instobj).getSubcomponent();
		} else if (instobj instanceof FeatureInstance) {
			return ((FeatureInstance) instobj).getFeature();
		} else if (instobj instanceof ConnectionInstance) {
			return ((ConnectionInstance) instobj).getConnectionReferences().get(0);
		}
		return null;
	}
}
