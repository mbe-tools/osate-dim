package fr.mem4csd.osatedim.utils;

import org.eclipse.emf.common.util.URI;
import org.osate.aadl2.Aadl2Factory;
import org.osate.aadl2.AadlPackage;
import org.osate.aadl2.PublicPackageSection;
import org.osate.aadl2.instance.SystemInstance;

public class PackageUtils {
	
	public static AadlPackage configureAadlPackage(URI selectionURI, SystemInstance topSystemInst) {
		final String packageName = computePackageName( selectionURI, topSystemInst );
		final AadlPackage aadlPackage = Aadl2Factory.eINSTANCE.createAadlPackage();
		aadlPackage.setName( packageName );
		final PublicPackageSection publicPack = Aadl2Factory.eINSTANCE.createPublicPackageSection();
		publicPack.setName(topSystemInst.getName()+"_public");
		aadlPackage.setOwnedPublicSection(publicPack);
		return aadlPackage;
	}
	
	public static String computeDeclarativeName(final URI instanceModelUri, final SystemInstance systemInstance) {
		final int indexSystName = instanceModelUri.lastSegment().indexOf( systemInstance.getName() );
		return instanceModelUri.trimFileExtension().lastSegment().substring( 0, indexSystName - 1 );
	}
	
	public static String computePackageName(final URI instanceModelUri, final SystemInstance systemInstance) {
		return getIdentifier(computeDeclarativeName(instanceModelUri, systemInstance));
	}
	
	public static String getIdentifier(String str) {
	    StringBuilder sb = new StringBuilder();
	    for (int i = 0; i < str.length(); i++) {
	        if ((i == 0 && Character.isJavaIdentifierStart(str.charAt(i))) || (i > 0 && Character.isJavaIdentifierPart(str.charAt(i))))
	            sb.append(str.charAt(i));
	        else
	            sb.append((int)str.charAt(i));
	    }
	    return sb.toString();
	}
}
