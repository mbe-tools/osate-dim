package fr.mem4csd.osatedim.utils;

import org.osate.aadl2.Property;
import org.osate.aadl2.PropertySet;

public class PropertySetUtils {
	
	private PropertySetUtils() {
		super();
	}
	
	public static Property getPropertyWithName(PropertySet propertySet, String name) {
		for (Property property : propertySet.getOwnedProperties()) {
			if (property.getName().matches(name)) {
				return property;
			}
		}
		return null;
	}

}
