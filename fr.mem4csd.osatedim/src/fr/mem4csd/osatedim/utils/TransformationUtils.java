package fr.mem4csd.osatedim.utils;

import java.io.IOException;

import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.emf.EMFScope;
import org.osate.aadl2.AadlPackage;
import org.osate.aadl2.PropertySet;
import org.osate.aadl2.instance.SystemInstance;
import fr.mem4csd.osatedim.preference.DIMPreferences;
import fr.mem4csd.osatedim.viatra.transformations.DIMTransformationState;

public class TransformationUtils {
	
	public static DIMTransformationState executeStateDIM(ResourceSet resSet, SystemInstance topSystemInst, AadlPackage aadlPackage, Resource aadlResource, DIMPreferences preferences, PropertySet dimPropertySet, DIMLogger logger) throws IOException {
		final ViatraQueryEngine engine = ViatraQueryEngine.on(new EMFScope( resSet ) );
		final DIMTransformationState transformation = new DIMTransformationState(engine, topSystemInst, aadlPackage.getPublicSection(), preferences, dimPropertySet, logger);
		transformation.execute();
		return transformation;
	}
	
	public static void finishStateDIM(DIMTransformationState transformation) throws IOException {
		transformation.getTopSystemInst().eResource().save(null);
		transformation.getAadlPublickPackage().eResource().save(null);
		transformation.dispose();
	}
}
