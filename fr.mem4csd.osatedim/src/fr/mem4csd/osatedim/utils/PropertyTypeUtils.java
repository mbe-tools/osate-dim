package fr.mem4csd.osatedim.utils;

import org.osate.aadl2.BasicProperty;
import org.osate.aadl2.BasicPropertyAssociation;
import org.osate.aadl2.Element;
import org.osate.aadl2.ListType;
import org.osate.aadl2.NonListType;
import org.osate.aadl2.PropertyAssociation;
import org.osate.aadl2.PropertyExpression;
import org.osate.aadl2.PropertyType;
import org.osate.aadl2.RecordType;
import org.osate.aadl2.ReferenceType;


public class PropertyTypeUtils {

	private PropertyTypeUtils() {
		super();
	}
	
	public static PropertyType getPropertyType(PropertyExpression propexp) {
		Element propassoc = PropertyUtils.getContainingBasicPropertyAssociation(propexp);
		if (propassoc instanceof BasicPropertyAssociation) {
			return ((BasicPropertyAssociation) propassoc).getProperty().getPropertyType();
		} else {
			return ((PropertyAssociation) propassoc).getProperty().getPropertyType();
		}
	}
	
	public static Boolean isSimpleRefProperty(PropertyType proptype) {
		if (proptype instanceof ReferenceType) {
			return true;
		}
		return false;
	}
	
	// Property Type Structure
	public static Boolean isSimpleProperty(PropertyType proptype) {
		if (proptype instanceof NonListType && !((proptype instanceof RecordType))) {
			return true;
		}
		return false;
	}
	
	// Property Type Value
	public static Boolean isValueProperty(PropertyType proptype) {
		if (isSimpleProperty(proptype)) {
			if (proptype instanceof ReferenceType) {
				return false;
			}
		} else {
			if(proptype instanceof RecordType) {
				for (BasicProperty propfield : ((RecordType) proptype).getOwnedFields()) {
					if (!isValueProperty( propfield.getPropertyType())) {
						return false;
					}
				}
			} else {
				if (((ListType) proptype).getOwnedElementType() == null) {
					proptype = ((ListType) proptype).getReferencedElementType();
				} else {
					proptype = ((ListType) proptype).getOwnedElementType();
				}
				if (!isValueProperty(proptype)) {
					return false;
				}
			}
		}
		return true;
	}
}