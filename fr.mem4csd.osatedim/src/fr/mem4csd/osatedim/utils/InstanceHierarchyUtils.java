package fr.mem4csd.osatedim.utils;

import org.eclipse.emf.common.util.BasicEList;
import org.eclipse.emf.common.util.EList;
import org.osate.aadl2.BasicPropertyAssociation;
import org.osate.aadl2.ComponentImplementation;
import org.osate.aadl2.ComponentType;
import org.osate.aadl2.Connection;
import org.osate.aadl2.ContainmentPathElement;
import org.osate.aadl2.Feature;
import org.osate.aadl2.ListValue;
import org.osate.aadl2.ModalPropertyValue;
import org.osate.aadl2.NamedElement;
import org.osate.aadl2.Property;
import org.osate.aadl2.PropertyAssociation;
import org.osate.aadl2.PropertyExpression;
import org.osate.aadl2.PropertyValue;
import org.osate.aadl2.RecordType;
import org.osate.aadl2.RecordValue;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.InstanceObject;
import org.osate.aadl2.instance.InstanceReferenceValue;
import org.osate.aadl2.instance.PropertyAssociationInstance;

public class InstanceHierarchyUtils{
	
	private InstanceHierarchyUtils() {
		super();
	}
	
	public static EList<InstanceObject> getAllParentInstanceObject(InstanceObject instobj) {
		final EList<InstanceObject> returnlist = new BasicEList<InstanceObject>();
		while (instobj.eContainer() != null) {
			returnlist.add((InstanceObject) instobj.eContainer());
			instobj = (InstanceObject) instobj.eContainer();
		}
		return returnlist;
	}
	
	public static InstanceObject getLowestCommonParentInstanceObject(InstanceObject instobj1, InstanceObject instobj2) {
		final EList<InstanceObject> inst1parents = getAllParentInstanceObject(instobj1);
		final EList<InstanceObject> inst2parents = getAllParentInstanceObject(instobj2);
		if (inst1parents.isEmpty()) {
			return instobj1;
		} else if (inst2parents.isEmpty()) {
			return instobj2;
		} else {
			int index = 0;
			while (!(inst2parents.contains(inst1parents.get(index)))) {
				index++;
			}
			return inst1parents.get(index);
		}		
	}
	
	//Utilities for Connection Elements
	
	public static EList<ComponentInstance> getAllParentComponentInstance(ComponentInstance compinst) {
		final EList<ComponentInstance> returnlist = new BasicEList<ComponentInstance>();
		while (!(compinst.eContainer() == null)) {
			returnlist.add((ComponentInstance) compinst.eContainer());
			compinst = (ComponentInstance) compinst.eContainer();
		}
		return returnlist;
	}
	
	public static ComponentInstance getLowestCommonParentComponentInstance(ComponentInstance compinst1, ComponentInstance compinst2) {
		final EList<ComponentInstance> compinst1parents = getAllParentComponentInstance(compinst1);
		final EList<ComponentInstance> compinst2parents = getAllParentComponentInstance(compinst2);
		if (compinst1parents.isEmpty()) {
			return compinst1;
		} else if (compinst2parents.isEmpty()) {
			return compinst2;
		} else {
			int index = 0;
			while (!(compinst2parents.contains(compinst1parents.get(index)))) {
				index++;
			}
			return compinst1parents.get(index);
		}
	}
	
	public static EList<ComponentInstance> getPathBetweenComponentInstance(ComponentInstance compinstsrc, ComponentInstance compinstdes) {
		EList<ComponentInstance> returnlist = new BasicEList<ComponentInstance>();
		ComponentInstance comppeak = getLowestCommonParentComponentInstance(compinstsrc,compinstdes);
		if (comppeak == compinstsrc) { //downward direction in subcomponent tree
			while (!(compinstdes == compinstsrc)) {
				returnlist.add(0,(ComponentInstance) compinstdes.eContainer());
				compinstdes = (ComponentInstance) compinstdes.eContainer();
			}
		} else if (comppeak == compinstdes) { //upward direction in subcomponent tree
			returnlist = getAllParentComponentInstance(compinstsrc);
			while (!(compinstsrc == compinstdes)) {
				returnlist.add((ComponentInstance) compinstsrc.eContainer());
				compinstsrc = (ComponentInstance) compinstsrc.eContainer();
			}
		} else { //goes upward to comppeak, then down, in the subcomponent tree
			while (!(compinstsrc.eContainer() == comppeak)) {
				returnlist.add((ComponentInstance) compinstsrc.eContainer());
				compinstsrc = (ComponentInstance) compinstsrc.eContainer();
			}
			returnlist.add(comppeak);
			int downindex = returnlist.size();
			while (!(compinstdes.eContainer() == comppeak)) {
				returnlist.add(downindex,(ComponentInstance) compinstdes.eContainer());
				compinstdes = (ComponentInstance) compinstdes.eContainer();
			}
		}
		return returnlist;
	}
	
	public static EList<ComponentImplementation> getAllExtensionAncestor(ComponentImplementation compimp) {
		final EList<ComponentImplementation> returnlist = new BasicEList<ComponentImplementation>();
		ComponentImplementation currentcompimp = compimp;
		while (!(currentcompimp.getExtended() == null)) {
			returnlist.add(currentcompimp.getExtended());
			currentcompimp = currentcompimp.getExtended();
		}
		return returnlist;
	}
	
	public static EList<ComponentType> getAllExtensionAncestor(ComponentType comptype) {
		final EList<ComponentType> returnlist = new BasicEList<ComponentType>();
		ComponentType currentcomptype = comptype;
		while (!(currentcomptype.getExtended() == null)) {
			returnlist.add(currentcomptype.getExtended());
			currentcomptype = currentcomptype.getExtended();
		}
		return returnlist;
	}
	
	public static EList<Feature> getAllRefinementAncestor(Feature feat) {
		final EList<Feature> returnlist = new BasicEList<Feature>();
		Feature currentfeat = feat;
		while (!(currentfeat.getRefined() == null)) {
			returnlist.add(currentfeat.getRefined());
			currentfeat = currentfeat.getRefined();
		}
		return returnlist;
	}
	
	public static EList<Connection> getAllRefinementAncestor(Connection conn) {
		final EList<Connection> returnlist = new BasicEList<Connection>();
		Connection currentconn = conn;
		while (!(currentconn.getRefined() == null)) {
			returnlist.add(currentconn.getRefined());
			currentconn = currentconn.getRefined();
		}
		return returnlist;
	}
	
	public static ComponentInstance getLCPCIForListRefProperty(PropertyAssociationInstance propinst) {
		ComponentInstance returncomp = (ComponentInstance) propinst.eContainer();
		for (ModalPropertyValue modalprop : propinst.getOwnedValues()) {
			ListValue listval = (ListValue) modalprop.getOwnedValue();
			for (PropertyExpression listelem : listval.getOwnedListElements()) {
				if (listelem instanceof BasicPropertyAssociation) {
					if (PropertyTypeUtils.isSimpleRefProperty(((BasicPropertyAssociation) listelem).getProperty().getPropertyType())) {
						returncomp = getLowestCommonParentComponentInstance(returncomp,(ComponentInstance) ((InstanceReferenceValue) ((BasicPropertyAssociation) listelem).getOwnedValue()).getReferencedInstanceObject());
					}
				} else if (listelem instanceof RecordValue) {
					for (BasicPropertyAssociation basicprop : ((RecordValue) listelem).getOwnedFieldValues()) {
						if (basicprop.getOwnedValue() instanceof InstanceReferenceValue) {
							returncomp = getLowestCommonParentComponentInstance(returncomp, (ComponentInstance) ((InstanceReferenceValue) basicprop.getOwnedValue()).getReferencedInstanceObject());
						}
					}
				}
			}
		}
		return returncomp;
	}
	
	public static EList<ComponentInstance> getContainmentPath (ComponentInstance compinstsrc,ComponentInstance compinstdes) {
		EList<ComponentInstance> returnlist = getPathBetweenComponentInstance(compinstsrc, compinstdes);
		returnlist.remove(0);
		returnlist.add(compinstdes);
		return returnlist;
	}
	
	public static EList<InstanceObject> getContainmentPath(InstanceObject instobjsrc, InstanceObject instobjdes) {
		EList<InstanceObject> returnlist = new BasicEList<InstanceObject>();
		returnlist.add(instobjdes);
		while (instobjdes.eContainer() != instobjsrc) {
			returnlist.add(0,(InstanceObject) instobjdes.eContainer());
			instobjdes = (InstanceObject) instobjdes.eContainer();
		}
		return returnlist;
	}
	
	public static PropertyAssociation containsPropertyAssociation(NamedElement element, Property prop) {
		for (PropertyAssociation currentpropassoc : element.getOwnedPropertyAssociations()) {
			if (currentpropassoc.getProperty() == prop) {
				return currentpropassoc;
			}
		}
		return null;
	}
	
	public static PropertyAssociation containsPropertyAssociationAppliesTo(NamedElement element, Property prop, NamedElement applied) {
		for (PropertyAssociation currentpropassoc : element.getOwnedPropertyAssociations()) {
			if (currentpropassoc.getProperty() == prop && appliesTo(currentpropassoc) == applied) {
				return currentpropassoc;
			}
		}
		return null;
	}
	
	public static NamedElement appliesTo(PropertyAssociation prop) {
		if (prop.getAppliesTos() == null) {
			return null;
		}
		ContainmentPathElement currentpath = prop.getAppliesTos().get(0).getContainmentPathElements().get(0);  
		while (!(currentpath.getPath() == null)) {
			currentpath = currentpath.getPath();
		}
		return currentpath.getNamedElement();
	}
	
	
	public static InstanceObject getRefPropertyLowestCommonParentInstance(PropertyAssociationInstance propinst) {
		InstanceObject returnobj = (InstanceObject) propinst.eContainer();
		if (PropertyTypeUtils.isSimpleProperty(propinst.getProperty().getPropertyType())) {
			for (ModalPropertyValue modpropinst : propinst.getOwnedValues()) {
				if (modpropinst.getOwnedValue() instanceof InstanceReferenceValue) {
					returnobj = getLowestCommonParentInstanceObject(((InstanceReferenceValue) ((PropertyValue) modpropinst.getOwnedValue())).getReferencedInstanceObject(),returnobj);
				}
			}
		} else {
			if (propinst.getProperty().getPropertyType() instanceof RecordType) {
				for (ModalPropertyValue modpropinst : propinst.getOwnedValues()) {
					for (BasicPropertyAssociation basicprop : ((RecordValue) modpropinst.getOwnedValue()).getOwnedFieldValues()) {
						if (basicprop.getOwnedValue() instanceof InstanceReferenceValue) {
							returnobj = getLowestCommonParentInstanceObject((ComponentInstance) ((InstanceReferenceValue) basicprop.getOwnedValue()).getReferencedInstanceObject(),returnobj);
						}
					}
				}
			} else {
				for (ModalPropertyValue modpropinst : propinst.getOwnedValues()) {
					for (PropertyExpression listelem : ((ListValue) modpropinst.getOwnedValue()).getOwnedListElements()) {
						if (listelem instanceof RecordValue) {
							for (BasicPropertyAssociation basicprop : ((RecordValue) listelem).getOwnedFieldValues()) {
								if (basicprop.getOwnedValue() instanceof InstanceReferenceValue) {
									returnobj = getLowestCommonParentInstanceObject((ComponentInstance) ((InstanceReferenceValue) basicprop.getOwnedValue()).getReferencedInstanceObject(),returnobj);
								}
							}
						} else if (listelem instanceof PropertyValue) {
							if (listelem instanceof InstanceReferenceValue) {
								if (((InstanceReferenceValue) listelem).getReferencedInstanceObject() != null) {
									returnobj = getLowestCommonParentInstanceObject(((InstanceReferenceValue) listelem).getReferencedInstanceObject(), returnobj);
								}
							}
						} 
					}
				}
			}
		}
		return returnobj;
	}
}