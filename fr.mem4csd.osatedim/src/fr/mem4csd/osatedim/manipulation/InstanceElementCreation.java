package fr.mem4csd.osatedim.manipulation;

import org.eclipse.emf.ecore.util.EcoreUtil;
import org.osate.aadl2.ComponentCategory;
import org.osate.aadl2.ComponentClassifier;
import org.osate.aadl2.DirectionType;
import org.osate.aadl2.Feature;
import org.osate.aadl2.instance.ComponentInstance;
import org.osate.aadl2.instance.ConnectionInstance;
import org.osate.aadl2.instance.ConnectionInstanceEnd;
import org.osate.aadl2.instance.ConnectionKind;
import org.osate.aadl2.instance.FeatureCategory;
import org.osate.aadl2.instance.FeatureInstance;
import org.osate.aadl2.instance.InstanceObject;
import org.osate.aadl2.instance.InstancePackage;

public class InstanceElementCreation {
	
	public InstanceElementCreation() {
		super();
	}

	public static ComponentInstance createComponentInstance(ComponentInstance parent, String name, ComponentCategory category, ComponentClassifier classifier) {
		ComponentInstance newComponentInstance = (ComponentInstance) EcoreUtil.create(InstancePackage.Literals.COMPONENT_INSTANCE);
		if (name != null) {
			newComponentInstance.setName(name);
		}
		if (category != null) {
			newComponentInstance.setCategory(category);
		} else {
			newComponentInstance.setCategory(ComponentCategory.ABSTRACT);
		}
		if (classifier != null) {
			newComponentInstance.setClassifier(classifier);
		}
		if (parent != null) {
			parent.getComponentInstances().add(newComponentInstance);
		}
		return newComponentInstance;
	}
	
	public static FeatureInstance createFeatureInstance(InstanceObject parent, String name, FeatureCategory category, DirectionType direction, Feature feature) {
		FeatureInstance newFeatureInstance = (FeatureInstance) EcoreUtil.create(InstancePackage.Literals.FEATURE_INSTANCE);
		if (name != null) {
			newFeatureInstance.setName(name);
		}
		if (category != null) {
			newFeatureInstance.setCategory(category);
		} else {
			newFeatureInstance.setCategory(FeatureCategory.ABSTRACT_FEATURE);
		}
		if (direction != null) {
			newFeatureInstance.setDirection(direction);
		} else {
			newFeatureInstance.setDirection(DirectionType.IN_OUT);
		}
		if (feature != null) {
			newFeatureInstance.setFeature(feature);
		}
		if (parent != null) {
			if (parent instanceof ComponentInstance) {
				((ComponentInstance) parent).getFeatureInstances().add(newFeatureInstance);
			} else if (parent instanceof FeatureInstance && ((FeatureInstance) parent).getCategory() == FeatureCategory.FEATURE_GROUP) {
				((FeatureInstance) parent).getFeatureInstances().add(newFeatureInstance);
			}
		}
		return newFeatureInstance;
	}

	public static ConnectionInstance createConnectionInstance(ComponentInstance parent, String name, ConnectionInstanceEnd source, ConnectionInstanceEnd destination, ConnectionKind kind, boolean bidirectional) {
		ConnectionInstance newConnectionInstance = (ConnectionInstance) EcoreUtil.create(InstancePackage.Literals.CONNECTION_INSTANCE);
		if (name != null) {
			newConnectionInstance.setName(name);
		}
		if (source != null) {
			newConnectionInstance.setSource(source);
		}
		if (destination != null) {
			newConnectionInstance.setDestination(destination);
		}
		if(kind != null) {
			newConnectionInstance.setKind(kind);
		} else {
			newConnectionInstance.setKind(ConnectionKind.FEATURE_CONNECTION);
		}
		newConnectionInstance.setBidirectional(bidirectional);
		if (parent != null) {
			parent.getConnectionInstances().add(newConnectionInstance);
		}
		return newConnectionInstance;
	}
	
	public static ConnectionInstance createConnectionInstance(ComponentInstance parent, String name, ConnectionInstanceEnd source, ConnectionInstanceEnd destination, ConnectionKind kind, boolean bidirectional, boolean complete) {
		ConnectionInstance newConnectionInstance = (ConnectionInstance) EcoreUtil.create(InstancePackage.Literals.CONNECTION_INSTANCE);
		if (name != null) {
			newConnectionInstance.setName(name);
		}
		if (source != null) {
			newConnectionInstance.setSource(source);
		}
		if (destination != null) {
			newConnectionInstance.setDestination(destination);
		}
		if(kind != null) {
			newConnectionInstance.setKind(kind);
		} else {
			newConnectionInstance.setKind(ConnectionKind.FEATURE_CONNECTION);
		}
		newConnectionInstance.setBidirectional(bidirectional);
		newConnectionInstance.setComplete(complete);
		if (parent != null) {
			parent.getConnectionInstances().add(newConnectionInstance);
		}
		return newConnectionInstance;
	}
}
