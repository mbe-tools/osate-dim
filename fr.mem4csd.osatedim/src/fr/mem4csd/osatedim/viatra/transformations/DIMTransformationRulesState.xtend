package fr.mem4csd.osatedim.viatra.transformations

import org.osate.aadl2.BasicPropertyAssociation
import org.osate.aadl2.ListValue
import org.osate.aadl2.ModalPropertyValue
import org.osate.aadl2.PropertyExpression
import org.osate.aadl2.PropertyValue
import org.osate.aadl2.PublicPackageSection
import org.osate.aadl2.RecordValue
import org.osate.aadl2.instance.PropertyAssociationInstance

class DIMTransformationRulesState extends DIMTransformationRules {
	
	protected def propertyInstanceFoundDIM(PropertyAssociationInstance propinst, PublicPackageSection aadlPackage) {
		propertyInstanceCreatedDIM(propinst, aadlPackage)
		for (ModalPropertyValue modpropinst : propinst.ownedValues) {
			var modpropadd = modalPropertyCreatedDIM(modpropinst)
			if (modpropinst.ownedValue instanceof PropertyValue && !(modpropinst.ownedValue instanceof RecordValue)) {
				val propvalinst = modpropinst.ownedValue as PropertyValue
				simplePropertyValueCreation(propinst.property.propertyType, propvalinst, modpropadd)
			} else if (modpropinst.ownedValue instanceof RecordValue) {
				var recvaladd = modpropadd.createChild(modalPropertyValue_OwnedValue, recordValue)
				for (BasicPropertyAssociation basicpropinst : (modpropinst.ownedValue as RecordValue).ownedFieldValues) {
					var basicpropadd = recvaladd.createChild(recordValue_OwnedFieldValue, basicPropertyAssociation) as BasicPropertyAssociation
					basicpropadd.set(basicPropertyAssociation_Property, basicpropinst.property)
					simplePropertyValueCreation(basicpropinst.property.propertyType, basicpropinst.ownedValue as PropertyValue, basicpropadd)
				}
			} else {
				var listvalinst = modpropinst.ownedValue as ListValue
				var listvaladd = modpropadd.createChild(modalPropertyValue_OwnedValue, listValue) as ListValue
				if (listvalinst.ownedListElements.get(0) instanceof RecordValue) {
					for (PropertyExpression recvalinst : listvalinst.ownedListElements) {
						var recvaladd = listvaladd.createChild(listValue_OwnedListElement, recordValue)
						for (BasicPropertyAssociation basicpropinst : (recvalinst as RecordValue).ownedFieldValues) {
							var basicpropadd = recvaladd.createChild(recordValue_OwnedFieldValue, basicPropertyAssociation) as BasicPropertyAssociation
							basicpropadd.set(basicPropertyAssociation_Property, basicpropinst.property)
							simplePropertyValueCreation(basicpropinst.property.propertyType, basicpropinst.ownedValue as PropertyValue, basicpropadd)
						}
					}
				} else {
					for (PropertyExpression propvalinst : listvalinst.ownedListElements) {
						simplePropertyValueCreation(propinst.property.propertyType, propvalinst as PropertyValue, listvaladd)
					}
				}
			}
		}
    }
}