package fr.mem4csd.osatedim.viatra.transformations

import fr.mem4csd.osatedim.utils.LibraryUtils
import org.osate.aadl2.AbstractImplementation
import org.osate.aadl2.BusImplementation
import org.osate.aadl2.ComponentCategory
import org.osate.aadl2.ComponentImplementation
import org.osate.aadl2.DataImplementation
import org.osate.aadl2.DeviceImplementation
import org.osate.aadl2.MemoryImplementation
import org.osate.aadl2.ProcessImplementation
import org.osate.aadl2.ProcessorImplementation
import org.osate.aadl2.SubprogramGroupImplementation
import org.osate.aadl2.SubprogramImplementation
import org.osate.aadl2.SystemImplementation
import org.osate.aadl2.ThreadGroupImplementation
import org.osate.aadl2.ThreadImplementation
import org.osate.aadl2.VirtualBusImplementation
import org.osate.aadl2.VirtualProcessorImplementation
import org.osate.aadl2.instance.ComponentInstance
import org.osate.aadl2.instance.SystemInstance
import org.osate.aadl2.instance.FeatureInstance
import org.eclipse.emf.common.util.EList
import org.osate.aadl2.ComponentClassifier
import org.osate.aadl2.ComponentType
import org.osate.aadl2.instance.ConnectionReference
import org.osate.aadl2.Connection
import org.osate.aadl2.instance.ConnectionInstance
import org.osate.aadl2.instance.ConnectionKind
import org.osate.aadl2.instance.FeatureCategory
import org.osate.aadl2.VirtualProcessorType
import org.osate.aadl2.Feature
import fr.mem4csd.osatedim.utils.FeatureUtils
import org.osate.aadl2.AbstractType
import org.osate.aadl2.BusType
import org.osate.aadl2.DataType
import org.osate.aadl2.DeviceType
import org.osate.aadl2.MemoryType
import org.osate.aadl2.ProcessType
import org.osate.aadl2.ProcessorType
import org.osate.aadl2.SubprogramType
import org.osate.aadl2.SubprogramGroupType
import org.osate.aadl2.SystemType
import org.osate.aadl2.ThreadType
import org.osate.aadl2.ThreadGroupType
import org.osate.aadl2.VirtualBusType
import org.osate.aadl2.Subcomponent
import org.osate.aadl2.instance.PropertyAssociationInstance
import org.osate.aadl2.ModalPropertyValue
import org.osate.aadl2.PropertyValue
import org.osate.aadl2.RecordType
import fr.mem4csd.osatedim.utils.PropertyUtils
import fr.mem4csd.osatedim.utils.PropertyTypeUtils
import org.osate.aadl2.RecordValue
import org.osate.aadl2.ListValue
import org.osate.aadl2.BasicPropertyAssociation
import org.osate.aadl2.PublicPackageSection
import org.osate.aadl2.StringLiteral
import org.osate.aadl2.IntegerLiteral
import org.osate.aadl2.RealLiteral
import org.osate.aadl2.ClassifierValue
import org.osate.aadl2.BooleanLiteral
import org.osate.aadl2.RangeValue
import org.osate.aadl2.ComputedValue
import org.osate.aadl2.NamedValue
import org.osate.aadl2.instance.InstanceReferenceValue
import org.osate.aadl2.ModelUnit
import org.osate.aadl2.ContainedNamedElement
import fr.mem4csd.osatedim.utils.InstanceHierarchyUtils
import org.osate.aadl2.PropertyAssociation
import fr.mem4csd.osatedim.utils.TraceUtils
import org.osate.aadl2.instance.InstanceObject

class DIMTransformationRulesDelta extends DIMTransformationRules {
	
	def protected componentInstanceDeletedDIM(Subcomponent subcomp) {
    	var parentcompimp = subcomp.eContainer
    	if (parentcompimp instanceof AbstractImplementation) {
    			switch (subcomp.category) {
   					case ComponentCategory.ABSTRACT : parentcompimp.remove(componentImplementation_OwnedAbstractSubcomponent, subcomp)
   					case ComponentCategory.BUS : parentcompimp.remove(abstractImplementation_OwnedBusSubcomponent, subcomp)
 					case ComponentCategory.DATA : parentcompimp.remove(abstractImplementation_OwnedDataSubcomponent, subcomp)
 					case ComponentCategory.DEVICE : parentcompimp.remove(abstractImplementation_OwnedDeviceSubcomponent, subcomp)
  					case ComponentCategory.MEMORY : parentcompimp.remove(abstractImplementation_OwnedMemorySubcomponent, subcomp)
  					case ComponentCategory.PROCESS : parentcompimp.remove(abstractImplementation_OwnedProcessSubcomponent, subcomp) 
					case ComponentCategory.PROCESSOR : parentcompimp.remove(abstractImplementation_OwnedProcessorSubcomponent, subcomp)
  					case ComponentCategory.SUBPROGRAM : parentcompimp.remove(abstractImplementation_OwnedSubprogramSubcomponent, subcomp)
  					case ComponentCategory.SUBPROGRAM_GROUP : parentcompimp.remove(abstractImplementation_OwnedSubprogramGroupSubcomponent, subcomp)
  					case ComponentCategory.THREAD : parentcompimp.remove(abstractImplementation_OwnedThreadSubcomponent, subcomp)
  					case ComponentCategory.THREAD_GROUP : parentcompimp.remove(abstractImplementation_OwnedThreadGroupSubcomponent, subcomp)
  					case ComponentCategory.SYSTEM : parentcompimp.remove(abstractImplementation_OwnedSystemSubcomponent, subcomp)
					case ComponentCategory.VIRTUAL_BUS : parentcompimp.remove(abstractImplementation_OwnedVirtualBusSubcomponent, subcomp)
   					case ComponentCategory.VIRTUAL_PROCESSOR : parentcompimp.remove(abstractImplementation_OwnedVirtualProcessorSubcomponent, subcomp)
				}
			} else if (parentcompimp instanceof BusImplementation) {
				switch (subcomp.category) {
    				case ComponentCategory.ABSTRACT : parentcompimp.remove(componentImplementation_OwnedAbstractSubcomponent, subcomp)
   					case ComponentCategory.VIRTUAL_BUS : parentcompimp.remove(busImplementation_OwnedVirtualBusSubcomponent, subcomp)
					default : {}
				}	
			} else if (parentcompimp instanceof DataImplementation) {
				switch (subcomp.category) {
    				case ComponentCategory.ABSTRACT : parentcompimp.remove(componentImplementation_OwnedAbstractSubcomponent, subcomp)
   					case ComponentCategory.DATA : parentcompimp.remove(dataImplementation_OwnedDataSubcomponent, subcomp)
   					case ComponentCategory.SUBPROGRAM : parentcompimp.remove(dataImplementation_OwnedSubprogramSubcomponent, subcomp)
					default : {}
				}
			} else if (parentcompimp instanceof DeviceImplementation) {
				switch (subcomp.category) {
    				case ComponentCategory.ABSTRACT : parentcompimp.remove(componentImplementation_OwnedAbstractSubcomponent, subcomp)
    				case ComponentCategory.BUS : parentcompimp.remove(deviceImplementation_OwnedBusSubcomponent, subcomp)
   					case ComponentCategory.DATA : parentcompimp.remove(deviceImplementation_OwnedDataSubcomponent, subcomp)
   					case ComponentCategory.VIRTUAL_BUS : parentcompimp.remove(deviceImplementation_OwnedVirtualBusSubcomponent, subcomp)
					default : {}
				}
			} else if (parentcompimp instanceof MemoryImplementation) {
				switch (subcomp.category) {
    				case ComponentCategory.ABSTRACT : parentcompimp.remove(componentImplementation_OwnedAbstractSubcomponent, subcomp)
    				case ComponentCategory.BUS : parentcompimp.remove(memoryImplementation_OwnedBusSubcomponent, subcomp)
   					case ComponentCategory.MEMORY : parentcompimp.remove(memoryImplementation_OwnedMemorySubcomponent, subcomp)
					default : {}
				}
			} else if (parentcompimp instanceof ProcessImplementation) {
				switch (subcomp.category) {
    				case ComponentCategory.ABSTRACT : parentcompimp.remove(componentImplementation_OwnedAbstractSubcomponent, subcomp)
   					case ComponentCategory.DATA : parentcompimp.remove(processImplementation_OwnedDataSubcomponent, subcomp)
   					case ComponentCategory.SUBPROGRAM : parentcompimp.remove(processImplementation_OwnedSubprogramSubcomponent, subcomp)
   					case ComponentCategory.SUBPROGRAM_GROUP : parentcompimp.remove(processImplementation_OwnedSubprogramGroupSubcomponent, subcomp)
   					case ComponentCategory.THREAD : parentcompimp.remove(processImplementation_OwnedThreadSubcomponent, subcomp)
   					case ComponentCategory.THREAD_GROUP : parentcompimp.remove(processImplementation_OwnedThreadGroupSubcomponent, subcomp)
					default : {}
				}						
			} else if (parentcompimp instanceof ProcessorImplementation) {
				switch (subcomp.category) {
    				case ComponentCategory.ABSTRACT : parentcompimp.remove(componentImplementation_OwnedAbstractSubcomponent, subcomp)
    				case ComponentCategory.BUS : parentcompimp.remove(processorImplementation_OwnedBusSubcomponent, subcomp)
   					case ComponentCategory.MEMORY : parentcompimp.remove(processorImplementation_OwnedMemorySubcomponent, subcomp)
   					case ComponentCategory.VIRTUAL_BUS : parentcompimp.remove(processorImplementation_OwnedVirtualBusSubcomponent, subcomp)
   					case ComponentCategory.VIRTUAL_PROCESSOR : parentcompimp.remove(processorImplementation_OwnedVirtualProcessorSubcomponent, subcomp)
					default : {}
				}
			} else if (parentcompimp instanceof SubprogramImplementation) {
				switch (subcomp.category) {
    				case ComponentCategory.ABSTRACT : parentcompimp.remove(componentImplementation_OwnedAbstractSubcomponent, subcomp)
   					case ComponentCategory.DATA : parentcompimp.remove(subprogramImplementation_OwnedDataSubcomponent, subcomp)
   					case ComponentCategory.SUBPROGRAM : parentcompimp.remove(subprogramImplementation_OwnedSubprogramSubcomponent, subcomp)
					default : {}
				}
			} else if (parentcompimp instanceof SubprogramGroupImplementation) {
				switch (subcomp.category) {
    				case ComponentCategory.ABSTRACT : parentcompimp.remove(componentImplementation_OwnedAbstractSubcomponent, subcomp)
   					case ComponentCategory.DATA : parentcompimp.remove(subprogramGroupImplementation_OwnedDataSubcomponent, subcomp)
   					case ComponentCategory.SUBPROGRAM : parentcompimp.remove(subprogramGroupImplementation_OwnedSubprogramSubcomponent, subcomp)
   					case ComponentCategory.SUBPROGRAM_GROUP : parentcompimp.remove(subprogramGroupImplementation_OwnedSubprogramGroupSubcomponent, subcomp)
					default : {}
				}
			} else if (parentcompimp instanceof SystemImplementation) {
				switch (subcomp.category) {
    				case ComponentCategory.ABSTRACT : parentcompimp.remove(componentImplementation_OwnedAbstractSubcomponent, subcomp)
    				case ComponentCategory.BUS : parentcompimp.remove(systemImplementation_OwnedBusSubcomponent, subcomp)
   					case ComponentCategory.DATA : parentcompimp.remove(systemImplementation_OwnedDataSubcomponent, subcomp)
   					case ComponentCategory.DEVICE : parentcompimp.remove(systemImplementation_OwnedDeviceSubcomponent, subcomp)
   					case ComponentCategory.MEMORY : parentcompimp.remove(systemImplementation_OwnedMemorySubcomponent, subcomp)
   					case ComponentCategory.PROCESS : parentcompimp.remove(systemImplementation_OwnedProcessSubcomponent, subcomp) 
					case ComponentCategory.PROCESSOR : parentcompimp.remove(systemImplementation_OwnedProcessorSubcomponent, subcomp)
   					case ComponentCategory.SUBPROGRAM : parentcompimp.remove(systemImplementation_OwnedSubprogramSubcomponent, subcomp)
   					case ComponentCategory.SUBPROGRAM_GROUP : parentcompimp.remove(systemImplementation_OwnedSubprogramGroupSubcomponent, subcomp)
   					case ComponentCategory.SYSTEM : parentcompimp.remove(systemImplementation_OwnedSystemSubcomponent, subcomp)
   					case ComponentCategory.VIRTUAL_BUS : parentcompimp.remove(systemImplementation_OwnedVirtualBusSubcomponent, subcomp)
   					case ComponentCategory.VIRTUAL_PROCESSOR : parentcompimp.remove(systemImplementation_OwnedVirtualProcessorSubcomponent, subcomp)
					default : {}
				}
			} else if (parentcompimp instanceof ThreadImplementation) {
				switch (subcomp.category) {
    				case ComponentCategory.ABSTRACT : parentcompimp.remove(componentImplementation_OwnedAbstractSubcomponent, subcomp)
   					case ComponentCategory.DATA : parentcompimp.remove(threadImplementation_OwnedDataSubcomponent, subcomp)
   					case ComponentCategory.SUBPROGRAM : parentcompimp.remove(threadImplementation_OwnedSubprogramSubcomponent, subcomp)
   					case ComponentCategory.SUBPROGRAM_GROUP : parentcompimp.remove(threadImplementation_OwnedSubprogramGroupSubcomponent, subcomp)
					default : {}
				}
			} else if (parentcompimp instanceof ThreadGroupImplementation) {
				switch (subcomp.category) {
    				case ComponentCategory.ABSTRACT : parentcompimp.remove(componentImplementation_OwnedAbstractSubcomponent, subcomp)
   					case ComponentCategory.DATA : parentcompimp.remove(threadGroupImplementation_OwnedDataSubcomponent, subcomp)
   					case ComponentCategory.SUBPROGRAM : parentcompimp.remove(threadGroupImplementation_OwnedSubprogramSubcomponent, subcomp)
   					case ComponentCategory.SUBPROGRAM_GROUP : parentcompimp.remove(threadGroupImplementation_OwnedSubprogramGroupSubcomponent, subcomp)
   					case ComponentCategory.THREAD : parentcompimp.remove(threadGroupImplementation_OwnedThreadSubcomponent, subcomp)
   					case ComponentCategory.THREAD_GROUP : parentcompimp.remove(threadGroupImplementation_OwnedThreadGroupSubcomponent, subcomp)
					default : {}
				}
			} else if (parentcompimp instanceof VirtualBusImplementation) {
				switch (subcomp.category) {
    				case ComponentCategory.ABSTRACT : parentcompimp.remove(componentImplementation_OwnedAbstractSubcomponent, subcomp)
   					case ComponentCategory.VIRTUAL_BUS : parentcompimp.remove(virtualBusImplementation_OwnedVirtualBusSubcomponent, subcomp)
					default : {}
				}
			} else if (parentcompimp instanceof VirtualProcessorImplementation) {
				switch (subcomp.category) {
    				case ComponentCategory.ABSTRACT : parentcompimp.remove(componentImplementation_OwnedAbstractSubcomponent, subcomp)
   					case ComponentCategory.VIRTUAL_BUS : parentcompimp.remove(virtualProcessorImplementation_OwnedVirtualBusSubcomponent, subcomp)
   					case ComponentCategory.VIRTUAL_PROCESSOR : parentcompimp.remove(virtualProcessorImplementation_OwnedVirtualProcessorSubcomponent, subcomp)
					default : {}
				}
    	}
	}
	
    
    def protected ComponentImplementation extendParentImplementationForRefinement(ComponentInstance subcompinst) {
    	var compinst = subcompinst.eContainer as ComponentInstance
		if (compinst !== topSystemInst) {
			if (LibraryUtils.isAffectingInstanceObject(compinst, compinst.eContainer as ComponentInstance, aadlPublicPackage, engine, preferences.modifyReused, false)) {
				denyImplementnUpdatePropagn(compinst, compinst.eContainer as ComponentInstance, false)
			}
		}
		var newcompimp = createComponentImplementation(compinst.category)
		classifierCreationDIMPropertyAddition(newcompimp)
		var oldcompimp = {
			if (compinst == topSystemInst) {
				(compinst as SystemInstance).componentImplementation
			} else {
				compinst.classifier
			}
		} as ComponentImplementation
		newcompimp.set(componentImplementation_Extended, oldcompimp)
		newcompimp.set(componentImplementation_Type, oldcompimp.type)
		newcompimp.set(namedElement_Name, oldcompimp.name+"_ext")
		compinst.set(componentInstance_Classifier, newcompimp)
		if (compinst !== topSystemInst) {
			setSubcomponentType(compinst, compinst.classifier)	
		} else {
			(compinst as SystemInstance).set(systemInstance_ComponentImplementation, newcompimp)
		}
		return newcompimp
    }
    
    def protected denyFeatureUpdatePropagn(FeatureInstance featinst) {
    	var compinst = featinst.eContainer as ComponentInstance 
		if (LibraryUtils.isAffectingInstanceObject(featinst, compinst, aadlPublicPackage, engine, preferences.modifyReused, true)) {
			if (compinst.classifier instanceof ComponentImplementation) {
				var EList<ComponentImplementation> implList = LibraryUtils.getAllTypeInheritingImplementations(featinst.feature.eContainer as ComponentType, compinst.classifier as ComponentImplementation)
				var implIndex = LibraryUtils.computeExtensionIndex(implList, aadlPublicPackage, engine, preferences.modifyReused)
				if (implIndex !== null) {
    				var ComponentImplementation previousImpl
    				for (ComponentClassifier currentCompImpl : implList.reverse) {
    					if (implList.indexOf(currentCompImpl) < implList.size - implIndex) {
    						previousImpl = copyClassifier(currentCompImpl, previousImpl, implList, featinst) as ComponentImplementation
    					} else if (implList.indexOf(currentCompImpl) == implList.size - implIndex) {
    						currentCompImpl.set(componentImplementation_Extended, previousImpl)
    					}	
    					if (implList.indexOf(currentCompImpl) == implList.size - 1) {
    						compinst.set(componentInstance_Classifier, previousImpl)
    						if (compinst !== topSystemInst) {
    							if (LibraryUtils.isAffectingInstanceObject(compinst, compinst.eContainer as ComponentInstance, aadlPublicPackage, engine, preferences.modifyReused, false)) {
    								denyImplementnUpdatePropagn(compinst, compinst.eContainer as ComponentInstance, false)
    							}
    						}
    					}			 		
    				}
    			}
    			var EList<? extends ComponentClassifier> typeList = LibraryUtils.getAllInheritingParentClassifiers(featinst.feature, compinst, true)
    			var typeIndex = LibraryUtils.computeExtensionIndex(typeList, aadlPublicPackage, engine, preferences.modifyReused)
    			if (typeIndex !== null) {
    				var ComponentType previousType
    				for (ComponentClassifier currentCompType : typeList.reverse) {
    					if (typeList.indexOf(currentCompType) < typeList.size - typeIndex) {
    						previousType = copyClassifier(currentCompType, previousType, typeList, featinst) as ComponentType
    						for (ComponentImplementation newCompImpl : implList) {
  								if (newCompImpl.type == currentCompType) {
  									newCompImpl.set(componentImplementation_Type, previousType)
  								}
  							}
    					}  		
    				}
    			}
			} else {
				var EList<? extends ComponentClassifier> typeList = LibraryUtils.getAllInheritingParentClassifiers(featinst.feature, compinst, true)
    			var typeIndex = LibraryUtils.computeExtensionIndex(typeList, aadlPublicPackage, engine, preferences.modifyReused)
   				var ComponentType previousType
   				for (ComponentClassifier currentCompType : typeList.reverse) {
    				if (typeList.indexOf(currentCompType) < typeList.size - typeIndex) {
    					previousType = copyClassifier(currentCompType, previousType, typeList, featinst) as ComponentType
    				} else if (typeList.indexOf(currentCompType) == typeList.size - typeIndex) {
    					currentCompType.set(componentType_Extended, previousType)
    				}	 		
    				if (typeList.indexOf(currentCompType) == typeList.size - 1) {
    					compinst.set(componentInstance_Classifier, previousType)
    					if (compinst !== topSystemInst) {
    						if (LibraryUtils.isAffectingInstanceObject(compinst, compinst.eContainer as ComponentInstance, aadlPublicPackage, engine, preferences.modifyReused, false)) {
    							denyImplementnUpdatePropagn(compinst, compinst.eContainer as ComponentInstance, false)
    						}
    					}
    				}
    			}
			}
		}				
    }
    
    def protected featureInstanceDeletedDIM(Feature feature) {
    	var ComponentType comptype = feature.eContainer as ComponentType
    	var FeatureCategory category = FeatureUtils.getFeatureCategory(feature)
		if (comptype instanceof AbstractType) {
			switch (category) {
				case FeatureCategory.DATA_PORT : comptype.remove(abstractType_OwnedDataPort, feature)
				case FeatureCategory.EVENT_PORT : comptype.remove(abstractType_OwnedEventPort, feature)
				case FeatureCategory.EVENT_DATA_PORT : comptype.remove(abstractType_OwnedEventDataPort, feature)
				case FeatureCategory.BUS_ACCESS : comptype.remove(abstractType_OwnedBusAccess, feature)
				case FeatureCategory.DATA_ACCESS : comptype.remove(abstractType_OwnedDataAccess, feature)
				case FeatureCategory.SUBPROGRAM_ACCESS : comptype.remove(abstractType_OwnedSubprogramAccess, feature)
				case FeatureCategory.SUBPROGRAM_GROUP_ACCESS : comptype.remove(abstractType_OwnedSubprogramGroupAccess, feature)
				case FeatureCategory.FEATURE_GROUP : comptype.remove(componentType_OwnedFeatureGroup, feature)
				case FeatureCategory.ABSTRACT_FEATURE : comptype.remove(componentType_OwnedAbstractFeature, feature)
				default : {}
			}
		}
    	if (comptype instanceof BusType) {
			switch (category) {
				case FeatureCategory.DATA_PORT : comptype.remove(busType_OwnedDataPort, feature)
				case FeatureCategory.EVENT_PORT : comptype.remove(busType_OwnedEventPort, feature)
				case FeatureCategory.EVENT_DATA_PORT : comptype.remove(busType_OwnedEventDataPort, feature)
				case FeatureCategory.BUS_ACCESS : comptype.remove(busType_OwnedBusAccess, feature)
				case FeatureCategory.FEATURE_GROUP : comptype.remove(componentType_OwnedFeatureGroup, feature)
				case FeatureCategory.ABSTRACT_FEATURE : comptype.remove(componentType_OwnedAbstractFeature, feature)
				default : {}
			}
   		}
   		if (comptype instanceof DataType) {
			switch (category) {
				case FeatureCategory.DATA_ACCESS : comptype.remove(dataType_OwnedDataAccess, feature)
				case FeatureCategory.SUBPROGRAM_ACCESS : comptype.remove(dataType_OwnedSubprogramAccess, feature)
				case FeatureCategory.SUBPROGRAM_GROUP_ACCESS : comptype.remove(dataType_OwnedSubprogramGroupAccess, feature)
				case FeatureCategory.FEATURE_GROUP : comptype.remove(componentType_OwnedFeatureGroup, feature)
				case FeatureCategory.ABSTRACT_FEATURE : comptype.remove(componentType_OwnedAbstractFeature, feature)
				default : {}
			}
   		}
   		if (comptype instanceof DeviceType) {
			switch (category) {
				case FeatureCategory.DATA_PORT : comptype.remove(deviceType_OwnedDataPort, feature)
				case FeatureCategory.EVENT_PORT : comptype.remove(deviceType_OwnedEventPort, feature)
				case FeatureCategory.EVENT_DATA_PORT : comptype.remove(deviceType_OwnedEventDataPort, feature)
				case FeatureCategory.BUS_ACCESS : comptype.remove(deviceType_OwnedBusAccess, feature)
				case FeatureCategory.SUBPROGRAM_ACCESS : comptype.remove(deviceType_OwnedSubprogramAccess, feature)
				case FeatureCategory.SUBPROGRAM_GROUP_ACCESS : comptype.remove(deviceType_OwnedSubprogramGroupAccess, feature)
				case FeatureCategory.FEATURE_GROUP : comptype.remove(componentType_OwnedFeatureGroup, feature)
				case FeatureCategory.ABSTRACT_FEATURE : comptype.remove(componentType_OwnedAbstractFeature, feature)
				default : {}
			}
		}
   		if (comptype instanceof MemoryType) {
			switch (category) {
				case FeatureCategory.DATA_PORT : comptype.remove(memoryType_OwnedDataPort, feature)
				case FeatureCategory.EVENT_PORT : comptype.remove(memoryType_OwnedEventPort, feature)
				case FeatureCategory.EVENT_DATA_PORT : comptype.remove(memoryType_OwnedEventDataPort, feature)
				case FeatureCategory.BUS_ACCESS : comptype.remove(memoryType_OwnedBusAccess, feature)
				case FeatureCategory.FEATURE_GROUP : comptype.remove(componentType_OwnedFeatureGroup, feature)
				case FeatureCategory.ABSTRACT_FEATURE : comptype.remove(componentType_OwnedAbstractFeature, feature)
				default : {}
			}
   		}
   		if (comptype instanceof ProcessType) {
			switch (category) {
				case FeatureCategory.DATA_PORT : comptype.remove(processType_OwnedDataPort, feature)
				case FeatureCategory.EVENT_PORT : comptype.remove(processType_OwnedEventPort, feature)
				case FeatureCategory.EVENT_DATA_PORT : comptype.remove(processType_OwnedEventDataPort, feature)
				case FeatureCategory.DATA_ACCESS : comptype.remove(processType_OwnedDataAccess, feature)
				case FeatureCategory.SUBPROGRAM_ACCESS : comptype.remove(processType_OwnedSubprogramAccess, feature)
				case FeatureCategory.SUBPROGRAM_GROUP_ACCESS : comptype.remove(processType_OwnedSubprogramGroupAccess, feature)
				case FeatureCategory.FEATURE_GROUP : comptype.remove(componentType_OwnedFeatureGroup, feature)
				case FeatureCategory.ABSTRACT_FEATURE : comptype.remove(componentType_OwnedAbstractFeature, feature)
				default : {}
			}
   		}
   		if (comptype instanceof ProcessorType) {
			switch (category) {
				case FeatureCategory.DATA_PORT : comptype.remove(processorType_OwnedDataPort, feature)
				case FeatureCategory.EVENT_PORT : comptype.remove(processorType_OwnedEventPort, feature)
				case FeatureCategory.EVENT_DATA_PORT : comptype.remove(processorType_OwnedEventDataPort, feature)
				case FeatureCategory.BUS_ACCESS : comptype.remove(processorType_OwnedBusAccess, feature)
				case FeatureCategory.SUBPROGRAM_ACCESS : comptype.remove(processorType_OwnedSubprogramAccess, feature)
				case FeatureCategory.SUBPROGRAM_GROUP_ACCESS : comptype.remove(processorType_OwnedSubprogramGroupAccess, feature)
				case FeatureCategory.FEATURE_GROUP : comptype.remove(componentType_OwnedFeatureGroup, feature)
				case FeatureCategory.ABSTRACT_FEATURE : comptype.remove(componentType_OwnedAbstractFeature, feature)
				default : {}
			}
		}
   		if (comptype instanceof SubprogramType) {
			switch (category) {
				case FeatureCategory.EVENT_PORT : comptype.remove(subprogramType_OwnedEventPort, feature)
				case FeatureCategory.EVENT_DATA_PORT : comptype.remove(subprogramType_OwnedEventDataPort, feature)
				case FeatureCategory.PARAMETER : comptype.remove(subprogramType_OwnedParameter, feature)
				case FeatureCategory.DATA_ACCESS : comptype.remove(subprogramType_OwnedDataAccess, feature)
				case FeatureCategory.SUBPROGRAM_ACCESS : comptype.remove(subprogramType_OwnedSubprogramAccess, feature)
				case FeatureCategory.SUBPROGRAM_GROUP_ACCESS : comptype.remove(subprogramType_OwnedSubprogramGroupAccess, feature)
				case FeatureCategory.FEATURE_GROUP : comptype.remove(componentType_OwnedFeatureGroup, feature)
				case FeatureCategory.ABSTRACT_FEATURE : comptype.remove(componentType_OwnedAbstractFeature, feature)
				default : {}
			}
   		}
   		if (comptype instanceof SubprogramGroupType) {
			switch (category) {
				case FeatureCategory.SUBPROGRAM_ACCESS : comptype.remove(subprogramGroupType_OwnedSubprogramAccess, feature)
				case FeatureCategory.SUBPROGRAM_GROUP_ACCESS : comptype.remove(subprogramGroupType_OwnedSubprogramGroupAccess, feature)
				case FeatureCategory.FEATURE_GROUP : comptype.remove(componentType_OwnedFeatureGroup, feature)
				case FeatureCategory.ABSTRACT_FEATURE : comptype.remove(componentType_OwnedAbstractFeature, feature)
				default : {}
			}
   		}
   		if (comptype instanceof SystemType) {
			switch (category) {
				case FeatureCategory.DATA_PORT : comptype.remove(systemType_OwnedDataPort, feature)
				case FeatureCategory.EVENT_PORT : comptype.remove(systemType_OwnedEventPort, feature)
				case FeatureCategory.EVENT_DATA_PORT : comptype.remove(systemType_OwnedEventDataPort, feature)
				case FeatureCategory.BUS_ACCESS : comptype.remove(systemType_OwnedBusAccess, feature)
				case FeatureCategory.DATA_ACCESS : comptype.remove(systemType_OwnedDataAccess, feature)
				case FeatureCategory.SUBPROGRAM_ACCESS : comptype.remove(systemType_OwnedSubprogramAccess, feature)
				case FeatureCategory.SUBPROGRAM_GROUP_ACCESS : comptype.remove(systemType_OwnedSubprogramGroupAccess, feature)
				case FeatureCategory.FEATURE_GROUP : comptype.remove(componentType_OwnedFeatureGroup, feature)
				case FeatureCategory.ABSTRACT_FEATURE : comptype.remove(componentType_OwnedAbstractFeature, feature)
				default : {}
			}
   		}
   		if (comptype instanceof ThreadType) {
			switch (category) {
				case FeatureCategory.DATA_PORT : comptype.remove(threadType_OwnedDataPort, feature)
				case FeatureCategory.EVENT_PORT : comptype.remove(threadType_OwnedEventPort, feature)
				case FeatureCategory.EVENT_DATA_PORT : comptype.remove(threadType_OwnedEventDataPort, feature)
				case FeatureCategory.DATA_ACCESS : comptype.remove(threadType_OwnedDataAccess, feature)
				case FeatureCategory.SUBPROGRAM_ACCESS : comptype.remove(threadType_OwnedSubprogramAccess, feature)
				case FeatureCategory.SUBPROGRAM_GROUP_ACCESS : comptype.remove(threadType_OwnedSubprogramGroupAccess, feature)
				case FeatureCategory.FEATURE_GROUP : comptype.remove(componentType_OwnedFeatureGroup, feature)
				case FeatureCategory.ABSTRACT_FEATURE : comptype.remove(componentType_OwnedAbstractFeature, feature)
			default : {}
			}
   		}
   		if (comptype instanceof ThreadGroupType) {
			switch (category) {
				case FeatureCategory.DATA_PORT : comptype.remove(threadGroupType_OwnedDataPort, feature)
				case FeatureCategory.EVENT_PORT : comptype.remove(threadGroupType_OwnedEventPort, feature)
				case FeatureCategory.EVENT_DATA_PORT : comptype.remove(threadGroupType_OwnedEventDataPort, feature)
				case FeatureCategory.DATA_ACCESS : comptype.remove(threadGroupType_OwnedDataAccess, feature)
				case FeatureCategory.SUBPROGRAM_ACCESS : comptype.remove(threadGroupType_OwnedSubprogramAccess, feature)
				case FeatureCategory.SUBPROGRAM_GROUP_ACCESS : comptype.remove(threadGroupType_OwnedSubprogramGroupAccess, feature)
				case FeatureCategory.FEATURE_GROUP : comptype.remove(componentType_OwnedFeatureGroup, feature)
					case FeatureCategory.ABSTRACT_FEATURE : comptype.remove(componentType_OwnedAbstractFeature, feature)
				default : {}
			}
   		}
   		if (comptype instanceof VirtualBusType) {
			switch (category) {
				case FeatureCategory.DATA_PORT : comptype.remove(virtualBusType_OwnedDataPort, feature)
				case FeatureCategory.EVENT_PORT : comptype.remove(virtualBusType_OwnedEventPort, feature)
				case FeatureCategory.EVENT_DATA_PORT : comptype.remove(virtualBusType_OwnedEventDataPort, feature)
				case FeatureCategory.BUS_ACCESS : comptype.remove(virtualBusType_OwnedBusAccess, feature)
				case FeatureCategory.FEATURE_GROUP : comptype.remove(componentType_OwnedFeatureGroup, feature)
				case FeatureCategory.ABSTRACT_FEATURE : comptype.remove(componentType_OwnedAbstractFeature, feature)
				default : {}
			}
   		}
   		if (comptype instanceof VirtualProcessorType) {
			switch (category) {
				case FeatureCategory.DATA_PORT : comptype.remove(virtualProcessorType_OwnedDataPort, feature)
				case FeatureCategory.EVENT_PORT : comptype.remove(virtualProcessorType_OwnedEventPort, feature)
				case FeatureCategory.EVENT_DATA_PORT : comptype.remove(virtualProcessorType_OwnedEventDataPort, feature)
				case FeatureCategory.BUS_ACCESS : comptype.remove(virtualProcessorType_OwnedBusAccess, feature)
				case FeatureCategory.SUBPROGRAM_ACCESS : comptype.remove(virtualProcessorType_OwnedSubprogramAccess, feature)
				case FeatureCategory.SUBPROGRAM_GROUP_ACCESS : comptype.remove(virtualProcessorType_OwnedSubprogramGroupAccess, feature)
				case FeatureCategory.FEATURE_GROUP : comptype.remove(componentType_OwnedFeatureGroup, feature)
				case FeatureCategory.ABSTRACT_FEATURE : comptype.remove(componentType_OwnedAbstractFeature, feature)
				default : {}
			}
		}
    }
    
    protected def refineConnection(ConnectionInstance conninst) {
    	for (ConnectionReference connref : conninst.connectionReferences) {
			var compinst = connref.context
			if (compinst !== topSystemInst && LibraryUtils.isAffectingInstanceObject(compinst, compinst.eContainer as ComponentInstance, aadlPublicPackage, engine, preferences.modifyReused, false)) {
				denyImplementnUpdatePropagn(compinst, compinst.eContainer as ComponentInstance, false)
			}
			var ComponentImplementation compimp = compinst.classifier as ComponentImplementation
			var ComponentImplementation refimp = createComponentImplementation(compinst.category)
			refimp.set(namedElement_Name, compimp.name+"_ext")
			refimp.set(componentImplementation_Extended, compimp)
			var refconn = createConnection(conninst.kind, refimp)
			refconn.set(connection_Refined, connref.connection)
			refconn.set(namedElement_Name, connref.connection.name)
			populateNewConnection(refconn, conninst, connref)
			compinst.set(componentInstance_Classifier, refimp)
		}
    }
    
    protected def connectionInstanceDeletedDIM(ConnectionReference connref, Connection conn) {
    	if (LibraryUtils.isAffectingInstanceObject(connref,connref.context,aadlPublicPackage,engine,preferences.modifyReused, true)) {
			denyImplementnUpdatePropagn(connref, connref.context, true)
		}
    	switch ((connref.eContainer as ConnectionInstance).kind) {
    		case ConnectionKind.ACCESS_CONNECTION : (conn.eContainer as ComponentImplementation).remove(componentImplementation_OwnedAccessConnection, conn)
			case ConnectionKind.FEATURE_CONNECTION : (conn.eContainer as ComponentImplementation).remove(componentImplementation_OwnedFeatureConnection, conn)
			case ConnectionKind.FEATURE_GROUP_CONNECTION : (conn.eContainer as ComponentImplementation).remove(componentImplementation_OwnedFeatureGroupConnection, conn)
			case ConnectionKind.PARAMETER_CONNECTION : (conn.eContainer as ComponentImplementation).remove(componentImplementation_OwnedParameterConnection, conn)
			case ConnectionKind.PORT_CONNECTION : (conn.eContainer as ComponentImplementation).remove(componentImplementation_OwnedPortConnection, conn)
			default: {}
		}
    }
    
    // Because there are no traces between PropertyValues, it is not possible to keep track of the order.
    // To maintain consistency, when adding new propertyValues, please ensure they are defined in order and not inserted in between pre-existing elements of the list.
    protected def propertyValueCreatedDIM(PropertyAssociationInstance propinst, ModalPropertyValue modpropinst, PropertyValue propvalinst) {
		val modpropadd = PropertyUtils.getDeclarativeModalPropertyValue(modpropinst)
		if (PropertyTypeUtils.isSimpleProperty(propinst.property.propertyType)) {
			simplePropertyValueCreation(propinst.property.propertyType, propvalinst, modpropadd)
		} else if (propinst.property.propertyType instanceof RecordType) {
			var recvaladd = {
				if (modpropadd.ownedValue === null) {
					modpropadd.createChild(modalPropertyValue_OwnedValue, recordValue)	
				} else if (!(modpropadd.ownedValue instanceof RecordValue)) {
					modpropadd.ownedValue.remove()
					modpropadd.createChild(modalPropertyValue_OwnedValue, recordValue)
				} else {
					modpropadd.ownedValue
				} as RecordValue
			}
			var basicpropinst = propvalinst.eContainer as BasicPropertyAssociation
			var basicpropadd = recvaladd.createChild(recordValue_OwnedFieldValue, basicPropertyAssociation) as BasicPropertyAssociation
			basicpropadd.set(basicPropertyAssociation_Property, basicpropinst.property)
			simplePropertyValueCreation(basicpropinst.property.propertyType, propvalinst, basicpropadd)
		} else {
			var listvaladd = {
				if (modpropadd.ownedValue === null) {
					modpropadd.createChild(modalPropertyValue_OwnedValue, listValue)	
				} else if (!(modpropadd.ownedValue instanceof ListValue)) {
					modpropadd.ownedValue.remove()
					modpropadd.createChild(modalPropertyValue_OwnedValue, listValue)
				} else {
					modpropadd.ownedValue
				} 
			} as ListValue			
			if (propvalinst.eContainer instanceof BasicPropertyAssociation) {
				var listvalinst = modpropinst.ownedValue as ListValue
				var basicpropinst = propvalinst.eContainer as BasicPropertyAssociation
				var recvalinst = basicpropinst.eContainer as RecordValue
				var recvaladd = {
					if (listvalinst.ownedListElements.indexOf(recvalinst) < listvaladd.ownedListElements.size) {
						listvaladd.ownedListElements.get(listvalinst.ownedListElements.indexOf(recvalinst))
					} else {
						listvaladd.createChild(listValue_OwnedListElement, recordValue)		
					}
				}
				var basicpropadd = recvaladd.createChild(recordValue_OwnedFieldValue, basicPropertyAssociation) as BasicPropertyAssociation
				basicpropadd.set(basicPropertyAssociation_Property, basicpropinst.property)
				simplePropertyValueCreation(basicpropinst.property.propertyType, propvalinst, basicpropadd)
			} else {
				simplePropertyValueCreation(propinst.property.propertyType, propvalinst, listvaladd)
			}
		}
    }
    
    protected def propertyValueUpdatedDIM(PropertyValue propvalinst, PublicPackageSection aadlPackage) {
		var propval = PropertyUtils.getDeclarativePropertyValue(propvalinst)
		if (propvalinst instanceof StringLiteral) {
			propval.set(stringLiteral_Value, (propvalinst as StringLiteral).value)
		} else if (propvalinst instanceof IntegerLiteral) {
			propval.set(integerLiteral_Value, (propvalinst as IntegerLiteral).value)
			propval.set(integerLiteral_Base, (propvalinst as IntegerLiteral).base)
			propval.set(numberValue_Unit, (propvalinst as IntegerLiteral).unit)
		} else if (propvalinst instanceof RealLiteral) {
			propval.set(realLiteral_Value, (propvalinst as RealLiteral).value)
			propval.set(numberValue_Unit, (propvalinst as RealLiteral).unit)
		} else if (propvalinst instanceof ClassifierValue) {
			if ((propvalinst as ClassifierValue).classifier !== null && LibraryUtils.isLibraryClassifier((propvalinst as ClassifierValue).classifier as ComponentClassifier, aadlPublicPackage)) {
				aadlPublicPackage.add(packageSection_ImportedUnit, (propvalinst as ClassifierValue).classifier.eContainer.eContainer as ModelUnit)		
			}
			propval.set(classifierValue_Classifier, (propvalinst as ClassifierValue).classifier)
		} else if (propvalinst instanceof BooleanLiteral) {
			propval.set(booleanLiteral_Value, (propvalinst as BooleanLiteral).getValue())
		} else if (propvalinst instanceof RangeValue) {
			propval.set(rangeValue_Delta, (propvalinst as RangeValue).delta)
			rangeValueDefinition(propvalinst, propval as RangeValue)
		} else if (propvalinst instanceof ComputedValue) {
			propval.set(computedValue_Function, (propvalinst as ComputedValue).function)
		} else if (propvalinst instanceof NamedValue) {
			propval.set(namedValue_NamedValue, (propvalinst as NamedValue).namedValue)
		} else if (propvalinst instanceof InstanceReferenceValue) {
			val propassocinst = PropertyUtils.getContainingPropertyAssociationInstance(propvalinst)
			if (InstanceHierarchyUtils.getAllParentInstanceObject((propvalinst as InstanceReferenceValue).referencedInstanceObject).contains(propassocinst.eContainer)) {
				if ((propval as ContainedNamedElement).path !== null) {
					(propval as ContainedNamedElement).path.remove()	
				}
				appliesToPathDefinition(propval as ContainedNamedElement, propassocinst.eContainer as InstanceObject, (propvalinst as InstanceReferenceValue).referencedInstanceObject)
			} else {
				var propassoc = propassocinst.propertyAssociation as PropertyAssociation
				var objinst = InstanceHierarchyUtils.getLowestCommonParentInstanceObject((propvalinst as InstanceReferenceValue).referencedInstanceObject, propassocinst.eContainer as InstanceObject)
				propassoc.moveTo(TraceUtils.getDeclarativeElement(objinst), namedElement_OwnedPropertyAssociation)
				propassoc.appliesTos.clear()
				var contelem = (propassoc as PropertyAssociation).createChild(propertyAssociation_AppliesTo, containedNamedElement)
				appliesToPathDefinition(contelem as ContainedNamedElement, objinst, propassocinst.eContainer as InstanceObject)
				if ((propval as ContainedNamedElement).path !== null) {
					(propval as ContainedNamedElement).path.remove()	
				}
				appliesToPathDefinition(propval as ContainedNamedElement, objinst, (propvalinst as InstanceReferenceValue).referencedInstanceObject)
				LOGGER.logInfo("DIM: Property association of property "+propassoc.property.name+" moved due to change in ReferenceValue LCPCI position")
			}
		}
	}
}