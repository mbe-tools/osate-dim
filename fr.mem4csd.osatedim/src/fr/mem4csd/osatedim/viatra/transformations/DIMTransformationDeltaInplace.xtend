package fr.mem4csd.osatedim.viatra.transformations

import fr.mem4csd.osatedim.preference.DIMPreferences
import fr.mem4csd.osatedim.viatra.queries.FindConnection
import fr.mem4csd.osatedim.viatra.queries.FindConnectionReference
import fr.mem4csd.osatedim.viatra.queries.FindDerivedMode
import fr.mem4csd.osatedim.viatra.queries.FindFeature
import fr.mem4csd.osatedim.viatra.queries.FindModalProperty
import fr.mem4csd.osatedim.viatra.queries.FindMode
import fr.mem4csd.osatedim.viatra.queries.FindModeTransition
import fr.mem4csd.osatedim.viatra.queries.FindProperty
import fr.mem4csd.osatedim.viatra.queries.FindPropertyValue
import fr.mem4csd.osatedim.viatra.queries.FindSubcomponent
import fr.mem4csd.osatedim.viatra.queries.FindSystem
import fr.mem4csd.osatedim.utils.ConnectionUtils
import fr.mem4csd.osatedim.utils.FeatureUtils
import fr.mem4csd.osatedim.utils.LibraryUtils
import fr.mem4csd.osatedim.utils.ModeUtils
import fr.mem4csd.osatedim.utils.PropertyUtils
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine
import org.eclipse.viatra.transformation.evm.specific.Lifecycles
import org.eclipse.viatra.transformation.evm.specific.crud.CRUDActivationStateEnum
import org.eclipse.viatra.transformation.evm.specific.resolver.InvertedDisappearancePriorityConflictResolver
import org.eclipse.viatra.transformation.runtime.emf.modelmanipulation.SimpleModelManipulations
import org.eclipse.viatra.transformation.runtime.emf.transformation.eventdriven.EventDrivenTransformation
import org.osate.aadl2.AbstractFeature
import org.osate.aadl2.AbstractImplementation
import org.osate.aadl2.AbstractType
import org.osate.aadl2.ComponentCategory
import org.osate.aadl2.ComponentClassifier
import org.osate.aadl2.ComponentImplementation
import org.osate.aadl2.ComponentType
import org.osate.aadl2.DirectedFeature
import org.osate.aadl2.DirectionType
import org.osate.aadl2.FeatureConnection
import org.osate.aadl2.ModalPropertyValue
import org.osate.aadl2.Mode
import org.osate.aadl2.ModeBinding
import org.osate.aadl2.PropertyAssociation
import org.osate.aadl2.PropertySet
import org.osate.aadl2.PublicPackageSection
import org.osate.aadl2.instance.ComponentInstance
import org.osate.aadl2.instance.ConnectionInstance
import org.osate.aadl2.instance.ConnectionReference
import org.osate.aadl2.instance.FeatureInstance
import org.osate.aadl2.instance.InstanceObject
import org.osate.aadl2.instance.PropertyAssociationInstance
import org.osate.aadl2.instance.SystemInstance
import org.osate.aadl2.instance.SystemOperationMode
import org.osate.aadl2.modelsupport.FileNameConstants
import org.osate.aadl2.modelsupport.util.AadlUtil
import fr.mem4csd.osatedim.utils.DIMLogger
import org.osate.aadl2.Feature
import org.osate.aadl2.instance.FeatureCategory
import org.osate.aadl2.DataAccess

class DIMTransformationDeltaInplace extends DIMTransformationRulesDelta {
	
	protected extension EventDrivenTransformation transformation
	
	new(SystemInstance topSystemInst, ViatraQueryEngine engine, DIMPreferences preferences, PropertySet dimProperties, DIMLogger logger) {     
        this.topSystemInst = topSystemInst
        this.aadlPublicPackage = topSystemInst.componentClassifier.owner as PublicPackageSection
        this.engine = engine
        this.preferences = preferences
		this.dimPropertySet  = dimProperties
		this.LOGGER = logger;
		prepare(engine)
        createTransformation
    }
     
    def execute() {
    	transformation.executionSchema.startUnscheduledExecution
    }
    
    def dispose() {
    	if (transformation !== null) {
        	transformation.executionSchema.dispose
        }
    	topSystemInst.eResource.save(null)
    	aadlPublicPackage.eResource.save(null)
    }
	
 	private def createTransformation() {
 		this.manipulation = new SimpleModelManipulations(engine)
		var fixedPriorityResolver = new InvertedDisappearancePriorityConflictResolver
		fixedPriorityResolver.setPriority(topSystemInstance2Declarative.ruleSpecification, 1)
		fixedPriorityResolver.setPriority(componentInstance2Declarative.ruleSpecification, 2)
		fixedPriorityResolver.setPriority(featureInstance2Declarative.ruleSpecification, 3)
		fixedPriorityResolver.setPriority(connectionInstance2ConnectionReferences.ruleSpecification, 4)
		fixedPriorityResolver.setPriority(connectionReference2Declarative.ruleSpecification, 5)
		fixedPriorityResolver.setPriority(modeInstance2Declarative.ruleSpecification, 6)
		fixedPriorityResolver.setPriority(derivedModeInstance2Declarative.ruleSpecification, 7)
		fixedPriorityResolver.setPriority(modeTransitionInstance2Declarative.ruleSpecification, 8)
		fixedPriorityResolver.setPriority(propertyInstance2Declarative.ruleSpecification, 9)
		fixedPriorityResolver.setPriority(modalProperty2Declarative.ruleSpecification, 10)
		fixedPriorityResolver.setPriority(propertyval2declarative.ruleSpecification, 11)
//		fixedPriorityResolver.setPriority(flowspecinstance2declarative.ruleSpecification, 13)
		
		transformation = EventDrivenTransformation
		.forEngine(engine)
		.setConflictResolver(fixedPriorityResolver)
		.addRule(topSystemInstance2Declarative)
		.addRule(componentInstance2Declarative)
		.addRule(featureInstance2Declarative)
		.addRule(connectionInstance2ConnectionReferences)
		.addRule(connectionReference2Declarative)
		.addRule(modeInstance2Declarative)
		.addRule(derivedModeInstance2Declarative)
		.addRule(modeTransitionInstance2Declarative)
		.addRule(propertyInstance2Declarative)
		.addRule(modalProperty2Declarative)
		.addRule(propertyval2declarative)
//		.addRule(flowspecinstance2declarative)
		.build
	} 
    ///////////////////////////////////////////////
    // TOP-LEVEL SYSTEM INSTANCE DEINSTANTIATION //
    ///////////////////////////////////////////////
	val topSystemInstance2Declarative = createRule(FindSystem.Matcher.querySpecification)
	.action(CRUDActivationStateEnum.UPDATED) [
		val ComponentImplementation compimp = systeminst.componentImplementation
		if (compimp.getTypeName+"_"+compimp.getImplementationName+FileNameConstants.INSTANCE_MODEL_POSTFIX !== systeminst.name) {
			if (systeminst.name.startsWith(compimp.getTypeName+"_") && systeminst.name.endsWith(FileNameConstants.INSTANCE_MODEL_POSTFIX)) {
				compimp.set(namedElement_Name, compimp.getTypeName+"."+systeminst.name.substring(compimp.getTypeName.length + 1, systeminst.name.length - FileNameConstants.INSTANCE_MODEL_POSTFIX.length))
			} else {
				LOGGER.logWarning("DIM: Updated name of top-level system instance "+systeminst.name+" is not legal")
				systeminst.set(namedElement_Name, compimp.getTypeName+"_"+compimp.getImplementationName+FileNameConstants.INSTANCE_MODEL_POSTFIX)
				LOGGER.logCancel("DIM: WARNING: Top-level system instance name reverted to "+systeminst.name)
			}
		}
    ].addLifeCycle(Lifecycles.getDefault(true, true)).build
    
    ////////////////////////////////////////
    // COMPONENT INSTANCE DEINSTANTIATION //
    ////////////////////////////////////////
    val componentInstance2Declarative = createRule(FindSubcomponent.Matcher.querySpecification)
	.action(CRUDActivationStateEnum.CREATED) [
		if (subcompinst.subcomponent === null) {
			componentInstanceCreatedDIM(subcompinst)
			elementCreationPropertyInheritance(subcompinst)
			componentCreationModeInheritance(subcompinst)
			LOGGER.logInfo("DIM: Component instance "+subcompinst.name+" de-instantiated")
		} else {
			LOGGER.logInfo("DIM: Declarative of component instance "+subcompinst.name+" already exists!")
		}
	].action(CRUDActivationStateEnum.UPDATED) [
		// Component name update
		var String name = subcompinst.name
		if (!name.isNullOrEmpty) {
			var subcomp = subcompinst.subcomponent
			if (subcomp.name.isNullOrEmpty || !name.contentEquals(subcompinst.subcomponent.name)) {
				if (LibraryUtils.isAffectingInstanceObject(subcompinst, compinst, aadlPublicPackage, engine, preferences.modifyReused, true)) {
					denyImplementnUpdatePropagn(subcompinst, compinst, true)
				}
				do {
					subcomp.set(namedElement_Name, name)
					subcomp = subcomp.refined	
				} while (subcomp !== null)
				var classifier = subcompinst.classifier
				if (classifier.name !== null && classifier.name.contentEquals("null_ext")) {
					if (classifier instanceof ComponentType) {
						classifier.set(namedElement_Name, name)
					} else {
						classifier.set(namedElement_Name, name+".impl")
					}	
				}
				LOGGER.logInfo("DIM: Name of component instance "+name+" updated")
			}
		}
		// Component category refinement
		if (subcompinst.category !== subcompinst.subcomponent.category) {
			if (subcompinst.subcomponent.category == ComponentCategory.ABSTRACT) {
				var newcompimp = extendParentImplementationForRefinement(subcompinst)
				var newsubcomp = createSubcomponentInsideComponentImplementation(newcompimp, subcompinst)
				newsubcomp.set(namedElement_Name, subcompinst.subcomponent.name)
				newsubcomp.set(subcomponent_Refined, subcompinst.subcomponent)
				var ComponentClassifier newsubcompclass = {
					if (subcompinst.classifier instanceof ComponentType) {
						createComponentType(subcompinst.category)
					} else {
						createComponentImplementation(subcompinst.category)
					}
				} as ComponentClassifier
				classifierCreationDIMPropertyAddition(newsubcompclass)
				newsubcompclass.set(namedElement_Name, subcompinst.classifier.name+"_ext")
				if (subcompinst.classifier instanceof AbstractType) {
					newsubcompclass.set(componentType_Extended, subcompinst.classifier)
				} else if (subcompinst.classifier instanceof AbstractImplementation) {
					newsubcompclass.set(componentImplementation_Extended, subcompinst.classifier)
					newsubcompclass.set(componentImplementation_Type, (subcompinst.classifier as ComponentImplementation).type)
				}	
				subcompinst.set(componentInstance_Classifier, newsubcompclass)
				subcompinst.set(componentInstance_Subcomponent, newsubcomp)
				setSubcomponentType(subcompinst, newsubcompclass)
				LOGGER.logInfo("DIM: Category of component instance "+subcompinst.name+" refined")	
			} else {
				subcompinst.set(componentInstance_Category, subcompinst.subcomponent.category)
				LOGGER.logWarning("DIM: Category update of component instance "+subcompinst.name+" not refine-able! Reverted back to original")	
			}
		}
		// Component classifier refinement
		if (subcompinst.classifier !== subcompinst.subcomponent.classifier) {
			if (LibraryUtils.isClassifierRefinableFrom(subcompinst.classifier, subcompinst.subcomponent.classifier)) {
				if (subcompinst.category !== subcompinst.classifier.category) {
					subcompinst.set(componentInstance_Category, subcompinst.classifier.category)	
				}
				var newcompimp = extendParentImplementationForRefinement(subcompinst)
				var newsubcomp = createSubcomponentInsideComponentImplementation(newcompimp, subcompinst)
				newsubcomp.set(namedElement_Name, subcompinst.subcomponent.name)
				newsubcomp.set(subcomponent_Refined, subcompinst.subcomponent)
				subcompinst.set(componentInstance_Subcomponent, newsubcomp)
				setSubcomponentType(subcompinst, subcompinst.classifier)
				LOGGER.logInfo("DIM: Classifier of component instance "+subcompinst.name+" refined")
			} else {
				subcompinst.set(componentInstance_Classifier, subcompinst.subcomponent.classifier)
				LOGGER.logWarning("DIM: Classifier update of component instance "+subcompinst.name+" not refine-able! Reverted back to original")
			}	
		}
    ].action(CRUDActivationStateEnum.DELETED) [
    	if (LibraryUtils.isAffectingInstanceObject(subcompinst,compinst,aadlPublicPackage,engine,preferences.modifyReused, true)) {
			denyImplementnUpdatePropagn(subcompinst, subcompinst.eContainer as ComponentInstance, true)
		}
		var subcomp = subcompinst.subcomponent
		do {
			componentInstanceDeletedDIM(subcomp)
			subcomp = subcomp.refined
		} while (subcomp !== null)
    	LOGGER.logInfo("DIM: Component instance "+subcompinst.name+" deleted")	
    ].addLifeCycle(Lifecycles.getDefault(true, true)).build
    
    //////////////////////////////////////
    // FEATURE INSTANCE DEINSTANTIATION //
    //////////////////////////////////////
    val featureInstance2Declarative = createRule(FindFeature.Matcher.querySpecification) 
	.action(CRUDActivationStateEnum.CREATED) [
		if (featinst.feature === null) {
			featureInstanceCreatedDIM(featinst)
			elementCreationPropertyInheritance(featinst)
			LOGGER.logInfo("DIM: Feature instance "+featinst.name+" de-instantiated")
		} else {
			LOGGER.logInfo("DIM: Declarative of feature instance "+featinst.name+" already exists!")
		}
	].action(CRUDActivationStateEnum.UPDATED) [
		// Feature name update
		if (featinst.feature.name !== featinst.name) {
			denyFeatureUpdatePropagn(featinst)
			var feat = featinst.feature
			do {
				feat.set(namedElement_Name,featinst.name)
				feat = feat.refined	
			} while (feat !== null)
			LOGGER.logInfo("DIM: Name of feature instance "+featinst.name+" updated")
		}
		// Feature direction change
		if (featinst.feature instanceof DirectedFeature) {
			if (featinst.direction !== null) {
				if ((featinst.direction == DirectionType.IN && !((featinst.feature as DirectedFeature).in && !(featinst.feature as DirectedFeature).out)) ||
				(featinst.direction == DirectionType.OUT && !(!(featinst.feature as DirectedFeature).in && (featinst.feature as DirectedFeature).out)) ||
				(featinst.direction == DirectionType.IN_OUT && !((featinst.feature as DirectedFeature).in && (featinst.feature as DirectedFeature).out))) {
					var compinst = featinst.eContainer as ComponentInstance
					// Feature direction refinement
					if (FeatureUtils.isRefinedFeatureDirection(featinst.direction, FeatureUtils.getFeatureDirection(featinst.feature as DirectedFeature))) {
						if (compinst !== topSystemInst && LibraryUtils.isAffectingInstanceObject(compinst, compinst.eContainer as ComponentInstance, aadlPublicPackage, engine, preferences.modifyReused, false)) {
							denyImplementnUpdatePropagn(compinst, compinst.eContainer as ComponentInstance, false)
						}
						val comptype = {
							switch (compinst.classifier) {
								case ComponentType : compinst.classifier
								case ComponentImplementation : (compinst.classifier as ComponentImplementation).type
							}
						}
						val reftype =  createComponentType(compinst.category)
						reftype.set(namedElement_Name, comptype.name+"_ext")
						reftype.set(componentType_Extended, comptype)
						var reffeat = createFeature(compinst, reftype, featinst)
						reffeat.set(namedElement_Name, featinst.feature.name)
						reffeat.set(feature_Refined, featinst.feature)
						if (featinst.direction == DirectionType.IN) {
							reffeat.set(directedFeature_In,true)
							reffeat.set(directedFeature_Out,false)
						} else if (featinst.direction == DirectionType.OUT) {
							reffeat.set(directedFeature_In,false)
							reffeat.set(directedFeature_Out,true)
						} else {
							reffeat.set(directedFeature_In,true)
							reffeat.set(directedFeature_Out,true)
						}
						featinst.set(featureInstance_Feature, reftype)
						switch (compinst.classifier) {
							case ComponentType : {
								compinst.set(componentInstance_Classifier, reftype)
								setSubcomponentType(compinst, reftype)
							}
							case ComponentImplementation : compinst.classifier.set(componentImplementation_Type, reftype)
						}
						LOGGER.logInfo("DIM: Direction of feature instance "+featinst.name+" refined")
					// Feature direction update
					} else {
						denyFeatureUpdatePropagn(featinst)
						var currfeat = featinst.feature
						do {
							if (featinst.direction == DirectionType.IN) {
								currfeat.set(directedFeature_In,true)
								currfeat.set(directedFeature_Out,false)
							} else if (featinst.direction == DirectionType.OUT) {
								currfeat.set(directedFeature_In,false)
								currfeat.set(directedFeature_Out,true)
							} else {
								currfeat.set(directedFeature_In,true)
								currfeat.set(directedFeature_Out,true)
							}
							currfeat = currfeat.refined
						} while (currfeat !== null && !FeatureUtils.isRefinedFeatureDirection(featinst.direction, FeatureUtils.getFeatureDirection(currfeat as DirectedFeature)))
						LOGGER.logInfo("DIM: Direction of feature instance "+featinst.name+" updated")
					}
				}
			}				 
		}
		// Feature kind change
		if (featinst.category !== FeatureUtils.getFeatureCategory(featinst.feature)) {
			var compinst = featinst.eContainer as ComponentInstance
			// Feature kind refinement
			if (featinst.feature instanceof AbstractFeature) {
				if (compinst !== topSystemInst && LibraryUtils.isAffectingInstanceObject(compinst, compinst.eContainer as ComponentInstance, aadlPublicPackage, engine, preferences.modifyReused, false)) {
					denyImplementnUpdatePropagn(compinst, compinst.eContainer as ComponentInstance, false)
				}
				val comptype = {
					switch (compinst.classifier) {
						case ComponentType : compinst.classifier
						case ComponentImplementation : (compinst.classifier as ComponentImplementation).type
					}
				}
				val reftype = createComponentType(compinst.category)
				reftype.set(namedElement_Name, comptype.name+"_ext")
				reftype.set(componentType_Extended, comptype)
				var reffeat = createFeature(compinst, reftype, featinst)
				reffeat.set(namedElement_Name, featinst.feature.name)
				reffeat.set(feature_Refined, featinst.feature)
				reffeat.set(directedFeature_In, (featinst.feature as DirectedFeature).in)
				reffeat.set(directedFeature_Out, (featinst.feature as DirectedFeature).out)
				featinst.set(featureInstance_Feature, reftype)
				switch (compinst.classifier) {
					case ComponentType : {
						compinst.set(componentInstance_Classifier, reftype)
						setSubcomponentType(compinst, reftype)
					}
					case ComponentImplementation : compinst.classifier.set(componentImplementation_Type, reftype)
				}
				LOGGER.logInfo("DIM: Category of feature instance "+featinst.name+" refined")
			// Feature kind update
			} else {		
				denyFeatureUpdatePropagn(featinst)
				var newrefineefeat = featinst.feature
				for (Feature oldfeat : featinst.feature.allFeatureRefinements.reverse) {
					val feattype = oldfeat.eContainer as ComponentType
					var newfeat = createFeature(compinst, feattype, featinst)
					if (oldfeat instanceof DirectedFeature && newfeat instanceof DirectedFeature) {
						newfeat.set(directedFeature_In, (oldfeat as DirectedFeature).in)
						newfeat.set(directedFeature_Out, (oldfeat as DirectedFeature).out)
					}
					for (PropertyAssociation propassoc :  oldfeat.ownedPropertyAssociations) {
						propassoc.moveTo(newfeat as Resource)
					}
					if (oldfeat == featinst.feature) {
						featinst.set(featureInstance_Feature, newfeat)
					} else {
						newfeat.set(feature_Refined, newrefineefeat)
						newrefineefeat = newfeat as Feature	
					}
//					for (FindParameterConnection.Match match : engine.findParameterConnection.getAllMatches(null, oldfeat)) {
//						var parconn = match.parconn
//						if (parconn.source.connectionEnd == oldfeat) {
//							parconn.source.set(connectedElement_ConnectionEnd, newfeat)
//						} else {
//							parconn.destination.set(connectedElement_ConnectionEnd, newfeat)
//						}
//					}
					featureInstanceDeletedDIM(oldfeat)
				}
				LOGGER.logInfo("DIM: Category of feature instance "+featinst.name+" updated")
			}
		}
		// Access Feature Type Update
		if (featinst.category == FeatureCategory.DATA_ACCESS && featinst.type !== null) {
			if (featinst.type.classifier !== (featinst.feature as DataAccess).dataFeatureClassifier) {
				denyFeatureUpdatePropagn(featinst)
				var feat = featinst.feature
				do {
					(feat as DataAccess).set(dataAccess_DataFeatureClassifier, featinst.type.classifier)
					feat = feat.refined
				} while (feat !== null && feat instanceof DataAccess)
					LOGGER.logInfo("DIM: Classifier of dataAccess feature instance "+featinst.name+" updated")
				}	
		}	
    ].action(CRUDActivationStateEnum.DELETED) [
    	denyFeatureUpdatePropagn(featinst)
    	var feat = featinst.feature
    	do {
    		featureInstanceDeletedDIM(feat)
    		feat = feat.refined	
    	} while (feat !== null)
    	LOGGER.logInfo("DIM: Feature instance "+featinst.name+" deleted")	
    ].addLifeCycle(Lifecycles.getDefault(true, true)).build

    /////////////////////////////////////////    	
	// CONNECTION INSTANCE DEINSTANTIATION //
	/////////////////////////////////////////
    
    val connectionInstance2ConnectionReferences = createRule(FindConnection.Matcher.querySpecification)
	.action(CRUDActivationStateEnum.CREATED) [
		connectionInstanceCreatedDIM(conninst)
		elementCreationPropertyInheritance(conninst)
		connectionCreationModeInheritance(conninst)
	].action(CRUDActivationStateEnum.UPDATED) [
		// Connection Destination update
		if (conninst.destination !== conninst.connectionReferences.last.destination) {
			connectionInstanceCreatedDIM(conninst)
			LOGGER.logInfo("DIM: Destination of Connection Instance "+conninst.name+" updated")
		// Connection Source update
		} else if (conninst.source !== conninst.connectionReferences.get(0).source) {
			var complist = ConnectionUtils.getPathBetweenConnectionInstanceEnds(conninst.destination,conninst.source)
			if (conninst.connectionReferences.isNullOrEmpty) {
				conninst.createChild(connectionInstance_ConnectionReference, connectionReference)
			}
			var connreflist = conninst.connectionReferences.reverse
			for (i : 0..< max(connreflist.size, complist.size)) {
				if (i >= complist.size && connreflist.get(i) !== null) {
					conninst.remove(connectionInstance_ConnectionReference, connreflist.get(i))	
				} else {
					var connref = {
						if (connreflist.get(i) !== null) {
							connreflist.get(i)
						} else {
							var connreflocal = conninst.createChild(connectionInstance_ConnectionReference, connectionReference)
							conninst.changeIndex(connectionInstance_ConnectionReference, conninst.connectionReferences.size - 1, 0)
							connreflocal
						}	
					} as ConnectionReference
					// Context
					if (connref.context === null) {
						connref.set(connectionReference_Context, complist.get(i))
						LOGGER.logInfo("DIM: Context of the "+ i.toString +"th (reverse) connection reference for connection "+ conninst.name +" added")
					} else if (connref.context !== complist.get(i)) {
						connref.set(connectionReference_Context, complist.get(i))
						LOGGER.logInfo("DIM: Context of the "+ i.toString +"th (reverse) connection reference for connection "+ conninst.name +" changed")
						connref.set(connectionReference_Source, null)
						connref.set(connectionReference_Destination, null)
					}
					// Destination Feature
					if (connref.destination === null) {
						if (i == 0) {
							connref.set(connectionReference_Destination, conninst.destination)
							LOGGER.logInfo("DIM: Destination of the "+ i.toString +"th (reverse) connection reference for connection "+ conninst.name +" added")	 
						} else if (conninst.connectionReferences.get(conninst.connectionReferences.indexOf(connref) + 1).source !== null) {
							connref.set(connectionReference_Destination, conninst.connectionReferences.get(conninst.connectionReferences.indexOf(connref) + 1).source)
							LOGGER.logInfo("DIM: Destination of the "+ i.toString +"th (reverse) connection reference for connection "+ conninst.name +" added")		
						}
					} else {
						if (i == 0 && connref.destination !== conninst.destination) {
							connref.set(connectionReference_Destination, conninst.destination)
							LOGGER.logWarning("DIM: Destination of the "+ i.toString +"th (reverse) connection reference for connection "+ conninst.name +" changed")
 						} else if (i !== 0 && conninst.connectionReferences.get(conninst.connectionReferences.indexOf(connref) + 1).source !== null && connref.source !== conninst.connectionReferences.get(conninst.connectionReferences.indexOf(connref) + 1).source) {
 							connref.set(connectionReference_Destination, conninst.connectionReferences.get(conninst.connectionReferences.indexOf(connref) + 1).source)
 							LOGGER.logWarning("DIM: Destination of the "+ i.toString +"th (reverse) connection reference for connection "+ conninst.name +" changed")
 						}
					}	
					// Source feature
					if (connref.source === null) {
						if (i == complist.size - 1) {
							connref.set(connectionReference_Source, conninst.source)
							LOGGER.logInfo("DIM: Source of the "+ i.toString +"th connection reference for connection "+ conninst.name +" added")
						} else if (preferences.createInterFeatures) {
							var feataddcompinst = {
								if (complist.get(i+1).eContainer() == complist.get(i)) {
									complist.get(i+1)
								} else {
									complist
								}
							} as FeatureInstance
							var srcfeatinst = feataddcompinst.createChild(componentInstance_FeatureInstance,featureInstance) as FeatureInstance
							if (conninst.source instanceof FeatureInstance) {
								srcfeatinst.set(featureInstance_Category, (conninst.source as FeatureInstance).category)	
							} else {
								srcfeatinst.set(featureInstance_Category, (conninst.destination as FeatureInstance).category)	
							}
							if (conninst.bidirectional == true) {
								srcfeatinst.set(featureInstance_Direction, DirectionType.IN_OUT)	
							} else {
								if (complist.get(i+1).eContainer() == complist.get(i)) {
									srcfeatinst.set(featureInstance_Direction, DirectionType.IN)
								} else {
									srcfeatinst.set(featureInstance_Direction, DirectionType.OUT)
								}
							}
							srcfeatinst.set(namedElement_Name, conninst.name+"_feat")
							connref.set(connectionReference_Source, srcfeatinst)
							LOGGER.logInfo("DIM: Source of the "+ i.toString +"th connection reference for connection "+ conninst.name +" created")
						}			
					} else if (i==complist.size-1 && connref.source !== conninst.source) {
						connref.set(connectionReference_Source, conninst.source)
						LOGGER.logWarning("DIM: Destination of the connection reference changed")
 					} //TODO: the corner case where the source may not be null, but may still be wrong is not accounted for
 				}
 				LOGGER.logInfo("DIM: Connection Instance "+conninst.name+" transformed to connection reference")
 			}
			LOGGER.logInfo("DIM: Source of Connection Instance "+conninst.name+" updated")
		}
		if (conninst.connectionReferences.get(0).connection !== null) {
			// Connection Kind change
			if (conninst.kind !== ConnectionUtils.getConnectionKind(conninst.connectionReferences.get(0).connection)) {
				// Connection Kind refinement
				if (conninst.connectionReferences.get(0).connection instanceof FeatureConnection) {
					refineConnection(conninst)
					LOGGER.logInfo("DIM: Kind of Connection Instance "+conninst.name+" refined")
				// Connection Kind update
				} else {
					for (ConnectionReference connref : conninst.connectionReferences) {
						if (LibraryUtils.isAffectingInstanceObject(connref,connref.context,aadlPublicPackage,engine,preferences.modifyReused, true)) {
							denyImplementnUpdatePropagn(connref, connref.context, true)
						}
						val oldconn = connref.connection
						val connimp = oldconn.eContainer as ComponentImplementation
						var newconn = createConnection(conninst.kind, connimp)
						newconn.set(namedElement_Name, oldconn.name)
						populateNewConnection(newconn, conninst,connref)
						for (PropertyAssociation propassoc :  oldconn.ownedPropertyAssociations) {
							propassoc.moveTo(newconn as Resource)
						}
						connectionInstanceDeletedDIM(connref, oldconn)
					} 
				}
			}
			// Connection Bidirectional change
			if (conninst.bidirectional !== conninst.connectionReferences.get(0).connection.bidirectional) {
				// Connection Bidirectional refinement
				if (conninst.connectionReferences.get(0).connection.bidirectional == true) {
					refineConnection(conninst)
					LOGGER.logInfo("DIM: Bidirectional of Connection Instance "+conninst.name+" refined")
					// Connection Bidirectional update
				} else {
//					for (ConnectionReference connref : conninst.connectionReferences) {
//						if (LibraryUtils.isAffectingInstanceObject(connref,connref.context,aadlPublicPackage,engine,preferences.modifyReused, true)) {
//							denyImplementnUpdatePropagn(connref, connref.context, true)
//						}
//						connref.connection.set(connection_Bidirectional, conninst.bidirectional)
//					}
					conninst.set(connectionInstance_Bidirectional, conninst.connectionReferences.get(0).connection.bidirectional)
					LOGGER.logWarning("DIM: WARNING: Bidirectional update of Connection Instance "+conninst.name+" is not refine-able! Reverted back to original")	
				}
			}
		}
    ].action(CRUDActivationStateEnum.DELETED) [
    	// Need to do nothing since relationship is containment, and all connection references are automatically deleted with the Connection instance.
    	LOGGER.logInfo("DIM: Connection "+ conninst.name +" deleted")
    ].addLifeCycle(Lifecycles.getDefault(true, true)).build
    
    //////////////////////////////////////////    	
	// CONNECTION REFERENCE DEINSTANTIATION //
	//////////////////////////////////////////
	
    val connectionReference2Declarative = createRule(FindConnectionReference.Matcher.querySpecification)
    .action(CRUDActivationStateEnum.CREATED) [
    	if (connref.connection === null) {
    		connectionReferenceFoundDIM(connref)			
			LOGGER.logInfo("DIM: Connection reference "+(connref.eContainer as ConnectionInstance).name+" de-instantiated")
    	}
//    ].action(CRUDActivationStateEnum.UPDATED) [
//    	// ConnectionReference Context update
//    	if (!LibraryUtils.hasContributingClassifier(connref.context, connref.connection.eContainer as ComponentImplementation)) {
//    		var ComponentInstance oldcontext = null
//    		for (FindComponentInstance.Match match : engine.findComponentInstance.getAllMatches(oldcontext)) {
//    			if (LibraryUtils.hasContributingClassifier(match.compinst,connref.connection.eContainer as ComponentImplementation) &&
//    				InstanceHierarchyUtils.getAllParentComponentInstance(match.compinst).contains(connref.eContainer.eContainer as ComponentInstance)) {
//    				oldcontext = match.compinst
//    			}
//     		}
//     		if (LibraryUtils.isAffectingInstanceObject(connref,oldcontext,aadlPublicPackage,engine,preferences.modifyReused)) {
//				denyImplementnUpdatePropagn(connref, oldcontext)
//			}
//     		connectionAdditionPreparation(connref.context)
//    		connref.connection.moveTo(connref.context.classifier as Resource)
//    	}
//    	// ConnectionReference Source update
//    	if (!ConnectionUtils.hasContributingConnectionEnd(connref.source, connref.connection.source.connectionEnd)) {
//			var connOrigin = connref.connection
//			while (connOrigin.refined !== null) {
//				connOrigin = connOrigin.refined
//			} 
//			var srcfeatOrigin = ConnectionUtils.getConnectedEndUltimateRefinee(connref.source)
//			var ComponentImplementation connImp = connOrigin.eContainer as ComponentImplementation
//			var ComponentType featType = srcfeatOrigin.eContainer as ComponentType
//			if (LibraryUtils.isClassifierExtendedFrom(connImp.type, featType)) {
//				if (LibraryUtils.isAffectingInstanceObject(connref,connref.context,aadlPublicPackage,engine,preferences.modifyReused, true)) {
//					denyImplementnUpdatePropagn(connref, connref.context, true)
//				}
//				var conn= connref.connection
//				do { 
//					populateSourceConnectedElement(conn.source, connref, connref.connection)
//					conn = conn.refined
//				} while ()	
//			} else {
//				LOGGER.log(Level.INFO, "DIM: ERROR: Change in source of "+(connref.eContainer as ConnectionInstance).name+" is invalid")
//			}   		
//    	}
//    	// ConnectionReference Destination update
//    	if (!ConnectionUtils.hasContributingConnectionEnd(connref.destination, connref.connection.destination.connectionEnd)) {
//    		if (LibraryUtils.isAffectingInstanceObject(connref,connref.context,aadlPublicPackage,engine,preferences.modifyReused, true)) {
//				denyImplementnUpdatePropagn(connref, connref.context, true)
//			}
//			var conn= connref.connection
//			do {
//				// add destination visibility condition (destination may not be visible to the connection if it is a refinement-ancestor)
//				populateDestinationConnectedElement(conn.destination, connref, connref.connection)
//				conn = conn.refined
//			} while (conn !== null)
//    	}
    ].action(CRUDActivationStateEnum.DELETED) [
    	connectionInstanceDeletedDIM(connref, connref.connection)
    	LOGGER.logInfo("DIM: Connection reference "+(connref.eContainer as ConnectionInstance).name+" deleted")	
    ].addLifeCycle(Lifecycles.getDefault(true, true)).build
    	
    ///////////////////////////////////
    // MODE INSTANCE DEINSTANTIATION //
    ///////////////////////////////////
    val modeInstance2Declarative = createRule(FindMode.Matcher.querySpecification)
    .action(CRUDActivationStateEnum.CREATED) [
    	if(modeinst.mode === null) {
    		modeInstanceCreatedDIM(modeinst) 	
    		LOGGER.logInfo("DIM: Mode instance "+modeinst.name+" de-instantiated")
    	} else {
			LOGGER.logInfo("DIM: Declarative of mode instance "+modeinst.name+" already exists!")
		}
    ].action(CRUDActivationStateEnum.UPDATED) [
    	// Mode name change
    	if (modeinst.name !== modeinst.mode.name) {
    		modeChangedParentSubcomponentDefinition(modeinst)
    		modeinst.mode.set(namedElement_Name, modeinst.name)	
    		LOGGER.logInfo("DIM: Name of mode instance "+modeinst.name+" updated")
    	}
    	// Mode initial boolean changed
    	if (modeinst.initial !== modeinst.mode.initial) {
    		modeChangedParentSubcomponentDefinition(modeinst)
    		modeinst.mode.set(mode_Initial, modeinst.initial)
    		LOGGER.logInfo("DIM: Initial value of mode instance "+modeinst.name+" updated")	
    	}
    	// Mode binding added/changed
    	var compinst = modeinst.eContainer as ComponentInstance
    	if (modeinst.derived !== compinst.classifier.derivedModes) {
    		modeChangedParentSubcomponentDefinition(modeinst)
    		if (modeinst.derived == true) {
    			(modeinst.mode.eContainer as ComponentClassifier).set(componentClassifier_DerivedModes, true)
    			var modebind = compinst.subcomponent.createChild(subcomponent_OwnedModeBinding, modeBinding)
    			modebind.set(modeBinding_DerivedMode, modeinst.mode)
    			if (!modeinst.parents.isEmpty) {
    				modebind.set(modeBinding_ParentMode, modeinst.parents.get(0).mode)	
    			}
    		} else {
    			(modeinst.mode.eContainer as ComponentClassifier).set(componentClassifier_DerivedModes, false)
    			for (ModeBinding modeBind : compinst.subcomponent.ownedModeBindings) {
    				if (modeBind.derivedMode == modeinst.mode) {
    					modeBind.eContainer.remove(subcomponent_OwnedModeBinding, modeBind)
    				}
    			}
    		}
    		LOGGER.logInfo("DIM: Derived value of mode instance "+modeinst.name+" updated")
    	}
    ].action(CRUDActivationStateEnum.DELETED) [
    	modeChangedParentSubcomponentDefinition(modeinst)
    	(modeinst.mode.eContainer as ComponentClassifier).remove(componentClassifier_OwnedMode, modeinst.mode)
    	LOGGER.logInfo("DIM: Mode instance "+modeinst.name+" deleted")
    ].addLifeCycle(Lifecycles.getDefault(true, true)).build
    
    val derivedModeInstance2Declarative = createRule(FindDerivedMode.Matcher.querySpecification)
    .action(CRUDActivationStateEnum.CREATED) [
    	if(modeinst.mode === null) {
    		modeInstanceCreatedDIM(modeinst) 	
    		LOGGER.logInfo("DIM: Mode instance "+modeinst.name+" de-instantiated")
    	} else {
			LOGGER.logInfo("DIM: Declarative of mode instance "+modeinst.name+" already exists!")
		}
    ].action(CRUDActivationStateEnum.UPDATED) [
    	modeChangedParentSubcomponentDefinition(modeinst)
    	// Mode name change
    	if (modeinst.name !== modeinst.mode.name) {
    		modeinst.mode.set(namedElement_Name, modeinst.name)	
    		LOGGER.logInfo("DIM: Name of mode instance "+modeinst.name+" updated")
    	}
    	// Mode initial boolean changed
    	if (modeinst.initial !== modeinst.mode.initial) {
    		modeinst.mode.set(mode_Initial, modeinst.initial)
    		LOGGER.logInfo("DIM: Initial value of mode instance "+modeinst.name+" updated")	
    	}
    	// Mode binding added/changed
    	var compinst = modeinst.eContainer as ComponentInstance
    	if (modeinst.derived !== compinst.classifier.derivedModes) {
    		if (modeinst.derived == true) {
    			(modeinst.mode.eContainer as ComponentClassifier).set(componentClassifier_DerivedModes, true)
    			var modebind = compinst.subcomponent.createChild(subcomponent_OwnedModeBinding, modeBinding)
    			modebind.set(modeBinding_DerivedMode, modeinst.mode)
    			if (!modeinst.parents.isEmpty) {
    				modebind.set(modeBinding_ParentMode, modeinst.parents.get(0).mode)	
    			}
    		} else {
    			(modeinst.mode.eContainer as ComponentClassifier).set(componentClassifier_DerivedModes, false)
    			for (ModeBinding modeBind : compinst.subcomponent.ownedModeBindings) {
    				if (modeBind.derivedMode == modeinst.mode) {
    					modeBind.eContainer.remove(subcomponent_OwnedModeBinding, modeBind)
    				}
    			}
    		}
    		LOGGER.logInfo("DIM: Derived value of mode instance "+modeinst.name+" updated")
    	}
    ].action(CRUDActivationStateEnum.DELETED) [
    	modeChangedParentSubcomponentDefinition(modeinst)
    	(modeinst.mode.eContainer as ComponentClassifier).remove(componentClassifier_OwnedMode, modeinst.mode)
    	LOGGER.logInfo("DIM: Mode instance "+modeinst.name+" deleted")
    ].addLifeCycle(Lifecycles.getDefault(true, true)).build
    
    /////////////////////////////////////////////
    // MODETRANSITION INSTANCE DEINSTANTIATION //
    /////////////////////////////////////////////
    val modeTransitionInstance2Declarative = createRule(FindModeTransition.Matcher.querySpecification)
    .action(CRUDActivationStateEnum.CREATED) [
    	if(modetransinst.modeTransition === null) {
    		modeTransitionInstanceCreatedDIM(modetransinst)
    		if (preferences.isInheritProperty) {
				elementCreationPropertyInheritance(modetransinst)
			}
    		LOGGER.logInfo("DIM: ModeTransition instance "+modetransinst.name+" de-instantiated")
    	} else {
			LOGGER.logInfo("DIM: Declarative of mode transition instance "+modetransinst.name+" already exists!")
		}
    ].action(CRUDActivationStateEnum.UPDATED) [
    	modeTransChangedParentSubcomponentDefinition(modetransinst)
    	// Mode transition name change
    	if (modetransinst.name !== modetransinst.modeTransition.name) {
    		modetransinst.modeTransition.set(namedElement_Name, modetransinst.name)	
    		LOGGER.logInfo("DIM: Name of mode transition instance "+modetransinst.name+" updated")
    	}
		// Mode transition source change
		if (modetransinst.source.mode !== modetransinst.modeTransition.source) {
			modetransinst.modeTransition.set(modeTransition_Source, modetransinst.source.mode)
			LOGGER.logInfo("DIM: Source of mode transition instance "+modetransinst.name+" updated")
		}
		// Mode transition destination change
		if (modetransinst.destination.mode !== modetransinst.modeTransition.destination) {
			modetransinst.modeTransition.set(modeTransition_Destination, modetransinst.destination.mode)
			LOGGER.logInfo("DIM: Destination of mode transition instance "+modetransinst.name+" updated")
		}
    ].action(CRUDActivationStateEnum.DELETED) [
    	modeTransChangedParentSubcomponentDefinition(modetransinst)
    	(modetransinst.modeTransition.eContainer as ComponentClassifier).remove(componentClassifier_OwnedModeTransition, modetransinst.modeTransition)
    	LOGGER.logInfo("DIM: ModeTransition instance "+modetransinst.name+" deleted")
    ].addLifeCycle(Lifecycles.getDefault(true, true)).build
        	
    //////////////////////////////////////////////////
	// PROPERTY ASSOCIATION INSTANCE TRANSFORMATION //
	//////////////////////////////////////////////////
    val propertyInstance2Declarative = createRule(FindProperty.Matcher.querySpecification)
	.action(CRUDActivationStateEnum.CREATED) [
		if(propinst.propertyAssociation === null) {
			if (!PropertyUtils.isInheritedProperty(propinst)) {
				propertyInstanceCreatedDIM(propinst, aadlPublicPackage)	
				if (propinst.property !== null) {
					LOGGER.logInfo("DIM: Property instance "+propinst.property.name+" de-instantiated")	
				} else {
					LOGGER.logInfo("DIM: Property instance in object "+(propinst.eContainer as InstanceObject).name+" de-instantiated")
				}
			} else {
				LOGGER.logInfo("DIM: Property instance "+propinst.property.name+" inherited")
			}
		} else {
			LOGGER.logInfo("DIM: Declarative of property instance "+propinst.property.name+" already exists!")
		}
	].action(CRUDActivationStateEnum.UPDATED)[
    	if (propinst.property !== propinst.propertyAssociation.property) {
    		if (!aadlPublicPackage.importedUnits.contains(propinst.property.eContainer)) {
    			if (aadlPublicPackage.noAnnexes === true) {
    				aadlPublicPackage.set(packageSection_NoAnnexes, false)
    			}
    			if (!AadlUtil.isPredeclaredPropertySet((propinst.property.eContainer as PropertySet).name)) {
    				aadlPublicPackage.add(packageSection_ImportedUnit, propinst.property.eContainer)
    			}
    		}
    		propinst.propertyAssociation.set(propertyAssociation_Property, propinst.property)
    		LOGGER.logInfo("DIM: Property of property instance "+propinst.property.name+" updated")
    	}
	].action(CRUDActivationStateEnum.DELETED) [
    	propinst.propertyAssociation.eContainer.remove(namedElement_OwnedPropertyAssociation,propinst.propertyAssociation)
    	LOGGER.logInfo("DIM: Property instance "+propinst.property.name+" deleted")
    ].addLifeCycle(Lifecycles.getDefault(true, true)).build
	
	/////////////////////////////////////////
	// MODAL PROPERTY VALUE TRANSFORMATION //
	/////////////////////////////////////////
	val modalProperty2Declarative = createRule(FindModalProperty.Matcher.querySpecification)
	.action(CRUDActivationStateEnum.CREATED) [
		val propinst = modpropinst.eContainer as PropertyAssociationInstance
		if (PropertyUtils.getDeclarativeModalPropertyValue(modpropinst) === null) {
			if (!PropertyUtils.isInheritedProperty(propinst)) {
				modalPropertyCreatedDIM(modpropinst)
				LOGGER.logInfo("DIM: Modal property instance for property "+propinst.property.name+" (for "+(propinst.eContainer as InstanceObject).name+") de-instantiated")	
			}
		} else {
			LOGGER.logInfo("DIM: Declarative of modal property value in property instance "+propinst.property.name+" (for "+(propinst.eContainer as InstanceObject).name+") already exists!")
		}
	].action(CRUDActivationStateEnum.UPDATED) [
		val propinst = modpropinst.eContainer as PropertyAssociationInstance
		// ModalPropertyValue inModes
		//TODO: add more conditions to avoid extraneous update when there has been no change in the modalpropertyvalue EObject properties
		if (!(modpropinst.inModes.isEmpty())) {
			val ModalPropertyValue modprop = PropertyUtils.getDeclarativeModalPropertyValue(modpropinst)
			modprop.inModes.clear
			for (Mode modpropmode : modpropinst.inModes) {
				modprop.add(modalElement_InMode,ModeUtils.findObjectModeInstInSOM(modpropinst.eContainer.eContainer as InstanceObject, modpropmode as SystemOperationMode).mode)
			}
			LOGGER.logInfo("DIM: Modes in modal property instance for property "+propinst.property.name+" (for "+(propinst.eContainer as InstanceObject).name+") refreshed")	
		}
	].action(CRUDActivationStateEnum.DELETED) [
		modpropinst.eContainer.remove(propertyAssociation_OwnedValue, PropertyUtils.getDeclarativeModalPropertyValue(modpropinst))
	].addLifeCycle(Lifecycles.getDefault(true, true)).build
	
	////////////////////////////////////////////
	// PROPERTY VALUE INSTANCE TRANSFORMATION //
	////////////////////////////////////////////
	val propertyval2declarative = createRule(FindPropertyValue.Matcher.querySpecification)
	.action(CRUDActivationStateEnum.CREATED)[
		val modpropinst = PropertyUtils.getContainingModalPropertyValue(propvalinst)
		if (modpropinst !== null && modpropinst.eContainer instanceof PropertyAssociationInstance) {
			val propinst = modpropinst.eContainer as PropertyAssociationInstance
			if (!PropertyUtils.isInheritedProperty(propinst)) {
				if (PropertyUtils.getDeclarativePropertyValue(propvalinst) === null) {
					propertyValueCreatedDIM(propinst, modpropinst, propvalinst)
					LOGGER.logInfo("DIM: Value of property "+propinst.property.name+" (for "+(propinst.eContainer as InstanceObject).name+") de-instantiated")	
				} else {
					LOGGER.logInfo("DIM: Value of property "+propinst.property.name+" (for "+(propinst.eContainer as InstanceObject).name+") already exists!")	
				}
			}	
		}
	].action(CRUDActivationStateEnum.UPDATED) [
		val modpropinst = PropertyUtils.getContainingModalPropertyValue(propvalinst)
		if (modpropinst !== null && modpropinst.eContainer instanceof PropertyAssociationInstance) {
			val modprop = PropertyUtils.getDeclarativeModalPropertyValue(modpropinst)
			if (modprop.ownedValue === null || !PropertyUtils.checkAaxl2AadlPropertyValueEquality(modpropinst.ownedValue, modprop.ownedValue)) {
				val propinst = modpropinst.eContainer as PropertyAssociationInstance
				if (PropertyUtils.isReceivedProperty(propinst)) {
					if (PropertyUtils.isInheritedProperty(propinst)) {
						if (modprop.ownedValue === null || !PropertyUtils.checkAaxl2AadlPropertyValueEquality(modpropinst.ownedValue, modprop.ownedValue)) {
							propinst.propertyAssociation.eContainer.remove(namedElement_OwnedPropertyAssociation,propinst.propertyAssociation)
							propinst.set(propertyAssociationInstance_PropertyAssociation, PropertyUtils.getParentPropertyAssociationInstance(propinst))
							LOGGER.logInfo("DIM: Property instance "+propinst.property.name+" value updated is inherited, hence corresponding Property Association deleted.")
						}
					} else {
						if (modprop.ownedValue !== null && PropertyUtils.checkAaxl2AadlPropertyValueEquality(PropertyUtils.getParentModalPropertyValue(modpropinst).ownedValue, modprop.ownedValue)) {
							propertyInstanceCreatedDIM(propinst, aadlPublicPackage)	
							LOGGER.logInfo("DIM: Inherited property instance "+propinst.property.name+" value updated")
						} else {
							propertyValueUpdatedDIM(propvalinst, aadlPublicPackage)
							LOGGER.logInfo("DIM: Value of property "+PropertyUtils.getContainingPropertyAssociationInstance(propvalinst).property.name+" updated")	
						}
					}	
				} else {
					propertyValueUpdatedDIM(propvalinst, aadlPublicPackage)
					LOGGER.logInfo("DIM: Value of property "+PropertyUtils.getContainingPropertyAssociationInstance(propvalinst).property.name+" updated")
				}
			}
		}
   	].addLifeCycle(Lifecycles.getDefault(true, true)).build
}