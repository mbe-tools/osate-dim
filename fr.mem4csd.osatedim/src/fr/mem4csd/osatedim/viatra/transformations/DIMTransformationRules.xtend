package fr.mem4csd.osatedim.viatra.transformations

import fr.mem4csd.osatedim.preference.DIMPreferences
import fr.mem4csd.osatedim.utils.ConnectionUtils
import fr.mem4csd.osatedim.utils.InstanceHierarchyUtils
import fr.mem4csd.osatedim.utils.LibraryUtils
import fr.mem4csd.osatedim.utils.ModeUtils
import fr.mem4csd.osatedim.utils.PropertyTypeUtils
import fr.mem4csd.osatedim.utils.PropertyUtils
import fr.mem4csd.osatedim.utils.TraceUtils
import fr.mem4csd.osatedim.viatra.queries.DIMQueriesInplace
import org.eclipse.emf.common.util.EList
import org.eclipse.emf.ecore.EReference
import org.eclipse.emf.ecore.util.EcoreUtil
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine
import org.eclipse.viatra.transformation.runtime.emf.modelmanipulation.IModelManipulations
import org.eclipse.viatra.transformation.runtime.emf.rules.eventdriven.EventDrivenTransformationRuleFactory
import org.osate.aadl2.Aadl2Package
import org.osate.aadl2.Access
import org.osate.aadl2.AccessCategory
import org.osate.aadl2.AccessConnection
import org.osate.aadl2.AccessType
import org.osate.aadl2.BasicPropertyAssociation
import org.osate.aadl2.BooleanLiteral
import org.osate.aadl2.ClassifierValue
import org.osate.aadl2.ComponentCategory
import org.osate.aadl2.ComponentClassifier
import org.osate.aadl2.ComponentImplementation
import org.osate.aadl2.ComponentType
import org.osate.aadl2.ComputedValue
import org.osate.aadl2.ConnectedElement
import org.osate.aadl2.Connection
import org.osate.aadl2.ContainedNamedElement
import org.osate.aadl2.DirectedFeature
import org.osate.aadl2.DirectionType
import org.osate.aadl2.Element
import org.osate.aadl2.IntegerLiteral
import org.osate.aadl2.ListValue
import org.osate.aadl2.ModalPropertyValue
import org.osate.aadl2.Mode
import org.osate.aadl2.ModelUnit
import org.osate.aadl2.NamedValue
import org.osate.aadl2.PropertyAssociation
import org.osate.aadl2.PropertySet
import org.osate.aadl2.PropertyType
import org.osate.aadl2.PropertyValue
import org.osate.aadl2.PublicPackageSection
import org.osate.aadl2.RangeValue
import org.osate.aadl2.RealLiteral
import org.osate.aadl2.StringLiteral
import org.osate.aadl2.Subcomponent
import org.osate.aadl2.instance.ComponentInstance
import org.osate.aadl2.instance.ConnectionInstance
import org.osate.aadl2.instance.ConnectionKind
import org.osate.aadl2.instance.ConnectionReference
import org.osate.aadl2.instance.FeatureCategory
import org.osate.aadl2.instance.FeatureInstance
import org.osate.aadl2.instance.InstanceObject
import org.osate.aadl2.instance.InstancePackage
import org.osate.aadl2.instance.InstanceReferenceValue
import org.osate.aadl2.instance.ModeInstance
import org.osate.aadl2.instance.ModeTransitionInstance
import org.osate.aadl2.instance.PropertyAssociationInstance
import org.osate.aadl2.instance.SystemInstance
import org.osate.aadl2.instance.SystemOperationMode
import org.osate.aadl2.modelsupport.util.AadlUtil
import fr.mem4csd.osatedim.utils.DIMLogger
import org.osate.aadl2.DataAccess

class DIMTransformationRules {
	/* VIATRA Query Pattern group */
	protected val extension DIMQueriesInplace DIMQueries = DIMQueriesInplace.instance
	
	protected DIMLogger LOGGER = null;
	
	/* Transformation-related extensions */
	protected extension IModelManipulations manipulation
	protected extension EventDrivenTransformationRuleFactory = new EventDrivenTransformationRuleFactory
	
	/* EMF metamodels */
	protected extension InstancePackage instPackage = InstancePackage::eINSTANCE
	protected extension Aadl2Package aadl2Package = Aadl2Package::eINSTANCE
	
	protected SystemInstance topSystemInst 
	protected ViatraQueryEngine engine
	protected DIMPreferences preferences
	protected PublicPackageSection aadlPublicPackage
	protected PropertySet dimPropertySet
	
	def SystemInstance getTopSystemInst() {
		return topSystemInst;
	}
	
	def PublicPackageSection getAadlPublickPackage() {
		return aadlPublicPackage;
	}
	
	/////////////////////////////////////
	// COMPONENT DEINSTANTIATION RULES //
	/////////////////////////////////////
	
	def protected componentInstanceCreatedDIM(ComponentInstance subcompinst) {
    	val compinst = subcompinst.eContainer as ComponentInstance
    	val classifier = compinst.classifier 
		// Parent Component Implementation Creation
		if (classifier instanceof ComponentType || LibraryUtils.isAffectingClassifier(classifier, aadlPublicPackage, engine, preferences.modifyReused)) {
			var compimp = createComponentImplementation(compinst.category)
			classifierCreationDIMPropertyAddition(compimp)
			compimp.set(namedElement_Name, compinst.name+".impl")
			if (classifier instanceof ComponentType) {
				compimp.set(componentImplementation_Type, classifier)		
			} else {
				compimp.set(componentImplementation_Extended, classifier)
			}
			if (LibraryUtils.isLibraryClassifier(classifier, aadlPublicPackage)) {
				aadlPublicPackage.add(packageSection_ImportedUnit, classifier.eContainer.eContainer as ModelUnit)		
			}
			compinst.set(componentInstance_Classifier, compimp)
		}
		// Parent Component Instance Subcomponent Definition
		if (compinst !== topSystemInst) {
			if (LibraryUtils.isAffectingInstanceObject(compinst,compinst.eContainer as ComponentInstance,aadlPublicPackage,engine,preferences.modifyReused, false)) {
				denyImplementnUpdatePropagn(compinst, compinst.eContainer as ComponentInstance, false)
			} else {
				setSubcomponentType(compinst, compinst.classifier)	
			}
		}
		// Subcomponent Creation
		var subcomp = createSubcomponentInsideComponentImplementation(compinst.classifier as ComponentImplementation, subcompinst)
		subcompinst.set(componentInstance_Subcomponent, subcomp)
		subcomp.set(subcomponent_Refined, null)
		//Type Creation/Selection
   		var subcomptype = {
   			if (subcompinst.classifier === null) {
   				createComponentType(subcompinst.category)
			} else {
				subcompinst.classifier
			}
		}
		if (subcompinst.name !== null) {
			subcomp.set(namedElement_Name,subcompinst.name)
			subcomptype.set(namedElement_Name,subcompinst.name)
		} else {
			subcomptype.set(namedElement_Name,"null_ext")
		}
		if (subcompinst.classifier === null) {
			classifierCreationDIMPropertyAddition(subcomptype as ComponentClassifier)
		}
		subcompinst.set(componentInstance_Classifier, subcomptype)
		setSubcomponentType(subcompinst, subcomptype as ComponentClassifier)
    }
    
    // Method to ensure that the modifications made to a component's classifier, do not unknowingly propagate to classifiers of its parent components
	def protected void denyImplementnUpdatePropagn(InstanceObject childinst, ComponentInstance parentcompinst, boolean refinement) {
    	var EList<? extends ComponentClassifier> extensionList = {
    		if (childinst instanceof ComponentInstance) {
    			LibraryUtils.getAllInheritingParentClassifiers(childinst.subcomponent, parentcompinst, refinement)	
    		} else {
    			LibraryUtils.getAllInheritingParentClassifiers(childinst as ConnectionReference, parentcompinst, refinement)
    		}
    	}
    	if (parentcompinst != topSystemInst && LibraryUtils.isAffectingInstanceObject(parentcompinst,parentcompinst.eContainer as ComponentInstance, aadlPublicPackage, engine, preferences.modifyReused, false)) {
    		var ComponentImplementation previousImp
    		for (ComponentClassifier currentcompimp : extensionList.reverse) {
    			previousImp = copyClassifier(currentcompimp, previousImp, extensionList, childinst) as ComponentImplementation 
    			if (extensionList.indexOf(currentcompimp) === extensionList.size - 1) {
    				parentcompinst.set(componentInstance_Classifier, previousImp)
    				denyImplementnUpdatePropagn(parentcompinst, parentcompinst.eContainer as ComponentInstance, false)
    			}
    		}
    	} else {
    		var extensionIndex = LibraryUtils.computeExtensionIndex(extensionList, aadlPublicPackage, engine, preferences.modifyReused)
    		var ComponentImplementation previousImp
    		for (ComponentClassifier currentcompimp : extensionList.reverse) {
    			if (extensionList.indexOf(currentcompimp) < extensionList.size - extensionIndex) {
    				previousImp = copyClassifier(currentcompimp, previousImp, extensionList, childinst) as ComponentImplementation
    				if (extensionList.indexOf(currentcompimp) == extensionList.size - 1) {
    					parentcompinst.set(componentInstance_Classifier, previousImp)
    				}
    			} else if (extensionList.indexOf(currentcompimp) == extensionList.size - extensionIndex) {
    				currentcompimp.set(componentImplementation_Extended, previousImp)
    			}
    		}	
    	}
    	if (childinst instanceof ComponentInstance) {
    		setSubcomponentType(childinst, childinst.classifier)	
    	} 
    }
    
    // Method to parallelly copy the classifier and also create extension relations
	def protected ComponentClassifier copyClassifier(ComponentClassifier currentCompClass, ComponentClassifier previousCompClass, EList<? extends ComponentClassifier> extensionList, InstanceObject childInst) {
		var compClassCopy = EcoreUtil.copy(currentCompClass) as ComponentClassifier
    	classifierCreationDIMPropertyAddition(compClassCopy)
    	compClassCopy.moveTo(aadlPublicPackage, packageSection_OwnedClassifier)
    	compClassCopy.set(namedElement_Name,currentCompClass.name+"_copy")
    	if(extensionList.indexOf(currentCompClass) == 0 && childInst !== null) {
    		if (childInst instanceof ComponentInstance) {
    			var newSubcomp = LibraryUtils.getCopySubcomponent(childInst.subcomponent, compClassCopy as ComponentImplementation)
    			childInst.set(componentInstance_Subcomponent, newSubcomp)	
    		} else if (childInst instanceof FeatureInstance) {
    			var newFeat = LibraryUtils.getCopyFeature(childInst.feature, compClassCopy as ComponentType)
    			childInst.set(featureInstance_Feature, newFeat)
    		} else if (childInst instanceof ConnectionReference) {
    			var newConn = LibraryUtils.getCopyConnection(childInst.connection, compClassCopy as ComponentImplementation)
    			childInst.set(connectionReference_Connection, newConn)
    		}
    	} else {
    		if (currentCompClass instanceof ComponentType) {
    			compClassCopy.set(componentType_Extended, previousCompClass)
    		} else {
    			compClassCopy.set(componentImplementation_Extended, previousCompClass)
    		}
    	}
    	return compClassCopy
	}
    
    def protected ComponentImplementation createComponentImplementation(ComponentCategory category) {
		return {
			switch (category) {
				case ComponentCategory.ABSTRACT : aadlPublicPackage.createChild(packageSection_OwnedClassifier,abstractImplementation)
    			case ComponentCategory.BUS : aadlPublicPackage.createChild(packageSection_OwnedClassifier,busImplementation)
   				case ComponentCategory.DATA : aadlPublicPackage.createChild(packageSection_OwnedClassifier,dataImplementation)
   				case ComponentCategory.DEVICE : aadlPublicPackage.createChild(packageSection_OwnedClassifier,deviceImplementation)
   				case ComponentCategory.MEMORY : aadlPublicPackage.createChild(packageSection_OwnedClassifier,memoryImplementation)
   				case ComponentCategory.PROCESS : aadlPublicPackage.createChild(packageSection_OwnedClassifier,processImplementation)
   				case ComponentCategory.PROCESSOR : aadlPublicPackage.createChild(packageSection_OwnedClassifier,processorImplementation)
   				case ComponentCategory.SUBPROGRAM : aadlPublicPackage.createChild(packageSection_OwnedClassifier,subprogramImplementation)
   				case ComponentCategory.SUBPROGRAM_GROUP : aadlPublicPackage.createChild(packageSection_OwnedClassifier,subprogramGroupImplementation)
   				case ComponentCategory.SYSTEM : aadlPublicPackage.createChild(packageSection_OwnedClassifier,systemImplementation)
   				case ComponentCategory.THREAD : aadlPublicPackage.createChild(packageSection_OwnedClassifier,threadImplementation)
   				case ComponentCategory.THREAD_GROUP : aadlPublicPackage.createChild(packageSection_OwnedClassifier,threadGroupImplementation)
   				case ComponentCategory.VIRTUAL_BUS : aadlPublicPackage.createChild(packageSection_OwnedClassifier,virtualBusImplementation)
   				case ComponentCategory.VIRTUAL_PROCESSOR : aadlPublicPackage.createChild(packageSection_OwnedClassifier,virtualProcessorImplementation)
				default : aadlPublicPackage.createChild(packageSection_OwnedClassifier,abstractImplementation)
			}
		} as ComponentImplementation
	}
	
	def protected ComponentType createComponentType(ComponentCategory category) {
		return {
			switch (category) {
    			case ComponentCategory.ABSTRACT : aadlPublicPackage.createChild(packageSection_OwnedClassifier,abstractType) 
    			case ComponentCategory.BUS : aadlPublicPackage.createChild(packageSection_OwnedClassifier,busType)
   				case ComponentCategory.DATA : aadlPublicPackage.createChild(packageSection_OwnedClassifier,dataType)
   				case ComponentCategory.DEVICE : aadlPublicPackage.createChild(packageSection_OwnedClassifier,deviceType)
   				case ComponentCategory.MEMORY : aadlPublicPackage.createChild(packageSection_OwnedClassifier,memoryType)
   				case ComponentCategory.PROCESS : aadlPublicPackage.createChild(packageSection_OwnedClassifier,processType)
   				case ComponentCategory.PROCESSOR : aadlPublicPackage.createChild(packageSection_OwnedClassifier,processorType)
   				case ComponentCategory.SUBPROGRAM : aadlPublicPackage.createChild(packageSection_OwnedClassifier,subprogramType)
   				case ComponentCategory.SUBPROGRAM_GROUP : aadlPublicPackage.createChild(packageSection_OwnedClassifier,subprogramGroupType)
   				case ComponentCategory.SYSTEM : aadlPublicPackage.createChild(packageSection_OwnedClassifier,systemType)
   				case ComponentCategory.THREAD : aadlPublicPackage.createChild(packageSection_OwnedClassifier,threadType)
   				case ComponentCategory.THREAD_GROUP : aadlPublicPackage.createChild(packageSection_OwnedClassifier,threadGroupType)
   				case ComponentCategory.VIRTUAL_BUS : aadlPublicPackage.createChild(packageSection_OwnedClassifier,virtualBusType)
   				case ComponentCategory.VIRTUAL_PROCESSOR : aadlPublicPackage.createChild(packageSection_OwnedClassifier,virtualProcessorType)
				default : aadlPublicPackage.createChild(packageSection_OwnedClassifier,abstractType)
			}
		} as ComponentType
	}
    
    def protected Subcomponent createSubcomponentInsideComponentImplementation(ComponentImplementation compimp, ComponentInstance subcompinst) {
    	return {	
			switch (compimp.category) {
				case ComponentCategory.ABSTRACT : {
					switch (subcompinst.category) {
    					case ComponentCategory.ABSTRACT : compimp.createChild(componentImplementation_OwnedAbstractSubcomponent, abstractSubcomponent)
    					case ComponentCategory.BUS : compimp.createChild(abstractImplementation_OwnedBusSubcomponent, busSubcomponent)
   						case ComponentCategory.DATA : compimp.createChild(abstractImplementation_OwnedDataSubcomponent, dataSubcomponent)
   						case ComponentCategory.DEVICE : compimp.createChild(abstractImplementation_OwnedDeviceSubcomponent, deviceSubcomponent)
   						case ComponentCategory.MEMORY : compimp.createChild(abstractImplementation_OwnedMemorySubcomponent, memorySubcomponent)
   						case ComponentCategory.PROCESS : compimp.createChild(abstractImplementation_OwnedProcessSubcomponent, processSubcomponent) 
						case ComponentCategory.PROCESSOR : compimp.createChild(abstractImplementation_OwnedProcessorSubcomponent, processorSubcomponent)
   						case ComponentCategory.SUBPROGRAM : compimp.createChild(abstractImplementation_OwnedSubprogramSubcomponent, subprogramSubcomponent)
   						case ComponentCategory.SUBPROGRAM_GROUP : compimp.createChild(abstractImplementation_OwnedSubprogramGroupSubcomponent, subprogramGroupSubcomponent)
   						case ComponentCategory.THREAD : compimp.createChild(abstractImplementation_OwnedThreadSubcomponent, threadSubcomponent)
   						case ComponentCategory.THREAD_GROUP : compimp.createChild(abstractImplementation_OwnedThreadGroupSubcomponent, threadGroupSubcomponent)
   						case ComponentCategory.SYSTEM : compimp.createChild(abstractImplementation_OwnedSystemSubcomponent, systemSubcomponent)
   						case ComponentCategory.VIRTUAL_BUS : compimp.createChild(abstractImplementation_OwnedVirtualBusSubcomponent, virtualBusSubcomponent)
   						case ComponentCategory.VIRTUAL_PROCESSOR : compimp.createChild(abstractImplementation_OwnedVirtualProcessorSubcomponent, virtualProcessorSubcomponent)
						default : compimp.createChild(componentImplementation_OwnedAbstractSubcomponent, abstractSubcomponent)
					}
				} case ComponentCategory.BUS : {
					switch (subcompinst.category) {
    					case ComponentCategory.ABSTRACT : compimp.createChild(componentImplementation_OwnedAbstractSubcomponent, abstractSubcomponent)
   						case ComponentCategory.VIRTUAL_BUS : compimp.createChild(busImplementation_OwnedVirtualBusSubcomponent, virtualBusSubcomponent)
						default : compimp.createChild(componentImplementation_OwnedAbstractSubcomponent, abstractSubcomponent)
					}	
				} case ComponentCategory.DATA : {
					switch (subcompinst.category) {
    					case ComponentCategory.ABSTRACT : compimp.createChild(componentImplementation_OwnedAbstractSubcomponent, abstractSubcomponent)
   						case ComponentCategory.DATA : compimp.createChild(dataImplementation_OwnedDataSubcomponent, dataSubcomponent)
   						case ComponentCategory.SUBPROGRAM : compimp.createChild(dataImplementation_OwnedSubprogramSubcomponent, subprogramSubcomponent)
						default : compimp.createChild(componentImplementation_OwnedAbstractSubcomponent, abstractSubcomponent)
					}
				} case ComponentCategory.DEVICE : {
					switch (subcompinst.category) {
    					case ComponentCategory.ABSTRACT : compimp.createChild(componentImplementation_OwnedAbstractSubcomponent, abstractSubcomponent)
    					case ComponentCategory.BUS : compimp.createChild(deviceImplementation_OwnedBusSubcomponent, busSubcomponent)
   						case ComponentCategory.DATA : compimp.createChild(deviceImplementation_OwnedDataSubcomponent, dataSubcomponent)
   						case ComponentCategory.VIRTUAL_BUS : compimp.createChild(deviceImplementation_OwnedVirtualBusSubcomponent, virtualBusSubcomponent)
						default : compimp.createChild(componentImplementation_OwnedAbstractSubcomponent, abstractSubcomponent)
					}
				} case ComponentCategory.MEMORY : {
					switch (subcompinst.category) {
    					case ComponentCategory.ABSTRACT : compimp.createChild(componentImplementation_OwnedAbstractSubcomponent, abstractSubcomponent)
    					case ComponentCategory.BUS : compimp.createChild(memoryImplementation_OwnedBusSubcomponent, busSubcomponent)
   						case ComponentCategory.MEMORY : compimp.createChild(memoryImplementation_OwnedMemorySubcomponent, memorySubcomponent)
						default : compimp.createChild(componentImplementation_OwnedAbstractSubcomponent, abstractSubcomponent)
					}
				} case ComponentCategory.PROCESS : {
					switch (subcompinst.category) {
    					case ComponentCategory.ABSTRACT : compimp.createChild(componentImplementation_OwnedAbstractSubcomponent, abstractSubcomponent)
   						case ComponentCategory.DATA : compimp.createChild(processImplementation_OwnedDataSubcomponent, dataSubcomponent)
   						case ComponentCategory.SUBPROGRAM : compimp.createChild(processImplementation_OwnedSubprogramSubcomponent, subprogramSubcomponent)
   						case ComponentCategory.SUBPROGRAM_GROUP : compimp.createChild(processImplementation_OwnedSubprogramGroupSubcomponent, subprogramGroupSubcomponent)
   						case ComponentCategory.THREAD : compimp.createChild(processImplementation_OwnedThreadSubcomponent, threadSubcomponent)
   						case ComponentCategory.THREAD_GROUP : compimp.createChild(processImplementation_OwnedThreadGroupSubcomponent, threadGroupSubcomponent)
						default : compimp.createChild(componentImplementation_OwnedAbstractSubcomponent, abstractSubcomponent)
					}						
				} case ComponentCategory.PROCESSOR : {
					switch (subcompinst.category) {
    					case ComponentCategory.ABSTRACT : compimp.createChild(componentImplementation_OwnedAbstractSubcomponent, abstractSubcomponent)
    					case ComponentCategory.BUS : compimp.createChild(processorImplementation_OwnedBusSubcomponent, busSubcomponent)
   						case ComponentCategory.MEMORY : compimp.createChild(processorImplementation_OwnedMemorySubcomponent, memorySubcomponent)
   						case ComponentCategory.VIRTUAL_BUS : compimp.createChild(processorImplementation_OwnedVirtualBusSubcomponent, virtualBusSubcomponent)
   						case ComponentCategory.VIRTUAL_PROCESSOR : compimp.createChild(processorImplementation_OwnedVirtualProcessorSubcomponent, virtualProcessorSubcomponent)
						default : compimp.createChild(componentImplementation_OwnedAbstractSubcomponent, abstractSubcomponent)
					}
				} case ComponentCategory.SUBPROGRAM : {
					switch (subcompinst.category) {
    					case ComponentCategory.ABSTRACT : compimp.createChild(componentImplementation_OwnedAbstractSubcomponent, abstractSubcomponent)
   						case ComponentCategory.DATA : compimp.createChild(subprogramImplementation_OwnedDataSubcomponent, dataSubcomponent)
   						case ComponentCategory.SUBPROGRAM : compimp.createChild(subprogramImplementation_OwnedSubprogramSubcomponent, subprogramSubcomponent)
						default : compimp.createChild(componentImplementation_OwnedAbstractSubcomponent, abstractSubcomponent)
					}
				} case ComponentCategory.SUBPROGRAM_GROUP : {
					switch (subcompinst.category) {
    					case ComponentCategory.ABSTRACT : compimp.createChild(componentImplementation_OwnedAbstractSubcomponent, abstractSubcomponent)
   						case ComponentCategory.DATA : compimp.createChild(subprogramGroupImplementation_OwnedDataSubcomponent, dataSubcomponent)
   						case ComponentCategory.SUBPROGRAM : compimp.createChild(subprogramGroupImplementation_OwnedSubprogramSubcomponent, subprogramSubcomponent)
   						case ComponentCategory.SUBPROGRAM_GROUP : compimp.createChild(subprogramGroupImplementation_OwnedSubprogramGroupSubcomponent, subprogramGroupSubcomponent)
						default : compimp.createChild(componentImplementation_OwnedAbstractSubcomponent, abstractSubcomponent)
					}
				} case ComponentCategory.SYSTEM : {
					switch (subcompinst.category) {
    					case ComponentCategory.ABSTRACT : compimp.createChild(componentImplementation_OwnedAbstractSubcomponent, abstractSubcomponent)
    					case ComponentCategory.BUS : compimp.createChild(systemImplementation_OwnedBusSubcomponent, busSubcomponent)
   						case ComponentCategory.DATA : compimp.createChild(systemImplementation_OwnedDataSubcomponent, dataSubcomponent)
   						case ComponentCategory.DEVICE : compimp.createChild(systemImplementation_OwnedDeviceSubcomponent, deviceSubcomponent)
   						case ComponentCategory.MEMORY : compimp.createChild(systemImplementation_OwnedMemorySubcomponent, memorySubcomponent)
   						case ComponentCategory.PROCESS : compimp.createChild(systemImplementation_OwnedProcessSubcomponent, processSubcomponent) 
						case ComponentCategory.PROCESSOR : compimp.createChild(systemImplementation_OwnedProcessorSubcomponent, processorSubcomponent)
   						case ComponentCategory.SUBPROGRAM : compimp.createChild(systemImplementation_OwnedSubprogramSubcomponent, subprogramSubcomponent)
   						case ComponentCategory.SUBPROGRAM_GROUP : compimp.createChild(systemImplementation_OwnedSubprogramGroupSubcomponent, subprogramGroupSubcomponent)
   						case ComponentCategory.SYSTEM : compimp.createChild(systemImplementation_OwnedSystemSubcomponent, systemSubcomponent)
   						case ComponentCategory.VIRTUAL_BUS : compimp.createChild(systemImplementation_OwnedVirtualBusSubcomponent, virtualBusSubcomponent)
   						case ComponentCategory.VIRTUAL_PROCESSOR : compimp.createChild(systemImplementation_OwnedVirtualProcessorSubcomponent, virtualProcessorSubcomponent)
						default : compimp.createChild(componentImplementation_OwnedAbstractSubcomponent, abstractSubcomponent)
					}
				} case ComponentCategory.THREAD : {
					switch (subcompinst.category) {
    					case ComponentCategory.ABSTRACT : compimp.createChild(componentImplementation_OwnedAbstractSubcomponent, abstractSubcomponent)
   						case ComponentCategory.DATA : compimp.createChild(threadImplementation_OwnedDataSubcomponent, dataSubcomponent)
   						case ComponentCategory.SUBPROGRAM : compimp.createChild(threadImplementation_OwnedSubprogramSubcomponent, subprogramSubcomponent)
   						case ComponentCategory.SUBPROGRAM_GROUP : compimp.createChild(threadImplementation_OwnedSubprogramGroupSubcomponent, subprogramGroupSubcomponent)
						default : compimp.createChild(componentImplementation_OwnedAbstractSubcomponent, abstractSubcomponent)
					}
				} case ComponentCategory.THREAD_GROUP : {
					switch (subcompinst.category) {
    					case ComponentCategory.ABSTRACT : compimp.createChild(componentImplementation_OwnedAbstractSubcomponent, abstractSubcomponent)
   						case ComponentCategory.DATA : compimp.createChild(threadGroupImplementation_OwnedDataSubcomponent, dataSubcomponent)
   						case ComponentCategory.SUBPROGRAM : compimp.createChild(threadGroupImplementation_OwnedSubprogramSubcomponent, subprogramSubcomponent)
   						case ComponentCategory.SUBPROGRAM_GROUP : compimp.createChild(threadGroupImplementation_OwnedSubprogramGroupSubcomponent, subprogramGroupSubcomponent)
   						case ComponentCategory.THREAD : compimp.createChild(threadGroupImplementation_OwnedThreadSubcomponent, threadSubcomponent)
   						case ComponentCategory.THREAD_GROUP : compimp.createChild(threadGroupImplementation_OwnedThreadGroupSubcomponent, threadGroupSubcomponent)
						default : compimp.createChild(componentImplementation_OwnedAbstractSubcomponent, abstractSubcomponent)
					}
				} case ComponentCategory.VIRTUAL_BUS : {
					switch (subcompinst.category) {
    					case ComponentCategory.ABSTRACT : compimp.createChild(componentImplementation_OwnedAbstractSubcomponent, abstractSubcomponent)
   						case ComponentCategory.VIRTUAL_BUS : compimp.createChild(virtualBusImplementation_OwnedVirtualBusSubcomponent, virtualBusSubcomponent)
						default : compimp.createChild(componentImplementation_OwnedAbstractSubcomponent, abstractSubcomponent)
					}
				} case ComponentCategory.VIRTUAL_PROCESSOR : {
					switch (subcompinst.category) {
    					case ComponentCategory.ABSTRACT : compimp.createChild(componentImplementation_OwnedAbstractSubcomponent, abstractSubcomponent)
   						case ComponentCategory.VIRTUAL_BUS : compimp.createChild(virtualProcessorImplementation_OwnedVirtualBusSubcomponent, virtualBusSubcomponent)
   						case ComponentCategory.VIRTUAL_PROCESSOR : compimp.createChild(virtualProcessorImplementation_OwnedVirtualProcessorSubcomponent, virtualProcessorSubcomponent)
						default : compimp.createChild(componentImplementation_OwnedAbstractSubcomponent, abstractSubcomponent)
					}
				}				
			}
		} as Subcomponent
    }
    
    // Method to set the Classifier of a ComponentInstance's subcomponent to CompClass
    def protected setSubcomponentType(ComponentInstance compinst, ComponentClassifier compclass) {
    	val subcomponent = compinst.subcomponent
    	switch (compclass.category) {
			case ComponentCategory.ABSTRACT : subcomponent.set(abstractSubcomponent_AbstractSubcomponentType, compclass)
    		case ComponentCategory.BUS : subcomponent.set(busSubcomponent_BusSubcomponentType, compclass)
   			case ComponentCategory.DATA : subcomponent.set(dataSubcomponent_DataSubcomponentType, compclass)
   			case ComponentCategory.DEVICE : subcomponent.set(deviceSubcomponent_DeviceSubcomponentType, compclass)
   			case ComponentCategory.MEMORY : subcomponent.set(memorySubcomponent_MemorySubcomponentType, compclass)
   			case ComponentCategory.PROCESS : subcomponent.set(processSubcomponent_ProcessSubcomponentType, compclass)
   			case ComponentCategory.PROCESSOR : subcomponent.set(processorSubcomponent_ProcessorSubcomponentType, compclass)
   			case ComponentCategory.SUBPROGRAM : subcomponent.set(subprogramSubcomponent_SubprogramSubcomponentType, compclass)
   			case ComponentCategory.SUBPROGRAM_GROUP : subcomponent.set(subprogramGroupSubcomponent_SubprogramGroupSubcomponentType, compclass)
   			case ComponentCategory.SYSTEM : subcomponent.set(systemSubcomponent_SystemSubcomponentType, compclass)
   			case ComponentCategory.THREAD : subcomponent.set(threadSubcomponent_ThreadSubcomponentType, compclass)
   			case ComponentCategory.THREAD_GROUP : subcomponent.set(threadGroupSubcomponent_ThreadGroupSubcomponentType, compclass)
   			case ComponentCategory.VIRTUAL_BUS : subcomponent.set(virtualBusSubcomponent_VirtualBusSubcomponentType, compclass)
   			case ComponentCategory.VIRTUAL_PROCESSOR : subcomponent.set(virtualProcessorSubcomponent_VirtualProcessorSubcomponentType, compclass)
		}
    }
    
    ///////////////////
    // UTILITY RULES //
    ///////////////////
    
    def Integer max(Integer int1, Integer int2) {
		if (int1 > int2) {
			return int1
		} else {
			return int2
		}
	}
    
    // User-oriented utility for AUTO-ADDITION of Inherited Properties in newly created InstanceObjects
    // SET THE RELEVANT PREFERENCE TO TRUE TO USE THIS FUNCTIONALITY
    def protected elementCreationPropertyInheritance(InstanceObject instanceObj) {
		if (preferences.isInheritProperty) {
			for (PropertyAssociation propinst : (instanceObj.eContainer as InstanceObject).ownedPropertyAssociations) {
				if (propinst.property.inherit && instanceObj.acceptsProperty(propinst.property)) {
					var propadd = EcoreUtil.copy(propinst)
					instanceObj.add(namedElement_OwnedPropertyAssociation, propadd)
				}
			}	
		}
	}
	
	// User-oriented utility for AUTO-ADDITION of Inherited Modes in newly created ComponentInstances
    // SET THE RELEVANT PREFERENCE TO TRUE TO USE THIS FUNCTIONALITY
	def protected componentCreationModeInheritance(ComponentInstance subcompinst) {
		if (preferences.isInheritMode) {
			for (ModeInstance modeinst : (subcompinst.eContainer as ComponentInstance).inModes) {
				var modeadd = EcoreUtil.copy(modeinst)
				subcompinst.add(componentInstance_InMode, modeadd)
			}	
		}
	}
	
	def protected connectionCreationModeInheritance(ConnectionInstance conninst) {
		if (preferences.isInheritMode) {
			for (ModeInstance modeinst : (conninst.eContainer as ComponentInstance).inModes) {
				for (SystemOperationMode som : topSystemInst.systemOperationModes) {
					if (som.currentModes.contains(modeinst)) {
						conninst.add(connectionInstance_InSystemOperationMode, som)		
					}
				}
			}
		}
	}
	
	// Method for ADDITION of DIM_Classifier property in newly created Component Classifiers
    // SET THE RELEVANT PREFERENCE TO TRUE TO USE THIS FUNCTIONALITY
	def protected classifierCreationDIMPropertyAddition(ComponentClassifier compclass) {
		if (preferences.isAddClassifierProperty) {
			var propadd = compclass.createChild(namedElement_OwnedPropertyAssociation, propertyAssociation) 
			propadd.set(propertyAssociation_Property, dimPropertySet.findNamedElement("DIM_Classifier"))
			var modpropadd = propadd.createChild(propertyAssociation_OwnedValue, modalPropertyValue)
			var propvaladd = modpropadd.createChild(modalPropertyValue_OwnedValue, booleanLiteral)
			propvaladd.set(booleanLiteral_Value,true)
		}
	}
	
	///////////////////////////////////
	// FEATURE DEINSTANTIATION RULES //
	//////////////////////////////////
	
	def protected featureInstanceCreatedDIM(FeatureInstance featinst) {
    	var compinst = featinst.eContainer as ComponentInstance
		// Parent Component Classifier Creation
		var ComponentClassifier compClassifier = {
			if (compinst == topSystemInst) {
				(compinst as SystemInstance).componentImplementation
			} else {
				compinst.classifier
			}
		}
		var ComponentType comptype;
		if (compClassifier instanceof ComponentImplementation) {
			comptype = (compClassifier as ComponentImplementation).type
			if (LibraryUtils.isAffectingClassifier(compClassifier, aadlPublicPackage, engine, preferences.modifyReused)) {
				var compimp = createComponentImplementation(compinst.category)
				classifierCreationDIMPropertyAddition(compimp)
				compimp.set(namedElement_Name, compClassifier.name+"_ext")
				compimp.set(componentImplementation_Type, comptype)
				compimp.set(componentImplementation_Extended, compClassifier)
				if (LibraryUtils.isLibraryClassifier(compClassifier, aadlPublicPackage)) {
					aadlPublicPackage.add(packageSection_ImportedUnit, compinst.classifier.eContainer.eContainer as ModelUnit)	
				}
				compinst.set(componentInstance_Classifier, compimp)
			}
		} else {
			comptype = compinst.classifier as ComponentType
		}
		if (LibraryUtils.isAffectingClassifier(comptype,aadlPublicPackage, engine, preferences.modifyReused)) {
			var newcomptype = createComponentType(compinst.category)
			classifierCreationDIMPropertyAddition(newcomptype)
			newcomptype.set(namedElement_Name, comptype.name+"_ext")
			newcomptype.set(componentType_Extended, comptype)
			if (compClassifier instanceof ComponentImplementation) {
				compinst.classifier.set(componentImplementation_Type, newcomptype)
			} else {
				compinst.set(componentInstance_Classifier, newcomptype)
			}
			comptype = newcomptype
		}
		// Parent Component Instance Subcomponent Definition
		if (compinst !== topSystemInst) {
			if (LibraryUtils.isAffectingInstanceObject(compinst,compinst.eContainer as ComponentInstance,aadlPublicPackage,engine,preferences.modifyReused, false)) {
				denyImplementnUpdatePropagn(compinst, compinst.eContainer as ComponentInstance, false)
			} else {
				setSubcomponentType(compinst, compinst.classifier)
			}	
		}
		// Feature Creation
		var featadd = createFeature(compinst, comptype, featinst)
		if (featinst.name !== null) {
			featadd.set(namedElement_Name,featinst.name)	
		}
		// Feature Direction
		if (featadd instanceof DirectedFeature && featinst.direction !== null) {
			if (featinst.direction == DirectionType.IN) {
				featadd.set(directedFeature_In,true)
				featadd.set(directedFeature_Out,false)
			} else if (featinst.direction == DirectionType.OUT) {
				featadd.set(directedFeature_In,false)
				featadd.set(directedFeature_Out,true)
			} else {
				featadd.set(directedFeature_In,true)
				featadd.set(directedFeature_Out,true)
			} 
		}
		// Data Access Feature Type
		if (featinst.type !== null) {
			if (featadd instanceof DataAccess) {
				if (featinst.type.classifier !== null) {
					featadd.set(dataAccess_DataFeatureClassifier, featinst.type.classifier)
				} 
			}
		}
		// Trace Creation
		featinst.set(featureInstance_Feature,featadd)
    }
    
	def protected createFeature(ComponentInstance compinst, ComponentType comptype, FeatureInstance featinst) {
    	switch (compinst.category) {
			case ComponentCategory.ABSTRACT : {
				switch (featinst.category) {
					case FeatureCategory.DATA_PORT : comptype.createChild(abstractType_OwnedDataPort, dataPort)
					case FeatureCategory.EVENT_PORT : comptype.createChild(abstractType_OwnedEventPort, eventPort)
					case FeatureCategory.EVENT_DATA_PORT : comptype.createChild(abstractType_OwnedEventDataPort, eventDataPort)
					case FeatureCategory.BUS_ACCESS : comptype.createChild(abstractType_OwnedBusAccess, busAccess)
					case FeatureCategory.DATA_ACCESS : comptype.createChild(abstractType_OwnedDataAccess, dataAccess)
					case FeatureCategory.SUBPROGRAM_ACCESS : comptype.createChild(abstractType_OwnedSubprogramAccess, subprogramAccess)
					case FeatureCategory.SUBPROGRAM_GROUP_ACCESS : comptype.createChild(abstractType_OwnedSubprogramGroupAccess, subprogramGroupAccess)
					case FeatureCategory.FEATURE_GROUP : comptype.createChild(componentType_OwnedFeatureGroup, featureGroup)
					case FeatureCategory.ABSTRACT_FEATURE : comptype.createChild(componentType_OwnedAbstractFeature, abstractFeature)
					default : null
				}
			}
    		case ComponentCategory.BUS : {
				switch (featinst.category) {
					case FeatureCategory.DATA_PORT : comptype.createChild(busType_OwnedDataPort, dataPort)
					case FeatureCategory.EVENT_PORT : comptype.createChild(busType_OwnedEventPort, eventPort)
					case FeatureCategory.EVENT_DATA_PORT : comptype.createChild(busType_OwnedEventDataPort, eventDataPort)
					case FeatureCategory.BUS_ACCESS : comptype.createChild(busType_OwnedBusAccess, busAccess)
					case FeatureCategory.FEATURE_GROUP : comptype.createChild(componentType_OwnedFeatureGroup, featureGroup)
					case FeatureCategory.ABSTRACT_FEATURE : comptype.createChild(componentType_OwnedAbstractFeature, abstractFeature)
					default : null
				}
   			}
   			case ComponentCategory.DATA : {
				switch (featinst.category) {
					case FeatureCategory.DATA_ACCESS : comptype.createChild(dataType_OwnedDataAccess, dataAccess)
					case FeatureCategory.SUBPROGRAM_ACCESS : comptype.createChild(dataType_OwnedSubprogramAccess, subprogramAccess)
					case FeatureCategory.SUBPROGRAM_GROUP_ACCESS : comptype.createChild(dataType_OwnedSubprogramGroupAccess, subprogramGroupAccess)
					case FeatureCategory.FEATURE_GROUP : comptype.createChild(componentType_OwnedFeatureGroup, featureGroup)
					case FeatureCategory.ABSTRACT_FEATURE : comptype.createChild(componentType_OwnedAbstractFeature, abstractFeature)
					default : null
				}
   			}
   			case ComponentCategory.DEVICE : {
				switch (featinst.category) {
					case FeatureCategory.DATA_PORT : comptype.createChild(deviceType_OwnedDataPort, dataPort)
					case FeatureCategory.EVENT_PORT : comptype.createChild(deviceType_OwnedEventPort, eventPort)
					case FeatureCategory.EVENT_DATA_PORT : comptype.createChild(deviceType_OwnedEventDataPort, eventDataPort)
					case FeatureCategory.BUS_ACCESS : comptype.createChild(deviceType_OwnedBusAccess, busAccess)
					case FeatureCategory.SUBPROGRAM_ACCESS : comptype.createChild(deviceType_OwnedSubprogramAccess, subprogramAccess)
					case FeatureCategory.SUBPROGRAM_GROUP_ACCESS : comptype.createChild(deviceType_OwnedSubprogramGroupAccess, subprogramGroupAccess)
					case FeatureCategory.FEATURE_GROUP : comptype.createChild(componentType_OwnedFeatureGroup, featureGroup)
					case FeatureCategory.ABSTRACT_FEATURE : comptype.createChild(componentType_OwnedAbstractFeature, abstractFeature)
					default : null
				}
			}
   			case ComponentCategory.MEMORY : {
				switch (featinst.category) {
					case FeatureCategory.DATA_PORT : comptype.createChild(memoryType_OwnedDataPort, dataPort)
					case FeatureCategory.EVENT_PORT : comptype.createChild(memoryType_OwnedEventPort, eventPort)
					case FeatureCategory.EVENT_DATA_PORT : comptype.createChild(memoryType_OwnedEventDataPort, eventDataPort)
					case FeatureCategory.BUS_ACCESS : comptype.createChild(memoryType_OwnedBusAccess, busAccess)
					case FeatureCategory.FEATURE_GROUP : comptype.createChild(componentType_OwnedFeatureGroup, featureGroup)
					case FeatureCategory.ABSTRACT_FEATURE : comptype.createChild(componentType_OwnedAbstractFeature, abstractFeature)
					default : null
				}
   			}
   			case ComponentCategory.PROCESS : {
				switch (featinst.category) {
					case FeatureCategory.DATA_PORT : comptype.createChild(processType_OwnedDataPort, dataPort)
					case FeatureCategory.EVENT_PORT : comptype.createChild(processType_OwnedEventPort, eventPort)
					case FeatureCategory.EVENT_DATA_PORT : comptype.createChild(processType_OwnedEventDataPort, eventDataPort)
					case FeatureCategory.DATA_ACCESS : comptype.createChild(processType_OwnedDataAccess, dataAccess)
					case FeatureCategory.SUBPROGRAM_ACCESS : comptype.createChild(processType_OwnedSubprogramAccess, subprogramAccess)
					case FeatureCategory.SUBPROGRAM_GROUP_ACCESS : comptype.createChild(processType_OwnedSubprogramGroupAccess, subprogramGroupAccess)
					case FeatureCategory.FEATURE_GROUP : comptype.createChild(componentType_OwnedFeatureGroup, featureGroup)
					case FeatureCategory.ABSTRACT_FEATURE : comptype.createChild(componentType_OwnedAbstractFeature, abstractFeature)
					default : null
				}
   			}
   			case ComponentCategory.PROCESSOR : {
				switch (featinst.category) {
					case FeatureCategory.DATA_PORT : comptype.createChild(processorType_OwnedDataPort, dataPort)
					case FeatureCategory.EVENT_PORT : comptype.createChild(processorType_OwnedEventPort, eventPort)
					case FeatureCategory.EVENT_DATA_PORT : comptype.createChild(processorType_OwnedEventDataPort, eventDataPort)
					case FeatureCategory.BUS_ACCESS : comptype.createChild(processorType_OwnedBusAccess, busAccess)
					case FeatureCategory.SUBPROGRAM_ACCESS : comptype.createChild(processorType_OwnedSubprogramAccess, subprogramAccess)
					case FeatureCategory.SUBPROGRAM_GROUP_ACCESS : comptype.createChild(processorType_OwnedSubprogramGroupAccess, subprogramGroupAccess)
					case FeatureCategory.FEATURE_GROUP : comptype.createChild(componentType_OwnedFeatureGroup, featureGroup)
					case FeatureCategory.ABSTRACT_FEATURE : comptype.createChild(componentType_OwnedAbstractFeature, abstractFeature)
					default : null
				}
			}
   			case ComponentCategory.SUBPROGRAM : {
				switch (featinst.category) {
					case FeatureCategory.EVENT_PORT : comptype.createChild(subprogramType_OwnedEventPort, eventPort)
					case FeatureCategory.EVENT_DATA_PORT : comptype.createChild(subprogramType_OwnedEventDataPort, eventDataPort)
					case FeatureCategory.PARAMETER : comptype.createChild(subprogramType_OwnedParameter, parameter)
					case FeatureCategory.DATA_ACCESS : comptype.createChild(subprogramType_OwnedDataAccess, dataAccess)
					case FeatureCategory.SUBPROGRAM_ACCESS : comptype.createChild(subprogramType_OwnedSubprogramAccess, subprogramAccess)
					case FeatureCategory.SUBPROGRAM_GROUP_ACCESS : comptype.createChild(subprogramType_OwnedSubprogramGroupAccess, subprogramGroupAccess)
					case FeatureCategory.FEATURE_GROUP : comptype.createChild(componentType_OwnedFeatureGroup, featureGroup)
					case FeatureCategory.ABSTRACT_FEATURE : comptype.createChild(componentType_OwnedAbstractFeature, abstractFeature)
					default : null
				}
   			}
   			case ComponentCategory.SUBPROGRAM_GROUP : {
				switch (featinst.category) {
					case FeatureCategory.SUBPROGRAM_ACCESS : comptype.createChild(subprogramGroupType_OwnedSubprogramAccess, subprogramAccess)
					case FeatureCategory.SUBPROGRAM_GROUP_ACCESS : comptype.createChild(subprogramGroupType_OwnedSubprogramGroupAccess, subprogramGroupAccess)
					case FeatureCategory.FEATURE_GROUP : comptype.createChild(componentType_OwnedFeatureGroup, featureGroup)
					case FeatureCategory.ABSTRACT_FEATURE : comptype.createChild(componentType_OwnedAbstractFeature, abstractFeature)
					default : null
				}
   			}
   			case ComponentCategory.SYSTEM : {
				switch (featinst.category) {
					case FeatureCategory.DATA_PORT : comptype.createChild(systemType_OwnedDataPort, dataPort)
					case FeatureCategory.EVENT_PORT : comptype.createChild(systemType_OwnedEventPort, eventPort)
					case FeatureCategory.EVENT_DATA_PORT : comptype.createChild(systemType_OwnedEventDataPort, eventDataPort)
					case FeatureCategory.BUS_ACCESS : comptype.createChild(systemType_OwnedBusAccess, busAccess)
					case FeatureCategory.DATA_ACCESS : comptype.createChild(systemType_OwnedDataAccess, dataAccess)
					case FeatureCategory.SUBPROGRAM_ACCESS : comptype.createChild(systemType_OwnedSubprogramAccess, subprogramAccess)
					case FeatureCategory.SUBPROGRAM_GROUP_ACCESS : comptype.createChild(systemType_OwnedSubprogramGroupAccess, subprogramGroupAccess)
					case FeatureCategory.FEATURE_GROUP : comptype.createChild(componentType_OwnedFeatureGroup, featureGroup)
					case FeatureCategory.ABSTRACT_FEATURE : comptype.createChild(componentType_OwnedAbstractFeature, abstractFeature)
					default : null
				}
   			}
   			case ComponentCategory.THREAD : {
				switch (featinst.category) {
					case FeatureCategory.DATA_PORT : comptype.createChild(threadType_OwnedDataPort, dataPort)
					case FeatureCategory.EVENT_PORT : comptype.createChild(threadType_OwnedEventPort, eventPort)
					case FeatureCategory.EVENT_DATA_PORT : comptype.createChild(threadType_OwnedEventDataPort, eventDataPort)
					case FeatureCategory.DATA_ACCESS : comptype.createChild(threadType_OwnedDataAccess, dataAccess)
					case FeatureCategory.SUBPROGRAM_ACCESS : comptype.createChild(threadType_OwnedSubprogramAccess, subprogramAccess)
					case FeatureCategory.SUBPROGRAM_GROUP_ACCESS : comptype.createChild(threadType_OwnedSubprogramGroupAccess, subprogramGroupAccess)
					case FeatureCategory.FEATURE_GROUP : comptype.createChild(componentType_OwnedFeatureGroup, featureGroup)
					case FeatureCategory.ABSTRACT_FEATURE : comptype.createChild(componentType_OwnedAbstractFeature, abstractFeature)
					default : null
				}
   			}
   			case ComponentCategory.THREAD_GROUP : {
				switch (featinst.category) {
					case FeatureCategory.DATA_PORT : comptype.createChild(threadGroupType_OwnedDataPort, dataPort)
					case FeatureCategory.EVENT_PORT : comptype.createChild(threadGroupType_OwnedEventPort, eventPort)
					case FeatureCategory.EVENT_DATA_PORT : comptype.createChild(threadGroupType_OwnedEventDataPort, eventDataPort)
					case FeatureCategory.DATA_ACCESS : comptype.createChild(threadGroupType_OwnedDataAccess, dataAccess)
					case FeatureCategory.SUBPROGRAM_ACCESS : comptype.createChild(threadGroupType_OwnedSubprogramAccess, subprogramAccess)
					case FeatureCategory.SUBPROGRAM_GROUP_ACCESS : comptype.createChild(threadGroupType_OwnedSubprogramGroupAccess, subprogramGroupAccess)
					case FeatureCategory.FEATURE_GROUP : comptype.createChild(componentType_OwnedFeatureGroup, featureGroup)
					case FeatureCategory.ABSTRACT_FEATURE : comptype.createChild(componentType_OwnedAbstractFeature, abstractFeature)
					default : null
				}
   			}
   			case ComponentCategory.VIRTUAL_BUS : {
				switch (featinst.category) {
					case FeatureCategory.DATA_PORT : comptype.createChild(virtualBusType_OwnedDataPort, dataPort)
					case FeatureCategory.EVENT_PORT : comptype.createChild(virtualBusType_OwnedEventPort, eventPort)
					case FeatureCategory.EVENT_DATA_PORT : comptype.createChild(virtualBusType_OwnedEventDataPort, eventDataPort)
					case FeatureCategory.BUS_ACCESS : comptype.createChild(virtualBusType_OwnedBusAccess, busAccess)
					case FeatureCategory.FEATURE_GROUP : comptype.createChild(componentType_OwnedFeatureGroup, featureGroup)
					case FeatureCategory.ABSTRACT_FEATURE : comptype.createChild(componentType_OwnedAbstractFeature, abstractFeature)
					default : null
				}
   			}
   			case ComponentCategory.VIRTUAL_PROCESSOR : {
				switch (featinst.category) {
					case FeatureCategory.DATA_PORT : comptype.createChild(virtualProcessorType_OwnedDataPort, dataPort)
					case FeatureCategory.EVENT_PORT : comptype.createChild(virtualProcessorType_OwnedEventPort, eventPort)
					case FeatureCategory.EVENT_DATA_PORT : comptype.createChild(virtualProcessorType_OwnedEventDataPort, eventDataPort)
					case FeatureCategory.BUS_ACCESS : comptype.createChild(virtualProcessorType_OwnedBusAccess, busAccess)
					case FeatureCategory.SUBPROGRAM_ACCESS : comptype.createChild(virtualProcessorType_OwnedSubprogramAccess, subprogramAccess)
					case FeatureCategory.SUBPROGRAM_GROUP_ACCESS : comptype.createChild(virtualProcessorType_OwnedSubprogramGroupAccess, subprogramGroupAccess)
					case FeatureCategory.FEATURE_GROUP : comptype.createChild(componentType_OwnedFeatureGroup, featureGroup)
					case FeatureCategory.ABSTRACT_FEATURE : comptype.createChild(componentType_OwnedAbstractFeature, abstractFeature)
					default : null
				}
			}
		}
    }
    
    //////////////////////////////////////
    // CONNECTION DEINSTANTIATION RULES //
    //////////////////////////////////////
    
    def protected connectionInstanceCreatedDIM(ConnectionInstance conninst) {
    	var complist = ConnectionUtils.getPathBetweenConnectionInstanceEnds(conninst.source,conninst.destination)
		if (conninst.connectionReferences.isNullOrEmpty) {
			conninst.createChild(connectionInstance_ConnectionReference, connectionReference)
		}
		val connreflist = conninst.connectionReferences
		for (i : 0 ..< max(connreflist.size, complist.size)) {
			if (i >= complist.size && connreflist.get(i) !== null) {
				conninst.remove(connectionInstance_ConnectionReference, connreflist.get(i))	
			} else {
				var connref = {
					if (connreflist.get(i) !== null) {
						connreflist.get(i)
					} else {
						LOGGER.logInfo("DIM: "+ i.toString +"th connection reference for connection "+ conninst.name +" created")
						conninst.createChild(connectionInstance_ConnectionReference, connectionReference)
					}
				} as ConnectionReference
				// Context
				if (connref.context === null) {
					connref.set(connectionReference_Context, complist.get(i))
					LOGGER.logInfo("DIM: Context of the "+ i.toString +"th connection reference for connection "+ conninst.name +" added")
				} else if (connref.context !== complist.get(i)) {
					connref.set(connectionReference_Context, complist.get(i))
					LOGGER.logInfo("DIM: Context of the "+ i.toString +"th connection reference for connection "+ conninst.name +" changed")
					connref.set(connectionReference_Source, null)
					connref.set(connectionReference_Destination, null)
				}
				// Source feature
				if (connref.source === null) {
					if (i == 0) {
						connref.set(connectionReference_Source, conninst.source)
						LOGGER.logInfo("DIM: Source of the "+ i.toString +"th connection reference for connection "+ conninst.name +" added")	 
					} else if (conninst.connectionReferences.get(i - 1).destination !== null) {
						connref.set(connectionReference_Source, conninst.connectionReferences.get(i - 1).destination)
						LOGGER.logInfo("DIM: Source of the "+ i.toString +"th connection reference for connection "+ conninst.name +" added")		
					}
				} else {
					if (i == 0 && connref.source !== conninst.source) {
						connref.set(connectionReference_Source, conninst.source)
						LOGGER.logWarning("DIM: Source of the "+ i.toString +"th connection reference for connection "+ conninst.name +" changed")
 					} else if (i !== 0 && conninst.connectionReferences.get(i - 1).destination !== null && connref.source !== conninst.connectionReferences.get(i - 1).destination) {
 						connref.set(connectionReference_Source, conninst.connectionReferences.get(i - 1).destination)
 						LOGGER.logWarning("DIM: Source of the "+ i.toString +"th connection reference for connection "+ conninst.name +" changed")
 					}
				}	
 				// Destination feature
				if (connref.destination === null) {
					if (i == complist.size - 1) {
						connref.set(connectionReference_Destination, conninst.destination)
						LOGGER.logInfo("DIM: Destination of the "+ i.toString +"th connection reference for connection "+ conninst.name +" added")
					} else if (preferences.createInterFeatures) {
						var feataddcompinst = {
							if (complist.get(i+1).eContainer() == complist.get(i)) {
								complist.get(i+1)
							} else {
								complist
							}
						} as FeatureInstance
						var dstfeatinst = feataddcompinst.createChild(componentInstance_FeatureInstance,featureInstance) as FeatureInstance
						if (conninst.source instanceof FeatureInstance) {
							dstfeatinst.set(featureInstance_Category, (conninst.source as FeatureInstance).category)	
						} else {
							dstfeatinst.set(featureInstance_Category, (conninst.destination as FeatureInstance).category)	
						}
						if (conninst.bidirectional == true) {
							dstfeatinst.set(featureInstance_Direction, DirectionType.IN_OUT)	
						} else {
							if (complist.get(i+1).eContainer() == complist.get(i)) {
								dstfeatinst.set(featureInstance_Direction, DirectionType.IN)
							} else {
								dstfeatinst.set(featureInstance_Direction, DirectionType.OUT)
							}
						}
						dstfeatinst.set(namedElement_Name, conninst.name+"_feat")
						connref.set(connectionReference_Destination, dstfeatinst)
						LOGGER.logInfo("DIM: Destination of the "+ i.toString +"th connection reference for connection "+ conninst.name +" created")
					}			
				} else if (i==complist.size-1 && connref.destination !== conninst.destination) {
					connref.set(connectionReference_Destination, conninst.destination)
					LOGGER.logWarning("DIM: Destination of the connection reference changed")
 				} //TODO: the corner case where the destination may not be null, but may still be wrong is not accounted for
			}
			LOGGER.logInfo("DIM: Connection Instance "+conninst.name+" transformed to connection reference")
		}
    }
    
    //////////////////////////////////////
    // CONNECTION DEINSTANTIATION RULES //
    //////////////////////////////////////
    
    def protected createConnection(ConnectionKind connkind, ComponentImplementation compimp) {
    	return {
			switch (connkind) {
				case ConnectionKind.ACCESS_CONNECTION : compimp.createChild(componentImplementation_OwnedAccessConnection, accessConnection)
				case ConnectionKind.FEATURE_CONNECTION : compimp.createChild(componentImplementation_OwnedFeatureConnection, featureConnection)
				case ConnectionKind.FEATURE_GROUP_CONNECTION : compimp.createChild(componentImplementation_OwnedFeatureGroupConnection, featureGroupConnection)
				case ConnectionKind.PARAMETER_CONNECTION : compimp.createChild(componentImplementation_OwnedParameterConnection, parameterConnection)
				case ConnectionKind.PORT_CONNECTION : compimp.createChild(componentImplementation_OwnedPortConnection, portConnection)
				default : null
			}		
		} as Connection
    }
    
    def protected populateSourceConnectedElement(ConnectedElement connaddsrcelem, ConnectionReference connref, Connection connadd) {
    	connaddsrcelem.set(connectedElement_ConnectionEnd, TraceUtils.getDeclarativeElement(connref.source))
		if (connref.source.eContainer !== connref.context) {
			connaddsrcelem.set(connectedElement_Context, (connref.source.eContainer as ComponentInstance).subcomponent)
		} else {
			connaddsrcelem.set(connectedElement_Context, null)
		}
		if (connref.source instanceof FeatureInstance && connadd instanceof AccessConnection) {
			var srcconnelem = (connref.source as FeatureInstance).feature
			if (srcconnelem instanceof Access) {
				if (connref.source.eContainer != connref.context) {
					srcconnelem.set(access_Kind, AccessType.PROVIDES)
				} else {
					srcconnelem.set(access_Kind, AccessType.REQUIRES)
				}	
			}
		}
    }
 
    def protected populateDestinationConnectedElement(ConnectedElement connadddestelem, ConnectionReference connref, Connection connadd) {
    	connadddestelem.set(connectedElement_ConnectionEnd, TraceUtils.getDeclarativeElement(connref.destination))
		if (connref.destination.eContainer !== connref.context) {
			connadddestelem.set(connectedElement_Context, (connref.destination.eContainer as ComponentInstance).subcomponent)
		} else {
			connadddestelem.set(connectedElement_Context, null)
		}
		if (connref.destination instanceof FeatureInstance && connadd instanceof AccessConnection) {
			var destconnelem = (connref.destination as FeatureInstance).feature
			if (destconnelem instanceof Access) {
				if (connref.context == connref.destination.eContainer) {
					destconnelem.set(access_Kind, AccessType.PROVIDES)
				} else {
					destconnelem.set(access_Kind, AccessType.REQUIRES)
				}	
			}
		}
    }
    
    def protected populateNewConnection(Connection connadd, ConnectionInstance conninst, ConnectionReference connref) {
    	connadd.set(connection_Bidirectional, conninst.bidirectional)
			if (connadd instanceof AccessConnection) {
				if (conninst.source instanceof FeatureInstance) {
					switch ((conninst.source as FeatureInstance).category) {
						case  FeatureCategory.BUS_ACCESS : (connadd as AccessConnection).set(accessConnection_AccessCategory, AccessCategory.BUS)
						case  FeatureCategory.DATA_ACCESS : (connadd as AccessConnection).set(accessConnection_AccessCategory, AccessCategory.DATA)
						case  FeatureCategory.SUBPROGRAM_ACCESS : (connadd as AccessConnection).set(accessConnection_AccessCategory, AccessCategory.SUBPROGRAM)
						case  FeatureCategory.SUBPROGRAM_GROUP_ACCESS : (connadd as AccessConnection).set(accessConnection_AccessCategory, AccessCategory.SUBPROGRAM_GROUP)
						default: {}
					}
				} else if (conninst.source instanceof ComponentInstance) {
					switch ((conninst.source as ComponentInstance).category) {
						case  ComponentCategory.BUS : (connadd as AccessConnection).set(accessConnection_AccessCategory, AccessCategory.BUS)
						case  ComponentCategory.DATA : (connadd as AccessConnection).set(accessConnection_AccessCategory, AccessCategory.DATA)
						case  ComponentCategory.SUBPROGRAM : (connadd as AccessConnection).set(accessConnection_AccessCategory, AccessCategory.SUBPROGRAM)
						case  ComponentCategory.SUBPROGRAM_GROUP : (connadd as AccessConnection).set(accessConnection_AccessCategory, AccessCategory.SUBPROGRAM_GROUP)
						default: {}
					}
				}
			}
			
			var connaddsrcelem = connadd.createChild(connection_Source,connectedElement) as ConnectedElement
			populateSourceConnectedElement(connaddsrcelem, connref, connadd)
			
			var connadddestelem = connadd.createChild(connection_Destination,connectedElement) as ConnectedElement
			populateDestinationConnectedElement(connadddestelem, connref, connadd)
			
			// Trace Creation
			connref.set(connectionReference_Connection,connadd)
    }
    
    def protected connectionAdditionPreparation(ComponentInstance compinst) {
    		val classifier = {
    			if (compinst == topSystemInst) {
    				(compinst as SystemInstance).componentImplementation	
	    		} else {
	    			compinst.classifier 
				}
			}
			// Parent Component Implementation Creation
			if (classifier instanceof ComponentType || LibraryUtils.isLibraryClassifier(classifier, aadlPublicPackage) || (LibraryUtils.isReusedClassifier(classifier,engine) && !preferences.modifyReused)) {
				var compimp =  createComponentImplementation(compinst.category)
				classifierCreationDIMPropertyAddition(compimp)
				compimp.set(namedElement_Name, compinst.name+".impl")
				if (classifier instanceof ComponentType) {
					compimp.set(componentImplementation_Type, classifier)		
				} else {
					compimp.set(componentImplementation_Extended, classifier)
				}
				if (LibraryUtils.isLibraryClassifier(classifier, aadlPublicPackage)) {
					aadlPublicPackage.add(packageSection_ImportedUnit, classifier.eContainer.eContainer as ModelUnit)		
				}
				compinst.set(componentInstance_Classifier, compimp)
			}	
			// Parent Component Instance Subcomponent Definition
			if (compinst !== topSystemInst) {
				if (LibraryUtils.isAffectingInstanceObject(compinst,compinst.eContainer as ComponentInstance,aadlPublicPackage,engine,preferences.modifyReused, false)) {
					denyImplementnUpdatePropagn(compinst, compinst.eContainer as ComponentInstance, false)
				} else {
					setSubcomponentType(compinst, compinst.classifier)	
				}
			}
    }
    
    def protected connectionReferenceFoundDIM(ConnectionReference connref) {
    	val compinst = connref.context
    	connectionAdditionPreparation(compinst)
		// Connection Creation
		val conninst = connref.eContainer as ConnectionInstance
		var connadd = createConnection(conninst.kind, compinst.classifier as ComponentImplementation)
		connadd.set(namedElement_Name, ConnectionUtils.getDeclarativeConnectionName(connref))
		populateNewConnection(connadd, conninst, connref)
    }
    
    ////////////////////////////////
    // MODE DEINSTANTIATION RULES //
    ////////////////////////////////
    
    protected def modeInstanceCreatedDIM (ModeInstance modeinst) {
		val compinst = modeinst.eContainer as ComponentInstance
		// Parent Component Classifier Selection
		if (LibraryUtils.isLibraryClassifier(compinst.classifier, aadlPublicPackage) || (LibraryUtils.isReusedClassifier(compinst.classifier, engine) && !preferences.modifyReused)) {
			val classifier = {
				if (compinst.classifier instanceof ComponentImplementation) {
					switch (compinst.category) {
						case ComponentCategory.ABSTRACT : aadlPublicPackage.createChild(packageSection_OwnedClassifier,abstractImplementation)
    					case ComponentCategory.BUS : aadlPublicPackage.createChild(packageSection_OwnedClassifier,busImplementation)
   						case ComponentCategory.DATA : aadlPublicPackage.createChild(packageSection_OwnedClassifier,dataImplementation)
   						case ComponentCategory.DEVICE : aadlPublicPackage.createChild(packageSection_OwnedClassifier,deviceImplementation)
   						case ComponentCategory.MEMORY : aadlPublicPackage.createChild(packageSection_OwnedClassifier,memoryImplementation)
   						case ComponentCategory.PROCESS : aadlPublicPackage.createChild(packageSection_OwnedClassifier,processImplementation)
   						case ComponentCategory.PROCESSOR : aadlPublicPackage.createChild(packageSection_OwnedClassifier,processorImplementation)
   						case ComponentCategory.SUBPROGRAM : aadlPublicPackage.createChild(packageSection_OwnedClassifier,subprogramImplementation)
   						case ComponentCategory.SUBPROGRAM_GROUP : aadlPublicPackage.createChild(packageSection_OwnedClassifier,subprogramGroupImplementation)
   						case ComponentCategory.SYSTEM : aadlPublicPackage.createChild(packageSection_OwnedClassifier,systemImplementation)
   						case ComponentCategory.THREAD : aadlPublicPackage.createChild(packageSection_OwnedClassifier,threadImplementation)
   						case ComponentCategory.THREAD_GROUP : aadlPublicPackage.createChild(packageSection_OwnedClassifier,threadGroupImplementation)
   						case ComponentCategory.VIRTUAL_BUS : aadlPublicPackage.createChild(packageSection_OwnedClassifier,virtualBusImplementation)
   						case ComponentCategory.VIRTUAL_PROCESSOR : aadlPublicPackage.createChild(packageSection_OwnedClassifier,virtualProcessorImplementation)
					} as ComponentImplementation
				} else {
					 switch (compinst.category) {
						case ComponentCategory.ABSTRACT : aadlPublicPackage.createChild(packageSection_OwnedClassifier,abstractType)
    					case ComponentCategory.BUS : aadlPublicPackage.createChild(packageSection_OwnedClassifier,busType)
   						case ComponentCategory.DATA : aadlPublicPackage.createChild(packageSection_OwnedClassifier,dataType)
   						case ComponentCategory.DEVICE : aadlPublicPackage.createChild(packageSection_OwnedClassifier,deviceType)
   						case ComponentCategory.MEMORY : aadlPublicPackage.createChild(packageSection_OwnedClassifier,memoryType)
   						case ComponentCategory.PROCESS : aadlPublicPackage.createChild(packageSection_OwnedClassifier,processType)
   						case ComponentCategory.PROCESSOR : aadlPublicPackage.createChild(packageSection_OwnedClassifier,processorType)
   						case ComponentCategory.SUBPROGRAM : aadlPublicPackage.createChild(packageSection_OwnedClassifier,subprogramType)
   						case ComponentCategory.SUBPROGRAM_GROUP : aadlPublicPackage.createChild(packageSection_OwnedClassifier,subprogramGroupType)
   						case ComponentCategory.SYSTEM : aadlPublicPackage.createChild(packageSection_OwnedClassifier,systemType)
   						case ComponentCategory.THREAD : aadlPublicPackage.createChild(packageSection_OwnedClassifier,threadType)
   						case ComponentCategory.THREAD_GROUP : aadlPublicPackage.createChild(packageSection_OwnedClassifier,threadGroupType)
   						case ComponentCategory.VIRTUAL_BUS : aadlPublicPackage.createChild(packageSection_OwnedClassifier,virtualBusType)
   						case ComponentCategory.VIRTUAL_PROCESSOR : aadlPublicPackage.createChild(packageSection_OwnedClassifier,virtualProcessorType)
					} as ComponentType
				}
			}
			classifierCreationDIMPropertyAddition(classifier)
			classifier.set(namedElement_Name, compinst.name+".impl")
			if (classifier instanceof ComponentType) {
				classifier.set(componentType_Extended, compinst.classifier)		
			} else {
				classifier.set(componentImplementation_Extended, compinst.classifier)
			}
			if (LibraryUtils.isLibraryClassifier(compinst.classifier, aadlPublicPackage)) {
				aadlPublicPackage.add(packageSection_ImportedUnit, compinst.classifier.eContainer.eContainer as ModelUnit)		
			}
			compinst.set(componentInstance_Classifier, classifier)
		}
		// Parent Component Instance Subcomponent Definition
		if (compinst !== topSystemInst) {
			if (LibraryUtils.isAffectingInstanceObject(compinst,compinst.eContainer as ComponentInstance,aadlPublicPackage, engine, preferences.modifyReused, false)) {
				denyImplementnUpdatePropagn(compinst, compinst.eContainer as ComponentInstance, false)
			}	
		}
    	// Mode Creation
    	var classifier = compinst.classifier
    	var modeadd = classifier.createChild(componentClassifier_OwnedMode, mode)
		if (modeinst.name !== null) {
			modeadd.set(namedElement_Name, modeinst.name)	
		}
    	if (modeinst.initial) {
    		modeadd.set(mode_Initial, true)
    	} else {
    		modeadd.set(mode_Initial, false)
    	}	
    	// Mode Binding Creation
    	if (modeinst.derived == true) {
    		(modeadd.eContainer as ComponentClassifier).set(componentClassifier_DerivedModes, true)
    		var modebind = compinst.subcomponent.createChild(subcomponent_OwnedModeBinding, modeBinding)
    		modebind.set(modeBinding_DerivedMode, modeadd)
    		if (!modeinst.parents.isEmpty) {
    			modebind.set(modeBinding_ParentMode, modeinst.parents.get(0).mode)	
    		}
    	}
    	// Trace Creation
    	modeinst.set(modeInstance_Mode, modeadd)
    }
    
    protected def void modeChangedParentSubcomponentDefinition(ModeInstance modeinst) {
    	var compinst = modeinst.eContainer as ComponentInstance
    	var EList<? extends ComponentClassifier> extensionList = LibraryUtils.getAllInheritingParentClassifiers(modeinst.mode, compinst, false)
    	var extensionIndex = LibraryUtils.computeExtensionIndex(extensionList, aadlPublicPackage, engine, preferences.modifyReused)
    	if (extensionIndex !== -1) {
    		var ComponentClassifier previousClass
    		for (ComponentClassifier currentcompclass : extensionList.reverse) {
    			if (extensionList.indexOf(currentcompclass) < extensionList.size - extensionIndex) {
    				var compclasscopy = EcoreUtil.copy(currentcompclass)
    				classifierCreationDIMPropertyAddition(compclasscopy)
    				compclasscopy.moveTo(aadlPublicPackage, packageSection_OwnedClassifier)
    				compclasscopy.set(namedElement_Name,currentcompclass.name+"_dimcopy")
    				if(extensionList.indexOf(currentcompclass) == 0) {
    					var newMode = LibraryUtils.getCopyMode(modeinst.mode,compclasscopy)
    					modeinst.set(modeInstance_Mode, newMode)
    				} else {
    					if (compclasscopy instanceof ComponentType) {
    						compclasscopy.set(componentType_Extended, previousClass)	
    					} else {
    						compclasscopy.set(componentImplementation_Extended, previousClass)
    					}
    				}
    				previousClass = compclasscopy
    			} else {
    				if (compinst == topSystemInst || !LibraryUtils.isAffectingInstanceObject(compinst, compinst.eContainer as ComponentInstance, aadlPublicPackage, engine, preferences.modifyReused, false)) {
    					if (extensionList.indexOf(currentcompclass) == extensionList.size - extensionIndex) {
    						if (currentcompclass instanceof ComponentType) {
    							currentcompclass.set(componentType_Extended, previousClass)	
    						} else {
    							currentcompclass.set(componentImplementation_Extended, previousClass)
    						}
    					}
    				} else {
    					var compclasscopy = EcoreUtil.copy(currentcompclass)
    					classifierCreationDIMPropertyAddition(compclasscopy)
    					compclasscopy.moveTo(aadlPublicPackage, packageSection_OwnedClassifier)
    					compclasscopy.set(namedElement_Name,currentcompclass.name+"_dimcopy")
    					compclasscopy.set(componentImplementation_Extended, previousClass)	
    					previousClass = compclasscopy
    					if (extensionList.indexOf(currentcompclass) === extensionList.size - 1) {
    						compinst.set(componentInstance_Classifier, compclasscopy)
    						denyImplementnUpdatePropagn(compinst, compinst.eContainer as ComponentInstance, false)
    					}
    				}	 
    			}	
    		}	
    	} else if (compinst != topSystemInst && LibraryUtils.isAffectingInstanceObject(compinst,compinst.eContainer as ComponentInstance, aadlPublicPackage, engine, preferences.modifyReused, false)) {
    		denyImplementnUpdatePropagn(compinst, compinst.eContainer as ComponentInstance, false)
    	}
    	setSubcomponentType(compinst, compinst.classifier)
    }
    
    ///////////////////////////////////////////
    // MODE TRANSITION DEINSTANTIATION RULES //
    ///////////////////////////////////////////
    
    protected def void modeTransChangedParentSubcomponentDefinition(ModeTransitionInstance modetransinst) {
    	var compinst = modetransinst.eContainer as ComponentInstance
    	var EList<? extends ComponentClassifier> extensionList = LibraryUtils.getAllInheritingParentClassifiers(modetransinst.modeTransition, compinst, false)
    	var libraryIndex = LibraryUtils.getClosestLibraryClassifierIndex(extensionList, aadlPublicPackage)
    	var reusedIndex = LibraryUtils.getClosestReusedClassifierIndex(extensionList, engine) 
    	var extensionIndex = {
    		if (preferences.modifyReused) {
    			libraryIndex
    		} else {
    			if (libraryIndex == -1) {
    				reusedIndex
    			} else if (reusedIndex == -1) {
    				libraryIndex
    			} else {
    				Math.min(reusedIndex, libraryIndex)
    			}
    		}
    	}
    	if (extensionIndex !== -1) {
    		var ComponentClassifier previousClass
    		for (ComponentClassifier currentcompclass : extensionList.reverse) {
    			if (extensionList.indexOf(currentcompclass) < extensionList.size - extensionIndex) {
    				var compclasscopy = EcoreUtil.copy(currentcompclass)
    				classifierCreationDIMPropertyAddition(compclasscopy)
    				aadlPublicPackage.add(packageSection_OwnedClassifier, compclasscopy)
    				compclasscopy.set(namedElement_Name,currentcompclass.name+"_dimcopy")
    				if(extensionList.indexOf(currentcompclass) == 0) {
    					var newModeTrans = LibraryUtils.getCopyModeTransition(modetransinst.modeTransition,compclasscopy)
    					modetransinst.set(modeTransitionInstance_ModeTransition, newModeTrans)
    				} else {
    					if (compclasscopy instanceof ComponentType) {
    						compclasscopy.set(componentType_Extended, previousClass)	
    					} else {
    						compclasscopy.set(componentImplementation_Extended, previousClass)
    					}
    				}
    				previousClass = compclasscopy
    			} else {
    				if (compinst == topSystemInst || !LibraryUtils.isAffectingInstanceObject(compinst,compinst.eContainer as ComponentInstance,aadlPublicPackage,engine,preferences.modifyReused, false)) {
    					if (extensionList.indexOf(currentcompclass) == extensionList.size - extensionIndex) {
    						if (currentcompclass instanceof ComponentType) {
    							currentcompclass.set(componentType_Extended, previousClass)	
    						} else {
    							currentcompclass.set(componentImplementation_Extended, previousClass)
    						}
    					}
    				} else {
    					var compclasscopy = EcoreUtil.copy(currentcompclass)
    					classifierCreationDIMPropertyAddition(compclasscopy)
    					aadlPublicPackage.add(packageSection_OwnedClassifier, compclasscopy)
    					compclasscopy.set(namedElement_Name,currentcompclass.name+"_dimcopy")
    					compclasscopy.set(componentImplementation_Extended, previousClass)	
    					previousClass = compclasscopy
    					if (extensionList.indexOf(currentcompclass) === extensionList.size - 1) {
    						compinst.set(componentInstance_Classifier, compclasscopy)
    						denyImplementnUpdatePropagn(compinst, compinst.eContainer as ComponentInstance, false)
    					}
    				}	 
    			}	
    		}	
    	} else if (compinst != topSystemInst && LibraryUtils.isAffectingInstanceObject(compinst,compinst.eContainer as ComponentInstance,aadlPublicPackage,engine,preferences.modifyReused, false)) {
    		denyImplementnUpdatePropagn(compinst, compinst.eContainer as ComponentInstance, false)
    	}
    	setSubcomponentType(compinst, compinst.classifier)
    }
    
    protected def modeTransitionInstanceCreatedDIM (ModeTransitionInstance modetransinst) {
    	var compinst = modetransinst.eContainer as ComponentInstance
    	// Parent Component Classifier Selection
		if (LibraryUtils.isLibraryClassifier(compinst.classifier, aadlPublicPackage) || (LibraryUtils.isReusedClassifier(compinst.classifier,engine) && !preferences.modifyReused)) {
			val classifier = {
				if (compinst.classifier instanceof ComponentImplementation) {
					switch (compinst.category) {
						case ComponentCategory.ABSTRACT : aadlPublicPackage.createChild(packageSection_OwnedClassifier,abstractImplementation)
    					case ComponentCategory.BUS : aadlPublicPackage.createChild(packageSection_OwnedClassifier,busImplementation)
   						case ComponentCategory.DATA : aadlPublicPackage.createChild(packageSection_OwnedClassifier,dataImplementation)
   						case ComponentCategory.DEVICE : aadlPublicPackage.createChild(packageSection_OwnedClassifier,deviceImplementation)
   						case ComponentCategory.MEMORY : aadlPublicPackage.createChild(packageSection_OwnedClassifier,memoryImplementation)
   						case ComponentCategory.PROCESS : aadlPublicPackage.createChild(packageSection_OwnedClassifier,processImplementation)
   						case ComponentCategory.PROCESSOR : aadlPublicPackage.createChild(packageSection_OwnedClassifier,processorImplementation)
   						case ComponentCategory.SUBPROGRAM : aadlPublicPackage.createChild(packageSection_OwnedClassifier,subprogramImplementation)
   						case ComponentCategory.SUBPROGRAM_GROUP : aadlPublicPackage.createChild(packageSection_OwnedClassifier,subprogramGroupImplementation)
   						case ComponentCategory.SYSTEM : aadlPublicPackage.createChild(packageSection_OwnedClassifier,systemImplementation)
   						case ComponentCategory.THREAD : aadlPublicPackage.createChild(packageSection_OwnedClassifier,threadImplementation)
   						case ComponentCategory.THREAD_GROUP : aadlPublicPackage.createChild(packageSection_OwnedClassifier,threadGroupImplementation)
   						case ComponentCategory.VIRTUAL_BUS : aadlPublicPackage.createChild(packageSection_OwnedClassifier,virtualBusImplementation)
   						case ComponentCategory.VIRTUAL_PROCESSOR : aadlPublicPackage.createChild(packageSection_OwnedClassifier,virtualProcessorImplementation)
					} as ComponentImplementation
				} else {
					 switch (compinst.category) {
						case ComponentCategory.ABSTRACT : aadlPublicPackage.createChild(packageSection_OwnedClassifier,abstractType)
    					case ComponentCategory.BUS : aadlPublicPackage.createChild(packageSection_OwnedClassifier,busType)
   						case ComponentCategory.DATA : aadlPublicPackage.createChild(packageSection_OwnedClassifier,dataType)
   						case ComponentCategory.DEVICE : aadlPublicPackage.createChild(packageSection_OwnedClassifier,deviceType)
   						case ComponentCategory.MEMORY : aadlPublicPackage.createChild(packageSection_OwnedClassifier,memoryType)
   						case ComponentCategory.PROCESS : aadlPublicPackage.createChild(packageSection_OwnedClassifier,processType)
   						case ComponentCategory.PROCESSOR : aadlPublicPackage.createChild(packageSection_OwnedClassifier,processorType)
   						case ComponentCategory.SUBPROGRAM : aadlPublicPackage.createChild(packageSection_OwnedClassifier,subprogramType)
   						case ComponentCategory.SUBPROGRAM_GROUP : aadlPublicPackage.createChild(packageSection_OwnedClassifier,subprogramGroupType)
   						case ComponentCategory.SYSTEM : aadlPublicPackage.createChild(packageSection_OwnedClassifier,systemType)
   						case ComponentCategory.THREAD : aadlPublicPackage.createChild(packageSection_OwnedClassifier,threadType)
   						case ComponentCategory.THREAD_GROUP : aadlPublicPackage.createChild(packageSection_OwnedClassifier,threadGroupType)
   						case ComponentCategory.VIRTUAL_BUS : aadlPublicPackage.createChild(packageSection_OwnedClassifier,virtualBusType)
   						case ComponentCategory.VIRTUAL_PROCESSOR : aadlPublicPackage.createChild(packageSection_OwnedClassifier,virtualProcessorType)
					} as ComponentType
				}
			}
			classifierCreationDIMPropertyAddition(classifier)
			classifier.set(namedElement_Name, compinst.name+".impl")
			if (classifier instanceof ComponentType) {
				classifier.set(componentType_Extended, compinst.classifier)		
			} else {
				classifier.set(componentImplementation_Extended, compinst.classifier)
			}
			if (LibraryUtils.isLibraryClassifier(compinst.classifier, aadlPublicPackage)) {
				aadlPublicPackage.add(packageSection_ImportedUnit, compinst.classifier.eContainer.eContainer as ModelUnit)		
			}
			compinst.set(componentInstance_Classifier, classifier)
		}
		// Parent Component Instance Subcomponent Definition
		if (compinst !== topSystemInst) {
			if (LibraryUtils.isAffectingInstanceObject(compinst,compinst.eContainer as ComponentInstance,aadlPublicPackage,engine,preferences.modifyReused, false)) {
				denyImplementnUpdatePropagn(compinst, compinst.eContainer as ComponentInstance, false)
			}	
		}
    	// Mode Transition Creation
		var modetransadd = compinst.classifier.createChild(componentClassifier_OwnedModeTransition, modeTransition)
		if (modetransinst.name !== null) {
			modetransadd.set(namedElement_Name, modetransinst.name)	
		}
		if (modetransinst.source !== null) {
			modetransadd.set(modeTransition_Source, (modetransinst.source as ModeInstance).mode)	
		}
		if (modetransinst.destination !== null) {
			modetransadd.set(modeTransition_Destination, (modetransinst.destination as ModeInstance).mode)	
		}
		// Mode Transition Trigger Creation
		for (FeatureInstance trigger : modetransinst.triggers) {
			var modetrigadd = modetransadd.createChild(modeTransition_OwnedTrigger, modeTransitionTrigger)
			modetrigadd.set(modeTransitionTrigger_TriggerPort, trigger.feature)	
		}
		// Trace Creation
		modetransinst.set(modeTransitionInstance_ModeTransition, modetransadd)
    }
    
    ////////////////////////////////////
    // PROPERTY DEINSTANTIATION RULES //
    ////////////////////////////////////
    
    protected def propertyInstanceCreatedDIM(PropertyAssociationInstance propinst, PublicPackageSection aadlPackage) {
    	//Property PropertySet Addition
    	if (propinst.property !== null) {
    		if (!aadlPackage.importedUnits.contains(propinst.property.eContainer)) {
    			if (aadlPackage.noAnnexes === true) {
    				aadlPackage.set(packageSection_NoAnnexes, false)
    			}
    			if (!AadlUtil.isPredeclaredPropertySet((propinst.property.eContainer as PropertySet).name)) {
    				aadlPackage.add(packageSection_ImportedUnit, propinst.property.eContainer)
    			}
    		}
    	}	
    	// PropertyAssociation Container Identification
		var objinst = {
			if (propinst.property !== null) {
				if (PropertyTypeUtils.isValueProperty(propinst.property.propertyType)) {
					propinst.eContainer
				} else {
//					if (engine.findPropertyValue.getOneArbitraryMatch(propinst, null,null).present) {
						InstanceHierarchyUtils.getRefPropertyLowestCommonParentInstance(propinst)	
//					} else {
//						propinst.eContainer
//					}
				}	
			} else {
				propinst.eContainer
			}
		}
		// PropertyAssociation Creation
		var propadd = {
			if (objinst instanceof SystemInstance && (objinst as ComponentInstance).subcomponent === null) {
				(objinst as SystemInstance).componentImplementation.createChild(namedElement_OwnedPropertyAssociation, propertyAssociation)
			} else if (objinst instanceof ComponentInstance) {
				if (objinst.subcomponent !== null) {
					objinst.subcomponent.createChild(namedElement_OwnedPropertyAssociation, propertyAssociation)	
				} else if (objinst.classifier !== null) {
					objinst.classifier.createChild(namedElement_OwnedPropertyAssociation, propertyAssociation)
				}
			} else if (objinst instanceof FeatureInstance) {
				objinst.feature.createChild(namedElement_OwnedPropertyAssociation, propertyAssociation)	
			} else if (objinst instanceof ConnectionInstance) {
				if (objinst.connectionReferences.size == 1) {
					objinst.connectionReferences.get(0).connection.createChild(namedElement_OwnedPropertyAssociation, propertyAssociation)	
				} else {
					null
				}
			} else if (objinst instanceof ModeInstance) {
				objinst.mode.createChild(namedElement_OwnedPropertyAssociation, propertyAssociation)
			} else if (objinst instanceof ModeTransitionInstance) {
				objinst.modeTransition.createChild(namedElement_OwnedPropertyAssociation, propertyAssociation)}
		}
		if (propadd === null) {
			LOGGER.logWarning("DIM: Property instance "+propinst.property.name+" not attached to connection due to multiple connection references")
		} else {
			(propadd as PropertyAssociation).set(propertyAssociation_Property, propinst.property)
			// PropertyAssociation AppliesTo
			if (objinst != propinst.eContainer) {
				var contelem = (propadd as PropertyAssociation).createChild(propertyAssociation_AppliesTo, containedNamedElement)
				appliesToPathDefinition(contelem as ContainedNamedElement, objinst as InstanceObject, propinst.eContainer as InstanceObject)
			}
			// Trace Creation
			propinst.set(propertyAssociationInstance_PropertyAssociation, propadd)
		}
    }
    
    protected def ModalPropertyValue modalPropertyCreatedDIM(ModalPropertyValue modpropinst) {
    	// ModalPropertyValue Creation
		var modpropadd = (modpropinst.eContainer as PropertyAssociationInstance).propertyAssociation.createChild(propertyAssociation_OwnedValue, modalPropertyValue)
		// ModalPropertyValue inModes
		if (!(modpropinst.inModes.isEmpty())) {
			for (Mode modpropmode : modpropinst.inModes) {
				modpropadd.add(modalElement_InMode, ModeUtils.findObjectModeInstInSOM(modpropinst.eContainer.eContainer as InstanceObject, modpropmode as SystemOperationMode).mode)
			}
		}
		return modpropadd as ModalPropertyValue
	}
    
    protected def appliesToPathDefinition(ContainedNamedElement refvaladd, InstanceObject objinst, InstanceObject refinstobj) {
    	var refpathadd = refvaladd.createChild(containedNamedElement_Path, containmentPathElement)
		var containmentPath = InstanceHierarchyUtils.getContainmentPath(objinst as InstanceObject, refinstobj)
		refpathadd.set(containmentPathElement_NamedElement, TraceUtils.getDeclarativeElement(containmentPath.get(0)))
		for (InstanceObject refpathinstobj : containmentPath) {
			if (InstanceHierarchyUtils.getContainmentPath(objinst as InstanceObject, refinstobj).indexOf(refpathinstobj) != 0) {
				refpathadd = refpathadd.createChild(containmentPathElement_Path, containmentPathElement)
				refpathadd.set(containmentPathElement_NamedElement,TraceUtils.getDeclarativeElement(refpathinstobj))
			}
		}
	}
	
	protected def rangeValueDefinition(RangeValue rangeval, RangeValue rangevaladd){
		if (rangeval.maximum instanceof IntegerLiteral) {
			var maxvaladd = rangevaladd.createChild(rangeValue_Maximum, integerLiteral)
			maxvaladd.set(integerLiteral_Value, (rangeval.maximum as IntegerLiteral).value)
			maxvaladd.set(integerLiteral_Base, (rangeval.maximum as IntegerLiteral).base)
			maxvaladd.set(numberValue_Unit, (rangeval.maximum as IntegerLiteral).unit)
			var minvaladd = rangevaladd.createChild(rangeValue_Minimum, integerLiteral)
			minvaladd.set(integerLiteral_Value, (rangeval.minimum as IntegerLiteral).value)
			minvaladd.set(integerLiteral_Base, (rangeval.minimum as IntegerLiteral).base)
			minvaladd.set(numberValue_Unit, (rangeval.minimum as IntegerLiteral).unit)
		} else {
			var maxvaladd = rangevaladd.createChild(rangeValue_Maximum, realLiteral)
			maxvaladd.set(realLiteral_Value, (rangeval.maximum as RealLiteral).value)
			maxvaladd.set(numberValue_Unit, (rangeval.maximum as RealLiteral).unit)
			var minvaladd = rangevaladd.createChild(rangeValue_Minimum, realLiteral)
			minvaladd.set(realLiteral_Value, (rangeval.minimum as RealLiteral).value)
			minvaladd.set(numberValue_Unit, (rangeval.minimum as RealLiteral).unit)			
		}
	}
	
	protected def simplePropertyValueCreation(PropertyType proptype, PropertyValue propvalinst, Element propvalcontainer) {
		if (PropertyTypeUtils.isValueProperty(proptype)) {
			valuePropertyValueCreation(propvalinst, propvalcontainer)
		} else {
			val propassocinst = PropertyUtils.getContainingPropertyAssociationInstance(propvalinst)
			var refvaladd = propvalcontainer.createChild(propertyValueContainmentReference(propvalcontainer), referenceValue)
			if ((propvalinst as InstanceReferenceValue).referencedInstanceObject !== null) {
				if (InstanceHierarchyUtils.getAllParentInstanceObject((propvalinst as InstanceReferenceValue).referencedInstanceObject).contains(propassocinst.eContainer)) {
					appliesToPathDefinition(refvaladd as ContainedNamedElement, propassocinst.eContainer as InstanceObject, (propvalinst as InstanceReferenceValue).referencedInstanceObject)
				} else {
					var propassoc = propassocinst.propertyAssociation as PropertyAssociation
					var objinst = InstanceHierarchyUtils.getLowestCommonParentInstanceObject((propvalinst as InstanceReferenceValue).referencedInstanceObject, propassocinst.eContainer as InstanceObject)
					propassoc.moveTo(TraceUtils.getDeclarativeElement(objinst), namedElement_OwnedPropertyAssociation)
					propassoc.appliesTos.clear()
					var contelem = (propassoc as PropertyAssociation).createChild(propertyAssociation_AppliesTo, containedNamedElement)
					appliesToPathDefinition(contelem as ContainedNamedElement, objinst, propassocinst.eContainer as InstanceObject)
					appliesToPathDefinition(refvaladd as ContainedNamedElement, objinst, (propvalinst as InstanceReferenceValue).referencedInstanceObject)
					LOGGER.logInfo("DIM: Property association of property "+propassoc.property.name+" moved due to change in ReferenceValue LCPCI position")
				}		
			}
		}
	}
	
	private def EReference propertyValueContainmentReference(Element propvalcontainer) {
		return {
			if (propvalcontainer instanceof BasicPropertyAssociation) {
				basicPropertyAssociation_OwnedValue
			} else if (propvalcontainer instanceof ModalPropertyValue) {
				modalPropertyValue_OwnedValue
			} else if (propvalcontainer instanceof ListValue) {
				listValue_OwnedListElement
			} else {
				null 
			}
		}
	}
	
	private def valuePropertyValueCreation(PropertyValue propval, Element propvalcontainer) {
		var EReference reference = propertyValueContainmentReference(propvalcontainer)
		if (propval instanceof StringLiteral) {
			var propvaladd = propvalcontainer.createChild(reference, stringLiteral)
			propvaladd.set(stringLiteral_Value, (propval as StringLiteral).value)
		} else if (propval instanceof IntegerLiteral) {
			var propvaladd = propvalcontainer.createChild(reference, integerLiteral)
			propvaladd.set(integerLiteral_Value, (propval as IntegerLiteral).value)
			propvaladd.set(integerLiteral_Base, (propval as IntegerLiteral).base)
			propvaladd.set(numberValue_Unit, (propval as IntegerLiteral).unit)
		} else if (propval instanceof RealLiteral) {
			var propvaladd = propvalcontainer.createChild(reference, realLiteral)
			propvaladd.set(realLiteral_Value, (propval as RealLiteral).value)
			propvaladd.set(numberValue_Unit, (propval as RealLiteral).unit)
		} else if (propval instanceof ClassifierValue) {
			var propvaladd = propvalcontainer.createChild(reference, classifierValue)
			if ((propval as ClassifierValue).classifier !== null && LibraryUtils.isLibraryClassifier((propval as ClassifierValue).classifier as ComponentClassifier, aadlPublicPackage)) {
				aadlPublicPackage.add(packageSection_ImportedUnit, (propval as ClassifierValue).classifier.eContainer.eContainer as ModelUnit)		
			}
			propvaladd.set(classifierValue_Classifier, (propval as ClassifierValue).classifier)
		} else if (propval instanceof BooleanLiteral) {
			var propvaladd = propvalcontainer.createChild(reference, booleanLiteral)
			propvaladd.set(booleanLiteral_Value, (propval as BooleanLiteral).getValue())
		} else if (propval instanceof RangeValue) {
			var propvaladd = propvalcontainer.createChild(reference, rangeValue)
			propvaladd.set(rangeValue_Delta, (propval as RangeValue).delta)
			rangeValueDefinition(propval, propvaladd as RangeValue)
		} else if (propval instanceof ComputedValue) {
			var propvaladd = propvalcontainer.createChild(reference, computedValue)
			propvaladd.set(computedValue_Function, (propval as ComputedValue).function)
		} else if (propval instanceof NamedValue) {
			var propvaladd = propvalcontainer.createChild(reference, namedValue)
			propvaladd.set(namedValue_NamedValue, (propval as NamedValue).namedValue)
		}
	}
}