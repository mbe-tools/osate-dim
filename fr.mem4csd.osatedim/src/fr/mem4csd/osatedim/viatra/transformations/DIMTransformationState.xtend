package fr.mem4csd.osatedim.viatra.transformations

import fr.mem4csd.osatedim.preference.DIMPreferences
import fr.mem4csd.osatedim.utils.PropertyUtils
import fr.mem4csd.osatedim.viatra.queries.FindConnectionReference
import fr.mem4csd.osatedim.viatra.queries.FindDerivedMode
import fr.mem4csd.osatedim.viatra.queries.FindFeature
import fr.mem4csd.osatedim.viatra.queries.FindInstanceObject
import fr.mem4csd.osatedim.viatra.queries.FindMode
import fr.mem4csd.osatedim.viatra.queries.FindModeTransition
import fr.mem4csd.osatedim.viatra.queries.FindProperty
import fr.mem4csd.osatedim.viatra.queries.FindPropertyReferencingElements
import fr.mem4csd.osatedim.viatra.queries.FindPropertyValue
import fr.mem4csd.osatedim.viatra.queries.FindSubcomponent
import fr.mem4csd.osatedim.viatra.queries.FindSystem
import fr.mem4csd.osatedim.viatra.transformations.MScheduler.MSchedulerFactory
import java.io.FileNotFoundException
import org.eclipse.emf.ecore.InternalEObject
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine
import org.eclipse.viatra.transformation.evm.specific.Lifecycles
import org.eclipse.viatra.transformation.evm.specific.crud.CRUDActivationStateEnum
import org.eclipse.viatra.transformation.evm.specific.resolver.InvertedDisappearancePriorityConflictResolver
import org.eclipse.viatra.transformation.runtime.emf.modelmanipulation.SimpleModelManipulations
import org.eclipse.viatra.transformation.runtime.emf.transformation.eventdriven.EventDrivenTransformation
import org.eclipse.viatra.transformation.runtime.emf.transformation.eventdriven.EventDrivenTransformation.EventDrivenTransformationBuilder
import org.osate.aadl2.BasicProperty
import org.osate.aadl2.BasicPropertyAssociation
import org.osate.aadl2.ClassifierValue
import org.osate.aadl2.PropertySet
import org.osate.aadl2.PublicPackageSection
import org.osate.aadl2.ReferenceValue
import org.osate.aadl2.instance.ComponentInstance
import org.osate.aadl2.instance.ConnectionInstance
import org.osate.aadl2.instance.ConnectionReference
import org.osate.aadl2.instance.FeatureInstance
import org.osate.aadl2.instance.InstanceObject
import org.osate.aadl2.instance.ModeInstance
import org.osate.aadl2.instance.ModeTransitionInstance
import org.osate.aadl2.instance.PropertyAssociationInstance
import org.osate.aadl2.instance.SystemInstance
import org.osate.aadl2.modelsupport.FileNameConstants
import fr.mem4csd.osatedim.utils.DIMLogger
import fr.mem4csd.osatedim.viatra.queries.FindConnection
import fr.mem4csd.osatedim.viatra.queries.FindExtraComponent
import org.osate.aadl2.ComponentCategory
import fr.mem4csd.osatedim.utils.AnnexUtils
import org.osate.aadl2.NamedElement

class DIMTransformationState extends DIMTransformationRulesState {
 
 	public extension EventDrivenTransformation transformation
 	
	new(ViatraQueryEngine engine, SystemInstance topSystemInst, PublicPackageSection aadlPublicPackage, DIMPreferences preferences, PropertySet dimProperties, DIMLogger logger){
		this.topSystemInst = topSystemInst
		this.aadlPublicPackage = aadlPublicPackage
		this.engine = engine
		this.preferences = preferences
		this.dimPropertySet  = dimProperties
		this.LOGGER = logger
		prepare(engine)
		createTransformation
	}
	
	def execute() {
		transformation.executionSchema.startUnscheduledExecution	
    }   
    
    def dispose() {
        if (transformation !== null) {
            transformation.executionSchema.dispose
            transformation = null
        }
        return
    }
	
 	private def createTransformation() {
    	this.manipulation = new SimpleModelManipulations(engine)
        var fixedPriorityResolver = new InvertedDisappearancePriorityConflictResolver
		fixedPriorityResolver.setPriority(checkInstancePropertyExistence.ruleSpecification, 1)
		fixedPriorityResolver.setPriority(cleanInstance2Declarative.ruleSpecification, 2)
		fixedPriorityResolver.setPriority(cleanPropertyInstance2Declarative.ruleSpecification, 2)
		fixedPriorityResolver.setPriority(extraComponentInstance2Declarative.ruleSpecification, 3)
		fixedPriorityResolver.setPriority(topSystemInstance2Declarative.ruleSpecification, 3)
		fixedPriorityResolver.setPriority(componentInstance2Declarative.ruleSpecification, 4)
		fixedPriorityResolver.setPriority(featureInstance2Declarative.ruleSpecification, 5)
		fixedPriorityResolver.setPriority(connectionInstance2Reference.ruleSpecification, 6)
		fixedPriorityResolver.setPriority(connectionReference2Declarative.ruleSpecification, 7)
		fixedPriorityResolver.setPriority(modeInstance2Declarative.ruleSpecification, 8)
		fixedPriorityResolver.setPriority(derivedModeInstance2Declarative.ruleSpecification, 9)
		fixedPriorityResolver.setPriority(modeTransitionInstance2Declarative.ruleSpecification, 10)
		fixedPriorityResolver.setPriority(propertyInstance2Declarative.ruleSpecification, 11)
//		fixedPriorityResolver.setPriority(flowspecinstance2declarative.ruleSpecification, 11)
		
		var EventDrivenTransformationBuilder builder = EventDrivenTransformation.forEngine(engine).setConflictResolver(fixedPriorityResolver)
		.addRule(checkInstancePropertyExistence)
		.addRule(cleanInstance2Declarative)
		.addRule(cleanPropertyInstance2Declarative)
		.addRule(extraComponentInstance2Declarative)
		.addRule(topSystemInstance2Declarative)
		.addRule(componentInstance2Declarative)
		.addRule(featureInstance2Declarative)
		.addRule(connectionInstance2Reference)
		.addRule(connectionReference2Declarative)
		.addRule(modeInstance2Declarative)
		.addRule(derivedModeInstance2Declarative)
		.addRule(modeTransitionInstance2Declarative)
		.addRule(propertyInstance2Declarative)
//		.addRule(flowspecinstance2declarative)

		builder.schedulerFactory = new MSchedulerFactory()
		
		transformation = builder.build
	}
	
	// Check for existence of properties
	protected val checkInstancePropertyExistence = createRule(FindPropertyReferencingElements.instance)
	.action(CRUDActivationStateEnum.CREATED) [
		val BasicProperty property = {
			if (element instanceof PropertyAssociationInstance) {
				(element as PropertyAssociationInstance).property as BasicProperty
			} else if (element instanceof BasicPropertyAssociation){
				(element as BasicPropertyAssociation).property
			}
		}
		if (property.eContainer === null) {
			throw new FileNotFoundException((property as InternalEObject).eProxyURI.toString+" not found!")
		}
	].addLifeCycle(Lifecycles.getDefault(true, true)).build
    
	// Clean aaxl2aadl traces
    protected val cleanInstance2Declarative = createRule(FindInstanceObject.instance)
	.action(CRUDActivationStateEnum.CREATED) [
		if (objinst instanceof ComponentInstance) {
			objinst.set(componentInstance_Classifier, null)
			LOGGER.logInfo("DIM: Component instance "+objinst.name+" cleaned")	
		} else if (objinst instanceof FeatureInstance) {
			objinst.set(featureInstance_Feature, null)
			LOGGER.logInfo("DIM: Feature instance "+objinst.name+" cleaned")	
		} else if (objinst instanceof ConnectionInstance) {
			for (ConnectionReference connref : (objinst as ConnectionInstance).connectionReferences) {
				connref.set(connectionReference_Connection, null)
			}
			LOGGER.logInfo("DIM: Connection instance "+objinst.name+" cleaned")	
		} else if (objinst instanceof ModeInstance) {
			objinst.set(modeInstance_Mode, null)
			LOGGER.logInfo("DIM: Mode instance "+objinst.name+" cleaned")
		} else if (objinst instanceof ModeTransitionInstance) {
			objinst.set(modeTransitionInstance_ModeTransition, null)
			LOGGER.logInfo("DIM: Mode Transition instance "+objinst.name+" cleaned")
		} else if (objinst instanceof PropertyAssociationInstance) {
			objinst.set(propertyAssociationInstance_PropertyAssociation, null)
			LOGGER.logInfo("DIM: Property instance "+(objinst as PropertyAssociationInstance).property.name+" cleaned")
		}
	].addLifeCycle(Lifecycles.getDefault(true, true)).build
	
	protected val cleanPropertyInstance2Declarative = createRule(FindPropertyValue.instance)
	.action(CRUDActivationStateEnum.CREATED) [
		val modpropinst = PropertyUtils.getContainingModalPropertyValue(propvalinst)
		if (modpropinst !== null && modpropinst.eContainer instanceof PropertyAssociationInstance) {
			if (propvalinst instanceof ClassifierValue || propvalinst instanceof ReferenceValue) {
				modpropinst.eContainer.remove()
				LOGGER.logInfo("DIM: Property instance "+(modpropinst.eContainer as PropertyAssociationInstance).property.name+" removed since it has declarative references.")
			}
		}
	].addLifeCycle(Lifecycles.getDefault(true, true)).build
	
	// Extra Data ComponentInstance transformation
    protected val extraComponentInstance2Declarative = createRule(FindExtraComponent.Matcher.querySpecification)
	.action(CRUDActivationStateEnum.CREATED) [
		if (compinst.category == ComponentCategory.DATA) {
				var datatype = aadlPublicPackage.createChild(packageSection_OwnedClassifier, dataType)
				if (compinst.name.contains(FileNameConstants.AADL_PACKAGE_SEPARATOR)) {
					datatype.set(namedElement_Name, compinst.name.substring(compinst.name.indexOf(FileNameConstants.AADL_PACKAGE_SEPARATOR)+2))
					val packagename = compinst.name.substring(0,compinst.name.indexOf(FileNameConstants.AADL_PACKAGE_SEPARATOR))
					if (AnnexUtils.isAnnexContributionPackage(packagename)) {
						val pack = AnnexUtils.loadAnnexPackage(aadlPublicPackage.eResource.resourceSet, packagename)
						if (!aadlPublicPackage.importedUnits.contains(pack)) {
    						if (aadlPublicPackage.noAnnexes === true) {
    							aadlPublicPackage.set(packageSection_NoAnnexes, false)
    						}
    						aadlPublicPackage.add(packageSection_ImportedUnit, pack)
    					}
    					datatype.set(componentType_Extended, AnnexUtils.getClassifier(pack.ownedPublicSection, (datatype as NamedElement).name))
					}
				} else {
					datatype.set(namedElement_Name, compinst.name)
				}
				// aaxl2aadl Trace Creation
				compinst.set(componentInstance_Classifier,datatype)
				LOGGER.logInfo("DIM: Extra data component instance "+compinst.name+" de-instantiated")	
		} 
	].addLifeCycle(Lifecycles.getDefault(true, true)).build
	
	// Top-level SystemInstance transformation
	protected val topSystemInstance2Declarative = createRule(FindSystem.instance)
	.action(CRUDActivationStateEnum.CREATED) [
		//System Type Creation
		var systemtype = aadlPublicPackage.createChild(packageSection_OwnedClassifier, systemType)
		systemtype.set(namedElement_Name,systeminst.name.substring(0,systeminst.name.indexOf("_")))
		//System Implementation Creation
		var systemimp = aadlPublicPackage.createChild(packageSection_OwnedClassifier, systemImplementation)
		systemimp.set(namedElement_Name,systeminst.name.substring(0,systeminst.name.indexOf("_"))+"."+systeminst.name.substring(systeminst.name.indexOf("_")+1,systeminst.name.indexOf(FileNameConstants.INSTANCE_MODEL_POSTFIX)))
		systemimp.set(componentImplementation_Type,systemtype)
		//aaxl2aadl Trace Creation
		systeminst.set(systemInstance_ComponentImplementation,systemimp)
		systeminst.set(componentInstance_Classifier,systemimp)
		LOGGER.logInfo("DIM: Top-level system instance "+systeminst.name+" de-instantiated")
	].addLifeCycle(Lifecycles.getDefault(true, true)).build

    // SubComponentInstance transformation     
    protected val componentInstance2Declarative = createRule(FindSubcomponent.instance)
	.action(CRUDActivationStateEnum.CREATED) [
		componentInstanceCreatedDIM(subcompinst)
		LOGGER.logInfo("DIM: Component instance "+subcompinst.name+" de-instantiated")
	].addLifeCycle(Lifecycles.getDefault(true, true)).build
    
    // FeatureInstance transformation
    protected val featureInstance2Declarative = createRule(FindFeature.instance) 
	.action(CRUDActivationStateEnum.CREATED) [
		featureInstanceCreatedDIM(featinst)
		LOGGER.logInfo("DIM: Feature instance "+featinst.name+" de-instantiated")
	].addLifeCycle(Lifecycles.getDefault(true, true)).build
    
    // ConnectionInstance transformation
    protected val connectionInstance2Reference = createRule(FindConnection.instance)
	.action(CRUDActivationStateEnum.CREATED) [
		connectionInstanceCreatedDIM(conninst)
		LOGGER.logInfo("DIM: Connection instance "+conninst.name+" defined as connection reference")
	].addLifeCycle(Lifecycles.getDefault(true, true)).build
    
    // ConnectionReference transformation
    protected val connectionReference2Declarative = createRule(FindConnectionReference.instance)
	.action(CRUDActivationStateEnum.CREATED) [
		connectionReferenceFoundDIM(connref)
		LOGGER.logInfo("DIM: Connection reference "+connref.connection.name+" de-instantiated")
	].addLifeCycle(Lifecycles.getDefault(true, true)).build
    
    // ModeInstance Transformation
    protected val modeInstance2Declarative = createRule(FindMode.instance)
    .action(CRUDActivationStateEnum.CREATED) [
    	modeInstanceCreatedDIM(modeinst) 	
    	LOGGER.logInfo("DIM: Mode instance "+modeinst.name+" for component "+(modeinst.eContainer as InstanceObject).name+" de-instantiated")
    ].addLifeCycle(Lifecycles.getDefault(true, true)).build
    
    protected val derivedModeInstance2Declarative = createRule(FindDerivedMode.instance)
    .action(CRUDActivationStateEnum.CREATED) [
    	modeInstanceCreatedDIM(modeinst) 	
    	LOGGER.logInfo("DIM: Mode instance "+modeinst.name+" for component "+(modeinst.eContainer as InstanceObject).name+" de-instantiated")
    ].addLifeCycle(Lifecycles.getDefault(true, true)).build
    
    // ModeTransitionInstance Transformation
	protected val modeTransitionInstance2Declarative = createRule(FindModeTransition.instance)
    .action(CRUDActivationStateEnum.CREATED) [
    	modeTransitionInstanceCreatedDIM(modetransinst)
    	LOGGER.logInfo("DIM: ModeTransition instance "+modetransinst.name+" de-instantiated")
    ].addLifeCycle(Lifecycles.getDefault(true, true)).build
    
    // PropertyAssociation Instance Transformation
    protected val propertyInstance2Declarative = createRule(FindProperty.instance)
	.action(CRUDActivationStateEnum.CREATED) [
		if (!PropertyUtils.isInheritedProperty(propinst)) {
			propertyInstanceFoundDIM(propinst, aadlPublicPackage)
			LOGGER.logInfo("DIM: Property instance "+propinst.property.name+" attached to "+(propinst.eContainer as InstanceObject).name+" de-instantiated")
		} else {
			LOGGER.logInfo("DIM: Property instance "+propinst.property.name+" attached to "+(propinst.eContainer as InstanceObject).name+" inherited")
		}
	].addLifeCycle(Lifecycles.getDefault(true, true)).build
}