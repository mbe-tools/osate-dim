package fr.mem4csd.osatedim.viatra.transformations

import fr.mem4csd.osatedim.viatra.queries.DIMQueriesOutplace
import fr.mem4csd.osatedim.viatra.queries.Find_PropertyAttach2Component
import fr.mem4csd.osatedim.utils.InstanceHierarchyUtils
import fr.mem4csd.osatedim.utils.PropertyTypeUtils
import fr.mem4csd.osatedim.utils.TraceUtils
import org.eclipse.emf.ecore.resource.Resource
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine
import org.eclipse.viatra.transformation.evm.specific.Lifecycles
import org.eclipse.viatra.transformation.evm.specific.crud.CRUDActivationStateEnum
import org.eclipse.viatra.transformation.evm.specific.resolver.InvertedDisappearancePriorityConflictResolver
import org.eclipse.viatra.transformation.runtime.emf.modelmanipulation.IModelManipulations
import org.eclipse.viatra.transformation.runtime.emf.modelmanipulation.SimpleModelManipulations
import org.eclipse.viatra.transformation.runtime.emf.rules.eventdriven.EventDrivenTransformationRuleFactory
import org.eclipse.viatra.transformation.runtime.emf.transformation.eventdriven.EventDrivenTransformation
import org.osate.aadl2.Aadl2Package
import org.osate.aadl2.ListValue
import org.osate.aadl2.ModalPropertyValue
import org.osate.aadl2.Mode
import org.osate.aadl2.RecordValue
import org.osate.aadl2.instance.ComponentInstance
import org.osate.aadl2.instance.InstanceReferenceValue
import org.osate.aadl2.instance.ModeInstance
import org.osate.aadl2.instance.SystemInstance
import org.osate.aadl2.instance.SystemOperationMode
import fr.mem4csd.osatedim.deltatrace.aaxl2aaxl.Aaxl2AaxlTraceSpec

class DIMTransformationDeltaOutplace {

    /* Transformation-related extensions */
    extension EventDrivenTransformation transformation
    
    /* Transformation rule-related extensions */
    extension EventDrivenTransformationRuleFactory = new EventDrivenTransformationRuleFactory
    extension IModelManipulations manipulation
	
	/* VIATRA Query Pattern group */
	extension DIMQueriesOutplace dim_queries = DIMQueriesOutplace.instance

	/* EMF metamodels */
	extension Aadl2Package aadl2Package = Aadl2Package::eINSTANCE

	
    ViatraQueryEngine engine
    protected Resource resource
//    Aaxl2AaxlTraceSpec aaxl2aaxl
	
	var initialized = false;

	def initialize(
		Aaxl2AaxlTraceSpec aaxl2aaxl,
		ViatraQueryEngine engine
	) {
		if (!initialized) {
//			this.aaxl2aaxl = aaxl2aaxl
			this.engine = engine
			prepare(engine)
			manipulation = new SimpleModelManipulations(engine)
			createTransformation
			initialized = true
		}
	}
	
 	private def createTransformation() {
		val fixedPriorityResolver = new InvertedDisappearancePriorityConflictResolver
//		fixedPriorityResolver.setPriority(componentattach2componentimp.ruleSpecification, 1)
//		fixedPriorityResolver.setPriority(componentattach2componenttype.ruleSpecification, 1)
//		fixedPriorityResolver.setPriority(featureattach2component.ruleSpecification, 2)
//		fixedPriorityResolver.setPriority(connectionattach2component.ruleSpecification, 3)
		fixedPriorityResolver.setPriority(propertyattach2component.ruleSpecification, 4)
//		fixedPriorityResolver.setPriority(propertyvalattach2feature.ruleSpecification, 5)
//		fixedPriorityResolver.setPriority(propertyrefattach2feature.ruleSpecification, 5)
//		fixedPriorityResolver.setPriority(propertyvalattach2connection.ruleSpecification, 6)
//		fixedPriorityResolver.setPriority(propertyrefattach2connection.ruleSpecification, 6)
		
		val builder = EventDrivenTransformation.forEngine(engine).setConflictResolver(fixedPriorityResolver)
//		.addRule(componentattach2componentimp)
//		.addRule(componentattach2componenttype)
//		.addRule(featureattach2component)
//		.addRule(connectionattach2component)
		.addRule(propertyattach2component)
//		.addRule(propertyvalattach2feature)
//		.addRule(propertyrefattach2feature)
//		.addRule(propertyvalattach2connection)
//		.addRule(propertyrefattach2connection)

		transformation = builder.build
	}
    
    def execute() {
        transformation.executionSchema.startUnscheduledExecution
    }
    
    // Dispose model transformation
    def dispose() {
        if (transformation !== null) {
            transformation.executionSchema.dispose
            transformation = null
        }
        return
    }
    
	////////////////////////////////
	//OUT-OF-PLACE TRANSFORMATIONS//
	////////////////////////////////

	val propertyattach2component = createRule(Find_PropertyAttach2Component.instance)
    .action(CRUDActivationStateEnum.CREATED) [
		if (PropertyTypeUtils.isSimpleRefProperty(propinst.property.propertyType)) {
			var prop2add = compinst.subcomponent.createChild(namedElement_OwnedPropertyAssociation, propertyAssociation)
			prop2add.set(propertyAssociation_Property,propinst.property)
			for (ModalPropertyValue modalprop : propinst.ownedValues) {
				val modprop2add = prop2add.createChild(propertyAssociation_OwnedValue, modalPropertyValue) 
				if (!(modalprop.inModes.isEmpty())) {
					modprop2add.add(modalElement_InMode, ((modalprop.inModes.get(0) as SystemOperationMode).currentModes.get(0) as ModeInstance).mode)
				}
				modprop2add.set(modalPropertyValue_OwnedValue, modalprop.ownedValue)
			}
			println("DIM: Simple Value Property "+propinst.property.name+" attached to Object "+compinst.name)
		} else {
			val compinst2att = TraceUtils.getLeftInstanceObject(InstanceHierarchyUtils.getLCPCIForListRefProperty(propinst),trace) as ComponentInstance
			if (compinst2att.subcomponent === null) {
				val prop2add = (compinst2att as SystemInstance).componentImplementation.createChild(namedElement_OwnedPropertyAssociation, propertyAssociation)			
				// PropertyAssociation Property
				prop2add.set(propertyAssociation_Property,propinst.property)
				// PropertyAssociation AppliesTo
				val contelem = prop2add.createChild(propertyAssociation_AppliesTo, containedNamedElement)
				val firstpath2add = contelem.createChild(containedNamedElement_Path, containmentPathElement)
					firstpath2add.set(containmentPathElement_NamedElement,InstanceHierarchyUtils.getContainmentPath(compinst2att,compinst).get(0).subcomponent)	
					var path2add = firstpath2add
				for (ComponentInstance pathcompinst : InstanceHierarchyUtils.getContainmentPath(compinst2att,compinst)) {
					if (InstanceHierarchyUtils.getContainmentPath(compinst2att,compinst).indexOf(pathcompinst) == 0) {
					} else {
						path2add = path2add.createChild(containmentPathElement_Path, containmentPathElement)
						path2add.set(containmentPathElement_NamedElement,pathcompinst)
					}
				}
				// PropertyAssociation  Mode
				for (ModalPropertyValue modalprop : propinst.ownedValues) {
					val modprop2add = prop2add.createChild(propertyAssociation_OwnedValue, modalPropertyValue) 
			
					if (!(modalprop.inModes.isEmpty())) {
						for (Mode som : modalprop.inModes) {
						modprop2add.add(modalElement_InMode, ((som as SystemOperationMode).currentModes.get(0) as ModeInstance).mode)
						}
					}
				// PropertyAssociation Value
					val listval2add = modprop2add.createChild(modalPropertyValue_OwnedValue, listValue)
					for (recordval : (modalprop.ownedValue as ListValue).ownedListElements) {
						val recordval2add = listval2add.createChild(listValue_OwnedListElement, recordValue)
						for (basicpropassoc : (recordval as RecordValue).ownedFieldValues) {
							val bpa2add = recordval2add.createChild(recordValue_OwnedFieldValue,basicPropertyAssociation)
							bpa2add.set(basicPropertyAssociation_Property,basicpropassoc.property)
							if (basicpropassoc.ownedValue instanceof InstanceReferenceValue) {
								val val2add = bpa2add.createChild(basicPropertyAssociation_OwnedValue, referenceValue)
								var firstrefpath2add = val2add.createChild(containedNamedElement_Path, containmentPathElement)
								val refcompinst = (TraceUtils.getLeftInstanceObject(((basicpropassoc.ownedValue as InstanceReferenceValue).referencedInstanceObject as ComponentInstance),trace) as ComponentInstance)
								firstrefpath2add.set(containmentPathElement_NamedElement,InstanceHierarchyUtils.getContainmentPath(compinst2att,refcompinst).get(0).subcomponent)	
								var refpath2add = firstrefpath2add
								for (ComponentInstance pathcompinst : InstanceHierarchyUtils.getContainmentPath(compinst2att,refcompinst)) {
									if (InstanceHierarchyUtils.getContainmentPath(compinst2att,refcompinst).indexOf(pathcompinst) == 0) {
									} else {
										refpath2add = refpath2add.createChild(containmentPathElement_Path, containmentPathElement)
										refpath2add.set(containmentPathElement_NamedElement,pathcompinst)
									}
								}
							} else {
								bpa2add.set(basicPropertyAssociation_OwnedValue,basicpropassoc.ownedValue)	
							}
						}
					}
				}
			} else {
				val prop2add = (compinst2att as SystemInstance).subcomponent.createChild(namedElement_OwnedPropertyAssociation, propertyAssociation)			
				// PropertyAssociation Property
				prop2add.set(propertyAssociation_Property,propinst.property)
				// PropertyAssociation AppliesTo
				val contelem = prop2add.createChild(propertyAssociation_AppliesTo, containedNamedElement)
				val firstpath2add = contelem.createChild(containedNamedElement_Path, containmentPathElement)
					firstpath2add.set(containmentPathElement_NamedElement,InstanceHierarchyUtils.getContainmentPath(compinst2att,compinst).get(0).subcomponent)	
					var path2add = firstpath2add
				for (ComponentInstance pathcompinst : InstanceHierarchyUtils.getContainmentPath(compinst2att,compinst)) {
					if (InstanceHierarchyUtils.getContainmentPath(compinst2att,compinst).indexOf(pathcompinst) == 0) {
					} else {
						path2add = path2add.createChild(containmentPathElement_Path, containmentPathElement)
						path2add.set(containmentPathElement_NamedElement,pathcompinst)
					}
				}
				// PropertyAssociation  Mode
				for (ModalPropertyValue modalprop : propinst.ownedValues) {
					val modprop2add = prop2add.createChild(propertyAssociation_OwnedValue, modalPropertyValue) 
			
					if (!(modalprop.inModes.isEmpty())) {
						for (Mode som : modalprop.inModes) {
						modprop2add.add(modalElement_InMode, ((som as SystemOperationMode).currentModes.get(0) as ModeInstance).mode)
						}
					}
				// PropertyAssociation Value
					val listval2add = modprop2add.createChild(modalPropertyValue_OwnedValue, listValue)
					for (recordval : (modalprop.ownedValue as ListValue).ownedListElements) {
						val recordval2add = listval2add.createChild(listValue_OwnedListElement, recordValue)
						for (basicpropassoc : (recordval as RecordValue).ownedFieldValues) {
							val bpa2add = recordval2add.createChild(recordValue_OwnedFieldValue,basicPropertyAssociation)
							bpa2add.set(basicPropertyAssociation_Property,basicpropassoc.property)
							if (basicpropassoc.ownedValue instanceof InstanceReferenceValue) {
								val val2add = bpa2add.createChild(basicPropertyAssociation_OwnedValue, referenceValue)
								var firstrefpath2add = val2add.createChild(containedNamedElement_Path, containmentPathElement)
								val refcompinst = (TraceUtils.getLeftInstanceObject(((basicpropassoc.ownedValue as InstanceReferenceValue).referencedInstanceObject as ComponentInstance),trace) as ComponentInstance)
								firstrefpath2add.set(containmentPathElement_NamedElement,InstanceHierarchyUtils.getContainmentPath(compinst2att,refcompinst).get(0).subcomponent)	
								var refpath2add = firstrefpath2add
								for (ComponentInstance pathcompinst : InstanceHierarchyUtils.getContainmentPath(compinst2att,refcompinst)) {
									if (InstanceHierarchyUtils.getContainmentPath(compinst2att,refcompinst).indexOf(pathcompinst) == 0) {
									} else {
										refpath2add = refpath2add.createChild(containmentPathElement_Path, containmentPathElement)
										refpath2add.set(containmentPathElement_NamedElement,pathcompinst)
									}
								}
							} else {
								bpa2add.set(basicPropertyAssociation_OwnedValue,basicpropassoc.ownedValue)	
							}
						}
					}
				}
			}
		println("DIM: Complex Reference Property "+propinst.property.name+" attached to Object "+compinst.name)
		} 
    ].action(CRUDActivationStateEnum.UPDATED) [	
    ].action(CRUDActivationStateEnum.DELETED) [
    ].addLifeCycle(Lifecycles.getDefault(true, true)).build
}
