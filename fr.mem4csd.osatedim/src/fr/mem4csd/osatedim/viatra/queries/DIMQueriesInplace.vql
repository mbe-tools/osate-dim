package fr.mem4csd.osatedim.viatra.queries

import "http://aadl.info/AADL/2.0/instance"
import "http://aadl.info/AADL/2.0"

// INPLACE TRANSFORMATION QUERY PATTERNS
// The condition that the declarative element/s of the parent and referenced instance element/s has to be defined is unnecessary
// except for component instances. This is controlled by the priority setting of different model manipulation functions. The 
// condition in the query is only added as a second safe-guard.

// Find an instance object, used to clean up aaxl2aadl traces
pattern findInstanceObject(objinst : InstanceObject)
{
	InstanceObject(objinst);
}

pattern findComponentInstance(compinst : ComponentInstance) 
{
	ComponentInstance(compinst);
}

// Check for reused classifiers
pattern findReusedClassifier(compinst1 : ComponentInstance, compclass : ComponentClassifier, compinst2 : ComponentInstance) 
{
	ComponentInstance.classifier(compinst1, compclass);
	ComponentInstance.classifier(compinst2, compclass);
	compinst1 != compinst2;
}

// Find a component instance which is not part of the system instance tree
pattern findExtraComponent(compinst : ComponentInstance)
{
	neg ComponentInstance.componentInstance(_,compinst);
	neg SystemInstance(compinst);
}

// Find the root system instance of the instance model
pattern findSystem(systeminst : SystemInstance)
{
	neg ComponentInstance.componentInstance(_,systeminst);
}

// Find a subcomponent with a component that has a declared classifier
pattern findSubcomponent(subcompinst : ComponentInstance, compinst : ComponentInstance)
{
	ComponentInstance.componentInstance(compinst,subcompinst);
	ComponentInstance.classifier(compinst,_);
} or {
	ComponentInstance.componentInstance(compinst,subcompinst);
	SystemInstance.componentImplementation(compinst,_);
}

// Find feature attached to component that has a declared classifier
pattern findFeature(featinst : FeatureInstance)
{
	ComponentInstance.featureInstance(compinst,featinst);
	ComponentInstance.classifier(compinst,_);
}

// Find connection with source and destination features/components that have declared classifiers
pattern findConnection(conninst : ConnectionInstance)
{
	ConnectionInstance.destination(conninst,dstfeat);
	FeatureInstance.feature(dstfeat,_);
	ConnectionInstance.source(conninst,srcfeat);
	FeatureInstance.feature(srcfeat,_);
} or {
	ConnectionInstance.destination(conninst,dstcomp);
	ComponentInstance.subcomponent(dstcomp,_);
	ConnectionInstance.source(conninst,srccomp);
	ComponentInstance.subcomponent(srccomp,_);
} or {
	ConnectionInstance.destination(conninst,dstcomp);
	ComponentInstance.subcomponent(dstcomp,_);
	ConnectionInstance.source(conninst,srcfeat);
	FeatureInstance.feature(srcfeat,_);
} or {
	ConnectionInstance.source(conninst,srccomp);
	ComponentInstance.subcomponent(srccomp,_);
	ConnectionInstance.destination(conninst,dstfeat);
	FeatureInstance.feature(dstfeat,_);
} or {
	ConnectionInstance.source(conninst,srctrans);
	ConnectionInstance.destination(conninst,dsttrans);
	ModeTransitionInstance.modeTransition(srctrans,_);
	ModeTransitionInstance.modeTransition(dsttrans,_);
}

// Find connection references of a connection with source and destination components that have declared classifiers
pattern findConnectionReference(connref : ConnectionReference)
{
	ConnectionInstance.connectionReference(conninst, connref);
	find findConnection(conninst);
	ConnectionInstance.kind(conninst,_);
	ConnectionReference.source(connref,_);
	ConnectionReference.destination(connref,_);
}

// Find mode instances
pattern findMode(modeinst: ModeInstance)
{
	ComponentInstance.modeInstance(_,modeinst);
	neg ModeInstance.parent(modeinst,_);
}

pattern findDerivedMode(modeinst: ModeInstance)
{
	ComponentInstance.modeInstance(_,modeinst);
	ModeInstance.parent(modeinst,_);
}

// Find mode transition instances
pattern findModeTransition(modetransinst: ModeTransitionInstance)
{
	ComponentInstance.modeTransitionInstance(_,modetransinst);
}

// Find property attached to an instance object
pattern findProperty(propinst : PropertyAssociationInstance)
{
	InstanceObject.ownedPropertyAssociation(_,propinst);
}

// Find modal property value attached to an property association instance
pattern findModalProperty(modpropinst : ModalPropertyValue)
{
	PropertyAssociationInstance.ownedValue(propinst,modpropinst);
	PropertyAssociationInstance.propertyAssociation(propinst,_);
}

// Find property value (this pattern matches with property values on the declarative side as well)
// Appropriate measures are added within the transformation rules to account for this, and only react to states of property
// values on the instance side
pattern findPropertyValue(propvalinst : PropertyValue) {
	neg RecordValue(propvalinst);
	PropertyValue(propvalinst);
}

pattern findPropertyReferencingElements(element: Element) {
	PropertyAssociationInstance(element);
} or {
	BasicPropertyAssociation(element);
}

// QUERIES FOR EXCEPTION HANDLING

pattern findParameterConnection(parconn : ParameterConnection, feat : Feature) {
	AbstractImplementation.ownedParameterConnection(_,parconn);
	ParameterConnection.source.connectionEnd(parconn, feat); } or {
	BusImplementation.ownedParameterConnection(_,parconn); 
	ParameterConnection.source.connectionEnd(parconn, feat); } or {
	DataImplementation.ownedParameterConnection(_,parconn); 
	ParameterConnection.source.connectionEnd(parconn, feat); } or {
	DeviceImplementation.ownedParameterConnection(_,parconn);
	ParameterConnection.source.connectionEnd(parconn, feat); } or {
	MemoryImplementation.ownedParameterConnection(_,parconn);
	ParameterConnection.source.connectionEnd(parconn, feat); } or {
	ProcessImplementation.ownedParameterConnection(_,parconn);
	ParameterConnection.source.connectionEnd(parconn, feat); } or {
	ProcessorImplementation.ownedParameterConnection(_,parconn);
	ParameterConnection.source.connectionEnd(parconn, feat); } or {
	SubprogramImplementation.ownedParameterConnection(_,parconn);
	ParameterConnection.source.connectionEnd(parconn, feat); } or {
	SubprogramGroupImplementation.ownedParameterConnection(_,parconn);
	ParameterConnection.source.connectionEnd(parconn, feat); } or {
	SystemImplementation.ownedParameterConnection(_,parconn);
	ParameterConnection.source.connectionEnd(parconn, feat); } or {
	ThreadImplementation.ownedParameterConnection(_,parconn);
	ParameterConnection.source.connectionEnd(parconn, feat); } or {
	ThreadGroupImplementation.ownedParameterConnection(_,parconn);
	ParameterConnection.source.connectionEnd(parconn, feat); } or {
	VirtualBusImplementation.ownedParameterConnection(_,parconn);
	ParameterConnection.source.connectionEnd(parconn, feat); } or {
	VirtualProcessorImplementation.ownedParameterConnection(_,parconn);
	ParameterConnection.source.connectionEnd(parconn, feat); } or {
	AbstractImplementation.ownedParameterConnection(_,parconn);
	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
	BusImplementation.ownedParameterConnection(_,parconn); 
	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
	DataImplementation.ownedParameterConnection(_,parconn); 
	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
	DeviceImplementation.ownedParameterConnection(_,parconn);
	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
	MemoryImplementation.ownedParameterConnection(_,parconn);
	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
	ProcessImplementation.ownedParameterConnection(_,parconn);
	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
	ProcessorImplementation.ownedParameterConnection(_,parconn);
	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
	SubprogramImplementation.ownedParameterConnection(_,parconn);
	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
	SubprogramGroupImplementation.ownedParameterConnection(_,parconn);
	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
	SystemImplementation.ownedParameterConnection(_,parconn);
	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
	ThreadImplementation.ownedParameterConnection(_,parconn);
	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
	ThreadGroupImplementation.ownedParameterConnection(_,parconn);
	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
	VirtualBusImplementation.ownedParameterConnection(_,parconn);
	ParameterConnection.destination.connectionEnd(parconn, feat); } or {
	VirtualProcessorImplementation.ownedParameterConnection(_,parconn);
	ParameterConnection.destination.connectionEnd(parconn, feat); 
} 
