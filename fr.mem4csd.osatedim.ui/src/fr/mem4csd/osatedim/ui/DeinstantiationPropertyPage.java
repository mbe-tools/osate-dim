package fr.mem4csd.osatedim.ui;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.resources.ProjectScope;
import org.eclipse.core.runtime.Adapters;
import org.eclipse.core.runtime.Assert;
import org.eclipse.core.runtime.preferences.IScopeContext;
import org.eclipse.jface.preference.IPreferenceNode;
import org.eclipse.jface.preference.IPreferencePage;
import org.eclipse.jface.preference.PreferenceDialog;
import org.eclipse.jface.preference.PreferenceManager;
import org.eclipse.jface.preference.PreferenceNode;
import org.eclipse.swt.SWT;
import org.eclipse.swt.custom.BusyIndicator;
import org.eclipse.swt.events.SelectionAdapter;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.layout.GridLayout;
import org.eclipse.swt.widgets.Button;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Control;
import org.eclipse.swt.widgets.Label;
import org.eclipse.ui.dialogs.PropertyPage;
import org.osate.ui.internal.preferences.InstantiationPreferencePage;
import org.osgi.service.prefs.BackingStoreException;
import org.osgi.service.prefs.Preferences;

public class DeinstantiationPropertyPage extends PropertyPage {
	
	private Button inheritPropertyButton;
	private Button inheritModeButton;
	private Button createInterFeaturesButton;
	private Button modifyReusedButton;
	private Button addClassifierPropertyButton;
	
	private Preferences preferences;
	
//	private Button noClassifierExtensionButton, requiredClassifierExtensionButton, alwaysClassifierExtensionButton;
	
	Button useWorkspaceSettingsButton;
	Button useProjectSettingsButton;
	Button configureButton;
	
	@Override
	protected Control createContents(Composite parent) {
		// Get the project
		IProject project = Adapters.adapt(getElement(), IProject.class);
		if (project == null) {
			IResource resource = Adapters.adapt(getElement(), IResource.class);
			Assert.isNotNull(resource, "unable to adapt element to a project");
			project = resource.getProject();
		}

		// Get the project's preferences
		final IScopeContext context = new ProjectScope(project);
		preferences = context.getNode(DIMUIPlugin.PREFS_QUALIFIER);

		// Create the overall composite to fill up
		final Composite composite = new Composite(parent, SWT.NONE);
		composite.setLayout(new GridLayout(2, false));

		// Create the buttons for choosing between workspace and project preferences
		final Composite selectionGroup = new Composite(composite, SWT.NONE);
		final GridLayout sgLayout = new GridLayout(2, false);
		sgLayout.marginHeight = 0;
		sgLayout.marginWidth = 0;
		selectionGroup.setLayout(sgLayout);
		selectionGroup.setLayoutData(new GridData(SWT.FILL, SWT.TOP, true, false, 2, 1));

		final Composite radioGroup = new Composite(selectionGroup, SWT.NONE);
		radioGroup.setLayout(new GridLayout());
		radioGroup.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));

		useWorkspaceSettingsButton = new Button(radioGroup, SWT.RADIO);
		useWorkspaceSettingsButton.setText("Use workspace settings");

		useProjectSettingsButton = new Button(radioGroup, SWT.RADIO);
		useProjectSettingsButton.setText("Use project settings");

		configureButton = new Button(selectionGroup, SWT.PUSH);
		configureButton.setText("Configure Workspace Settings ...");
				
		// Create the actual property fields that we want to edit
		inheritPropertyButton = new Button(composite, SWT.CHECK);
		inheritPropertyButton.setSelection(getInheritProperty());
		inheritPropertyButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, true, false, 1, 1));
				
		final Label label0 = new Label(composite, SWT.NONE);
		label0.setText("AUTO-ADD INHERITED PROPERTIES to new instance elements");
		label0.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));

		inheritModeButton = new Button(composite, SWT.CHECK);
		inheritModeButton.setSelection(getInheritMode());
		inheritModeButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, true, false, 1, 1));
				
		final Label label1 = new Label(composite, SWT.NONE);
		label1.setText("AUTO-ADD INHERITED MODES to new instance elements");
		label1.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		
		createInterFeaturesButton = new Button(composite, SWT.CHECK);
		createInterFeaturesButton.setSelection(getModifyReused());
		createInterFeaturesButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, true, false, 1, 1));
		
		final Label label4 = new Label(composite, SWT.NONE);
		label4.setText("AUTO-CREATE INTERMEDIATE FEATURES for new semantic connections");
		label4.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		
		modifyReusedButton = new Button(composite, SWT.CHECK);
		modifyReusedButton.setSelection(getModifyReused());
		modifyReusedButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, true, false, 1, 1));
				
		final Label label2 = new Label(composite, SWT.NONE);
		label2.setText("MODIFY multiply-used classifiers");
		label2.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
		
		addClassifierPropertyButton = new Button(composite, SWT.CHECK);
		addClassifierPropertyButton.setSelection(getAddClassifierProperty());
		addClassifierPropertyButton.setLayoutData(new GridData(SWT.BEGINNING, SWT.CENTER, true, false, 1, 1));
				
		final Label label3 = new Label(composite, SWT.NONE);
		label3.setText("ADD DIM_Classifier Property to newly created classifiers");
		label3.setLayoutData(new GridData(SWT.FILL, SWT.CENTER, false, false, 1, 1));
				
//		final Composite radioGroup1 = new Composite(composite, SWT.NONE);
//		radioGroup1.setLayout(new GridLayout());
//		radioGroup1.setLayoutData(new GridData(GridData.FILL_HORIZONTAL));
//
//		noClassifierExtensionButton = new Button(radioGroup1, SWT.RADIO);
//		noClassifierExtensionButton.setText("NONE");
//
//		requiredClassifierExtensionButton = new Button(radioGroup1, SWT.RADIO);
//		requiredClassifierExtensionButton.setText("ONLY REQUIRED (Structural changes)");
//				
//		alwaysClassifierExtensionButton = new Button(radioGroup1, SWT.RADIO);
//		alwaysClassifierExtensionButton.setText("ALWAYS");
//		if (getExtendClassifier() == "none") {
//			noClassifierExtensionButton.setSelection(true);
//		} else if (getExtendClassifier() == "required") {
//			requiredClassifierExtensionButton.setSelection(true);
//		} else {
//			alwaysClassifierExtensionButton.setSelection(true);
//		}
		
		// Configure property fields enabled status
		if (useWorkspacePreferences(project)) {
			useWorkspaceSettingsButton.setSelection(true);
			useProjectSettingsButton.setSelection(false);
			configureButton.setEnabled(true);
			inheritPropertyButton.setEnabled(false);
			inheritModeButton.setEnabled(false);
			createInterFeaturesButton.setEnabled(false);
			modifyReusedButton.setEnabled(false);
			addClassifierPropertyButton.setEnabled(false);
//			radioGroup1.setEnabled(false);
		} else {
			useWorkspaceSettingsButton.setSelection(false);
			useProjectSettingsButton.setSelection(true);
			configureButton.setEnabled(false);
			inheritPropertyButton.setEnabled(true);
			inheritModeButton.setEnabled(true);
			createInterFeaturesButton.setEnabled(true);
			modifyReusedButton.setEnabled(true);
			addClassifierPropertyButton.setEnabled(true);
//			radioGroup1.setEnabled(true);
		}
	
		// Add listeners
		useWorkspaceSettingsButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				configureButton.setEnabled(true);
				inheritPropertyButton.setEnabled(false);
				inheritModeButton.setEnabled(false);
				modifyReusedButton.setEnabled(false);
				addClassifierPropertyButton.setEnabled(false);
//				radioGroup1.setEnabled(false);
			}
		});
			
		useProjectSettingsButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				configureButton.setEnabled(false);
				inheritPropertyButton.setEnabled(true);
				inheritModeButton.setEnabled(true);
				modifyReusedButton.setEnabled(true);
				addClassifierPropertyButton.setEnabled(true);
//				radioGroup1.setEnabled(true);
			}
		});
	
		configureButton.addSelectionListener(new SelectionAdapter() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				configureWorkspaceSettings();
			}
		});

		// return the whole thing
		return composite;
	}

	private boolean getInheritProperty() {
		DIMUIPlugin.getInstance();
		return preferences.getBoolean(DIMUIPlugin.PREF_INHERIT_PROPERTY, DIMUIPlugin.INHERIT_PROPERTY_DEFAULT);
	}
	
	private boolean getInheritMode() {
		DIMUIPlugin.getInstance();
		return preferences.getBoolean(DIMUIPlugin.PREF_INHERIT_MODE, DIMUIPlugin.INHERIT_MODE_DEFAULT);
	}
	
//	private boolean getCreateInterFeatures() {
//		DIMUIPlugin.getInstance();
//		return preferences.getBoolean(DIMUIPlugin.PREF_CREATE_INTER_FEATURES, DIMUIPlugin.CREATE_INTER_FEATURES_DEFAULT);
//	}
	
	private boolean getModifyReused() {
		DIMUIPlugin.getInstance();
		return preferences.getBoolean(DIMUIPlugin.PREF_MODIFY_REUSED, DIMUIPlugin.MODIFY_REUSED_DEFAULT);
	}
	
	private boolean getAddClassifierProperty() {
		DIMUIPlugin.getInstance();
		return preferences.getBoolean(DIMUIPlugin.PREF_ADD_CLASSIFIER_PROPERTY, DIMUIPlugin.ADD_CLASSIFIER_PROPERTY_DEFAULT);
	}
	
//	private String getExtendClassifier() {
//		DIMUIPlugin.getInstance();
//		return preferences.get(DIMUIPlugin.PREF_EXTEND_CLASSIFIER, DIMUIPlugin.EXTEND_CLASSIFIER_DEFAULT);
//	}
	
	private boolean useWorkspacePreferences(IProject project) {
		return preferences.getBoolean(DIMUIPlugin.PREF_USE_WORKSPACE, true);
	}

	@Override
	public boolean performOk() {
		preferences.putBoolean(DIMUIPlugin.PREF_USE_WORKSPACE, useWorkspaceSettingsButton.getSelection());
		preferences.putBoolean(DIMUIPlugin.PREF_INHERIT_PROPERTY, inheritPropertyButton.getSelection());
		preferences.putBoolean(DIMUIPlugin.PREF_INHERIT_MODE, inheritModeButton.getSelection());
		preferences.putBoolean(DIMUIPlugin.PREF_CREATE_INTER_FEATURES, createInterFeaturesButton.getSelection());
		preferences.putBoolean(DIMUIPlugin.PREF_MODIFY_REUSED, modifyReusedButton.getSelection());
		preferences.putBoolean(DIMUIPlugin.PREF_ADD_CLASSIFIER_PROPERTY, addClassifierPropertyButton.getSelection());
//		if (noClassifierExtensionButton.getSelection()) {
//			preferences.put(DIMUIPlugin.PREF_EXTEND_CLASSIFIER, ClassifierExtensionPreference.NONE_EXTENSION.getLiteral());
//		} else if (requiredClassifierExtensionButton.getSelection()) {
//			preferences.put(DIMUIPlugin.PREF_EXTEND_CLASSIFIER, ClassifierExtensionPreference.REQUIRED_EXTENSION.getLiteral());
//		} else {
//			preferences.put(DIMUIPlugin.PREF_EXTEND_CLASSIFIER, ClassifierExtensionPreference.ALWAYS_EXTENSION.getLiteral());
//		}
		try {
			preferences.flush();
		} catch (final BackingStoreException e) {
			e.printStackTrace();
		}
		return true;
	}

	@Override
	protected void performDefaults() {
		useWorkspaceSettingsButton.setSelection(true);
		useProjectSettingsButton.setSelection(false);
		configureButton.setEnabled(true);
		inheritPropertyButton.setSelection(DIMUIPlugin.INHERIT_PROPERTY_DEFAULT);
		inheritPropertyButton.setEnabled(false);
		inheritModeButton.setSelection(DIMUIPlugin.INHERIT_MODE_DEFAULT);
		inheritModeButton.setEnabled(false);
		createInterFeaturesButton.setSelection(DIMUIPlugin.CREATE_INTER_FEATURES_DEFAULT);
		createInterFeaturesButton.setEnabled(false);
		modifyReusedButton.setSelection(DIMUIPlugin.MODIFY_REUSED_DEFAULT);
		modifyReusedButton.setEnabled(false);
		addClassifierPropertyButton.setSelection(DIMUIPlugin.ADD_CLASSIFIER_PROPERTY_DEFAULT);
		addClassifierPropertyButton.setEnabled(false);
//		noClassifierExtensionButton.setSelection(false);
//		requiredClassifierExtensionButton.setSelection(true);
//		alwaysClassifierExtensionButton.setSelection(false);
//		noClassifierExtensionButton.setEnabled(false);
//		requiredClassifierExtensionButton.setEnabled(false);
//		alwaysClassifierExtensionButton.setEnabled(false);
		// Why? Because the default implementation does this
		updateApplyButton();
	}
	
	private void configureWorkspaceSettings() {
		final IPreferencePage page = new InstantiationPreferencePage();
		page.setTitle(getTitle());
		final IPreferenceNode targetNode = new PreferenceNode("fr.mem4csd.osatedim.ui.preferencePage", page);
		final PreferenceManager manager = new PreferenceManager();
		manager.addToRoot(targetNode);
		final PreferenceDialog dialog = new PreferenceDialog(getControl().getShell(), manager);
		BusyIndicator.showWhile(getControl().getDisplay(), () -> {
			dialog.create();
			dialog.setMessage(targetNode.getLabelText());
			dialog.open();
		});
	}
}
