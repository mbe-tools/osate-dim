package fr.mem4csd.osatedim.ui;

import java.util.ArrayList;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ResourcesPlugin;
import org.eclipse.jface.preference.BooleanFieldEditor;
import org.eclipse.jface.preference.FieldEditorPreferencePage;
//import org.eclipse.jface.preference.RadioGroupFieldEditor;
import org.eclipse.jface.window.Window;
import org.eclipse.swt.SWT;
import org.eclipse.swt.events.SelectionEvent;
import org.eclipse.swt.events.SelectionListener;
import org.eclipse.swt.layout.GridData;
import org.eclipse.swt.widgets.Composite;
import org.eclipse.swt.widgets.Label;
import org.eclipse.swt.widgets.Link;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchPreferencePage;
import org.eclipse.ui.dialogs.PreferencesUtil;
import org.osate.core.AadlNature;
import org.osate.ui.dialogs.ProjectSelectionDialog;
import fr.mem4csd.osatedim.ui.utils.SpacerFieldEditor;
//import fr.mem4csd.osatedim.viatra.preference.ClassifierExtensionPreference;

public class DeinstantiationPreferencePage extends FieldEditorPreferencePage implements IWorkbenchPreferencePage {
	private static final String LABEL = "Configure Project Specific Settings...";
	private static final String TITLE = "Project Specific Configuration";
	private static final String MESSAGE = "Select the project to configure:";
	private static final String ID = "fr.mem4csd.osatedim.ui.propertyPage";
	private static final String[] ID_LIST = { ID };
	private static final Object DUMMY_DATA = new Object();

	private Link changeWorkspaceSettings;

	public DeinstantiationPreferencePage() {
		super(GRID);
		setPreferenceStore(DIMUIPlugin.getInstance().getPreferenceStore());
		setDescription("OSATE-DIM: Deinstantiation Preferences");
	}

	@Override
	protected Label createDescriptionLabel(final Composite parent) {
		changeWorkspaceSettings = createLink(parent, LABEL);
		changeWorkspaceSettings.setLayoutData(new GridData(SWT.END, SWT.CENTER, true, false));
		return super.createDescriptionLabel(parent);
	}

	private Link createLink(final Composite composite, final String text) {
		Link link = new Link(composite, SWT.NONE);
		link.setFont(composite.getFont());
		link.setText("<A>" + text + "</A>");
		link.addSelectionListener(new SelectionListener() {
			@Override
			public void widgetSelected(SelectionEvent e) {
				doLinkActivated((Link) e.widget);
			}

			@Override
			public void widgetDefaultSelected(SelectionEvent e) {
				doLinkActivated((Link) e.widget);
			}
		});
		return link;
	}

	/**
	 * Create the field editors.
	 */
	@Override
	public void createFieldEditors() {
		SpacerFieldEditor spacer0 = new SpacerFieldEditor(getFieldEditorParent());
		addField(spacer0);
		
		final BooleanFieldEditor propertyInheritanceUtilityField = new BooleanFieldEditor(
			DIMUIPlugin.INHERIT_PROPERTY_PROPERTY,"AUTO-ADD INHERITED PROPERTIES to new instance elements", getFieldEditorParent());
		addField(propertyInheritanceUtilityField);
		
		SpacerFieldEditor spacer1 = new SpacerFieldEditor(getFieldEditorParent());
		addField(spacer1);
		
		final BooleanFieldEditor modeInheritanceUtilityField = new BooleanFieldEditor(
			DIMUIPlugin.INHERIT_MODE_PROPERTY,"AUTO-ADD INHERITED MODES to new instance elements", getFieldEditorParent());
		addField(modeInheritanceUtilityField);
		
		SpacerFieldEditor spacer4 = new SpacerFieldEditor(getFieldEditorParent());
		addField(spacer4);
		
		final BooleanFieldEditor createIntermediateFeaturesField = new BooleanFieldEditor(
			DIMUIPlugin.CREATE_INTER_FEATURES_PROPERTY,"AUTO-CREATE INTERMEDIATE FEATURES for new semantic connections", getFieldEditorParent());
		addField(createIntermediateFeaturesField);
		
		SpacerFieldEditor spacer2 = new SpacerFieldEditor(getFieldEditorParent());
		addField(spacer2);
		
		final BooleanFieldEditor modifyReusedClassifierField = new BooleanFieldEditor(
			DIMUIPlugin.MODIFY_REUSED_PROPERTY,"MODIFY multiply-used classifiers", getFieldEditorParent());
		addField(modifyReusedClassifierField);
		
		SpacerFieldEditor spacer3 = new SpacerFieldEditor(getFieldEditorParent());
		addField(spacer3);
		
		final BooleanFieldEditor addClassifierPropertyField = new BooleanFieldEditor(
			DIMUIPlugin.ADD_CLASSIFIER_PROPERTY_PROPERTY,"ADD DIM_Classifier Property to newly created classifiers", getFieldEditorParent());
		addField(addClassifierPropertyField);
		
//		SpacerFieldEditor spacer4 = new SpacerFieldEditor(getFieldEditorParent());
//		addField(spacer4);
//		
//		final RadioGroupFieldEditor classifierExtensionField = new RadioGroupFieldEditor(
//			DIMUIPlugin.EXTEND_CLASSIFIER_PROPERTY,"Classifier extension preference:",1, 
//			new String[][] {{"NONE",ClassifierExtensionPreference.NONE_EXTENSION.getLiteral()},
//				{"ONLY REQUIRED (Structural changes)",ClassifierExtensionPreference.REQUIRED_EXTENSION.getLiteral()},
//				{"ALWAYS",ClassifierExtensionPreference.ALWAYS_EXTENSION.getLiteral()}},getFieldEditorParent());
//		addField(classifierExtensionField);
	}
	
	@Override
	public void init(final IWorkbench workbench) {
	}

	final void doLinkActivated(final Link link) {
		final ArrayList<IProject> projectsWithSpecifics = new ArrayList<>();
		final IProject[] projects = ResourcesPlugin.getWorkspace().getRoot().getProjects();
		for (IProject project : projects) {
			if (AadlNature.hasNature(project)) {
				projectsWithSpecifics.add(project);
			}
		}

		final ProjectSelectionDialog dialog = new ProjectSelectionDialog(getShell(), projectsWithSpecifics, TITLE, MESSAGE);
		if (dialog.open() == Window.OK) {
			final IProject project = dialog.getSelectedProject();
			PreferencesUtil.createPropertyDialogOn(getShell(), project, ID, ID_LIST, DUMMY_DATA).open();
		}
	}

}
