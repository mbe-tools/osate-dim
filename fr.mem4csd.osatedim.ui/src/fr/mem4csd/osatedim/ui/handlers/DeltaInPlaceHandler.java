package fr.mem4csd.osatedim.ui.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.osate.aadl2.instance.SystemInstance;
import fr.mem4csd.osatedim.ui.DIMStartup;
import fr.mem4csd.osatedim.ui.utils.DeinstantiationUtils;
import fr.mem4csd.osatedim.viatra.transformations.DIMTransformationDeltaInplace;

public class DeltaInPlaceHandler extends AbstractHandler {
	
	private static final DIMUILogger LOGGER = new DIMUILogger();
	
	@Override
	public Object execute(final ExecutionEvent event)
	throws ExecutionException {		
		final IStructuredSelection selection = HandlerUtil.getCurrentStructuredSelection(event);
		final Object selectionObject = selection.getFirstElement();
		final SystemInstance topSystemInst = DeinstantiationUtils.openSelectionTopSystemInstance(selectionObject);
		
		if (topSystemInst != null) {
			final Resource sysRes = topSystemInst.eResource();
			final URI resUri = sysRes.getURI();
			
			if (!DIMStartup.getInstance().isRegistered(resUri)) {
				LOGGER.logInfo("DIM: Initializing incremental in-place deinstantiation for "+ topSystemInst.getName());
				DIMTransformationDeltaInplace transformation = null;
				try {
					transformation = DIMStartup.getInstance().registerTransformation(topSystemInst, LOGGER);
				} catch (IllegalStateException e) {
					 DIMExceptionHandler.handleInplaceException(e, transformation, topSystemInst, event);
				} catch (CoreException e) {
					throw new ExecutionException("Error registering transformation!", e);
				}
			} 
			else {
				LOGGER.logWarning("DIM: Incremental in-place deinstantiation for "+ topSystemInst.getName() + " already initialized!");
			}
		}
		return null;
	}
}