package fr.mem4csd.osatedim.ui.handlers;

import org.eclipse.core.commands.ExecutionException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IWorkbench;
import org.eclipse.ui.IWorkbenchListener;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.PlatformUI;

public class DIMListener implements IPartListener, IWorkbenchListener {
	
	final IWorkbenchPart part;
	final URI resUri;
	
	public DIMListener (IWorkbenchPart part) {
		this.part = part;
		resUri = ( (IEditingDomainProvider) part ).getEditingDomain().getResourceSet().getResources().get(0).getURI();
	}

	@Override
	public void partActivated(IWorkbenchPart part) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void partBroughtToTop(IWorkbenchPart part) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void partClosed(IWorkbenchPart part) {
		if (part == this.part) {
			try {
				StopInPlaceHandler.stopTransformation(resUri);
			} catch (ExecutionException e) {
				e.printStackTrace();
			}
			PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().removePartListener(this);
		}
	}

	@Override
	public void partDeactivated(IWorkbenchPart part) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public void partOpened(IWorkbenchPart part) {
		// TODO Auto-generated method stub
		
	}

	@Override
	public boolean preShutdown(IWorkbench workbench, boolean forced) {
		try {
			StopInPlaceHandler.stopTransformation(resUri);
		} catch (ExecutionException e) {
			e.printStackTrace();
		}
		return true;
	}

	@Override
	public void postShutdown(IWorkbench workbench) {
		// TODO Auto-generated method stub
		
	}
}
