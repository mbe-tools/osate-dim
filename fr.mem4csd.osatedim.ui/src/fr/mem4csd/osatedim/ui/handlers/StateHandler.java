package fr.mem4csd.osatedim.ui.handlers;

import java.io.FileNotFoundException;
import java.io.IOException;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IProject;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import org.eclipse.viatra.query.runtime.exception.ViatraQueryException;
import org.osate.aadl2.AadlPackage;
import org.osate.aadl2.instance.SystemInstance;
import org.osate.aadl2.modelsupport.resources.OsateResourceUtil;
import fr.mem4csd.osatedim.ui.DIMUIPlugin;
import fr.mem4csd.osatedim.ui.utils.DeinstantiationUtils;
import fr.mem4csd.osatedim.utils.PackageUtils;
import fr.mem4csd.osatedim.utils.TransformationUtils;
import fr.mem4csd.osatedim.viatra.transformations.DIMTransformationState;

public class StateHandler extends AbstractHandler {
	
	private final DIMUILogger LOGGER = new DIMUILogger();
	
	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		LOGGER.logInfo("DIM: Preparing state-based deinstantiation");
		final IStructuredSelection selection = HandlerUtil.getCurrentStructuredSelection(event);
		final SystemInstance topSystemInst = DeinstantiationUtils.openSelectionTopSystemInstance( selection.getFirstElement() );
		final ResourceSet resSet =  topSystemInst.eResource().getResourceSet();
		
		final URI selectionURI = topSystemInst.eResource().getURI();
		final AadlPackage aadlPackage = PackageUtils.configureAadlPackage(selectionURI, topSystemInst);
		final URI aadlURI = DeinstantiationUtils.computeDeclarativeUri(selectionURI, topSystemInst);
		final Resource aadlResource = resSet.createResource(aadlURI);
		aadlResource.getContents().add(aadlPackage);
		
		final IProject project = OsateResourceUtil.toIFile(aadlResource.getURI()).getProject();
		
		LOGGER.logInfo("DIM: Starting state-based deinstantiation");
		DIMTransformationState transformation = null;
		try {
			transformation = TransformationUtils.executeStateDIM(resSet, topSystemInst, aadlPackage, aadlResource, DIMUIPlugin.getInstance().getPreferences(project), DeinstantiationUtils.getPluginDIMPropertySet(resSet), LOGGER);
			TransformationUtils.finishStateDIM(transformation);
			LOGGER.logInfo("DIM: Static deinstantiation finished");
		} 
		catch (FileNotFoundException e) {
			DIMExceptionHandler.handleFileNotFoundException(e, transformation, event, topSystemInst);
		} catch (IllegalStateException e) {
			DIMExceptionHandler.handleStateException(e, transformation, event, topSystemInst);
		} catch ( final ViatraQueryException | IOException e ) {
			throw new ExecutionException("Error when creating declarative model!", e);
		} 
		
		return null;
	}
}
