package fr.mem4csd.osatedim.ui.handlers;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.resources.IResource;
import org.eclipse.core.runtime.IProgressMonitor;
import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import org.eclipse.core.runtime.jobs.Job;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.ui.handlers.HandlerUtil;
import org.osate.aadl2.modelsupport.resources.OsateResourceUtil;

import fr.mem4csd.osatedim.viatra.transformations.DIMTransformationDeltaOutplace;

public class DeltaOutPlaceHandler extends AbstractHandler {

	public DeltaOutPlaceHandler() {
		super();
	}

	@Override
	public Object execute(final ExecutionEvent event) throws ExecutionException {
		// Get a COPY of the selection for thread safety
		@SuppressWarnings("unchecked")
		final List<?> selectionAsList = new ArrayList<>(HandlerUtil.getCurrentStructuredSelection(event).toList());
		
		final Job job = new KickoffJob(selectionAsList);
		job.setRule(null); // doesn't use resources
		job.schedule();

		// Supposed to always return null
		return null;
	}

	private final class KickoffJob extends Job {
		private List<?> selectionAsList;
		
		 //should be an ifile

		public KickoffJob(final List<?> selectionAsList) {
			super("Reinstantiation");
			this.selectionAsList = selectionAsList;
		}

		@Override
		protected IStatus run(final IProgressMonitor monitor) {
			final URI SELECTION_URI = OsateResourceUtil.toResourceURI((IResource) selectionAsList.get(0));
			ResourceSet resourceSet = new ResourceSetImpl();
			final Resource aaxl2aaxlResource = resourceSet.getResource(SELECTION_URI, true); 
			
//			ViatraQueryEngine engine = ViatraQueryEngine.on(new EMFScope(resourceSet));
			DIMTransformationDeltaOutplace transformation = new DIMTransformationDeltaOutplace();
//			transformation.initialize(aaxl2aaxlResource, engine);
			transformation.execute();
			transformation.dispose();
			
			try {
				aaxl2aaxlResource.save(null);
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			System.out.println("DIM: State-based DIMTransformation finished");
			
			return Status.OK_STATUS;
		}
	}

}
