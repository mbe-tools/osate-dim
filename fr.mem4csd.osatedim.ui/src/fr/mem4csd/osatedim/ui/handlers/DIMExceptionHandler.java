package fr.mem4csd.osatedim.ui.handlers;

import fr.mem4csd.osatedim.ui.DIMStartup;
import fr.mem4csd.osatedim.viatra.transformations.DIMTransformationDeltaInplace;
import fr.mem4csd.osatedim.viatra.transformations.DIMTransformationState;
import java.io.FileNotFoundException;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.dialogs.MessageDialog;
import org.eclipse.ui.handlers.HandlerUtil;
import org.osate.aadl2.instance.SystemInstance;

public class DIMExceptionHandler {
	
	private static final DIMUILogger LOGGER = new DIMUILogger();
	
	public static void handleStateException(Exception ex, DIMTransformationState transformation, ExecutionEvent event, SystemInstance topSystemInst) {
		ex.printStackTrace();
		String topSystemInstURI = topSystemInst.eResource().getURI().toString();
		MessageDialog.openError(HandlerUtil.getActiveShell(event), "DIM: Error", "An error occured in the synchronization of file "+topSystemInstURI+". The transformation will be disposed. Consult the logs in the console. Please file an appropriate Bug report on the OSATE-DIM GitLab repository, or message dominique.blouin@telecom-paris.fr");
		LOGGER.logError("DIM: An error occured in the synchronization of file "+topSystemInstURI+".");
		if (transformation != null) {
			transformation.dispose();
		}
		LOGGER.logCancel("DIM: Transformation of file "+topSystemInstURI+" disposed.");
}
	
	public static void handleInplaceException(Exception ex, DIMTransformationDeltaInplace transformation, SystemInstance topSystemInst, ExecutionEvent event) {
		ex.printStackTrace();
		URI topSystemInstURI = topSystemInst.eResource().getURI();
		MessageDialog.openError(HandlerUtil.getActiveShell(event), "DIM: Error", "An error occured in the synchronization of file "+topSystemInstURI.toString()+". The transformation will be disposed. Consult the logs in the console. Please file an appropriate Bug report on the OSATE-DIM GitLab repository, or message dominique.blouin@telecom-paris.fr");
		try {
			DIMStartup.getInstance().unregisterTransformation(topSystemInstURI);	
		} catch (CoreException e) {
			LOGGER.logError("DIM: Error encountered in un-registering trasformation. Consult the logs in the console. Please file an appropriate Bug report on the OSATE-DIM GitLab repository, or message dominique.blouin@telecom-paris.fr"); 
		} 
	}

	public static void handleFileNotFoundException(FileNotFoundException ex, DIMTransformationState transformation, ExecutionEvent event, SystemInstance topSystemInst) {
		LOGGER.logError(ex.getMessage()+" not found.");
		MessageDialog.openError(HandlerUtil.getActiveShell(event), "DIM: Initialization Error", ex.getMessage()+" not found. Consult the logs in the console. Please file an appropriate Bug report on the OSATE-DIM GitLab repository, or message dominique.blouin@telecom-paris.fr");
		handleStateException(ex, transformation, event, topSystemInst);
	}
}
