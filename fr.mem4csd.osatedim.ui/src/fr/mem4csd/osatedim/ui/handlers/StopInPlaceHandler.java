package fr.mem4csd.osatedim.ui.handlers;

import org.eclipse.core.commands.AbstractHandler;
import org.eclipse.core.commands.ExecutionEvent;
import org.eclipse.core.commands.ExecutionException;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.emf.common.util.URI;
import org.eclipse.jface.viewers.IStructuredSelection;
import org.eclipse.ui.handlers.HandlerUtil;
import fr.mem4csd.osatedim.ui.DIMStartup;
import fr.mem4csd.osatedim.ui.utils.DeinstantiationUtils;

public class StopInPlaceHandler extends AbstractHandler {
	
	private static final DIMUILogger LOGGER = new DIMUILogger();
	
	@Override
	public Object execute( final ExecutionEvent event )
	throws ExecutionException {
		final IStructuredSelection selection = HandlerUtil.getCurrentStructuredSelection( event );
		final URI resUri = DeinstantiationUtils.getSelectionFileUri( selection.getFirstElement() );
		stopTransformation(resUri);
		return null;
	}
	
	public static void stopTransformation(URI resUri) throws ExecutionException {
		try {
			if ( DIMStartup.getInstance().unregisterTransformation( resUri ) == null ) {
				LOGGER.logWarning("DIM: Incremental in-place deinstantiation for "+ resUri +" not initialized!");
			}
			else {
				LOGGER.logInfo("DIM: Incremental in-place deinstantiation for "+ resUri +" stopped.");
			}
		} catch ( final CoreException ex ) {
			throw new ExecutionException( "Error while stopping synchronization!", ex );
		}
	}
}