package fr.mem4csd.osatedim.ui.handlers;

import org.eclipse.core.runtime.IStatus;
import org.eclipse.core.runtime.Status;
import fr.mem4csd.osatedim.ui.DIMUIPlugin;
import fr.mem4csd.osatedim.utils.DIMLogger;

public class DIMUILogger implements DIMLogger {

	@Override
	public void log(int status, String msg) {
		DIMUIPlugin.getInstance().getLog().log(new Status(status, DIMUIPlugin.getPluginId(), IStatus.OK, msg, null));
	}

	@Override
	public void logInfo(String msg) {
		log(IStatus.INFO, msg);
	}
	
	@Override
	public void logWarning(String msg) {
		log(IStatus.WARNING, msg);
	}
	
	@Override
	public void logError(String msg) {
		log(IStatus.ERROR, msg);
	}
	
	@Override
	public void logCancel(String msg) {
		log(IStatus.CANCEL, msg);
	}
}
