package fr.mem4csd.osatedim.ui;

import org.eclipse.core.resources.IProject;
import org.eclipse.core.resources.ProjectScope;
import org.eclipse.core.runtime.preferences.IScopeContext;
import org.eclipse.jface.preference.IPreferenceStore;
import org.eclipse.ui.plugin.AbstractUIPlugin;
import org.osgi.service.prefs.Preferences;

import fr.mem4csd.osatedim.preference.ClassifierExtensionPreference;
import fr.mem4csd.osatedim.preference.DIMPreferences;

public class DIMUIPlugin extends AbstractUIPlugin{
	
	private static DIMUIPlugin instance = null;
	
	public DIMUIPlugin() {
		instance = this;
		initializeDefaultPreferences(getPreferenceStore());
	}
	
	public static  DIMUIPlugin getInstance() {
		return instance;
	}
	
	// Preference qualifiers
	public static final String PREFS_QUALIFIER = "fr.mem4csd.osatedim.ui";
	public static final String PREF_USE_WORKSPACE = "fr.mem4csd.osatedim.ui.use_workspace";
	public static final String PREF_INHERIT_PROPERTY = "fr.mem4csd.osatedim.ui.inherit_property";
	public static final String PREF_INHERIT_MODE = "fr.mem4csd.osatedim.ui.inherit_mode";
	public static final String PREF_CREATE_INTER_FEATURES = "fr.mem4csd.osatedim.ui.create_inter_features";
	public static final String PREF_ADD_CLASSIFIER_PROPERTY = "fr.mem4csd.osatedim.ui.add_classifier_property";
	public static final String PREF_MODIFY_REUSED = "fr.mem4csd.osatedim.ui.modify_reused";
	public static final String PREF_EXTEND_CLASSIFIER = "fr.mem4csd.osatedim.ui.extend_classifier";
	
	// Default Values
	public static final boolean INHERIT_PROPERTY_DEFAULT = true;
	public static final boolean INHERIT_MODE_DEFAULT = false;
	public static final boolean CREATE_INTER_FEATURES_DEFAULT = false;
	public static final boolean ADD_CLASSIFIER_PROPERTY_DEFAULT = false;
	public static final boolean MODIFY_REUSED_DEFAULT = false;
	public static final String EXTEND_CLASSIFIER_DEFAULT = ClassifierExtensionPreference.REQUIRED_EXTENSION.getLiteral();
	
	@Override
	protected void initializeDefaultPreferences(IPreferenceStore store) {
		store.setDefault(INHERIT_PROPERTY_PROPERTY, INHERIT_PROPERTY_DEFAULT);
		store.setDefault(INHERIT_MODE_PROPERTY, INHERIT_MODE_DEFAULT);
		store.setDefault(CREATE_INTER_FEATURES_PROPERTY, CREATE_INTER_FEATURES_DEFAULT);
		store.setDefault(ADD_CLASSIFIER_PROPERTY_PROPERTY, ADD_CLASSIFIER_PROPERTY_DEFAULT);
		store.setDefault(MODIFY_REUSED_PROPERTY, MODIFY_REUSED_DEFAULT);
		store.setDefault(EXTEND_CLASSIFIER_PROPERTY, EXTEND_CLASSIFIER_DEFAULT);
	}
	
	// Properties
	public static final String INHERIT_PROPERTY_PROPERTY = "inheritProperty";
	public static final String INHERIT_MODE_PROPERTY = "inheritMode";
	public static final String CREATE_INTER_FEATURES_PROPERTY = "createInterFeatures";
	public static final String ADD_CLASSIFIER_PROPERTY_PROPERTY = "addClassifierProperty";
	public static final String MODIFY_REUSED_PROPERTY = "modifyReused";
	public static final String EXTEND_CLASSIFIER_PROPERTY = "extendClassifier";
	
	// Getter methods
	public DIMPreferences getPreferences(IProject project) {
		DIMPreferences preferences = new DIMPreferences(getInheritProperty(project), getInheritMode(project), getCreateInterFeatures(project), getAddClassifierProperty(project), getModifyReused(project)/*, getExtendClassifier(project)*/);
		return preferences;
	}
	
	private final boolean getInheritProperty(IProject project) {
		IScopeContext context = new ProjectScope(project);
		Preferences prefs = context.getNode(PREFS_QUALIFIER);
		if (!prefs.getBoolean(PREF_USE_WORKSPACE, true)) {
			return prefs.getBoolean(PREF_INHERIT_PROPERTY, true);
		} else {
			final IPreferenceStore store = getPreferenceStore();
			return store.getBoolean(INHERIT_PROPERTY_PROPERTY);
		}
	}
	
	private final boolean getInheritMode(IProject project) {
		IScopeContext context = new ProjectScope(project);
		Preferences prefs = context.getNode(PREFS_QUALIFIER);
		if (!prefs.getBoolean(PREF_USE_WORKSPACE, true)) {
			return prefs.getBoolean(PREF_INHERIT_MODE, true);
		} else {
			final IPreferenceStore store = getPreferenceStore();
			return store.getBoolean(INHERIT_MODE_PROPERTY);
		}
	}
	
	private final boolean getCreateInterFeatures(IProject project) {
		IScopeContext context = new ProjectScope(project);
		Preferences prefs = context.getNode(PREFS_QUALIFIER);
		if (!prefs.getBoolean(PREF_USE_WORKSPACE, true)) {
			return prefs.getBoolean(PREF_CREATE_INTER_FEATURES, true);
		} else {
			final IPreferenceStore store = getPreferenceStore();
			return store.getBoolean(CREATE_INTER_FEATURES_PROPERTY);
		}
	}
	
	private final boolean getAddClassifierProperty(IProject project) {
		IScopeContext context = new ProjectScope(project);
		Preferences prefs = context.getNode(PREFS_QUALIFIER);
		if (!prefs.getBoolean(PREF_USE_WORKSPACE, true)) {
			return prefs.getBoolean(PREF_ADD_CLASSIFIER_PROPERTY, true);
		} else {
			IPreferenceStore store = getPreferenceStore();
			return store.getBoolean(ADD_CLASSIFIER_PROPERTY_PROPERTY);
		}
	}
	
	private final boolean getModifyReused(IProject project) {
		IScopeContext context = new ProjectScope(project);
		Preferences prefs = context.getNode(PREFS_QUALIFIER);
		if (!prefs.getBoolean(PREF_USE_WORKSPACE, true)) {
			return prefs.getBoolean(PREF_MODIFY_REUSED, true);
		} else {
			IPreferenceStore store = getPreferenceStore();
			return store.getBoolean(MODIFY_REUSED_PROPERTY);
		}
	}
	
	public static String getPluginId() {
		return instance.getBundle().getSymbolicName();
	}
	
//	private final String getExtendClassifier(IProject project) {
//		IScopeContext context = new ProjectScope(project);
//		Preferences prefs = context.getNode(PREFS_QUALIFIER);
//		if (!prefs.getBoolean(PREF_USE_WORKSPACE, true)) {
//			return prefs.get(PREF_EXTEND_CLASSIFIER, null);
//		} else {
//			final IPreferenceStore store = getPreferenceStore();
//			return store.getString(EXTEND_CLASSIFIER_PROPERTY);	
//		}
//	}
}
