package fr.mem4csd.osatedim.ui;

import java.util.HashMap;
import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.core.resources.IProject;
import org.eclipse.core.runtime.CoreException;
import org.eclipse.core.runtime.QualifiedName;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IPartListener;
import org.eclipse.ui.IStartup;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchPart;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.viatra.query.runtime.api.ViatraQueryEngine;
import org.eclipse.viatra.query.runtime.emf.EMFScope;
import org.osate.aadl2.PropertySet;
import org.osate.aadl2.instance.SystemInstance;
import org.osate.aadl2.modelsupport.FileNameConstants;
import org.osate.aadl2.modelsupport.resources.OsateResourceUtil;

import fr.mem4csd.osatedim.ui.handlers.DIMUILogger;
import fr.mem4csd.osatedim.ui.utils.DeinstantiationUtils;
import fr.mem4csd.osatedim.ui.utils.EditorUtils;
import fr.mem4csd.osatedim.viatra.transformations.DIMTransformationDeltaInplace;

public class DIMStartup implements IStartup, IPartListener {
	
	private static DIMStartup instance = null;
	private static final String SYNCHRONIZED = Boolean.TRUE.toString();
	private final QualifiedName syncPropQualName;
	private final Map<URI, DIMTransformationDeltaInplace> transformationMap;
	
	public static DIMStartup getInstance() {
		return instance;
	}
	
	public DIMStartup() {
		super();
		instance = this;
		transformationMap = new HashMap<URI, DIMTransformationDeltaInplace>();
		syncPropQualName = new QualifiedName(getClass().getPackage().getName(), "synchronized");
	}

	@Override
	public void earlyStartup() {
		registerPartListener();
		
		try {
			initSynchronization();
		}
		catch (final CoreException ex) {
			ex.printStackTrace();
		}
	}
	
	private void registerPartListener() {
		IWorkbenchPage page = null;
		final IWorkbenchWindow window = PlatformUI.getWorkbench().getActiveWorkbenchWindow();
		
		if (PlatformUI.getWorkbench().getActiveWorkbenchWindow() != null) {
			page = window.getActivePage();
		}
		
		if ( page == null )	{
			// Look for a window and get the page off it!
			for (final IWorkbenchWindow windowIter : PlatformUI.getWorkbench().getWorkbenchWindows())	{
				if (windowIter != null) {
					page = windowIter.getActivePage();
			
					if ( page != null ) {
						page.addPartListener(this);

						break;
					}
				}
			}
		}
	}
	
	private void initSynchronization() 
	throws CoreException {
		for (final IEditorReference editorRef :  EditorUtils.getAllOpenEditors()) {
			try {
				final IEditorInput editorInput = editorRef.getEditorInput();
				
				if (editorInput instanceof IFileEditorInput) {
					final IFile file = ((IFileEditorInput) editorInput).getFile();

					if ( FileNameConstants.INSTANCE_FILE_EXT.equals(file.getFileExtension())) {
						final IEditorPart editor = editorRef.getEditor(false);
						partOpened(editor);
					}
				}
			}
			catch(final PartInitException ex) {
				ex.printStackTrace();
			}
		}
	}
	
	public DIMTransformationDeltaInplace getTransformation(final URI resUri) {
		return transformationMap.get(resUri);
	}
	
	public DIMTransformationDeltaInplace registerTransformation(final SystemInstance topSystemInst, DIMUILogger logger)
	throws CoreException {
		final Resource resource = topSystemInst.eResource();
		final URI resUri = resource.getURI();
		final PropertySet dimPropertySet = DeinstantiationUtils.getPluginDIMPropertySet(resource.getResourceSet());
		final ViatraQueryEngine engine = ViatraQueryEngine.on( new EMFScope( resource.getResourceSet() ) );
		final IProject project = OsateResourceUtil.toIFile(resUri).getProject();
		final DIMTransformationDeltaInplace transformation = new DIMTransformationDeltaInplace(topSystemInst, engine, DIMUIPlugin.getInstance().getPreferences(project), dimPropertySet, logger);
		return registerTransformation(resUri, transformation);
	}
	
	public boolean isRegistered(final URI resUri) {
		return transformationMap.containsKey(resUri);
	}
	
	public DIMTransformationDeltaInplace registerTransformation(final URI resUri, final DIMTransformationDeltaInplace transformation) 
	throws CoreException {
		final IFile fileRes = OsateResourceUtil.toIFile(resUri);
		fileRes.setPersistentProperty(syncPropQualName, SYNCHRONIZED);
		if (transformation != null) {
			transformation.execute();
		}
		return transformationMap.put(resUri, transformation);
	}
	
	public DIMTransformationDeltaInplace unregisterTransformation(final URI resUri) 
	throws CoreException {
		final DIMTransformationDeltaInplace transformation = transformationMap.remove(resUri);
		if (transformation != null) {
			transformation.dispose();
		}
		final IFile fileRes = OsateResourceUtil.toIFile(resUri);
		fileRes.setPersistentProperty(syncPropQualName, null);
		return transformation;
	}
	
	@Override
	public void partActivated(IWorkbenchPart part) {
	}

	@Override
	public void partBroughtToTop(IWorkbenchPart part) {
	}

	@Override
	public void partClosed(final IWorkbenchPart part) {
	}
	
	@Override
	public void partDeactivated(IWorkbenchPart part) {
	}

	@Override
	public void partOpened(final IWorkbenchPart part) {
		try {
			if (isSynchronized(part)) {
				final IEditorPart editor = (IEditorPart) part;
				final URI fileUri = OsateResourceUtil.toResourceURI(getFile(editor));
				final DIMTransformationDeltaInplace transformation = transformationMap.remove(fileUri);
				
				if (transformation != null) {
					transformation.dispose();
				}

				if (editor instanceof IEditingDomainProvider) {
					final ResourceSet resSet = ((IEditingDomainProvider) editor).getEditingDomain().getResourceSet();
					final SystemInstance systInstance = DeinstantiationUtils.getSystemInstance(fileUri, resSet);
					registerTransformation(systInstance, new DIMUILogger());
				}
			}
		}
		catch (final CoreException ex)  {
			ex.printStackTrace();
		}
	}
	
	private boolean isSynchronized(final IWorkbenchPart part) 
	throws CoreException {
		if (part instanceof IEditorPart) {
			final IFile file = getFile((IEditorPart) part);
			
			return isSynchronized(file);
		}
		return false;
	}
	
	public boolean isSynchronized(final URI fileResUri) 
	throws CoreException {
		return isSynchronized(OsateResourceUtil.toIFile(fileResUri));
	}
	
	private boolean isSynchronized( final IFile fileRes ) 
	throws CoreException {
		if ( fileRes != null && fileRes.exists() ) {
			return fileRes.getPersistentProperty( syncPropQualName ) != null;
		}
		
		return false;
	}

	private IFile getFile( final IEditorPart editor ) {
		final IEditorInput editorInput = editor.getEditorInput();
		
		if ( editorInput instanceof IFileEditorInput ) {
			return ( (IFileEditorInput) editorInput ).getFile();
		}
		
		return null;
	}
}
