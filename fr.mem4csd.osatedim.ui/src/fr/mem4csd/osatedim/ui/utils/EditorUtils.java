package fr.mem4csd.osatedim.ui.utils;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.core.resources.IFile;
import org.eclipse.ui.IEditorInput;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.IFileEditorInput;
import org.eclipse.ui.IWorkbenchPage;
import org.eclipse.ui.IWorkbenchWindow;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;

public class EditorUtils {

	private EditorUtils(){
		super();
	}
	
	public static List<IEditorReference> getAllOpenEditors() {
		final List<IEditorReference> editorRefs = new ArrayList<IEditorReference>();
		
		for ( final IWorkbenchWindow window : PlatformUI.getWorkbench().getWorkbenchWindows() ) {
		    for ( final IWorkbenchPage page : window.getPages() ) {
		        for (IEditorReference editorRef : page.getEditorReferences()) {
		        	editorRefs.add( editorRef );
		        }
		    }
		}
		
		return editorRefs;
	}
	
	public static IEditorReference getEditorCorrespondingToFile( final IFile file ) {
		final List <IEditorReference> editorList = getAllOpenEditors();
		for ( final IEditorReference editorRef : editorList ) {
			try {
				final IEditorInput editorInput = editorRef.getEditorInput();
				if ( editorInput instanceof IFileEditorInput) {
					if ( file.equals( ( (IFileEditorInput) editorInput ).getFile() ) ) {
						return editorRef;
					}
				}
			}
			catch( final PartInitException ex ) {
				ex.printStackTrace();
			}
		}
		return null;
	}
}
