package fr.mem4csd.osatedim.ui.utils;

import java.util.Map;

import org.eclipse.core.resources.IFile;
import org.eclipse.emf.common.util.URI;
import org.eclipse.emf.ecore.EObject;
import org.eclipse.emf.ecore.resource.Resource;
import org.eclipse.emf.ecore.resource.ResourceSet;
import org.eclipse.emf.ecore.resource.impl.ResourceSetImpl;
import org.eclipse.emf.edit.domain.IEditingDomainProvider;
import org.eclipse.ui.IEditorPart;
import org.eclipse.ui.IEditorReference;
import org.eclipse.ui.PartInitException;
import org.eclipse.ui.PlatformUI;
import org.eclipse.ui.ide.IDE;
import org.osate.aadl2.PropertySet;
import org.osate.aadl2.instance.SystemInstance;
import org.osate.aadl2.modelsupport.EObjectURIWrapper;
import org.osate.aadl2.modelsupport.FileNameConstants;
import org.osate.aadl2.modelsupport.resources.OsateResourceUtil;
import org.osate.pluginsupport.PluginSupportUtil;
import fr.mem4csd.osatedim.ui.handlers.DIMListener;
import fr.mem4csd.osatedim.utils.PackageUtils;

public class DeinstantiationUtils {
	
	private DeinstantiationUtils() {
		super();
	}
	
	public static URI getSelectionFileUri( final Object selection ) {
		//From the Instance Model Editor or Outline
		if (selection instanceof SystemInstance) {
			return ( (SystemInstance) selection ).eResource().getURI();
		}
		// Object From the AADL Navigator
		if (selection instanceof EObjectURIWrapper) {
			if (((EObjectURIWrapper) selection).getEClass().getName().matches("SystemInstance")) {
				return ((EObjectURIWrapper) selection).getUri();
			}
		}
		// AAXL2 File From the AADL Navigator
		if (selection instanceof IFile) {
			return OsateResourceUtil.toResourceURI( (IFile) selection );
		}
		return null;
	}

	public static SystemInstance openSelectionTopSystemInstance(	final Object selection ) {
		//From the Instance Model Editor or Outline
		if (selection instanceof SystemInstance) {
			URI instanceUri = ((SystemInstance) selection).eResource().getURI();
			return openSystemInstance(OsateResourceUtil.toIFile(instanceUri));
		}
		// Object From the AADL Navigator
		if (selection instanceof EObjectURIWrapper) {
			if (((EObjectURIWrapper) selection).getEClass().getName().matches("SystemInstance")) {
				URI instanceUri = ((EObjectURIWrapper) selection).getUri().trimFragment();
				return openSystemInstance(OsateResourceUtil.toIFile(instanceUri));
			}
		}
		// AAXL2 File From the AADL Navigator
		if (selection instanceof IFile) {
			return openSystemInstance((IFile) selection);
		}
		return null;
	}
	
	public static SystemInstance openSystemInstance(final IFile file) {
		final IEditorReference editorRef = EditorUtils.getEditorCorrespondingToFile(file);
		IEditorPart editor = null ;
		if (editorRef == null) {
			try {
				editor = IDE.openEditor(PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage(), file);
			} catch (PartInitException e) {
				e.printStackTrace();
			}
		} else {
			editor = editorRef.getEditor( true );
		}
		PlatformUI.getWorkbench().getActiveWorkbenchWindow().getActivePage().addPartListener(new DIMListener(editor));
		
		if (editor instanceof IEditingDomainProvider) {
			final ResourceSet resSet = ( (IEditingDomainProvider) editor ).getEditingDomain().getResourceSet();
			return getSystemInstance( file, resSet );
		} 
		return getSystemInstance( file, new ResourceSetImpl() );
	}
	
	public static SystemInstance getSystemInstance(final IFile file, final ResourceSet resSet) {
		return getSystemInstance(OsateResourceUtil.toResourceURI(file), resSet);
	}
	
	public static SystemInstance getSystemInstance(final URI fileUri, final ResourceSet resSet) {
		final Resource res = resSet.getResource(fileUri, true);
		if ( !res.getContents().isEmpty() ) {
			final EObject root = res.getContents().get( 0 );
			if ( root instanceof SystemInstance ) {
				return (SystemInstance) root;
			}
		}
		return null;
	}
	
	public static URI computeDeclarativeUri(final URI instanceModelUri, final SystemInstance systemInstance) {
		final String declFileName = PackageUtils.computeDeclarativeName(instanceModelUri, systemInstance);
		return instanceModelUri.trimSegments( 2 ).appendSegment( declFileName ).appendFileExtension( FileNameConstants.SOURCE_FILE_EXT );			
	}
	
	public static PropertySet getPluginDIMPropertySet(ResourceSet resSet) {
		for (final Map.Entry<URI, String> entry : PluginSupportUtil.getContributedPropertySets().entrySet()) {
			 if ( "DIM_Properties".equals( entry.getValue() ) ) {
				return (PropertySet) resSet.getEObject( entry.getKey().appendFragment( entry.getValue() ), true );
			 }
		}
		return null;
	}

}